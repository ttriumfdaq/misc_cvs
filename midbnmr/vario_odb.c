/*
CVS log information:
$Log: vario_odb.c,v $
Revision 1.1  2005/10/17 17:16:39  suz
initial CVS version

*/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "midas.h"
/* #include "camp_clnt.h" /* just need LEN_NODENAME for darc_imusr.h...  */
#define LEN_NODENAME 127

#include "experim.h"
#include "darc_odb.h"


/* header struct from darc_odb.h revision 1.17 */

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */

  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[48]; /* increase this from 15 */
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;

  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  long ncycles;           /* not used */
    
  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[16];     /* time zero */     
  long his_good_begin[16];/* first good bin */
  long his_good_end[16];  /* last good bin */
  long his_first_bkg[16]; /* first background bin */
  long his_last_bkg[16];  /* last background bin */
  
  char his_titles[16][32]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];

  long nscal; /* number of scalers in use */
  long scaler_save[80];/* contains scaler data */
  char scaler_titles[80][32];

  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[16];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

} D_ODB_117;


/* header struct from darc_odb.h revision 1.16 */

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */

  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[48]; /* increase this from 15 */
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;

  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  long ncycles;           /* not used */
    
  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[16];     /* time zero */     
  long his_good_begin[16];/* first good bin */
  long his_good_end[16];  /* last good bin */
  long his_first_bkg[16]; /* first background bin */
  long his_last_bkg[16];  /* last background bin */
  
  char his_titles[16][32]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];

  long nscal; /* number of scalers in use */
  long scaler_save[80];/* contains scaler data */
  char scaler_titles[80][32];
  
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[16];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

} D_ODB_116;


/* header struct from darc_odb.h revision 1.13 */

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */

  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[15];
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;
  
  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  long ncycles;           /* not used */

  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[16];     /* time zero */     
  long his_good_begin[16];/* first good bin */
  long his_good_end[16];  /* last good bin */
  long his_first_bkg[16]; /* first background bin */
  long his_last_bkg[16];  /* last background bin */
  
  char his_tits[16][11]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];

  long nscal; /* number of scalers in use */
  long scaler_save[80];/* contains scaler data */
  char scaler_titles[80][32];
  
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[16];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

} D_ODB_113;


/* header struct from darc_odb.h revision 1.10 */

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */
  
  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[15];
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;
  
  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  long ncycles;           /* not used */

  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[16];     /* time zero */     
  long his_good_begin[16];/* first good bin */
  long his_good_end[16];  /* last good bin */
  long his_first_bkg[16]; /* first background bin */
  long his_last_bkg[16];  /* last background bin */
  
  char his_tits[16][11]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 
 
  long nscal; /* number of scalers in use */
  long scaler_save[80];/* contains scaler data */
  char scaler_titles[80][32];
  
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[16];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

} D_ODB_110;

/* header struct from darc_odb.h revision 1.07 */

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */
  
  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[80];
  char sample[15];
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;
  
  /* B-nmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms) */
  long ncycles;

  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[16];     /* time zero */     
  long his_good_begin[16];/* first good bin */
  long his_good_end[16];  /* last good bin */
  long his_first_bkg[16]; /* first background bin */
  long his_last_bkg[16];  /* last background bin */
  
  char his_tits[16][11]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 
  
  long nscal; /* number of scalers in use */
  long scaler_save[60];/* contains scaler data */
  char scaler_titles[60][32];
  
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[16];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

} D_ODB_107;

/*
 *  Macros to copy elements of header from old versions of structure to new
 *  version.  (This would be a lot easier in a higher-level language!)
 */
#define _c_val(p,n) memcpy( (char*)&tmp_hdr.n, (void*)&(p->n), sizeof(p->n) )
#define _upgrade_hdr(p) \
  _c_val(p,area);\
  _c_val(p,rig);\
  _c_val(p,mode);\
  _c_val(p,run_state);\
  _c_val(p,run_number);\
  _c_val(p,experiment_number);\
  _c_val(p,experimenter);\
  _c_val(p,run_title);\
  _c_val(p,sample);\
  _c_val(p,orientation);\
  _c_val(p,field);\
  _c_val(p,temperature);\
  _c_val(p,start_time_str);\
  _c_val(p,start_time_binary);\
  _c_val(p,stop_time_str);\
  _c_val(p,stop_time_binary);\
  _c_val(p,dwell_time);\
  _c_val(p,beam_time);\
  _c_val(p,ncycles);\
  _c_val(p,his_n);\
  _c_val(p,his_nbin);\
  _c_val(p,nchan);\
  _c_val(p,his_bzero);\
  _c_val(p,his_good_begin);\
  _c_val(p,his_good_end);\
  _c_val(p,his_first_bkg);\
  _c_val(p,his_last_bkg);\
  _c_val(p,purge_after);\
  _c_val(p,save_dir);\
  _c_val(p,camp_host);\
  _c_val(p,nscal);\
  _c_val(p,scaler_save);\
  _c_val(p,scaler_titles);\
  _c_val(p,time_save);\
  _c_val(p,file_save);\
  _c_val(p,his_total);\
  _c_val(p,total_save)

/* omitted due to variations.  The titles are variously named his_titles and his_tits, 
 * so they must be copied individually.  The variable names can be ignored.
  _c_val(p,his_titles);\
  _c_val(p,temperature_variable);\
  _c_val(p,field_variable);\
*/

/* Upgrade midas darc header block.
 * Params: size: size of header event (used as indicator of header version)
 *         pmidhdr: pointer to header data
 * Changes: header data at *pmidhdr, possibly rearranged for version
 * Returns: 0 for failure; header version number for success.
 */
int upgrade_mid_hdr ( int size, D_ODB *pmidhdr )
{
  D_ODB tmp_hdr = { 0 };
  int ver = 118;

  D_ODB_117 *hdr117;
  D_ODB_116 *hdr116;
  D_ODB_113 *hdr113;
  D_ODB_110 *hdr110;
  D_ODB_107 *hdr107;


  if ( size == sizeof(D_ODB) ) return( ver );

  if ( size == sizeof(D_ODB_117) ) {
    ver = 117;
    hdr117 = (D_ODB_117*)pmidhdr;
    _upgrade_hdr(hdr117);
    _c_val(hdr117,his_titles);
  } else if ( size == sizeof(D_ODB_116) ) {
    ver = 116;
    hdr116 = (D_ODB_116*)pmidhdr;
    _upgrade_hdr(hdr116);
    _c_val(hdr116,his_titles);
  } else if ( size == sizeof(D_ODB_113) ) {
    ver = 113;
    hdr113 = (D_ODB_113*)pmidhdr;
    _upgrade_hdr(hdr113);
    memcpy( (char*)&tmp_hdr.his_titles, (void*)&(hdr113->his_tits), sizeof(hdr113->his_tits) );
  } else if ( size == sizeof(D_ODB_110) ) {
    ver = 110;
    hdr110 = (D_ODB_110*)pmidhdr;
    _upgrade_hdr(hdr110);
    memcpy( (char*)&tmp_hdr.his_titles, (void*)&(hdr110->his_tits), sizeof(hdr110->his_tits) );
  } else if ( size == sizeof(D_ODB_107) ) {
    ver = 107;
    hdr107 = (D_ODB_107*)pmidhdr;
    _upgrade_hdr(hdr107);
    memcpy( (char*)&tmp_hdr.his_titles, (void*)&(hdr107->his_tits), sizeof(hdr107->his_tits) );
  } else {
    return( 0 );
  }

  /* Copy temporary hdr block back into main variable */  
  memcpy( (char*)pmidhdr, (void*)&tmp_hdr, sizeof(tmp_hdr) );

  return( ver );
}
