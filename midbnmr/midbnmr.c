/*
 *  midbnmr.c -- main and CLI dispatch routines for program
 *		 converting bnmr midas files to mud files.
 *
 *
 * CVS log information:
 * $Log: midbnmr.c,v $
 * Revision 1.5  2008/11/11 02:49:15  asnd
 * Handle Epics variables.
 * Make two passes to get all header events before any data events.
 *
 * Revision 1.3  2006/06/27 00:00:39  asnd
 * Implement TD-LCR conversion (TD hisrograms at each sweep value).
 *
 * Revision 1.2  2006/06/18 03:34:22  asnd
 * Implement mid2td (make time-differential mud files)
 *
 * Revision 1.1  2005/10/17 17:13:53  suz
 * initial CVS version
 *
 *
 *  Revision history prior to CVS version:
 *  13-Apr-2005   Quick change: Make bNQR L/R act like bNMR F/B
 *  10-Apr-2005   Implement mid2ts; partial mid2td
 *  20-Oct-2003   Multiple time-bin ranges
 *  27-June-2002  Camp var scan
 *   8-July-2002  Fix empty spectra bugs
 */

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif

#include "midas.h"
#include "msystem.h"
#include "ybos.h"

#define BOOL_DEFINED
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
/* #include "camp_clnt.h" /* just need LEN_NODENAME for darc_imusr.h...  */
#define LEN_NODENAME 127
#include "mud_util.h"

#include "experim.h"
#include "darc_odb.h"
#include "trii_fmt.h"
#include <readline/readline.h>
#include <readline/history.h>
#include <getopt.h>


#define MAX_BIN_RANGES 5

/* * /
#define DEBUG
/* */

/*
 * Terminated strncpy.  len arg should be *LESS* than the dimensioned length
 */
#define _strncpy(to,from,len) \
    strncpy(to,from,len); \
    to[len]='\0';


/*  Global variables */
static DWORD  cycle;
static DWORD  scycle;
static DWORD  scan;
static DWORD  phase;
static DWORD  scanflg;
static DWORD  cellread;
static DWORD  cellmv;
static DWORD  frequency;

/* 
   scanflg:  0            1        2      256        512
   scantyp:  0            1        2      3          4
   what:   ppg frequency  Na Cell  Laser  Camp freq  Camp Mag
*/

static DWORD  indepval;
static BOOL   headYet = FALSE;
static int    ndumpTot;
static double currcamp[64];
static double currepics[64];
static BOOL   cCopied, eCopied;

static int    numAllArr;
static int    numMonArr;
static int    numHistArr;
static int    numScalArr;
static int    numCampArr, numEpicsArr;
static int    numCamp, numEpics;
static int    numTimes;
static BOOL   useBits;
static BOOL   usePhas;

static int    numPoint;

static int    itmult;
static int    icmult;

static D_ODB  mid_hdr;

static char  * MonBankNames[8] = { "HM00", "HM01", "HM04", "HM05" };
static char  * HisBanksBNMR[8] = { "HIBP", "HIFP" };
static char  * HisBanksBNQR[8] = { "HM02", "HM03" };
static BOOL    bNQRflag = FALSE;

/*
 *  Integer tokens corresponding to the command, defining what conversion is performed.
 */
enum CMD_TOK {
  Cmd_NONE,
  Cmd_TI,
  Cmd_TD,
  Cmd_TS,
  Cmd_3D
};
typedef enum CMD_TOK Cmd_t;

/* Global variables for ubits info */

enum UBFLAGVAL {
  UB_none,
  UB_provided,
  UB_forced
};
typedef enum UBFLAGVAL UBflag_t;

static UBflag_t UBcycFlag = UB_none;   /* flag if/how prototype ubits provided */

typedef struct {
  int num;
  double low, high, offset, sum, sum2, sum3;
} CAMP_SUMS_t;

/* 
 *  Allocated memory:
 */

/* Array with one cycle of ubits provided as a command-line parameter (malloced) */
static int *UBcyc = NULL; 
static int numUBc = 0;

/* Array of user-bit offsets (4*val) long enough for hist banks (malloced) */
static int *ubits = NULL;

/* Array of scaler totals, allocated by realloc and zeroed in M2Malloc; not freed: */
static unsigned int *tots = NULL;

/* Camp/Epics variable(s) info; static allocation */
static IMUSR_CAMP_VAR campVars[64], epicsVars[64];
static CAMP_SUMS_t campSums[64], epicsSums[64];

/* Plus mud file structures, allocated by MUD_new and released by MUD_free */

/*
 *  Function prototypes
 */

int BNMRmid2MUD_convert( Cmd_t icmd, char* infile, char* outfile, 
                int nBinRanges, int bin0[], int bin1[], char btag[][8], int f0, int f1 );

int BNMRmid_TI_write( Cmd_t icmd, char* outfile, 
                int nBinRanges, int bin0[], int bin1[], char btag[][8], int f0, int f1 );

int BNMRmid_TD_write( Cmd_t icmd, char* outfile, char btag[][8], int f0, int f1 );

static int M2Mcommand( int argc, char* argv[] );

static char* getHistName( int i, char btag[][8] );

int upgrade_mid_hdr ( int size, D_ODB *pmidhdr );


/*  Display help screen  */
static void
M2Musage()
{
    printf( "\n" );
    printf( "Usage:\n" );
    printf( "midbnmr <command> [-o<outfile>] [-b<from>,<to>[,<tag>]] [-f<from>,<to>] -u<digits> <infile>\n" );
    printf( "\n" );
    /*    printf( "\t<command>      -  mid2ti | mid2td | mid2ts | quit | help \n" ); */
    printf( "    <command>  -   mid2ti | mid2td | mid2ts | quit | help  (more in future)\n" );
    printf( "          mid2ti:  Time integral mud file (summing time-bins)\n" );
    printf( "          mid2td:  Time differential mud file (summing sweep values)\n" );
    printf( "          mid2ts:  Time sequence (TI format) mud file (separate time-bins)\n" );
    printf( "          mid2mtd: Multiplexed TD histograms (separated sweep values)\n" );
    printf( "    <infile>            - specify input file name\n" );
    printf( "    -o<outfile>         - specify output file name\n" );
    printf( "    -b<from>,<to>       - specify time-bin range to integrate\n" );
    printf( "    -b<from>,<to>,<tag> - additional time-bin ranges, with identifiers\n" );
    printf( "    -f<from>,<to>       - specify frequency range to combine\n" );
    printf( "    -u<digits> (-U)     - provide (force) user-bits; digits repeat cyclically\n" );

    printf( "\n" );
}


int
main( argc, argv )
    int argc;
    char* argv[];
{
    char prog[8] = { "midbnmr" };

    /***************** 
     * Hack for allocation bugs, probably in the yb_* routines
     *
     *setenv ("MALLOC_CHECK_", "0", 0);
    /*****************/

#ifdef DEBUG
    printf ( "Debug mode\n") ;
#endif
    if( argc > 1 )
    {
        M2Mcommand( argc, argv );
        exit( 0 );
    }
    else
    {
      int i;
      char* p;
      char* line_read = NULL;
      int aargc;
      char* aargv[16];

      aargv[0] = prog;
      
      do {
        if( line_read ) free( line_read );
        line_read = readline( "midbnmr> " );
        p = line_read;
        aargc = 1;
        if( line_read )
        {
          add_history( line_read );
          while( 1 )
          {
            while( *p && !isgraph( *p ) ) *p++ = 0;
            if( *p == 0 ) break;
            aargv[aargc++] = p;
            while( isgraph( *p ) ) p++;
          }
        }
      } while( M2Mcommand( aargc, aargv ) != -1 );
      if( line_read ) free( line_read );
    }
}


static int
M2Mcommand( argc, argv )
    int argc;
    char* argv[];
{
  int   c;
  int   status;
  int   i, n, l;
  char* p;
  Cmd_t icmd;
  char  command[64] = { 0 };
  char  infile[64] = { 0 };
  char  outfile[64] = { 0 };
  int   nBinRanges = 0;
  int   bin0[MAX_BIN_RANGES] = { 0 };
  int   bin1[MAX_BIN_RANGES] = { 0 };
  char  btag[MAX_BIN_RANGES][8] = { 0 };
  char  freqs[32] = { 0 };
  int   f0 = 0;
  int   f1 = 0;

  numUBc = 0;
  UBcycFlag = UB_none;

  optind = 0;

  if( argc == 1 ) return( 0 );

  while( 1 )
  {
    c = getopt( argc, argv, "b:f:o:u:U:" );
    if( c == EOF ) break;

    switch( c )
    {
      case 'b': 
        if( sscanf( optarg, "%d,%d,%8s", &bin0[nBinRanges], &bin1[nBinRanges], btag[nBinRanges] ) >=2 )
          nBinRanges++;
        break;

      case 'U':
      case 'u':
        l = strlen(optarg);
        if( l > 0 ) 
        {
          if( UBcyc ) free( UBcyc );
          UBcyc = malloc( l*sizeof(int) );
          for(numUBc = 0 ; numUBc < l ; numUBc++) 
          {
            UBcyc[numUBc] = optarg[numUBc] - '0';
            if( UBcyc[numUBc] < 0 || UBcyc[numUBc] > 3 )
            {
              printf("Invalid ubits specification\n");
              M2Musage();
              return( 0 );
            }
          }
        }
        UBcycFlag = ( c == 'u' ? UB_provided : UB_forced );
        break;

      case 'f':
        _strncpy( freqs, optarg, 31 );
        break;

      case 'o':
        _strncpy( outfile, optarg, 63 );
        break;

      default:
        printf( "invalid option %c\n", c );
        M2Musage();
        return( 0 );
    }
  }

  switch( argc-optind )  /* number of non-option parameters */
  {
      case 1:
        strcpy( command, argv[argc-1] );
        strcpy( infile, "" );
        break;

      case 2:
        strcpy( command, argv[argc-2] );
        strcpy( infile, argv[argc-1] );
        break;

      default:
        M2Musage();
        return( 0 );
  }

  if( !strncmp( command, "e", 1 ) || !strncmp( command, "q", 1 ) )
  {
      return( -1 );
  }

  if( !strcmp( infile, "" ) )
  { 
      M2Musage();
      return( 0 );
  }

  if( !strchr( infile, '.' ) ) strcat( infile, ".mid" );

  if( !strcmp( outfile, "" ) )
  {
      if( strlen( infile ) > 3 && strncmp( infile, "run", 3) == 0 )
        p = infile + 3;
      else
        p = infile;

      if( sscanf( p, "%d", &n) == 1 )  /* there is a number */
        sprintf( outfile, "%.6d.msr", n );
      else
        strcpy( outfile, p );
      p = strchr( outfile, '.' );
      if( p ) *p = '\0';
  }

  if( !strchr( outfile, '.' ) ) strcat( outfile, ".msr" );

  icmd = Cmd_NONE;
  if( !strncmp( command, "mid2ti", 6 ) ) icmd = Cmd_TI ; /* time-integral (like ImuSR) */
  if( !strncmp( command, "mid2td", 6 ) ) icmd = Cmd_TD ; /* time-differential (like TD-muSR) */
  if( !strncmp( command, "mid2ts", 6 ) ) icmd = Cmd_TS ; /* time-series - all points consecutively */
  if( !strncmp( command, "mid23d", 6 ) ) icmd = Cmd_3D ; /* multiplexed TD  */
  if( !strncmp( command, "mid2mtd", 7 ) ) icmd = Cmd_3D ; /* multiplexed TD  */
  if( icmd == Cmd_NONE ) 
  {
    M2Musage();
    return( 0 );
  }

  if( strcmp( freqs, "" ) )
  {
      sscanf( freqs, "%d,%d", &f0, &f1 );
  }

  if( nBinRanges < 1 ) 
  {
    nBinRanges = 1;
  }
  else if( icmd != Cmd_TI ) /* ignore time-bin ranges for TD and TS conversions */
  {
    nBinRanges = 1;
    bin0[0] = 0;
    bin1[0] = 0;
    btag[0][0] = '\0';
    printf( "Warning: Bin range request ignored!\n" );
  }

#ifdef DEBUG
  printf( "Convert %s to %s, using %s,\n", infile, outfile, command );
  printf( "Integrating %d bin range(s): ", nBinRanges);
  for( i=0 ; i<nBinRanges ; i++ )
    printf( "%d-%d (%s) ", bin0[i],bin1[i],btag[i]);
  printf( "\n" );
#endif

  status = BNMRmid2MUD_convert( icmd, infile, outfile, nBinRanges, bin0, bin1, btag, f0, f1 );

  switch( status )
  {
  case 0:
    break;

  case 1:
    fprintf( stderr, "Error %d: File %s could not be opened.\n", status, infile );
    break;

  case 2:
    fprintf( stderr, "Error %d: Could not allocate memory for data.\n", status );
    break;

  case 3:
  case 4:
  case 5:
  case 6:
    fprintf( stderr, "Error %d: Invalid Beta-NMR Midas file.\n", status );
    break;

  case 7:
    fprintf( stderr, "Error %d: Output file could not be written.\n", status );
    break;

  default:
    fprintf( stderr, "Error %d: Something wrong.\n", status );
    break;

  }
  return( 0 );
}


/******************************************************************************/

/* 
 *   BNMRmid2MUD_convert 
 *
 *   Convert midas file for beta-NMR "type 1" experiment to a mud file.
 *   The midas file contains successive readouts of the multi-channel scaler,
 *   appearing like a time-differential histogram.  Each readout can have
 *   different values for some independent variable.  Within the readout sweep,
 *   some other independent variables my be changing in a regular cycle (user
 *   bits) or it may be a pure time-differential histogram.
 *
 *   Mud output is one of four types, depending on icmd:
 *  
 *   1) mid2ti (time integral)
 *      Make a TI (LCR) file by taking the total counts in each TD histogram,
 *      and recording these totals vs independent variable (frequency or
 *      cellmv).  If user bits are in use, keep separate totals for each
 *      value of the user bits.  Total only over a specified bin range.
 *   2) mid2td (time differential)
 *      Make a TD mud file by summing all the TD histograms taken within
 *      a specified frequency (independent variable) range.  This makes 
 *      no sense if user bits are in use.
 *   3) mid2ts (time series)
 *      Make a TD file by chaining together all the TD histograms.
 *      Neither user bits nor frequency-range selection are used.
 *   4) mid23d
 *      Like TI (1) but remember the huge array of histograms, and write 
 *      a series of TD histograms at end of the file.  (Fail if user bits
 *      are in use.)  ... maybe.
 *
 *   The return value is a status code:
 *
 *   0) Success
 *   1) file <infile> could not be opened
 *   2) could not allocate memory
 *   3) events out of order (data before header etc)
 *   4)
 *   5) could not parse camp paths
 *   6) [previously, number of camp variables changed]
 *   7) could not write output mud file
 */

static int M2Malloc( unsigned int** pparray, int ncol, int nrow );
static void camp_values_copy( DWORD ic );
static void epics_values_copy( DWORD ic );
static int his_event_do ( DWORD* plocal, Cmd_t icmd, int nBinRanges, int bin0[], int bin1[], int f0, int f1 );
static int head_event_do ( DWORD* plocal, Cmd_t icmd );
static int camp_event_do ( DWORD* plocal, Cmd_t icmd );
static int epics_event_do ( DWORD* plocal, Cmd_t icmd );
static int scal_event_do ( DWORD* plocal );


int
BNMRmid2MUD_convert( icmd, infile, outfile, nBinRanges, bin0, bin1, btag, f0, f1 )
     Cmd_t icmd;
     char  infile[], outfile[];
     int   nBinRanges;
     int   bin0[MAX_BIN_RANGES];
     int   bin1[MAX_BIN_RANGES];
     char  btag[MAX_BIN_RANGES][8];
     int   f0, f1;
{

  FILE   *outFH; 

  char   *pevent;
  DWORD  evtlen;

  int    i, id, msk, status;
  int    ndumpTot;

  ndumpTot = 0;

  status   = 0;

#ifdef DEBUG
    printf ( "mid2mudConvert mid file \"%s\" to mud file \"%s\" with %d bin ranges:",
             infile,outfile,nBinRanges) ;
    for( i=0 ; i<nBinRanges ; i++ )
      printf( " %d-%d (%s)", bin0[i],bin1[i],btag[i]);
    printf( "\n" );
#endif

  headYet  = FALSE ;   /* Have we processed header event yet? */
  useBits  = FALSE ;   /* Do we have user-bits? */
  numCamp  = 0 ;       /* Number of Camp variables */
  numEpics = 0 ;       /* Number of Epics variables */
  cCopied  = TRUE ;    /* Have Camp values been copied for current scycle? */
  eCopied  = TRUE ;    /* Have Epics values been copied for current scycle? */
  numHistArr  = 16 ;   /* Number of (F,B) histogram arrays per time-bin range */
  numScalArr  = 16 ;   /* Number of scaler arrays (multiple of numHistArr) */
  numMonArr   = 6 ;    /* Number of extra monitored scalers */
  numCampArr  = 0 ;    /* Number of Camp variables histogrammed */
  numEpicsArr = 0 ;    /* Number of Epics variables histogrammed */
  numAllArr   = 0 ;    /* Total number of arrays, incl indep var, scalers, camp and epics*/
  numTimes    = 1 ;    /* Number time-bins (for TD; distinct from nBinRanges) */
  
  cycle      = 0 ;
  scycle     = 0 ;
  scan       = 0 ;
  phase      = 0 ;
  scanflg    = 0 ;
  cellmv     = 0 ;
  frequency  = 0 ;
  indepval   = 0 ;
  numPoint   = 0 ;

  /*
   *  Cases of icmd.
   *  TI:  Total over time-bin range, then append to hists
   *  TD:  No tots, but sum the TD histograms (no ubits)
   *  TS:  No tots, but chain the TD histograms (ignore helicity and ubits)
   *  3D:  Preserve raw arrays when 3d.
   */

  switch ( icmd ) {

  case Cmd_TI:
    usePhas = TRUE;   /* sort helicities */
    itmult = 0;       /* combine time bins */
    icmult = 1;       /* separate cycles */
    break;

  case Cmd_TD:
    usePhas = TRUE;   /* sort helicities */
    itmult = 1;       /* separate time bins */
    icmult = 0;       /* combine cycles */
    break;

  case Cmd_TS:
    usePhas = TRUE;   /* sort helicities */  /* FALSE;  /* combine helicities */
    itmult = 1;       /* separate time bins */
    icmult = 1;       /* separate cycles */
    break;

  case Cmd_3D:
    usePhas = TRUE;   /* sort helicities */
    itmult = 1;       /* separate time bins */
    icmult = 1;       /* separate cycles, but repeated indep values will be summed */
    break;

  }
  /* 
   * Initial checks on bin and frequency ranges are here, but
   * further checks (on the exact bin ranges) are deferred until
   * after the header event is processed (case 14, just below)
   */

  if( f0 >= f1 || f0 < 0 )
  {
      f0 = -INT_MAX;
      f1 = INT_MAX;
  }
  if( nBinRanges < 0 || nBinRanges > MAX_BIN_RANGES )
      nBinRanges = 1;
  
  /*
   * In case I previously returned without freeing ubits...
   */
  if( ubits ) 
  {
    free( ubits );
    ubits = NULL;
  }


  /***************************************************************************\
  *                                                                           *
  *  The independent variables configuration is being delayed until after     *
  *  the start of data taking.  Yuck.  We need to do two passes through       *
  *  the input file to collect header information first, then to collect      *
  *  the data.                                                                *
  *                                                                           *
  \***************************************************************************/


  /***************************************************************************\
  *              Preliminary  Header  Event  Processing  Loop                 *
  \***************************************************************************/

  /* open input data file */
  if (yb_any_file_ropen (infile, FORMAT_MIDAS) != SS_SUCCESS)
  {
    return( 1 );
  }

  while (yb_any_event_get(FORMAT_MIDAS, (void *)&pevent, &evtlen) == YB_SUCCESS)
  {
    yb_any_event_swap(FORMAT_MIDAS, pevent);

    id = ((EVENT_HEADER *)(pevent))->event_id;
    msk = ((EVENT_HEADER *)(pevent))->trigger_mask;

    if ( id == 14 )
    {                /*  Header event  (also called DARC event) */
      status = head_event_do( (DWORD *)pevent, icmd );
      /* 
       *  Now perform sanity checks on bin ranges. Bin numbering is natural 1...n, not
       *  C array style 0...n-1 (??).
       */
      for( i=0; i<nBinRanges; i++ )
      {
          if( bin0[i] >= bin1[i] || bin0[i] < 0 )
          {
              bin0[i] = 1;
              bin1[i] = mid_hdr.his_nbin;
          }
          if( bin0[i] < 1 )  bin0[i] = 1;
          if( bin1[i] > mid_hdr.his_nbin )  bin1[i] = mid_hdr.his_nbin;
      }
    }
  }

  /*
   *   I would like to "rewind" the file here, but no midas function for it!?
   */
  yb_any_file_rclose(FORMAT_MIDAS);


  /***************************************************************************\
  *                      Data  Event  Processing  Loop                        *
  \***************************************************************************/

  if (yb_any_file_ropen (infile, FORMAT_MIDAS) != SS_SUCCESS)
  {
    return( 1 );
  }

  while (yb_any_event_get(FORMAT_MIDAS, (void *)&pevent, &evtlen) == YB_SUCCESS)
  {
    yb_any_event_swap(FORMAT_MIDAS, pevent);

    id = ((EVENT_HEADER *)(pevent))->event_id;
    msk = ((EVENT_HEADER *)(pevent))->trigger_mask;

    /*
     *  Always allocate enough array space for the *next* scycle.
     */
#ifdef DEBUG
    //    printf("Check allocation for  numPoint=%d, evtdim=%d numAllArr=%d\n",
    //       numPoint,icmult*numPoint + 1,numAllArr);
#endif
    if( M2Malloc( &tots, numAllArr*numTimes, icmult*numPoint + 1 ) )
    {
        fprintf( stderr, "ERROR!  Failed to expand arrays [dim=%d].\n", icmult*numPoint);
        status = 2 ;
        id = -32768 ;
    }

    switch ( id )
    {
    case 2: /*   Histogram event */
      status = his_event_do( (DWORD *)pevent, icmd, nBinRanges, bin0, bin1, f0, f1 );
      break;

    case 3: /*   H-Scaler event */
      status = scal_event_do( (DWORD *)pevent );
      break;

    case 13: /*  Camp values  */
#ifdef DEBUG
      printf("Call camp_event_do\n");
#endif
      status = camp_event_do( (DWORD *)pevent, icmd );
      break;

    case 14: /*  Header event  (also called DARC event) */
      break;

    case 19: /*  Epics values  */
#ifdef DEBUG
      printf("Call epics_event_do\n");
#endif
      status = epics_event_do( (DWORD *)pevent, icmd );
      break;

    case -32767: /*  ignore these, they're bogus */
    case -32768:
    case 4:
      break;
    default:
      printf( "WARNING:  Unexpected event with ID %d\n",id);
      break;
    }
    if( status ) 
    {
      if( !ubits )
      {
        free(ubits);
        ubits = NULL;
      }
      return( status );
    }
  }


  if( icmd == Cmd_TD || icmd == Cmd_3D ) {
    status = BNMRmid_TD_write( icmd, outfile, btag, f0, f1 );
  }
  else {
    /* copy current camp values into final point */
    camp_values_copy( numPoint - 1 );
    epics_values_copy( numPoint - 1 );
    status = BNMRmid_TI_write( icmd, outfile, nBinRanges, bin0, bin1, btag, f0, f1 );
  }

  yb_any_file_rclose(FORMAT_MIDAS);

  return( status );
}




/* 
ID mask # banks
 2  1   5 CYCL(2) HIBP HIFP HM00 HM01 HM02 HM03 HM04 HM05 UBIT
 3  1   1 HSCL(40)
 4  1   1 CH01(100)  ---- debugging???
13  1   1 CVAR
14  1   2 DARC CAMP EPICS

*/

/*
 * Copy Camp values into the tots list, but putting in floats (disguised as ints)
 * This is only for the sweep runs (TI and TS, not TD).
 */

static void camp_values_copy( ic )
     DWORD ic;  /* index:  point number minus 1 */
{
  int i, j;
  float f;

  if ( itmult )
  { /* fill each time bin with the same camp value, for each camp variable */

    for( i=0; i<numCamp; i++ )
    {
      f = currcamp[i];
#ifdef DEBUG
      printf("Copy camp variable %d value %f %d times.\n", i,f,numTimes);
#endif
      for( j=0; j<numTimes; j++ )
      {
        tots[numTimes*(numAllArr*ic+1+numScalArr+numMonArr+i)+j] = *((int*)&f); /* float not converted */
      }
    }
  }
  else
  { /* copy each camp variable */
    for( i=0; i<numCamp; i++ )
    {
      f = currcamp[i];
#ifdef DEBUG
      printf("Copy camp variable %d value %f to %d.\n", i,f,
          (numAllArr*ic+1+numScalArr+numMonArr+i) );
#endif
      tots[numAllArr*ic+1+numScalArr+numMonArr+i] = *((int*)&f); /* float not converted */
    }
  }
  return;
}

/*
 * Copy Epics values into the tots list, but putting in floats (disguised as ints)
 * This is only for the sweep runs (TI and TS, not TD).
 */

static void epics_values_copy( ic )
     DWORD ic;  /* index:  point number minus 1 */
{
  int i, j;
  float f;

  if ( itmult )
  { /* fill each time bin with the same value, for each epics variable */

    for( i=0; i<numEpics; i++ )
    {
      f = currepics[i];
#ifdef DEBUG
      printf("Copy epics variable %d value %f %d times.\n", i,f,numTimes);
#endif
      for( j=0; j<numTimes; j++ )
      {
        tots[numTimes*(numAllArr*ic+1+numScalArr+numMonArr+numCampArr+i)+j] = *((int*)&f); /* float not converted */
      }
    }
  }
  else
  { /* copy each Epics variable */
    for( i=0; i<numEpics; i++ )
    {
      f = currepics[i];
#ifdef DEBUG
      printf("Copy Epics variable %d value %f to %d.\n", i,f,numAllArr*ic+1+numScalArr+numMonArr+numCampArr+i );
#endif
      tots[numAllArr*ic+1+numScalArr+numMonArr+numCampArr+i] = *((int*)&f); /* float not converted */
    }
  }
  return;
}

/********************************************************************/
static int his_event_do ( plocal, icmd, nBinRanges, bin0, bin1, f0, f1 )
/********************************************************************\
  Routine: his_event_do
  Purpose: Read histogram events; set (global) scycle and point count.
           sum events for each histogram (="bank")).
  Input:   DWORD * plocal : pointer to event
           Cmd_t icmd       : type of processing
           int bin0, bin1     : bin range for summation
           int f0, f1     : frequency range used
  Output:  none
  Return:  status code (0 = success )
\********************************************************************/
     DWORD* plocal;
     Cmd_t  icmd;
     int    f0, f1;
     int    nBinRanges;
     int    bin0[MAX_BIN_RANGES];
     int    bin1[MAX_BIN_RANGES];
{
  INT     i, jj, n_items;
  DWORD  * pdata ;
  DWORD   thiscycle ;
  INT     ipt;
  unsigned char * p;
  unsigned char   u;
  int     b0x,b1x;
  float   f;
  int     jiv;
  BOOL    newCycle = FALSE;

  int mb; 
  int ibr;
  int phind, ibase;

  char ** HisBankNames;


  ipt = numPoint-1;

  if( !headYet ) 
  {
#ifdef DEBUG
    printf("Hist event ignored because no header yet\n");
    return( 0 );
#else
    return( 0 );  /* maybe ( 3 ) */
#endif
  }


  phind = (usePhas ? 2*phase : 0 );

  /* 
   *  Get the "CYCL" bank telling all the independent values for the sweeps
   */

  n_items = bk_locate((EVENT_HEADER *)plocal+1, "CYCL", &pdata);

  if (n_items > 6)
  {
    cycle     =  *pdata++;
    thiscycle =  *pdata++;
    scan      =  *pdata++;
    phase     =  *pdata++;
    scanflg   =  *pdata++;
    cellread  =  *pdata++;
    cellmv    =  *pdata++;
    frequency =  *pdata++;
    indepval  =  ( scanflg == 1 ? cellmv : frequency );

#ifdef DEBUG
    printf( "Hist event size %5d: %5d %5d %5d %5d %5d %9d %9d %11d %11d\n",
            n_items, cycle, thiscycle, scan, phase, scanflg, cellread, cellmv, frequency, indepval);
#endif
    if( thiscycle < scycle )
    {
      printf( "Bad events: scycle jumped from %d to %d!\n", scycle, thiscycle );
      return( 3 );
    }

    newCycle = ( thiscycle > scycle );
    if( newCycle ) 
    {
      if( icmd == Cmd_TI || icmd == Cmd_TS )
      { /*
         * new cycle for a TI run. If camp/epics values not yet copied for previous scycle 
         * then copy current values into scycle just ending
         */
        if( !cCopied )
        { 
          camp_values_copy( ipt );
          cCopied = TRUE;
        }
        if( !eCopied )
        { 
          epics_values_copy( ipt );
          eCopied = TRUE;
        }
      }
      /* 
       * Do increments for new cycle (TD runs overlay all cycles).  On the first
       * cycle, numAllArr may be incorrect, but it doesn't matter because ipt is 0.
       */
      numPoint++;
      ipt++;
    }
    /* 
     * Calculate base index into tots for this event.  numAllArr may be incorrect during
     * the first event, but it doesn't matter because ipt is 0.
     */
    ibase = ipt *  numTimes * numAllArr * icmult;
    if( icmd == Cmd_3D ) {
      /*
       * For multiplexed TD data, look to see if we have recorded this indep value 
       * already.
       */
      for( jiv = 0; jiv < ibase; jiv += numTimes * numAllArr ) {
        if( indepval == tots[jiv] ) {
          /*
           * It is a repeat independent value.  Use the old index.  Anti-increment numPoint.
           */
          numPoint--;
          ibase = jiv;
          break;
        }
      }
    }

    scycle = thiscycle;

  } /* end CYCL bank handling */

  /*
   * Look for, and read, user bits only once (null pointer indicates not read)
   * If no user bits bank in this first event, then declare all user bits zero.
   * If user bits provided from the command line, then maybe use them.
   */

  if( !ubits )
    {
      useBits = FALSE;
      n_items = bk_locate((EVENT_HEADER *)plocal+1, "UBIT", &pdata);
      
      if( n_items > 0 )
        {
#ifdef DEBUG
          printf( "Found user-bits for %d time-slices.\n", n_items );
#endif
          ubits = malloc( n_items*sizeof(int) );
          if( ubits == NULL ) return( 2 );

          if( UBcycFlag == UB_forced && numUBc > 0 )
            {
#ifdef DEBUG
              printf( "But forced to use user-provided values!\n");
#endif
              for( i=0; i<n_items; i++ ) {
                ubits[i] = (4 * UBcyc[i%numUBc]);
              }
            }
          else
            {
              for( i=0; i<n_items; i++ ) {
                ubits[i] = (4 * pdata[i]);
              }
            }
        }
      else     /* no user bits found in run file */
        {
          n_items = mid_hdr.his_nbin;

          ubits = malloc( n_items*sizeof(int) );
          if( ubits == NULL ) return( 2 );

          if( UBcycFlag != UB_none && numUBc > 0 )
            {
#ifdef DEBUG
              printf( "No user-bits in run, but provided by user now.\n" );
#endif
              for( i=0; i<n_items; i++ ) {
                ubits[i] = (4 * UBcyc[i%numUBc]);
              }
            }
          else /* none provided on command line */
            {
#ifdef DEBUG
              printf( "No user-bits so prepare %d zeros\n", n_items );
#endif
              bzero( ubits, n_items*sizeof(int) );
            }
        }

      /*
       * Now check if ubits were all the same (debug messages too)
       */
      
      useBits = FALSE;
      for( i=1; i<n_items; i++ )
        {
          if( ubits[i] !=  ubits[i-1] )
            {
              useBits = TRUE;
              break;
            }
        }

#ifdef DEBUG
      if( useBits ) printf( "Yes, User-bits:" );
      else printf( "No, User-bits:" );
      for( i=0; i<n_items; i++ )
        {
          if( i%50 == 0 ) printf( "\n" );
          printf( "%2d", ubits[i]/4 );
        }
      printf( "\n" );
#endif
      /*
       * Check if we can use the ubits (checked last so we can give an error message)
       */
      if( useBits && (icmd==Cmd_TD || icmd==Cmd_TS || icmd==Cmd_3D) ) 
        {
          printf("Warning: User bits were found but ignored.\n" );
          useBits = FALSE;
        }

      /*
       * If ubits are unwanted, all same, or not given, set them all to zero
       */
      if( ! useBits ) {
        bzero( ubits, mid_hdr.his_nbin*sizeof(int) );
      } 

      numHistArr = 2 * ( useBits ? 4 : 1 ) * ( usePhas ? 2 : 1 );
      numScalArr = numHistArr * nBinRanges ;
      numMonArr = 2 + ( usePhas ? 4 : 2 );
      numAllArr = 1 + numScalArr + numMonArr + numCampArr + numEpicsArr;

    } /* end-if !ubits -- end of ubits loading */

  if( indepval<f0 || indepval>f1 ) {
    //    printf  ("**** OUTSIDE FREQ RANGE ****  if( %d<%d || %d>%d ) return( 0 );   \n", indepval, f0, indepval, f1); 
    //  return(0);
  }

  cCopied = FALSE;

  /*
   *  Now read the histograms of the 4 types (split into 16 if user bits)
   */

  if( bNQRflag ) 
    HisBankNames = HisBanksBNQR;
  else
    HisBankNames = HisBanksBNMR;

  /*
   * Record independent value (typ. frequency)
   */

  for( i=0; i<numTimes; i++ ) {
    tots[ibase+i] = indepval;
  }

  /*
   *  Collect Histogram banks, split into user bits, maybe summing for each bin range
   */
  n_items = bk_locate((EVENT_HEADER *)plocal+1, HisBankNames[0], &pdata);
  if (n_items > 0)
  {
#ifdef DEBUG
    printf("%s with %d items, ",HisBankNames[0],n_items);
#endif
    for( ibr=0 ; ibr < nBinRanges ; ibr++ )
    {
      b1x = _min(bin1[ibr],n_items);
      for( i=bin0[ibr]-1 ; i<b1x ; i++ )
        tots[ibase + numTimes*(1+(ibr*numHistArr)+phind+ubits[i]) + (itmult*i)] += pdata[i];
    }
  }

  n_items = bk_locate((EVENT_HEADER *)plocal+1, HisBankNames[1], &pdata);
  if (n_items > 0)
  {
#ifdef DEBUG
    printf("%s with %d items, ",HisBankNames[1],n_items);
#endif
    for( ibr=0 ; ibr < nBinRanges ; ibr++ )
    {
      b1x = _min( bin1[ibr], n_items);
      for( i=bin0[ibr]-1 ; i<b1x ; i++ )
        tots[ibase + numTimes*(1+(ibr*numHistArr)+phind+ubits[i]+1) + (itmult*i)] += pdata[i];
    }
  }


  /* Collect Monitor Banks "HM00" - "HM01" (direct copy) */

  for( mb=0 ; mb < 2 ; mb++ )
  {
    n_items = bk_locate((EVENT_HEADER *)plocal+1, MonBankNames[mb], &pdata);
    if (n_items > 0)
    {
#ifdef DEBUG
      printf("%s with %d items, ",MonBankNames[mb],n_items);
#endif
      for( i=0; i<n_items; i++ )
        tots[ibase + numTimes*(1+numScalArr+mb) + (itmult*i)] += pdata[i];
    }
  }

  /* DO NOT Collect Monitor Banks "HM02" - "HM03" */

  /* Collect Monitor Banks "HM04" - "HM05" (split by phase) */

  for( mb=2 ; mb < 4 ; mb++ )
  {
    n_items = bk_locate((EVENT_HEADER *)plocal+1, MonBankNames[mb], &pdata);
    if (n_items > 0)
    {
#ifdef DEBUG
      printf("%s with %d items, ",MonBankNames[mb],n_items);
#endif
      for( i=0; i<n_items; i++ )
        tots[ibase + numTimes*(1+numScalArr+phind+mb) + (itmult*i)] += pdata[i];
    }
  }

#ifdef DEBUG
    printf("\n");
    printf("Top %d\n", 
           ibase + numTimes*(1+numScalArr+2+phind+mb) + itmult*(n_items-1));
#endif

  return( 0 );
}


/********************************************************************/
static int head_event_do ( plocal, icmd )
/********************************************************************\
  Routine: head_event_do
  Purpose: Read header event (darc and camp and epics banks)
  Input:   DWORD * plocal : pointer to event
  Output:  none
  Return:  status code (0 = success )
\********************************************************************/
     DWORD * plocal;
     Cmd_t     icmd; 
{
  INT     n_items, n_char;
  DWORD * pdata ;
  static  char  * pbuff = NULL;
  static  int     buffSize = 0;
  int     nitem;
  char  * p;
  char  * pn;
  int i;
  long l;
  unsigned long lp;

#ifdef DEBUG
  printf( "Header event (14) detected.\n" );
#endif

  /********************************  DARC  ******************************************/
  n_items = bk_locate((EVENT_HEADER *)plocal+1, "DARC", &pdata);
#ifdef DEBUG
  printf( "DARC bank size %d in header.\n",n_items);
#endif
  if (n_items > 0)
  {
    if( n_items > sizeof(mid_hdr) ) {
      printf( "DARC (head) event is too large (%d vs %d)!  Truncate and continue.\n",n_items,sizeof(mid_hdr));
      n_items = sizeof(mid_hdr);
    }

    memcpy( (char*) &mid_hdr, (char*) pdata, n_items );

    if ( n_items < sizeof(mid_hdr) )
    {
      i = upgrade_mid_hdr ( n_items, &mid_hdr );
      if ( i > 0 ) {
        printf("Revision %d DARC header, size %d, upgraded successfully.\n", i,n_items);
      } 
      else {
        fprintf( stderr, "ERROR: unrecognized run header size: %d.  Continuing...\n", n_items);
      }
    }
#ifdef DEBUG
    printf("i   bzero     begin     end       bkgd1     bkgd2 \n");
    for (i=0; i<14; i++) {
      printf( "%.2d  %.8d  %.8d  %.8d  %.8d  %.8d\n", i,
              mid_hdr.his_bzero[i], mid_hdr.his_good_begin[i], mid_hdr.his_good_end[i],
              mid_hdr.his_first_bkg[i], mid_hdr.his_last_bkg[i] );
    }
#endif

    /* 
     * don't copy header into mud header yet, because we may change our mind
     * about which file type after we read some data.
     */

    headYet = TRUE;
  }
  numTimes  = (icmd == Cmd_TI ? 1 : mid_hdr.his_nbin) ;
  numAllArr = 1 + numScalArr + numMonArr + numCampArr + numEpicsArr ;


  /********************************  CAMP  ******************************************/

  n_char = bk_locate((EVENT_HEADER *)plocal+1, "CAMP", &pdata);
#ifdef DEBUG
  printf( "CAMP bank size %d in header.\n",n_char);
#endif
  if (n_char > 0)
  {
    if ( numCamp > 0 &&  numCamp !=  n_char/sizeof(IMUSR_CAMP_VAR) )
      printf( "WARNING: Number of Logged Camp Variables changed from %d to %d.\n", 
              numCamp, n_char/sizeof(IMUSR_CAMP_VAR) );
    numCamp = n_char/sizeof(IMUSR_CAMP_VAR);
#ifdef DEBUG
    printf( "numCamp set to %d.\n",numCamp);
#endif
    if ( icmd == Cmd_TD || icmd == Cmd_3D ) 
    {/*
      *  Zero space for collecting Camp variable statistics, for
      *  Time-Differential data.
      */
      numCampArr = 0;
      bzero( (void*)campSums, numCamp * sizeof( CAMP_SUMS_t ) );
    }
    else
    {
      numCampArr = numCamp;
    }
    /* 
     * Copy camp variable strucure.  Note that structure contains fixed length 
     * strings, not just pointers, so memcpy does copy everything.
     */
    memcpy( campVars, (char*) pdata, n_char );

  }

  /********************************  EPICS  ******************************************/

  n_char = bk_locate((EVENT_HEADER *)plocal+1, "EPICS", &pdata);
#ifdef DEBUG
  printf( "EPICS bank size %d in header.\n",n_char);
#endif
  if (n_char > 0)
  {
    if ( numEpics > 0 &&  numEpics !=  n_char/sizeof(IMUSR_CAMP_VAR) )
      printf( "WARNING: Number of Logged Epics Variables changed from %d to %d.\n", 
              numEpics, n_char/sizeof(IMUSR_CAMP_VAR) );
    numEpics = n_char/sizeof(IMUSR_CAMP_VAR);
#ifdef DEBUG
    printf( "numEpics set to %d.\n",numEpics);
#endif
    if ( icmd == Cmd_TD || icmd == Cmd_3D ) 
    {/*
      *  Zero space for collecting Epics variable statistics, for
      *  Time-Differential data.
      */
      numEpicsArr = 0;
      bzero( (void*)epicsSums, numEpics * sizeof( CAMP_SUMS_t ) );
    }
    else
    {
      numEpicsArr = numEpics;
    }
    /* 
     * Copy epics variable strucure.  Note that structure contains fixed length 
     * strings, not just pointers, so memcpy does copy everything.
     */
    memcpy( epicsVars, (char*) pdata, n_char );

  }

  /*********************************  Done  *************************************/

  numAllArr = 1 + numScalArr + numMonArr + numCampArr + numEpicsArr;

  if( strchr(mid_hdr.area,'Q') || strchr(mid_hdr.area,'q') )
    bNQRflag = TRUE;
  else
    bNQRflag = FALSE;

  return( 0 );
}


/********************************************************************/
static int camp_event_do ( plocal, icmd )
/********************************************************************\
  Routine: camp_event_do
  Purpose: Read list of camp variable values.
  Input:   DWORD * plocal : pointer to event
           Cmd_t icmd     : Type of data file to produce
  Output:  none
  Return:  status code (0 = success)
\********************************************************************/
DWORD * plocal;
Cmd_t icmd;
{
  INT i,n_items;
  double *pdata;
  double val, oval;

  n_items = bk_locate((EVENT_HEADER *)plocal+1, "CVAR", &pdata);

#ifdef DEBUG
  printf( "Camp event CVAR bank has %d items (of %d).\nValues: ", n_items, numCamp );
  for (i=0; i<n_items; i++) printf( "%8.2f ",pdata[i]);
  printf("\n");
#endif

  if (n_items == 0) return( 0 );

  for (i=0; i<n_items; i++)
    currcamp[i] = pdata[i];

  if (n_items != numCamp) return( 6 );

  /*
   *  For TD data files, we won't be recording each individual Camp value.
   *  Instead, accumulate statistics as we go.
   */
  if ( icmd == Cmd_TD || icmd == Cmd_3D )
  {
    for (i=0; i<numCamp; i++)
    {
      val = currcamp[i];
      if( campSums[i].num == 0 ) 
      { /* initializa */
#ifdef DEBUG
        printf("Initialize summation\n");
#endif
        campSums[i].sum = campSums[i].sum2 = campSums[i].sum3 = 0.0;
        campSums[i].low = campSums[i].high = campSums[i].offset = val;
        campSums[i].num = 1;
      }
      else
      { /* accumulate */
#ifdef DEBUG
        printf("Accumulate %d-th value %f\n", campSums[i].num+1, val);
#endif
        campSums[i].low = _min( campSums[i].low, val );
        campSums[i].high = _max( campSums[i].high, val );
        oval = currcamp[i] - campSums[i].offset;
        campSums[i].sum  += oval;
        campSums[i].sum2 += oval*oval;
        campSums[i].sum3 += oval*oval*oval;
        campSums[i].num  ++;
      }
    }
  }
  return( 0 ); 
}

/********************************************************************/
static int epics_event_do ( plocal, icmd )
/********************************************************************\
  Routine: epics_event_do
  Purpose: Read list of epics variable values.
  Input:   DWORD * plocal : pointer to event
           Cmd_t icmd     : Type of data file to produce
  Output:  none
  Return:  status code (0 = success)
\********************************************************************/
DWORD * plocal;
Cmd_t icmd;
{
  INT i,n_items;
  double *pdata;
  double val, oval;

  n_items = bk_locate((EVENT_HEADER *)plocal+1, "EVAR", &pdata);

#ifdef DEBUG
  printf( "Epics event EVAR bank has %d items (of %d).\nValues: ", n_items, numEpics );
  for (i=0; i<n_items; i++) printf( "%8.2f ",pdata[i]);
  printf("\n");
#endif

  if (n_items == 0) return( 0 );

  for (i=0; i<n_items; i++)
    currepics[i] = pdata[i];

  if (n_items != numEpics) return( 6 );

  /*
   *  For TD data files, we won't be recording each individual value.
   *  Instead, accumulate statistics as we go.
   */
  if ( icmd == Cmd_TD || icmd == Cmd_3D )
  {
    for (i=0; i<numEpics; i++)
    {
      val = currepics[i];
      if( epicsSums[i].num == 0 ) 
      { /* initializa */
#ifdef DEBUG
        printf("Initialize summation\n");
#endif
        epicsSums[i].sum = epicsSums[i].sum2 = epicsSums[i].sum3 = 0.0;
        epicsSums[i].low = epicsSums[i].high = epicsSums[i].offset = val;
        epicsSums[i].num = 1;
      }
      else
      { /* accumulate */
#ifdef DEBUG
        printf("Accumulate %d-th value %f\n", epicsSums[i].num+1, val);
#endif
        epicsSums[i].low = _min( epicsSums[i].low, val );
        epicsSums[i].high = _max( epicsSums[i].high, val );
        oval = currepics[i] - epicsSums[i].offset;
        epicsSums[i].sum  += oval;
        epicsSums[i].sum2 += oval*oval;
        epicsSums[i].sum3 += oval*oval*oval;
        epicsSums[i].num  ++;
      }
    }
  }
  return( 0 ); 
}


/********************************************************************/
static int scal_event_do( plocal )
/********************************************************************\
  Routine: scal_event_sum
  Purpose: Read scaler events.  Pull out the histogram totals
           Do we have any meaningful scalers here?
  Input:   char * plocal : pointer to event
  Output:  none
  Return:  void
\********************************************************************/
DWORD * plocal;
{
  double   tot[16];

  /*
   * I'm not sure what the numbers in the scaler bank mean, so
   * ignore them, and just report what the header says (probably
   * bad; fix later).
   */

#ifdef Not_Used

  n_items = bk_locate((EVENT_HEADER *)plocal+1, "HSCL", &pdata);
  if ( (n_items > 38) && (numPoint > 0) )
  {/* How do I determine this offset from the run `headers' ??? */
  }

#endif

  return( 0 );
}

/* 
 * M2Malloc:   checks and performs allocation of the main working data array 
 *               by "realloc", and initializes the extension to zeros.
 *    Returns:   status 0=success, 2=failure
 *               new pparray pointer
 */
static int
M2Malloc( unsigned int** pparray, int ncol, int nrow )
{
  size_t new, req;
  static size_t size = 0;

  req = nrow * ncol * sizeof(int);

#ifdef DEBUG
  /*
    printf( "Require tots array of size %d to hold %d by %d\n", req, ncol, nrow);
    printf( "Current size is %d, at address %x=%ld.\n", size, *pparray, (unsigned long)*pparray);
    fflush( stdout );
  */
#endif

  if( req <= size ) return( 0 );

  new = (req + 128*ncol)*2;

#ifdef DEBUG
  printf( "Try to expand main array from %d to %d bytes.\n", size, new );
  fflush( stdout );
#endif

  *pparray = (unsigned int*) realloc( (void*)*pparray, new );

  if ( *pparray == NULL ) {
    printf( "Array allocation FAILED!!!\n" );
    return( 2 );
  }
#ifdef DEBUG
  printf( "Now tots array is at %x=%ld, size %d, top=%ld, dimen=%d\n", 
          *pparray, (unsigned long)*pparray, new, (unsigned long)*pparray+new-4, new/4 );
  fflush( stdout );
#endif

#ifdef DEBUG
  /*  printf ( "Now zero out %d new bytes starting at %x=%ld\n", 
           new - size, (void*)((unsigned long)(*pparray)+size)
           , ((unsigned long)(*pparray)+size));
      fflush( stdout ); */
#endif
  bzero( (void*)((unsigned long)(*pparray)+size), new - size );

  size = new;
  return( 0 );
}



/*****************************************************************************/

/*
 *  BNMRmid_TI_write  -- write out the mud-TI file for a bnmr run.
 *		We have the header information in the midas darc-event
 *              structure, and the data, including camp, in a big array.
 */

static int M2MI_runDesc( MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc, int nBinRanges, int bin0[], int bin1[], char btag[][8] );
static int M2MI_hists( MUD_SEC_GRP* pMUD_histGrp, MUD_SEC_GRP* pMUD_indVarGrp,
                       int nBinRanges, int bin0[], int bin1[], char btag[][8]);

/*
 *  Declared in mud_util.h:
 *  char* trimBlanks( char* inStr, char* outStr );
 *  char* skipBlanks( char* ptr );
 *  char* strndup( char *str, int max_len );
 */

#define M2M_hdr_str(to,from,len) \
    strncpy( tempString, from, len ); \
    tempString[len] = '\0'; \
    trimBlanks( tempString, tempString ); \
    to = strdup( tempString )

int
BNMRmid_TI_write( Cmd_t icmd, char outfile[], int nBinRanges, int bin0[], int bin1[], char btag[][8], int f0, int f1 )
{
    FILE* fout;
    MUD_SEC_GRP* pMUD_fileGrp = NULL;
    MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc = NULL;
    MUD_SEC_GRP* pMUD_histGrp = NULL;
    MUD_SEC_GRP* pMUD_indVarGrp = NULL;

#ifdef DEBUG
    printf("Doing BNMRmid_TI_write\n");
#endif
    pMUD_fileGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TI_ID );

    /*
     *  Convert the run description
     */
    pMUD_desc = (MUD_SEC_TRI_TI_RUN_DESC*)MUD_new( MUD_SEC_TRI_TI_RUN_DESC_ID, 1 );

    M2MI_runDesc( pMUD_desc, nBinRanges, bin0, bin1, btag );

    /*
     *  Convert the histograms and independent variables
     */
    pMUD_histGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TI_HIST_ID );
    if( numCampArr+numEpicsArr > 0 )
      pMUD_indVarGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ARR_ID );
    else
      pMUD_indVarGrp = NULL;

    M2MI_hists( pMUD_histGrp, pMUD_indVarGrp, nBinRanges, bin0, bin1, btag );

    /*
     *  Assemble the first level sections
     */
    MUD_addToGroup( pMUD_fileGrp, pMUD_desc );
    MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );
    if( numCampArr+numEpicsArr > 0 )
      MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrp );

    /*
     *  Do the write
     */
 
    fout = MUD_openOutput( outfile );
    if( fout == NULL ) return( 7 );

    MUD_writeFile( fout, pMUD_fileGrp );

    fclose( fout );

    /*
     *  Free malloc'ed mem
     */
    MUD_free( pMUD_fileGrp );

    return( 0 );
}


static int
M2MI_runDesc( pMUD_desc, nBinRanges, bin0, bin1, btag )
     MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc;
     int nBinRanges;
     int bin0[], bin1[];
     char btag[][8];
{
    char tempString[256];
    int  i;

    pMUD_desc->runNumber = mid_hdr.run_number;

    pMUD_desc->exptNumber = mid_hdr.experiment_number;

    M2M_hdr_str(pMUD_desc->experimenter,mid_hdr.experimenter,32);

    pMUD_desc->timeBegin = mid_hdr.start_time_binary;
    pMUD_desc->timeEnd = mid_hdr.stop_time_binary;
    pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
    if( pMUD_desc->elapsedSec < 0 || pMUD_desc->elapsedSec > 9000000)
         pMUD_desc->elapsedSec = 0;

    M2M_hdr_str(pMUD_desc->title,mid_hdr.run_title, 127);

    pMUD_desc->lab = strdup( "TRIUMF" );
    M2M_hdr_str(pMUD_desc->area,mid_hdr.area, 32);
    pMUD_desc->method = strdup( "TI-bNMR" );
    pMUD_desc->das = strdup( "MIDAS" );

    M2M_hdr_str(pMUD_desc->apparatus,mid_hdr.rig, 32);
    M2M_hdr_str(pMUD_desc->insert,mid_hdr.mode, 32);

    pMUD_desc->subtitle = strdup( "none" );
    if( nBinRanges > 1 || bin0[0] > 1 || bin1[0] < mid_hdr.his_nbin )
    {
        strcpy( tempString, "Using time bins ");
        for( i=0 ; i<nBinRanges ; i++ )
        {
            sprintf( &tempString[strlen(tempString)], "%d to %d (%s), ", bin0[i], bin1[i], btag[i] );
        }
        tempString[strlen(tempString)-2] = '\0'; /* erase final ", " */
        pMUD_desc->comment1 = strdup( tempString );
    }
    else
    {
        pMUD_desc->comment1 = strdup( "no comment" );
    }

    pMUD_desc->comment2 = strdup( "no comment" );
    pMUD_desc->comment3 = strdup( "no comment" );

    M2M_hdr_str(pMUD_desc->sample,mid_hdr.sample, 15);
    M2M_hdr_str(pMUD_desc->orient,mid_hdr.orientation, 15);

    return( 1 );
}


/*
 *  Get array names.
 */
/********************************************************************/
static char* getHistName( i, btag )
/********************************************************************\
  Routine: getHistName
  Purpose: Generate names for scaler arrays
  Input:   int i : mapping number.  
           char btag[MAX_BIN_RANGES][8]:  list of bin-range identifiers
           Also use global variables:
                   scanflg, numScalArr, numHistArr, useBits, usePhas, bNQRflag
  Output:  none
  Return:  pointer to name string (or null)
\********************************************************************/
    int i;
    char btag[][8];
{
  static char* itypes[] = { "Frequency", "Na Cell mV", "Laser", "Magnet mA", "Dummy",
                            "Frequency", "Field G", "Field G" };
  static int  ivalues[] = { 0, 1, 2, 3, 4, 256, 512, 1024 };
  static char* NMR_hists[2] = { "B","F" };
  static char* NQR_hists[2] = { "L","R" };
  static char* ptypes[] = { "+","-" };
  static char* utypes[] = { "_frq0","_frq180","_ref0","_ref180" };
  static char* mtypes0[] = { "Const","FluM2" };
  static char* mtypes2[] = {  "NBMB","NBMF" };
  static char name[32]; /* 10 + 12 + 8 < 32 */ 

  char** stypes;

  int len, j, nbr;

#ifdef DEBUG
  printf( "Get histogram name for number %d\n",i);
  printf( "numScalArr: %d, numHistArr: %d, numMonArr: %d, numAllArr: %d\n",
          numScalArr, numHistArr, numMonArr, numAllArr);
#endif

  if( bNQRflag ) 
    stypes = NQR_hists;
  else 
    stypes = NMR_hists;


  /* 0: independent variable */
  if( i == 0 ) 
  {
      for( j=0; j<7; j++)
      {
          if ( ivalues[j] == scanflg ) break;
      }
#ifdef DEBUG
      printf("Independent value name for flag %d index %d is %s\n",
             scanflg, j, itypes[j] );
#endif
      return itypes[j];
  }

  /* 1 to numScalArr: scaler, perhaps with phase and user bits */
  if( i > 0 && i <= numScalArr ) 
  {
    /* Start name with tag for time-range */
    _strncpy( name, btag[(i-1)/numHistArr], 7 );
    j = (i-1)%numHistArr;

    strncat( name, stypes[j%2], 5); /* Counter (F/B) */
    j /= 2;

    if( usePhas )
    {
      strncat( name, ptypes[j%2], 5); /* Helicity phase (+/-) */ 
      j /= 2;
    }

    if( useBits )
    { 
      strncat( name, utypes[j], 12);
    }
#ifdef DEBUG
      printf("Got name %s\n", name);
#endif
 
    return( name );
  }

  /* > numScalArr: monitor count / auxilliary scaler */

  j = i - (numScalArr + 1) ;

#ifdef DEBUG
  printf( "Index into monitor arrays is %d\n", j);
#endif

  if( j < 0 ) printf( "PROGRAM ERROR\n" );
  if( j < 0 ) return( NULL );

  if( j < 2 ) return( mtypes0[j] );
  j -= 2;

  if( j < (usePhas ? 4 : 2) )
  {
    _strncpy( name, mtypes2[j%2], 7 );
  }
  else
  {
    printf( "PROGRAM ERROR - PAST MONITOR RANGE\n" );
    return( NULL );
  }
  j /= 2;

  if( usePhas )
    strncat( name, ptypes[j%2], 5); /* Helicity phase (+/-) */ 

#ifdef DEBUG
      printf("Got name %s\n", name);
#endif
 
  return( name );
}


static int
M2MI_hists( pMUD_histGrp, pMUD_indVarGrp, nBinRanges, bin0, bin1, btag )
    MUD_SEC_GRP* pMUD_histGrp;
    MUD_SEC_GRP* pMUD_indVarGrp;
    int nBinRanges;
    int bin0[], bin1[] ;
    char btag[][8];
{
    int h, b, d, p, i, j;
    MUD_SEC_GEN_HIST_HDR* pMUD_histHdr;
    MUD_SEC_GEN_HIST_DAT* pMUD_histDat;
    MUD_SEC_GEN_IND_VAR* pMUD_indVarHdr;
    MUD_SEC_GEN_ARRAY* pMUD_indVarDat;
    int nScalers;
    int indVarOffset;
    int npoints;
    double val, sum, sum2, sum3, mean, stddev;
    int num;

    nScalers = 1 + numScalArr + numMonArr ; /* including DAC/FRQ */
    npoints = numPoint * numTimes;

    for( h = 0; h < nScalers; h++ ) /* loop over scalers */
    {

	pMUD_histHdr = (MUD_SEC_GEN_HIST_HDR*)MUD_new( MUD_SEC_GEN_HIST_HDR_ID, h+1 );
	pMUD_histDat = (MUD_SEC_GEN_HIST_DAT*)MUD_new( MUD_SEC_GEN_HIST_DAT_ID, h+1 );

 	/*
 	 *  Allocate space for the array (4 bytes/bin, no packing)
	 */
	pMUD_histDat->pData = (caddr_t)zalloc( 4*npoints );

	for( d = 0; d < numPoint; d++ ) /* loop over cycles */
	{
            /*  
             *  Copy a block of time-bins (usually just 1 number for TI)
             */
            bcopy( &tots[(d*numAllArr+h)*numTimes], &((UINT32*)pMUD_histDat->pData)[d*numTimes], 4*numTimes );
	}

        pMUD_histHdr->histType = MUD_SEC_TRI_TI_HIST_ID;
        pMUD_histHdr->nBins = npoints;
        pMUD_histHdr->nEvents = npoints;
        pMUD_histHdr->bytesPerBin = 4;
        pMUD_histHdr->nBytes = pMUD_histHdr->bytesPerBin * pMUD_histHdr->nBins;
        pMUD_histHdr->title = strdup( getHistName( h, btag ) );

        /* Actually record ns/bin for bNMR runs, although name claims fs/bin; dwell time is in ms  */
        pMUD_histHdr->fsPerBin = (int)( mid_hdr.dwell_time * 1000000.0 + 0.49 );

        /* For F/B histograms, record time-bin range used */
        if( h > 0 && h <= numScalArr )
        {
            pMUD_histHdr->goodBin1 = bin0[(h-1)/numHistArr];
            pMUD_histHdr->goodBin2 = bin1[(h-1)/numHistArr];
        }

        pMUD_histDat->nBytes = pMUD_histHdr->nBytes;

	MUD_addToGroup( pMUD_histGrp, pMUD_histHdr );
	MUD_addToGroup( pMUD_histGrp, pMUD_histDat );
    }

    /*
     *  Now do the independent variables
     */
    indVarOffset = 1 + numScalArr + numMonArr;

    for( i = 0; i < numCampArr+numEpicsArr; i++ )
    {
    	h = i + indVarOffset;

	pMUD_indVarHdr = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, i+1 );
	pMUD_indVarDat = (MUD_SEC_GEN_ARRAY*)MUD_new( MUD_SEC_GEN_ARRAY_ID, i+1 );

 	/*
         *  Copy a block of Camp or Epics values (usually just 1 float)
 	 *  Allocate space for the array (4 bytes/bin, no packing)
	 */
	pMUD_indVarDat->pData = (caddr_t)zalloc( 4*npoints );

	for( d = 0; d < numPoint; d++ )
	{
            /*
             *  No need to decode/encode because MUD_writeFile encodes everything.
             *  In fact, if we encoded here, we would get garbage due to double-encoding.
             */
            bcopy( &tots[(d*numAllArr+h)*numTimes], &((UINT32*)pMUD_indVarDat->pData)[d*numTimes], 4*numTimes );
	}

	pMUD_indVarDat->num = npoints;
	pMUD_indVarDat->elemSize = 4;   /* 4 bytes/bin */
	pMUD_indVarDat->type = 2;       /* 2=real */
	pMUD_indVarDat->hasTime = 0;    /* no time data */
	pMUD_indVarDat->pTime = NULL;
        pMUD_indVarDat->nBytes = pMUD_indVarDat->num*pMUD_indVarDat->elemSize;


        if ( i < numCampArr )
        {
            pMUD_indVarHdr->name = strndup( campVars[i].path, campVars[i].len_path );
            pMUD_indVarHdr->description = strndup( campVars[i].title, campVars[i].len_title );
            pMUD_indVarHdr->units = strndup( campVars[i].units, campVars[i].len_units );
        }
        else
        {
            pMUD_indVarHdr->name = strndup( epicsVars[i-numCampArr].path, epicsVars[i-numCampArr].len_path );
            pMUD_indVarHdr->description = strndup( epicsVars[i-numCampArr].title, epicsVars[i-numCampArr].len_title );
            pMUD_indVarHdr->units = strndup( epicsVars[i-numCampArr].units, epicsVars[i-numCampArr].len_units );
        }


	/*
         *  Calculate statistics
 	 */
	num = pMUD_indVarDat->num;
	if( num > 0 )
        {
	  sum = sum2 = sum3 = 0.0;
	  pMUD_indVarHdr->low = pMUD_indVarHdr->high = 
                                    ((REAL32*)pMUD_indVarDat->pData)[0];
	  for( j = 0; j < num; j++ )
	  {
	    val = (double)((REAL32*)pMUD_indVarDat->pData)[j];
	    pMUD_indVarHdr->low = _min( pMUD_indVarHdr->low, val );
	    pMUD_indVarHdr->high = _max( pMUD_indVarHdr->high, val );
	    sum += val;
	    sum2 += val*val;
            sum3 += val*val*val;
	  }
	  mean = pMUD_indVarHdr->mean = sum/num;
	  stddev = pMUD_indVarHdr->stddev = ( num == 1 ) ? 0.0 :
                     sqrt( fabs( ( sum2 - sum*sum/num )/( num - 1 ) ) );
	  pMUD_indVarHdr->skewness = ( stddev == 0.0 ) ? 0.0 :
              ( sum3 - 3.0*mean*sum2 + 
                 2.0*( sum*sum*sum )/( ((double)num)*((double)num) ) ) /
              ( ((double)num)*(stddev*stddev*stddev) );

        }

	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarHdr );
	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVarDat );
    }

    return( 1 );
}

/*****************************************************************************/

/*
 *  BNMRmid_TD_write  -- write out the mud-TD file for a bnmr run.
 *		We have the header information in the midas darc-event
 *              structure, and the data, including camp, in a big array.
 */

static int M2MD_runDesc( MUD_SEC_GEN_RUN_DESC* pMUD_desc, int f0, int f1 );
static int M2MD_scalers( MUD_SEC_GRP* pMUD_scalGrp );
static int M2MD_hists( MUD_SEC_GRP* pMUD_histGrp, Cmd_t icmd, char btag[][8], int f0, int f1 );
static int M2MD_indvars( MUD_SEC_GRP* pMUD_indVarGrp );

int
BNMRmid_TD_write( Cmd_t icmd, char outfile[], char btag[][8], int f0, int f1 )
{
    FILE* fout;
    MUD_SEC_GRP* pMUD_fileGrp = NULL;
    MUD_SEC_GEN_RUN_DESC* pMUD_desc = NULL;
    MUD_SEC_GRP* pMUD_histGrp = NULL;
    MUD_SEC_GRP* pMUD_scalGrp = NULL;
    MUD_SEC_GRP* pMUD_indVarGrp = NULL;

#ifdef DEBUG
    printf("Doing BNMRmid_TD_write\n");
#endif
    pMUD_fileGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TD_ID );

    /*
     *  Convert the run description
     */
    pMUD_desc = (MUD_SEC_GEN_RUN_DESC*)MUD_new( MUD_SEC_GEN_RUN_DESC_ID, 1 );

    M2MD_runDesc( pMUD_desc, f0, f1 );

    /*
     *  Convert the scalers
     */
    pMUD_scalGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_SCALER_ID );

    M2MD_scalers( pMUD_scalGrp );

    /*
     *  Convert the histograms
     */
    pMUD_histGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_HIST_ID );

    M2MD_hists( pMUD_histGrp, icmd, btag, f0, f1 );

    /*
     *  Convert the independent variables (also some "PPG" parameters)
     */
    if ( numCamp+numEpics > 0 )
    {
        pMUD_indVarGrp = (MUD_SEC_GRP*)MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ID );
        M2MD_indvars( pMUD_indVarGrp );
    }

    /*
     *  Assemble the first level sections
     */
    MUD_addToGroup( pMUD_fileGrp, pMUD_desc ); 
    MUD_addToGroup( pMUD_fileGrp, pMUD_scalGrp );
    MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );
    if ( numCamp+numEpics > 0 )
      MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrp );

    /*
     *  Do the write
     */ 
    fout = MUD_openOutput( outfile );
    if( fout == NULL ) return( 7 );

    MUD_writeFile( fout, pMUD_fileGrp );

    fclose( fout );

    /*
     *  Free malloc'ed mem
     */
    MUD_free( pMUD_fileGrp );

    return( 0 );
}


static int
M2MD_runDesc( pMUD_desc, f0, f1 )
     MUD_SEC_GEN_RUN_DESC* pMUD_desc;
     int f0, f1;
{
    char tempString[256];
    int  i;

    pMUD_desc->runNumber = mid_hdr.run_number;

    pMUD_desc->exptNumber = mid_hdr.experiment_number;

    M2M_hdr_str(pMUD_desc->experimenter,mid_hdr.experimenter,32);

    pMUD_desc->timeBegin = mid_hdr.start_time_binary;
    pMUD_desc->timeEnd = mid_hdr.stop_time_binary;
    pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
    if( pMUD_desc->elapsedSec < 0 || pMUD_desc->elapsedSec > 9000000)
         pMUD_desc->elapsedSec = 0;

    M2M_hdr_str(pMUD_desc->title,mid_hdr.run_title, 127);

    pMUD_desc->lab = strdup( "TRIUMF" );
    M2M_hdr_str(pMUD_desc->area,mid_hdr.area, 32);
    pMUD_desc->method = strdup( "TD-bNMR" );
    pMUD_desc->das = strdup( "MIDAS" );

    M2M_hdr_str(pMUD_desc->apparatus,mid_hdr.rig, 32);
    M2M_hdr_str(pMUD_desc->insert,mid_hdr.mode, 32);

    M2M_hdr_str(pMUD_desc->sample,mid_hdr.sample, 48);
    M2M_hdr_str(pMUD_desc->orient,mid_hdr.orientation, 15);
    M2M_hdr_str(pMUD_desc->field,mid_hdr.field, 31);
    M2M_hdr_str(pMUD_desc->temperature,mid_hdr.temperature, 31);

    return( 1 );
}



static int
M2MD_scalers( pMUD_scalGrp )
    MUD_SEC_GRP* pMUD_scalGrp;
{
    int status;
    char tempString[256];
    int i;
    MUD_SEC_GEN_SCALER* pMUD_scal;

    for( i = 0; i < mid_hdr.nscal; i++ )
    {
        pMUD_scal = (MUD_SEC_GEN_SCALER*)MUD_new( MUD_SEC_GEN_SCALER_ID, i+1 );

        /*
         * Copy scaler values from header info, for now.
         */
        pMUD_scal->counts[0] = mid_hdr.scaler_save[i];
        _strncpy( tempString, mid_hdr.scaler_titles[i], 31 );
        trimBlanks( tempString, tempString );
        pMUD_scal->label = strdup( tempString );

        MUD_addToGroup( pMUD_scalGrp, pMUD_scal );
    }

    return( 1 );
}

static int
M2MD_hists( pMUD_histGrp, icmd, btag, f0, f1 )
    MUD_SEC_GRP* pMUD_histGrp;
    Cmd_t icmd;
    char btag[][8];
    int f0, f1;
{
    int h, b, d, p, i, j;
    int nbins, totev;
    int nh = 0;
    MUD_SEC_GEN_HIST_HDR* pMUD_histHdr;
    MUD_SEC_GEN_HIST_DAT* pMUD_histDat;
    int nScalers;
    int indVarOffset;
    int npoints;
    int ibase;
    char tmphisttitle[64];

    nScalers = numScalArr + numMonArr ;
    nbins = numTimes;
    if( icmd == Cmd_3D ) npoints = numPoint;
    else npoints = 1;

    for( i = 0; i < npoints; i++ ) 
    {
        ibase = i*(nScalers+1)*numTimes;
        indepval = ((int*)tots)[ibase];

        for( h = 1; h <= nScalers; h++ ) /* loop over scalers (skip frequency at 0) */
        {

            nh++;
            pMUD_histHdr = (MUD_SEC_GEN_HIST_HDR*)MUD_new( MUD_SEC_GEN_HIST_HDR_ID, nh );
            pMUD_histDat = (MUD_SEC_GEN_HIST_DAT*)MUD_new( MUD_SEC_GEN_HIST_DAT_ID, nh );

            /*
             *  Allocate space for the array (4 bytes/bin, no packing)
             */
            pMUD_histDat->pData = (caddr_t)zalloc( 4*nbins + 16 );
            
            totev = 0;
            for( d = 0; d < nbins; d++)
            {
              totev = totev + tots[ibase+h*numTimes+d];
            }
            pMUD_histHdr->histType = MUD_SEC_TRI_TD_HIST_ID;
            pMUD_histHdr->nBins = nbins;
            pMUD_histHdr->nEvents = totev;
            pMUD_histHdr->bytesPerBin = 0;
            /*
             *  Copy a block of time-bins
             */
            bcopy( &tots[ibase+h*numTimes], &((UINT32*)pMUD_histDat->pData)[0], 4*numTimes );

            pMUD_histHdr->nBytes =  MUD_SEC_GEN_HIST_pack( pMUD_histHdr->nBins,
		    4, &tots[ibase+h*numTimes],
		    pMUD_histHdr->bytesPerBin, pMUD_histDat->pData );
            pMUD_histDat->nBytes = pMUD_histHdr->nBytes;

            if( icmd == Cmd_3D ) 
            {
                printf( "Hist title: %s@%d", getHistName( h, btag ), indepval );
                sprintf( tmphisttitle, "%s@%d", getHistName( h, btag ), indepval );
                pMUD_histHdr->title = strdup( tmphisttitle );
            }
            else
            {
                pMUD_histHdr->title = strdup( getHistName( h, btag ) );
            }

            /* Actually record ns/bin for bNMR runs, although name claims fs/bin; dwell time is in ms  */
            pMUD_histHdr->fsPerBin = (int)( mid_hdr.dwell_time * 1000000.0 + 0.49 );

            /* record time-bin range used */
            pMUD_histHdr->t0_bin = mid_hdr.his_bzero[h-1];
            pMUD_histHdr->goodBin1 = mid_hdr.his_good_begin[h-1];
            pMUD_histHdr->goodBin2 = mid_hdr.his_good_end[h-1];
            pMUD_histHdr->t0_ps = 0.001*(pMUD_histHdr->fsPerBin)*( (double)pMUD_histHdr->t0_bin - 0.5 );
            pMUD_histHdr->bkgd1 = mid_hdr.his_first_bkg[h-1];
            pMUD_histHdr->bkgd2 = mid_hdr.his_last_bkg[h-1];

            MUD_addToGroup( pMUD_histGrp, pMUD_histHdr );
            MUD_addToGroup( pMUD_histGrp, pMUD_histDat );
        }
    }

    return( 1 );
}


static int M2MD_indvars( pMUD_indVarGrp )
        MUD_SEC_GRP* pMUD_indVarGrp;
{
    double val, sum, sum2, sum3, mean, stddev, offs;
    int num;
    int i,j;
    MUD_SEC_GEN_IND_VAR* pMUD_indVar;

    for( i = 0; i < numCamp+numEpics; i++ )
    {
        pMUD_indVar = (MUD_SEC_GEN_IND_VAR*)MUD_new( MUD_SEC_GEN_IND_VAR_ID, i+1 );
        if ( i < numCamp )
        {
            pMUD_indVar->name = strndup( campVars[i].path, campVars[i].len_path );
            pMUD_indVar->description = strndup( campVars[i].title, campVars[i].len_title );
            pMUD_indVar->units = strndup( campVars[i].units, campVars[i].len_units );
            sum  = campSums[i].sum;
            sum2 = campSums[i].sum2;
            sum3 = campSums[i].sum3;
            num  = campSums[i].num;
            offs = campSums[i].offset;
        }
        else
        {
            j = i - numCamp;
            pMUD_indVar->name = strndup( epicsVars[j].path, epicsVars[j].len_path );
            pMUD_indVar->description = strndup( epicsVars[j].title, epicsVars[j].len_title );
            pMUD_indVar->units = strndup( epicsVars[j].units, epicsVars[j].len_units );
            sum  = epicsSums[j].sum;
            sum2 = epicsSums[j].sum2;
            sum3 = epicsSums[j].sum3;
            num  = epicsSums[j].num;
            offs = epicsSums[j].offset;
        }
        /*
         *  Calculate statistics
         */
        if( num > 0 )
        {
            mean = pMUD_indVar->mean = offs + sum/num;
            stddev = pMUD_indVar->stddev = ( num == 1 ) ? 0.0 :
                     sqrt( fabs( ( sum2 - sum*sum/num )/( num - 1 ) ) );
            pMUD_indVar->skewness = ( stddev == 0.0 ) ? 0.0 :
                          ( sum3 - 3.0*mean*sum2 + 
                            2.0*( sum*sum*sum )/( ((double)num)*((double)num) ) ) /
                          ( ((double)num)*(stddev*stddev*stddev) );
        }

        MUD_addToGroup( pMUD_indVarGrp, pMUD_indVar );
    }
    return( 1 );
}

/* 
 *  skipBlanks() - return a pointer to the first non-blank in the string
 */
char*
skipBlanks( ptr )
    char* ptr;
{
    while( ( *ptr != '\0' ) && !isgraph( *ptr ) ) 
    {
    	ptr++;
    }
    return( ptr );
}


/* 
 *  trimBlanks() - return a string with no leading or trailing blanks
 */
char*
trimBlanks( inStr, outStr )
    char* inStr;
    char* outStr;
{
    char* p1;
    char* p2;
    int len;

    p1 = skipBlanks( inStr );
    if( *p1 == '\0' )
    {
    	outStr[0] = '\0';
    	return( outStr );
    }

    for( p2 = &inStr[strlen(inStr)-1]; ( p2 > p1 ) && !isgraph( *p2 ); p2-- );
    len = p2 - p1 + 1;
    strncpy( outStr, p1, len );
    outStr[len] = '\0';

    return( outStr );
}

