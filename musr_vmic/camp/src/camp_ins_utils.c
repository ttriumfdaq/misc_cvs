/*
 *  Name:       camp_ins_utils.c
 *
 *  Purpose:    Provide routines for management of instrument structures
 *              and access to instrument specific parameters.
 *              Also provide FORTRAN entry points to the routines that access
 *              instrument information.
 *
 *              Note that all routines except camp_insDel are not used by
 *              the CAMP server, CUI or CMD, but are provided in the library
 *              for client applications.
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include "camp.h"

#ifndef NO_FORTRAN

#include "cfortran.h"
#undef  fcallsc
#define fcallsc( UN, LN )   preface_fcallsc( F_, f_, UN, LN )

FCALLSCFUN2(LONG,camp_insGetLock,CAMP_INSGETLOCK,camp_insgetlock,STRING,PLOGICAL)
FCALLSCFUN2(LONG,camp_insGetLine,CAMP_INSGETLINE,camp_insgetline,STRING,PLOGICAL)
FCALLSCFUN2(LONG,camp_insGetFile,CAMP_INSGETFILE,camp_insgetfile,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_insGetTypeIdent,CAMP_INSGETTYPEIDENT,camp_insgettypeident,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_insGetIfTypeIdent,CAMP_INSGETIFTYPEIDENT,camp_insgetiftypeident,STRING,PSTRING)

#endif


/*
 *  Name:       camp_insDel
 *
 *  Purpose:    Delete an instrument from the variable list.
 *
 *  Called by:  campsrv_insdel_13 (camp_srv_proc.c)
 * 
 *  Inputs:     CAMP path of the instrument
 *
 *  Outputs:    CAMP status
 *
 */
int
camp_insDel( char* path )
{
    CAMP_VAR** ppVar;
    CAMP_VAR* pVar;

    /*
     *  Get address of pointer to instrument data
     */
    ppVar = camp_varGetpp( path );
    if( ppVar == NULL ) return( CAMP_INVAL_INS );

    pVar = *ppVar;
    *ppVar = (*ppVar)->pNext;
    pVar->pNext = NULL;
    set_current_xdr_flag( CAMP_XDR_ALL );
    _xdr_free( xdr_CAMP_VAR, pVar );

    return( CAMP_SUCCESS );
}


/*
 *  camp_insDelAll --
 *
 *        Delete the variable list.
 *
 *  NOT USED
 */
void
camp_insDelAll( void )
{
    _xdr_free( xdr_CAMP_VAR, pVarList );
}


/*
 *  NOT USED
 */
int
camp_insGetLock( char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
    {
        return( CAMP_INVAL_INS );
    }

    *pFlag = ( pVar->core.status & CAMP_INS_ATTR_LOCKED ) ? TRUE : FALSE;

    return( CAMP_SUCCESS );
}


/*
 *  NOT USED
 */
int
camp_insGetLine( char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
    {
        return( CAMP_INVAL_INS );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL ) 
    {
        *pFlag = FALSE;
    }
    else 
    {
        *pFlag = ( pIns->pIF->status & CAMP_IF_ONLINE ) ? TRUE : FALSE;
    }

    return( CAMP_SUCCESS );
}


/*
 *  NOT USED
 */
int
camp_insGetFile( char* path, char* filename )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
    {
        return( CAMP_INVAL_INS );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    strcpy( filename, pIns->iniFile );

    return( CAMP_SUCCESS );
}


/*
 *  NOT USED
 */
int
camp_insGetTypeIdent( char* path, char* typeIdent )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
    {
        return( CAMP_INVAL_INS );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    strcpy( typeIdent, pIns->typeIdent );

    return( CAMP_SUCCESS );
}


/*
 *  NOT USED
 */
int
camp_insGetIfTypeIdent( char* path, char* typeIdent )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
    {
        return( CAMP_INVAL_INS );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL ) 
    {
        return( CAMP_INVAL_IF );
    }
    else 
    {
        strcpy( typeIdent, pIns->pIF->typeIdent );
    }

    return( CAMP_SUCCESS );
}


