/*
 *  Name:      camp_msg_priv.c
 *
 *  Purpose:   Implement a minimal logging facility whereby the CAMP
 *             server can log informational messages to a file.
 *
 *             This facility has no relation to camp_msg_utils.c
 *
 *  Provides:
 *             camp_startLog   - open the log file, optionally specifying
 *                               whether the file should be kept closed
 *                               between log entries
 *             camp_stopLog    - close the log file
 *             camp_logVMSMsg  - (VMS only) log a message based on a VMS
 *                               message number
 *             camp_logMsg     - log a string message in the CAMP server
 *
 *  Called by:
 *             "main" (or campSrv for VxWorks) calls camp_startLog
 *             srv_rundown calls camp_stopLog
 *             camp_logMsg is called in many parts of the CAMP server
 *             camp_logVMSMsg is not used
 * 
 *  Preconditions:
 *             camp_startLog must be called before logging any messages
 *             with camp_logMsg or camp_logVMSMsg.
 *
 *  Postconditions:
 *             camp_stopLog should be called when finished with the log file.
 *
 *  Revision history:
 *
 */
/*
 *  camp_msg_priv.c,  Log file routines
 */

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include "camp_srv.h"

static char logFilename[LEN_FILENAME];
static FILE* fLog;
static bool_t keepClosed;

static bool_t openLog ( void );
static bool_t reopenLog ( void );
static bool_t closeLog ( void );


int
camp_startLog( char* filename, bool_t keepClosedFlag )
{
    fLog = NULL;

    if( filename != NULL )
    {
        strcpy( logFilename, filename );
        keepClosed = keepClosedFlag;
        if( !openLog() ) return( CAMP_FAILURE );
        if( keepClosed && !closeLog() ) return( CAMP_FAILURE );
    }
    else
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
camp_stopLog( void )
{
    if( !keepClosed && !closeLog() ) return( CAMP_FAILURE );
    fLog = NULL;

    return( CAMP_SUCCESS );
}


static bool_t
openLog( void )
{
    fLog = fopen( logFilename, "w" );
    if( fLog == NULL )
    {
        strcpy( logFilename, "" );
        keepClosed = FALSE;
        return( FALSE );
    }
    return( TRUE );
}


static bool_t
reopenLog( void )
{
    fLog = fopen( logFilename, "a" );
    if( fLog == NULL )
    {
        strcpy( logFilename, "" );
        keepClosed = FALSE;
        return( FALSE );
    }
    return( TRUE );
}


static bool_t
closeLog( void )
{
    if( fLog != NULL )
    {
        if( fclose( fLog ) == -1 ) return( FALSE );
        fLog = NULL;
    }

    return( TRUE );
}


#ifdef VMS
/*
 *  camp_VMSMsg,  Print a VMS Message Utility message
 */
void 
camp_logVMSMsg( u_long numMsg )
{
    int status;
    char strMsg[LEN_MSG+1];
    u_short lenMsg;
    s_dsc dscMsg;

    setdsctobuf( &dscMsg, strMsg, LEN_MSG );

#define TEXT 1
#define IDENT 2
#define SEVERITY 4
#define FACILITY 8

    /*
     *        Get the corresponding message string
     */
    status = sys$getmsg( numMsg, &lenMsg, &dscMsg, 
                         FACILITY | SEVERITY | TEXT, NULL );
    if( _failure( status ) )
    {
        camp_appendMsg( "camp_numMsg: failed sys$getmsg" );
    } 
    else
    {
        strMsg[lenMsg] = '\0';
        camp_logMsg( strMsg );
    }

    /*
     *        Exit whenever there is a SEVERE error
     */
/*
    if( ( numMsg & MSG_SEVER_MASK ) == MSG_SEVERE ) 
        exit( numMsg );
*/
}
#endif


void
camp_logMsg( char* msg )
{
    time_t t;
    struct tm tmb;
    char buf[32];

    if( keepClosed && !reopenLog() ) 
    {
        return;
    }

    if( fLog == NULL )
    {
        return;
    }

    time( &t );
    localtime_r( &t, &tmb );

    strftime( buf, 31, "[%c] ", &tmb );
    fputs( buf, fLog );
    fputs( msg, fLog );
    fputs( "\n", fLog );
/*
    fflush( fLog );
*/
    if( keepClosed ) closeLog();
}


