/*
 *  Name:       camp_if_none_gen.c
 *
 *  $Id$
 *
 *  $Revision$
 *
 *  Purpose:    Provides the "none" interface type, for Camp "instrument" 
 *              types that have no real instrument; for example the generic
 *              PID controller.
 *
 *              Provide the following:
 *                int if_none_init ( void );
 *                int if_none_online ( CAMP_IF *pIF );
 *                int if_none_offline ( CAMP_IF *pIF );
 *                int if_none_read( REQ* pReq );
 *                int if_none_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP "none" interface, the read and
 *              write functions are defined to execute some Tcl script in
 *              its own interp.  This might be useful for multi-threading
 *              slow operations.
 *
 *  $Log$
 *  Revision 1.1  2003/11/10 22:41:25  asnd
 *  Get interface type "none" working
 *
 */

#include <stdio.h>
#include "camp_srv.h"

int
if_none_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int none_if_type;

    pIF_t = camp_ifGetpIF_t( "none" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_none_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_none_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}

int
if_none_read( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed pseudo-read: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

int
if_none_write( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed write: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

