#include <iodef.h>
#include <ssdef.h>
#include <descrip.h>
#include <ttdef.h>
#include <tt2def.h>
 
typedef struct TTYBAUDS {    short        mask;
                unsigned short    speed;    } TTYBAUDS;
 
typedef struct IOSTATBLK {    short    status;
                char    txbps;
                short    rxbps;
                char    cr, lf;
                char    parity;
                char    dummy;        } IOSTATBLK;
 
typedef struct TTYSENSE {    char    class;
                char    type;
                short    width;
                long    basic;
                long    xtnd;        } TTYSENSE;
 
typedef struct TTYTERM {    long    zero;
                long    mask;        } TTYTERM;
 
typedef struct TYPEAHD {    short    count;
                char    first;
                char    dummy[5];    } TYPEAHD;
 
int _cbits;
int _sbits;
int _pbits = 1;
int _tspeed;
 
$DESCRIPTOR(ttyName, "SYS$COMMAND");
 
extern    int         pushBack;
static    short         gotTTY = 0;
static    short         ttyChan;
static    short         ttyCounts;
static    IOSTATBLK     ioStatBlk;
static    IOSTATBLK     ttyStat;
static    TTYSENSE     ttyStart;
static    TTYSENSE     ttySet;
static    TTYTERM         ttyTerm;
static    TYPEAHD         typeAhead;
static    int         rc;
 
static    TTYBAUDS     baudTbl[] = {
 
    {TT$C_BAUD_50,       50},    {TT$C_BAUD_75,              75},
    {TT$C_BAUD_110,      110},    {TT$C_BAUD_134,            134},
    {TT$C_BAUD_150,      150},    {TT$C_BAUD_300,            300},
    {TT$C_BAUD_600,      600},    {TT$C_BAUD_1200,     1200},
    {TT$C_BAUD_1800,     1800},    {TT$C_BAUD_2000,     2000},
    {TT$C_BAUD_2400,     2400},    {TT$C_BAUD_3600,     3600},
    {TT$C_BAUD_4800,     4800},    {TT$C_BAUD_7200,     7200},
    {TT$C_BAUD_9600,     9600},    {TT$C_BAUD_19200,    19200},
    {TT$C_BAUD_38400,    38400}};
 
static    int     nBauds = (sizeof(baudTbl) / sizeof(*baudTbl));
 
ttyinit()    /* Initialize ``raw'' mode. */
{
    int         i;
 
    if (!gotTTY) {
 
    if (((rc = SYS$ASSIGN(&ttyName, &ttyChan, 0, 0)) & 1) != 1) exit (rc);
 
    rc = SYS$QIOW(1,
           ttyChan, IO$_SENSEMODE, &ttyStat, 0,0, &ttyStart, 12, 0,0,0,0);
 
    if ((rc & 1) != 1)                    exit (rc);
 
    ttySet = ttyStart;    gotTTY = 1;
 
    ttySet.basic &= ~( TT$M_ESCAPE )   ;    /* Reset */
    ttySet.basic |=    TT$M_NOECHO     ;    /* Set   */
    ttySet.xtnd  |=    TT2$M_PASTHRU   ;    /* Set   */
 
    ttyCounts = (ttyStat.lf << 8) | ttyStat.cr;
 
    _cbits = (ttySet.basic & TT$M_EIGHTBIT) ? 8 : 7;
 
    for (i = 0; i < nBauds; i += 1)
 
        if (ttyStat.txbps == baudTbl[i].mask) break;
 
    if (i >= nBauds)                exit (SS$_PARITY);
 
    _sbits = ((_tspeed = baudTbl[i].speed) <= 150) ? 2 : 1;
    }
 
    rc = SYS$QIOW(1, ttyChan, IO$_SETMODE, &ioStatBlk,
                    0, 0, &ttySet, 12, 0, ttyCounts, 0, 0);
 
    if ((rc & 1) != 1)                        exit (rc);
    }
 
tty1byte()    /* Read one byte in ``raw mode''. */
{
    char     buf[1];
 
    rc = SYS$QIOW(1,
        ttyChan, IO$_READVBLK, &ioStatBlk, 0,0, buf, 1,0, &ttyTerm, 0,0);
 
    if ((rc & 1) != 1)                        exit (rc);
 
    return (*buf);
    }
 
ttywaiting()    /* Determine number of ``raw'' mode characters queued. */
{
    int         key;
 
    if (pushBack != 0)                    return(pushBack);
 
    rc = SYS$QIOW(1, ttyChan, IO$_SENSEMODE+IO$M_TYPEAHDCNT,
                &ttyStat, 0, 0, &typeAhead, 0, 0, 0, 0, 0);
 
    if ((rc & 1) != 1)                        exit (rc);
 
    return ((typeAhead.count > 0) ? typeAhead.first : 0);
    }
 
ttyexit()    /* Shut down ``raw'' mode. */
{
    rc = SYS$QIOW(1, ttyChan, IO$_SETMODE, &ioStatBlk,
                0, 0, &ttyStart, 12, 0, ttyCounts, 0, 0);
 
    if ((rc & 1) != 1)                        exit (rc);
    }
