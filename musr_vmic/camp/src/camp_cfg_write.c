/*
 *  Name:       camp_cfg_write.c
 *
 *  Purpose:    Write the CAMP configuration to a file from the CAMP server
 *
 *  Revision history:
 *    18-Dec-2000  TW  In varLink:  added curly brackets, and only write
 *                     lnkSet if path is something other than an empty string
 *
 */

#include "camp_srv.h"


/*
 *  Name:       campCfg_write
 *
 *  Purpose:    Write the CAMP configuration to a file
 *
 *  Called by:  srv_loop - CAMP server main thread loop, for autosave of
 *                         camp.cfg
 *              campsrv_sysrundown_13 - autosave of camp.cfg before shutdown
 *              campsrv_sysreboot_13 - autosave of camp.cfg before restart
 *              campsrv_syssave_13 - save configuration to any file name
 * 
 *  Inputs:
 *
 *  Preconditions:
 *
 *  Outputs:
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */
int
campCfg_write( char* filename )
{
    /*  Save all instrument initialization files  */
    save_all_ini();

    /*  Open the configuration file (see camp_write.c)  */
    if( !openOutput( filename ) ) return( CAMP_INVAL_FILE );

    /* 
     *  Initialize file status (sets up initial tab indents properly)
     */
    reinitialize( filename, NULL, NULL );

    /*  Write Tcl commands necessary to add the current list of instruments */
    cfgWrite_insadd();

    /*  Write particular instrument settings as Tcl commands */
    /*  This actually just writes the commands necessary to load instrument
        initialization files to the configuration file */
    cfgWrite_insset();

    /*  Make sure configuration file always exits with success status when
        interpreted by Tcl */
    print( "return -code ok\n" );

    /*  Close configuration file */
    closeOutput();

    return( CAMP_SUCCESS );
}


/*
 *  Name:       save_all_ini
 *
 *  Purpose:    Save all instrument initialization files according to the
 *              current state of the instrument.
 *              File names default to camp_ins_<name>.ini unless set otherwise
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void
save_all_ini( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    RES* pRes;
    INS_FILE_req insFile_req;
    char filename[LEN_FILENAME];

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;
        /*
         *  Make sure an INI file exists for the Instrument
         */
        if( ( pIns->iniFile == NULL ) || ( strlen( pIns->iniFile ) == 0 ) )
        {
            sprintf( filename, INI_PFMT, pVar->core.ident );
        }
        else
        {
            strcpy( filename, pIns->iniFile );
        }
        insFile_req.dreq.path = pVar->core.path;
        insFile_req.flag = 1;
        insFile_req.datFile = filename;
        pRes = campsrv_inssave_13( &insFile_req, get_current_rqstp() );
        _free( pRes );
    }
}


/*
 *  Name:       cfgWrite_insadd
 *
 *  Purpose:    Write Tcl command necessary to add all current instruments
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void 
cfgWrite_insadd( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;
        print_tab( "%s %s %s\n", toktostr( TOK_INS_ADD ), 
                    pIns->typeIdent, pVar->core.ident );
    }
}


/*
 *  Name:       cfgWrite_insset
 *
 *  Purpose:    Write Tcl commands necessary to load the instrument
 *              initialization files for all current instruments.
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void 
cfgWrite_insset( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;

        if( ( pIns->iniFile != NULL ) && ( strlen( pIns->iniFile ) > 0 ) )
        {
            print_tab( "catch {%s %s {%s}}\n", toktostr( TOK_INS_LOAD ), 
                        pVar->core.path, pIns->iniFile );
	}
        add_tab();
        camp_varDoProc_recursive( cfgWrite_varLink, pVar->pChild );
        del_tab();
    }
}


/*
 *  Name:         cfgWrite_if
 *
 *  Purpose:      Write interface settings
 *
 *  Called by:    iniWrite_Instrument (camp_ini_write.c)
 * 
 *  Inputs:       Instrument interface structure pointer
 *
 *  Outputs:      Status
 *
 *  Revision history:
 *
 */
int
cfgWrite_if( CAMP_IF* pIF )
{
    print( "%s %s %f %s ", 
                toktostr( TOK_OPT_IF_SET ),
		pIF->typeIdent,
                pIF->accessDelay,
                pIF->defn );

    return( CAMP_SUCCESS );
}


/*
 *  Name:       cfgWrite_varLink
 *
 *  Purpose:    Write Tcl command necessary to set the value of a
 *              Link variable.
 *
 *  Called by:  cfgWrite_insset - recursively for each instrument
 * 
 *  Inputs:     Pointer to CAMP variable structure
 *
 *  Outputs:    None
 *
 *  Revision history:
 *    18-Dec-2000  TW  Added curly brackets, and only write lnkSet if path
 *                     is something other than an empty string
 *
 */
void 
cfgWrite_varLink( CAMP_VAR* pVar )
{
    if( pVar->spec.varType == CAMP_VAR_TYPE_LINK )
    {
        CAMP_LINK* pLnk = pVar->spec.CAMP_SPEC_u.pLnk;

        if( !streq( pLnk->path, "" ) )
        {
          print( "%s %s {%s}\n", toktostr( TOK_LNK_SET ), 
                 pVar->core.path, pLnk->path );
        }
    }
}


