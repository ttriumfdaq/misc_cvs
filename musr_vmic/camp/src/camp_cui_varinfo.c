/*
 *  $Id$
 *
 *  $Revision$
 *
 *  Purpose:    Manage and display the variable info window (the window
 *              on the right of the screen with the "Variable" title).
 *
 *
 *  $Log$
 *  Revision 1.7  2009/08/19 02:25:52  asnd
 *  Ensure all windows flagged with NULL when deleted
 *
 *  Revision 1.6  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.5  2005/07/06 03:53:32  asnd
 *  Change menus (and hotkeys) for loading instrument configurations.
 *
 *  Revision 1.4  2004/01/28 03:14:37  asnd
 *  Add VME interface type.
 *
 *  Revision 1.3  2000/12/22 23:48:11  David.Morris
 *  Added INDPAK display of parameters
 *
 *
 *  Revision history:
 *  00-Oct-1999  DA     Clean up representation of floating point numbers,
 *                      Make some other things fit in their fields,
 *                      For Linux, uncoverVarInfoWin (automatic in VMS),
 *  24-Jan-2000  DA     Display the first non-null of: message, help, title
 */

#include <stdio.h>
#include <curses.h>
#include "timeval.h"
#include "camp_cui.h"

WINDOW* varInfoWin = NULL;

extern char currVarPath[LEN_PATH+1];

/* 
 *  Given a character buffer containing a floating point number, return 
 *  a representation of that number that fits a specified width.
 *  PACK_PRECISION is the maximum precision, to ensure that packDoubleF
 *  does not fill a wide field width with garbage digits.
 */

#define PACK_PRECISION 11

char*
packDoubleF( int width, char buf[] )
{
  double var, avar;
  int i,w;

    i = sscanf( buf, "%lf", &var );
    if( i==1 )
    {
      avar = var;
      if( var < 0.0 ) 
      {
	++i;
	avar = -var;
      }
      w = _min( width-i, PACK_PRECISION);
      if( ((avar >= 100000.) || (avar < 0.001)) && (avar > 1.0e-15) )
	sprintf( buf, "%*.*le", width, _max(0, _min(width-i-5, PACK_PRECISION-1)), var);
      else if( avar >= 10000. )
	sprintf( buf, "%*.*lf", width, w-5, var);
      else if( avar >= 1000. )
	sprintf( buf, "%*.*lf", width, w-4, var);
      else if( avar >= 100. )
	sprintf( buf, "%*.*lf", width, w-3, var);
      else if( avar >= 10. )
	sprintf( buf, "%*.*lf", width, w-2, var);
      else
	sprintf( buf, "%*.*lf", width, w-1, var);
    }
    return( buf );
}

/*
 * Routines for the display of variable info on the right side of the screen.
 */
int
initVarInfoWin( void )
{
    varInfoWin = newwin( VARINFOWIN_H, VARINFOWIN_W, 
                         VARINFOWIN_Y, VARINFOWIN_X ); 
    if( varInfoWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( varInfoWin, FALSE );
    scrollok( varInfoWin, FALSE );
    keypad( varInfoWin, TRUE );
    immedok( varInfoWin, FALSE );
    clearok( varInfoWin, FALSE );
    wtimeout( varInfoWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) {
      delwin( varInfoWin );
      varInfoWin = (WINDOW*)NULL;
    }
}


void 
clearVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) werase( varInfoWin );
}


void 
touchVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) touchwin( varInfoWin );
}


void 
refreshVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) 
    {
      wrefresh( varInfoWin );
    }
}

#ifndef VMS
void 
uncoverVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) 
    {
      touchwin( varInfoWin );
      wnoutrefresh( varInfoWin );
    }
}
#endif

void
displayVarInfoWin( void )
{
    CAMP_VAR* pVar;
    int y, x, i, w, width;
    char buf[128];
    CAMP_INSTRUMENT* pIns;
    char str[128];
    char *p1, *p2;

#define OFF_TEXT    "off"
#define NONE_TEXT   "<undefined>"
#define COL_OFFSET  2
#define COL1        2
#define COL2        (COL1+17)
#define COLTOT_W    (VARINFOWIN_W-4)
#define COL2_W      (VARINFOWIN_W-COL2-2)
#define COL1A       (COL1+COL_OFFSET)
#define COL1A_W     ((VARINFOWIN_W-COL1A-COL_OFFSET-2)/2)
#define COL2A       (COL1A+COL1A_W+COL_OFFSET)
#define COL2A_W     (VARINFOWIN_W-COL2A-2)
#define COLTOTA_W   (COL1A_W+COL_OFFSET+COL2A_W)
#define _nonull(s)  ((s==NULL||strlen(s)==0)?NONE_TEXT:s)
#define HELP_H      (VARINFOWIN_H-14)
#define HELP_Y      (VARINFOWIN_H-HELP_H-1)

    clearVarInfoWin();

    boxWin( varInfoWin, VARINFOWIN_H, VARINFOWIN_W );

    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL )
    {
        refreshVarInfoWin();
        return;
    }

    y = 0;

    wattron( varInfoWin, A_BOLD );
    sprintf( buf, "Variable: %s", pVar->core.path );
    x = _max( ((VARINFOWIN_W-strlen( buf ))/2), 1 );
    mvwaddstr( varInfoWin, y, x, buf );
    wattroff( varInfoWin, A_BOLD );
    y++;

    mvwaddstr( varInfoWin, y, COL1, "Type:" );
    wmove( varInfoWin, y, COL2 );
    wprintw( varInfoWin, "%*s", COL2_W, getTypeStr( pVar->core.varType ) );
    y++;

    if( HELP_H > 1 )
    {
        if ( strlen( pVar->core.statusMsg ) > 0 )
        {
            mvwaddstr( varInfoWin, HELP_Y, COL1, "Message:" );
            p1 = pVar->core.statusMsg;
        }
        else if ( strlen( pVar->core.help ) > 0 )
        {
            mvwaddstr( varInfoWin, HELP_Y, COL1, "Help:" );
            p1 = pVar->core.help;
        }
        else if ( strlen( pVar->core.title ) > 0 )
        {
            mvwaddstr( varInfoWin, HELP_Y, COL1, "Title:" );
            p1 = pVar->core.title;
        }
        else
        {
            p1 = NULL;
        }

	if( p1 != NULL )
	{
	    width = VARINFOWIN_W-COL1A-2;
	    /*
	     *  Turn all whitespaces into spaces
	     */
	    for( p2=p1; *p2 != '\0'; p2++ ) if( isspace(*p2) ) *p2 = ' ';
	    /*
	     *  Display the help nicely, without breaking up words.
	     */
	    for( i = 1; i < HELP_H; i++ )
	    {
		while( isspace( *p1 ) ) p1++;
		w = strlen( p1 );
		if( w == 0 ) break;
		else if( w > width )
		{
		    for( p2=p1+width-1; (p2>p1) && !isspace(*p2); p2-- ) ;
		    w = (p2==p1) ? width : p2-p1;
		}
		wmove( varInfoWin, HELP_Y+i, COL1A );
		wprintw( varInfoWin, "%.*s", w, p1 );
		p1 += w;
	    }
	}
    }
 
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_STRUCTURE:
        break;

      case CAMP_VAR_TYPE_INSTRUMENT:

        pIns = pVar->spec.CAMP_SPEC_u.pIns;

        mvwaddstr( varInfoWin, y, COL1, "Instrument type:" );
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, pIns->typeIdent );
        y++;

        wmove( varInfoWin, y, COL1 );
        waddstr( varInfoWin, "Loc" );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'k' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "er:" );
        wattroff( varInfoWin, A_BOLD );
        if( pVar->core.status & CAMP_INS_ATTR_LOCKED )
        {
            sprintf( buf, "%08x", pIns->lockPID );
        }
        else
        {
            strcpy( buf, "<none>" );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        wmove( varInfoWin, y, COL1 );
        waddch( varInfoWin, 'C' );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'o' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "m:" );
        wattroff( varInfoWin, A_BOLD );
        if( ( pIns->pIF == NULL ) || 
            !( pIns->pIF->status & CAMP_IF_ONLINE ) )
        {
            strcpy( buf, "offline" );
        }
        else
        {
            strcpy( buf, "online" );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        wmove( varInfoWin, y, COL1 );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'I' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "nterface:" );
        wattroff( varInfoWin, A_BOLD );
        if( pIns->pIF == NULL )
        {
            strcpy( buf, NONE_TEXT );
        }
        else
        {
            strcpy( buf, pIns->pIF->typeIdent );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        if( pIns->pIF == NULL ) break;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*d", COL1A_W, pIns->pIF->numReads );
        mvwaddstr( varInfoWin, y, COL1A, "Reads:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*d", COL2A_W, pIns->pIF->numWrites );
        mvwaddstr( varInfoWin, y, COL2A, "Writes:" );
        y++;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*d", COL1A_W, pIns->pIF->numReadErrors );
        mvwaddstr( varInfoWin, y, COL1A, "R.Errors:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*d", COL2A_W, pIns->pIF->numWriteErrors );
        mvwaddstr( varInfoWin, y, COL2A, "W.Errors:" );
        y++;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*d", COL1A_W, pIns->pIF->numConsecReadErrors );
        mvwaddstr( varInfoWin, y, COL1A, "R.E.Consec:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*d", COL2A_W, pIns->pIF->numConsecWriteErrors );
        mvwaddstr( varInfoWin, y, COL2A, "W.E.Consec:" );
        y++;

        switch( pIns->pIF->typeID )
        {
          case CAMP_IF_TYPE_NONE:
            break;

          case CAMP_IF_TYPE_RS232:
            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W,
			 camp_getIfRs232Port( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Name:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*d", COL2A_W,
			 camp_getIfRs232Baud( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL2A, "Baud:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfRs232Data( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Data bits:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfRs232Parity( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL2A, "Parity:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfRs232ReadTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfRs232WriteTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfRs232Timeout( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Timeout:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*d", COL2A_W,
			 camp_getIfRs232Stop( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL2A, "Stop bits:" );
            y++;

            break;

          case CAMP_IF_TYPE_GPIB:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfGpibAddr( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Addr:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfGpibReadTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfGpibWriteTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            break;

          case CAMP_IF_TYPE_CAMAC:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacB( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Branch:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacC( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Crate:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacN( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Slot:" );

            y++;

            break;

          case CAMP_IF_TYPE_TCPIP:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COLTOTA_W,
			 camp_getIfTcpipAddr( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Address:" );

            y++;
            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W,
			 camp_getIfTcpipPort( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Port:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*.1f", COL2A_W, 
			 camp_getIfTcpipTimeout( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL2A, "Timeout:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfTcpipReadTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfTcpipWriteTerm( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            break;

          case CAMP_IF_TYPE_INDPAK:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfIndpakSlot( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Slot:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfIndpakType( pIns->pIF->defn, str ) );
            mvwaddstr( varInfoWin, y, COL1A, "Type:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfIndpakChannel( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Channel:" );

            y++;

            break;

          case CAMP_IF_TYPE_VME:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfVmeBase( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Base:" );

            y++;
            break;

          case CAMP_IF_TYPE_TICS:
            break;

          default:
            break;
        }

        break;

      default:
        if( ( pVar->core.status & CAMP_VAR_ATTR_READ ) ||
            ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
        {
            wmove( varInfoWin, y, COL1 );
            waddstr( varInfoWin, "Value (" );

            if( ( pVar->core.status & CAMP_VAR_ATTR_READ ) &&
                ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 's' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "et/" );
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 'r' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "ead" );
            }
            else if( pVar->core.status & CAMP_VAR_ATTR_READ )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 'r' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "ead" );
            }
            else if( pVar->core.status & CAMP_VAR_ATTR_SET )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 's' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "et" );
            }

            waddstr( varInfoWin, "):" );
            wattroff( varInfoWin, A_BOLD );
        
            if( pVar->core.status & CAMP_VAR_ATTR_IS_SET )
            {
                varGetValStr( pVar, buf );
            }
            else
            {
                strcpy( buf, NONE_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_POLL )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'P' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "olling:" );
            wattroff( varInfoWin, A_BOLD );
            if( pVar->core.status & CAMP_VAR_ATTR_POLL )
            {
                sprintf( buf, "%.1f sec", pVar->core.pollInterval );
            }
            else
            {
                strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_LOG )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'L' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "ogging:" );
            wattroff( varInfoWin, A_BOLD );
            if( pVar->core.status & CAMP_VAR_ATTR_LOG )
            {
                strcpy( buf, pVar->core.logAction );
            }
            else
            {
                strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'A' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "larm:" );
            wattroff( varInfoWin, A_BOLD );
            if( pVar->core.status & CAMP_VAR_ATTR_ALARM )
            {
                strcpy( buf, pVar->core.alarmAction );
            }
            else
            {
                strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( ( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC ) &&
            ( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
        {
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_SPEC_u.pNum;

            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'T' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "olerance:" );
            wattroff( varInfoWin, A_BOLD );
            switch( pNum->tolType )
            {
              case 0:
                if( pNum->tol >= 0.0 ) sprintf( buf, "+-%f", pNum->tol );
                else strcpy( buf, NONE_TEXT );
                break;
              case 1:
                if( pNum->tol >= 0.0 ) sprintf( buf, "%.0f%%", pNum->tol );
                else strcpy( buf, NONE_TEXT );
                break;
              default:
                strcpy( buf, NONE_TEXT );
                break;
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC )
        {
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_SPEC_u.pNum;

            wmove( varInfoWin, y, COL2 );
            if( !streq( pNum->units, "" ) ) 
            {
                wprintw( varInfoWin, "%*s", COL2_W, pNum->units );
            }
            else
            {
                wprintw( varInfoWin, "%*s", COL2_W, NONE_TEXT );
            }
            mvwaddstr( varInfoWin, y, COL1, "Units:" );
        }
        y++;

        if( ( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC ) && 
            ( pVar->spec.CAMP_SPEC_u.pNum->timeStarted.tv_sec > 0 ) )
        {
            double mean, stddev, skew;
	    int bl;
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_SPEC_u.pNum;

            strcpy( buf, asctimeval( &pNum->timeStarted ) );
	    bl = strlen(buf) - 6;
            buf[bl] = '\0';
            wmove( varInfoWin, y, COL1 );
	    if( COLTOT_W - bl < 6 )
	    {
	      wprintw( varInfoWin, "%s", buf );
	    }
	    else if( COLTOT_W - bl < 12 )
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Start:", buf );
	    }
	    else if( COLTOT_W - bl < 21 )
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Stats since:", buf );
	    }
	    else
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Statistics starting:", buf );
	    }

            y++;

            wmove( varInfoWin, y, COL1A );
            numGetValStr( pVar->core.varType, pNum->low, buf );
            wprintw( varInfoWin, "Low:  %s", packDoubleF( COL1A_W-6, buf ) );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, pNum->hi, buf );
            wprintw( varInfoWin, "High: %s", packDoubleF( COL2A_W-6, buf ) );

            y++;

            camp_varNumCalcStats( pVar->core.path, &mean, &stddev, &skew );

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "Num: %*d", COL1A_W-5, pNum->num );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, mean, buf );
            wprintw( varInfoWin, "Mean: %s", packDoubleF( COL2A_W-6, buf ) );

            y++;

            wmove( varInfoWin, y, COL1A );
            numGetValStr( pVar->core.varType, stddev, buf );
            wprintw( varInfoWin, "StDv: %s", packDoubleF( COL1A_W-6, buf ) );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, skew, buf );
            wprintw( varInfoWin, "Skew: %s", packDoubleF( COL2A_W-6, buf ) );

            y++;
        }

        break;
    }

    refreshVarInfoWin();
}

