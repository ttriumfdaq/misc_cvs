/*
 *  Name:       camp_token.c
 *
 *  Purpose:    Define some common strings that are used by the Tcl
 *              interpreter as well as by file writing routines and other
 *              utility routines that operate on string tokens.
 *
 *  Called by:  Various
 * 
 *
 *  Revision history:
 *    06-Oct-1995  TW  TOK_OPT_IF_SET change -if_set => -if
 *
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "camp.h"

static TOKEN tokstrings[] = {
        {TOK_SYS_UPDATE, "sysUpdate"},
        {TOK_SYS_RUNDOWN, "sysShutdown"},
        {TOK_SYS_REBOOT, "sysReboot"},
        {TOK_SYS_LOAD, "sysLoad"},
        {TOK_SYS_SAVE, "sysSave"},
        {TOK_SYS_GETINSTYPES, "sysGetInsTypes"},
        {TOK_SYS_GETALARMACTS, "sysGetAlarmActs"},
        {TOK_SYS_GETLOGACTS, "sysGetLogActs"},
        {TOK_SYS_GETIFTYPES, "sysGetIfTypes"},
        {TOK_SYS_GETIFCONF, "sysGetIfConf"},
        {TOK_SYS_ADDALARMACT, "sysAddAlarmAct"},
        {TOK_SYS_ADDLOGACT, "sysAddLogAct"},
        {TOK_SYS_ADDIFTYPE, "sysAddIfType"},
        {TOK_SYS_ADDINSTYPE, "sysAddInsType"},
        {TOK_SYS_ADDINSAVAIL, "sysAddInsAvail"},
        {TOK_MSG, "msg"},
        {TOK_INS_ADD, "insAdd"},
        {TOK_INS_DEL, "insDel"},
        {TOK_INS_SET, "insSet"},
        {TOK_INS_LOAD, "insLoad"},
        {TOK_INS_SAVE, "insSave"},
        {TOK_INS_IF_ON, "insIfOn"},
        {TOK_INS_IF_OFF, "insIfOff"},
        {TOK_INS_IF_READ, "insIfRead"},
        {TOK_INS_IF_WRITE, "insIfWrite"},
        {TOK_INS_IF_READ_VERIFY, "insIfReadVerify"},
        {TOK_INS_IF_WRITE_VERIFY, "insIfWriteVerify"},
        {TOK_INS_IF_DUMP, "insIfDump"},
        {TOK_INS_IF_UNDUMP, "insIfUndump"},
        {TOK_VAR_SET, "varSet"},
        {TOK_VAR_DOSET, "varDoSet"},
        {TOK_VAR_READ, "varRead"},
        {TOK_VAR_GET, "varGet"},
        {TOK_LNK_SET, "lnkSet"},
        {TOK_INT, "CAMP_INT"},
        {TOK_FLOAT, "CAMP_FLOAT"},
        {TOK_SELECTION, "CAMP_SELECT"},
        {TOK_STRING, "CAMP_STRING"},
        {TOK_ARRAY, "CAMP_ARRAY"},
        {TOK_STRUCTURE, "CAMP_STRUCT"},
        {TOK_INSTRUMENT, "CAMP_INSTRUMENT"},
        {TOK_LINK, "CAMP_LINK"},
        {TOK_SELECTIONS, "-selections"},
        {TOK_ARRAYDEF, "-arrayDef"},
        {TOK_VARTYPE, "-varType"},
        {TOK_INSTYPE, "-insType"},
        {TOK_TITLE, "-T"},
        {TOK_HELP, "-H"},
        {TOK_SHOW_ATTR, "-D"},
        {TOK_SET_ATTR, "-S"},
        {TOK_READ_ATTR, "-R"},
        {TOK_POLL_ATTR, "-P"},
        {TOK_LOG_ATTR, "-L"},
        {TOK_ALARM_ATTR, "-A"},
        {TOK_SHOW_FLAG, "-d"},
        {TOK_SET_FLAG, "-s"},
        {TOK_READ_FLAG, "-r"},
        {TOK_POLL_FLAG, "-p"},
        {TOK_POLL_INTERVAL, "-p_int"},
        {TOK_LOG_FLAG, "-l"},
        {TOK_LOG_ACTION, "-l_act"},
        {TOK_ALARM_FLAG, "-a"},
        {TOK_ALARM_TOL, "-tol"},
        {TOK_ALARM_TOLTYPE, "-tolType"},
        {TOK_ALARM_ACTION, "-a_act"},
        {TOK_UNITS, "-units"},
        {TOK_VALUE_FLAG, "-v"},
        {TOK_ZERO_FLAG, "-z"},
        {TOK_ALERT_FLAG, "-alert"},
        {TOK_MSG_FLAG, "-m"},
        {TOK_ON, "on"},
        {TOK_OFF, "off"},
        {TOK_END, "end"},
        {TOK_OPT_DEF, "-d"},
        {TOK_OPT_IF_SET, "-if"},
        {TOK_OPT_IF_MOD, "-if_mod"},
        {TOK_OPT_IF, "-if"},
        {TOK_OPT_LOCK, "-lock"},
        {TOK_OPT_LINE, "-line"},
        {TOK_OPT_ONLINE, "-online"},
        {TOK_OPT_OFFLINE, "-offline"},
        {TOK_NONE, "none"},
        {TOK_ODD, "odd"},
        {TOK_EVEN, "even"},
        {TOK_CR, "CR"},
        {TOK_LF, "LF"},
        {TOK_CRLF, "CRLF"},
        {TOK_LFCR, "LFCR"},
        {TOK_OPT_READPROC, "-readProc"},
        {TOK_OPT_WRITEPROC, "-writeProc"},
        {TOK_OPT_SETPROC, "-setProc"},
        {TOK_OPT_DRIVERTYPE, "-driverType"},
        {TOK_OPT_INITPROC, "-initProc"},
        {TOK_OPT_ONLINEPROC, "-onlineProc"},
        {TOK_OPT_OFFLINEPROC, "-offlineProc"},
        {TOK_OPT_DELETEPROC, "-deleteProc"},
        {TOK_EOF, "??????"}
};


char* 
toktostr( TOK_KIND kind )
{
        TOKEN *sp;

        for( sp = tokstrings; 
             sp->kind != TOK_EOF && sp->kind != kind; 
             sp++);
        return (sp->str);
}


bool_t 
findToken( char* str, TOKEN* ptok )
{
    int len;
    TOKEN* pt;
    TOK_KIND start_token;

    start_token = TOK_SYS_UPDATE;

    for( pt = &tokstrings[(int)start_token]; pt->kind != TOK_EOF; pt++ ) 
    {
        len = strlen( pt->str );
        if( strncmp( str, pt->str, len ) == 0 ) 
        {
            if( !isalnum( str[len] ) && str[len] != '_' && str[len] != '/' ) 
            {
                ptok->kind = pt->kind;
                ptok->str = pt->str;
                return( TRUE );
            }
        }
    }
    return( FALSE );
}



