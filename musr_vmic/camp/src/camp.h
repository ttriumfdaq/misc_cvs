/*
 *	$Id$
 *
 *	$Revision$
 *
 *  Purpose:     This file handles all definitions for the system
 *
 *  Modification history:
 *    13-Dec-1999  TW  Casting for xdr_free (gcc warnings)
 *    14-Dec-1999  TW  Prototype of camp_varGetPHead added
 *                     Fixed double define of TRUE,FALSE
 *    16-Dec-1999  TW  Define CAMP_RPC_CLNT_TIMEOUT here
 *                     Use CAMP_DEBUG to conditionally compile all
 *                     camp_debug conditionals
 *    20-Jan-2000  SD  Add nodename to writeable /camp directories to
 *                     allow several servers (MVME) to be accessed by one host
 *                     (VAX or linux presently) without overwriting log,ini files
 *
 *  $Log$
 *  Revision 1.13  2009/04/04 00:00:35  asnd
 *  Remove and adjust priority handling that caused gpib lockups
 *
 *  Revision 1.12  2008/03/27 01:08:51  asnd
 *  Reduce the long "var" timeout, so system does not lock up for so long.
 *
 *  Revision 1.11  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.10  2004/01/28 03:50:32  asnd
 *  Add VME interface type.
 *
 *  Revision 1.9  2002/07/24 00:34:24  suz
 *  Donald adds ifdef MULTITHREADED to conditionally skip the thread definition that prevented mdarc from compiling
 *
 *  Revision 1.8  2002/05/08 00:47:43  suz
 *  Add support for ppc (casting & ifdef VXWORKS)
 *
 *  Revision 1.6  2001/02/10 04:16:15  asnd
 *  Add insIfDump and insIfUndump for binary-clean data dump from instrument to
 *  a file and from a file to the instrument.
 *
 *  Revision 1.5  2001/01/15 06:41:28  asnd
 *  Error message for rs232PortInUse.
 *
 *  Revision 1.4  2000/12/23 02:37:50  ted
 *  camp_getIfIndpakChan -> camp_getIfIndpakChannel for consistency (different in some places than others)
 *
 *  Revision 1.3  2000/12/22 22:34:57  David.Morris
 *  Added INDPAK parameter access functions and prototypes. Added CAMP_IF_TYPE_INDPAK
 *  to instrument type list
 *
 *
 */

#ifndef _CAMP_H_
#define _CAMP_H_


/*
 *  Need to know this here for the CAMP_INSTRUMENT struct.
 *  Add any other POSIX thread compliant OS here.
 *  _POSIX_THREADS is redefined in pthread.h (in camp_srv.h).
 *  I hope the value doesn't change (or matter) for other
 *  implementations.
 */
#ifdef MULTITHREADED
#if defined( VMS ) || defined( OSF1 ) || defined ( ULTRIX )
#define _POSIX_THREADS 1
#elif defined( VXWORKS )
#include "vx_pthread.h"
#endif
#endif /* MULTITHREADED */


/* 
 *  Support VxWorks
 *  Made conditional with definition VXWORKS
 */
#ifdef VXWORKS
#include <vxWorks.h>
#include <sockLib.h>
#include <selectLib.h>
#include <hostLib.h>
#include <in.h>
#include <ioLib.h>
#include <netinet/tcp.h>
#define NO_FORTRAN 1
#endif /* VXWORKS */

#ifdef linux
#define NO_FORTRAN 1
#endif /* linux */


/*
 *  RPC includes
 */
#if defined( VMS ) && defined( UCX )
#include <ucx$rpcxdr.h>
#else
/* UNIX, VxWorks, VMS using MultiNet, etc. */
#undef TRUE
#undef FALSE
#include <rpc/rpc.h>
#if !defined(TRUE)
#define TRUE 1
#endif
#if !defined(FALSE)
#define FALSE 0
#endif
#ifndef VXWORKS
#include <sys/time.h>
#endif /* VXWORKS */
#endif

/*
 *  timeval_t and related routines
 */
#include "timeval.h"

/*
 *  Tcl - need it here to define Tcl_Interp used in 
 *        CAMP_INSTRUMENT struct
 */
#include "tcl.h"


#ifdef VMS
#include "vaxc_utils.h"

/*
 * camp_if_rs232_tt_defs.h
 */
#include ttdef
#include tt2def
#include dcdef

#define  LEN_RS232_TT_PORT	63

#define  NO_PARITY		TT$M_ALTRPAR
#define  EVEN_PARITY		TT$M_ALTRPAR | TT$M_PARITY
#define  ODD_PARITY		TT$M_ALTRPAR | TT$M_PARITY | TT$M_ODD

#define  TERM_LF	1
#define  TERM_CR	2
#define  TERM_CRLF	3
#define  TERM_NONE	4

#define  BAUD_110	1
#define  BAUD_300	2
#define  BAUD_600	3
#define  BAUD_1200	4
#define  BAUD_2400	5
#define  BAUD_4800	6
#define  BAUD_9600	7
#define  BAUD_19200	8

#define  PARITY_NONE    1
#define  PARITY_ODD	2
#define  PARITY_EVEN    3

struct device_mode {
    u_char   class;
    u_char   type;
    short  page_width;
    union {
      struct {
	long  basic_chars;
      } s1;
      struct {
	u_char  dummy[3];
	u_char  page_len;
      } s2;
    } u1;
    long ext_chars;
};

#else /* !VMS */

#include "c_utils.h"

#define  SUCCESS_MASK           	0x00000001L
#define  _success( s )                  ( s & SUCCESS_MASK )
#define  _failure( s )                  !( s & SUCCESS_MASK )

#endif /* VMS */


/*
 *  Taken from camp_cui_main.c 16-Dec-1999
 *
 *  Changed method 19-Dec-1999 TW
 *  All timeouts now RPC timeouts
 *  This timeout overrides the RPC default of 25 seconds
 *
 *  14-Dec-2000  TW  Change RPC timeout to 30 seconds for some
 *                   RPC calls (instruments might have very long
 *                   read operations)
 *                   Decrease RPC timeout to 10 s for other calls
 */
#define CAMP_RPC_CLNT_TIMEOUT 10
#define CAMP_RPC_CLNT_VAR_TIMEOUT 15


/* 
 *  Message definitions
 */
typedef long MSG_TYPE;

/*
 *  Message masks/status bits (VMS Message Utility format)
 */
#define  MSG_SUCC_MASK		0x00000001L
#define  MSG_SEVER_MASK		0x00000007L

#define  MSG_SUCCESS		0x00000001L
#define  MSG_INFORMATIONAL	0x00000003L
#define  MSG_WARNING		0x00000000L
#define  MSG_ERROR		0x00000002L
#define  MSG_SEVERE		0x00000004L

/* success */
#define CAMP_SUCCESS                   ((1<<3)|MSG_SUCCESS) /* <success> */
/* informational */
#define CAMP_JOB_EXISTS                ((2<<3)|MSG_INFORMATIONAL) /* <the job PID was found to exist> */
#define CAMP_JOB_NONEXISTANT           ((3<<3)|MSG_INFORMATIONAL) /* <the job PID was not found to exist> */
#define CAMP_INS_NOT_LOCKED            ((4<<3)|MSG_INFORMATIONAL) /* <instrument is not locked> */
#define CAMP_INS_LOCK_REQUESTOR        ((5<<3)|MSG_INFORMATIONAL) /* <instrument was locked by the requesting process> */
/* warning */
#define CAMP_CANT_SET_IF               ((6<<3)|MSG_WARNING)  /* <can't set interface while online> */
#define CAMP_NOT_CONN                  ((7<<3)|MSG_WARNING)  /* <instrument not online - cannot perform operation> */
#define CAMP_INS_LOCKED                ((8<<3)|MSG_WARNING)  /* <cannot perform command: instrument is locked> */
#define CAMP_INS_LOCKED_OTHER          ((9<<3)|MSG_WARNING)  /* <instrument is locked by another process> */
#define CAMP_INVAL_LOCKER              ((10<<3)|MSG_WARNING) /* <cannot lock instrument from remote node> */
#define CAMP_SERVER_BUSY_TIMEOUT       ((11<<3)|MSG_WARNING) /* <warning: CAMP server busy, will process request ASAP, return status unknown> */
#define CAMP_RS232_PORT_IN_USE         ((12<<3)|MSG_WARNING) /* <rs232 port in use> */
/* failure */
#define CAMP_FAILURE                   ((11<<3)|MSG_ERROR) /* <failure> */
#define CAMP_FAIL_RPC                  ((12<<3)|MSG_ERROR) /* <failure: RPC call> */
#define CAMP_INVAL_INS                 ((13<<3)|MSG_ERROR) /* <invalid: instrument ident> */
#define CAMP_INVAL_INS_TYPE            ((14<<3)|MSG_ERROR) /* <invalid: instrument type> */
#define CAMP_INVAL_LINK                ((15<<3)|MSG_ERROR) /* <invalid: link> */
#define CAMP_INVAL_VAR                 ((16<<3)|MSG_ERROR) /* <invalid: variable item> */
#define CAMP_INVAL_VAR_TYPE            ((17<<3)|MSG_ERROR) /* <invalid: variable type> */
#define CAMP_INVAL_VAR_ATTR            ((18<<3)|MSG_ERROR) /* <invalid: variable attribute not allowed> */
#define CAMP_INVAL_FILE                ((19<<3)|MSG_ERROR) /* <invalid: filename> */
#define CAMP_INVAL_FILESPEC            ((20<<3)|MSG_ERROR) /* <invalid: filespec> */
#define CAMP_INVAL_IF                  ((21<<3)|MSG_ERROR) /* <invalid: interface> */
#define CAMP_INVAL_IF_TYPE             ((22<<3)|MSG_ERROR) /* <invalid: interface type> */
#define CAMP_INVAL_SELECTION           ((23<<3)|MSG_ERROR) /* <invalid: selection variable index> */
#define CAMP_INVAL_PARENT              ((24<<3)|MSG_ERROR) /* <error: couldn't find instrument for var> */
#define CAMP_FAIL_DUMP                 ((25<<3)|MSG_ERROR) /* <failure: couldn't dump to file> */
#define CAMP_FAIL_UNDUMP               ((26<<3)|MSG_ERROR) /* <failure: couldn't undump file to instrument> */

#define msg_inval_ins_type msg_getHelpString(CAMP_INVAL_INS_TYPE)
#define msg_inval_if       msg_getHelpString(CAMP_INVAL_IF)
#define msg_inval_if_t     msg_getHelpString(CAMP_INVAL_IF_TYPE)
#define msg_inval_parent   msg_getHelpString(CAMP_INVAL_PARENT)
#define msg_inval_var      msg_getHelpString(CAMP_INVAL_VAR)
#define msg_wrong_type     msg_getHelpString(CAMP_INVAL_VAR_TYPE)
#define msg_ins_locked     msg_getHelpString(CAMP_INS_LOCKED)
#define msg_if_notconn     msg_getHelpString(CAMP_NOT_CONN)


/*
 *  camp_defs.h
 */

#define	ON 1
#define OFF 0


/*
 *  Recognized operating system types
 *  (for checking the existance of a locker on
 *  a remote node)
 */
#define CAMP_OS_VMS  1
#define CAMP_OS_BSD  2


/*
 *  Directories/Filenames
 *
 *  Define a link or nfs mount point at /camp
 *  For VMS, this is a device called CAMP:
 *  Define also another mount point at /campnode  to be /camp/<node>
 */

// #define  CAMP_DIR		"/camp/"         /* was not used */
#define  CAMP_DAT_DIR		"/camp/dat/"     /* used in camp_sys_priv
                                                    as a default */
// #define  CAMP_LOG_DIR        "/camp/log/"     /* was not used */


// #define  CAMP_SRV_LOG_FILE	"/camp/log/camp_srv.log"     /* write */
#define  CAMP_SRV_LOG_FILE	"/campnode/log/camp_srv.log" /* write */


// #define  CAMP_SRV_AUTO_SAVE	"/camp/dat/camp.cfg"         /* write */
#define  CAMP_SRV_AUTO_SAVE	"/campnode/cfg/camp.cfg"     /* write */

#define  CAMP_SRV_INI		"/camp/dat/camp.ini"         /* read */

// #define  INI_MASK		"/camp/dat/camp_ins_*.ini"   /* write */
// #define  INI_PFMT		"/camp/dat/camp_ins_%s.ini"  /* write */
#define  INI_MASK		"/campnode/cfg/camp_ins_*.ini"  /* write */
#define  INI_PFMT		"/campnode/cfg/camp_ins_%s.ini" /* write */

// #define  INI_SFMT		"/camp/dat/camp_ins_%[^.]"/* not used ? */
// #define  INI_PFMT_BASE	"camp_ins_%s.ini"         /* not used ? */
#define  INI_SFMT_BASE		"camp_ins_%[^.]"


// #define  CAMP_CFG_MASK	"/camp/dat/camp_*.cfg"   /* format for finding config */
// #define  CAMP_CFG_PFMT	"/camp/dat/camp_%s.cfg"  /* format for writing config */

#define  CAMP_CFG_MASK		"/campnode/cfg/camp_*.cfg" /* format for finding config */
#define  CAMP_CFG_PFMT		"/campnode/cfg/camp_%s.cfg"/* format for writing config */

// #define  CAMP_CFG_SFMT	"/camp/dat/camp_%[^.]" /* not used ? */
// #define  CAMP_CFG_PFMT_BASE	"camp_%s.cfg" /* not used ?*/
#define  CAMP_CFG_SFMT_BASE	"camp_%[^.]"
#define  CAMP_TCL_MASK		"/camp/drv/camp_ins_*.tcl"  /* not used ?*/
#define  CAMP_TCL_PFMT		"/camp/drv/camp_ins_%s.tcl"
#define  CAMP_TCL_SFMT		"/camp/drv/camp_ins_%[^.]" /* not used ?*/
#define  CAMP_TCL_SFMT_BASE	"camp_ins_%[^.]" /* not used ?*/


/*
 *  Array variable maximums
 */
#define  MAX_ARRAY_DIM          3
#define  MAX_ARRAY_ELEM         1048576


/*
 *  Fixed-length string sizes
 */
#define  LEN_MSG		255
#define  MAX_LEN_MSG		2047
#define  LEN_IDENT		15
#define  LEN_INSSEC_NAME	LEN_IDENT
#define  LEN_PATH		127
#define  LEN_TITLE		31
#define  LEN_HELP		255
#define  LEN_SELECTION_LABEL	31	/* Labels for Selection data type */
#define  LEN_STRING		63	/* String data type */
#define  LEN_DISPLAY_DEFN	127
#define  LEN_STATUS_MSG		127
#define  LEN_IO			8192
#define  LEN_BUF		127
#define  LEN_STR    		127
#define  LEN_FILENAME		127	/* Space for full path */
#define  LEN_NODENAME		127	/* Space for full address */
#define  LEN_PROC		2047
#define  LEN_UNITS		15
#define  LEN_IF_DEFN		127


/*
 *  Alarm status bits
 */
#define	 CAMP_ALARM_ON		(1<<0)
#define	 CAMP_ALARM_WAS_ON	(1<<1)


/*
 *  Data status bits
 *  These must not conflict with instrument status bits
 */
#define  CAMP_VAR_ATTR_MASK	0x00000FFFL
#define  CAMP_INS_ATTR_MASK	0x0000F000L
#define  CAMP_VAR_PEND_MASK	0x00010000L

#define  CAMP_VAR_ATTR_SHOW	0x00000001L
#define  CAMP_VAR_ATTR_SET	0x00000002L
#define  CAMP_VAR_ATTR_READ	0x00000004L
#define  CAMP_VAR_ATTR_POLL	0x00000008L
#define  CAMP_VAR_ATTR_ALARM	0x00000010L
#define  CAMP_VAR_ATTR_LOG	0x00000020L
#define  CAMP_VAR_ATTR_ALERT	0x00000040L
#define  CAMP_VAR_ATTR_IS_SET	0x00000080L

#define  CAMP_INS_ATTR_LOCKED	0x00001000L

#define  CAMP_VAR_PEND_SET	0x00010000L


/*
 *  Interface masks/bits
 */
#define  CAMP_IF_ONLINE		(1<<0)


/*
 *  CAMP data type code definitions
 */
#define  CAMP_MAJOR_VAR_TYPE_MASK	0x0000FF00L


/*
 *  CAMP data type code definitions
 */
#define  CAMP_XDR_ALL		(1<<0)
#define  CAMP_XDR_UPDATE	(1<<1)
#define  CAMP_XDR_NO_NEXT	(1<<2)
#define  CAMP_XDR_NO_CHILD	(1<<3)
#define  CAMP_XDR_CHILD_LEVEL	(1<<4)



/* 
 * camp_types_mod.h
 */

u_long get_current_xdr_flag( void );
void set_current_xdr_flag( u_long flag );

#ifdef MULTITHREADED
#ifdef _POSIX_THREADS
#ifndef PTHREAD
/* 
 *  From pthread.h and cma.h, so I don't have to include
 *  pthread.h, which forces you to link with pthread
 *  libraries.
 */
typedef void                    *cma_t_address;
typedef struct CMA_T_HANDLE {
    cma_t_address       field1;
    short int           field2;
    short int           field3;
    } cma_t_handle;
typedef cma_t_handle    cma_t_mutex;    /* Needed for CMA_ONCE_BLOCK */
typedef cma_t_mutex     pthread_mutex_t;
#endif /* PTHREAD */
#endif /* _POSIX_THREADS */
#endif /* MULTITHREADED */


enum INS_CLASS {
	INS_ROOT = 1,
	INS_SUB = 1 + 1
};
typedef enum INS_CLASS INS_CLASS;


enum IO_TYPE {
	IO_READ = 1,
	IO_READ_REQ = 1 + 1,
	IO_WRITE = 1 + 2,
	IO_POLL = 1 + 3
};
typedef enum IO_TYPE IO_TYPE;


enum CAMP_VAR_TYPE {
	CAMP_VAR_TYPE_NONE = 0,
	CAMP_VAR_TYPE_NUMERIC = 256,
	CAMP_VAR_TYPE_INT = 273,
	CAMP_VAR_TYPE_FLOAT = 289,
	CAMP_VAR_TYPE_SELECTION = 512,
	CAMP_VAR_TYPE_STRING = 1024,
	CAMP_VAR_TYPE_ARRAY = 2048,
	CAMP_VAR_TYPE_STRUCTURE = 4096,
	CAMP_VAR_TYPE_INSTRUMENT = 8192,
	CAMP_VAR_TYPE_LINK = 16384
};
typedef enum CAMP_VAR_TYPE CAMP_VAR_TYPE;


enum CAMP_IF_TYPE {
	CAMP_IF_TYPE_NONE = 0,
	CAMP_IF_TYPE_RS232 = 1,
	CAMP_IF_TYPE_GPIB = 1 + 1,
	CAMP_IF_TYPE_TICS = 1 + 2,
	CAMP_IF_TYPE_CAMAC = 1 + 3,
	CAMP_IF_TYPE_INDPAK = 1 + 4,
	CAMP_IF_TYPE_VME = 1 + 5,
	CAMP_IF_TYPE_TCPIP = 1 + 6
};
typedef enum CAMP_IF_TYPE CAMP_IF_TYPE;


struct CAMP_NUMERIC {
	double val;
	timeval_t timeStarted;
	u_long num;
	double low;
	double hi;
	double sum;
	double sumSquares;
	double sumCubes;
	double offset;
	u_long tolType;
	float tol;
	char *units;
};
typedef struct CAMP_NUMERIC CAMP_NUMERIC;
bool_t xdr_CAMP_NUMERIC();


struct CAMP_SELECTION_ITEM {
	char *label;
	struct CAMP_SELECTION_ITEM *pNext;
};
typedef struct CAMP_SELECTION_ITEM CAMP_SELECTION_ITEM;
bool_t xdr_CAMP_SELECTION_ITEM();


struct CAMP_SELECTION {
	u_char val;
	u_char num;
	CAMP_SELECTION_ITEM *pItems;
};
typedef struct CAMP_SELECTION CAMP_SELECTION;
bool_t xdr_CAMP_SELECTION();


struct CAMP_STRING {
	char *val;
};
typedef struct CAMP_STRING CAMP_STRING;
bool_t xdr_CAMP_STRING();


struct CAMP_ARRAY {
	CAMP_VAR_TYPE varType;  /* CAMP_INT, CAMP_FLOAT or CAMP_STRING */
	u_int elemSize;
	u_int dim;
	u_int totElem;
	u_int dimSize[MAX_ARRAY_DIM];
	caddr_t pVal;
};
typedef struct CAMP_ARRAY CAMP_ARRAY;
bool_t xdr_CAMP_ARRAY();


struct CAMP_STRUCTURE {
	u_int dataItems_len;
};
typedef struct CAMP_STRUCTURE CAMP_STRUCTURE;
bool_t xdr_CAMP_STRUCTURE();


struct CAMP_IF {
	u_long status;
	char *typeIdent;
	timeval_t	lastAccess;
	float		accessDelay;
        u_long numReads;
        u_long numWrites;
        u_long numReadErrors;
        u_long numWriteErrors;
        u_long numConsecReadErrors;
        u_long numConsecWriteErrors;
	char *defn;  /* Tcl format definition */
	long priv;  /* For use by driver */
	CAMP_IF_TYPE typeID;
/*	CAMP_IF_SPEC spec; */
};
typedef struct CAMP_IF CAMP_IF;
bool_t xdr_CAMP_IF();


struct REQ ;


typedef struct {
        int (*onlineProc)( struct CAMP_IF* );
        int (*offlineProc)( struct CAMP_IF* );
        int (*readProc)( struct REQ* );
        int (*writeProc)( struct REQ* );
} IF_t_PROCS;


struct CAMP_IF_t {
	struct CAMP_IF_t *pNext;
	char *ident;
	IF_t_PROCS procs;
	char *defaultDefn;  /* Tcl format default defn */
	char *conf;
	long priv;
	CAMP_IF_TYPE typeID;
/*	CAMP_IF_t_SPEC spec; */
};
typedef struct CAMP_IF_t CAMP_IF_t;
bool_t xdr_CAMP_IF_t();


struct CAMP_INSTRUMENT {
	char *lockHost;
	u_long lockPID;
        char* lockOs;
	char *defFile;
	char *iniFile;
	u_int dataItems_len;
	CAMP_IF *pIF;
	char *typeIdent;
	u_long typeInstance;
	INS_CLASS class;
	char *initProc;
	char *deleteProc;
	char *onlineProc;
	char *offlineProc;
        Tcl_Interp* interp;
#ifdef MULTITHREADED
	pthread_mutex_t mutex;
#endif /* MULTITHREADED */
};
typedef struct CAMP_INSTRUMENT CAMP_INSTRUMENT;
bool_t xdr_CAMP_INSTRUMENT();


struct CAMP_LINK {
	CAMP_VAR_TYPE varType;
	char *path;
};
typedef struct CAMP_LINK CAMP_LINK;
bool_t xdr_CAMP_LINK();


struct CAMP_CORE {
	char *ident;
	char *path;
	CAMP_VAR_TYPE varType;
	u_long attributes;
	char *title;
	char *help;
	char *readProc;
	char *writeProc;
	u_long status;
	char *statusMsg;
	timeval_t timeLastSet;
	float pollInterval;
	float logInterval;
	char *logAction;
	char *alarmAction;
};
typedef struct CAMP_CORE CAMP_CORE;
bool_t xdr_CAMP_CORE();


struct CAMP_SPEC {
	CAMP_VAR_TYPE varType;
	union {
		CAMP_NUMERIC *pNum;
		CAMP_SELECTION *pSel;
		CAMP_STRING *pStr;
		CAMP_ARRAY *pArr;
		CAMP_STRUCTURE *pStc;
		CAMP_INSTRUMENT *pIns;
		CAMP_LINK *pLnk;
	} CAMP_SPEC_u;
};
typedef struct CAMP_SPEC CAMP_SPEC;
bool_t xdr_CAMP_SPEC();


struct CAMP_VAR {
	CAMP_CORE core;
	CAMP_SPEC spec;
	struct CAMP_VAR *pNext;
	struct CAMP_VAR *pChild;
};
typedef struct CAMP_VAR CAMP_VAR;
bool_t xdr_CAMP_VAR();


struct SYS_DYNAMIC {
	timeval_t timeLastSysChange;
	timeval_t timeLastInsChange;
	char *cfgFile;
};
typedef struct SYS_DYNAMIC SYS_DYNAMIC;
bool_t xdr_SYS_DYNAMIC();


struct DIRENT {
	struct DIRENT *pNext;
	char *filename;
};
typedef struct DIRENT DIRENT;
bool_t xdr_DIRENT();


typedef struct {
        int (*initProc)( struct CAMP_VAR* );
        int (*deleteProc)( struct CAMP_VAR* );
        int (*onlineProc)( struct CAMP_VAR* );
        int (*offlineProc)( struct CAMP_VAR* );
        int (*setProc)( struct CAMP_VAR*, struct CAMP_VAR*, struct CAMP_SPEC* );
        int (*readProc)( struct CAMP_VAR*, struct CAMP_VAR* );
} INS_TYPE_PROCS;


struct INS_TYPE {
	struct INS_TYPE *pNext;
	char *ident;
	char *driverType;
	INS_TYPE_PROCS procs;
};
typedef struct INS_TYPE INS_TYPE;
bool_t xdr_INS_TYPE();


struct INS_AVAIL {
	struct INS_AVAIL *pNext;
	char *ident;
	char *typeIdent;
	char *specInitProc;
};
typedef struct INS_AVAIL INS_AVAIL;
bool_t xdr_INS_AVAIL();


typedef struct {
        int (*alertProc)( bool_t );
} ALARM_ACT_PROCS;


struct ALARM_ACT {
	struct ALARM_ACT *pNext;
	char *ident;
	u_long status;
	timeval_t timeLastSet;
	char* proc;
/*
	ALARM_ACT_PROCS procs;
*/
};
typedef struct ALARM_ACT ALARM_ACT;
bool_t xdr_ALARM_ACT();


struct LOG_ACT {
	struct LOG_ACT *pNext;
	char *ident;
	char* proc;
};
typedef struct LOG_ACT LOG_ACT;
bool_t xdr_LOG_ACT();

/*
 * All the structures with REQ or req are requests from RPC client to the server
 * for various bits of info
 */

struct REQ_READ_REQ {
	CAMP_IF *pIF;
	u_long efn;
};
typedef struct REQ_READ_REQ REQ_READ_REQ;
bool_t xdr_REQ_READ_REQ();


struct REQ_READ {
	CAMP_IF *pIF;
	char *cmd;
	int cmd_len;
	char *buf;
	int buf_len;
	int read_len;
};
typedef struct REQ_READ REQ_READ;
bool_t xdr_REQ_READ();


struct REQ_WRITE {
	CAMP_IF *pIF;
	char *cmd;
	int cmd_len;
};
typedef struct REQ_WRITE REQ_WRITE;
bool_t xdr_REQ_WRITE();


struct REQ_SPEC {
	IO_TYPE type;
	union {
		REQ_READ_REQ readReq;
		REQ_READ read;
		REQ_WRITE write;
	} REQ_SPEC_u;
};
typedef struct REQ_SPEC REQ_SPEC;
bool_t xdr_REQ_SPEC();


typedef struct {
        int (*retProc)( struct REQ* );
} REQ_PROCS;


struct REQ {
	struct REQ *pNext;
        int pending;       /* is this one already processing */
	bool_t cancel;     /* flag to cancel next time around */
	timeval_t time;    /* time when it should next process */
	CAMP_VAR *pVar;
	REQ_PROCS procs;
	REQ_SPEC spec;
};
typedef struct REQ REQ;
bool_t xdr_REQ();


struct CAMP_SYS {
	char* hostname;
	char *prgName;
	SYS_DYNAMIC *pDyna;
	DIRENT *pDir;
	INS_TYPE *pInsTypes;
	INS_AVAIL *pInsAvail;
	ALARM_ACT *pAlarmActs;
	LOG_ACT *pLogActs;
	CAMP_IF_t *pIFTypes;
	REQ *pReqs;
};
typedef struct CAMP_SYS CAMP_SYS;
bool_t xdr_CAMP_SYS();


struct CMD_req {
	char *cmd;
};
typedef struct CMD_req CMD_req;
bool_t xdr_CMD_req();


struct FILE_req {
	int flag;
	char *filename;
};
typedef struct FILE_req FILE_req;
bool_t xdr_FILE_req();


struct INS_ADD_req {
	char *ident;
	char *typeIdent;
};
typedef struct INS_ADD_req INS_ADD_req;
bool_t xdr_INS_ADD_req();


struct DATA_req {
	char *path;
};
typedef struct DATA_req DATA_req;
bool_t xdr_DATA_req();


struct INS_LOCK_req {
	DATA_req dreq;
	bool_t flag;
};
typedef struct INS_LOCK_req INS_LOCK_req;
bool_t xdr_INS_LOCK_req();


struct INS_LINE_req {
	DATA_req dreq;
	bool_t flag;
};
typedef struct INS_LINE_req INS_LINE_req;
bool_t xdr_INS_LINE_req();


struct INS_FILE_req {
	DATA_req dreq;
	int flag;
	char *datFile;
};
typedef struct INS_FILE_req INS_FILE_req;
bool_t xdr_INS_FILE_req();


struct INS_LINK_req {
	DATA_req dreq;
	char *localIdent;
	char *ident;
};
typedef struct INS_LINK_req INS_LINK_req;
bool_t xdr_INS_LINK_req();


struct INS_IF_req {
	DATA_req dreq;
	CAMP_IF IF;
/*
	char *typeIdent;
	float		accessDelay;
	CAMP_IF_SPEC spec;
*/
};
typedef struct INS_IF_req INS_IF_req;
bool_t xdr_INS_IF_req();


struct INS_READ_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char *cmd_val;
	} cmd;
	u_int buf_len;
};
typedef struct INS_READ_req INS_READ_req;
bool_t xdr_INS_READ_req();


struct INS_WRITE_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char *cmd_val;
	} cmd;
};
typedef struct INS_WRITE_req INS_WRITE_req;
bool_t xdr_INS_WRITE_req();


struct INS_DUMP_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char *cmd_val;
	} cmd;
	struct {
		u_int fname_len;
		char *fname_val;
	} fname;
	struct {
		u_int skip_len;
		char *skip_val;
	} skip;
	u_int buf_len;
};
typedef struct INS_DUMP_req INS_DUMP_req;
bool_t xdr_INS_DUMP_req();


struct INS_UNDUMP_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char *cmd_val;
	} cmd;
	struct {
		u_int fname_len;
		char *fname_val;
	} fname;
	u_int buf_len;
};
typedef struct INS_UNDUMP_req INS_UNDUMP_req;
bool_t xdr_INS_UNDUMP_req();


struct DATA_SET_req {
	DATA_req dreq;
	CAMP_SPEC spec;
};
typedef struct DATA_SET_req DATA_SET_req;
bool_t xdr_DATA_SET_req();


struct DATA_POLL_req {
	DATA_req dreq;
	bool_t flag;
	float pollInterval;
};
typedef struct DATA_POLL_req DATA_POLL_req;
bool_t xdr_DATA_POLL_req();


struct DATA_ALARM_req {
	DATA_req dreq;
	bool_t flag;
	float tol;
	char *alarmAction;
};
typedef struct DATA_ALARM_req DATA_ALARM_req;
bool_t xdr_DATA_ALARM_req();


struct DATA_LOG_req {
	DATA_req dreq;
	bool_t flag;
	float logInterval;
	char *logAction;
};
typedef struct DATA_LOG_req DATA_LOG_req;
bool_t xdr_DATA_LOG_req();


struct DATA_GET_req {
	DATA_req dreq;
	u_long flag;
};
typedef struct DATA_GET_req DATA_GET_req;
bool_t xdr_DATA_GET_req();


/*
 * All structures with RES and res are result sets from server after an RPC 
 * request
 */

struct RES {
	int status;
	char *msg;
};
typedef struct RES RES;
bool_t xdr_RES();


struct CAMP_VAR_res {
	int status;
	char *msg;
	CAMP_VAR *pVar;
};
typedef struct CAMP_VAR_res CAMP_VAR_res;
bool_t xdr_CAMP_VAR_res();


struct CAMP_SYS_res {
	int status;
	char *msg;
	CAMP_SYS *pSys;
};
typedef struct CAMP_SYS_res CAMP_SYS_res;
bool_t xdr_CAMP_SYS_res();


struct SYS_DYNAMIC_res {
	int status;
	char *msg;
	SYS_DYNAMIC *pDyna;
};
typedef struct SYS_DYNAMIC_res SYS_DYNAMIC_res;
bool_t xdr_SYS_DYNAMIC_res();


struct DIR_res {
	int status;
	char *msg;
	DIRENT *pDir;
};
typedef struct DIR_res DIR_res;
bool_t xdr_DIR_res();


typedef bool_t (*PFB)( void );

#define _xdr_free(proc,objp)	if(objp!=NULL){xdr_free((xdrproc_t)proc,(char*)objp);free((void*)objp);objp=NULL;}


/*
 *  RPC definitions
 */
#define CAMP_SRV ((u_long)0x20000001)
#define CAMP_SRV_VERS ((u_long)13)
#define CAMPSRV_SYSRUNDOWN ((u_long)1)
extern RES *campsrv_sysrundown_13();
#define CAMPSRV_SYSUPDATE ((u_long)2)
extern RES *campsrv_sysupdate_13();
#define CAMPSRV_SYSLOAD ((u_long)3)
extern RES *campsrv_sysload_13();
#define CAMPSRV_SYSSAVE ((u_long)4)
extern RES *campsrv_syssave_13();
#define CAMPSRV_SYSREBOOT ((u_long)5)
extern RES *campsrv_sysreboot_13();
#define CAMPSRV_SYSGET ((u_long)51)
extern CAMP_SYS_res *campsrv_sysget_13();
#define CAMPSRV_SYSGETDYNA ((u_long)52)
extern SYS_DYNAMIC_res *campsrv_sysgetdyna_13();
#define CAMPSRV_SYSDIR ((u_long)53)
extern DIR_res *campsrv_sysdir_13();
#define CAMPSRV_INSADD ((u_long)101)
extern RES *campsrv_insadd_13();
#define CAMPSRV_INSDEL ((u_long)103)
extern RES *campsrv_insdel_13();
#define CAMPSRV_INSLOCK ((u_long)104)
extern RES *campsrv_inslock_13();
#define CAMPSRV_INSLINE ((u_long)105)
extern RES *campsrv_insline_13();
#define CAMPSRV_INSLOAD ((u_long)106)
extern RES *campsrv_insload_13();
#define CAMPSRV_INSSAVE ((u_long)107)
extern RES *campsrv_inssave_13();
#define CAMPSRV_INSIFSET ((u_long)109)
extern RES *campsrv_insifset_13();
#define CAMPSRV_INSIFREAD ((u_long)110)
extern RES *campsrv_insifread_13();
#define CAMPSRV_INSIFWRITE ((u_long)111)
extern RES *campsrv_insifwrite_13();
#define CAMPSRV_INSIFON ((u_long)112)
extern RES *campsrv_insifon_13();
#define CAMPSRV_INSIFOFF ((u_long)113)
extern RES *campsrv_insifoff_13();
#define CAMPSRV_INSIFDUMP ((u_long)114)
extern RES *campsrv_insifdump_13();
#define CAMPSRV_INSIFUNDUMP ((u_long)115)
extern RES *campsrv_insifundump_13();
#define CAMPSRV_VARSET ((u_long)201)
extern RES *campsrv_varset_13();
#define CAMPSRV_VARPOLL ((u_long)202)
extern RES *campsrv_varpoll_13();
#define CAMPSRV_VARALARM ((u_long)203)
extern RES *campsrv_varalarm_13();
#define CAMPSRV_VARLOG ((u_long)204)
extern RES *campsrv_varlog_13();
#define CAMPSRV_VARZERO ((u_long)205)
extern RES *campsrv_varzero_13();
#define CAMPSRV_VARDOSET ((u_long)206)
extern RES *campsrv_vardoset_13();
#define CAMPSRV_VARREAD ((u_long)207)
extern RES *campsrv_varread_13();
#define CAMPSRV_VARLNKSET ((u_long)208)
extern RES *campsrv_varlnkset_13();
#define CAMPSRV_VARGET ((u_long)251)
extern CAMP_VAR_res *campsrv_varget_13();
#define CAMPSRV_CMD ((u_long)301)
extern RES *campsrv_cmd_13();


/* 
 * camp_var_utils.c 
 */
char* get_current_ident( void );
void set_current_ident( char* ident );
void 	camp_varDoProc_recursive ( void (*proc )(CAMP_VAR *), CAMP_VAR *pVar_start );
CAMP_VAR* camp_varGetPHead( char* path );
CAMP_VAR **camp_varGetpp ( char *path_req );
CAMP_VAR *camp_varGetp ( char *path );
CAMP_VAR **camp_varGetTruePP ( char *path_req );
CAMP_VAR *camp_varGetTrueP ( char *path );
CAMP_CORE *camp_varGetpCore ( char *path );
caddr_t camp_varGetpSpec ( char *path );
CAMP_VAR *camp_varGetpIns ( char *path );
CAMP_VAR *varGetpIns ( CAMP_VAR *pVar );
bool_t 	camp_varExists ( char *path );
int 	camp_varGetStatus ( char *path , u_long *pStatus );
int 	camp_varGetPoll ( char *path , bool_t *pFlag , float *pInterval );
int 	camp_varGetAlarm ( char *path , bool_t *pFlag , char *alarmAction );
int 	camp_varGetLog ( char *path , bool_t *pFlag , char *action );
int 	camp_varGetValStr ( char *path , char *str );
int	camp_varGetTypeStr ( char* path, char* str );
int 	camp_varNumGetVal ( char *path , double *pVal );
int          varNumGetValStr ( CAMP_VAR *pVar , char *str );
bool_t          numGetValStr ( CAMP_VAR_TYPE varType , double val , char *str );
int 	camp_varNumGetUnits ( char *path , char* units );
int 	camp_varNumGetStats ( char *path , u_long *pN , double *pLow , double *pHi , double *pOffset , double *pSum , double *pSumSquares , double *pSumCubes );
int 	camp_varNumCalcStats ( char *path , double *pMean , double *pStdDev , double *pSkew );
bool_t 	camp_varNumTestTol( char *path , double val );
bool_t  camp_varNumTestAlert( char* path, double val );
int 	camp_varSelGetID ( char *path , u_char *pVal );
int 	camp_varSelGetLabel ( char *path , char *label );
int 	camp_varSelGetIDLabel ( char *path , u_char val , char *label );
int          varSelGetIDLabel ( CAMP_VAR *pVar , u_char val , char *label );
int 	camp_varSelGetLabelID ( char *path , char *label , u_char *pVal );
int          varSelGetLabelID ( CAMP_VAR *pVar , char *label , u_char *pVal );
int 	camp_varStrGetVal ( char *path , char *val );
int 	camp_varArrGetVal ( char *path , caddr_t pVal );
int 	camp_varLnkGetVal ( char *path , char *val );


/* 
 * camp_sys_utils.c 
 */
DIRENT *camp_sysGetpDirEnt ( char *filename );
INS_TYPE *camp_sysGetpInsType ( char *ident );
ALARM_ACT *camp_sysGetpAlarmAct ( char *ident );
LOG_ACT *camp_sysGetpLogAct ( char *ident );
CAMP_IF_t *camp_sysGetpIFType ( char *ident );


/* 
 * camp_ins_utils.c 
 */
int camp_insDel ( char *path );
void camp_insDelAll ( void );
int camp_insGetLock ( char *path , bool_t *pFlag );
int camp_insGetLine ( char *path , bool_t *pFlag );
int camp_insGetFile ( char *path , char *filename );
int camp_insGetTypeIdent ( char *path , char *typeIdent );
int camp_insGetIfTypeIdent ( char *path , char *typeIdent );


/* 
 * camp_if_utils.c 
 */
CAMP_IF_t *camp_ifGetpIF_t ( char *ident );
CAMP_IF_TYPE camp_getIfTypeID( char* typeIdent );
char* camp_getIfTypeIdent( CAMP_IF_TYPE typeId );
char* camp_getIfRs232Port( char* defn, char* str );
int camp_getIfRs232Baud( char* defn );
int camp_getIfRs232Data( char* defn );
char* camp_getIfRs232Parity( char* defn, char* str );
int camp_getIfRs232Stop( char* defn );
char* camp_getIfRs232ReadTerm( char* defn, char* str ) ;
char* camp_getIfRs232WriteTerm( char* defn, char* str );
int camp_getIfRs232Timeout( char* defn );
int camp_getIfGpibAddr( char* defn );
char* camp_getIfGpibReadTerm( char* defn, char* str );
char* camp_getIfGpibWriteTerm( char* defn, char* str );
int camp_getIfCamacB( char* defn );
int camp_getIfCamacC( char* defn );
int camp_getIfCamacN( char* defn );
int camp_getIfIndpakSlot( char* defn );
char* camp_getIfIndpakType( char* defn, char* str );
int camp_getIfIndpakChannel( char* defn );
int camp_getIfVmeBase( char* defn );
char* camp_getIfTcpipAddr( char* defn, char* str );
int camp_getIfTcpipPort( char* defn );
char* camp_getIfTcpipReadTerm( char* defn, char* str );
char* camp_getIfTcpipWriteTerm( char* defn, char* str );
float camp_getIfTcpipTimeout( char* defn );
CAMP_VAR* camp_rs232PortInUse( CAMP_INSTRUMENT* pIns );

/* 
 * camp_path_utils.c 
 */
bool_t camp_pathAtTop ( char *path );
void camp_pathInit ( char *path );
char *camp_pathExpand ( char *path , char *expansion );
bool_t camp_pathCompare ( char *path1 , char *path2 );
char *camp_pathGetFirst ( const char *path , char *ident );
char *camp_pathGetLast ( const char *path , char *ident );
char *camp_pathGetNext ( const char *path , const char *path_curr , char *ident_next );
char *camp_pathDownNext ( const char *path , char *path_curr );
char *camp_pathDown ( char *path , const char *ident );
char *camp_pathUp ( char *path );


/* 
 * camp_msg_utils.c 
 */
char* msg_getHelpString( MSG_TYPE type );
char* camp_getMsg( void );
bool_t camp_isMsg ( void );
void camp_setMsg ( char *fmt , ...);
void camp_appendMsg ( char *fmt , ...);
#ifdef VMS
void camp_appendVMSMsg ( u_long numMessage );
#endif /* VMS */


/* 
 * camp_token_defs.h
 */

/*
 * kinds of tokens 
 */
enum tok_kind {
	TOK_SYS_UPDATE,
	TOK_SYS_RUNDOWN,
	TOK_SYS_REBOOT,
	TOK_SYS_LOAD, 
	TOK_SYS_SAVE, 
	TOK_SYS_GETINSTYPES, 
	TOK_SYS_GETALARMACTS,
	TOK_SYS_GETLOGACTS,
	TOK_SYS_GETIFTYPES,
	TOK_SYS_GETIFCONF,
	TOK_SYS_ADDALARMACT,
	TOK_SYS_ADDLOGACT,
	TOK_SYS_ADDIFTYPE,
	TOK_SYS_ADDINSTYPE,
	TOK_SYS_ADDINSAVAIL,
	TOK_MSG, 
	TOK_INS_ADD, 
	TOK_INS_DEL, 
	TOK_INS_SET, 
	TOK_INS_LOAD, 
	TOK_INS_SAVE, 
	TOK_INS_IF_ON, 
	TOK_INS_IF_OFF, 
	TOK_INS_IF_READ, 
	TOK_INS_IF_WRITE, 
	TOK_INS_IF_READ_VERIFY, 
	TOK_INS_IF_WRITE_VERIFY, 
	TOK_INS_IF_DUMP, 
	TOK_INS_IF_UNDUMP, 
	TOK_VAR_SET, 
	TOK_VAR_DOSET, 
	TOK_VAR_READ, 
	TOK_VAR_GET, 
	TOK_LNK_SET,
	TOK_INT,
	TOK_FLOAT,
	TOK_SELECTION,
	TOK_STRING,
	TOK_ARRAY,
	TOK_STRUCTURE,
	TOK_INSTRUMENT,
	TOK_LINK,
	TOK_SELECTIONS,
        TOK_ARRAYDEF,
	TOK_VARTYPE,
	TOK_INSTYPE,
	TOK_TITLE,
	TOK_HELP,
	TOK_SHOW_ATTR,
	TOK_SET_ATTR,
	TOK_READ_ATTR,
	TOK_POLL_ATTR,
	TOK_LOG_ATTR,
	TOK_ALARM_ATTR,
	TOK_SHOW_FLAG,
	TOK_SET_FLAG,
	TOK_READ_FLAG,
	TOK_POLL_FLAG,
	TOK_POLL_INTERVAL,
	TOK_LOG_FLAG,
	TOK_LOG_ACTION,
	TOK_ALARM_FLAG,
	TOK_ALARM_TOL,
	TOK_ALARM_TOLTYPE,
	TOK_ALARM_ACTION,
	TOK_UNITS,
	TOK_VALUE_FLAG,
	TOK_ZERO_FLAG,
	TOK_ALERT_FLAG,
	TOK_MSG_FLAG,
	TOK_ON, 
	TOK_OFF, 
	TOK_END, 
	TOK_OPT_DEF, 
	TOK_OPT_IF_SET, 
	TOK_OPT_IF_MOD, 
	TOK_OPT_IF, 
	TOK_OPT_LOCK, 
	TOK_OPT_LINE, 
	TOK_OPT_ONLINE, 
	TOK_OPT_OFFLINE, 
	TOK_NONE, 
	TOK_ODD, 
	TOK_EVEN, 
	TOK_CR, 
	TOK_LF, 
	TOK_CRLF, 
	TOK_LFCR, 
	TOK_OPT_READPROC,
	TOK_OPT_WRITEPROC,
	TOK_OPT_SETPROC,
	TOK_OPT_DRIVERTYPE,
	TOK_OPT_INITPROC,
	TOK_OPT_ONLINEPROC,
	TOK_OPT_OFFLINEPROC,
	TOK_OPT_DELETEPROC,
	TOK_EOF
};
typedef enum tok_kind TOK_KIND;

/*
 * a token 
 */
struct token {
	TOK_KIND kind;
	char* str;
};
typedef struct token TOKEN;

typedef struct token_item {
    TOKEN* pTok;
    struct token_item* pNext;
} TOKEN_ITEM;


/* 
 * camp_token.c
 */
char *toktostr ( TOK_KIND kind );
bool_t findToken ( char *str , TOKEN *ptok );

/* 
 *  in camp_api_proc.c, 
 *  or defined explicitly if not using camp_api_proc.c
 */
extern CAMP_SYS* pSys;
extern CAMP_VAR* pVarList;
extern int camp_debug;

/*
 *  16-Dec-1999  TW  Define that overrides all camp_debug conditionals
 *                   (makes code smaller/faster)
 */
#if !defined(CAMP_DEBUG)
#define CAMP_DEBUG 0
#endif

#endif
