/*
 *  Name:       camp_cmd.c
 *
 *  Purpose:    Main program for camp_cmd executable
 *              Takes command line input and sends via an RPC call to the
 *              CAMP server.  The commands are interpreted in the CAMP
 *              server's Tcl interpreter.
 *
 *              Tcl commands may operate on all CAMP instruments and
 *              variables, but you will not have access to global Tcl variables
 *              that are within a CAMP instrument driver from this command.
 *
 *              Note though that the driver's interface and variable
 *              Tcl proc's do have access to internal Tcl global variables
 *              when initiated by this client program, as they would when
 *              initiated by the camp_cui or otherwise.
 *
 *              Is that confusing?
 *
 *              Because of this limitation, it is advised that all important
 *              variables be made CAMP variables within the CAMP instrument
 *              driver.  That way, they are always accessible from all
 *              CAMP client programs.
 *
 *  Revision history:
 *    16-Dec-1999  TW  Use CAMP_RPC_CLNT_TIMEOUT with clntInit 
 *
 */

#include <stdio.h>
#include "camp_clnt.h"

#ifdef VMS
#define failure_status (CAMP_FAILURE | 0x10000000)
#else /* !VMS */
#define failure_status CAMP_FAILURE
#endif /* VMS */

#define usage() \
        fprintf( stderr, "usage: %s [-node <server>] \"<command>\"\n", argv[0] );

#ifdef VXWORKS
int camp_cmd( char * aServerName, char * cmd)
#else
int main( int argc, char* argv[] )
#endif
{
  int status;
  char serverName[LEN_NODENAME+1];
  char* msg;
  
#ifdef VXWORKS
  if (aServerName)
    strcpy(serverName, aServerName);
#else
  char* cmd;
  if( argc < 2 )
  {
    usage();
    exit( failure_status );
  }
  
  if( argc > 2 )
  {
    if( streq( argv[1], "-node" ) )
    {
      if( argc < 4 )
      {
	usage();
	exit( failure_status );
      }
      
      strcpy( serverName, argv[2] );
      cmd = argv[3];
    }
    else
    {
      usage();
      exit( failure_status );
    }
  }
  else
  {
    char* host;
    
    /*
     *  Host is CAMP_HOST environment variable
     *  or else local host if undefined
     */
    host = getenv( "CAMP_HOST" );
    if( host == NULL )
    {
      gethostname( serverName, LEN_NODENAME );
    }
    else
    {
      strcpy( serverName, host );
    }
    
    cmd = argv[1];
  }
#endif
  /*
   *  printf("server:%s\n", serverName);
   *  printf("   cmd:%s\n", cmd);
   */
  status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );

  if( _success( status ) )
  {
    status = campSrv_cmd( cmd );
  }
  
  msg = camp_getMsg();

#ifdef VMS
  /*
   *  Set local (1) and global (2) symbols
   */
  lib_set_symbol( "CAMP_RESULT", msg, 1 );
  lib_set_symbol( "CAMP_RESULT", msg, 2 );

  if( isatty( fileno( stdin ) ) ) 
  {
    if( *msg != '\0' ) puts( msg );
  }
  
  exit( status | 0x10000000 );

#else  /* not VMS */

  /*
   *  Consider a corresponding setenv() here for Unix if desired, but
   *  it is generally useless because env propagates downward only.
   */

  if( *msg != '\0' ) puts( msg );

#ifdef VXWORKS
  return 0;
#else
  if( _success( status ) )
    exit( 0 );
  else
    exit( status );
#endif
#endif /* VMS */

} 


