/*
 *  Name:       camp_ini_write.c
 *
 *  Purpose:    Write an instrument initialization file based on the current
 *              instrument state.
 *              This file can be used to return the instrument to a previous
 *              state.
 *
 *  Called by:  campsrv_inssave_13 (in camp_srv_proc.c) calls campIni_write
 * 
 *  $Log$
 *  Revision 1.6  2005/11/24 02:14:32  asnd
 *  Write explicit -offline in ini file (when appropriate)
 *
 *  Revision 1.5  2005/07/06 10:59:48  asnd
 *  More save/restore instrument modifications
 *
 *  Revision 1.4  2005/07/06 03:51:24  asnd
 *  Write portable instrument ini files
 *
 *  Revision 1.3  2004/12/18 02:58:16  asnd
 *  Enable save and restore of variables' poll (on) status
 *
 *  Revision 1.2  2001/02/10 04:13:31  asnd
 *  Change order of instrument ini file, so the instrument is put online
 *  after all variables are set (varDoset).  The old way would force bad
 *  values on instruments, even when a variable was read by the onlineProc.
 *
 *
 *
 *  Revision history:
 *              05-Feb-2001   DJA  Change write ordering.
 *
 */

#include "camp_srv.h"

static char curr_path[LEN_PATH+1];


/*
 *  campIni_write(),        
 */
int
campIni_write( char* filename, CAMP_VAR* pVar )
{
    if( !openOutput( filename ) ) 
        return( CAMP_INVAL_FILE );

    /* 
     *  Init things
     */
    reinitialize( filename, NULL, NULL );

    camp_pathInit( curr_path );
    camp_pathDown( curr_path, pVar->core.ident );

    iniWrite_var( pVar );

    print( "return -code ok\n" );

    closeOutput();

    return( CAMP_SUCCESS );
}


/*
 *  Name:       iniWrite_stat
 *
 *  Purpose:    Write static (unchanging) information about a variable to
 *              to initialization file.  In effect, this is just the
 *              Tcl command varDoSet followed by the path string, i.e., 
 *
 *                 varDoSet <variablePath>
 *
 *              This will be followed by a call to iniWrite_dyna that will
 *              add the parameters to varDoSet that set the dynamic state.
 *
 *  Called by:  iniWrite_var
 * 
 *  Inputs:     Pointer to CAMP variable core structure
 *
 *  Postconditions:
 *              Call iniWrite_stat
 *
 */
void 
iniWrite_stat( CAMP_CORE* pCore )
{
/*  13-Nov-1996  TW  Changed TOK_VAR_SET to TOK_VAR_DOSET
                     Done this to stop redundant setting problems
                     But may need to reconsider

    print_tab( "%s ", toktostr( TOK_VAR_SET ) );
*/
    print_tab( "%s ", toktostr( TOK_VAR_DOSET ) );
    print( "%s ", curr_path );
}


/*
 *  Name:       iniWrite_dyna
 *
 *  Purpose:    Write dynamic (changing) information about a variable to
 *              to initialization file.  This information is general to
 *              all variables,
 *
 *  Called by:  iniWrite_var
 * 
 *  Inputs:     Pointer to CAMP variable core structure
 *
 *  Postconditions:
 *              Call the routine that will write the dynamic information
 *              that is specific to each variable.
 *
 */
void 
iniWrite_dyna( CAMP_CORE* pCore )
{
    /*
     *  Polling info
     */
/*
 *  Comment here was: 
 *	This can cause major problems
 *	Sorry, it'll have to be done individually
 *  and the writing of "-p on" was commented-out. 
 *
 *  But what problems did it cause?  DJA does not see a problem on preliminary
 *  tests.  I guess that there may have been a difficulty when the Camp server
 *  reboots and poll actions may have happened before an instrument was properly
 *  initialized.  If so, the instrument mutex should now prevent those problems.
 *  DJA restores the writing of "-p on", but only in the case where the poll
 *  interval is positive (Dec 2004).
 */
    if( (pCore->status & CAMP_VAR_ATTR_POLL) && (pCore->pollInterval > 0.0) ) 
    {
        print( "%s %s ", toktostr( TOK_POLL_FLAG ), toktostr( TOK_ON ) );
    }
/* */

    if( pCore->pollInterval > 0.0 ) 
    {
        print( "%s %f ", toktostr( TOK_POLL_INTERVAL ), pCore->pollInterval );
    }

    /*
     *  Logging info
     */
    if( pCore->status & CAMP_VAR_ATTR_LOG ) 
    {
        print( "%s %s ", toktostr( TOK_LOG_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->logAction[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_LOG_ACTION ), pCore->logAction );
    }

    /*
     *  Alarm info
     */
    if( pCore->status & CAMP_VAR_ATTR_ALARM ) 
    {
        print( "%s %s ", toktostr( TOK_ALARM_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->alarmAction[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_ALARM_ACTION ), pCore->alarmAction );
    }
}


bool_t 
ini_check_dyna( CAMP_CORE* pCore )
{
    if( ( pCore->status & CAMP_VAR_ATTR_POLL ) ||
        ( pCore->pollInterval > 0.0 ) ||
        ( pCore->status & CAMP_VAR_ATTR_LOG ) ||
        ( pCore->logAction[0] != '\0' ) ||
        ( pCore->status & CAMP_VAR_ATTR_ALARM ) ||
        ( pCore->alarmAction[0] != '\0' ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_num( CAMP_VAR* pVar )
{
    if( ( pVar->spec.CAMP_SPEC_u.pNum->tol >= 0.0 ) ||
        ( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
          ( pVar->core.status & CAMP_VAR_ATTR_SET ) ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_sel( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_str( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_arr( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_lnk( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


/*
 *  Name:       iniWrite_var
 *
 *  Purpose:    Write all information about a variable to
 *              to initialization file.
 *
 *              Since the calls to iniWrite_Instrument and iniWrite_Structure
 *              traverse the linked list for all variables contained within
 *              the grouping, this routine effectively writes out the whole
 *              tree of variables starting with the input variable.  Normally
 *              the input variable is a CAMP instrument variable so that all
 *              information for that instrument is written.
 *
 *  Called by:  campIni_write, iniWrite_Structure, iniWrite_Instrument
 * 
 *  Inputs:     Pointer to CAMP variable structure
 *
 */
void 
iniWrite_var( CAMP_VAR* pVar )
{
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        if( ini_check_dyna( &pVar->core ) || ini_check_num( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_Numeric( pVar );
        }
        break;
      case CAMP_VAR_TYPE_SELECTION:
        if( ini_check_dyna( &pVar->core ) || ini_check_sel( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            iniWrite_Selection( pVar );
        }
        break;
      case CAMP_VAR_TYPE_STRING:
        if( ini_check_dyna( &pVar->core ) || ini_check_str( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_String( pVar );
        }
        break;
      case CAMP_VAR_TYPE_ARRAY:
        if( ini_check_dyna( &pVar->core ) || ini_check_arr( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            iniWrite_Array( pVar );
        }
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        iniWrite_Structure( pVar );
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        iniWrite_Instrument( pVar );
        break;
      case CAMP_VAR_TYPE_LINK:
        if( ini_check_dyna( &pVar->core ) || ini_check_lnk( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_Link( pVar );
        }
        break;
    }
}


void 
iniWrite_Selection( CAMP_VAR* pVar )
{
    CAMP_SELECTION* pSel = pVar->spec.CAMP_SPEC_u.pSel;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %d ", toktostr( TOK_VALUE_FLAG ), pSel->val );
    }
    print( "\n" );
}


void 
iniWrite_Array( CAMP_VAR* pVar )
{
    CAMP_ARRAY* pArr = pVar->spec.CAMP_SPEC_u.pArr;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        /* Not done */
    }
    print( "\n" );
}


void 
iniWrite_Structure( CAMP_VAR* pStcVar )
{
    CAMP_VAR* pVar;

    add_tab();

    for( pVar = pStcVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, pVar->core.ident );
        iniWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}

/*
 * 05-Feb-2001   DJA  Write instrument interface cfg last.  This allows the 
 *                    onlineProc to read the actual values of variables, 
 *                    instead of having them restored from an old ini file.
 */
void 
iniWrite_Instrument( CAMP_VAR* pInsVar )
{
    CAMP_INSTRUMENT* pIns = pInsVar->spec.CAMP_SPEC_u.pIns;
    CAMP_VAR* pVar;
    char* pTypeIdent;
    char inst_path[LEN_PATH+1];

    strcpy( inst_path, curr_path );

    pTypeIdent = (pInsVar->spec.CAMP_SPEC_u.pIns)->typeIdent;

    if( pTypeIdent ) 
    {
      print_tab( "# instrument %s, of type %s\n", inst_path+1, pTypeIdent );
    }

    add_tab();

    for( pVar = pInsVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        strcpy( curr_path, "/~" );
        camp_pathDown( curr_path, pVar->core.ident );
        iniWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();

    if( pIns->pIF != NULL )
    {
        print_tab( "%s %s ", toktostr( TOK_INS_SET ), curr_path );
        cfgWrite_if( pIns->pIF );
        if( pIns->pIF->status & CAMP_IF_ONLINE )
            print( "%s", toktostr( TOK_OPT_ONLINE ) );
        else 
            print( "%s", toktostr( TOK_OPT_OFFLINE ) );

        print( "\n" );
    }

    strcpy( curr_path, inst_path );

}

