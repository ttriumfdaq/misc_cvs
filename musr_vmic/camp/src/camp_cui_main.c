/*
 *  Name:       camp_cui_main.c
 *
 *  Purpose:    Main program for the CAMP CUI
 *
 *  Inputs:     Optional command line parameters:
 *                   -node <host>      - Camp server host name
 *                   -update <intsec>  - Cycle time for refreshes
 *                   -pause <floatsec> - Pause time for transient messages     
 *                   -d <debugLevel>
 *                   -noansi           - should try to use non-graphical
 *                                       characters for screen line drawing
 *
 *  $Log$
 *  Revision 1.9  2009/04/08 01:57:01  asnd
 *  Symbolic names for clarity (enum didn't compile)
 *
 *  Revision 1.8  2008/04/08 06:53:26  asnd
 *  Avoid reading variables twice with page-updates (space-bar)
 *
 *  Revision 1.7  2005/07/13 04:13:30  asnd
 *  Command-line options to adjust behaviour
 *
 *  Revision 1.6  2004/01/28 03:13:26  asnd
 *  Add VME interface type. Make alert's BEEP more periodic.
 *
 *  Revision 1.5  2001/12/19 18:05:09  asnd
 *  Make "alerts" window (covering key window) to display alarm conditions.
 *
 *  Revision 1.4  2001/04/20 02:36:51  asnd
 *  Fix bug with stuck "server-down" message window.
 *
 *  Revision 1.3  2001/01/15 06:37:11  asnd
 *  Set X-window title; message for failure to initialize screen.
 *
 *
 *  Revision history:
 *    16-Dec-1999  TW  Use CAMP_RPC_CLNT_TIMEOUT with clntInit 
 *    15-Feb-2000  DA  Make updateData() follow links
 *    06-Oct-2000  DA  Window resizing handler
 *    14-Dec-2000  TW  waitForServer doesn't show popup window for first try
 *                     at reconnect (new parameter msgFlag)
 *    05-Jan-2001  DA  Set window title (X)
 *    19-Apr-2001  DA  Fix server-down message flag (cv TW 14-Dec-2000)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include "camp_cui.h"
#ifdef VMS
#include ssdef
#include iodef
#include ttdef
#include tt2def
#include dcdef
#include dvidef
#endif /* VMS */

#ifdef linux
#include <ctype.h>
#include <signal.h>

static void         winAdjSize(int sig);
#endif /* Linux */

#ifdef VMS
struct qiosb {
  short status;
  short d1,d2,d3;
};
#endif /* VMS */

char* prog_name = "CAMP CUI v1.7";

char   currViewPath[LEN_PATH+1];
bool_t isCursesInit = FALSE;
int    currDisplay = _DispTree;
long   poll_interval = POLL_INTERVAL;
unsigned long  msgPause_us = PAUSE_US;
bool_t isANSI = TRUE;

static char serverName[LEN_NODENAME+1];

extern enum clnt_stat camp_clnt_stat;
extern char currVarPath[LEN_PATH+1];


int  screenHeight, screenWidth; /* extern in camp_cui.h */
int  newScreenH, newScreenW;

bool_t noisy = TRUE;
bool_t timedUpdate = TRUE;

#define _handleServerReturn(stat)   \
        if(_success(stat)){addMessage("done");deleteMessage();}\
        else{addMessage("\n  Error: ");addMessage(camp_getMsg());confirmMessage();\
             deleteMessage();}

void usage( char* name );


int 
main( int argc, char* argv[] )
{
    int status;
    int i, j;
#ifdef VMS
    char term[L_ctermid];
    s_dsc dsc_term;
    ITEM_LIST item_list;
    u_long result;
    struct qiosb port_qiosb;
    struct device_mode deviceMode;
    short ioChannel;
#endif /* VMS */

    strcpy( serverName, "" );
    isANSI = TRUE;
    camp_debug = FALSE;

#ifdef VMS
    ctermid( term );
    setdsctostr( &dsc_term, term );
    item_list.buf_len = sizeof( u_long );
    item_list.item_code = DVI$_TT_ANSICRT;
    item_list.buf_addr = &result;
    result = 1;
    sys$getdviw( 0, 0, &dsc_term, &item_list, 0, 0, 0, 0 );
    if( !result )
    {
        isANSI = FALSE;
    }

    /*
     *  assign the port
     */
    status = sys$assign( &dsc_term, &ioChannel, 0, 0, 0 );
    if( _success( status ) )
      {
        bzero( &deviceMode, sizeof( struct device_mode ) );
	
	/*
	 *  Get terminal characteristics
	 */
        status = sys$qiow( 0,                   /* efn */
			  ioChannel,           /* chan */
			  IO$_SENSEMODE,         /* func */
			  &port_qiosb,         /* iosb */
			  0, 0,                /* astadr, astprm */
			  &deviceMode,         /* P1 - charac. buffer */
			  8,                   /* P2 - len charac. buffer */
			  0,                   /* P3 - baud */
			  0,                   /* P4 - crlf_fill */
			  0,                   /* P5 - parity */
			  0 );                 /* P6 */
	if( _failure( status ) )
	  {
	    printf( "Failed to get terminal settings, status %08x\n", status );
	  }
	else 
	  {
	    /*
	     *  Set terminal characteristics
	     *  Do not receive broadcasts
	     */
	    deviceMode.u1.s1.basic_chars |= TT$M_NOBRDCST;
	    /*
	     *  set port mode
	     *  note:  P2 (size of set mode buffer ) is 8 because this
	     *         is the size without extended characteristics which
	     *         are not setable without LOG_IO or PHY_IO
	     *         (don't need any anyway)
	     */
	    status = sys$qiow( 0,                   /* efn */
			      ioChannel,           /* chan */
			      IO$_SETMODE,         /* func */
			      &port_qiosb,         /* iosb */
			      0, 0,                /* astadr, astprm */
			      &deviceMode,         /* P1 - char. buffer */
			      8,                   /* P2 - len char. buffer */
			      TT$C_BAUD_9600,      /* P3 - baud */
			      0,                   /* P4 - crlf_fill */
			      TT$M_ALTRPAR,        /* P5 - parity */
			      0 );                 /* P6 */
	    if( _failure( status ) )
	      {
		printf( "Failed to set terminal settings, status %08x\n", 
		       status );
	      }
	  }
      }
    else
      {
	printf( "Could not assign terminal, status %08x\n", status );
      }
#endif /* VMS */

#ifdef linux
    (void) signal(SIGWINCH, winAdjSize); 
#endif /* Linux */


    for( i = 1; i < argc; i++ )
    {
        if( streq( argv[i], "-node" ) )
        {
            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }
            strcpy( serverName, argv[i] );
        }
        else if( streq( argv[i], "-d" ) )
        {
            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }
            camp_debug = atoi( argv[i] );
        }
        else if( streq( argv[i], "-pause" ) )
        {
            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }
            j = (int) (1000000.0*atof( argv[i] ) + 10);
            if ( j >= 0 && j <= 10000000)
            {
              msgPause_us = j;
            }
        }
        else if( streq( argv[i], "-update" ) )
        {
            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }
            j = atoi( argv[i] );
            if ( j > 0 && j < 30 )
            {
              poll_interval = j;
            }
        }
        else if( streq( argv[i], "-noansi" ) )
        {
            isANSI = FALSE;
        }
        else
        {
            usage( argv[0] );
            exit( CAMP_FAILURE );
        }
    }

    if( streq( serverName, "" ) )
    {
      char* host;

      /*
       *  Host is CAMP_HOST environment variable
       *  or else local host (VMS only) if undefined
       */
      host = getenv( "CAMP_HOST" );
      if( host == NULL )
      {
#ifdef linux
        /* linux has client only - no server available */
	printf("Linux host has no server available;\n");
	printf("Specify Camp server node on command line or setenv CAMP_HOST node \n");
	usage( argv[0] );
	exit( CAMP_FAILURE );
#else
        /* attempt to connect to a camp server on this computer */
       	gethostname( serverName, LEN_NODENAME );
#endif
      }
      else
      {
	strcpy( serverName, host );
      }
    }

    /* 
     *  Set terminal and icon names, presuming X window, by printing
     *  escape sequences.  Erase the line just in case it is an ordinary
     *  terminal.
     */
    if( isANSI )
    { 
      printf( "\033]2;Camp CUI for %s\07\033\\\033]21;Camp CUI for %s\033\\\033[2K\r",
	      serverName, serverName ); 
    }

    /*
     *  Initialize everything (the order does matter)
     */
    camp_setMsg( "" );

    atexit( camp_cui_rundown );

    status = initScreen();
    if( _failure( status ) )
    {
      endwin();
      printf( "Failed to initialize screen!\n" );
      exit( status );
    }

    camp_pathInit( currViewPath );
    camp_pathInit( currVarPath );
    camp_setMsg( "" );

    status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );
    if( _failure( status ) )
    {
        waitForServer( 1 );
    }
    camp_setMsg( "" );

    /*
     *  Initialize data and data display
     */
    printStatus();
    updateAndDisplayData();

    /*
     *  Enter user interface loop
     */
/*
    getMenuSelection();
*/
    varPicker();
}


void
usage( char* name )
{
    fprintf( stderr, "usage: %s [-node <server>] [-noansi] [-update <sec>] [-pause <sec>]\n",
                           name );
}


u_long 
initScreen( void )
{
    u_long status;

    if( ! isCursesInit ) 
    {
        isCursesInit = TRUE;
        initscr();
        /* remember screen geometry to detect later changes */
        screenHeight = LINES;
        screenWidth  = COLS;
        newScreenH = screenHeight;
        newScreenW = screenWidth;
    }
        
    noecho();
    cbreak();  /* Meaningful for UNIX only */
    nonl();

    scrollok( stdscr, FALSE );
    attroff( A_BLINK | A_BOLD | A_REVERSE | A_UNDERLINE );
/*
    status = initMessageDisplay();
    if( _failure( status ) ) return( status );
*/
    status = initStatusDisplay();
    if( _failure( status ) ) return( status );
/*
    status = initMenuDisplay();
    if( _failure( status ) ) return( status );
*/
    status = initDataDisplay();
    if( _failure( status ) ) return( status );

    status = initVarInfoWin();
    if( _failure( status ) ) return( status );

    status = initKeyWin();
    if( _failure( status ) ) return( status );

    if( currDisplay == _DispVar )
    {
	status = initVarWin();
	if( _failure( status ) ) return( status );
    }

    return( CAMP_SUCCESS );
}



void
refreshScreen( void )
{
    u_long status;

#ifdef linux

    if( (newScreenH != screenHeight) || (newScreenW != screenWidth) ) 
    {
	deleteDataDisplay();
	deleteHelpWin();
	deleteAlertWin();
	deleteKeyWin();
	deleteStatusDisplay();
	deleteVarInfoWin();
	deleteVarWin();

        screenHeight = newScreenH;
        screenWidth = newScreenW;
        resizeterm( screenHeight, screenWidth );

	status = initScreen();
	if( _failure( status ) ) 
        {
          endwin();
          printf( "\n\nFailed to \ninitialize \nnew screen!\n" );
          exit( status );
        }

	printStatus();
	updateDisplay();
    }

#endif /* Linux */

/*
    wclear( stdscr );
*/
/*
    touchwin( stdscr );
    wrefresh( stdscr );
*/
    touchStatusDisplay(); 
    refreshStatusDisplay();

    touchDataDisplay();
    refreshDataDisplay();

    touchVarInfoWin();
    refreshVarInfoWin();

    touchKeyWin();
    refreshKeyWin();

    touchAlertWin();
    refreshAlertWin();
/*
    touchMenuDisplay();
    refreshMenuDisplay();
*/
    touchVarWin();
    refreshVarWin();
/*
    touchMessageDisplay();
    refreshMessageDisplay();
*/

    /*  Any temporary message window, 18x60 center of screen */
    touchMessage();
    refreshMessage();

    /*  I found the pasteboard ID in curses.h */
/*  don't need this now, figured out how to do it portably above
        status = smg$repaint_screen( &stdpb->_id );
*/

    /*#else
        wrefresh( curscr );
      #endif
    */
}


/* 
 *  Procedure to redisplay windows that had been covered by another window.
 *  This is written so only one screen update is performed for the several
 *  windows.  VAXcurses does this automatically.
 */

void 
uncoverWindows( void )
{
#ifndef VMS
    uncoverStatusDisplay(); 
    if( currDisplay == _DispTree )
    {
      uncoverDataDisplay();
      uncoverVarInfoWin();
      uncoverKeyWin();
      uncoverAlertWin();
    }
    else if( currDisplay == _DispVar )
    {
      uncoverVarWin();
    }
    doupdate();
#endif
}


void 
camp_cui_rundown( void )
{
      /*----------------------------*/
     /*   Clean up                 */
    /*----------------------------*/
    if( isCursesInit ) endwin();
    camp_clntEnd();
}


int
updateAndDisplayData( void )
{
    int status;

    status = updateData();
    if( status == CAMP_FAIL_RPC ) return( status );

    updateDisplay();

    if( camp_debug > 2 )
    {
        int code = 0;
/* SD VMS system call */
#ifdef VMS
        lib$show_vm( &code, NULL, 0 );
#endif
    }

    return( CAMP_SUCCESS );
}


int
checkRPCstatus( void )
{
    if( camp_clnt_stat == RPC_SUCCESS )
    {
        return( CAMP_SUCCESS );
    }
/*
    else if( camp_clnt_stat == RPC_TIMEDOUT )
    {
        return;
    }
*/
    else
    {
      waitForServer( 1 );
      return( CAMP_FAIL_RPC );
    }
}


/*
 *  waitForServer()
 *
 *  Called by:
 *     update  (camp_cui_keyboard.c)
 *     _doRPC inline (camp_cui_menucb.c)
 *     main, checkRPCstatus   (camp_cui_main.c)
 *
 *  19-Dec-2000  TW  New msgFlag parameter:
 *                       msgFlag = 1  ->  normal operation
 *                       msgFlag = 0  ->  don't show popup window on first
 *                                        try at reconnecting
 */
void
waitForServer( int msgFlag )
{
    int tries = 0;
    int status;
    char str[64];

    camp_clntEnd();

    camp_pathInit( currViewPath );
    camp_pathInit( currVarPath );
    updateDisplay();

    sprintf( str, "CAMP Server error (clnt_stat=%d)", camp_clnt_stat );

    if( msgFlag )
    {
      startMessage( str, "CAMP Server is down, attempting to connect..." );
    }
    
    for( ;; )
    {
        tries++;
      
        /*  TW  16-Dec-1999  Use CAMP_RPC_CLNT_TIMEOUT with clntInit */
        status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );
        if( _success( status ) ) break;

        if( msgFlag || (tries > 1) ) {
          addMessage( "\nCAMP Server is down, attempting to connect..." );
        } else {
          startMessage( str, "CAMP Server is down, attempting to connect..." );
        }
          
        camp_clntEnd();

        sleep( 3 );
    }

    if( msgFlag || ( tries > 1 ) )
    {
        addMessage( "\nConnected to CAMP Server" );
        sleep( 1 );
        deleteMessage();
    }
     
    updateData();
    updateDisplay();
}

int
updateData( void )
{
    /*
     * request updated data for all variables at current level.
     * Feb 2000, DA:  request updates on linked variables too
     */
    int status;
    CAMP_VAR* pVar;

    do {
      status = camp_clntUpdate();
    } while( _failure( checkRPCstatus() ) || _failure( status ) );

    /*
     * If current path no longer exists, go to top
     */
    if( !( camp_pathAtTop( currViewPath ) ) && 
        ( camp_varGetp( currViewPath ) == NULL ) )
    {
        camp_pathInit( currViewPath );
        camp_pathInit( currVarPath );
        if( pVarList != NULL ) 
        {
            camp_pathDown( currVarPath, pVarList->core.ident );
        }
    }
    else if( camp_varGetp( currVarPath ) == NULL )
    {
        strcpy( currVarPath, currViewPath );

        pVar = camp_varGetPHead( currViewPath );
        if( pVar != NULL )
        {
            camp_pathDown( currVarPath, pVar->core.ident );
        }
    }

    do {
      status = campSrv_varGet( currViewPath, 
                             CAMP_XDR_UPDATE | CAMP_XDR_CHILD_LEVEL );
    } while( _failure( checkRPCstatus() ) );
    if( _failure( status ) ) return( status );

    if( (pVar = camp_varGetp( currVarPath )) != camp_varGetTrueP( currVarPath ) )
    {
	do {
	  status = campSrv_varGet( pVar->core.path, 
				   CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD );
	} while( _failure( checkRPCstatus() ) );
	if( _failure( status ) ) return( status );
    }
    
    return( CAMP_SUCCESS );
}



/* 
 * Donald tries to insert optimization code to avoid (re)reading each variable
 * when multiple variables share the same readProc.  Comparing readProc didn't work!  
 * It seems the core.readProc (and writeProc) pointers are zero.
 * Next attempt is to check the last-set time for each variable, and skip it if it
 * was just updated (whether by our actions or by regular polling).  This requires
 * retrieving fresh core data for each variable before deciding to read it.
 */
int
updateServerData( void )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_head;
    CAMP_VAR* pInsVar;
    char buf[LEN_BUF+1];
    bool_t message_started;
    unsigned int update_time;

    message_started = FALSE;
    update_time = 0;

    if( currDisplay == _DispTree )
    {
        pVar_head = camp_varGetPHead( currVarPath );
        if( pVar_head == NULL ) return( CAMP_INVAL_VAR );

        for( pVar = pVar_head; pVar != NULL; pVar = pVar->pNext )
        {
            if( pVar->core.status & CAMP_VAR_ATTR_READ )
            {
                if( !message_started )
                {
                    startMessage( "Variable update", "" );
                    message_started = TRUE;
                }

                if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
                {
                    pInsVar = varGetpIns( pVar );
                    if( pInsVar == NULL ) break;

                    if( ( pInsVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
                        !( pInsVar->spec.CAMP_SPEC_u.pIns->pIF->status & 
                           CAMP_IF_ONLINE ) )
                    {
                        sprintf( buf, "Instrument \"%s\" is offline, can't read variables", 
                                      pInsVar->core.path );
                        addMessage( buf );
			confirmMessage();
			break;
                        /* continue; */
                    }
                }

                /*
                 *  Get updated data to know when var was last changed
                 */
                do {
                  status = campSrv_varGet( pVar->core.path,
                             CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD | CAMP_XDR_NO_NEXT );
                } while( _failure( checkRPCstatus() ) );
                if( _failure( status ) ) continue;

                /*
                 * only read vars that have not changed since we read our first variable
                 */
                if ( update_time == 0 || update_time > pVar->core.timeLastSet.tv_sec )
                {
                    sprintf( buf, "Reading variable \"%s\"...", pVar->core.path );
                    addMessage( buf );

                    do {
                      status = campSrv_varRead( pVar->core.path );
                    } while( _failure( checkRPCstatus() ) );

                    if( _success( status ) )
                    {
                        addMessage( "done\n" );
                    }
                    else
                    {
                        addMessage( "failed\n  " );
                        addMessage( camp_getMsg() );
                        addMessage( "\n" );
                        confirmMessage();
                    }
                    /*
                     * Use the update-time of the first variable to define the start of updates
                     */
                    if ( update_time == 0 )
                    {
                        do {
                          status = campSrv_varGet( pVar->core.path,
                                                   CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD | CAMP_XDR_NO_NEXT );
                        } while( _failure( checkRPCstatus() ) );
                        if( _success( status ) )
                        {
                          update_time = pVar->core.timeLastSet.tv_sec;
                        }
                    }
                }
                else
                {
                    sprintf( buf, "Retain variable \"%s\". \n", pVar->core.path );
                    addMessage( buf );
                }
            }
        }

        if( message_started ) 
        {
            deleteMessage();
        }
    }
    else if( currDisplay == _DispVar )
    {
        pVar = camp_varGetp( currVarPath );
        if( pVar == NULL ) return( CAMP_INVAL_VAR );

        if( !( pVar->core.status & CAMP_VAR_ATTR_READ  ) )
        {
            return( CAMP_SUCCESS );
        }

        if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
        {
            pInsVar = varGetpIns( pVar );
            if( pInsVar == NULL ) 
            {
                addMessage( "\n  Error: invalid variable" );
                confirmMessage();
                deleteMessage();
                return( CAMP_INVAL_VAR );
            }

            if( ( pInsVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
                !( pInsVar->spec.CAMP_SPEC_u.pIns->pIF->status & 
                   CAMP_IF_ONLINE ) )
            {
                sprintf( buf, "Instrument \"%s\" is offline, can't read variables", 
                              pInsVar->core.path );
                addMessage( buf );
                confirmMessage();
                deleteMessage();
                return( CAMP_NOT_CONN );
            }
        }

        sprintf( buf, "Reading variable \"%s\"...", currVarPath );
        startMessage( "Variable update", buf );

        do {
          status = campSrv_varRead( currVarPath );
        } while( _failure( checkRPCstatus() ) );

	if( _success( status ) )
	  {
	    addMessage( "done\n" );
	  }
	else
	  {
	    addMessage( "failed\n  " );
	    addMessage( camp_getMsg() );
	    addMessage( "\n" );
	    confirmMessage();
	  }
	deleteMessage();
    }    

    return( CAMP_SUCCESS );
}


void 
updateDisplay( void )
{
    if( currDisplay == _DispTree )
    {
        displayData();
        displayVarInfoWin();
        if( anyAlerts() )
        {
            displayAlertWin();
        }
        else
        {
            displayKeyWin();
        }
    }
    else if( currDisplay == _DispVar )
    {
        displayVar();
    }
}

/* 
 *  Even though comments in the curses "resizeterm" routine state it
 *  is unsafe to use it in a signal handler, that is exactly what the
 *  default configuration of ncurses does.  This leads to core dumps
 *  on window resize.  Here we define our own signal handler for
 *  resizes (SIGWINCH) that records the new size, but redisplays the
 *  screen only later.  The resize is sensed either by the changed
 *  global newScreenW/newScreenH, or by the apparent input character
 *  KEY_RESIZE.
 */
#ifdef linux
#include <termios.h>
#include <sys/ioctl.h>

int numSigWinch = 0;

static void  winAdjSize(int sig)
{
  struct winsize size;
  
  ++numSigWinch;
  if (ioctl(fileno(stdout), TIOCGWINSZ, &size) == 0) 
  {
      newScreenW = size.ws_col;
      newScreenH = size.ws_row;
      ungetch( KEY_RESIZE );
  }
  
  (void) signal(SIGWINCH, winAdjSize);	/* need this according to man (?) */
}

#endif /* linux */

void BEEP()
{
  if ( noisy ) 
  {
#ifdef VMS
    addch('\07');
#else
    beep();
#endif
  }
  return;
}

