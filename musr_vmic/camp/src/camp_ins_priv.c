/*
 *  Name:       camp_ins_priv.c
 *
 *  Purpose:    Routines to call the instrument's instrument type procedures
 *              (initProc, deleteProc, onlineProc, offlineProc).
 *
 *              An instrument type is one of the available instrument types
 *              in the CAMP server.
 *
 *              NOTE that these routines call a C routine specific to the
 *              instrument (i.e., initProc, etc.) rather than simply
 *              interpreting the Tcl script associated with that proc
 *              _because_ CAMP was written so that there could be a suite
 *              of compiled instruments built into the server (non-Tcl
 *              instruments).  In implementation, however, Tcl instrument
 *              drivers were found to be much more convenient and sufficiently
 *              fast that compiled drivers were not implemented.
 *
 *              If you look closely at the SYS_ADDINSTYPE procedure in
 *              camp_tcl_sys (camp_tcl.c) you'll see that all instrument
 *              drivers are Tcl drivers.
 *
 *              Because all instrument drivers are Tcl drivers, this layer
 *              of routines is somewhat redundant, but nonetheless convenient.
 *
 *              BUT, compiled drivers could be implemented, in which case
 *              the sysAddInsType procedure in camp_tcl.c (see SYS_ADDINSTYPE)
 *              would need to be expanded to include consideration of
 *              the compiled drivers, including assignment of the
 *              associated ins_*_* routines (e.g., ins_tcl_init, etc.).
 *              There is also a test of the driver type in
 *              campsrv_insadd_13 that would need to be expanded.
 *
 *  Called by:  camp_srv_proc.c
 * 
 *  Inputs:     Pointer to instrument's CAMP variable structure.
 *
 *  Outputs:    CAMP status
 *
 *  Revision history:
 *   16-Dec-1999  TW  CAMP_DEBUG conditional compilation
 *
 */

#include <stdio.h>
#include "camp_srv.h"


/*
 *  camp_insInit -- call an instrument's "initProc"
 */
int
camp_insInit( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.initProc != NULL )
    {
        status = (*pInsType->procs.initProc)( pVar );
        if( _failure( status ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insInit: failure: initProc %s",
                            pVar->core.path );
            }
#endif /* CAMP_DEBUG */
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insDelete -- call an instruments "deleteProc"
 */
int
camp_insDelete( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.deleteProc != NULL )
    {
        status = (*pInsType->procs.deleteProc)( pVar );
        if( _failure( status ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: deleteProc %s",
                        pVar->core.path );
            }
#endif /* CAMP_DEBUG */
            return( status );
        }
    }
    else
    {
        /*
         *  Default action is to try to call the "offlineProc"
         */
        status = camp_insOffline( pVar );
        if( _failure( status ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: camp_insOffline %s",
                        pVar->core.path );
            }
#endif /* CAMP_DEBUG */
        }
    }

    /* 
     *  Make sure it is really offline
     *  Do this to try and ensure that the interface
     *  is freed (i.e., port deallocated, etc.).
     */
    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    if( ( pIns->pIF != NULL ) && ( pIns->pIF->status & CAMP_IF_ONLINE ) )
    {
        status = camp_ifOffline( pIns->pIF );
        if( _failure( status ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: camp_ifOffline  %s",
                        pVar->core.path );
            }
#endif /* CAMP_DEBUG */
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insOnline -- call an instruments "onlineProc"
 */
int
camp_insOnline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pVar->core.path );
        return( CAMP_INVAL_IF );
    }
*/
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.onlineProc != NULL )
    {
        status = (*pInsType->procs.onlineProc)( pVar );
        if( _failure( status ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insOnline: failure: onlineProc %s",
                            pVar->core.path );
            }
#endif /* CAMP_DEBUG */
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insOffline -- call an instruments "offlineProc"
 */
int 
camp_insOffline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pVar->core.path );
        return( CAMP_INVAL_IF );
    }
*/
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.offlineProc != NULL )
    {
        status = (*pInsType->procs.offlineProc)( pVar );
        if( _failure( status ) && ( status != CAMP_INVAL_IF ) )
        {
#if CAMP_DEBUG
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insOffline: failure: offlineProc %s",
                            pVar->core.ident );
            }
#endif /* CAMP_DEBUG */
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


