$ camp_cui :== $camp:[vax-vms]camp_cui.exe
$ camp	   :== $camp:[vax-vms]camp_cui.exe
$ camp_cmd :== $camp:[vax-vms]camp_cmd.exe
$ exit
$ !
$ ! camp_login.com  -  CAMP user definitions
$ !
$ ! Revision history:
$ !  25-Apr-1994  TW  Initial version
$ !  11-Oct-1994  TW  symbol "camp" runs camp_cui
$ !  30-Nov-1994  TW  Renamed to camp_login, no camp_srv,camp_cmd
$ !  25-Oct-1995  TW  Changed directory naming scheme, readded camp_srv
$ !  31-Oct-1995  TW  camp_root: -> camp:
$ !
