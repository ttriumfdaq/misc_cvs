/*
 *  Name:       camp_if_camac_deltat.c
 *
 *  Purpose:    Provides a CAMAC interface type.
 *              CAMAC interface routines implementing Dave Maden's DELTAT
 *              CAMAC library for VAX/VMS.
 *
 *              Note that CAMAC implementation in CAMP is by way of
 *              ASCII command strings which are interpreted in a Tcl
 *              interpreter.  The routines if_camac_write and if_camac_read
 *              supplied here simply pass valid ASCII command strings to
 *              the interpreter.  Consequently, there isn't actually very
 *              much specific to the DELTAT library in this file, just the
 *              camif() call to initialize the CAMAC library.
 *
 *              A CAMAC interface definition must provide the following
 *              routines:
 *                int if_camac_init ( void );
 *                int if_camac_online ( CAMP_IF *pIF );
 *                int if_camac_offline ( CAMP_IF *pIF );
 *                int if_camac_read( REQ* pReq );
 *                int if_camac_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP CAMAC interfaces one must be
 *              careful in considering the removal of the global lock during
 *              interpretation of the Tcl command.  The Tcl interpreter
 *              could be the main interpreter or an intrument interpreter
 *              and one must be sure about the safeness of the underlying
 *              CAMAC library.  For these reasons, CAMAC commands are called
 *              with the lock on.  This has not been a problem because
 *              there are normally few CAMAC commands in CAMP drivers, and
 *              they execute very quickly in comparison with other CAMP
 *              device type (RS232, GPIB).
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include "camp_srv.h"

/*
 *  NOTE:  the interfaces priv integer is used as a check on CAMAC availability
 *
 *  pIF_t->priv:
 *     bit 1: CAMAC available
 */
#define if_camac_ok  (pIF_t->priv!=0)


int
if_camac_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int camac_if_type;

    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /*
     *  priv == 0  means unavailable
     */
    pIF_t->priv = 0;

    if( sscanf( pIF_t->conf, "%d", &camac_if_type ) != 1 )
    {
        camp_appendMsg( "if_camac_init: bad configuration format" );
        return( CAMP_FAILURE );
    }

    status = camif( &camac_if_type );
    if( _failure( status ) ) 
    {
        camp_appendMsg( "failed camif, camac unavailable" );
        camp_appendVMSMsg( status );
        return( status );
    } 

    pIF_t->priv = 1;

    return( CAMP_SUCCESS );
}


int
if_camac_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }
    /*
     *  Must set pIF_t before calling if_camac_ok macro
     */
    if( !if_camac_ok ) 
    {
        status = if_camac_init();
        if( _failure( status ) ) 
        {
            camp_appendMsg( "camac unavailable" );
            return( CAMP_FAILURE );
        }
    }

    return( CAMP_SUCCESS );
}


int
if_camac_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }
    /*
     *  Must set pIF_t before calling if_camac_ok macro
     */
    if( !if_camac_ok ) 
    {
        camp_appendMsg( "camac unavailable" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_read( REQ* pReq )
{
    int status;
    char* cmd; 
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }
    /*
     *  Must set pIF_t before calling if_camac_ok macro
     */
    if( !if_camac_ok ) 
    {
        camp_appendMsg( "camac unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed CAMAC read: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_write( REQ* pReq )
{
    int status;
    char* cmd; 
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }
    /*
     *  Must set pIF_t before calling if_camac_ok macro
     */
    if( !if_camac_ok ) 
    {
        camp_appendMsg( "camac unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed CAMAC write: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

