/*
 *  Name:       camp_var_utils.c
 *
 *  Purpose:    Utilities to access variable information in the CAMP
 *              database.
 *
 *              FORTRAN wrappers are provided for essential routines using
 *              the cfortran interface.
 *
 *  Called by:  CAMP server and client routines.
 * 
 *  Revision history:
 *
 *  DA   31-Jan-2000   allow camp_varGetpp to get an unlinked link variable.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include "timeval.h"
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */

#if !defined( RPC_SERVER ) || !defined( MULTITHREADED )
static char current_ident[LEN_IDENT+1] = "";
#define get_thread_ident() current_ident
#define set_thread_ident(i) strcpy( current_ident, i )
#endif /* !RPC_SERVER || !MULTITHREADED */

#ifndef NO_FORTRAN

#include "cfortran.h"
#undef  fcallsc
#define fcallsc( UN, LN )   preface_fcallsc( F_, f_, UN, LN )

FCALLSCFUN1(LOGICAL,camp_varExists,CAMP_VAREXISTS,camp_varexists,STRING)
FCALLSCFUN2(LONG,camp_varGetIdent,CAMP_VARGETIDENT,camp_vargetident,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varGetType,CAMP_VARGETTYPE,camp_vargettype,STRING,PLONG)
FCALLSCFUN2(LONG,camp_varGetAttributes,CAMP_VARGETATTRIBUTES,camp_vargetattributes,STRING,PLONG)
FCALLSCFUN2(LONG,camp_varGetTitle,CAMP_VARGETTITLE,camp_vargettitle,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varGetHelp,CAMP_VARGETHELP,camp_vargethelp,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varGetStatus,CAMP_VARGETSTATUS,camp_vargetstatus,STRING,PLONG)
FCALLSCFUN2(LONG,camp_varGetStatusMsg,CAMP_VARGETSTATUSMSG,camp_vargetstatusmsg,STRING,PSTRING)
FCALLSCFUN3(LONG,camp_varGetPoll,CAMP_VARGETPOLL,camp_vargetpoll,STRING,PLOGICAL,PDOUBLE)
FCALLSCFUN3(LONG,camp_varGetAlarm,CAMP_VARGETALARM,camp_vargetalarm,STRING,PLOGICAL,PSTRING)
FCALLSCFUN3(LONG,camp_varGetLog,CAMP_VARGETLOG,camp_vargetlog,STRING,PLOGICAL,PDOUBLE)
FCALLSCFUN2(LONG,camp_varGetValStr,CAMP_VARGETVALSTR,camp_vargetvalstr,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varGetTypeStr,CAMP_VARGETTYPESTR,camp_vargettypestr,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varNumGetVal,CAMP_VARNUMGETVAL,camp_varnumgetval,STRING,PDOUBLE)
FCALLSCFUN2(LONG,camp_varNumGetUnits,CAMP_VARNUMGETUNITS,camp_varnumgetunits,STRING,PSTRING)
FCALLSCFUN2(LOGICAL,camp_varNumTestTol,CAMP_VARNUMTESTTOL,camp_varnumtesttol,STRING,DOUBLE)
FCALLSCFUN8(LONG,camp_varNumGetStats,CAMP_VARNUMGETSTATS,camp_varnumgetstats,STRING,PLONG,PDOUBLE,PDOUBLE,PDOUBLE,PDOUBLE,PDOUBLE,PDOUBLE)
FCALLSCFUN4(LONG,camp_varNumCalcStats,CAMP_VARNUMCALCSTATS,camp_varnumcalcstats,STRING,PDOUBLE,PDOUBLE,PDOUBLE)
FCALLSCFUN2(LONG,camp_varSelGetID,CAMP_VARSELGETID,camp_varselgetid,STRING,PBYTE)
FCALLSCFUN2(LONG,camp_varSelGetLabel,CAMP_VARSELGETLABEL,camp_varselgetlabel,STRING,PSTRING)
FCALLSCFUN3(LONG,camp_varSelGetIDLabel,CAMP_VARSELGETIDLABEL,camp_varselgetidlabel,STRING,BYTE,PSTRING)
FCALLSCFUN3(LONG,camp_varSelGetLabelID,CAMP_VARSELGETLABELID,camp_varselgetlabelid,STRING,STRING,PBYTE)
FCALLSCFUN2(LONG,camp_varStrGetVal,CAMP_VARSTRGETVAL,camp_varstrgetval,STRING,PSTRING)
FCALLSCFUN2(LONG,camp_varLnkGetVal,CAMP_VARLNKGETVAL,camp_varlnkgetval,STRING,PSTRING)

#endif


char*
get_current_ident( void )
{
    return( get_thread_ident() );
}


void
set_current_ident( char* ident )
{
    set_thread_ident( ident );
}


      /*---------------------------*/
     /*   Data access routines    */
    /*---------------------------*/

void
camp_varDoProc_recursive( void (*proc)( CAMP_VAR* ), CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        proc( pVar );
        if( pVar->pChild != NULL ) 
	{
	    camp_varDoProc_recursive( proc, pVar->pChild );
	}
    }
}


CAMP_VAR*
camp_varGetPHead( char* path )
{
    char path_up[LEN_PATH+1];
    CAMP_VAR* pVar_up;

    strcpy( path_up, path );
    camp_pathUp( path_up );

    if( camp_pathAtTop( path_up ) )
    {
	return( pVarList );
    }
    else
    {
	pVar_up = camp_varGetp( path_up );
	if( pVar_up == NULL ) return( NULL );

	return( pVar_up->pChild );
    }
}


CAMP_VAR** 
camp_varGetpp( char* path_req )
{
    char path_curr[LEN_PATH+1];
    char ident_next[LEN_IDENT+1];
    char ident_search[LEN_IDENT+1];
    CAMP_VAR** ppVar;
    CAMP_VAR** ppVarL;

    camp_pathInit( path_curr );

    for( ppVar = &pVarList; *ppVar != NULL; ppVar = &(*ppVar)->pChild )
    {
	if( camp_pathGetNext( path_req, path_curr, ident_next ) == NULL ) 
        {
	    return( NULL );
        }

	if( strlen( ident_next ) == 0 ) 
        {
            return( NULL );
        }
	else if( streq( ident_next, "~" ) ) 
        {
	    strcpy( ident_search, get_current_ident() );
        }
	else 
        {
	    strcpy( ident_search, ident_next );
        }

	/*
	 *  Traverse current level of the tree
	 */
	for( ; *ppVar != NULL; ppVar = &(*ppVar)->pNext )
        {
	    if( streq( ident_search, (*ppVar)->core.ident ) ) 
            {
		break;
            }
        }

	if( *ppVar == NULL ) 
        {
            return( NULL );
        }

	if( (*ppVar)->core.varType == CAMP_VAR_TYPE_LINK )
	{
 	    /* DA: only follow defined links */
	    if( strcmp( (*ppVar)->spec.CAMP_SPEC_u.pLnk->path, "") != 0 )
	    {
	        ppVarL = camp_varGetpp( (*ppVar)->spec.CAMP_SPEC_u.pLnk->path );
		if( ppVarL != NULL ) 
		{
		    if( *ppVarL != NULL ) 
		    {
		        ppVar = ppVarL;
		    }
		}
	    }
	}

	if( camp_pathDown( path_curr, ident_next ) == NULL ) 
        {
            return( NULL );
        }

	if( streq( path_req, path_curr ) ) 
        {
            return( ppVar );
        }
    }

    return( NULL );
}


CAMP_VAR* 
camp_varGetp( char* path )
{
    CAMP_VAR** ppVar;

    ppVar = camp_varGetpp( path );

    return( ( ppVar == NULL ) ? NULL : *ppVar );
}


CAMP_VAR** 
camp_varGetTruePP( char* path_req )
{
    char path_curr[LEN_PATH+1];
    char ident_next[LEN_IDENT+1];
    char ident_search[LEN_IDENT+1];
    CAMP_VAR** ppVar;

    camp_pathInit( path_curr );

    for( ppVar = &pVarList; *ppVar != NULL; ppVar = &(*ppVar)->pChild )
    {
	if( camp_pathGetNext( path_req, path_curr, ident_next ) == NULL ) 
        {
	    return( NULL );
        }

	if( strlen( ident_next ) == 0 ) 
        {
            return( NULL );
        }
	else if( streq( ident_next, "~" ) ) 
        {
	    strcpy( ident_search, get_current_ident() );
        }
	else 
        {
	    strcpy( ident_search, ident_next );
        }

	/*
	 *  Traverse current level of the tree
	 */
	for( ; *ppVar != NULL; ppVar = &(*ppVar)->pNext )
        {
	    if( streq( ident_search, (*ppVar)->core.ident ) ) 
            {
		break;
            }
        }

	if( *ppVar == NULL ) 
        {
            return( NULL );
        }

	camp_pathDown( path_curr, ident_next );

	/*
	 *  Return if this is the one we're looking for
	 */
	if( streq( path_req, path_curr ) ) 
        {
            return( ppVar );
        }

	/*
	 *  The current var is not the one we're looking for
	 *  but it may be a Link, so check and expand
	 */
	if( (*ppVar)->core.varType == CAMP_VAR_TYPE_LINK )
	{
	    ppVar = camp_varGetpp( (*ppVar)->spec.CAMP_SPEC_u.pLnk->path );
	    if( *ppVar == NULL ) 
            {
                return( NULL );
            }
	}
    }

    return( NULL );
}


CAMP_VAR* 
camp_varGetTrueP( char* path )
{
    CAMP_VAR** ppVar;

    ppVar = camp_varGetTruePP( path );

    return( ( ppVar == NULL ) ? NULL : *ppVar );
}


CAMP_CORE* 
camp_varGetpCore( char* path )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );

    return( ( pVar == NULL ) ? NULL : &pVar->core );
}


caddr_t 
camp_varGetpSpec( char* path )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );

    return( ( pVar == NULL ) ? (caddr_t)NULL : 
                               (caddr_t)&pVar->spec.CAMP_SPEC_u );
}


/*
 *  camp_varGetpIns  -  get pointer to a variable's parent
 *                      instrument variable
 */
CAMP_VAR*
camp_varGetpIns( char* path )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( NULL );

    return( varGetpIns( pVar ) );
}


/*
 *  varGetpIns  -  get pointer to a variable's parent
 *                 instrument variable
 */
CAMP_VAR*
varGetpIns( CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;
    char path[LEN_PATH+1];

    if( pVar_start->core.varType == CAMP_VAR_TYPE_INSTRUMENT )
      {
	return( pVar_start );
      }

    strcpy( path, pVar_start->core.path );
    camp_pathUp( path );

    /*
     *  Traverse up the tree,
     *  return the first instrument found
     */
    for( ; !camp_pathAtTop( path ); camp_pathUp( path ) )
    {
        pVar = camp_varGetp( path );

        if( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT )
        {
            return( pVar );
        }
    }

    return( NULL );
}


bool_t
camp_varExists( char* path )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );

    return( ( pVar == NULL ) ? FALSE : TRUE );
}


int
camp_varGetIdent( char* path, char* ident )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

	strcpy( ident, pVar->core.ident );

    return( CAMP_SUCCESS );
}


int
camp_varGetType( char* path, CAMP_VAR_TYPE* pVarType )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

	*pVarType = pVar->core.varType;

    return( CAMP_SUCCESS );
}


int
camp_varGetAttributes( char* path, u_long* pAttributes )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pAttributes = pVar->core.attributes;

    return( CAMP_SUCCESS );
}


int
camp_varGetTitle( char* path, char* title )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

	strcpy( title, pVar->core.title );

    return( CAMP_SUCCESS );
}


int
camp_varGetHelp( char* path, char* help )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

	strcpy( help, pVar->core.help );

    return( CAMP_SUCCESS );
}


int
camp_varGetStatus( char* path, u_long* pStatus )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pStatus = pVar->core.status;

    return( CAMP_SUCCESS );
}


int
camp_varGetStatusMsg( char* path, char* statusMsg )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

	strcpy( statusMsg, pVar->core.statusMsg );

    return( CAMP_SUCCESS );
}


int
camp_varGetPoll( char* path, bool_t* pFlag, float* pInterval )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_POLL ) ? TRUE : FALSE;
    *pInterval = pVar->core.pollInterval;

    return( CAMP_SUCCESS );
}


int
camp_varGetAlarm( char* path, bool_t* pFlag, char* alarmAction )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_ALARM ) ? TRUE : FALSE;
    strcpy( alarmAction, pVar->core.alarmAction );

    return( CAMP_SUCCESS );
}


int
camp_varGetLog( char* path, bool_t* pFlag, char* action )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_LOG ) ? TRUE : FALSE;
    strcpy( action, pVar->core.logAction );

    return( CAMP_SUCCESS );
}


int
camp_varGetValStr( char* path, char* str )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
		return( varNumGetValStr( pVar, str ) );
      case CAMP_VAR_TYPE_STRING:
		strcpy( str, pVar->spec.CAMP_SPEC_u.pStr->val );
        break;
      case CAMP_VAR_TYPE_SELECTION:
		varSelGetIDLabel( pVar, pVar->spec.CAMP_SPEC_u.pSel->val, str );
        break;
    }
    return( CAMP_SUCCESS );
}


int
camp_varGetTypeStr( char* path, char* str )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    switch( pVar->core.varType )
    {
      case CAMP_VAR_TYPE_INT:
        strcpy( str, toktostr( TOK_INT ) );
		break;
      case CAMP_VAR_TYPE_FLOAT:
        strcpy( str, toktostr( TOK_FLOAT ) );
		break;
      case CAMP_VAR_TYPE_SELECTION:
        strcpy( str, toktostr( TOK_SELECTION ) );
		break;
      case CAMP_VAR_TYPE_STRING:
        strcpy( str, toktostr( TOK_STRING ) );
		break;
      case CAMP_VAR_TYPE_ARRAY:
        strcpy( str, toktostr( TOK_ARRAY ) );
		break;
      case CAMP_VAR_TYPE_STRUCTURE:
        strcpy( str, toktostr( TOK_STRUCTURE ) );
		break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        strcpy( str, toktostr( TOK_INSTRUMENT ) );
		break;
      case CAMP_VAR_TYPE_LINK:
        strcpy( str, toktostr( TOK_LINK ) );
		break;
	  default:
		return( CAMP_INVAL_VAR );
    }

    return( CAMP_SUCCESS );
}


int
camp_varNumGetVal( char* path, double* pVal )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pNum = pVar->spec.CAMP_SPEC_u.pNum;

    *pVal = pNum->val;

    return( CAMP_SUCCESS );
}


int
varNumGetValStr( CAMP_VAR* pVar, char* str )
{
    return( ( numGetValStr( pVar->core.varType, 
	    pVar->spec.CAMP_SPEC_u.pNum->val, str ) ) ? 
	    CAMP_SUCCESS : CAMP_FAILURE );
}

bool_t
numGetValStr( CAMP_VAR_TYPE varType, double val, char* str )
{
    int exp;
    int prec;
#define STRING_PRECISION 10

    switch( varType )
    {
      case CAMP_VAR_TYPE_INT:
	sprintf( str, "%d", (int)val );
	break;

      case CAMP_VAR_TYPE_FLOAT:
	if( val == 0.0 ) 
	{
	    sprintf( str, "%.*f", STRING_PRECISION, val );
	}
	else
	{
	    exp = (int)floor( log10( fabs( val ) ) );

	    if( ( exp > 8 ) || ( exp < -4 ) )
	    {
                /*
                 * > 1e9 || < 0.0001
                 */
		sprintf( str, "%.*e", STRING_PRECISION, val );
	    }
	    else
	    {
                /*
                 *  0.0001 - 1e9
                 */
		if( exp > 0 ) prec = STRING_PRECISION-exp; /* 1e9 - 10 */
		else          prec = STRING_PRECISION;     /* 10 - 0.0001 */
		sprintf( str, "%.*f", prec, val );
	    }
	}
        break;	 

      default:
        return( FALSE );
    }
    return( TRUE );
}


int
camp_varNumGetUnits( char* path, char* units )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) == CAMP_VAR_TYPE_NUMERIC )
    {
      pNum = pVar->spec.CAMP_SPEC_u.pNum;
      strcpy( units, pNum->units );
    }
    else
    {
      units[0] = '\0';
    }

    return( CAMP_SUCCESS );
}


/*
 *  TRUE = within tolerance
 *  FALSE = out of tolerance
 */
bool_t
camp_varNumTestTol( char* path, double val )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;
    CAMP_SELECTION* pSel;
    double min, max;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( TRUE );

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) 
    {
        case CAMP_VAR_TYPE_NUMERIC:
	    pNum = pVar->spec.CAMP_SPEC_u.pNum;

	    switch( pNum->tolType )
	    {
	      /* Plus/minus tolerance */
	      case 0:
		if( pNum->tol < 0.0 ) return( TRUE );

		min = pNum->val - pNum->tol;
		max = pNum->val + pNum->tol;
		break;

	      /* Percent tolerance */
	      case 1:
		if( pNum->tol < 0.0 ) return( TRUE );

		min = pNum->val - pNum->val*pNum->tol/100.0;
		max = pNum->val + pNum->val*pNum->tol/100.0;
		break;

	      default:
		return( TRUE );
	    }

	    if( val < min || val > max ) return( FALSE );
	    else return( TRUE );

        case CAMP_VAR_TYPE_SELECTION:
	    pSel = pVar->spec.CAMP_SPEC_u.pSel;

            /* Zero tolerance for differences */

            return( val == pSel->val ? TRUE : FALSE );

	default:
	    return( TRUE );
    }
}


int
camp_varNumGetStats( char* path, u_long* pN, 
		  double* pLow, double* pHi, double* pOffset,
		  double* pSum, double* pSumSquares, double* pSumCubes )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pNum = pVar->spec.CAMP_SPEC_u.pNum;

    if( pN ) *pN = pNum->num;
    if( pLow ) *pLow = pNum->low;
    if( pHi ) *pHi = pNum->hi;
    if( pSum ) *pSum = pNum->sum;
    if( pSumSquares ) *pSumSquares = pNum->sumSquares;
    if( pSumCubes ) *pSumCubes = pNum->sumCubes;
    if( pOffset ) *pOffset = pNum->offset;

    return( CAMP_SUCCESS );
}


int
camp_varNumCalcStats( char* path, double* pMean, double* pStdDev,
		      double* pSkew )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;
    u_long n;
    double sum;
    double sumSquares;
    double sumCubes;
    double mean;
    double stddev;
    double skew;
    double var;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pNum = pVar->spec.CAMP_SPEC_u.pNum;

    n = pNum->num;
    sum = pNum->sum;
    sumSquares = pNum->sumSquares;
    sumCubes = pNum->sumCubes;

    if( n == 0 )
    {
	mean = stddev = skew = 0.0;
    }
    else
    {
	mean = ( sum == 0.0 ) ? 0.0 : sum/n;

	/*
	 *  Calculate standard deviation
	 */
	if( n == 1 ) 
	{
	    stddev = 0.0;
	}
	else
	{
	    var = ( sumSquares - ( (sum==0.0)?0.0:(pow(sum,2)/n) ) )/
		  ( n-1 );
	    stddev = sqrt( fabs( var ) );
	}

	/*
	 *  Calculate skewness
	 */
	if( stddev == 0.0 ) 
	{
	    skew = 0.0;
	}
	else
	{
	    skew = ( sumCubes - 3*mean*sumSquares + 
		     2*( (sum==0.0)?0.0:pow(sum,3) )/( pow(n,2) ) )/
		   ( n*pow(stddev,3) );
	}
    }

    *pMean = mean + pNum->offset;
    *pStdDev = stddev;
    *pSkew = skew;

    return( CAMP_SUCCESS );
}


int
camp_varSelGetID( char* path, u_char* pVal )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    *pVal = pVar->spec.CAMP_SPEC_u.pSel->val;

    return( CAMP_SUCCESS );
}


int
camp_varSelGetLabel( char* path, char* label )
{
    CAMP_VAR* pVar;
    CAMP_SELECTION* pSel;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pSel = pVar->spec.CAMP_SPEC_u.pSel;

    return( varSelGetIDLabel( pVar, pSel->val, label ) );
}


int
camp_varSelGetIDLabel( char* path, u_char val, char* label )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    return( varSelGetIDLabel( pVar, val, label ) );
}


int
varSelGetIDLabel( CAMP_VAR* pVar, u_char val, char* label )
{
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    int i;

    pSel = pVar->spec.CAMP_SPEC_u.pSel;

    for( i = 0,	    pItem = pSel->pItems; 
	 ( i < val ) && ( pItem != NULL ); 
	 i++,	    pItem = pItem->pNext ) ;

    if( pItem == NULL )
    {
	label[0] = '\0';
	return( CAMP_INVAL_SELECTION );
    }

    strcpy( label, pItem->label );

    return( CAMP_SUCCESS );
}


int
camp_varSelGetLabelID( char* path, char* label, u_char* pVal )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    return( varSelGetLabelID( pVar, label, pVal ) );
}


int
varSelGetLabelID( CAMP_VAR* pVar, char* label, u_char* pVal )
{
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    int i;

    pSel = pVar->spec.CAMP_SPEC_u.pSel;

    for( i = 0, pItem = pSel->pItems; 
		pItem != NULL; 
	 i++,	pItem = pItem->pNext )
    {
	if( streq( label, pItem->label ) )
	{
	    *pVal = (u_char)i;
	    return( CAMP_SUCCESS );
	}
    }

    return( CAMP_INVAL_SELECTION );
}


int
camp_varStrGetVal( char* path, char* val )
{
    CAMP_STRING* pSpec;
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pSpec = pVar->spec.CAMP_SPEC_u.pStr;
    strcpy( val, pSpec->val );

    return( CAMP_SUCCESS );
}


int
camp_varArrGetVal( char* path, caddr_t pVal )
{
    CAMP_VAR* pVar;
    CAMP_ARRAY* pSpec;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pSpec = pVar->spec.CAMP_SPEC_u.pArr;

    bcopy( pSpec->pVal, pVal, pSpec->elemSize*pSpec->totElem );

    return( CAMP_SUCCESS );
}


int
camp_varLnkGetVal( char* path, char* val )
{
    CAMP_VAR* pVar;
    CAMP_LINK* pSpec;

    pVar = camp_varGetTrueP( path );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );

    pSpec = pVar->spec.CAMP_SPEC_u.pLnk;

    strcpy( val, pSpec->path );

    return( CAMP_SUCCESS );
}


