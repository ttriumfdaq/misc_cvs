$ SaveVerify = f$verify(0)
$ @camp:[000000]camp_login      ! define symbol camp_cmd
$ if( f$mode() .nes. "OTHER" ) .or. -               ! detached is OTHER
    ( f$getjpi("","prcnam") .nes. "CAMP_SERVER" ) - ! but so is startup
    then goto StartDetached                         ! so check proc name
$ set message /nof/nos/noi/not
$ delete/symbol/global/nolog camp_status
$ delete/symbol/local/nolog  camp_status
$ set message /f/s/i/t
$RunIt:
$ srv_exe = "camp:[vax-vms]camp_srv.exe"
$ if ( f$sea( srv_exe ) .eqs. "" )
$ then
$   write sys$output "CAMP Server executable ''srv_exe' not found.  CAMP not started."
$   ExitStatus = 1
$   goto Exit
$ endif
$ set noon
$ run 'srv_exe'
$ ExitStatus = $status
$ set on
$ if( ( camp_status .eqs. "RESTART" ) .or. -
      ( camp_status .eqs. "FAILURE" ) ) then goto RunIt
$ goto Exit
$StartDetached:
$ gosub Check
$ if( up )
$ then
$   write sys$output "CAMP Server already running."
$   ExitStatus = 1
$   goto Exit
$ endif
$ log_file := camp:[log]start_camp_srv.log
$ log_file2 := camp:[log]camp_srv.log
$! msg_stat = f$environment( "MESSAGE" )
$ set noon
$! set message /nof/nos/noi/not
$ if f$search( "''log_file'" ) .nes. "" then purge/nolog/keep=3 'log_file'
$ if f$search( "''log_file2'" ) .nes. "" then purge/nolog/keep=3 'log_file2'
$! set message /f/s/i/t
$ set on
$ run sys$system:loginout -
    /detach -
    /input='F$ENVIRONMENT("PROCEDURE")' -
    /output='log_file' -
    /priv=(nosame,tmpmbx,netmbx) -
    /process_name="CAMP_SERVER"
$ ExitStatus = 1
$Exit:
$ SaveVerify = f$verify(SaveVerify)
$ exit ExitStatus
$Check:
$ up = 0
$ set noon
$ camp_cmd ""
$ status = $status
$ set on
$ if( status ) then up = 1
$ return
$ !
$ !  camp_srv.com  -  start CAMP server if nonexistant
$ !
$ !  Revision history:
$ !   30-Nov-1994  TW  Combined 3 startup files into this one
$ !   10-Oct-1995  TW  directory camp_log: -> camp_root:[log]
$ !   25-Oct-1995  TW  directory camp: -> camp_root:[000000]
$ !   31-Oct-1995  TW  camp_root: -> camp:
$ !
