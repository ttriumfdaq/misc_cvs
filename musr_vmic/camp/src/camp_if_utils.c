/*
 *	$Id$
 *
 *	$Revision$
 *
 *
 *  Purpose:     Routines for accessing interface type information in the
 *               CAMP database, either in general or specific to an
 *               interface type.
 *
 *  Called by:   camp_cui_*.c, camp_if_*.c, camp_tcl.c
 * 
 *  $Log$
 *  Revision 1.7  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.6  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 *  Revision 1.5  2001/01/17 04:16:10  asnd
 *  Debug rs232PortInUse error message
 *
 *  Revision 1.4  2001/01/15 06:41:28  asnd
 *  Error message for rs232PortInUse.
 *
 *  Revision 1.3  2000/12/23 02:37:50  ted
 *  camp_getIfIndpakChan -> camp_getIfIndpakChannel for consistency (different in some places than others)
 *
 *  Revision 1.2  2000/12/22 22:34:57  David.Morris
 *  Added INDPAK parameter access functions and prototypes. Added CAMP_IF_TYPE_INDPAK
 *  to instrument type list
 *
 *
 */

#include <string.h>
#include "camp.h"

static char* strIndex( char* str, int sep, int ind, char* ret );


/*
 *  Name:        camp_ifGetpIF_t
 *
 *  Purpose:     Return pointer to interface type structure based on
 *               interface type identifier (string)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type identifier (string) (e.g., rs232, gpib).
 *
 *  Outputs:     Pointer to interface type structure.
 *
 */
CAMP_IF_t* 
camp_ifGetpIF_t( char* ident )
{
    CAMP_IF_t* pIF_t;

    for( pIF_t = pSys->pIFTypes; 
	pIF_t != NULL; 
	pIF_t = pIF_t->pNext ) 
    {
	if( streq( ident, pIF_t->ident ) )
        {
	    return( pIF_t );
	}
    }
    
    return( NULL );
}


/*
 *  Name:        camp_getIfTypeID
 *
 *  Purpose:     Return interface type id (integer) based on
 *               interface type identifier (string)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type identifier (string) (e.g., rs232, gpib).
 *
 *  Outputs:     Interface type id (integer).
 *
 */
CAMP_IF_TYPE
camp_getIfTypeID( char* typeIdent )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( typeIdent );
    if( pIF_t == NULL ) return( 0 );

    return( pIF_t->typeID );
}


/*
 *  Name:        camp_getIfTypeIdent
 *
 *  Purpose:     Return interface type identifier (string) based on
 *               interface type id (integer)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type id (integer).
 *
 *  Outputs:     Interface type identifier (string) (e.g., rs232, gpib).
 *               The returned string pointer points to a memory location
 *               in the CAMP database and should not be overwritten or
 *               deallocated.
 */
char* 
camp_getIfTypeIdent( CAMP_IF_TYPE typeId )
{
    CAMP_IF_t* pIF_t;

    for( pIF_t = pSys->pIFTypes; 
         pIF_t != NULL; 
         pIF_t = pIF_t->pNext ) 
    {
        if( typeId == pIF_t->typeID ) 
	{
            return( pIF_t->ident );
	}
    }
        
    return( NULL );
}


/*
 *  strIndex 
 *
 *  Find and return (in ret) the ind substring
 *  of str delimited by the separator sep
 *
 *  This routine is declared "static" (local to this file) and is
 *  used only by the following camp_getIf* routines.
 */
static char*
strIndex( char* str, int sep, int ind, char* ret )
{
    int i;
    char* p;
    char* s;

    for( s = str, i = 0;
         (s != NULL) && (*s != '\0') && (i < ind);
         i++ )
    {
    	s = strchr( s, sep );
        if( (s != NULL) && (*s != '\0') ) s++;
    }

    for( p = ret;
         (s != NULL) && (*s != sep) && (*s != '\0');
         *p++ = *s++ ) ;

    *p = '\0';

    return ret;
}
  

/*
 *  Name:           camp_getIf*
 *
 *  Purpose:        The following routines are specific to an interface
 *                  type (RS232, GPIB,...).  They retrieve individual
 *                  strings from the interface definition, which is also
 *                  a string.
 *
 *                  Each instance of a CAMP instrument has an associated
 *                  interface (RS232, GPIB,...) and an interface definition.
 *                  The interface definition is a string that includes all
 *                  the parameters necessary to define an instance of the
 *                  interface, separated by spaces.  Interface definition
 *                  strings for current defined interface types are as follows:
 *
 *                  RS232:    "<port> <baud> <databits> <parity> <stopbits>
 *                             <readTerminator> <writeTerminator> <timeout>"
 *
 *                  GPIB:     "<address> <readTerminator> <writeTerminator>"
 *
 *                  CAMAC:    "<Branch> <Crate> <Num>"
 *
 *                  INDPAK:   "<slot> <type> <channel>"
 *
 *                  VME:      "<base>"
 *
 *                  TCPIP:    "<ipAddress> <portNumber> <readTerminator> 
 *                             <writeTerminator> <timeout>"
 *
 *                  The following routines are used in many parts of CAMP
 *                  to extract each of the parameters from the definition
 *                  string.
 *
 *  Called by:      camp_cui*.c, camp_if_*.c, camp_tcl.c
 * 
 *  Inputs:         Instrument interface definition string.
 *
 *  Preconditions:  An instrument's interface has been defined.
 *
 *  Outputs:        The parameter requested in either string or numeric
 *                  format as appropriate.
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */


/*
 *                  RS232:    "<port> <baud> <databits> <parity> <stopbits>
 *                             <readTerminator> <writeTerminator> <timeout>"
 */
char*
camp_getIfRs232Port( char* defn, char* str )
{
    return strIndex( defn, ' ', 0, str );
}


int
camp_getIfRs232Baud( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 1, str ) );
}


int
camp_getIfRs232Data( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 2, str ) );
}


char*
camp_getIfRs232Parity( char* defn, char* str )
{
    return strIndex( defn, ' ', 3, str );
}


int
camp_getIfRs232Stop( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 4, str ) );
}


char*
camp_getIfRs232ReadTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 5, str );
}


char*
camp_getIfRs232WriteTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 6, str );
}


int
camp_getIfRs232Timeout( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 7, str ) );
}


/*
 *                  GPIB:     "<address> <readTerminator> <writeTerminator>"
 */

int
camp_getIfGpibAddr( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 0, str ) );
}


char*
camp_getIfGpibReadTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 1, str );
}


char*
camp_getIfGpibWriteTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 2, str );
}


/*
 *                  CAMAC:    "<Branch> <Crate> <Num>"
 */

int
camp_getIfCamacB( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 0, str ) );
}


int
camp_getIfCamacC( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 1, str ) );
}


int
camp_getIfCamacN( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 2, str ) );
}


/*
 *                  INDPAK:   "<slot> <type> <channel>"
 */

int
camp_getIfIndpakSlot( char* defn )
{
    char str[LEN_IDENT+1];
    return atol( strIndex( defn, ' ', 0, str ) );
}


char*
camp_getIfIndpakType( char* defn, char* str )
{
    return strIndex( defn, ' ', 1, str );
}


int
camp_getIfIndpakChannel( char* defn )
{
    char str[LEN_IDENT+1];
    return atol( strIndex( defn, ' ', 2, str ) );
}


/*
 *                  VME:      "<base>"
 */

int
camp_getIfVmeBase( char* defn )
{
    char str[LEN_IDENT+1];
    return atol( strIndex( defn, ' ', 0, str ) );
}


/*
 *                  TCPIP:    "<ipAddress> <portNumber> <readTerminator> 
 *                             <writeTerminator> <timeout>"
 */

char*
camp_getIfTcpipAddr( char* defn, char* str )
{
    return strIndex( defn, ' ', 0, str );
}


int
camp_getIfTcpipPort( char* defn )
{
    char str[LEN_IDENT+1];

    return atol( strIndex( defn, ' ', 1, str ) );
}


char*
camp_getIfTcpipReadTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 2, str );
}


char*
camp_getIfTcpipWriteTerm( char* defn, char* str )
{
    return strIndex( defn, ' ', 3, str );
}


float
camp_getIfTcpipTimeout( char* defn )
{
    char str[LEN_IDENT+1];

    return (float)atof( strIndex( defn, ' ', 4, str ) );
}



/*
 *  Name:        camp_rs232PortInUse
 *
 *  Purpose:     Test if the rs232 port used by instrument *pIns
 *               is in use by another (online) instrument.  If yes,
 *               return pointer to that instrument; else NULL.
 *
 *  Inputs:      Pointer to instrument attempting to go online
 *               (also external pVarList).
 *
 *  Outputs:     Pointer to conflicting instrument (CAMP_VAR*) or NULL
 *
 *  Called by:   campsrv_insifon_13 in camp_srv_proc.c (but the test 
 *               may get moved to another place).
 * 
 *  Revision history:
 *               29-Sep-2000   DJA   Initial version
 */

CAMP_VAR*
camp_rs232PortInUse( CAMP_INSTRUMENT* pIns )
{
    CAMP_INSTRUMENT* pInsOther;
    CAMP_VAR* pVar;
    char port[LEN_IDENT+1];
    char portOther[LEN_IDENT+1];

    camp_getIfRs232Port( pIns->pIF->defn, port );

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pInsOther = pVar->spec.CAMP_SPEC_u.pIns;
        if( pInsOther != pIns )
        {
            if( pInsOther->pIF != NULL )
            {
                if( pInsOther->pIF->typeID == CAMP_IF_TYPE_RS232 )
                {
                    if( pInsOther->pIF->status & CAMP_IF_ONLINE )
                    {
                        camp_getIfRs232Port( pInsOther->pIF->defn, portOther );
                        if( strncmp( port, portOther, LEN_IDENT ) == 0)
                        {
                            return( pVar );
                        }
                    }
                }
            }
        }
    }
    return( (CAMP_VAR*)NULL );
}

