/*
 *  Name:       camp_ins_tcl.c
 *
 *  Purpose:    Routines to implement a Tcl instrument driver
 *
 *              Note that Tcl instrument drivers are the only instrument
 *              drivers implemented in CAMP.  An alternative could be
 *              compiled instrument drivers.  See also the note in the file
 *              camp_ins_priv.c.
 *
 *  Called by:  camp_ins_priv.c
 *
 *  Inputs:     Pointer to a CAMP instrument's variable structure
 *              (plus possibly more parameters)
 *
 *  Preconditions:
 *              Instrument types are added in camp_tcl_sys using the
 *              command sysAddInsType at server startup.  Instruments are
 *              then added as one of those types.  Tcl instrument drivers
 *              have parameters to the CAMP_INSTRUMENT definition that
 *              define various proc's to be executed for various states of
 *              the instrument.  The proc's are called as follows:
 *
 *                 ins_tcl_init      calls instrument's  -initProc 
 *                 ins_tcl_delete    calls instrument's  -deleteProc 
 *                 ins_tcl_online    calls instrument's  -onlineProc 
 *                 ins_tcl_offline   calls instrument's  -offlineProc 
 *
 *              Tcl instrument drivers also have variable definitions.
 *              Each variable has an associated "-readProc" and "-writeProc".
 *              The proc's are called as follows:
 *
 *                 ins_tcl_set       calls variable's  -writeProc
 *                 ins_tcl_read      calls variable's  -readProc
 *
 *              These procedures are evaluated in the instrument's own Tcl
 *              interpreter.
 *
 *  Outputs:    CAMP status
 *
 *  Revision history:
 *              Dec 2004   DJA    Get instrument mutex lock when using
 *                                instrument's Tcl interp.
 *
 *  $Log$
 *  Revision 1.2  2004/12/18 01:04:37  asnd
 *  Request mutex when switching Tcl interpreter -- fix server crashes.
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
ins_tcl_init( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->initProc == NULL ) || ( pIns->initProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
			 pIns->initProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "init failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_delete( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->deleteProc == NULL ) || ( pIns->deleteProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
#ifdef MULTITHREADED
    get_ins_thread_lock( pVar );
#endif /* MULTITHREADED */

    status = Tcl_VarEval( pIns->interp, 
			 pIns->deleteProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
#ifdef MULTITHREADED
    release_ins_thread_lock( pVar );
#endif /* MULTITHREADED */
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "delete failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_online( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->onlineProc == NULL ) || ( pIns->onlineProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
#ifdef MULTITHREADED
    get_ins_thread_lock( pVar );
#endif /* MULTITHREADED */

    status = Tcl_VarEval( pIns->interp, 
			 pIns->onlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

#ifdef MULTITHREADED
    release_ins_thread_lock( pVar );
#endif /* MULTITHREADED */
    set_thread_interp( oldInterp );

    if( status == TCL_ERROR )
      {
	camp_appendMsg( "online failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_offline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->offlineProc == NULL ) || ( pIns->offlineProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
#ifdef MULTITHREADED
    get_ins_thread_lock( pVar );
#endif /* MULTITHREADED */

    status = Tcl_VarEval( pIns->interp, 
			 pIns->offlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

#ifdef MULTITHREADED
    release_ins_thread_lock( pVar );
#endif /* MULTITHREADED */
    set_thread_interp( oldInterp );

    if( status == TCL_ERROR )
      {
	camp_appendMsg( "offline failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       ins_tcl_set
 *
 *  Purpose:    Call a variables -writeProc
 *
 *  Input:      pInsVar - the variable's instrument variable structure
 *              pVar    - the structure of the variable to set
 *              pSpec   - the CAMP_SPEC structure identifying the new value
 *                        of the variable.  CAMP_SPEC will be interpreted
 *                        differently according to the variable type.
 *
 */
int
ins_tcl_set( CAMP_VAR* pInsVar, CAMP_VAR* pVar, CAMP_SPEC* pSpec )
{
    int status;
    CAMP_VAR setVar;
    char val[LEN_STRING+1];
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.writeProc == NULL ) || 
        ( pVar->core.writeProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    /*
     *  This section gets the value as a string from the pSpec
     *  structure.  Selection variable values can be specified as
     *  either a label or an index.
     */
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
      {
      case CAMP_VAR_TYPE_NUMERIC:
	bcopy( pVar, &setVar, sizeof( CAMP_VAR ) );
	bcopy( pSpec, &setVar.spec, sizeof( CAMP_SPEC ) );
	varNumGetValStr( &setVar, val );
	break;
      case CAMP_VAR_TYPE_STRING:
	strcpy( val, pSpec->CAMP_SPEC_u.pStr->val );
	break;
      case CAMP_VAR_TYPE_SELECTION:
	if( pSpec->CAMP_SPEC_u.pSel->pItems != NULL )
	  {
	    if( pSpec->CAMP_SPEC_u.pSel->pItems->label == NULL )
	      {
		camp_appendMsg( "set failure: bad selection label" );
		return( CAMP_FAILURE );
	      }
	    strcpy( val, pSpec->CAMP_SPEC_u.pSel->pItems->label );
	  }
	else
	  {
	    sprintf( val, "%d", pSpec->CAMP_SPEC_u.pSel->val );
	  }
	break;
      }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
#ifdef MULTITHREADED
    get_ins_thread_lock( pInsVar );
#endif /* MULTITHREADED */

    status = Tcl_VarEval( pIns->interp, 
                          pVar->core.writeProc, 
			  " {",
                          pInsVar->core.ident, 
			  "} {",
                          val, 
                          "}",
                          NULL );
#ifdef MULTITHREADED
    release_ins_thread_lock( pInsVar );
#endif /* MULTITHREADED */
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
    {
        camp_appendMsg( "set failure: %s", pIns->interp->result );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       ins_tcl_read
 *
 *  Purpose:    Call a variables -readProc
 *
 *  Input:      pInsVar - the variable's instrument variable structure
 *              pVar    - the structure of the variable to read
 *
 */
int
ins_tcl_read( CAMP_VAR* pInsVar, CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.readProc == NULL ) || 
        ( pVar->core.readProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
#ifdef MULTITHREADED
    get_ins_thread_lock( pInsVar );
#endif /* MULTITHREADED */

    status = Tcl_VarEval( pIns->interp, 
                          pVar->core.readProc, 
                          " {",
                          pInsVar->core.ident, 
                          "}",
                          NULL );

#ifdef MULTITHREADED
    release_ins_thread_lock( pInsVar );
#endif /* MULTITHREADED */
    set_thread_interp( oldInterp );

    if( status == TCL_ERROR )
    {
        camp_appendMsg( "read failure: %s", pIns->interp->result );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

