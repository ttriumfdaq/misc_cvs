/*
 *  Name:       camp_cui_keywin.c
 *
 *  Purpose:    Manage the "key" window at the bottom of the CAMP CUI screen.
 *              This window provides hints to common keystrokes.
 *
 *  Called by:  
 * 
 *  Revision history:
 *
 */

#include <stdio.h>
#include <curses.h>
#include "timeval.h"
#include "camp_cui.h"

WINDOW* keyWin = NULL;

extern char currVarPath[LEN_PATH+1];

int
initKeyWin( void )
{
    keyWin = newwin( KEYWIN_H, KEYWIN_W, 
                         KEYWIN_Y, KEYWIN_X ); 
    if( keyWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( keyWin, FALSE );
    scrollok( keyWin, FALSE );
    keypad( keyWin, TRUE );
    immedok( keyWin, FALSE );
    clearok( keyWin, FALSE );
    wtimeout( keyWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteKeyWin( void )
{
    if( keyWin != (WINDOW*)NULL ) {
      delwin( keyWin );
      keyWin = (WINDOW*)NULL;
    }
}


void 
clearKeyWin( void )
{
    if( keyWin != (WINDOW*)NULL ) werase( keyWin );
}


void 
touchKeyWin( void )
{
    if( keyWin != (WINDOW*)NULL ) touchwin( keyWin );
}


#ifndef VMS
void 
uncoverKeyWin( void )
{
    if( keyWin != (WINDOW*)NULL ) 
    {
      touchwin( keyWin );
      wnoutrefresh( keyWin );
    }
}
#endif

void 
refreshKeyWin( void )
{
    if( keyWin != (WINDOW*)NULL ) 
    {
      /* clearok( keyWin, FALSE ); */
      wrefresh( keyWin );
    }
}


void
displayKeyWin( void )
{
    int y, x;
    char* title = "Key";

    /* First, delete the alert win, so we are not hidden */
    deleteAlertWin();

#define COL  ((KEYWIN_W-2)/3)
#define COL1 2
#define COL2 (COL1+COL)
#define COL3 (COL2+COL)

    clearKeyWin();

    boxWin( keyWin, KEYWIN_H, KEYWIN_W );

    y = 0;

    wattron( keyWin, A_BOLD );
    x = _max( ( ( KEYWIN_W - strlen( title ) )/2 ), 1 );
    mvwaddstr( keyWin, y, x, title );
    wattroff( keyWin, A_BOLD );
    y++;

    wmove( keyWin, y, COL1 );
    wattron( keyWin, A_BOLD );
#ifdef VMS
    waddstr( keyWin, "^Z" );
#else
    waddstr( keyWin, "^D" );
#endif
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=cancel       " );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "^E" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=exit" );
    y++;
    wmove( keyWin, y, COL1 );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Tab" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=main menu   " );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "^H" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=help" );

    y=1;
    wmove( keyWin, y, COL2 );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Up" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "/" );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Down" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=move cursor" );
    y++;
    wmove( keyWin, y, COL2 );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Left" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "/" );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Right" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=change path" );

    y=1;
    wmove( keyWin, y, COL3 );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Return" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=select variable" );
    y++;
    wmove( keyWin, y, COL3 );
    wattron( keyWin, A_BOLD );
    waddstr( keyWin, "Space" );
    wattroff( keyWin, A_BOLD );
    waddstr( keyWin, "=update variables" );

    refreshKeyWin();
}

