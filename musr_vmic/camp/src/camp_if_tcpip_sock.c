/*
 *  Name:       camp_if_tcpip_sock.c
 *
 *  Purpose:    Provides TCP/IP socket communications interface type.
 *              Implementation using standard tcp sockets on vxworks, which
 *              will be much the same on other systems (not tried yet).
 *
 *              A CAMP tcpip interface definition must provide the following
 *              routines:
 *                int if_tcpip_init ( void );
 *                int if_tcpip_online ( CAMP_IF *pIF );
 *                int if_tcpip_offline ( CAMP_IF *pIF );
 *                int if_tcpip_read( REQ* pReq );
 *                int if_tcpip_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini.
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation, the global mutex is unlocked
 *              during calls to connectwithtimeout(), send(), select(),
 *              and recv().
 *
 *
 *  Notes:
 *
 *     Ini file configuration string:
 *        {}
 *
 *     Meaning of private (driver-specific) variables:
 *        pIF_t->priv - not used
 *        pIF->priv - socket descriptor
 *
 *   $Log$
 *   Revision 1.1  2006/04/27 04:08:23  asnd
 *   Create a tcpip socket interface type
 *
 *
 */

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "ioLibx.h"
#include "types.h"
#include "socket.h" 
#include "sockLib.h"

#include "camp.h"

static int if_tcpip_connect( CAMP_IF* pIF );
static void getTerm( char* term_in, char* term_out, int* len );

/* 
 *  Number of chunks that a read may receive.  (According to the rules, this
 *  only needs to be 1, because the socket collects the full text before 
 *  triggering select().)
 */
#define MAX_RECV_TRY 3

/* 
 *  This structure defines a socket and associated address.  It is linked in 
 *  to pIf->priv.  Note that sockaddr_in is interchangeable with sockaddr 
 *  structure type, but has more detailed subdivisions.
 */

typedef struct socketDrv_str {
  int	 Fd;
  struct sockaddr_in SockAddr;
  struct timeval tv_timeout;
} socketDrv_t;

typedef struct socketDrv_str *SOCKET_ID;


int
if_tcpip_init( void )
{
    /*
     *  Don't check anything yet.
     */
    return( CAMP_SUCCESS );
}

static int
if_tcpip_connect( CAMP_IF* pIF )
{
  int stat;
  int yes = 1;
  char addr[LEN_NODENAME+1];
  int port;
  float timeout;
  socketDrv_t* pSock;
  int sockAddrSize = sizeof (struct sockaddr_in);

#ifdef DEBUG
  printf("Interface parameters are %s\n",pIF->defn);
#endif

  pSock = (socketDrv_t*)pIF->priv;

  if( pSock->Fd > 0 ) {
#ifdef DEBUG
    printf("Socket was left open -- close\n");
#endif
    close( pSock->Fd );
    pSock->Fd = 0;
  }

  camp_getIfTcpipAddr( pIF->defn, addr );
  port = camp_getIfTcpipPort( pIF->defn );
  timeout = camp_getIfTcpipTimeout( pIF->defn );

  /*
   * Fill in the timeout parameter, rounded to 1/10 second
   */
  pSock->tv_timeout.tv_sec = (long int)timeout;
  pSock->tv_timeout.tv_usec = (long int)( 10.0 * (timeout - pSock->tv_timeout.tv_sec) ) * 100000;

  /*
   * Fill in some connection address parameters
   */
  bzero ((char*)&pSock->SockAddr, sockAddrSize);
  pSock->SockAddr.sin_family = AF_INET;
  pSock->SockAddr.sin_port = htons(port);
  pSock->SockAddr.sin_addr.s_addr = inet_addr(addr);

  /*
   *  Open the socket connection.
   */ 

  pSock->Fd = socket( AF_INET, SOCK_STREAM, 0 );

  if (pSock->Fd == ERROR) {
#ifdef DEBUG
    printf("tcpip failed to create socket" );
#endif
    camp_appendMsg( "tcpip failed to create socket" );
    goto error;
  }

#ifdef DEBUG
  printf("Connect to sock %d with timeout %.1f\n",pSock->Fd,timeout);
#endif
#ifdef MULTITHREADED
  thread_unlock_global_np();
#endif /* MULTITHREADED */
  stat = connectWithTimeout(pSock->Fd, 
                            (struct sockaddr *)&pSock->SockAddr,
                            sizeof(struct sockaddr_in),
                            &pSock->tv_timeout
                           );
  /* Or maybe use a different timeout value for the connection */

#ifdef MULTITHREADED
  thread_lock_global_np();
#endif /* MULTITHREADED */

  if( stat != OK ) {
#ifdef DEBUG
    printf("Failed connection to sock %d: %d\n",pSock->Fd,stat);
#endif
    camp_appendMsg( "failure opening connection" );
    goto error;
  }
#ifdef DEBUG
  printf("Connected to sock %d with status %d\n",pSock->Fd,stat);
#endif

  /*
   *  If necessary...
   *  Set real-time operation -- no delays.
   *  Set blocking IO.
   */
  /*
   *  setsockopt (pSock->Fd, SOL_SOCKET, TCP_NODELAY, (char*)(&yes), sizeof(yes));
   *  stat = ioctl ( pSock->Fd, FIONBIO, (int)&yes );
   */

  return( CAMP_SUCCESS );

 error:

  if( pSock->Fd > 0 ) {
    close( pSock->Fd );
    pSock->Fd = 0;
  }
  return( CAMP_FAILURE );

}


int
if_tcpip_online( CAMP_IF* pIF )
{
  socketDrv_t* pSock;

  /*
   * Allocate the socket descriptor
   */
  pIF->priv = (long) malloc (sizeof(socketDrv_t));
  pSock = (socketDrv_t*)pIF->priv;
  pSock->Fd = 0;

  return( CAMP_SUCCESS );
}


int
if_tcpip_offline( CAMP_IF* pIF )
{
  socketDrv_t* pSock;
  pSock = (socketDrv_t*)pIF->priv;
  
  if( pSock->Fd > 0 ) {
    close( pSock->Fd );
  }
  free( (void*)pSock );
  pIF->priv = (long)NULL;

  return( CAMP_SUCCESS );
}


/*
 *  if_tcpip_read.  synchronous write, then read with timeout
 */
int
if_tcpip_read( REQ* pReq )
  {
    CAMP_IF* pIF;
    char* command;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    int stat;
    int j;
    int nfds;
    char* buf;
    int buf_len;
    char* bufBegin;
    char* eol;
    int* pRead_len;
    int nread;
    int avail;
    socketDrv_t* pSock;
    fd_set readfds;
    timeval_t tv;
#ifdef DEBUG
    char dbuf[512], dbuf1[32];
#endif /* DEBUG */

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    bufBegin =buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
    pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
    *pRead_len = 0;

    /*
     *  Open the connection and make settings
     */ 
    pSock = (socketDrv_t*)pIF->priv;

    if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) {
      return( CAMP_FAILURE );
    }

    /*
     *  Write the command
     */
    getTerm( camp_getIfTcpipWriteTerm( pIF->defn, term ), term, &term_len );
#ifdef DEBUG
    strncpy( dbuf, cmd, cmd_len );
    dbuf[cmd_len] = '\0';
    printf( "term_len = %d\n", term_len );
    if( term_len > 0 ) sprintf( &dbuf[cmd_len], "\\%03o", term[0] );
    if( term_len > 1 ) sprintf( &dbuf[cmd_len+4], "\\%03o", term[1] );
    printf( "writing '%s'\n", dbuf );
#endif /* DEBUG */

    if( cmd_len > 0 )
      {
        command = (char*)malloc( cmd_len + term_len + 1 );
        strncpy( command, cmd, cmd_len );
        strncpy( &command[cmd_len], term, term_len );
        command[cmd_len+term_len] = '\0';

#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */

        stat = send(pSock->Fd, command, cmd_len+term_len, 0);

#ifdef MULTITHREADED
        thread_lock_global_np();
#endif /* MULTITHREADED */

        free( (void*)command );

        if( stat == ERROR ) {
          camp_appendMsg( "failed to send request" );
          goto error;
        }

        if( stat < cmd_len+term_len ) {
          camp_appendMsg( "sent request incomplete (%d of %d)", stat, cmd_len+term_len );
          goto error;
        }

      }


    /*
     *  Read response with timeout
     */
    buf[0] = '\0';

    getTerm( camp_getIfTcpipReadTerm( pIF->defn, term ), term, &term_len );

    /*
     *  select() for sockets of type SOCK_STREAM, supposedly checks that the
     *  virtual socket corresponding to the socket has been closed, so we
     *  shouldn't have to loop over fragments of the response--it all comes
     *  at once.  But there has been flakiness, particularly with recv()
     *  hanging after select() returns!  Therefore, we will double-check that
     *  there is some text to read.  The for() loop is limited to MAX_RECV_TRY
     *  iterations because the select() always indicates a readable condition after
     *  the (virtual) socket gets closed. 
     */

    nread = 0;

    for( j = 0 ; j < MAX_RECV_TRY ; j++ )
      {

        tv.tv_sec = pSock->tv_timeout.tv_sec;
        tv.tv_usec = pSock->tv_timeout.tv_usec;

        FD_ZERO( &readfds );
        FD_SET( pSock->Fd, &readfds );

#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */

        nfds = select( (pSock->Fd)+1, &readfds, NULL, NULL, &tv );

#ifdef MULTITHREADED
        thread_lock_global_np();
#endif /* MULTITHREADED */

        if( nfds == -1 )
          {
            /* 
             *  Error
             */
            if( errno == EINTR ) continue;
            camp_appendMsg( "select failure" );
            goto error;
          }
        if( nfds == 0 )
          {
            /* 
             *  Timeout
             */
#ifdef DEBUG
            printf( "Timeout from select.\n");
#endif /* DEBUG */
            camp_appendMsg( "timeout during read; got '%s'", bufBegin );
            goto error;
          }
        
        if( !FD_ISSET( pSock->Fd, &readfds ) ) continue;
        
        /*
         *  The socket's fd has been stimulated...
         */
        stat = ioctl( pSock->Fd, FIONREAD, (int)&avail );
#ifdef DEBUG
        printf( "%d Socket has %d bytes available\n", j, avail );
#endif /* DEBUG */
        if( avail < 1 ) 
          {
            /*
             * There were actually no characters readable, so wait a bit and retry.
             * In practice, the retries don't seem to get any characters either.
             */
            camp_fsleep( 0.05 );
            if ( j+1 == MAX_RECV_TRY && *pRead_len == 0 ) {
              goto error;
            }
            continue;
          }
        
        if( buf_len == 0 )
          {
#ifdef DEBUG
            printf( "something to read but buffer is full\n" );
#endif /* DEBUG */
            break;
          }
        
#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */
        
#ifdef DEBUG
        printf( "if_tcpip_read: doing receive  %d...", j );
#endif
        
        nread = recv( pSock->Fd, buf, buf_len, 0 );
        
#ifdef MULTITHREADED
        thread_lock_global_np();
#endif /* MULTITHREADED */
        
#ifdef DEBUG
        strncpy( dbuf1, buf, nread );
        dbuf1[nread] = '\0';
        stoprint_expand( dbuf1, dbuf );
        printf( "received %d '%s'\n", nread, dbuf );
#endif /* DEBUG */
        
        buf += nread;
        buf_len -= nread;
        *pRead_len += nread;
        buf[0] = '\0';
        
        if( buf_len == 0 ) break; /* buffer is full, exit from loop */

        /*
         *  Check if we have terminator yet.  If so, truncate string there.
         */
        if( term_len > 0 )
          {
            eol = strstr( bufBegin, term );
            if( eol != NULL )
              {
                *eol = '\0';
                *pRead_len = (int)(eol - bufBegin);
              }
            break; /* from for loop */
          }
        else 
          {
            /*
             * For the case of no terminator, accept the first chunk of text
             * and return.  Do *not* wait for a timeout as we do with rs232.
             */
            break; /* from for loop */
          }
        
      } /* end for(;;) loop */
    
    close( pSock->Fd );
    pSock->Fd = 0;

    return( CAMP_SUCCESS );

  error:
    close( pSock->Fd );
    pSock->Fd = 0;

    return( CAMP_FAILURE );
}


/*
 *  if_tcpip_write,  synchronous write 
 */
int 
if_tcpip_write( REQ* pReq )
{
    CAMP_IF* pIF;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char* command;
    int stat;
    socketDrv_t* pSock;
#ifdef DEBUG
    char dbuf[512], dbuf1[32];
#endif /* DEBUG */

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

    pSock = (socketDrv_t*)pIF->priv;

    if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) {
      return( CAMP_FAILURE );
    }

    /*
     *  Write the command
     */
    getTerm( camp_getIfTcpipWriteTerm( pIF->defn, term ), term, &term_len );
#ifdef DEBUG
    strncpy( dbuf, cmd, cmd_len );
    dbuf[cmd_len] = '\0';
    printf( "term_len = %d\n", term_len );
    if( term_len > 0 ) sprintf( &dbuf[cmd_len], "\\%03o", term[0] );
    if( term_len > 1 ) sprintf( &dbuf[cmd_len+4], "\\%03o", term[1] );
    printf( "writing '%s'\n", dbuf );
#endif /* DEBUG */

    if( cmd_len > 0 )
      {
        command = (char*)malloc( cmd_len + term_len + 1 );
        strncpy( command, cmd, cmd_len );
        strncpy( &command[cmd_len], term, term_len );
        command[cmd_len+term_len] = '\0';

#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */

        stat = send(pSock->Fd, command, cmd_len+term_len, 0);

#ifdef MULTITHREADED
        thread_lock_global_np();
#endif /* MULTITHREADED */

        free( (void*)command );

        if( stat == ERROR ) {
          camp_appendMsg( "failed write" );
          goto error;
        }

        if( stat < cmd_len+term_len ) {
          camp_appendMsg( "incomplete write (%d of %d)", stat, cmd_len+term_len );
          goto error;
        }

      }

    close( pSock->Fd );
    pSock->Fd = 0;

    return( CAMP_SUCCESS );

error:
    close( pSock->Fd );
    pSock->Fd = 0;

    return( CAMP_FAILURE );
}


static void
getTerm( char* term_in, char* term_out, int* len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
      case TOK_LF:   strcpy( term_out, "\012" );     *len = 1; return;
      case TOK_CR:   strcpy( term_out, "\015" );     *len = 1; return;
      case TOK_CRLF: strcpy( term_out, "\015\012" ); *len = 2; return;
      case TOK_LFCR: strcpy( term_out, "\012\015" ); *len = 2; return;
      case TOK_NONE: strcpy( term_out, "" );         *len = 0; return;
      default:       strcpy( term_out, "" );         *len = 0; return;
    }
}
