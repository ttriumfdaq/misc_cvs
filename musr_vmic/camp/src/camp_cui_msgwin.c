/*
 *  Name:       camp_cui_msgwin.c
 *
 *  Purpose:    Routines to manage a generic popup message window
 *
 *  Called by:
 * 
 *  Revision history:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <curses.h>
#include "timeval.h"
#include "camp_cui.h"

#define msgWin_H 18
#define msgWin_W 60
#define msgWin_Y ((SCREEN_H-msgWin_H)/2)
#define msgWin_X ((SCREEN_W-msgWin_W)/2)

WINDOW* msgWin = NULL;
WINDOW* msgTextWin = NULL;

void startMessage( char* title, char* msg );
void deleteMessage( void );
void addMessage( char* msg );
void confirmMessage( void );


void
touchMessage( void )
{
    if( msgWin != NULL ) touchwin( msgWin );
    if( msgTextWin != NULL ) touchwin( msgTextWin );
}


void
refreshMessage( void )
{
    if( msgWin != NULL ) wrefresh( msgWin );
    if( msgTextWin != NULL ) wrefresh( msgTextWin );
}


void
startMessage( char* title, char* msg )
{
    msgWin = newwin( msgWin_H, msgWin_W, msgWin_Y, msgWin_X ); 
    if( msgWin == NULL )
    {
        return;
    }
    leaveok( msgWin, FALSE );
    scrollok( msgWin, FALSE );
    keypad( msgWin, TRUE );
    immedok( msgWin, FALSE );
    clearok( msgWin, FALSE );
    wtimeout( msgWin, -1);

    msgTextWin = newwin( msgWin_H-2, msgWin_W-2, msgWin_Y+1, msgWin_X+1 ); 
    if( msgWin == NULL )
    {
        return;
    }

    scrollok( msgTextWin, TRUE );
    leaveok( msgTextWin, FALSE );
    keypad( msgTextWin, TRUE );
    immedok( msgTextWin, FALSE );
    clearok( msgTextWin, FALSE );
    wtimeout( msgTextWin, -1);

    boxWin( msgWin, msgWin_H, msgWin_W );
    wattron( msgWin, A_BOLD );
    mvwaddstr( msgWin, 0, ( msgWin_W - strlen( title ) )/2, title );
    wattroff( msgWin, A_BOLD );
    wrefresh( msgWin );
    wmove( msgTextWin, 0, 0 );
    addMessage( msg );
}


void 
deleteMessage( void )
{
    if( msgTextWin != NULL ) 
    {
        delwin( msgTextWin );
        msgTextWin = NULL;
    }
    if( msgWin != NULL ) 
    {
        delwin( msgWin );
        msgWin = NULL;
    }
    uncoverWindows();
}


void
addMessage( char* msg )
{
    int y, x;
    char* p;

    for( p = msg; *p != '\0'; p++ )
    {
        getyx( msgTextWin, y, x );
        if( *p == '|' )
        {
            if( x != 0 )
            {
                if( y >= ( msgWin_H-3 ) )
                {
                    scroll( msgTextWin );
                    wmove( msgTextWin, msgWin_H-3, 2 );
                }
                else
                {
                    wmove( msgTextWin, y+1, 2 );
                }
            }
            while( isspace( *(p+1) ) ) p++;
        }
        else if( x == ( msgWin_W-3 ) )
        {
            if( *p != '\n' ) 
            {
                waddch( msgTextWin, *p );
/*                if( *(p+1) == '\n' ) p++;
*/
            }

            if( y >= ( msgWin_H-3 ) )
            {
                scroll( msgTextWin );
                wmove( msgTextWin, msgWin_H-3, 2 );
            }
            else
            {
                wmove( msgTextWin, y+1, 2 );
            }
            while( isspace( *(p+1) ) ) p++;
        }
        else
        {
            waddch( msgTextWin, *p );
        }
    }
    wrefresh( msgTextWin );
}


void
confirmMessage( void )
{
    int y, x;
    char* continueText = "Press any key to continue";

    getyx( msgTextWin, y, x );

    if( y >= ( msgWin_H-3 ) )
    {
        scroll( msgTextWin );
	wmove( msgTextWin, msgWin_H-3, 0 );
    }

    getyx( msgTextWin, y, x );
    if( x > 0 ) y++;
    wmove( msgTextWin, y, (msgWin_W-2-strlen(continueText))/2 );

    wattron( msgTextWin, A_REVERSE );
    addMessage( continueText );
    wattroff( msgTextWin, A_REVERSE );
    addMessage( "\n" );
    getKey();
}


