/*
 *  Name:       camp_alarm_priv.c
 *
 *  Purpose:    CAMP alarm related routines called from the CAMP server
 *
 *  $Log$
 *  Revision 1.3  2001/12/19 18:08:55  asnd
 *  Record the time at which alarms change their states
 *
 *  Revision 1.2  2001/07/19 02:22:35  asnd
 *  Make sure alarmAction gets switched off when an alarm is turned
 *  off or changed.
 *
 */

#include <stdio.h>
#include "camp_srv.h"


/*
 *  Name:       camp_alarmCheck
 *
 *  Purpose:    To initiate alarm actions when variables get an "alert"
 *              state.  There may be any number of alarm actions defined in
 *              CAMP which are defined by Tcl commands in the CAMP
 *              initialization file camp.ini.  CAMP variables may be set
 *              so that when they show an alarm, one of these alarm actions
 *              is triggered.
 *
 *              We will interpret the Tcl proc associated with an alarm using
 *              the Tcl interpreter of the main CAMP server thread.
 *
 *  Called by:  varSetAlert (camp_var_priv.c)
 *              This routine is (ultimately) called from the Tcl command
 *              varTestAlert which is normally called from variable
 *              read/write procs in the driver.
 * 
 *  Inputs:     The input is an alarm indentifier string, equivalent to
 *              the identifiers created using the sysAddAlarmAct Tcl command
 *              in the camp.ini file.
 *
 *  Preconditions:
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *
 *  Revision history:
 *    10-Dec-2001  DA  Record the time at which alarms change their states, 
 *                     both in the alarms area AND indicate a system change --
 *                     because alarm states are stored with the system data.
 *    20-Dec-1999  TW  Always use main Tcl interpreter for alarms.  
 *                     Was previously potentially using the instrument
 *                     interpreter.  Alarm procs are only defined in the
 *                     main interpreter (i.e., when the server initializes
 *                     by reading the camp.ini file).
 *
 */
void
camp_alarmCheck( char* alarmAction )
{
    int status;
    ALARM_ACT* pAlarmAct;
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    /*
     *  20-Dec-1999  TW  Changed.  Always use main Tcl interpreter
     */
    /* if( ( interp = get_thread_interp() ) == camp_tclInterp() ) */
    if( ( interp = camp_tclInterp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    pAlarmAct = camp_sysGetpAlarmAct( alarmAction );
    if( pAlarmAct == NULL ) return;

    pAlarmAct->status &= ~CAMP_ALARM_ON;

    /*
     *  Check to see if any data are ALERTed
     */
    camp_varDoProc_recursive( alarm_update, pVarList );

    if( pAlarmAct->status & CAMP_ALARM_ON )
    {
        /* 
         *  Set alarm ON if it is not already
         */
        if( !( pAlarmAct->status & CAMP_ALARM_WAS_ON ) )
        {
#if CAMP_DEBUG
	    if( camp_debug > 1 ) 
	        printf( "setting alarm %s on (%s, %d, %p)\n", alarmAction,
		       pAlarmAct->proc, useMainInterp, interp );
#endif /* CAMP_DEBUG */

            if( useMainInterp ) set_global_mutex_noChange( TRUE );
	    status = Tcl_VarEval( interp, pAlarmAct->proc, " {1}", NULL );
            if( useMainInterp ) set_global_mutex_noChange( FALSE );
            if( status == TCL_ERROR ) 
            {
                camp_appendMsg( "camp_alarmCheck: failure: alertProc (%s)", 
                            pAlarmAct->ident );
            }
            else
            {
                pAlarmAct->status |= CAMP_ALARM_WAS_ON;
                gettimeval( &pAlarmAct->timeLastSet );
                copytimeval( &pAlarmAct->timeLastSet,
                             &pSys->pDyna->timeLastSysChange );
            }
        }
    }
    else
    {
        /* 
         *  Set alarm OFF if it is not already
         */
        if( pAlarmAct->status & CAMP_ALARM_WAS_ON )
        {
#if CAMP_DEBUG
	  if( camp_debug > 1 )
	    printf( "setting alarm %s off (%s, %d, %p)\n", alarmAction,
		   pAlarmAct->proc, useMainInterp, interp );
#endif /* CAMP_DEBUG */

            if( useMainInterp ) set_global_mutex_noChange( TRUE );
	    status = Tcl_VarEval( interp, pAlarmAct->proc, " {0}", NULL );
            if( useMainInterp ) set_global_mutex_noChange( FALSE );
            if( status == TCL_ERROR ) 
            {
                camp_appendMsg( "camp_alarmCheck: failure: alertProc (%s)", 
                            pAlarmAct->ident );
            }
            else
            {
                pAlarmAct->status &= ~CAMP_ALARM_WAS_ON;
                gettimeval( &pAlarmAct->timeLastSet );
                copytimeval( &pAlarmAct->timeLastSet,
                             &pSys->pDyna->timeLastSysChange );
            }
        }
    }
}


/*
 *  Name:       alarm_update
 *
 *  Purpose:    Check a CAMP variable, if it has an alarm active then set
 *              the status of the corresponding alarm to ON.
 *
 *  Called by:  camp_alarmCheck (recursively using camp_varDoProc_recursive)
 *              (camp_varDoProc_recursive traverses the linked list calling
 *              a function for every list member).
 * 
 *  Inputs:     A pointer to a CAMP variable structure
 *
 *  Preconditions:
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */
void
alarm_update( CAMP_VAR* pVar )
{
    if( pVar->core.status & CAMP_VAR_ATTR_ALERT )
    {
        ALARM_ACT* pAlarmAct;

        pAlarmAct = camp_sysGetpAlarmAct( pVar->core.alarmAction );
        if( pAlarmAct == NULL ) return;

        pAlarmAct->status |= CAMP_ALARM_ON;
    }
}


