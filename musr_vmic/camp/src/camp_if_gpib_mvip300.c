/*
 *  Name:       camp_if_gpib_mvip300.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *              Routines to interface with GPIB instruments by way of an
 *              MVIP300 Industry Pack on an MVME processor.  Low level
 *              routines were supplied by a third-party (thank goodness).
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation a GPIB semaphore is used to lock
 *              access to the GPIB bus during communications.  During
 *              this time it is safe to unlock the global mutex (using
 *              thread_unlock_global_np).
 *
 *  Notes:
 *
 *     timeout = 1 to 17 (time = 1e((timeout+1)/2)usec, see ugpib.h)
 *
 *     INI file configuration string:
 *         none
 *
 *     Meaning of private (driver-specific) variables:
 *         pIF_t->priv == 1 (gpib available)
 *                     == 0 (gpib unavailable)
 *         pIF->priv   - pointer to GPIB_PHYS_DEV structure
 *
 *
 *  Revision history:
 *
 *   16-Dec-1999  TW  CAMP_DEBUG conditional compilation
 *   20-Dec-1999  TW  Added dvclr before every dvwrt call.  Found that
 *                    a clear before each write makes the LakeShore 330
 *                    much more reliable.  Haven't tested the affect on 
 *                    other instruments.
 *   21-Dec-1999  TW  Syd says that this is excessive.  Some devices
 *                    (e.g., HP devices) use a GPIB device clear as
 *                    more than an interface clear - it can affect the
 *                    operational state of the device.
 *                    Remove the dvclr, but consider as a separate Tcl
 *                    command to be called from instrument drivers.
 *   21-Dec-1999  TW  Most references to gpibStatus global variable
 *                    moved inside semGPIB take/give.  Should be all.
 *   13-Dec-2000  TW  Found that GPIB operations holding all other tasks
 *                    up on VxWorks.  Knocking down the priority during
 *                    read and write operations lets other tasks go.
 *                    The code for this was taken from gpibLib.c, where
 *                    it was already done.
 *   19-Dec-2008  DA  The previous change was involved with Camp hangups
 *                    that leave all gpib communication locked even after  
 *                    a soft reboot (one needed to press the reset button 
 *                    or cycle the (vme) power on the camp server).  Moving
 *                    the priority change so that the priority is lowered 
 *                    before waiting for the gpib semaphore improves but
 *                    does not cure the problem. The priority-changing is 
 *                    removed.
 */

#include <stdio.h>
#include "camp_srv.h"
#include "ugpib.h"
#define GPIBLIB_H    /* bring in global variables */
#include "gpibLib.h"

#define if_gpib_ok  (pIF_t->priv!=0)

static void getTerm( char* term_in, char* term_out, int* term_len );
/*  New 13-Dec-2000  priority_down, priority_set  */
/*  New 19-Dec-2008  Eliminate priority_down, priority_set  */
/*
 * static int priority_down( int down_amount );
 * static void priority_set( int iPriority );
 * #define GPIB_PRIORITY_REDUCTION 10
 */

int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /* 
     *  priv holds the GPIB_PHYS_DEV pointer
     *  The value is zero if GPIB is unavailable
     */
    pIF_t->priv = 0; 

    /*
     *  Do initialization in MVME startup command file
     *  So assume it's OK
     *  Could I check for it somehow?
     */

    pIF_t->priv = 1;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    if( ( ( addr = camp_getIfGpibAddr( pIF->defn ) ) < 0 ) ||
          ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    pIF->priv = (long)gpibPhysDevCreate( addr );
    if( pIF->priv == 0 )
    {
        camp_appendMsg( "failed allocating gpib device" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;
    int status;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    status = gpibPhysDevDelete( (GPIB_PHYS_DEV*)pIF->priv );
    if( status == ERROR )
    {
        camp_appendMsg( "failed deleting gpib device" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    GPIB_PHYS_DEV* pGpibItem;
    char* command;
    int maxReadCount;
    int count;
    int readCount;
    int endOfString;
    char term_test;
    /*    int oldPriority; */
#define MAX_READ 0xFFFE /* Same as IBMAXIO */

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    pGpibItem = (GPIB_PHYS_DEV*)pIF->priv;
    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

    getTerm( camp_getIfGpibWriteTerm( pIF->defn, str ), term, &term_len );

    command = (char*)malloc( cmd_len+term_len+1 );
    strncpy( command, cmd, cmd_len );
    strncpy( &command[cmd_len], term, term_len );
    command[cmd_len+term_len] = '\0';

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
    ibeos( endOfString );

#if CAMP_DEBUG
    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
      printf( "sending gpib command '%s' (%d)\n", command, cmd_len+term_len );
    }
#endif /* CAMP_DEBUG */

#ifdef MULTITHREADED
    thread_unlock_global_np();
    /*  
     *  Get the current priority and slow it down so that the 
     *  GPIB driver does not block other tasks
     */
    /* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */

#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );
    /* priority_down was here */

    count = cmd_len + term_len; 
    /*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
     *                   you send a clear every time.  Is this excessive?
     *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
     *                   (e.g., HP devices) use a GPIB device clear as
     *                   more than an interface clear - it can affect the
     *                   operational state of the device.
     *                   Remove this, but consider as a separate Tcl
     *                   command to be called from instrument drivers.
     */
    /* gpibStatus = dvclr( pGpibItem->iPriBusId ); */
    gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
    gpibError = iberr;
    /*
     *  Often get addressing error, but
     *  works if repeat command
     */
    if( ( gpibStatus & ERR ) && ( gpibError == EADR ) )
    {
      /*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
       *                   you send a clear every time.  Is this excessive?
       *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
       *                   (e.g., HP devices) use a GPIB device clear as
       *                   more than an interface clear - it can affect the
       *                   operational state of the device.
       *                   Remove this, but consider as a separate Tcl
       *                   command to be called from instrument drivers.
       */
      /* gpibStatus = dvclr( pGpibItem->iPriBusId ); */
      gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
      gpibError = iberr;
    }
    status = ( gpibStatus & ERR ) ? ERROR : OK;

    /* moved priority_set down */

    semGive( semGPIB );

#ifdef MULTITHREADED
    /* priority_set( oldPriority );*/  /*  Set priority back  */
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status != ERROR )
      {
#if CAMP_DEBUG
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: wrote %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( command[i] ) ) printf( "%c", command[i] );
            else printf( "\\%03o", command[i] );
    	  }
          printf( "'\n" );
        }
#endif /* CAMP_DEBUG */
      }

    free( command );

    if( status == ERROR )
    {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on write" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib write" );
	  }

#if CAMP_DEBUG
        if( camp_debug > 0 )
        {
          printf( "gpib write failed, status = %08X\n", gpibStatus );
        }
#endif /* CAMP_DEBUG */

        return( CAMP_FAILURE );
    }

    readCount = 0;

    getTerm( camp_getIfGpibReadTerm( pIF->defn, str ), term, &term_len );

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
#if CAMP_DEBUG
    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
    }
#endif /* CAMP_DEBUG */

    while( readCount < buf_len )
    {
      maxReadCount = ( buf_len-readCount < MAX_READ ) ? 
                       buf_len-readCount : MAX_READ;

#ifdef MULTITHREADED
      thread_unlock_global_np();
      /*  
       *  Get the current priority and slow it down so that the 
       *  GPIB driver does not block other tasks
       */
      /* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */
#endif /* MULTITHREADED */

#if CAMP_DEBUG
      if( camp_debug > 0 ) printf( "if_gpib_read: gave up global, taking gpib semaphore\n" );
#endif /* CAMP_DEBUG */

      semTake( semGPIB, WAIT_FOREVER );

#if CAMP_DEBUG
      if( camp_debug > 0 ) printf( "if_gpib_read: got gpib semaphore, doing read...\n" );
#endif /* CAMP_DEBUG */

      ibeos( endOfString );

      gpibStatus = dvrd( pGpibItem->iPriBusId, &buf[readCount], maxReadCount );
      gpibError = iberr;
      count = ibcnt;
      status = ( gpibStatus & ERR ) ? ERROR : OK;

#if CAMP_DEBUG
      if( camp_debug > 0 ) printf( "if_gpib_read: done read, giving up gpib semaphore\n" );
#endif /* CAMP_DEBUG */

      semGive( semGPIB );

#if CAMP_DEBUG
      if( camp_debug > 0 ) printf( "if_gpib_read: taking global\n" );
#endif /* CAMP_DEBUG */

#ifdef MULTITHREADED
      /* priority_set( oldPriority );*/  /*  Set priority back  */
      thread_lock_global_np();
#endif /* MULTITHREADED */

#if CAMP_DEBUG
      if( camp_debug > 0 ) printf( "if_gpib_read: got global\n" );
#endif /* CAMP_DEBUG */

      if( status == ERROR )
      {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on read" );
#if CAMP_DEBUG
	    if( camp_debug > 0 ) printf( "if_gpib_read: timeout on read\n" );
#endif /* CAMP_DEBUG */
	  }
	else
	  {
	    camp_appendMsg( "failed gpib read" );
#if CAMP_DEBUG
	    if( camp_debug > 0 ) printf( "if_gpib_read: failed read\n" );
#endif /* CAMP_DEBUG */
	  }
        return( CAMP_FAILURE );
      }
      else
      {
#if CAMP_DEBUG
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: read %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( buf[readCount+i] ) ) printf( "%c", buf[readCount+i] );
            else printf( "\\%03o", buf[readCount+i] );
    	  }
          printf( "'\n" );
        }
#endif /* CAMP_DEBUG */

        readCount += count;
      }
      /* 
       *  Could put check for termination here,
       *  instead just quit after one read
       */
      break;
    }

    /*
     *  Return number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = readCount;

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    GPIB_PHYS_DEV* pGpibItem;
    char* command;
    int endOfString;
    int count;
    /* int oldPriority; */

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    pGpibItem = (GPIB_PHYS_DEV*)pIF->priv;
    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

    getTerm( camp_getIfGpibWriteTerm( pIF->defn, str ), term, &term_len );

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
    ibeos( endOfString );

    command = (char*)malloc( cmd_len+term_len+1 );
    strncpy( command, cmd, cmd_len );
    strncpy( &command[cmd_len], term, term_len );
    command[cmd_len+term_len] = '\0';

#if CAMP_DEBUG
    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
      printf( "sending gpib command '%s'\n", command );
    }
#endif /* CAMP_DEBUG */

#ifdef MULTITHREADED
    thread_unlock_global_np();
    /*  
     *  Get the current priority and slow it down so that the 
     *  GPIB driver does not block other tasks
     */
    /* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */
#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );

    count = cmd_len + term_len;
    /*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
     *                   you send a clear every time.  Is this excessive?
     *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
     *                   (e.g., HP devices) use a GPIB device clear as
     *                   more than an interface clear - it can affect the
     *                   operational state of the device.
     *                   Remove this, but consider as a separate Tcl
     *                   command to be called from instrument drivers.
     */
    /* gpibStatus = dvclr( pGpibItem->iPriBusId ); */
    gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
    gpibError = iberr;
    /*
     *  Often get addressing error, but
     *  works if repeat command
     */
    if( ( gpibStatus & ERR ) && ( gpibError == EADR ) )
    {
      /*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
       *                   you send a clear every time.  Is this excessive?
       *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
       *                   (e.g., HP devices) use a GPIB device clear as
       *                   more than an interface clear - it can affect the
       *                   operational state of the device.
       *                   Remove this, but consider as a separate Tcl
       *                   command to be called from instrument drivers.
       */
      /* gpibStatus = dvclr( pGpibItem->iPriBusId ); */
      gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
      gpibError = iberr;
    }
    status = ( gpibStatus & ERR ) ? ERROR : OK;

    semGive( semGPIB );

#ifdef MULTITHREADED
    /* priority_set( oldPriority );*/  /*  Set priority back  */
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status != ERROR )
      {
#if CAMP_DEBUG
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: wrote %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( command[i] ) ) printf( "%c", command[i] );
            else printf( "\\%03o", command[i] );
    	  }
          printf( "'\n" );
        }
#endif /* CAMP_DEBUG */
      }

    free( command );

    if( status == ERROR )
    {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on write" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib write" );
	  }
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_gpib_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 */
int 
camp_gpib_clear( int gpib_addr )
{
    int status;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );

    gpibStatus = dvclr( gpib_addr );
    gpibError = iberr;
    /*
     *  Often get addressing error, but
     *  works if repeat command
     */
    if( ( gpibStatus & ERR ) && ( gpibError == EADR ) )
    {
      gpibStatus = dvclr( gpib_addr );
      gpibError = iberr;
    }
    status = ( gpibStatus & ERR ) ? ERROR : OK;
    semGive( semGPIB );

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status == ERROR )
    {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on clear" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib clear" );
	  }
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

/*
 *  camp_gpib_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 *                   For DELTAT implementation, GPIB_INIT would need
 *                   to be called again.
 */
int 
camp_gpib_timeout( int timeoutSeconds )
{
    int status, timeoutFlag;

    if( timeoutSeconds > 300 ) timeoutFlag = 17;
    else if( timeoutSeconds > 100 ) timeoutFlag = 16;
    else if( timeoutSeconds > 30 ) timeoutFlag = 15;
    else if( timeoutSeconds > 10 ) timeoutFlag = 14;
    else if( timeoutSeconds > 3 ) timeoutFlag = 13;
    else if( timeoutSeconds > 1 ) timeoutFlag = 12;
    else timeoutFlag = 11;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );
    gpibStatus = ibtmo( timeoutFlag );
    gpibError = iberr;
    status = ( gpibStatus & ERR ) ? ERROR : OK;
    semGive( semGPIB );

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status == ERROR )
    {
      camp_appendMsg( "failed camp_gpib_timeout" );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


static void
getTerm( char* term_in, char* term_out, int* term_len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
        case TOK_LF:   strcpy( term_out, "\012" );     *term_len = 1; return;
        case TOK_CR:   strcpy( term_out, "\015" );     *term_len = 1; return;
        case TOK_CRLF: strcpy( term_out, "\015\012" ); *term_len = 2; return;
        case TOK_LFCR: strcpy( term_out, "\012\015" ); *term_len = 2; return;
        case TOK_NONE: strcpy( term_out, "" );         *term_len = 0; return;
        default:       strcpy( term_out, "" );         *term_len = 0; return;
    }
}

/*
 * Not used now
 *
static int
priority_down( int down_amount )
{
  int tid, iPriority, iNewPriority;

  tid = taskIdSelf();

  taskPriorityGet( tid, &iPriority );

  iNewPriority = iPriority + down_amount;
  if( iNewPriority > 255 ) iNewPriority = 255;

  taskPrioritySet( tid, iNewPriority );

  return( iPriority );
}

static void
priority_set( int iPriority )
{
  taskPrioritySet( taskIdSelf(), iPriority );
}

*/

