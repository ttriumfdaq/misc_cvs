/*
 *  Name:       camp_if_camac_gen.c
 *
 *  Purpose:    Provides a CAMAC interface type.
 *              CAMAC interface routines implementing a generic CAMAC library.
 *
 *              Note that CAMAC implementation in CAMP is by way of
 *              ASCII command strings which are interpreted in a Tcl
 *              interpreter.  The routines if_camac_write and if_camac_read
 *              supplied here simply pass valid Camp-Tcl command strings to
 *              the interpreter.  Consequently, there doesn't need to be
 *              anything specific to a particular library in this file.
 *              The intent is that the command strings perform CAMAC access
 *              using commands provided by a particular library (cdreg, cfsa,
 *              etc.; see camp_tcl.c),
 *
 *              A CAMP CAMAC interface definition must provide the following
 *              routines:
 *                int if_camac_init ( void );
 *                int if_camac_online ( CAMP_IF *pIF );
 *                int if_camac_offline ( CAMP_IF *pIF );
 *                int if_camac_read( REQ* pReq );
 *                int if_camac_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP CAMAC interfaces one must be
 *              careful in considering the removal of the global lock during
 *              interpretation of the Tcl command.  The Tcl interpreter
 *              could be the main interpreter or an intrument interpreter
 *              and one must be sure about the safeness of the underlying
 *              CAMAC library.  For these reasons, CAMAC commands are called
 *              with the lock on.  This has not been a problem because
 *              there are normally few CAMAC commands in CAMP drivers, and
 *              they execute very quickly in comparison with other CAMP
 *              device type (RS232, GPIB).
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
if_camac_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int camac_if_type;

    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /*
     *  Interperate interface configuration info here
     *  (none for generic).
     */

    return( CAMP_SUCCESS );
}


int
if_camac_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_read( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed CAMAC read: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_write( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed CAMAC write: %s", interp->result );
      return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}
