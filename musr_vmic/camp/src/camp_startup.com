$ define/sys/exec/trans=conceal camp       dsk1:[camp.]
$ p7 = f$edit( f$getsyi( "startup_p7" ), "upcase,trim" )
$ camac_suffix = "if"
$ if ( p7 .eqs. "MVME" ) then camac_suffix = "rpc"
$ if ( p7 .eqs. "SCSI" ) then camac_suffix = "scsi"
$ if( f$sea( "camp:[vax-vms]camp_srv.exe" ) .nes. "" ) then -
          del camp:[vax-vms]camp_srv.exe;*
$ set file/enter=camp:[vax-vms]camp_srv.exe -
                 camp:[vax-vms]camp_srv_cam'camac_suffix'.exe
$ !
$ ! Start the CAMP Server (except if using CAMAC RPC)
$ !
$ if( p7 .eqs. "MVME" ) 
$ then
$   ! CAMP RPC host is same as CAMAC by default
$   define/sys/exec camp_host "''f$trnlnm("camac_rpc_host")'"
$ else
$   @camp:[000000]camp_srv
$ endif
$ exit
$ !
$ ! camp_startup.com  -  CAMP system-wide definitions
$ !                      Start CAMP server
$ !		         Call from systartup_vms.com at startup
$ !
$ ! Revision history:
$ !  25-Apr-1994  TW  Initial version
$ !  21-Jun-1994  TW  Server startup on queue camp_batch after 10 min.
$ !                         (necessary to wait for MultiNet)
$ !  22-Jun-1994  TW  Server startup on queue 'node'_startup
$ !  22-Jun-1994  TW  Executed after MultiNet up, don't submit
$ !  10-Oct-1995  TW  Use camp_root:, no more camp_dat:, camp_log:, etc.
$ !  31-Oct-1995  TW  camp_root: -> camp:
$ !  23-Feb-1996  TW  Support non-Q-Bus CAMAC interfaces
$ !
