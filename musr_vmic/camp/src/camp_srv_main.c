/*
 *  Name:       camp_srv_main.c
 *
 *  Purpose:    Main loop routines for the CAMP server
 *
 *  Called by:  None
 * 
 *  Revision history:
 *   v1.1  20-Apr-1994  TW  drivers in server, IFs generalized
 *   v1.3  23-Apr-1995  TW  Use _POSIX_THREADS to make server multithreaded
 *
 */

#include <stdio.h>
#include "timeval.h"
#include "camp_srv.h"
#ifdef VXWORKS
#include <rpc/rpcGbl.h>    /* for VxWorks RPC globals */
#endif /* VXWORKS */

/*
 *  Server polling definitions
 */
#define SRV_POLL_INTERVAL 1
#ifdef VMS
#include psldef
#define SRV_POLL_TIMER_ID 1
#endif

/*
 *  Auto save definitions
 */
#define AUTO_SAVE_INTERVAL 600
#define do_auto_save TRUE

/*
 *  Linked list head definitions
 */
CAMP_SYS* pSys = NULL;
CAMP_VAR* pVarList = NULL;

/*
 *  Misc. globals
 */
int camp_debug = 0;

extern void camp_srv_13(struct svc_req *rqstp, SVCXPRT *transp);
extern void srv_init_thread_once( void );

#ifdef MULTINET
extern void svc_run_once( void );  /* does it return void? */
#else
static void svc_run_once( void );
#endif
static int srv_init( void );
static void srv_loop_thread( void );
static void srv_loop( void );
static void my_svc_getreqset();

/*
 *  Name:       main (campSrv for VxWorks)
 *
 *  Purpose:    CAMP server main program
 *
 *              Initialize the CAMP server, then enter a loop waiting for
 *              RPC calls, and fulfilling requests (etc.).
 *
 *  Called by:  None
 * 
 *  Inputs:     Optional debug level
 *              For VxWorks: integer parameter
 *              For other  : -d [<integer>]
 *              The integer is the debug level.  A debug level of 1
 *              prints minimal information, a level of 2 prints lots of
 *              info.
 *
 *  Revision history:
 *
 */

#ifdef VXWORKS
int 
campSrv( int camp_debug_on )
#else /* !VXWORKS */
int 
main( int argc, char* argv[] )
#endif /* VXWORKS */
{
    int status;
    SVCXPRT* svc_transport;
#ifdef MULTITHREADED
    pthread_once_t t_init_once_block = pthread_once_init;
#endif /* MULTITHREADED */

#ifdef VXWORKS
    camp_debug = camp_debug_on;
#else
    if( ( argc > 1 ) && ( streq( argv[1], "-d" ) ) )
    {
        camp_debug = 1;
    }

    if( ( argc > 2 ) )
    {
        camp_debug = atoi( argv[2] );
    }
#endif /* VXWORKS */

#ifdef VXWORKS
    /*
     *  Make sure global linked lists are freed in case
     *  CAMP Server was stopped uncleanly
     */
    _xdr_free( xdr_CAMP_VAR, pVarList );
    _xdr_free( xdr_CAMP_SYS, pSys );

    /*
     *  Initialize VxWorks libraries
     *  Find that only rpcTaskInit is really necessary
     */
    /* stdioInit(); */
    selectInit();
    /* taskVarInit(); */
    rpcTaskInit();
#endif /* VXWORKS */

    /*
     *  Initialize an RPC TCP transport
     */
    svc_transport = svctcp_create( RPC_ANYSOCK, 0, 0 );
    if( svc_transport == NULL )
    {
        fprintf( stderr, "failure: svctcp_create\n" );
        exit( CAMP_FAILURE );
    }

    /*
     *  Unregister any other CAMP server with the RPC portmapper
     */
    pmap_unset( CAMP_SRV, CAMP_SRV_VERS );

    /*
     *  Register the program as the CAMP server with the RPC
     *  portmapper.
     */
    if( !svc_register( svc_transport, CAMP_SRV, CAMP_SRV_VERS, camp_srv_13, 
                       IPPROTO_TCP)) 
    {
        fprintf( stderr, "failure: svc_register\n" );
        exit( CAMP_FAILURE );
    }

#ifndef MULTITHREADED
    /* 
     *  Initialize exit handler
     */
    atexit( srv_rundown );
#endif /* MULTITHREADED */

    /*
     *  Global symbol to indicate exit status to shell
     */
#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "FAILURE", 1 );
#endif /* VMS */

    /*
     *  Direct messages to log file
     */
    status = camp_startLog( CAMP_SRV_LOG_FILE, TRUE );
    if( _failure( status ) )
    {
        fprintf( stderr, "failed to open log file\n" );
    }

#ifdef MULTITHREADED
    /* 
     *  Thread initialization
     *  sets up thread key data etc.
     */
    pthread_once( &t_init_once_block, srv_init_thread_once );

    /*
     *  Initialize the main thread
     *  In Multithreaded mode the CAMP server runs a main thread which
     *  dispatches RPC request to SVC threads, and starts Poll threads
     *  to do variable polling.
     */
    status = srv_init_main_thread();
    if( _failure( status ) )
    {
        exit( status );
    }
#endif /* MULTITHREADED */

    /*
     *  Initialize server
     */
    srv_init();

    /*
     *  Go into server loop
     */
    srv_loop_thread();

    fprintf( stderr, "srv_loop_thread returned\n" );
    exit( CAMP_FAILURE );
}


/*
 *  Name:       srv_loop_thread
 *
 *  Purpose:    CAMP server processing loop
 *
 *              Loop continuously doing regular server tasks, processing RPC
 *              requests and checking for a shutdown condition.  The loop
 *              has a 100 ms delay to be nice to system resources.
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server initialized completely
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              CAMP server should exit on returning from this routine
 *
 *  Revision history:
 *
 */
static void
srv_loop_thread( void )
{
  extern int try_find_free_thread( void );

    for( ;; )
    {
      /*
       *  Do regular server tasks
       */
#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "srv_loop_thread calling srv_loop..." );
#endif /* CAMP_DEBUG */
      srv_loop();
#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

      /*
       *  Dispatch pending RPC calls (i.e., from the CAMP CUI, camp_cmd
       *  or archiver).  Note that svc_run_once is called with the global
       *  thread locked.  This is necessary because the RPC library can not
       *  necessarily be trusted for re-entrancy.  The condition of the global
       *  lock on within this call is assumed for routines dispatched by
       *  svc_run_once which may unlock the global lock at certain times during
       *  long waiting periods.  This is a very important point to understand
       *  the operation of the CAMP server.
       *
       *  22-Dec-1999  TW  Now only dispatch RPCs if there is a free thread
       *                   available to service the RPC.  Also, svc_run_once
       *                   now only dispatches at most one RPC per call.  So,
       *                   if two RPCs are pending, the second will not be
       *                   serviced until the next call to svc_run_once.  The
       *                   try_find_free_thread mechanism will thus work when
       *                   there is more than one pending RPC.
       */
#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "srv_loop_thread calling try_find_free_thread..." );
#endif /* CAMP_DEBUG */

      if( try_find_free_thread() > -1 )
      {
        thread_lock_global_np();
        svc_run_once();
        thread_unlock_global_np();
      }

#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

      /*
       *  See if we should shutdown
       */
      if( check_shutdown() ) break;

#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "srv_loop_thread calling camp_fsleep..." );
#endif /* CAMP_DEBUG */

      /*
       *  Delay 100 ms to take the load off
       *  Less when we're on an MVME with VxWorks
       */
#ifdef VXWORKS
      taskDelay( 1 );  /*  Must be more than talkDelay( 0 )  */
#else
      camp_fsleep( 0.1 );
#endif /* VXWORKS */

#if CAMP_DEBUG
      if( camp_debug > 1 ) printf( "done\n" );
#endif /* CAMP_DEBUG */
    }
    /*
     *  Clean up before exiting
     */
    srv_rundown();
    exit( 0 );
}


/*
 *  Name:       svc_run_once
 *
 *  Purpose:    Check for any pending calls to the RPC sockets with a
 *              zero timeout.  If there is a call, dispatch it.
 *
 *              22-Dec-1999  TW  Multinet implementation note
 *              Note that in the Multinet (VAX/VMS) implementation, there
 *              is already a routine called svc_run_once which is used
 *              instead (found by analyzing Multinet rpc.olb (svc.obj)).
 *              This routine will call the svc_getreqset in the RPC
 *              library.  Since this is not the recommended procedure for
 *              implementing a multithreaded RPC server, the Multinet
 *              implementation of the CAMP server is no longer recommended
 *              for multithreading.  Replacement of svc_run_once for Multinet
 *              (i.e., a port of svc_run_once to Multinet, which would
 *              involve determining the correct procedure for the "select"
 *              call) would be necessary before re-implementing a multithreaded
 *              server with Multinet.
 *
 *  Called by:  srv_loop_thread
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server initialized as RPC server.
 *              CAMP global lock is ON.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              CAMP global lock will still be ON and should be turned OFF
 *              (when appropriate).
 *
 *              In the multithreaded implementation, there may still be
 *              valid RPC requests pending when this routine returns.  It
 *              will dispatch at most one RPC per call.  Furthermore, since
 *              threads are dispatched to satisfy the RPC call, the RPC will
 *              effectively still be active after returning from this call.
 *              Private variables in the svc thread's structure indicate when
 *              the svc thread is finished processing.  It is not until after
 *              this condition that the main thread will reply to the RPC
 *              client (which is done in srv_loop).  So, it is important to
 *              note that RPCs are still active after a return from this
 *              routine, and that associated RPC tranports must be considered
 *              active as well until the reply to the client.
 *
 *              In the single-threaded implementation, all pending RPCs
 *              should be satisfied when this routine returns.
 *
 *  Revision history:
 *    22-Dec-1999  TW  Call to my_svc_getreqset for multithreaded implementation
 *                     now processes at most one pending RPC.  Also, have made
 *                     sure that RPC transports are not re-used between calls
 *                     unless the RPC has really been satisfied (by the svc
 *                     thread).
 */

#ifndef MULTINET
static void
svc_run_once( void )
{
    int dtbsz;
    timeval_t tv = { 0, 0 };  /* poll */
    fd_set readfds;
    int selectStat;
#ifdef DEFUNCT_MULTINET
    /*
     *  Can't get select to work with MultiNet
     */
    extern SVCXPRT* svc_transport;
    fd_set svc_fdset;

    FD_ZERO( &svc_fdset );
    FD_SET( svc_transport->xp_sock, &svc_fdset );
#endif /* DEFUNCT_MULTINET */


#ifdef VXWORKS
#define getdtablesize()  FD_SETSIZE   /* check the maximum */
#define svc_fdset        taskRpcStatics->svc.svc_fdset
#define xports           taskRpcStatics->svc.xports
#define svc_head         taskRpcStatics->svc.svc_head
#endif /* VXWORKS */


    dtbsz = getdtablesize();

        readfds = svc_fdset;

#if CAMP_DEBUG
        if( camp_debug > 1 ) printf( "svc_run_once calling select()\n" );
#endif /* CAMP_DEBUG */

	/* selectStat = select( dtbsz, &readfds, (int*)0, (int*)0, &tv );
	   get rid of warnings by casting to fd_set* 
	 */
	 selectStat = select( dtbsz, &readfds, (fd_set*)0, (fd_set*)0, &tv );

#if CAMP_DEBUG
        if( camp_debug > 1 ) printf( "svc_run_once done select()\n" );
#endif /* CAMP_DEBUG */

        switch( selectStat )
        {
          case -1:
            if( errno == EINTR ) break;
            camp_logMsg( "svc: failed select" );
            break;
          case 0:
            break;
          default:
#ifdef MULTITHREADED
#if CAMP_DEBUG
	    if( camp_debug > 1 ) printf( "svc_run_once calling my_svc_getreqset()\n" );
#endif /* CAMP_DEBUG */
	    my_svc_getreqset( &readfds );
#if CAMP_DEBUG
	    if( camp_debug > 1 ) printf( "svc_run_once done my_svc_getreqset()\n" );
#endif /* CAMP_DEBUG */
#else
	    svc_getreqset( &readfds );
#endif /* MULTITHREADED */
            break;
        }
}
#endif /* !MULTINET */


/*
 *  Name:       srv_init
 *
 *  Purpose:    Initialize CAMP server database, Tcl interpreter
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server should be initialized as an RPC server and
 *              should have thread properties initialized (for multithreaded
 *              implementation).
 *
 *  Outputs:    CAMP status
 *
 *  Postconditions:
 *              Server now ready to enter (infinite) processing loop
 *
 *  Revision history:
 *
 */

static int
srv_init( void )
{
    int status;
    RES* pRes;
    FILE_req req;

    /* 
     *  Tcl interpreter initialization
     */
    status = camp_tclInit();
    if( _failure( status ) )
    {
        camp_logMsg( camp_getMsg() );
        return( status );
    }

    /* 
     *  System structure initialization
     */
    status = sys_init();
    if( _failure( status ) ) 
    {
        camp_appendMsg( "srv_init: failure: sys_init" );
        camp_logMsg( camp_getMsg() );
        exit( status );
    }

    if( camp_isMsg() ) camp_logMsg( camp_getMsg() );
    camp_setMsg( "" );

    if( do_auto_save )
    {
        bzero( &req, sizeof( req ) );
        req.flag = 0;
        req.filename = CAMP_SRV_AUTO_SAVE;

        /* 
         *  Load the CAMP configuration file
         *
         *  Careful - some routines (reading/writing instruments)
         *            expect the global thread lock to be ON
         */
        thread_lock_global_np();
        pRes = campsrv_sysload_13( &req, NULL );
        thread_unlock_global_np();

        _free( pRes );

        if( camp_isMsg() ) camp_logMsg( camp_getMsg() );
        camp_setMsg( "" );
    }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       srv_rundown
 *
 *  Purpose:    Clean up before exiting
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              Server received shutdown request and ready to exit
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              Server should exit after returning from this routine
 *
 *  Revision history:
 *
 */
void 
srv_rundown( void )
{
#ifdef MULTITHREADED
   /*
    *  Stop all other threads
    */
    kill_all_threads();
    camp_logMsg( "all threads stopped" );
    printf( "all threads stopped\n" );
#endif /* MULTITHREADED */

    /*
     *  Unregister RPC service
     */
    svc_unregister( CAMP_SRV, CAMP_SRV_VERS );

    /*
     *  Clean up
     */
    srv_delAllInss();
    sys_free();
    camp_tclEnd();
    srv_end_thread();
    camp_logMsg( "shutdown complete" );
    printf( "shutdown complete" );
    camp_stopLog();
}


/*
 *  Name:       srv_loop
 *
 *  Purpose:    Process regular server tasks which are checked continuously
 *              throughout the life of the server.
 *
 *  Called by:  srv_loop_thread
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              Server initialized as RPC server.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */
static void
srv_loop( void )
{
    static time_t time_last_auto_save = 0;
    time_t time_now;
    int status;
    extern void srv_handle_served_svc_threads( void );

/*
#if CAMP_DEBUG
    if( camp_debug > 0 ) printf( "srv_loop: doing REQ_doPending...\n" );
#endif /* CAMP_DEBUG */

    /*
     *  process all pending polling requests 
     *  thread lock done inside
     */
    REQ_doPending();

/*
#if CAMP_DEBUG
    if( camp_debug > 0 ) printf( "srv_loop: doing srv_handle_served_svc_threads...\n" );
#endif /* CAMP_DEBUG */

    /*
     *  17-Dec-1999  TW  Main thread makes RPC replies for svc threads
     *                   and then cleans up after them.  This means
     *                   that the main thread is the only thread that 
     *                   makes RPC calls which is appropriate for the 
     *                   VxWorks implementation of ONC RPC.
     */
    srv_handle_served_svc_threads();

    /*
     *  Autosave the CAMP configuration file at regular time intervals
     */
    if( do_auto_save )
    {
        /*
         *  Set initial time_last_auto_save so that an auto save
         *  is not done at startup
         */
        if( time_last_auto_save == 0 )
        {
            time( &time_last_auto_save );
        }
    
        if( ( time( &time_now ) - time_last_auto_save ) > AUTO_SAVE_INTERVAL )
        {
            time_last_auto_save = time_now;
    
            /*
             *  Write the CAMP configuration file
             *
             *  The global lock should be on to be safe while doing
             *  file I/O and accessing the entire CAMP database.
             */
            thread_lock_global_np();
            status = campCfg_write( CAMP_SRV_AUTO_SAVE );
	    if( _failure( status ) ) camp_logMsg( "failed auto-save" );
            thread_unlock_global_np();
        }
    }
}


/*
 *  A replacement of the ffs C library function which does not exist
 *  in all C libraries.
 *
 *  ffs finds the first set bit in a four-byte integer
 */
static u_long
my_ffs( u_long i )
{
  register u_long j;

  if( i == 0 ) return( 0 );

  for( j = 1; j <= ( sizeof( u_long )*NBBY ); j++ )
    {
      if( (i>>(j-1))&1 ) return( j );
    }

  return( 0 );
}



/*
 *  Name:       my_svc_getreqset
 *
 *  Purpose:    Replace the RPC library routine svc_getreqset with a version
 *              compatible with a multithreaded RPC server.
 *
 *              This routine is only needed to implement a multithreaded server.
 *
 *              The version of svc_getreqset that this was based on is
 *              contemporary with the CAMP server creation.  It may now (1999)
 *              have been superseded by later versions, but is still sufficient
 *              for our purposes.
 *
 *              Differences from svc_getreqset in the RPC library are:
 *
 *              1.  If input is found waiting on an RPC transport socket
 *                  that is associated with an active RPC call (one that an
 *                  svc thread has not yet completed) do not try any operations
 *                  on this transport.
 *
 *              2.  Within the RPC dispatch "do" loop, only destroy (free)
 *                  RPC transports that are really inactive.  This is similar
 *                  to point "1" above, but effectively does not allow
 *                  a transport to be destroyed immediately after the RPC call
 *                  has been dispatched.
 *
 *              3.  If an RPC call is dispatched, return from the routine
 *                  without testing any more sockets.  This means that at most
 *                  one RPC call is dispatched per call.
 *
 *  Called by:  svc_run_once
 * 
 *  Inputs:     The pointer to the file descriptor mask that is associated
 *              with the RPC server sockets.
 *
 *  Preconditions:
 *              The RPC server sockets have been checked for input using
 *              a select() call.  A socket has input waiting.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              In multithreaded implementation a dispatched RPC call will
 *              not have been satisfied when returning from this routine.  As
 *              such, associated RPC information (in particular the transport
 *              structure) must not be used until the RPC call is really
 *              completed and the client has been sent a response (unless
 *              of course the client has exited prematurely).
 *
 *  Revision history:
 *    22-Dec-1999  TW  Creation
 *
 */

#ifndef MULTINET

/* @(#)svc.c	2.4 88/08/11 4.0 RPCSRC; from 1.44 88/02/08 SMI */

#define NULL_SVC ((struct svc_callout *)0)
#define	RQCRED_SIZE	400		/* this size is excessive */

static void
my_svc_getreqset(readfds)
#ifdef FD_SETSIZE
	fd_set *readfds;
{
#else
	int *readfds;
{
    int readfds_local = *readfds;
#endif /* def FD_SETSIZE */
	enum xprt_stat stat;
	struct rpc_msg msg;
	int prog_found;
	u_long low_vers;
	u_long high_vers;
	struct svc_req r;
	register SVCXPRT *xprt;
	register u_long mask;
	register int bit;
	register u_long *maskp;
	register int setsize;
	register int sock;
	char cred_area[2*MAX_AUTH_BYTES + RQCRED_SIZE];
        struct authunix_parms* aupp;
        struct authunix_parms* aupp_orig;

        int num_dispatched = 0;
        int i;
        extern int check_transport_in_use( SVCXPRT* transp );

	msg.rm_call.cb_cred.oa_base = cred_area;
	msg.rm_call.cb_verf.oa_base = &(cred_area[MAX_AUTH_BYTES]);
	r.rq_clntcred = &(cred_area[2*MAX_AUTH_BYTES]);

#ifdef FD_SETSIZE
	setsize = getdtablesize();
#ifdef linux
#define NFDBITS	32
	maskp = (u_long *)readfds;
#else
	maskp = (u_long *)readfds->fds_bits;
#endif
	for (sock = 0; sock < setsize; sock += NFDBITS) {
	    for (mask = *maskp++; bit = my_ffs(mask); mask ^= (1 << (bit - 1))) {
		/* sock has input waiting */
		xprt = xports[sock + bit - 1];
#else
	for (sock = 0; readfds_local != 0; sock++, readfds_local >>= 1) {
	    if ((readfds_local & 1) != 0) {
		/* sock has input waiting */
		xprt = xports[sock];
#endif /* def FD_SETSIZE */

		/*
		 *  18-Dec-1999  TW  transports now active between calls to
                 *                   svc_getreqset.  A symptom is that active
                 *                   tranports may still report input waiting
                 *                   between calls, and sometimes SVC_RECV will
                 *                   be successful multiple times on the same
                 *                   transport, causing unpredictible results.
		 *                   FIX:  check that a transport is already
                 *                   active on a running svc thread.  If so,
                 *                   don't try any operations on it here.
		 */
		if( ( i = check_transport_in_use( xprt ) ) > -1 ) 
		  {
#if CAMP_DEBUG
		    if( camp_debug > 1 ) printf( "found transport in use thread %d sock %d bit %d\n", i, sock, bit );
#endif /* CAMP_DEBUG */
		    continue;
		  }
#if CAMP_DEBUG
		if( camp_debug > 1 ) printf( "before dispatch SVC_STAT( xprt ) %d sock %d bit %d\n", SVC_STAT( xprt ), sock, bit );
#endif /* CAMP_DEBUG */

		/* now receive msgs from xprtprt (support batch calls) */
		do {
			if (SVC_RECV(xprt, &msg)) {

				/* now find the exported program and call it */
				register struct svc_callout *s;
				enum auth_stat why;

				r.rq_xprt = xprt;
				r.rq_prog = msg.rm_call.cb_prog;
				r.rq_vers = msg.rm_call.cb_vers;
				r.rq_proc = msg.rm_call.cb_proc;
				r.rq_cred = msg.rm_call.cb_cred;

				/* first authenticate the message */
				if ((why= _authenticate(&r, &msg)) != AUTH_OK) {
					svcerr_auth(xprt, why);
					goto call_done;
				}

				/* now match message with a registered service*/
				prog_found = FALSE;
				low_vers = 0 - 1;
				high_vers = 0;
				for (s = svc_head; s != NULL_SVC; s = s->sc_next) {
					if (s->sc_prog == r.rq_prog) {
						if (s->sc_vers == r.rq_vers) {
							(*s->sc_dispatch)(&r, xprt);
							num_dispatched++;
							goto call_done;
						}  /* found correct version */
						prog_found = TRUE;
						if (s->sc_vers < low_vers)
							low_vers = s->sc_vers;
						if (s->sc_vers > high_vers)
							high_vers = s->sc_vers;
					}   /* found correct program */
				}
				/*
				 * if we got here, the program or version
				 * is not served ...
				 */
				if (prog_found)
					svcerr_progvers(xprt,
					low_vers, high_vers);
				else
					 svcerr_noprog(xprt);
				/* Fall through to ... */
			}
		call_done:
			if ((stat = SVC_STAT(xprt)) == XPRT_DIED){
                          /*
                           *  Only destroy RPC transports (that is, free them
                           *  up for re-use) if the transport is really inactive
                           *  There can be a condition here in which the client
                           *  has exited prematurely (thus leaving the port
                           *  in the XPRT_DIED state) but the SVC thread has
                           *  not finished the RPC request.  If a new RPC
                           *  request is made and same transport was available,
                           *  two active RPC calls would now be associated with
                           *  one transport (that appeared in non-dead state
                           *  to both).  When the first SVC thread then
                           *  finished it's call, it could potentially reply
                           *  to the wrong client request.  INSTEAD, we must
                           *  not try to use an RPC transport until the
                           *  corresponding RPC request is really finished.
                           */
			  if( check_transport_in_use( xprt ) == -1 ) 
			    {
#if CAMP_DEBUG
			      if( camp_debug > 1 ) printf( "server destroying stale xprt sock %d bit %d\n", sock, bit );
#endif
			      SVC_DESTROY( xprt );
			    }
                          /*
                           * Don't break here, want to check num_dispatched
                           * first
                           */
			  /* break; */
			}
			/*
			 *  Return if there has been an RPC dispatch
                         *  This means that a maximum on one RPC will be
                         *  dispatched.  This is necessary to allow the
                         *  try_find_free_thread methodology in the calling
                         *  routine to work properly.
			 */
			if( num_dispatched > 0 )
			  {
#if CAMP_DEBUG
			    if( camp_debug > 1 ) printf( "num_dispatched %d stat %d sock %d bit %d\n", 
				   num_dispatched, stat, sock, bit );
#endif
			    return;
			  }

			if( stat == XPRT_MOREREQS )
			  {
#if CAMP_DEBUG
			    if( camp_debug > 1 ) printf( "more requests on sock %d bit %d\n", sock, bit );
#endif
			  }

		} while (stat == XPRT_MOREREQS);
	    }
	}
}

#endif /* !MULTINET */
