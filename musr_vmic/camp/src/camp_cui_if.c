/*
 *  $Id$
 *
 *  $Revision$
 *
 *  Purpose:    Routines to prompt for input of interface specifications
 *
 *  Called by:  camp_cui_menucb.c
 * 
 *  $Log$
 *  Revision 1.5  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.4  2004/01/28 03:14:37  asnd
 *  Add VME interface type.
 *
 *  Revision 1.3  2000/12/23 02:42:57  ted
 *  Minor change inputIf_indpak, camp Type[] -> char* Type[]
 *
 *  Revision 1.2  2000/12/22 23:59:31  David.Morris
 *  Added INDPAK support for adding instruments in the CUI
 *
 *
 *  Revision history:
 *   v1.1  20-Apr-1994  [T. Whidden] drivers in server, Ifs generalized
 *         14-Dec-1999  TW  Additional include for gcc (Linux)
 *                          Some integer type changes for gcc
 *
 */

#ifdef linux
#include <string.h>
#endif /* linux */
#include "camp_cui.h"


bool_t
inputIf( CAMP_VAR* pVar, char* typeIdent, double* pDelay, char* defn )
{
    CAMP_IF_t* pIF_t;
    char defaultTypeIdent[LEN_IDENT+1];
    CAMP_INSTRUMENT* pIns;
    double delay;
    char defaultDefn[LEN_IF_DEFN+1];

    defaultTypeIdent[0] = '\0';
    delay = 0.0;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF != NULL ) 
    {
        strcpy( defaultTypeIdent, pIns->pIF->typeIdent );
        delay = (double)pIns->pIF->accessDelay;
    }

    if( !inputIfType( defaultTypeIdent, typeIdent ) ) 
    {
        return( FALSE );
    }

    pIF_t = camp_ifGetpIF_t( typeIdent );
    if( pIF_t == NULL ) 
    {
        return( FALSE );
    }

    if( !inputFloat( "Access delay", delay, &delay ) )
    {
        return( FALSE );
    }

    *pDelay = delay;

    if( ( pIns->pIF != NULL ) && ( pIns->pIF->typeID == pIF_t->typeID ) )
    {
	strcpy( defaultDefn, pIns->pIF->defn );
    }
    else
    {
	strcpy( defaultDefn, pIF_t->defaultDefn );
    }

    switch( pIF_t->typeID )
    {
      case CAMP_IF_TYPE_NONE:
        break;

      case CAMP_IF_TYPE_RS232:
        if( !inputIf_rs232( defaultDefn, pIF_t->conf, defn ) )
        {
            return( FALSE );
        }
        break;

      case CAMP_IF_TYPE_GPIB:
        if( !inputIf_gpib( defaultDefn, defn ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_CAMAC:
        if( !inputIf_camac( defaultDefn, defn ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_VME:
        if( !inputIf_vme( defaultDefn, defn ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_TCPIP:
        if( !inputIf_tcpip( defaultDefn, defn ) )
        {
            return( FALSE );
        }
        break;

      case CAMP_IF_TYPE_INDPAK:
        if( !inputIf_indpak( defaultDefn, defn ) )
        {
            return( FALSE );
        }

        break;

      default:
        return( FALSE );
    }

    return( TRUE );
}


bool_t
inputIfType( char* defaultTypeIdent, char* typeIdent )
{
    char* names[MAX_NUM_SELECTIONS];
    CAMP_IF_t* pIF_t;
    int index;
    int num;
    int defIndex;

    num = 0;
    defIndex = 0;
    for( pIF_t = pSys->pIFTypes; 
         pIF_t != NULL; 
         pIF_t = pIF_t->pNext )
    {
        if( streq( defaultTypeIdent, pIF_t->ident ) )
        {
            defIndex = num;
        }
        names[num++] = pIF_t->ident;
    }

    if( inputSelectWin( "Interface Type", num, names, defIndex, &index ) )
    {
        strcpy( typeIdent, names[index] );
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
inputIf_rs232( char* defaultDefn, char* conf, char* defn )
{
    long i;
    char buf[128];
    char* names[MAX_NUM_SELECTIONS];
    int index;
    int num;
    int defIndex;
    bool_t flag = TRUE;
    char defPort[LEN_IDENT+1];
    int defBaud;
    int defData;
    int defStop;
    char defParity[LEN_IDENT+1];
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];
    int defTimeout;

    camp_getIfRs232Port( defaultDefn, defPort );
    camp_getIfRs232Parity( defaultDefn, defParity );
    camp_getIfRs232ReadTerm( defaultDefn, defReadTerm );
    camp_getIfRs232WriteTerm( defaultDefn, defWriteTerm );
    defBaud = camp_getIfRs232Baud( defaultDefn );
    defData = camp_getIfRs232Data( defaultDefn );
    defStop = camp_getIfRs232Stop( defaultDefn );
    defTimeout = camp_getIfRs232Timeout( defaultDefn );

    strcpy( buf, conf );
    
    defIndex = 0;
    for( num = 0, names[0] = strtok( buf, " " ); 
         names[num] != NULL; 
         names[++num] = strtok( NULL, " " ) )
    {
        if( streq( defPort, names[num] ) ) defIndex = num;
    }

    if( !inputSelectWin( "Port", num, names, defIndex, &index ) )
        return( FALSE );

    strcpy( defn, names[index] );
    strcat( defn, " " );

    num = 0;
    names[num++] = strdup( "110" );
    names[num++] = strdup( "300" );
    names[num++] = strdup( "600" );
    names[num++] = strdup( "1200" );
    names[num++] = strdup( "2400" );
    names[num++] = strdup( "4800" );
    names[num++] = strdup( "9600" );
    names[num++] = strdup( "19200" );

    switch( defBaud )
    {
      case 110:
        defIndex = 0;
        break;
      case 300:
        defIndex = 1;
        break;
      case 600:
        defIndex = 2;
        break;
      case 1200:
        defIndex = 3;
        break;
      case 2400:
        defIndex = 4;
        break;
      case 4800:
        defIndex = 5;
        break;
      case 9600:
        defIndex = 6;
        break;
      case 19200:
        defIndex = 7;
        break;
      default:
        defIndex = 6;
        break;
    }

    if( !inputSelectWin( "Baud rate", num, names, defIndex, &index ) )
        flag = FALSE;

    strcat( defn, names[index] );
    strcat( defn, " " );
    for( i = 0; i < num; i++ ) free( names[i] );
    if( !flag ) return( FALSE );

    if( !inputInteger( "Data bits", defData, &i ) )
    {
        return( FALSE );
    }
    sprintf( buf, "%d ", i );
    strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_ODD );
    names[num++] = toktostr( TOK_EVEN );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defParity, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Parity", num, names, defIndex, &index ) )
        flag = FALSE;

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    if( !inputInteger( "Stop bits", defStop, &i ) )
    {
        return( FALSE );
    }
    sprintf( buf, "%d ", i );
    strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    if( !inputInteger( "Read timeout", defTimeout, &i ) )
    {
        return( FALSE );
    }
    sprintf( buf, "%d ", i );
    strcat( defn, buf );

    return( TRUE );
}


bool_t 
inputIf_gpib( char* defaultDefn, char* defn )
{
    long i;
    char* names[MAX_NUM_SELECTIONS];
    int index;
    int num;
    int defIndex;
    bool_t flag = TRUE;
    int defAddr;
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];

    defAddr = camp_getIfGpibAddr( defaultDefn );
    camp_getIfGpibReadTerm( defaultDefn, defReadTerm );
    camp_getIfGpibWriteTerm( defaultDefn, defWriteTerm );

    if( !inputInteger( "GPIB address", defAddr, &i ) )
        return( FALSE );

    sprintf( defn, "%d ", i );
    
    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
        flag = FALSE;

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
        flag = FALSE;

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    return( TRUE );
}


bool_t 
inputIf_camac( char* defaultDefn, char* defn )
{
    long i;
    long def;
    char str[LEN_STR+1];

    def = camp_getIfCamacB( defaultDefn );
    if( !inputInteger( "Branch", def, &i ) )
        return( FALSE );

    sprintf( defn, "%d ", i );
    
    def = camp_getIfCamacC( defaultDefn );
    if( !inputInteger( "Crate", def, &i ) )
        return( FALSE );

    sprintf( str, "%d ", i );
    strcat( defn, str );

    def = camp_getIfCamacN( defaultDefn );
    if( !inputInteger( "Slot", def, &i ) )
        return( FALSE );

    sprintf( str, "%d ", i );
    strcat( defn, str );

    return( TRUE );
}


bool_t 
inputIf_vme( char* defaultDefn, char* defn )
{
    long i;
    long def;
    char str[LEN_STR+1];

    def = camp_getIfVmeBase( defaultDefn );
    if( !inputInteger( "Base address", def, &i ) )
        return( FALSE );

    sprintf( defn, "%d ", i );

    return( TRUE );
}


bool_t 
inputIf_tcpip( char* defaultDefn, char* defn )
{
    long i;
    double f;
    char buf[128];
    char* names[MAX_NUM_SELECTIONS];
    int index;
    int num;
    int defIndex;
    bool_t flag = TRUE;
    char defAddr[32];
    int defPort;
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];
    double defTimeout;

    camp_getIfTcpipAddr( defaultDefn, defAddr );
    defPort = camp_getIfTcpipPort( defaultDefn );
    camp_getIfTcpipReadTerm( defaultDefn, defReadTerm );
    camp_getIfTcpipWriteTerm( defaultDefn, defWriteTerm );
    defTimeout = camp_getIfTcpipTimeout( defaultDefn );
 
    if( !inputString( "TCP/IP Address", 31, defAddr, buf, isPrintChar ) )
    {
        return( FALSE );
    }
    strcat( defn, buf );
    strcat( defn, " " );

    if( !inputInteger( "Port number", defPort, &i ) )
    {
        return( FALSE );
    }
    sprintf( buf, "%d ", i );
    strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }
    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    strcat( defn, names[index] );
    strcat( defn, " " );

    if( !inputFloat( "Timeout", defTimeout, &f ) )
    {
        return( FALSE );
    }
    sprintf( buf, "%.1lf ", (fabs(f)+0.02) );
    strcat( defn, buf );

    return( TRUE );
}

bool_t 
inputIf_indpak( char* defaultDefn, char* defn )
{
    long i;
    long def;
    char str[LEN_STR+1];
    char* Type[] = {"adc ", "dac ", "motor "};

    def = camp_getIfIndpakSlot( defaultDefn );
    if( !inputInteger( "Slot", def, &i ) )
        return( FALSE );

    sprintf( defn, "%d ", i );
    
/* Change this to a select box with default selected etc like GPIB */

    camp_getIfIndpakType( defaultDefn, str );
    if( !inputInteger( "Type", def, &i ) )
        return( FALSE );

/* Check for bounds here! */

    strcat( defn, Type[i] );

    def = camp_getIfIndpakChannel( defaultDefn );
    if( !inputInteger( "Channel", def, &i ) )
        return( FALSE );

    sprintf( str, "%d ", i );
    strcat( defn, str );

    return( TRUE );
}



