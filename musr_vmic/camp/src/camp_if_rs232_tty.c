/*
 *  Name:       camp_if_rs232_tty.c
 *
 *  Purpose:    Provides an RS232 serial communications interface type.
 *              Implementation using standard Unix TTY ports.
 *
 *              A CAMP RS232 interface definition must provide the following
 *              routines:
 *                int if_rs232_init ( void );
 *                int if_rs232_online ( CAMP_IF *pIF );
 *                int if_rs232_offline ( CAMP_IF *pIF );
 *                int if_rs232_read( REQ* pReq );
 *                int if_rs232_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation, the global mutex is unlocked
 *              during calls to the Unix library functions write(),
 *              select(), and read().
 *
 *  Notes:
 *
 *     Ini file configuration string:
 *        {<port1> <port2> ...}
 *
 *     Meaning of private (driver-specific) variables:
 *        pIF_t->priv - not used
 *        pIF->priv - terminal port file descriptor
 *
 *  Revision history:
 *
 *    1999-10-19   dbm Added stop bit call into if-rs232-settings
 *    15-Dec-1999  TW/DA In if_rs232_read: don't check terminator if there
 *                     isn't one
 *                     Also, continue normally if FIOSTOPBITS ioctl fails
 *                     (some TTY drivers are not implemented with a stop bits
 *                     function even though probably all RS232 chips will
 *                     do it).
 *    21-Dec-1999  TW  Took out calls to srv_check_timeout() - not used anymore
 *                     (this was previously allowing the possibility of 
 *                     a timeout response to a CAMP client before the client
 *                     does an RPC timeout, but found to be better without)
 *    24-Jan-1999  DA  When there is a terminator, terminate the string!
 *
 */

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#ifdef VXWORKS
#include "ioLibx.h"
#else
#include <sys/termio.h>
#include <sys/sgtty.h>
#include <sys/ioctl.h>
#endif /* VXWORKS */

#include "camp.h"

static int if_rs232_settings( CAMP_IF* pIF, int fd );
#ifndef VXWORKS
static int getBaud( int baud );
#endif /* !VXWORKS */
static void getTerm( char* term_in, char* term_out, int* len );

int
if_rs232_init( void )
{
    /*
     *  Could check that the ports exist here
     */
    return( CAMP_SUCCESS );
}


static int
if_rs232_settings( CAMP_IF* pIF, int fd )
{
    char port[LEN_STR+1];
    int baud;
    int databits;
    char sParity[8];
    int cParity;
    int stopbits;
#ifndef VXWORKS
    struct sgttyb ttySettings;
#endif /* !VXWORKS */

    camp_getIfRs232Port( pIF->defn, port );
    baud = camp_getIfRs232Baud( pIF->defn );
    databits = camp_getIfRs232Data( pIF->defn );
    camp_getIfRs232Parity( pIF->defn, sParity );
    cParity = toupper( sParity[0] );
    stopbits = camp_getIfRs232Stop( pIF->defn );

    /*
     *  Set the port settings
     */
#ifdef VXWORKS
    if( ioctl( fd, FIOSETOPTIONS, OPT_RAW ) == ERROR )
    {
      camp_appendMsg( "failure setting port %s parameters", port );
      return( CAMP_FAILURE );
    }

    if( ioctl( fd, FIOBAUDRATE, baud ) == ERROR )
    {
      camp_appendMsg( "failure setting port %s baud rate", port );
      return( CAMP_FAILURE );
    }

    /*
     *  ioctl functions FIOPARITY and FIODATABITS were
     *  added to drivers for onboard serial, IP-QuadSerial, 
     *  and IP-OctalSerial.
     */
    if( ioctl( fd, FIOPARITY, cParity ) == ERROR )
    {
      camp_appendMsg( "failure setting port '%s' parity", port );
      return( CAMP_FAILURE );
    }

    if( ioctl( fd, FIODATABITS, databits ) == ERROR )
    {
      camp_appendMsg( "failure setting port '%s' databits", port );
      return( CAMP_FAILURE );
    }

    if( ioctl( fd, FIOSTOPBITS, stopbits ) == ERROR )
    {
      /*  15-Dec-1999 / 20-Sep-2000  tw/da  Not all MVME ports allow stopbit ioctl.
          Since they have fixed stopbits = 1, suppress error return in that case.
          To do: test the port for effect of change.
      */
      if( stopbits != 1 )
      {
          camp_appendMsg( "failure setting port '%s' stopbits", port );
          return( CAMP_FAILURE );
      }
    }

#else
    if( ioctl( fd, TIOCGETP, &ttySettings ) == -1 ) 
    {
      camp_appendMsg( "failure getting port %s parameters", port );
      return( CAMP_FAILURE );
    }

    ttySettings.sg_flags |= RAW;
    ttySettings.sg_flags &= ~(ECHO|CRMOD);

    ttySettings.sg_ispeed = getBaud( baud );
    ttySettings.sg_ospeed = ttySettings.sg_ispeed;

    /*
     *  Must set parity/databits here
     *  Default is 8 data, no parity
     */

    if( ioctl( fd, TIOCSETP, &ttySettings ) == -1 )
    {
      camp_appendMsg( "failure setting port %s parameters", port );
      return( CAMP_FAILURE );
    }
#endif /* VXWORKS */

    pIF->priv = fd;

    return( CAMP_SUCCESS );
}


int
if_rs232_online( CAMP_IF* pIF )
{
  int fd;
  char port[LEN_STR+1];

  camp_getIfRs232Port( pIF->defn, port );

  /*
   *  Open the port
   */ 
  if( ( fd = open( port, O_RDWR, 0 ) ) < 0 )
    {
      camp_appendMsg( "failure opening port %s", port );
      return( CAMP_FAILURE );
    }

  if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) return( CAMP_FAILURE );

  close( fd );

  return( CAMP_SUCCESS );
}


int
if_rs232_offline( CAMP_IF* pIF )
{
    return( CAMP_SUCCESS );
}


/*
 *  if_rs232_read,  synchronous write, then read with timeout
 */
int
if_rs232_read( REQ* pReq )
{
    CAMP_IF* pIF;
    int fd, nfds;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    fd_set readfds;
    char port[LEN_IDENT+1];
    char* buf;
    int buf_len;
    char* bufBegin;
    char* eol;
    int* pRead_len;
    int nread;
    long timeout;
    timeval_t tv;
    timeval_t tvFlush = { 0, 0 }; /* zero wait for typeahead to clear */
#ifdef DEBUG
    char dbuf[512], dbuf1[32];
#endif /* DEBUG */

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;
/*
    fd = pIF->priv;
*/
    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    bufBegin = buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
    getTerm( camp_getIfRs232WriteTerm( pIF->defn, term ), term, &term_len );
    timeout = camp_getIfRs232Timeout( pIF->defn );
    tv.tv_sec = labs( timeout );
    tv.tv_usec = 0;
    camp_getIfRs232Port( pIF->defn, port );
    pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
    *pRead_len = 0;

    /*
     *  Open the port
     */ 
    if( ( fd = open( port, O_RDWR, 0 ) ) < 0 )
    {
        camp_appendMsg( "failure opening port %s", port );
        return( CAMP_FAILURE );
    }

    if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) goto error;

    if( timeout > 0 )
      {
  /*
   *  Flush typeahead if read timeout is greater than zero
   */
	FD_ZERO( &readfds );
	FD_SET( fd, &readfds );
	while( select( fd+1, &readfds, NULL, NULL, &tvFlush ) > 0 )
	  {
	    if( FD_ISSET( fd, &readfds ) )
	      {
		/*
		 *  This data is ignored, just using buf for space
		 */
		read( fd, buf, buf_len );
	      }
	    FD_ZERO( &readfds );
	    FD_SET( fd, &readfds );
	  }
      }

    /*
     *  Write the command
     */
#ifdef DEBUG
    strncpy( dbuf, cmd, cmd_len );
    dbuf[cmd_len] = '\0';
    printf( "term_len = %d\n", term_len );
    if( term_len > 0 ) sprintf( &dbuf[cmd_len], "\\%03o", term[0] );
    if( term_len > 1 ) sprintf( &dbuf[cmd_len+4], "\\%03o", term[1] );
    printf( "writing '%s' to port '%s'\n", dbuf, port );
#endif /* DEBUG */

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    if( cmd_len > 0 )
      {
	if( write( fd, cmd, cmd_len ) > -1 )
	  {
	    if( term_len > 0 ) write( fd, term, term_len );
	  }
      }

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    buf[0] = '\0';

    getTerm( camp_getIfRs232ReadTerm( pIF->defn, term ), term, &term_len );

    /*
     *  Read with timeout
     */
    for( ;; )
    {
        FD_ZERO( &readfds );
        FD_SET( fd, &readfds );

#ifdef MULTITHREADED
	thread_unlock_global_np();
#endif /* MULTITHREADED */

	nfds = select( fd+1, &readfds, NULL, NULL, &tv );

#ifdef MULTITHREADED
	/*
	 *  17-Dec-1996  TW  Check CAMP's own RPC timeout
	 *  16-Dec-1999  TW  reverse order - call srv_check_timeout within global lock
         *                   This is important because there is non-reentrant code
         *                   within srv_check_timeout (RPC library, etc.) 
	 *  21-Dec-1999  TW  srv_check_timeout no longer used
	 */
	thread_lock_global_np();
	/* srv_check_timeout(); */
#endif /* MULTITHREADED */

        switch( nfds )
          {
          case -1:
            /* 
             *  Error
             */
            if( errno == EINTR ) continue;
            camp_appendMsg( "select failure on port '%s'", port );
            goto error;

          case 0:
            /* 
             *  Timeout. 
             *  Success if terminator "none" and buffer unflushed.
             *  Success if terminator "none" and we got some characters.
             *  Timeout error otherwise.
             */
            if( term_len == 0 && (timeout <= 0 || *pRead_len > 0) ) goto success;
            camp_appendMsg( "timeout reading port '%s', got '%s'", port, 
                            bufBegin );
            goto error;

          default:
            if( !FD_ISSET( fd, &readfds ) ) continue;
            /* 
             *  Something to read
             */
            
            if( buf_len == 0 )
              {
#ifdef DEBUG
                printf( "something to read from port '%s' but buffer is full\n", port);
#endif /* DEBUG */
                break;
              }

#ifdef MULTITHREADED
            thread_unlock_global_np();
#endif /* MULTITHREADED */

#if CAMP_DEBUG
            if( camp_debug > 0 ) printf( "if_rs232_read: doing read..." );
#endif /* CAMP_DEBUG */

	    nread = read( fd, buf, buf_len );

#if CAMP_DEBUG
            if( camp_debug > 0 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

#ifdef MULTITHREADED
	    thread_lock_global_np();
#endif /* MULTITHREADED */

#ifdef DEBUG
	    strncpy( dbuf1, buf, nread );
	    dbuf1[nread] = '\0';
	    stoprint_expand( dbuf1, dbuf );
	    printf( "read '%s' from port '%s'\n", dbuf, port );
#endif /* DEBUG */

            buf += nread;
	    buf_len -= nread;
            *pRead_len += nread;
            buf[0] = '\0';
            break; /* from switch */
	}

	if( buf_len == 0 ) break; /* from for loop */

        /*
         *  Check if we have terminator yet
         *  15-Dec-1999  TW  If terminator == NONE, don't look for one!
	 *  24-Jan-1999  DA  When there is a terminator, terminate the string!
         */
        if( term_len > 0 )
	  {
	    eol = strstr( bufBegin, term );
	    if( eol != NULL ) 
	      {
		*eol = '\0';
		*pRead_len = (int)(eol - bufBegin);
		 /* ... or even ...
		 *pRead_len -= term_len;
		 */
		break; /* from for loop */
	      }
	  }
    } /* end for(;;) loop */

success:
    close( fd );
    return( CAMP_SUCCESS );

error:
    close( fd );
    return( CAMP_FAILURE );
}


/*
 *  if_rs232_write,  synchronous write 
 */
int 
if_rs232_write( REQ* pReq )
{
    CAMP_IF* pIF;
    int fd;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char* command;
    char port[LEN_IDENT+1];

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;
/*
    fd = pIF->priv;
*/
    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;
    getTerm( camp_getIfRs232WriteTerm( pIF->defn, term ), term, &term_len );
    camp_getIfRs232Port( pIF->defn, port );

    /*
     *  Open the port
     */ 
    if( ( fd = open( port, O_WRONLY, 0 ) ) < 0 )
    {
        camp_appendMsg( "failure opening port %s", port );
        return( CAMP_FAILURE );
    }

    if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) goto error;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    if( write( fd, cmd, cmd_len ) > -1 )
      {
	if( term_len > 0 ) write( fd, term, term_len );
      }

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    close( fd );
    return( CAMP_SUCCESS );

error:
    close( fd );
    return( CAMP_FAILURE );
}


#ifndef VXWORKS
static int
getBaud( int baud )
{
    switch( baud )
    {
      case 110: return( B110 );
      case 300: return( B300 );
      case 600: return( B600 );
      case 1200: return( B1200 );
      case 2400: return( B2400 );
      case 4800: return( B4800 );
      case 9600: return( B9600 );
      case 19200: return( B19200 );
      default: return( B9600 );
    }
}
#endif /* !VXWORKS */

static void
getTerm( char* term_in, char* term_out, int* len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
      case TOK_LF:   strcpy( term_out, "\012" );     *len = 1; return;
      case TOK_CR:   strcpy( term_out, "\015" );     *len = 1; return;
      case TOK_CRLF: strcpy( term_out, "\015\012" ); *len = 2; return;
      case TOK_LFCR: strcpy( term_out, "\012\015" ); *len = 2; return;
      case TOK_NONE: strcpy( term_out, "" );         *len = 0; return;
      default:       strcpy( term_out, "" );         *len = 0; return;
    }
}
