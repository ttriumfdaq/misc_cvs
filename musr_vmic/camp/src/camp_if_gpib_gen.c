/*
 *  Name:       camp_if_gpib_gen.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *              Generic routines to provide a shell for a new GPIB interface
 *              implementation.
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *
 *              NOTE: this is not fully functioning, just stub routines
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *  Notes:
 *
 *      pIF_t->priv == 1 (gpib available)
 *                  == 0 (gpib unavailable)
 *
 *  Revision history:
 *
 */


#include <stdio.h>
#include "camp_srv.h"

#define if_gpib_ok  (pIF_t->priv!=0)

static void getTerm( char* term_in, char* term_out, int* term_len );


int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    char cIpModuleIndex;
    int timeout;

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    pIF_t->priv = 0; 

    /*
     *  Check configuration data here
     */

    pIF_t->priv = 1;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    if( ( ( addr = camp_getIfGpibAddr( pIF->defn ) ) < 0 ) ||
          ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    /*
     *  Turn online here
     */

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;
    int status;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    /*
     *  Turn offline here
     */

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Write here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */


#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Read here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = 0;

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Write here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    return( CAMP_SUCCESS );
}


/*
 *  camp_gpib_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 *
 *  21-Dec-1999  TW  Implemented for VxWorks MVIP300 only.  Found
 *                   to increase reliability of LakeShore GPIB
 *                   devices.
 */
int 
camp_gpib_clear( int gpib_addr )
{
  return( CAMP_SUCCESS );
}


/*
 *  camp_gpib_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 */
int 
camp_gpib_timeout( int timeoutVal )
{
  return( CAMP_SUCCESS );
}


/*
static void
getTerm( char* term_in, char* term_out, int* term_len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
        case TOK_LF:   strcpy( term_out, "\012" );     *term_len = 1; return;
        case TOK_CR:   strcpy( term_out, "\015" );     *term_len = 1; return;
        case TOK_CRLF: strcpy( term_out, "\015\012" ); *term_len = 2; return;
        case TOK_LFCR: strcpy( term_out, "\012\015" ); *term_len = 2; return;
        case TOK_NONE: strcpy( term_out, "" );         *term_len = 0; return;
        default:       strcpy( term_out, "" );         *term_len = 0; return;
    }
}
*/

