#ifndef _CAMP_SRV_H_
#define _CAMP_SRV_H_

/*
 *  Includes for Multithreading (see also camp.h).
 *  Add any other POSIX thread compliant OS here.
 *  No includes needed for VxWorks
 */
#ifdef MULTITHREADED
#if defined( VMS ) || defined( OSF1 ) || defined( ULTRIX ) || defined( linux )
#include <pthread.h>
#elif defined( VXWORKS )
#include <vxWorks.h>
/*
 *  Mimic pthreads
 */
#include "vx_pthread.h"
#endif /* operating system */
#endif /* MULTITHREADED */

#include "tcl.h"
#include "camp.h"

/*
 *  Automatically turn an instrument offline
 *  after this many consecutive read or write
 *  errors
 */
#define MAX_CONSECUTIVE_IF_ERRORS 20

/* camp_srv_main.c */
void srv_rundown ( void );

/* camp_srv_utils.c */
void srv_delAllInss( void );
/* some of these need fixing for non-multithreaded compilation !!!! */
int camp_checkInsLock( CAMP_VAR* pVar, struct svc_req* rq );
int camp_checkAllInssLock( struct svc_req* rq );
void camp_sleep( timeval_t *ptv );
void camp_fsleep( double delay );

/* camp_srv_svc_mod.c */
#ifdef MULTITHREADED
int camp_thread_mutex_lock( pthread_mutex_t * mutex );
#endif /* MULTITHREADED */
void thread_lock_global_np( void );
void thread_unlock_global_np( void );
void set_global_mutex_noChange( int val );
struct svc_req* get_current_rqstp( void );
void set_current_rqstp( struct svc_req* rqstp );
Tcl_Interp* get_thread_interp( void );
void set_thread_interp( Tcl_Interp* interp );
u_long get_thread_xdr_flag( void );
void set_thread_xdr_flag( u_long flag );
char* get_thread_ident( void );
void set_thread_ident( char* ident );
char* get_thread_msg( void );
void set_thread_msg( char* msg );
int check_shutdown( void );
void set_shutdown( void );
void kill_all_threads( void );
int srv_init_main_thread( void );
void srv_init_thread_once( void );
void start_poll_thread( REQ* pReq );
void srv_end_thread( void );
/*
void check_start_svc_thread( void );
void start_svc_thread( fd_set* pReadfds );
*/
void srv_check_timeout( void );

/* camp_msg_priv.c */
int camp_startLog ( char *filename , bool_t keepClosedFlag );
int camp_stopLog ( void );
void camp_logMsg ( char *msg );
#ifdef VMS
void camp_logVMSMsg ( u_long numMessage );
#endif /* VMS */

/* camp_write.c */
bool_t openOutput ( char *filename );
bool_t closeOutput ( void );
void add_tab ( void );
void del_tab ( void );
void print ( char *fmt , ...);
void print_tab ( char *fmt , ...);
void reinitialize ( char *name , PFB parse_proc , char *line );

/* camp_def_write.c */
int campDef_write ( char *filename , CAMP_VAR *pVar );
void defWrite_var ( CAMP_VAR *pVar );
void defWrite_stat ( CAMP_CORE *pCore );
void defWrite_dyna ( CAMP_CORE *pCore );
void defWrite_spec ( CAMP_VAR *pVar );
void defWrite_Numeric ( CAMP_VAR *pVar );
void defWrite_Selection ( CAMP_VAR *pVar );
void defWrite_String ( CAMP_VAR *pVar );
void defWrite_Array ( CAMP_VAR *pVar );
void defWrite_Structure ( CAMP_VAR *pStcVar );
void defWrite_Facility ( CAMP_VAR *pInsVar );
void defWrite_Link ( CAMP_VAR *pVar );

/* camp_ini_write.c */
int campIni_write ( char *filename , CAMP_VAR *pVar );
void iniWrite_stat ( CAMP_CORE *pCore );
void iniWrite_dyna ( CAMP_CORE *pCore );
void iniWrite_var ( CAMP_VAR *pVar );
void iniWrite_Selection ( CAMP_VAR *pVar );
void iniWrite_Array ( CAMP_VAR *pVar );
void iniWrite_Structure ( CAMP_VAR *pStcVar );
void iniWrite_Instrument ( CAMP_VAR *pInsVar );

/* camp_cfg_write.c */
int campCfg_write ( char *filename );
void save_all_ini ( void );
void cfgWrite_insadd ( void );
void cfgWrite_insset ( void );
int cfgWrite_if ( CAMP_IF *pIF );
void cfgWrite_varLink ( CAMP_VAR *pVar );

/* camp_sys_priv.c */
void sys_free ( void );
int sys_init ( void );
int sys_update ( void );
int sys_initDyna ( void );
int sys_initDir ( char *filespec_in );
u_long sys_getTypeInstance ( char *typeIdent );

/* camp_var_priv.c */
void varInitPoll ( CAMP_VAR *pVar );
int varRead ( CAMP_VAR *pVar );
int varSetReq ( CAMP_VAR *pVar , CAMP_SPEC *pSpec );
int varSetSpec ( CAMP_VAR *pVar , CAMP_SPEC *pSpec );
int var_set ( CAMP_VAR *pVar );
int varNumSet ( CAMP_VAR *pVar , double val );
int varNumSetTol ( CAMP_VAR *pVar , u_long tolType , float tol );
int varSelSet ( CAMP_VAR *pVar , u_char val );
int varSelSetLabel ( CAMP_VAR *pVar , char *label );
int varStrSet ( CAMP_VAR *pVar , char *val );
int varArrSet ( CAMP_VAR *pVar , caddr_t pVal );
int varLnkSet ( CAMP_VAR *pVar , char *path );
int varSetShow ( CAMP_VAR *pVar , bool_t flag );
int varSetSet ( CAMP_VAR *pVar , bool_t flag );
int varSetRead ( CAMP_VAR *pVar , bool_t flag );
int varSetPoll ( CAMP_VAR *pVar , bool_t flag , float interval );
int varSetAlarm ( CAMP_VAR *pVar , bool_t flag , char *alarmAction );
int varSetLog ( CAMP_VAR *pVar , bool_t flag , char *logAction );
bool_t varNumTestAlert( CAMP_VAR* pVar, double val );
int varSetAlert ( CAMP_VAR *pVar , bool_t flag );
int varSetIsSet ( CAMP_VAR *pVar , bool_t flag );
int varSetPendSet ( CAMP_VAR *pVar , bool_t flag );
int varSetMsg ( CAMP_VAR *pVar , char *msg );
int varZeroStats ( CAMP_VAR *pVar );
void varNumZeroStats ( CAMP_NUMERIC *pNum );
int  varDoStats ( CAMP_VAR *pVar );
void varNumDoStats ( CAMP_NUMERIC *pNum );
void varFree ( CAMP_VAR *pVar );
int  varAdd ( char *path , CAMP_VAR *pVar );

/* camp_ins_priv.c */
int camp_insInit ( CAMP_VAR *pVar );
int camp_insDelete ( CAMP_VAR *pVar );
int camp_insOnline ( CAMP_VAR *pVar );
int camp_insOffline ( CAMP_VAR *pVar );

/* camp_if_priv.c */
int camp_ifSet ( CAMP_IF **ppIF, CAMP_IF* pIF );
int camp_ifOnline ( CAMP_IF *pIF );
int camp_ifOffline ( CAMP_IF *pIF );
int camp_ifRead ( CAMP_IF *pIF , CAMP_VAR *pVar , char *cmd , int cmd_len , char *buf , int buf_len , int *pRead_len );
int camp_ifWrite ( CAMP_IF *pIF , CAMP_VAR *pVar , char *cmd , int cmd_len );

/* camp_log_priv.c */

/* camp_alarm_priv.c */
void camp_alarmCheck ( char *alarmAction );
void alarm_update ( CAMP_VAR *pVar );
int alarm_TD ( bool_t flag );
int alarm_TI ( bool_t flag );

/* camp_req.c */
int REQ_add ( REQ *pReq );
int REQ_remove ( REQ *pReq );
void REQ_cancel ( CAMP_VAR *pVar , IO_TYPE type );
void REQ_cancelAllIns ( CAMP_VAR *pVar );
void REQ_addPoll ( CAMP_VAR *pVar );
int REQ_doPoll ( REQ* pReq );

/* camp_tcl.c */
Tcl_Interp* camp_tclInterp( void );
int camp_tclInit ( void );
void camp_tclEnd( void );
Tcl_Interp* campTcl_CreateInterp( void );
void campTcl_DeleteInterp( Tcl_Interp* interp );


/* if_none */
int if_none_init( void );
int if_none_online( CAMP_IF* pIF );
int if_none_offline( CAMP_IF* pIF );
int if_none_read( REQ* pReq );
int if_none_write( REQ* pReq );


/* if_rs232 */
int if_rs232_init( void );
int if_rs232_online ( CAMP_IF *pIF );
int if_rs232_offline ( CAMP_IF *pIF );
int if_rs232_read ( REQ *pReq );
int if_rs232_write ( REQ *pReq );

/* if_gpib */
int if_gpib_init( void );
int if_gpib_online ( CAMP_IF *pIF );
int if_gpib_offline ( CAMP_IF *pIF );
int if_gpib_read ( REQ *pReq );
int if_gpib_write ( REQ *pReq );

/* if_camac */
int if_camac_init ( void );
int if_camac_online ( CAMP_IF *pIF );
int if_camac_offline ( CAMP_IF *pIF );
int if_camac_read( REQ* pReq );
int if_camac_write( REQ* pReq );

/* if_indpak */
int if_indpak_init ( void );
int if_indpak_online ( CAMP_IF *pIF );
int if_indpak_offline ( CAMP_IF *pIF );
int if_indpak_read( REQ* pReq );
int if_indpak_write( REQ* pReq );

/* if_vme */
int if_vme_init ( void );
int if_vme_online ( CAMP_IF *pIF );
int if_vme_offline ( CAMP_IF *pIF );
int if_vme_read( REQ* pReq );
int if_vme_write( REQ* pReq );

/* if_tcpip */
int if_tcpip_init( void );
int if_tcpip_online ( CAMP_IF *pIF );
int if_tcpip_offline ( CAMP_IF *pIF );
int if_tcpip_read ( REQ *pReq );
int if_tcpip_write ( REQ *pReq );

/* ins_tcl */
int ins_tcl_init ( CAMP_VAR *pVar );
int ins_tcl_delete ( CAMP_VAR *pVar );
int ins_tcl_online ( CAMP_VAR *pVar );
int ins_tcl_offline ( CAMP_VAR *pVar );
int ins_tcl_set ( CAMP_VAR *pInsVar , CAMP_VAR *pVar , CAMP_SPEC *pSpec );
int ins_tcl_read ( CAMP_VAR *pInsVar , CAMP_VAR *pVar );

#endif /* _CAMP_SRV_H_ */
