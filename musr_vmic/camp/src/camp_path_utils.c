/*
 *  Name:       camp_path_utils.c
 *
 *  Purpose:    Routines to maintain CAMP variable paths
 *
 *              All CAMP variables (including instruments) are identified by
 *              a path that looks much like a Unix filename.  The first
 *              level of the path (e.g., /lake330_1) is unique to the
 *              instance of each instrument.  All the variables below this
 *              level (e.g., /lake330_1/reading) are common to the instrument
 *              type and are defined in the Tcl instrument driver.
 *
 *              There is one special path identifier.  When the instrument
 *              is referred to as "/~" this indicates that this should be
 *              expanded to the unique identifier of the instrument
 *              (e.g., /lake330_1) that is currently in context.  Normally
 *              this is only used in the Tcl instrument driver.  Since
 *              the instrument driver is general to many instruments of one
 *              type, the unique identifier must not be hardcoded in the
 *              Tcl driver.
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include <string.h>
#include "camp.h"

#define _camp_pathAtTop( p )  ( ( p[1] == '\0' ) ? TRUE : FALSE )

#ifndef NO_FORTRAN

#include "cfortran.h"
#undef  fcallsc
#define fcallsc( UN, LN )   preface_fcallsc( F_, f_, UN, LN )

FCALLSCFUN1(LONG,camp_pathAtTop,CAMP_PATHATTOP,camp_pathattop,STRING)
FCALLSCFUN1(VOID,camp_pathInit,CAMP_PATHINIT,camp_pathinit,PSTRING)
FCALLSCFUN2(LOGICAL,camp_pathCompare,CAMP_PATHCOMPARE,camp_pathcompare,STRING,STRING)
FCALLSCFUN2(VOID,camp_pathGetFirst,CAMP_PATHGETFIRST,camp_pathgetfirst,STRING,PSTRING)
FCALLSCFUN2(VOID,camp_pathGetLast,CAMP_PATHGETLAST,camp_pathgetlast,STRING,PSTRING)
FCALLSCFUN3(VOID,camp_pathGetNext,CAMP_PATHGETNEXT,camp_pathgetnext,STRING,STRING,PSTRING)
FCALLSCFUN2(VOID,camp_pathDownNext,CAMP_PATHDOWNNEXT,camp_pathdownnext,STRING,PSTRING)
FCALLSCFUN2(VOID,camp_pathDown,CAMP_PATHDOWN,camp_pathdown,PSTRING,STRING)
FCALLSCFUN1(VOID,camp_pathUp,CAMP_PATHUP,camp_pathup,PSTRING)

#endif


/*
 *  Test whether a path is at the root level (i.e., "/")
 */
bool_t 
camp_pathAtTop( char* path )
{
    return( _camp_pathAtTop( path ) );
}


/*
 *  Initialize a path to "/"
 */
void 
camp_pathInit( char* path )
{
    path[0] = '/';
    path[1] = '\0';
}


/*
 *  Copy a path, expanding a tilde identifier "/~" with the unique
 *  identifier of the current instrument.
 *
 *  The ident of the current
 *  instrument is set (using set_current_ident) whenever the context
 *  of a high level operation in the CAMP server becomes associated
 *  with an instrument (including insadd, insload, insInit, insDelete,
 *  insOnline, insOffline, varRead and varSetReq).
 */
char*
camp_pathExpand( char* path, char* expansion )
{
    char* p;

    if( ( path[1] == '\0' ) || ( path[1] != '~' ) )
    {
        strcpy( expansion, path );
    } 
    else
    {
        camp_pathInit( expansion );
        camp_pathDown( expansion, get_current_ident() );
        p = strchr( &path[1], '/' );
        if( p != NULL )
        {
            strcat( expansion, p );
        }
    }
    return( expansion );
}


/*
 *  Compare two paths, with expansion
 */
bool_t 
camp_pathCompare( char* path1, char* path2 )
{
    char temp_path1[LEN_PATH+1];
    char temp_path2[LEN_PATH+1];

    camp_pathExpand( path1, temp_path1 );
    camp_pathExpand( path2, temp_path2 );

    if( streq( temp_path1, temp_path2 ) ) return( TRUE );

    return( FALSE );
}


/*
 *  Get the first identifier in the path
 */
char*
camp_pathGetFirst( const char* path, char* ident )
{
    int len;

    if( path == NULL ) return( NULL );

    if( path[0] == '\0' ) len = 0;
    else
    {
        /*
         *  Get the ident after the first delimeter
         */
        len = strcspn( &path[1], "/" );
        strncpy( ident, &path[1], len );
    }

    ident[len] = '\0';

    return( ident );
}


/*
 *  Get the last identifier in the path
 */
char*
camp_pathGetLast( const char* path, char* ident )
{
    if( path == NULL ) return( NULL );
    if( ident == NULL ) return( NULL );

    /*
     *  Get the ident after the last delimeter
     */
    strcpy( ident, strrchr( path, '/' ) + 1 );

    return( ident );
}


/*
 *  Get the next identifier in the path based on a current path string
 */
char*
camp_pathGetNext( const char* path, const char* path_curr, char* ident_next )
{
    if( path == NULL ) return( NULL );
    if( path_curr == NULL ) return( NULL );
    if( strlen( path_curr ) > strlen( path ) ) return( NULL );

    /*
     *  Get the next ident from the path
     */
    if( _camp_pathAtTop( path_curr ) ) 
    {
        return( camp_pathGetFirst( path, ident_next ) );
    }
    else 
    {
        return( camp_pathGetFirst( &path[strlen(path_curr)], 
                ident_next ) );
    }

}


/*
 *  Move down the path based on a current path string.  The result is
 *  return in path_curr
 */
char*
camp_pathDownNext( const char* path, char* path_curr )
{
    char ident_next[LEN_IDENT+1];

    camp_pathGetNext( path, path_curr, ident_next );
    camp_pathDown( path_curr, ident_next );

    return( path_curr );
}


/*
 *  Append an identifier to a path string
 */
char*
camp_pathDown( char* path, const char* ident )
{
/*
    if( path == NULL ) return( NULL );
    if( ident == NULL ) return( NULL );
*/
    /*
     *  Append ident to the path
     */
    if( !_camp_pathAtTop( path ) ) strcat( path, "/" );
    strcat( path, ident );

    return( path );
}


/*
 *  Move up a level in the path.  The result is returned in "path".
 */
char*
camp_pathUp( char* path )
{
    char* pos;
/*
    if( path == NULL ) return( NULL );
*/
    /*
     *  Terminate the string at the last occurence of the delimeter
     */
    pos = strrchr( path, '/' );
    if( pos == path ) 
        *(++pos) = '\0';
    else 
        *pos =  '\0';

    return( path );
}


