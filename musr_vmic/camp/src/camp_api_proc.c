/*
 *  Name:       camp_api_proc.c
 *
 *  Purpose:    Layer for all CAMP applications
 *              See the CAMP Programmer's Guide for individual descriptions.
 *              See programs like camp_cmd.c, camp_cui.c and the MuSR
 *              data archivers for examples of usage (camp_cmd.c is the
 *              simplest to start).
 *
 *  Includes:   campSrv_* -  client routines that make RPC calls to the CAMP
 *                           server
 *              camp_clnt* - client routines that manage the particular state
 *                           of the CAMP client program
 *              All FORTRAN wrappers for the above, using cfortran.h
 *              FORTRAN wrappers have the preface "f_"
 *              
 *  Revision history:
 *    09-Feb-2001  DJA Dump/Undump
 *    14-Dec-1999  TW  Casting for Linux (gcc warnings)
 *
 *  $Log$
 *  Revision 1.7  2004/01/28 03:42:36  asnd
 *  Includes Suzannah's PPC revisions, plus minor editing.
 *
 *  Revision 1.6  2002/09/27 00:59:07  asnd
 *  camp_clntUpdate: check status of campSrv_varGet properly
 *
 *  Revision 1.5  2002/05/08 00:47:43  suz
 *  Add support for ppc (casting & ifdef VXWORKS)
 *
 *  Revision 1.4  2001/04/20 02:41:30  asnd
 *  Fixes for VMS build
 *
 *  Revision 1.3  2001/02/10 07:14:49  asnd
 *  DJA: insIfDump / insIfUndump API routines
 *
 */

#include "camp_clnt.h"

/*
 *  Need this for struct hostent
 */
#ifdef MULTINET
#include "multinet_common_root:[multinet.include]netdb.h"
#else
#include <netdb.h>
#endif /* MULTINET */

/*
 *  Define CAMP globals
 */
CAMP_SYS*        pSys = NULL;
CAMP_VAR*        pVarList = NULL;
bool_t           camp_debug = FALSE;

char                camp_serverHostname[LEN_NODENAME+1] = "";
enum clnt_stat        camp_clnt_stat;

struct hostent* camp_pServerHostent = NULL;

static CLIENT*        camp_client = NULL;
static timeval_t localTimeLastSysChange;
static timeval_t localTimeLastInsChange;

/*
 *  Define FORTRAN wrappers
 */
#ifndef NO_FORTRAN
#include "cfortran.h"
#undef  fcallsc
#define fcallsc( UN, LN )   preface_fcallsc( F_, f_, UN, LN )

FCALLSCFUN2(INT,camp_clntInit,CAMP_CLNTINIT,camp_clntinit,STRING,LONG)
FCALLSCFUN0(INT,camp_clntEnd,CAMP_CLNTEND,camp_clntend)
FCALLSCFUN0(INT,camp_clntUpdate,CAMP_CLNTUPDATE,camp_clntupdate)

          /*----------------------------------------*/
         /*  00X System actions :  to server       */
        /*----------------------------------------*/

FCALLSCFUN0(INT,campSrv_sysRundown,CAMPSRV_SYSRUNDOWN,campsrv_sysrundown)
FCALLSCFUN0(INT,campSrv_sysUpdate,CAMPSRV_SYSUPDATE,campsrv_sysupdate)
FCALLSCFUN2(INT,campSrv_sysLoad,CAMPSRV_SYSLOAD,campsrv_sysload,STRING,INT)
FCALLSCFUN2(INT,campSrv_sysSave,CAMPSRV_SYSSAVE,campsrv_syssave,STRING,INT)

          /*----------------------------------------*/
         /*  05X System actions :  from server     */
        /*----------------------------------------*/

FCALLSCFUN0(INT,campSrv_sysGet,CAMPSRV_SYSGET,campsrv_sysget)
FCALLSCFUN0(INT,campSrv_sysGetDyna,CAMPSRV_SYSGETDYNA,campsrv_sysgetdyna)
FCALLSCFUN1(INT,campSrv_sysDir,CAMPSRV_SYSDIR,campsrv_sysdir,STRING)

          /*----------------------------------------*/
         /*  10X instrument actions :  to server   */
        /*----------------------------------------*/

FCALLSCFUN2(INT,campSrv_insAdd,CAMPSRV_INSADD,campsrv_insadd,STRING,STRING)
FCALLSCFUN1(INT,campSrv_insDel,CAMPSRV_INSDEL,campsrv_insdel,STRING)
FCALLSCFUN2(INT,campSrv_insLock,CAMPSRV_INSLOCK,campsrv_inslock,STRING,LOGICAL)
FCALLSCFUN2(INT,campSrv_insLine,CAMPSRV_INSLINE,campsrv_insline,STRING,LOGICAL)
FCALLSCFUN3(INT,campSrv_insLoad,CAMPSRV_INSLOAD,campsrv_insload,STRING,STRING,INT)
FCALLSCFUN3(INT,campSrv_insSave,CAMPSRV_INSSAVE,campsrv_inssave,STRING,STRING,INT)
FCALLSCFUN4(INT,campSrv_insIfRead,CAMPSRV_INSIFREAD,campsrv_insifread,STRING,STRING,INT,INT)
FCALLSCFUN3(INT,campSrv_insIfWrite,CAMPSRV_INSIFWRITE,campsrv_insifwrite,STRING,STRING,INT)
FCALLSCFUN8(INT,campSrv_insIfDump,CAMPSRV_INSIFDUMP,campsrv_insifdump,STRING,STRING,INT,STRING,INT,STRING,INT,INT)
FCALLSCFUN6(INT,campSrv_insIfUndump,CAMPSRV_INSIFUNDUMP,campsrv_insifundump,STRING,STRING,INT,STRING,INT,INT)

          /*------------------------------------*/
         /*  20X Data actions :  to server     */
        /*------------------------------------*/

FCALLSCFUN2(INT,campSrv_varNumSetVal,CAMPSRV_VARNUMSETVAL,campsrv_varnumsetval,STRING,DOUBLE)
FCALLSCFUN3(INT,campSrv_varNumSetTol,CAMPSRV_VARNUMSETTOL,campsrv_varnumsettol,STRING,INT,FLOAT)
FCALLSCFUN2(INT,campSrv_varSelSetVal,CAMPSRV_VARSELSETVAL,campsrv_varselsetval,STRING,BYTE)
FCALLSCFUN2(INT,campSrv_varStrSetVal,CAMPSRV_VARSTRSETVAL,campsrv_varstrsetval,STRING,STRING)
FCALLSCFUN2(INT,campSrv_varLnkSetVal,CAMPSRV_VARLNKSETVAL,campsrv_varlnksetval,STRING,STRING)
FCALLSCFUN1(INT,campSrv_varRead,CAMPSRV_VARREAD,campsrv_varread,STRING)
FCALLSCFUN3(INT,campSrv_varPoll,CAMPSRV_VARPOLL,campsrv_varpoll,STRING,LOGICAL,FLOAT)
FCALLSCFUN3(INT,campSrv_varAlarm,CAMPSRV_VARALARM,campsrv_varalarm,STRING,LOGICAL,STRING)
FCALLSCFUN3(INT,campSrv_varLog,CAMPSRV_VARLOG,campsrv_varlog,STRING,LOGICAL,STRING)
FCALLSCFUN1(INT,campSrv_varZero,CAMPSRV_VARZERO,campsrv_varzero,STRING)

          /*------------------------------------*/
         /*  25X Data actions :  from server   */
        /*------------------------------------*/

FCALLSCFUN2(INT,campSrv_varGet,CAMPSRV_VARGET,campsrv_varget,STRING,INT)

          /*------------------------------------*/
         /*  30X misc                          */
        /*------------------------------------*/

FCALLSCFUN1(INT,campSrv_cmd,CAMPSRV_CMD,campsrv_cmd,STRING)

#endif /* !NO_FORTRAN */


#include <rpc/rpc.h>
#include <sys/socket.h>
#ifdef PPCxxx
#include <errno.h>
#else
#include <sys/errno.h>
#endif
/*        Already included!!  #include <netdb.h>  */


#ifdef NOT_USED
#ifndef MULTINET
extern CLIENT *
my_clnttcp_create( struct sockaddr_in *raddr,
                   u_long prog,
                   u_long vers,
                   register int *sockp,
                   u_int sendsz,
                   u_int recvsz );

/*
 * Generic client creation: takes (hostname, program-number, protocol) and
 * returns client handle. Default options are set, which the user can 
 * change using the rpc equivalent of ioctl()'s.
 */
static CLIENT *
my_clnt_create(hostname, prog, vers, proto)
	char *hostname;
	unsigned prog;
	unsigned vers;
	char *proto;
{
	struct hostent *h;
	struct protoent *p;
	struct sockaddr_in sin;
	int sock;
	struct timeval tv;
	CLIENT *client;

#ifdef VXWORKS
#ifdef PPCxxx
	h =  (struct hostent *)hostGetByName(hostname);
#else
	h =  (struct hostent *)hostGetByName(hostname);
#endif
#else
	h =  (struct hostent *) gethostbyname(hostname);
#endif
	if (h == NULL) {
		/*printf("Error in hostGetByName\n");*/
		rpc_createerr.cf_stat = RPC_UNKNOWNHOST;
		return (NULL);
	}
	if (h->h_addrtype != AF_INET) {
		/*
		 * Only support INET for now
		 */
		rpc_createerr.cf_stat = RPC_SYSTEMERROR;
		rpc_createerr.cf_error.re_errno = EAFNOSUPPORT; 
		return (NULL);
	}
#ifdef linux
	bzero((char *) &sin, sizeof(sin));
#endif
	sin.sin_family = h->h_addrtype;
	sin.sin_port = 0;
#ifndef linux
	bzero(sin.sin_zero, sizeof(sin.sin_zero));
#endif
	bcopy(h->h_addr, (char*)&sin.sin_addr, h->h_length);

	p = (struct protoent *) getprotobyname(proto);
	if (p == NULL) {
		rpc_createerr.cf_stat = RPC_UNKNOWNPROTO;
		rpc_createerr.cf_error.re_errno = EPFNOSUPPORT; 
		return (NULL);
	}
	sock = RPC_ANYSOCK;
	switch (p->p_proto) {
	case IPPROTO_UDP:
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		client = clntudp_create(&sin, prog, vers, tv, &sock);
		if (client == NULL) {
			return (NULL);
		}
		tv.tv_sec = 25;
		clnt_control(client, CLSET_TIMEOUT, (char*)&tv);
		break;
	case IPPROTO_TCP:
		client = my_clnttcp_create(&sin, prog, vers, &sock, 0, 0);
		if (client == NULL) {
			return (NULL);
		}
		tv.tv_sec = 25;
		tv.tv_usec = 0;
		clnt_control(client, CLSET_TIMEOUT, (char*)&tv);
		break;
	default:
		rpc_createerr.cf_stat = RPC_SYSTEMERROR;
		rpc_createerr.cf_error.re_errno = EPFNOSUPPORT; 
		return (NULL);
	}
	return (client);
}
#endif /* !MULTINET */
#endif /* NOT_USED */

int
camp_clntInit( char* serverName, long clientTimeout )
{
    int status;
    timeval_t tv;
    int uid;
    int gid;
    int gids[3];
    char os[8];
    char hostname[LEN_NODENAME];
    int  host_addr=0;

/*
 *  printf("\n");
 *  printf("camp_clntInit: now starting with serverName=%s, timeout=%d\n",
 *     serverName,clientTimeout);
 *  fflush( stdout );
 */
    cleartimeval( &localTimeLastSysChange );
    cleartimeval( &localTimeLastInsChange );

#ifdef VXWORKS
    host_addr = hostGetByName(serverName);
#ifdef PPCxxx
    if (host_addr == -1)
#else
    if (host_addr == 0)
#endif
    {
        /* printf("camp_clntInit: error: can't find server \"%s\"\n", serverName ); */
        camp_appendMsg( "camp_clntInit: error: can't find server \"%s\"", 
                        serverName );
        return( CAMP_FAILURE );
    }
    strcpy(camp_serverHostname, serverName );
#else
    camp_pServerHostent = (struct hostent *)gethostbyname(serverName);
    if( camp_pServerHostent == NULL )
    {
	/* printf("Can't find server  \"%s\"",serverName); */
        camp_appendMsg( "camp_clntInit: error: can't find server \"%s\"", 
                        serverName );
        return( CAMP_FAILURE );
    }

    /*
     *  initialize global and static variables
     */
    strcpy( camp_serverHostname, camp_pServerHostent->h_name );
#endif

    /*
     *  build the camp_client structure
     */
    /*  printf( "calling clnt_create with camp_serverHostname=%s...\n",camp_serverHostname ); */

#ifdef MULTINET
    camp_client = clnt_create( camp_serverHostname, CAMP_SRV, CAMP_SRV_VERS, "tcp" );
#else
    /*
     *  19-Dec-2000  TW  Tried using own version of clnt_create in order
     *                   to have control of the timeout at the first
     *                   attempted connection.
     *                   But, BE CAREFUL, using my_clnt_create on Linux
     *                   caused the CUI to intermittently act strangely.
     *                   (getting false RPC timeouts).
     *                   So, don't use it.
     */
/*    camp_client = my_clnt_create( camp_serverHostname, CAMP_SRV,
 *     CAMP_SRV_VERS, "tcp" ); */
    camp_client = clnt_create( camp_serverHostname, CAMP_SRV, CAMP_SRV_VERS, "tcp" );

#endif /* MULTINET */
    /* printf( "clnt_create is done\n" );*/ 

    if( camp_client == NULL ) 
    {
        camp_appendMsg( clnt_spcreateerror( camp_serverHostname ) );
        return( CAMP_FAILURE );
    }

    /*
     *  Possibly change the client timeout
     *
     
     *  16-Dec-1999  TW  Use this again, but set the 'hard' RPC timeout
     *                   to a value somewhat larger than the 'soft' timeout 
     */
    /*
     * printf( "clientTimeout=%d \n",clientTimeout );
     * fflush( stdout );
     */
    if( clientTimeout > 0 )
    {
        tv.tv_sec = clientTimeout;
        tv.tv_usec = 0;
        clnt_control( camp_client, CLSET_TIMEOUT, (char*)&tv );
    }
    
    /*
     *  build the camp_client authorization
     */
    gethostname( hostname, LEN_NODENAME );
    /*
     * printf( "called gethostname with hostname=%s,
     *     LEN_NODENAME=%d\n",hostname,LEN_NODENAME);
     * fflush( stdout );
     */
    
#ifdef VXWORKS
    uid = 0;
    gid = 0;
    gids[1] = CAMP_OS_BSD;
#else
    uid = getuid();
    gid = getgid();
    gids[0] = getpid();
#endif
    /*
     *  Define client operating system
     *  Must be 7 characters or less
     *  Valid selections are "vms", "bsd"
     */
#if defined( VMS )
    gids[1] = CAMP_OS_VMS;
#else /* Works for Ultrix, Digital Unix, SunOS */
    gids[1] = CAMP_OS_BSD;
#endif

    /*
     *  TW  04-Dec-1996  Server controls timeout
     */
    gids[2] = (int)clientTimeout;

    camp_client->cl_auth = authunix_create( hostname, uid, gid, 3, gids );
    if( camp_client->cl_auth == NULL ) 
    {
	/*printf("camp_clntInit: no authorization for camp host \"%s\"",camp_serverHostname);*/
        camp_appendMsg( clnt_spcreateerror( camp_serverHostname ) );
        return( CAMP_FAILURE );
    }

    /*
     *  initialize global variables
     */
    camp_setMsg( "" );

    /*
     *  initialize system section
     */
/*
    status = campSrv_sysGet();
    if( _failure( status ) ) return( status );
*/
    return( CAMP_SUCCESS );
}


int
camp_clntEnd( void )
{
    camp_setMsg( "" );

    _xdr_free( xdr_CAMP_VAR, pVarList );
    _xdr_free( xdr_CAMP_SYS, pSys );
    cleartimeval( &localTimeLastSysChange );
    cleartimeval( &localTimeLastInsChange );

    if( camp_client != NULL ) 
    {
        if( camp_client->cl_auth != NULL ) 
        {
            auth_destroy( camp_client->cl_auth );
            camp_client->cl_auth = NULL;
        }

        /*
         *  Close the socket and free the memory
         *  from the clnt_create() call
         */
        clnt_destroy( camp_client );
        camp_client = NULL;
    }

    return( CAMP_SUCCESS );
}


int
camp_clntUpdate( void )
{
    int status;

    if( pSys == NULL )
    {
        status = campSrv_sysGet();
        if( _failure( status ) ) 
        {
            camp_appendMsg( "failed campSrv_sysGet()" );
            return( status );
        }
    }
    else
    {
        status = campSrv_sysGetDyna();
        if( _failure( status ) ) 
        {
            camp_appendMsg( "failed campSrv_sysGetDyna()" );
            return( status );
        }

        if( difftimeval( &localTimeLastSysChange, 
                         &pSys->pDyna->timeLastSysChange ) < 0.0 )
        {
            copytimeval( &pSys->pDyna->timeLastSysChange, 
                         &localTimeLastSysChange );

            status = campSrv_sysGet();
            if( _failure( status ) ) 
            {
                camp_appendMsg( "failed campSrv_sysGet()" );
                return( status );
            }
        }
    }

    if( difftimeval( &localTimeLastInsChange, 
                     &pSys->pDyna->timeLastInsChange ) < 0.0 )
    {
        copytimeval( &pSys->pDyna->timeLastInsChange, &localTimeLastInsChange );

        status = campSrv_varGet( "/", CAMP_XDR_ALL );
        if( _failure( status ) ) 
        {
            camp_appendMsg( "failed campSrv_varGet()" );
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


          /*----------------------------------------*/
         /*  00X System actions :  to server       */
        /*----------------------------------------*/

int 
campSrv_sysRundown( void )
{
    RES* pRes;

    pRes = campsrv_sysrundown_13( NULL, camp_client );
    return( pRes->status );
}


int 
campSrv_sysUpdate( void )
{
    RES* pRes;

    pRes = campsrv_sysupdate_13( NULL, camp_client );
    return( pRes->status );
}


int
campSrv_sysLoad( char* filename, int flag )
{
    RES* pRes;
    FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.flag = flag;
    req.filename = filename;

    pRes = campsrv_sysload_13( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }
    return( pRes->status );
}


int
campSrv_sysSave( char* filename, int flag )
{
    RES* pRes;
    FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.flag = flag;
    req.filename = filename;

    pRes = campsrv_syssave_13( &req, camp_client );
    return( pRes->status );
}


          /*----------------------------------------*/
         /*  05X System actions :  from server     */
        /*----------------------------------------*/

int 
campSrv_sysGet( void )
{
    CAMP_SYS_res* pRes; 

/*    _xdr_free( xdr_CAMP_SYS, pSys ); */
    pRes = campsrv_sysget_13( NULL, camp_client );
/*    pSys = pRes->pSys; */

    return( pRes->status );
}


int
campSrv_sysGetDyna( void )
{
    SYS_DYNAMIC_res* pRes;

/*    _xdr_free( xdr_SYS_DYNAMIC, pSys->pDyna );*/
    pRes = campsrv_sysgetdyna_13( NULL, camp_client );
/*    pSys->pDyna = pRes->pDyna;*/

    return( pRes->status );
}


int
campSrv_sysDir( char* filespec )
{
    FILE_req req;
    DIR_res* pRes;

    bzero((void *)&req, sizeof( req ) );
    req.filename = filespec;

/*    _xdr_free( xdr_SETUP, pSys->pAvailSetups );*/
    pRes = campsrv_sysdir_13( &req, camp_client );
/*    pSys->pAvailSetups = pRes->pSetup;*/

    return( pRes->status );
}


          /*----------------------------------------*/
         /*  10X instrument actions :  to server     */
        /*----------------------------------------*/

int
campSrv_insAdd( char* typeIdent, char* ident )
{
    int status;
    RES* pRes;
    INS_ADD_req req;

    if( ident == NULL ) return( CAMP_INVAL_INS );
    if( typeIdent == NULL ) return( CAMP_INVAL_INS_TYPE );

    bzero((void *)&req, sizeof( req ) );
    req.ident = ident;
    req.typeIdent = typeIdent;

    pRes = campsrv_insadd_13( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }

    return( pRes->status );
}


/*
 *  campSrv_insDel,  send path because could be a reference to a instrument
 *                    included under another instrument
 */
int 
campSrv_insDel( char* path )
{
    RES* pRes;
    int status;
    DATA_req req;

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_insdel_13( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }

    return( pRes->status );
}


int 
campSrv_insLock( char* path, bool_t flag )
{
    RES* pRes;
    INS_LOCK_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pRes = campsrv_inslock_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insLine( char* path, bool_t flag )
{
    RES* pRes;
    INS_LINE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pRes = campsrv_insline_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insLoad( char* path, char* filename, int flag )
{
    RES* pRes;
    INS_FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.datFile = filename;
    req.flag = flag;

    pRes = campsrv_insload_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insSave( char* path, char* filename, int flag )
{
    RES* pRes;
    INS_FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.datFile = filename;
    req.flag = flag;

    pRes = campsrv_inssave_13( &req, camp_client );
    return( pRes->status );
}


int
campSrv_insIf( char* path, char* typeIdent, float accessDelay, char* defn )
{
    RES* pRes;
    INS_IF_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.IF.typeIdent = typeIdent;
    req.IF.accessDelay = accessDelay;
    req.IF.defn = defn;

    pRes = campsrv_insifset_13( &req, camp_client );
    return( pRes->status );
}


RES*
campSrv_insIfRead( char* path, char* cmd, int cmd_len, int buf_len )
{
    TOKEN tok;
    RES* pRes;
    INS_READ_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifread_13( &req, camp_client );
    return( pRes );
}


int
campSrv_insIfWrite( char* path, char* cmd, int cmd_len )
{
    TOKEN tok;
    RES* pRes;
    INS_WRITE_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;

    pRes = campsrv_insifwrite_13( &req, camp_client );
    return( pRes->status );
}

RES*
campSrv_insIfDump( char* path, char* fname, int fname_len, char* cmd, int cmd_len, char* skip, int skip_len, int buf_len )
{
    TOKEN tok;
    RES* pRes;
    INS_DUMP_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.fname.fname_val = fname;
    req.fname.fname_len = fname_len;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.skip.skip_val = skip;
    req.skip.skip_len = skip_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifdump_13( &req, camp_client );
    return( pRes );
}


int
campSrv_insIfUndump( char* path, char* fname, int fname_len, char* cmd, int cmd_len, int buf_len )
{
    TOKEN tok;
    RES* pRes;
    INS_UNDUMP_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.fname.fname_val = fname;
    req.fname.fname_len = fname_len;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifwrite_13( &req, camp_client );
    return( pRes->status );
}


          /*------------------------------------*/
         /*  20X Data actions :  to server     */
        /*------------------------------------*/

int  
campSrv_varSet( char* path, caddr_t pSpec )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
        return( CAMP_INVAL_VAR );

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_SPEC_u.pNum = (CAMP_NUMERIC*)pSpec;

    pRes = campsrv_varset_13( &req, camp_client );
    return( pRes->status );
}


int  
campSrv_varDoSet( char* path, caddr_t pSpec )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
        return( CAMP_INVAL_VAR );

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_SPEC_u.pNum = (CAMP_NUMERIC*)pSpec;

    pRes = campsrv_vardoset_13( &req, camp_client );
    return( pRes->status );
}


int
campSrv_varNumSetVal( char* path, double val )
{
    CAMP_NUMERIC num;
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return;

    bcopy((void *)pVar->spec.CAMP_SPEC_u.pNum, (void *)&num, sizeof( CAMP_NUMERIC ) );
    num.val = val;

    return( campSrv_varSet( path, (caddr_t)&num ) );
}


int
campSrv_varNumSetTol( char* path, u_long tolType, float tol )
{
    CAMP_NUMERIC num;
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path );
    if( pVar == NULL ) return;

    bcopy( (void *)pVar->spec.CAMP_SPEC_u.pNum, (void *)&num, sizeof( CAMP_NUMERIC ) );
    num.tol = tol;
    num.tolType = tolType;

    return( campSrv_varSet( path, (caddr_t)&num ) );
}


int
campSrv_varSelSetVal( char* path, u_char val )
{
    CAMP_SELECTION spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.val = val;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varStrSetVal( char* path, char* val )
{
    CAMP_STRING spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.val = val;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varArrSetVal( char* path, caddr_t pVal )
{
    CAMP_ARRAY spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.pVal = pVal;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varLnkSetVal( char* path, char* val )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_LINK spec;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetTrueP( path ) ) == NULL )
        return( CAMP_INVAL_VAR );

    if( pVar->core.varType != CAMP_VAR_TYPE_LINK ) 
        return( CAMP_INVAL_VAR );

    bzero((void *)&spec, sizeof( spec ) );
    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_SPEC_u.pLnk = &spec;
    req.spec.CAMP_SPEC_u.pLnk->path = val;

    pRes = campsrv_varlnkset_13( &req, camp_client );
    return( pRes->status );
}


int  
campSrv_varRead( char* path )
{
    RES* pRes;
    DATA_req req;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetp( path ) ) == NULL )
        return( CAMP_INVAL_VAR );

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_varread_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_varPoll( char* path, bool_t flag, float pollInterval )
{
    RES* pRes;
    DATA_POLL_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.pollInterval = pollInterval;

    pRes = campsrv_varpoll_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_varAlarm( char* path, bool_t flag, char* alarmAction )
{
    RES* pRes;
    DATA_ALARM_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.alarmAction = alarmAction;
    
    pRes = campsrv_varalarm_13( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_varLog( char* path, bool_t flag, char* logAction )
{
    RES* pRes;
    DATA_LOG_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.logAction = logAction;

    pRes = campsrv_varlog_13( &req, camp_client );
    return( pRes->status );
}


/*
 *  campSrv_varZero,  zero the statistics of a data item and 
 *                           all data items below
 */
int 
campSrv_varZero( char* path )
{
    RES* pRes;
    DATA_req req;

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_varzero_13( &req, camp_client );
    return( pRes->status );
}


          /*------------------------------------*/
         /*  25X Data actions :  from server   */
        /*------------------------------------*/

/*
 *  campSrv_varGet() - Get a data item from the server and store
 *                          it locally
 */
int 
campSrv_varGet( char* path, int flag )
{
    DATA_GET_req req;
    CAMP_VAR_res* pVar_res;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pVar_res = campsrv_varget_13( &req, camp_client );
    return( pVar_res->status );
}


          /*----------------------------------------*/
         /*  30X misc                              */
        /*----------------------------------------*/

int
campSrv_cmd( char* cmd )
{
    CMD_req req;
    RES* pRes;

    bzero((void *)&req, sizeof(req) );
    req.cmd = cmd;

    pRes = campsrv_cmd_13( &req, camp_client );
    return( pRes->status );
}


