/*
 *  Name:        camp_cui_menucb.c
 *
 *  Purpose:     Callback routines for "menu" selections
 *               Including, entry points to most popup selection windows
 *               and the routines to invoke on selection.
 *
 *  Called by:   varPicker (in camp_cui_data.c) calls many entry points
 *               based on user keystrokes and context.
 * 
 *  Revision history:
 *   12-Dec-1996  TW  Don't retry RPC requests, just try once.
 *      Oct-1999  DA  Add "choose" menu entry for paths
 *   15-Dec-1999  TW  Conditional compilation of basename -> basename_tw 
 *                    for Linux (who made the change originally?)
 *                    This could also be a permanent change in libc_tw
 *   22-Jan-2000  DA  Make hotkeys work in menu.
 *   19-Dec-2000  TW  waitForServer now has parameter
 *   19-Dec-2001  DA  Remove conditional basename: always use basename_tw.
 *                    (Where did VAX basename come from before?)
 *   17-Sep-2004  DA  Shorten transient message display
 *   05-Jul-2005  DA  Alter save/restore functions (add Recreate)
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <curses.h>
#include "camp_cui.h"

/* line from camp.h : */
#define INI_PFMT    "/campnode/cfg/camp_ins_%s.ini"

extern long poll_interval;
extern unsigned long msgPause_us;
extern bool_t noisy;

char currVarPath[LEN_PATH+1];

extern enum clnt_stat camp_clnt_stat;

/*
 *  Display transient status messages for time selected by user; default
 *  0.7 sec.   Some systems may only handle integer seconds, so round.
 */

#ifdef linux
#define _msgSleep usleep( msgPause_us )
#else
#define _msgSleep sleep( (unsigned int)((msgPause_us+800000)/1000000) )
#endif

#define _doRPC( proc, title, msg ) \
        /* do { */ \
	  if( strcmp( msg, "" ) != 0 ) startMessage( title, msg ); \
          status = proc; \
          if( _failure( status ) ) \
	    { \
	      if( strcmp( msg, "" ) == 0 ) startMessage( title, "" ); \
              addMessage( "\n  " ); \
              addMessage( camp_getMsg() ); \
	      confirmMessage(); \
	      deleteMessage(); \
	    } \
	  else if( strcmp( msg, "" ) != 0 ) \
	    { \
	      addMessage( "done" ); \
              _msgSleep ; \
	      deleteMessage(); \
	    } \
        /* } while( _failure( checkRPCstatus() ) ); */ \
          if( camp_clnt_stat != RPC_SUCCESS ) {waitForServer( 0 );}\
          if( _failure( status ) ) return( status );

int
menuFileQuit( void )
{
    camp_cui_rundown();
    exit( 1 );
}


int
menuFileLoadNew( void )
{
    int status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    _doRPC( campSrv_sysDir(CAMP_CFG_MASK), "Dir config files", "" )

    if( !inputSetupFile( "Open config file", 
                         pSys->pDyna->cfgFile, str, CAMP_CFG_SFMT_BASE ) )
    {
        return( CAMP_SUCCESS );
    }

    sprintf( filename, CAMP_CFG_PFMT, str );

    sprintf( buf, "Opening config file \"%s\" (\"%s\")...", str, filename );

    _doRPC( campSrv_sysLoad(filename,1), "Open config", buf );

    updateData();
    updateDisplay();
}


int
menuFileLoad( void )
{
    int status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    _doRPC( campSrv_sysDir( CAMP_CFG_MASK ), "Dir config files", "" );

    if( !inputSetupFile( "Import config file", 
                         pSys->pDyna->cfgFile, str, CAMP_CFG_SFMT_BASE ) )
    {
        return( CAMP_SUCCESS );
    }

    sprintf( filename, CAMP_CFG_PFMT, str );

    sprintf( buf, "Importing config file \"%s\" (\"%s\")...", 
                  str, filename );

    _doRPC( campSrv_sysLoad( filename, 0 ), "Import config", buf );

    updateData();
    updateDisplay();
}


int 
menuFileSave( void )
{
    int status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1] = { "" };
    char buf[LEN_BUF+1];

    buf[0] = '\0';
    if( pSys->pDyna->cfgFile != NULL )
    {
        /* basename --> basename_tw for linux */
        basename_tw( pSys->pDyna->cfgFile, filename );
        sscanf( filename, CAMP_CFG_SFMT_BASE, str );
    }

    if( !inputString( "Save config file", 32, str, str, isIdentChar ) )
    {
        return( CAMP_SUCCESS );
    }

    if( str[0] == '\0' )
      { /* no file name, so use the same as for auto-save of config */
        strcpy( filename, CAMP_SRV_AUTO_SAVE );
      }
    else
      { /* get full path and file name */
        sprintf( filename, CAMP_CFG_PFMT, str );
      }

    sprintf( buf, "Saving config file %s (%s)...", str, filename );

    _doRPC( campSrv_sysSave( filename, 0 ), "Save config file", buf );
}


int
menuEdit( u_short key )
{
    CAMP_VAR* pVar;
    CAMP_VAR* pLinkVar;
    int n;
    int i;
    int select;
    u_short hotKey[MAX_NUM_SELECTIONS];
    HK_FLAG hotKeyFlag = HK_HOT;
    char buf[LEN_BUF+1];
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    pVar = camp_varGetTrueP( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( pVar->core.varType == CAMP_VAR_TYPE_LINK )
    {
        pLinkVar = pVar;
        pVar = camp_varGetp( currVarPath );
        if( pVar == NULL ) return( CAMP_SUCCESS );
    }

    n = 0;

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
      case CAMP_VAR_TYPE_STRING:
      case CAMP_VAR_TYPE_SELECTION:
      case CAMP_VAR_TYPE_LINK:
      case CAMP_VAR_TYPE_ARRAY:
        if( pVar->core.status & CAMP_VAR_ATTR_SET )
        {
            hotKey[n] = 's';
            choices[n] = "Set"; callback[n] = menuVarSet; n++;
        }
        if( pVar->core.status & CAMP_VAR_ATTR_READ )
        {
            hotKey[n] = 'r';
            choices[n] = "Read"; callback[n] = menuVarRead; n++;
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_POLL )
        {
            hotKey[n] = 'p';
            if( pVar->core.status & CAMP_VAR_ATTR_POLL )
            {
                choices[n] = "Polling OFF"; callback[n] = menuVarPoll; n++;
            }
            else
            {
                choices[n] = "Polling ON"; callback[n] = menuVarPoll; n++;
            }
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            hotKey[n] = 'a';
            if( pVar->core.status & CAMP_VAR_ATTR_ALARM )
            {
                choices[n] = "Alarm OFF"; callback[n] = menuVarAlarm; n++;
            }
            else
            {
                choices[n] = "Alarm ON"; callback[n] = menuVarAlarm; n++;
            }
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_LOG )
        {
            hotKey[n] = 'l';
            if( pVar->core.status & CAMP_VAR_ATTR_LOG )
            {
                choices[n] = "Logging OFF"; callback[n] = menuVarLog; n++;
            }
            else
            {
                choices[n] = "Logging ON"; callback[n] = menuVarLog; n++;
            }
        }
        break;

      case CAMP_VAR_TYPE_STRUCTURE:
	hotKey[n] = 'c';
        choices[n] = "Choose ->"; callback[n] = menuInsChoose; n++;

        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        break;

      case CAMP_VAR_TYPE_INSTRUMENT:
	hotKey[n] = 'c';
        choices[n] = "Choose ->"; callback[n] = menuInsChoose; n++;

        if( ( pVar->spec.CAMP_SPEC_u.pIns->pIF == NULL )
            || !( pVar->spec.CAMP_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE ) )
        {
            hotKey[n] = 'i';
            choices[n] = "Interface"; callback[n] = menuInsIf; n++;
        }

        if( pVar->spec.CAMP_SPEC_u.pIns->pIF != NULL )
        {
            hotKey[n] = 'o';
            if( pVar->spec.CAMP_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE )
            {
                choices[n] = "Offline"; callback[n] = menuInsLine; n++;
            }
            else
            {
                choices[n] = "Online"; callback[n] = menuInsLine; n++;
            }
        }

        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        hotKey[n] = 'l';
        choices[n] = "Load ini"; callback[n] = menuInsLoadIni; n++;
        hotKey[n] = 's';
        choices[n] = "Save ini"; callback[n] = menuInsSave; n++;
        hotKey[n] = 'b';
        choices[n] = "Become (saved ini)"; callback[n] = menuInsBecomeIni; n++;
        hotKey[n] = 'r';
        choices[n] = "Redirect ini"; callback[n] = menuInsRedirect; n++;
        hotKey[n] = 'k';
        if( pVar->core.status & CAMP_INS_ATTR_LOCKED )
        {
            choices[n] = "Lock OFF"; callback[n] = menuInsLock; n++;
        }
        else
        {
            choices[n] = "Lock ON"; callback[n] = menuInsLock; n++;
        }
        break;
    }

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            hotKey[n] = 't';
            choices[n] = "Tolerance"; callback[n] = menuVarTol; n++;
        }
        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        break;

      case CAMP_VAR_TYPE_LINK:
        if( pLinkVar->core.status & CAMP_VAR_ATTR_SET )
        {
            hotKey[n] = 'k';
            choices[n] = "Set Link"; callback[n] = menuLnkVarSet; n++;
        }
        break;
    }

    hotKey[n] = 'v';
    choices[n] = "View parameters"; callback[n] = menuVarShow; n++;

    sprintf( buf, "Variable %s", currVarPath );

    select = -1;

    if( key != 0 )
    {
        key = tolower( key );
        for( i = 0; i < n; i++ )
        {
            if( key == hotKey[i] )
            {
                select = i;
                break;
            }
        }
        if( select == -1 )
        {
            /*  
             *  DA: beep now provided by alert window
             *  BEEP(); 
             */
            return( CAMP_SUCCESS );
        }
    }
    else
    {
        if( !inputHotSelectWin( buf, n, choices, 0, &select, hotKeyFlag, hotKey ) )
        {
            return( CAMP_SUCCESS );
        }
    }

    (*callback[select])();

    updateData();
    updateDisplay();
}

/*
int
menuViewNavigate( void )
{
    CAMP_VAR* pVar_curr;
    CAMP_VAR* pVar;
    int n;
    int select;
    char buf[LEN_BUF+1];
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    strcpy( currVarPath, currViewPath );

    for( ;; )
    {
        if( camp_pathAtTop( currVarPath ) )
        {
            pVar_curr = pVarList;
        }
        else
        {
            pVar_curr = camp_varGetp( currVarPath );
            if( pVar_curr == NULL ) 
            {
                return( CAMP_SUCCESS );
            }

            pVar_curr = pVar_curr->pChild;
        }

        n = 0;

        choices[n++] = "< OK >";

        if( !camp_pathAtTop( currVarPath ) )
        {
            choices[n++] = "< Up >";
        }

        for( pVar = pVar_curr; pVar != NULL; pVar = pVar->pNext )
        {
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) ||
                ( pVar->core.varType == CAMP_VAR_TYPE_STRUCTURE ) )
            {
                choices[n++] = pVar->core.ident;
            }
        }

        sprintf( buf, "Set view path [%s]", currVarPath );

        if( !inputSelectWin( buf, n, choices, 0, &select ) )
        {
            return( CAMP_SUCCESS );
        }

        if( select == 0 ) 
        {
            break;
        }
        else if( streq( choices[select], "< Up >" ) )
        {
            camp_pathUp( currVarPath );
        }
        else
        {
            camp_pathDown( currVarPath, choices[select] );
        }
    }

    strcpy( currViewPath, currVarPath );
    updateData();
    updateDisplay();
}
*/

/*
int 
menuViewRoot( void )
{
    camp_pathInit( currViewPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewUp( void )
{
    camp_pathUp( currViewPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewDown( void )
{
    if( !inputVar( currVarPath, "Variable", 
                        CAMP_VAR_TYPE_STRUCTURE | CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
        return( CAMP_SUCCESS );

    strcpy( currViewPath, currVarPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewRedraw( void )
{
    refreshScreen();
}
*/
/*
int
menuViewRate( void )
{
    inputInteger( "Set screen update rate (seconds)", 
                  poll_interval, &poll_interval );
}
*/

int 
menuInsAdd( void )
{
    int status;
    char typeIdent[LEN_IDENT+1];
    char ident[LEN_IDENT+1];
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];

    if( !inputInsAvail( ident, typeIdent ) ) 
    {
        return( CAMP_SUCCESS );
    }

    if( streq( typeIdent, "" ) )
    {
        if( !inputInsType( typeIdent ) ) 
        {
            return( CAMP_SUCCESS );
        }

        if( !inputString( "Unique identifier", LEN_IDENT, "", 
                           ident, isIdentChar ) ) 
        {
            return( CAMP_SUCCESS );
        }
    }

    sprintf( buf, "Adding instrument \"%s\"...", ident );

    _doRPC( campSrv_insAdd( typeIdent, ident ), "Add instrument", buf );

    updateData();

    camp_pathInit( currViewPath );
    camp_pathInit( currVarPath );
    camp_pathDown( currVarPath, ident );

    pVar = camp_varGetp( currVarPath );
    if( ( pVar != NULL ) && 
        ( ( pVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
          !( pVar->spec.CAMP_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE ) ) ) 
    {
        menuInsIf();
        updateData();

        menuInsLine();
        updateData();
    }

    updateDisplay();
}


int
menuInsChoose( void )
{
    CAMP_VAR* pVar;
    pVar = camp_varGetTrueP( currVarPath );

    camp_pathDown( currViewPath, pVar->core.ident );

    pVar = pVar->pChild;
    strcpy( currVarPath, currViewPath );
    if( pVar != NULL )
    {
	camp_pathDown( currVarPath, pVar->core.ident );
    }
}

/*
 * menuInsRecreate -- Add a new instrument and restore its config from
 *                   from the saved ini file.
 */
int 
menuInsRecreate( void )
{
    int status;
    char typeIdent[LEN_IDENT+1];
    char ident[LEN_IDENT+1];
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];
    char filename[LEN_FILENAME+1];
    char* description;
    char ident_descrip[LEN_IDENT+1];

    _doRPC( campSrv_sysDir( INI_MASK ), "Dir init files", "" );

    if( !inputSetupFile( "Recreate old instrument",
                         NULL, ident, INI_SFMT_BASE ) )
    {
        return( CAMP_SUCCESS );
    }

    /* 
     * Attempt to determine the type of the instrument from the header-comment
     * in the .ini file.
     */
    /*  INI_PFMP = /campnode/cfg/camp_ins_%s.ini */
    typeIdent[0] = '\0';
    /*     sprintf( filename, INI_PFMP, ident );  */
    sprintf( filename, "/campnode/cfg/camp_ins_%s.ini", ident );

    sprintf( buf, "set ifd [open {%s}]; gets $ifd s; close $ifd; set s", filename );
    status = campSrv_cmd( buf );
    description = camp_getMsg();
    if( _success( status ) )
    {
        sscanf( description, "# instrument %[^,], of type %s", ident_descrip, typeIdent );
        if( !streq( ident_descrip, ident ) )
        {
            typeIdent[0] = '\0';
        }
    }

    /*
     * If no type was automatically determined, ask user.
     */
    if( typeIdent[0] == '\0' )
    {
        if( !inputInsType( typeIdent ) ) 
        {
            return( CAMP_SUCCESS );
        }
    }

    sprintf( buf, "Adding instrument \"%s\"...", ident );

    _doRPC( campSrv_insAdd( typeIdent, ident ), "Add instrument", buf );

    updateData();

    camp_pathInit( currViewPath );
    camp_pathInit( currVarPath );
    camp_pathDown( currVarPath, ident );

    pVar = camp_varGetp( currVarPath );
    if( pVar != NULL )
    {

      sprintf( filename, INI_PFMT , ident );
      sprintf( buf, "Loading instrument \"%s\" ini file...", 
                        currVarPath );

      _doRPC( campSrv_insLoad( currVarPath, filename, 0 ), "Load initialization", buf );

      updateData();

    }

    updateDisplay();
}


int 
menuInsDel( void )
{
    int status;
    char* confirmLabels[] = { "No", "Yes" };
    int selection;
    char buf[LEN_BUF+1];
    char path[LEN_PATH+1];

    if( !inputVar( path, "Instrument", 
                        CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
    {
        return( CAMP_SUCCESS );
    }

    if( !inputSelectWin( "Confirm delete", 2, confirmLabels, 0, &selection ) )
    {
        return( CAMP_SUCCESS );
    }
    if( selection == 0 ) return( CAMP_SUCCESS );

    sprintf( buf, "Deleting instrument \"%s\"...", path );

    _doRPC( campSrv_insDel( path ), "Delete instrument", buf );

    updateData();
    updateDisplay();
}


int 
menuInsLock( void )
{
    int status;
    bool_t flag;
    char buf[LEN_BUF+1];

    if( !inputInsLock( currVarPath, &flag ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        sprintf( buf, "Locking instrument \"%s\"...", currVarPath );
    }
    else
    {
        sprintf( buf, "Unlocking instrument \"%s\"...", currVarPath );
    }

    _doRPC( campSrv_insLock( currVarPath, flag ), "Lock instrument", buf );
}

int 
menuInsLine( void )
{
    int status;
    bool_t flag;
    char buf[LEN_BUF+1];

    if( !inputInsLine( currVarPath, &flag ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        sprintf( buf, "Setting instrument \"%s\" online...", currVarPath );
    }
    else
    {
        sprintf( buf, "Setting instrument \"%s\" offline...", currVarPath );
    }

    _doRPC( campSrv_insLine( currVarPath, flag ), "Instrument connection", buf );
}


int 
menuInsBecomeIni( void )
{
    insLoad( 1, "Load and update ini file" );
}


int
menuInsLoadIni( void )
{
    insLoad( 0, "Load inst ini file" );
}


int
insLoad( int flag, char* title )
{
    int status;
    CAMP_VAR* pVar;
    char filename[LEN_FILENAME+1];
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];

    _doRPC( campSrv_sysDir( INI_MASK ), "Dir init files", "" );

    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    /* 
     *  get the filename
     */
    if( !inputSetupFile( title, 
                         pVar->spec.CAMP_SPEC_u.pIns->iniFile, 
                         str, INI_SFMT_BASE ) )
    {
        return( CAMP_SUCCESS );
    }

    sprintf( filename, INI_PFMT, str );

    if( flag )
    {
        sprintf( buf, "Load and write instrument \"%s\" ini file \"%s\"...", 
                        currVarPath, str );
    }
    else
    {
        sprintf( buf, "Load instrument \"%s\" ini file \"%s\"...", 
                        currVarPath, str );
    }

    _doRPC( campSrv_insLoad( currVarPath, filename, flag ), title, buf );
}


int 
menuInsSave( void )
{
    insSave( 0, "Save inst ini file" );
}


int
menuInsRedirect( void )
{
    insSave( 1, "Write and update ini file" );
}

int 
insSave( int flag, char* title )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char filename[LEN_FILENAME+1];
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];

    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    str[0] = '\0';
    if( pIns->iniFile != NULL )
    {
        /* basename --> basename_tw for linux */
        basename_tw( pIns->iniFile, filename );
        sscanf( filename, INI_SFMT_BASE, str );
    }

    if( !inputString( title, 32, str, str, isIdentChar ) ) 
        return( CAMP_SUCCESS );

    /* If user enters null, then revert to the instrument name */
    if( str[0] == '\0' )
        strcpy( str, pVar->core.ident );

    sprintf( filename, INI_PFMT, str );

    sprintf( buf, "Saving instrument \"%s\" ini file \"%s\"...", 
                    currVarPath, str );

    _doRPC( campSrv_insSave( currVarPath, filename, flag ), "Save file", buf );
}


int
menuInsIf( void )
{
    int status;
    CAMP_VAR* pVar;
    char typeIdent[LEN_IDENT+1];
    double delay;
    char defn[LEN_IF_DEFN+1];
    char buf[LEN_BUF+1];

    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    *defn = '\0';

    if( !inputIf( pVar, typeIdent, &delay, defn ) ) return( CAMP_SUCCESS );

    sprintf( buf, 
             "Setting instrument \"%s\"\nto interface type %s ( %f %s)...", 
             currVarPath, typeIdent, delay, defn );

    _doRPC( campSrv_insIf( currVarPath, typeIdent, (float)delay, defn ),
            "Instrument interface", buf );
}

int
menuVarShow( void )
{
    displayVarLoop();
}


int 
menuVarSet( void )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    double dVal;
    int iVal;
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     *  ARRAYs not implemented
     */
    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !( pVar->core.status & CAMP_VAR_ATTR_SET  ) ) 
      return( CAMP_INVAL_VAR );

    /*
     *  Check that instrument is online
     */
    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) return( CAMP_INVAL_VAR );

    if( ( pInsVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
       !( pInsVar->spec.CAMP_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
      {
	sprintf( buf, "Instrument \"%s\" is offline, can't set variable", 
		pInsVar->core.path );
	startMessage( "Set variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();
	return( CAMP_FAILURE );
      }
  
    /* 
     *  data is SETable, get the new setting
     */
    switch( pVar->core.varType )
    {
      case CAMP_VAR_TYPE_INT:
        if( !inputVarInteger( pVar, &dVal ) ) return( CAMP_SUCCESS );

        sprintf( buf, "Setting variable \"%s\" to \"%d\"...", 
                        currVarPath, (int)dVal );

        _doRPC( campSrv_varNumSetVal( currVarPath, dVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_FLOAT:
        if( !inputVarFloat( pVar, &dVal ) ) return( CAMP_SUCCESS );

        sprintf( buf, "Setting variable \"%s\" to \"%f\"...", 
                        currVarPath, dVal );

        _doRPC( campSrv_varNumSetVal( currVarPath, dVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_SELECTION:
        if( !inputVarSelection( pVar, &iVal ) ) return( CAMP_SUCCESS );

        varSelGetIDLabel( pVar, iVal, str );
        sprintf( buf, "Setting variable \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varSelSetVal( currVarPath, iVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_STRING:
        if( !inputVarString( pVar, str ) ) return( CAMP_SUCCESS );

        sprintf( buf, "Setting variable \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varStrSetVal( currVarPath, str ), "Set variable", buf);
        break;

      case CAMP_VAR_TYPE_ARRAY:
        /*
         *  Only done in server
         */
        break;

      default:
        return( CAMP_SUCCESS );
    }
}


int 
menuLnkVarSet( void )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char str[LEN_STRING+1];
    char buf[LEN_BUF+1];

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the source of
     *  the link, not the destination
     */
    pVar = camp_varGetTrueP( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !( pVar->core.status & CAMP_VAR_ATTR_SET  ) ) 
      return( CAMP_INVAL_VAR );

    /*
     *  Check that instrument is online
     */
    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) return( CAMP_INVAL_VAR );

    if( ( pInsVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
       !( pInsVar->spec.CAMP_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
      {
	sprintf( buf, "Instrument \"%s\" is offline, can't set variable", 
		pInsVar->core.path );
	startMessage( "Set variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();
	return( CAMP_FAILURE );
      }
  
    switch( pVar->core.varType )
    {
      case CAMP_VAR_TYPE_LINK:
        if( !inputString( "Full path", LEN_STRING,
            pVar->spec.CAMP_SPEC_u.pLnk->path, str, isPathChar ) ) 
        {
            return( CAMP_SUCCESS );
        }

        sprintf( buf, "Setting link \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varLnkSetVal( currVarPath, str ), "Set link", buf );
        break;

      default:
        return( CAMP_SUCCESS );
    }
}


int 
menuVarRead( void )
{
    int status;
    char buf[LEN_BUF+1];
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;

    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );
    
    if( !( pVar->core.status & CAMP_VAR_ATTR_READ  ) ) 
      return( CAMP_INVAL_VAR );

    /*
     *  Check that instrument is online
     */
    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) return( CAMP_INVAL_VAR );

    if( ( pInsVar->spec.CAMP_SPEC_u.pIns->pIF == NULL ) ||
       !( pInsVar->spec.CAMP_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
      {
	sprintf( buf, "Instrument \"%s\" is offline, can't read variable", 
		pInsVar->core.path );
	startMessage( "Read variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();
	return( CAMP_FAILURE );
      }
  
    sprintf( buf, "Reading variable \"%s\"...", currVarPath );
    
    _doRPC( campSrv_varRead( currVarPath ), "Read variable", buf );
}


int 
menuVarPoll( void )
{
    int status;
    bool_t flag;
    double interval;
    char buf[LEN_BUF+1];

    if( !inputVarPoll( currVarPath, &flag, &interval ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        sprintf( buf, "Setting variable \"%s\" polling to %.1f seconds...", 
                      currVarPath, interval );
    }
    else
    {
        sprintf( buf, "Setting variable \"%s\" polling off...", currVarPath );
    }

    _doRPC( campSrv_varPoll( currVarPath, flag, (float)interval ), "Poll variable", buf );
}


int 
menuVarAlarm( void )
{
    int status;
    bool_t flag;
    char ident[LEN_IDENT+1];
    char buf[LEN_BUF+1];
    
    if( !inputVarAlarm( currVarPath, &flag, ident ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        sprintf( buf, "Setting variable \"%s\" alarm to \"%s\"...", 
                      currVarPath, ident );
    }
    else
    {
        sprintf( buf, "Setting variable \"%s\" alarm off...", currVarPath );
    }

    _doRPC( campSrv_varAlarm( currVarPath, flag, ident ), "Alarm variable", buf );
}


int 
menuVarLog( void )
{
    int status;
    bool_t flag;
    char ident[LEN_IDENT+1];
    char buf[LEN_BUF+1];
    
    if( !inputVarLog( currVarPath, &flag, ident ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        sprintf( buf, "Setting variable \"%s\" logging to \"%s\"...", 
                      currVarPath, ident );
    }
    else
    {
        sprintf( buf, "Setting variable \"%s\" logging off...", currVarPath );
    }

    _doRPC( campSrv_varLog( currVarPath, flag, ident ), "Log variable", buf );
}


int 
menuVarZero( void )
{
    int status;
    char buf[LEN_BUF+1];

    sprintf( buf, "Zeroing variable \"%s\" statistics...", currVarPath );

    _doRPC( campSrv_varZero( currVarPath ), "Zero variable", buf );
}


int 
menuVarTol( void )
{
    int status;
    u_long tolType;
    double tol;
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];


#define NONE_TEXT "<undefined>"
    
    /*
     *  Only NUMERIC variables with attribute ALARM
     *  (SELECTION variables not yet implemented)
     */

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     */
    pVar = camp_varGetp( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !inputVarTol( pVar, &tolType, &tol ) )
        return( CAMP_SUCCESS );

    switch( tolType )
    {
      case 0:
        if( tol >= 0.0 ) sprintf( str, "+-%f", tol );
        else strcpy( str, NONE_TEXT );
        break;

      case 1:
        if( tol >= 0.0 ) sprintf( str, "%.0f%%", tol );
        else strcpy( str, NONE_TEXT );
        break;

      default:
        strcpy( str, NONE_TEXT );
        break;
    }

    sprintf( buf, "Setting variable \"%s\" tolerance to %s...", 
                    currVarPath, str );

    _doRPC( campSrv_varNumSetTol( currVarPath, tolType, (float)tol ), "Variable tolerance", buf );
}


int
cbMainMenuHelpAbout( void )
{
    int y;
    extern WINDOW* helpWin;
    extern char* prog_name;

    createHelpWin();

    y = 0;
    mvwaddstr( helpWin, y++, 0, 
"About the CAMP Character-cell User Interface" );
    y++;
    wmove( helpWin, y++, 0 );
    wprintw( helpWin,
"  Version:          %s", prog_name );
    wmove( helpWin, y++, 0 );
    wprintw( helpWin,
"  Server version:   %s", pSys->prgName );
    y++;
    mvwaddstr( helpWin, y++, 0, 
"  To get help:" );
    mvwaddstr( helpWin, y++, 0, 
"    from CAMP CUI:  choose 'On CAMP CUI' from the 'Help' menu" );
#ifdef VMS
    mvwaddstr( helpWin, y++, 0, 
"    from DCL:       $ help @camp user_manual cui_manual" );
#endif
    mvwaddstr( helpWin, y++, 0, 
"    printed:        see contact" );
    y++;
    mvwaddstr( helpWin, y++, 0, 
"  Contact:          Suzannah Daviel (suz@triumf.ca)" );

#define  QUIT_TEXT  "Press any key to continue"
    wattron( helpWin, A_REVERSE );
    wmove( helpWin, HELPWIN_H-1, (HELPWIN_W-strlen(QUIT_TEXT))/2 );
    wprintw( helpWin, QUIT_TEXT );
    wattroff( helpWin, A_REVERSE );

    wrefresh( helpWin );

    getKey();

    deleteHelpWin();
}


int
cbMainMenuHelpManual( void )
{
#ifdef VMS
#include hlpdef
    s_dsc line_desc;
    s_dsc library_name;
    long output_width;
    long flags;

    createHelpWin();

    setdsctostr( &line_desc, "user_manual cui_manual" );
    setdsctostr( &library_name, "camp:[lib]camp.hlb" );
    output_width = HELPWIN_W;
    flags = HLP$M_PROMPT | HLP$M_HELP;
    lbr$output_help( helpPutOutput, &output_width, 
                     &line_desc, &library_name,
                     &flags, helpGetInput );

    deleteHelpWin();

#else /* Non-VMS, but still use VMS help files. */

    char library_name[80] = { "" };
    char subject[64] = { "User_Manual" };
    char* library_place;

    library_place = getenv( "CAMP_HELP" );
    if ( library_place != NULL )
    {
      strncpy( library_name, library_place, 70 );
      if( library_name[strlen(library_name)-1] != '/' )
      {
        strcat( library_name, "/" );
      }
    }
    strcat( library_name, "camp.hlp" );

    createHelpWin();

    vmshelp_reader ( subject, library_name );

    deleteHelpWin();

#endif /* VMS */
}


int
cbMainMenuBell( void )
{
  noisy = !noisy;
}

int
cbMainMenuHelpCamp( void )
{
#ifdef VMS

#include hlpdef
    s_dsc line_desc;
    s_dsc library_name;
    long output_width;
    long flags;

    createHelpWin();

    setdsctostr( &line_desc, "" );
    setdsctostr( &library_name, "camp:[lib]camp.hlb" );
    output_width = HELPWIN_W;
    flags = HLP$M_PROMPT | HLP$M_HELP;
    lbr$output_help( helpPutOutput, &output_width, 
                     &line_desc, &library_name,
                     &flags, helpGetInput );

    deleteHelpWin();

#else 

    char library_name[80] = { "" };
    char subject[64] = { "" };
    char* library_place;

    library_place = getenv( "CAMP_HELP" );
    if ( library_place != NULL )
    {
      strncpy( library_name, library_place, 70 );
      if( library_name[strlen(library_name)-1] != '/' )
      {
        strcat( library_name, "/" );
      }
    }
    strcat( library_name, "camp.hlp" );

    createHelpWin();

    vmshelp_reader ( subject, library_name );

    deleteHelpWin();

#endif /* VMS */
}


int
doMainMenu( void )
{
    int n;
    int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "Configure";
    callback[n] = cbMainMenuConfig;
    n++;

    choices[n] = "Toggle beeps";
    callback[n] = cbMainMenuBell;
    n++;
/*
    choices[n] = "Options";
    callback[n] = cbMainMenuOpt;
    n++;
*/
    choices[n] = "Help";
    callback[n] = cbMainMenuHelp;
    n++;
    choices[n] = "Exit CAMP";
    callback[n] = cbMainMenuQuit;
    n++;

    for( ;; )
    {
        if( !inputSelectWin( "Main menu", n, choices, 0, &select ) )
        {
            break;
        }

        (*callback[select])();
    }
}


int
cbMainMenuConfig( void )
{
    int n;
    int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;
    choices[n] = "Add instrument";
    callback[n] = menuInsAdd;
    n++;
    choices[n] = "Delete instrument"; 
    callback[n] = menuInsDel;
    n++;
    choices[n] = "Recreate instrument"; 
    callback[n] = menuInsRecreate;
    n++;
    choices[n] = "Open config file";
    callback[n] = menuFileLoadNew;
    n++;
    choices[n] = "Import (add) config file";
    callback[n] = menuFileLoad;
    n++;
    choices[n] = "Save config";
    callback[n] = menuFileSave;
    n++;

    if( !inputSelectWin( "Configure", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();
}

/*
int
cbMainMenuOpt( void )
{
    int n;
    int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "Set rate";
    callback[n] = menuViewRate;
    n++;

    if( !inputSelectWin( "Options", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();
}
*/

int
cbMainMenuHelp( void )
{
    int n;
    int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "About CAMP CUI";
    callback[n] = cbMainMenuHelpAbout;
    n++;
    choices[n] = "On CAMP CUI";
    callback[n] = cbMainMenuHelpManual;
    n++;
    choices[n] = "On CAMP";
    callback[n] = cbMainMenuHelpCamp;
    n++;

    if( !inputSelectWin( "Help", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();
}


int
cbMainMenuQuit( void )
{
/*
    int n;
    int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "No";
    n++;
    choices[n] = "Yes";
    n++;

    if( !inputSelectWin( "Confirm exit", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    if( select == 0 ) return( CAMP_SUCCESS );
*/
    camp_cui_rundown();
    exit( 1 );
}
