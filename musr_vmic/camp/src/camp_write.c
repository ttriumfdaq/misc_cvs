/*
 *  Name:       camp_write.c
 *
 *  Purpose:    General utilities for CAMP file writing
 *
 *  Called by:  camp_*_write.c
 * 
 *  Preconditions:
 *              openOutput must be called before any file I/O.  This will
 *              set the "fout" global variable of which there is only one,
 *              meaning that there can only be one open file at a time.
 *
 *              add_tab and del_tab change the tabbing context that is
 *              maintained in the global variable "tabs".  These are used
 *              to somewhat prettify the output files.
 *
 *  Postconditions:
 *              closeOutput must be called when a file write procedure
 *              is finished.
 *
 *  Revision history:
 *   v1.2  16-May-1994  [T. Whidden] Taken from the old camp_parse.c
 *
 */

#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include "camp_srv.h"

static FILE *fout;                        /* file pointer of current input */
static char tabs[20];


bool_t
openOutput( char* filename )
{
#ifdef VMS
    fout = fopen( filename, "r+" );
    if( fout == NULL )
    {
#endif /* VMS */
        fout = fopen( filename, "w" );
        if( fout == NULL )
        {
            camp_appendMsg( "Unable to open file \"%s\" for output", filename );
            return( FALSE );
        }
#ifdef VMS
    }
#endif /* VMS */
    return( TRUE );
}


bool_t
closeOutput( void )
{
    if( fclose( fout ) == 0 )
    {
        fout = NULL;
        return( TRUE );
    }
    else
    {
        return( FALSE );
    }
}


void
add_tab( void )
{
    strcat( tabs, "\t" );
}


void
del_tab( void )
{
    tabs[ strlen( tabs ) - 1 ] = '\0';
}


void
print( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    vfprintf( fout, fmt, args );

    va_end( args );
}


void
print_tab( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    fputs( tabs, fout );
    vfprintf( fout, fmt, args );

    va_end( args );
}


/*
 * initialize the world
 */
void 
reinitialize( char* name, PFB parse_proc, char* line )
{
    tabs[0] = '\0';
}


int
camp_writeDumpFile( unsigned char* filename, unsigned int fnlen,
                    unsigned char* dump, unsigned int dump_len )
{
    int n;

    if( !openOutput( filename ) ) return( CAMP_INVAL_FILE );

    n = fwrite( dump, 1, dump_len, fout );

    closeOutput();

    if( n == dump_len )
      return( CAMP_SUCCESS );
    else
      return( CAMP_FAIL_DUMP );
}

