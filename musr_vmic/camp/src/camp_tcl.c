/*
 *  $Id$
 *
 *  $Revision$
 *
 *  Purpose:    Create and manage the CAMP Tcl interpreters
 *              Routines to interpret all CAMP Tcl commands
 *
 *  Called by:
 * 
 *  Inputs:
 *
 *  Preconditions:
 *
 *  Outputs:
 *
 *  Postconditions:
 *
 *  Notes:
 *              Tcl and VxWorks:
 *              The port of Tcl 7.3 that I'm using allows only one interpreter
 *              per task.  A single taskVar is used for the Tcl context (called
 *              Tcl_ctxt).  But, this seems to be only true when using the
 *              tclTCP extensions.  So, I removed all references to Tcl_ctxt
 *              in tclBasic.c and removed tclTCP.c and simpleEvent.c from the
 *              Makefile.
 *
 *  Revision history:
 *
 *  v1.0   ??-???-1994  TW  Initial version
 *  v1.0a  01-Nov-1994  TW  Added insIfReadVerify, insIfWriteVerify and 
 *                            proc-specific usage error messages
 *         06-Oct-1995  TW  No more rs232 retries
 *  V1.1   28-Apr-1998  DBM Added drivers for TIP850 and Tech80 Motor
 *                          Controller Industry Packs. Added some documentation
 *         20-Dec-1999  TW  Added gpibClear
 *  V1.1a  29-Sep-2000  DJA Fix bug where "insSet -if_mod" changed access delay 
 *                          to 0.0s
 *         13-Dec-2000  TW  Added gpibTimeout
 *         14-Dec-2000  TW/DJA  Added sysGetInsNames
 *         18-Dec-2000  TW  Make invalid tokens in varSet an error.  This was
 *                          too relaxed before.  Now properly returns error
 *                          on instrument loading, etc.
 *         22-Dec-2000  DM  Indpack interface type
 *         23-Dec-2000  TW  Prototypes for indpak; revise dirs of adcdac and motor
 *         10-Feb-2001  DJA Added insIfDump and insIfUndump
 *         19-Dec-2001  DJA Added sysGetLoggedVars, sysGetAlertVars, sysGetVarsOnPath
 *
 *  $Log$
 *  Revision 1.22  2009/04/08 01:58:54  asnd
 *  Change motor API to pass values instead of variable names
 *
 *  Revision 1.21  2008/03/27 01:29:40  asnd
 *  Remove old unused omegaLib.
 *
 *  Revision 1.20  2007/09/15 01:48:40  asnd
 *  Make sysGetLoggedVars match logging methods
 *
 *  Revision 1.19  2007/02/16 05:45:49  asnd
 *  Make previous change use floats for int variables (sums are big)
 *
 *  Revision 1.18  2007/02/16 05:12:28  asnd
 *  Avoid bad round-off when retrieving statistics
 *
 *  Revision 1.17  2006/05/06 06:18:36  asnd
 *  Allow values to be specified before selections on CAMP_SELECT (apply value setting last)
 *
 *  Revision 1.16  2006/05/02 07:46:33  asnd
 *  Enable alarms on selection variables
 *
 *  Revision 1.15  2006/04/28 07:49:13  asnd
 *  Watch for invalid selection-variable indices
 *
 *  Revision 1.14  2006/04/27 04:10:03  asnd
 *  Create a tcpip socket interface type.
 *  Generate more and better error messages.
 *
 *  Revision 1.13  2005/11/16 23:23:59  asnd
 *  Commit updates for new motor boards
 *
 *  Revision 1.12  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 *  Revision 1.11  2003/11/10 22:41:25  asnd
 *  Get interface type "none" working
 *
 *  Revision 1.10  2003/07/25 10:16:06  asnd
 *  Bug fixes on the initial minimal omega-controller low-level driver
 *
 *  Revision 1.9  2003/07/23 00:01:17  midas
 *  Added Omega instrument to camp_tcl.c
 *  Added Omega iSeries Ethernet Heater Controller driver files for CAMP
 *  Modified Makefile to build into vxWorks version of CAMP
 *
 *  Revision 1.8  2001/12/19 18:07:35  asnd
 *  Added sysGetLoggedVars, sysGetAlertVars, sysGetVarsOnPath
 *
 *  Revision 1.7  2001/02/10 04:16:15  asnd
 *  Add insIfDump and insIfUndump for binary-clean data dump from instrument to
 *  a file and from a file to the instrument.
 *
 *  Revision 1.6  2000/12/23 00:44:27  ted
 *  Prototypes for indpak
 *
 *  Revision 1.5  2000/12/23 00:30:46  ted
 *  No absolute directories for adcdac and motor
 *
 *  Revision 1.4  2000/12/22 23:36:20  David.Morris
 *  Added INDPAK support:
 *  In campTcl_CreateInterp added Tcl_CreateCommand def'ns.
 *  Added camp_tcl_varGetIfIndpak routine to process commands
 *  Added case for selecting the right processing command
 *  Added case for processing INDPAK parameters
 *  Added case for modifying channel parameter on the fly
 *
 *  Revision 1.3  2000/12/22 22:43:45  David.Morris
 *  Added function to make Tcl aware of the INDPAK interface in camp_tcl_sys
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "camp_srv.h"
#include "camacLib.h"

#ifdef VXWORKS
/*  TW  22-Dec-2000
#include "../../vxworks/adcdac/tip850Lib.h"
#include "../../vxworks/motor/te28Lib.h"
*/
#include "tip850Lib.h"
#include "te28Lib.h"
#endif
/* This next may apply to more than VXWORKS */
#include "camp_vme_mem.h"

/*
 *  Main Tcl interpreter
 *  For multithreaded, this is used for syUpdate, sysLoad, insAdd
 *  and RPC-sent Tcl commands and alarm action procs
 */
Tcl_Interp* tclInterp = NULL;

#define usage( str )        Tcl_SetResult( interp, str, TCL_VOLATILE );
#define if_failure_status_return( status )  \
  if( _failure( status ) ) { camp_tcl_statmsg( interp, status ); return( TCL_ERROR ); }

static char* itoa( int i );
static char* ltoa( long i );
static char* ftoa( double d );
static void  camp_tcl_statmsg( Tcl_Interp *interp, int stat );
int camp_tcl_insAvail ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_sys ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_ins ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varDef ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varSet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varRead ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varTest ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_lnkSet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varNumGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varArr ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varSelGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varInsGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfRs232 ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfGpib ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfCamac ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfVme ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfTcpip ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfIndpak ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_lnkGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_msg ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_sleep ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gettime ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_camac_cdreg ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_camac_single ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_camac_block ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_vme_io ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gpib_clear ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gpib_timeout ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_tip850(ClientData clientData, Tcl_Interp * interp, int argc, char * argv[]);
int camp_tcl_te28(ClientData clientData, Tcl_Interp * interp, int argc, char * argv[]);
int camp_tcl_alarmTdMusr( ClientData clientData, Tcl_Interp* interp, int argc, char* argv[] );
int camp_tcl_alarmIMusr( ClientData clientData, Tcl_Interp* interp, int argc, char* argv[] );

void campTcl_list_each( Tcl_Interp* interp, u_long attr, CAMP_VAR* pVar_start );


typedef enum
{
    SYS_UPDATE=1,
    SYS_RUNDOWN,
    SYS_REBOOT,
    SYS_LOAD,
    SYS_SAVE,
    SYS_DIR,
    SYS_GETDIR,
    SYS_GETINSTYPES,
    SYS_GETALARMACTS,
    SYS_GETLOGACTS,
    SYS_GETIFTYPES,
    SYS_GETIFCONF,
    SYS_ADDALARMACT,
    SYS_ADDLOGACT,
    SYS_ADDIFTYPE,
    SYS_ADDINSTYPE,
    SYS_ADDINSAVAIL,
    SYS_GETINSNAMES,
    SYS_GETVARSONPATH,
    SYS_GETLOGGEDVARS,
    SYS_GETALERTVARS,
} SYS_PROCS;

typedef enum 
{
    INS_ADD=1,
    INS_SET,
    INS_DEL,
    INS_LOAD,
    INS_SAVE,
    INS_IF_ON,
    INS_IF_OFF,
    INS_IF_READ,
    INS_IF_WRITE,
    INS_IF_READ_VERIFY,
    INS_IF_WRITE_VERIFY,
    INS_IF_DUMP,
    INS_IF_UNDUMP,
} INS_PROCS;

typedef enum 
{
    VAR_INT=1,
    VAR_FLOAT,
    VAR_STRING,
    VAR_SELECTION,
    VAR_ARRAY,
    VAR_STRUCTURE,
    VAR_INSTRUMENT,
    VAR_LINK,
    VAR_DEF,
    VAR_SET,
    VAR_DOSET,
    VAR_READ,
    LNK_SET,
} VAR_PROCS;

typedef enum 
{
    VAR_TESTTOL,
    VAR_TESTALERT,
} VAR_UTILPROCS;

typedef enum 
{
    VAR_IDENT=1,
    VAR_PATH,
    VAR_VARTYPE,
    VAR_ATTRIBUTES,
    VAR_TITLE,
    VAR_HELP,
    VAR_STATUS,
    VAR_STATUSMSG,
    VAR_TIMELASTSET,
    VAR_POLLINTERVAL,
    VAR_LOGINTERVAL,
    VAR_LOGACTION,
    VAR_ALARMACTION,
    VAR_VAL,
} VAR_GETPROCS;

typedef enum 
{
    VAR_TIMESTARTED=1,
    VAR_NUM,
    VAR_LOW,
    VAR_HI,
    VAR_SUM,
    VAR_SUMSQUARES,
    VAR_SUMCUBES,
    VAR_OFFSET,
    VAR_TOL,
    VAR_TOLTYPE,
    VAR_UNITS,
} VAR_NUMGETPROCS;

typedef enum 
{
    SEL_VALLABEL=1,
} VAR_SELGETPROCS;

typedef enum
{
    ARR_SETVAL=1,
    ARR_RESIZE,
    ARR_GETVAL,
    ARR_GETVARTYPE,
    ARR_GETELEMSIZE,
    ARR_GETDIM,
    ARR_GETDIMSIZE,
    ARR_GETTOTELEM,
} VAR_ARR_PROCS;

typedef enum 
{
    INS_LOCKHOST=1,
    INS_LOCKPID,
    INS_DEFFILE,
    INS_INIFILE,
    INS_DATAITEMS_LEN,
    INS_TYPEIDENT,
    INS_TYPEINSTANCE,
    INS_LEVELCLASS,
    INS_IF_STATUS,
    INS_IF_TYPEIDENT,
    INS_IF_DELAY,
    INS_IF,
} VAR_INSGETPROCS;

typedef enum 
{
    INS_CAMP_IF_RS232_NAME=1,
    INS_CAMP_IF_RS232_BAUD,
    INS_CAMP_IF_RS232_DATABITS,
    INS_CAMP_IF_RS232_PARITY,
    INS_CAMP_IF_RS232_STOPBITS,
    INS_CAMP_IF_RS232_READTERM,
    INS_CAMP_IF_RS232_WRITETERM,
    INS_CAMP_IF_RS232_READTIMEOUT,
    INS_CAMP_IF_GPIB_ADDR,
    INS_CAMP_IF_GPIB_READTERM,
    INS_CAMP_IF_GPIB_WRITETERM,
    INS_CAMP_IF_CAMAC_B,
    INS_CAMP_IF_CAMAC_C,
    INS_CAMP_IF_CAMAC_N,
    INS_CAMP_IF_INDPAK_SLOT,
    INS_CAMP_IF_INDPAK_TYPE,
    INS_CAMP_IF_INDPAK_CHANNEL,
    INS_CAMP_IF_VME_BASE,
    INS_CAMP_IF_TCPIP_ADDRESS,
    INS_CAMP_IF_TCPIP_PORT,
    INS_CAMP_IF_TCPIP_READTERM,
    INS_CAMP_IF_TCPIP_WRITETERM,
    INS_CAMP_IF_TCPIP_TIMEOUT
} VAR_GETIFPROCS;

typedef enum 
{
    LNK_VARTYPE=1,
    LNK_PATH,
} VAR_GETLNKPROCS;

typedef enum
{
    CAMAC_CDREG=1,
    CAMAC_CFSA,
    CAMAC_CSSA,
    CAMAC_CFUBC,
    CAMAC_CFUBR,
    CAMAC_CSUBC,
    CAMAC_CSUBR
} CAMAC_PROCS;

typedef enum
{
    VME_READ=1,
    VME_WRITE
} VME_PROCS;

/*
 *  GPIB procs - direct GPIB bus commands
 *  21-Dec-1999  TW  gpib_clear does a GPIB SDC (selected device clear)
 */
typedef enum
{
  GPIB_CLEAR=1,
  GPIB_TIMEOUT
} GPIB_PROCS;


/* DAC/ADC functions for tip850 Industry Pack channels */

typedef enum
{
    DAC_SET=1,
    DAC_READ,
    ADC_READ,
    GAIN_SET
} DACADC_PROCS;

/* Motor functions for te28 Industry Pack channels */

typedef enum
{
  MOTOR_RESET = 1,
  MOTOR_VEL,
  MOTOR_ACC,
  MOTOR_FILTER_KP,
  MOTOR_FILTER_KI,
  MOTOR_FILTER_KD,
  MOTOR_FILTER_IL,
  MOTOR_FILTER_SI,
  MOTOR_STOP,
  MOTOR_ABORT,
  MOTOR_SLOPE,
  MOTOR_OFFSET,
  MOTOR_ENCODER,
  MOTOR_LIMIT,
  MOTOR_MOVE,
  MOTOR_POSITION
} MOTOR_PROCS;

/*
 *  itoa - integer to ASCII
 *
 *  Warning NON-REENTRANT, returns pointer to static string
 */
static char*
itoa( int i )
{
    static char s[32];

    sprintf( s, "%d", i );
    return( s );
}


/*
 *  ltoa - long integer to ASCII
 *
 *  Warning NON-REENTRANT, returns pointer to static string
 */
static char*
ltoa( long i )
{
    static char s[32];

    sprintf( s, "%d", i );
    return( s );
}


/*
 *  ftoa - floating point to ASCII
 *
 *  Warning NON-REENTRANT, returns pointer to static string
 */
static char*
ftoa( double d )
{
    static char s[32];

    sprintf( s, "%g", d );
    return( s );
}


/*
 *  Return pointer to main Tcl interp
 */
Tcl_Interp*
camp_tclInterp( void )
{
    return( tclInterp );
}


/*
 *  camp_tclInit()  -  create main Tcl interpreter
 */
int
camp_tclInit( void )
{
    tclInterp = campTcl_CreateInterp();
    if( tclInterp == NULL )
    {
        camp_appendMsg( "failed to create Tcl interpreter" );
        return( CAMP_FAILURE );
    }
    return( CAMP_SUCCESS );
}


/*
 *  camp_tclEnd
 *
 *  Delete the main Tcl interpreter
 */
void
camp_tclEnd( void )
{
    campTcl_DeleteInterp( tclInterp );
}


/*
 *  campTcl_CreateInterp()  -  create a Tcl interpreter with all
 *                             of the CAMP variables and built-in
 *                             procedures defined.
 *
 *  In multithreaded implementation this routine is called to create
 *  a Tcl interpreter for the main server thread, and each instance
 *  of a CAMP instrument.  This allows use of Tcl interpreters for
 *  instrument drivers without conflict between the context of interpreters
 *  for different instruments.  Since only one action (read/write) is
 *  allowed on an instrument at one time, one interpreter is sufficient
 *  per instrument.
 */
Tcl_Interp* 
campTcl_CreateInterp( void )
{
    char hostname[LEN_NODENAME+1];
    char* s;
    Tcl_Interp* interp;

    interp = Tcl_CreateInterp();
    if( interp == NULL )
    {
        camp_appendMsg( "failed to create Tcl interpreter" );
        return( NULL );
    }

    /*
     *  Hostname variables
     */
    gethostname( hostname, LEN_NODENAME );
    stolower( hostname );
    Tcl_SetVar( interp, "camp_hostname", hostname, TCL_GLOBAL_ONLY );
    if( ( s = strchr( hostname, '.' ) ) != NULL ) *s = '\0';
    Tcl_SetVar( interp, "camp_host", hostname, TCL_GLOBAL_ONLY );

    /*
     *  Set the precision to something more than the 
     *  default of 6
     */
    Tcl_SetVar( interp, "tcl_precision", "12", TCL_GLOBAL_ONLY );

    /*
     *  Var attribute constants
     */
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_SHOW", 
                itoa( CAMP_VAR_ATTR_SHOW ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_SET", 
                itoa( CAMP_VAR_ATTR_SET ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_READ", 
                itoa( CAMP_VAR_ATTR_READ ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_POLL", 
                itoa( CAMP_VAR_ATTR_POLL ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_ALARM", 
                itoa( CAMP_VAR_ATTR_ALARM ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_LOG", 
                itoa( CAMP_VAR_ATTR_LOG ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_ALERT", 
                itoa( CAMP_VAR_ATTR_ALERT ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_IS_SET", 
                itoa( CAMP_VAR_ATTR_IS_SET ), TCL_GLOBAL_ONLY );

    /*
     *  Instrument constants
     */
    Tcl_SetVar( interp, "CAMP_INS_ATTR_LOCKED", 
                itoa( CAMP_INS_ATTR_LOCKED ), TCL_GLOBAL_ONLY );

    /*
     *  Interface constants
     */
    Tcl_SetVar( interp, "CAMP_IF_ONLINE", 
                itoa( CAMP_IF_ONLINE ), TCL_GLOBAL_ONLY );

    Tcl_SetVar( interp, "CAMP_MAJOR_VAR_TYPE_MASK", 
                itoa( CAMP_MAJOR_VAR_TYPE_MASK ), TCL_GLOBAL_ONLY );

    /*
     *  Var type constants
     */
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_NUMERIC", 
                itoa( CAMP_VAR_TYPE_NUMERIC ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_INT", 
                itoa( CAMP_VAR_TYPE_INT ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_FLOAT", 
                itoa( CAMP_VAR_TYPE_FLOAT ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_SELECTION", 
                itoa( CAMP_VAR_TYPE_SELECTION ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_STRING", 
                itoa( CAMP_VAR_TYPE_STRING ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_STRUCTURE", 
                itoa( CAMP_VAR_TYPE_STRUCTURE ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_ARRAY", 
                itoa( CAMP_VAR_TYPE_ARRAY ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_INSTRUMENT", 
                itoa( CAMP_VAR_TYPE_INSTRUMENT ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_LINK", 
                itoa( CAMP_VAR_TYPE_LINK ), TCL_GLOBAL_ONLY );

    /*
     *  Interface type constants
     */
/*  Don't need these, interfaces are identified by
    the TypeIdent

    Tcl_SetVar( interp, "CAMP_IF_TYPE_NONE", 
                itoa( CAMP_IF_TYPE_NONE ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_RS232", 
                itoa( CAMP_IF_TYPE_RS232 ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_GPIB", 
                itoa( CAMP_IF_TYPE_GPIB ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_TICS_RPC", 
                itoa( CAMP_IF_TYPE_TICS ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_CAMAC", 
                itoa( CAMP_IF_TYPE_CAMAC ), TCL_GLOBAL_ONLY );
*/
    /*
     *  Alarm commands
     */
    Tcl_CreateCommand( interp, "alarmTdMusr", camp_tcl_alarmTdMusr,
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "alarmIMusr", camp_tcl_alarmIMusr,
                        (ClientData)0, NULL );

    /*
     *  sys commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_UPDATE ), camp_tcl_sys,
                        (ClientData)SYS_UPDATE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_RUNDOWN ), camp_tcl_sys,
                        (ClientData)SYS_RUNDOWN, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_REBOOT ), camp_tcl_sys,
                        (ClientData)SYS_REBOOT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_LOAD ), camp_tcl_sys,
                        (ClientData)SYS_LOAD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_SAVE ), camp_tcl_sys,
                        (ClientData)SYS_SAVE, NULL );
    Tcl_CreateCommand( interp, "sysDir", camp_tcl_sys,
                        (ClientData)SYS_DIR, NULL );
    Tcl_CreateCommand( interp, "sysGetDir", camp_tcl_sys,
                        (ClientData)SYS_GETDIR, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETINSTYPES ), camp_tcl_sys,
                        (ClientData)SYS_GETINSTYPES, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETALARMACTS ), camp_tcl_sys,
                        (ClientData)SYS_GETALARMACTS, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETLOGACTS ), camp_tcl_sys,
                        (ClientData)SYS_GETLOGACTS, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETIFTYPES ), camp_tcl_sys,
                        (ClientData)SYS_GETIFTYPES, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETIFCONF ), camp_tcl_sys,
                        (ClientData)SYS_GETIFCONF, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDALARMACT ), camp_tcl_sys,
                        (ClientData)SYS_ADDALARMACT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDLOGACT ), camp_tcl_sys,
                        (ClientData)SYS_ADDLOGACT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDIFTYPE ), camp_tcl_sys,
                        (ClientData)SYS_ADDIFTYPE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDINSTYPE ), camp_tcl_sys,
                        (ClientData)SYS_ADDINSTYPE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDINSAVAIL ), camp_tcl_sys,
                        (ClientData)SYS_ADDINSAVAIL, NULL );
    Tcl_CreateCommand( interp, "sysGetInsNames", camp_tcl_sys,
                        (ClientData)SYS_GETINSNAMES, NULL );
    Tcl_CreateCommand( interp, "sysGetVarsOnPath", camp_tcl_sys,
                        (ClientData)SYS_GETVARSONPATH, NULL );
    Tcl_CreateCommand( interp, "sysGetLoggedVars", camp_tcl_sys,
                        (ClientData)SYS_GETLOGGEDVARS, NULL );
    Tcl_CreateCommand( interp, "sysGetAlertVars", camp_tcl_sys,
                        (ClientData)SYS_GETALERTVARS, NULL );

    /*
     *  ins commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_INS_ADD ), camp_tcl_ins, 
                        (ClientData)INS_ADD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_SET ), camp_tcl_ins, 
                        (ClientData)INS_SET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_DEL ), camp_tcl_ins, 
                        (ClientData)INS_DEL, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_LOAD ), camp_tcl_ins, 
                        (ClientData)INS_LOAD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_SAVE ), camp_tcl_ins, 
                        (ClientData)INS_SAVE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_ON ), camp_tcl_ins, 
                        (ClientData)INS_IF_ON, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_OFF ), camp_tcl_ins, 
                        (ClientData)INS_IF_OFF, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_READ ), camp_tcl_ins, 
                        (ClientData)INS_IF_READ, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_WRITE ), camp_tcl_ins, 
                        (ClientData)INS_IF_WRITE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_READ_VERIFY ), 
                        camp_tcl_ins, 
                        (ClientData)INS_IF_READ_VERIFY, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_WRITE_VERIFY ), 
                        camp_tcl_ins, 
                        (ClientData)INS_IF_WRITE_VERIFY, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_DUMP ), camp_tcl_ins, 
                        (ClientData)INS_IF_DUMP, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_UNDUMP ), camp_tcl_ins, 
                        (ClientData)INS_IF_UNDUMP, NULL );

    /*
     *  varSet commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_INT ), camp_tcl_varDef, 
                        (ClientData)VAR_INT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_FLOAT ), camp_tcl_varDef, 
                        (ClientData)VAR_FLOAT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_STRING ), camp_tcl_varDef, 
                        (ClientData)VAR_STRING, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SELECTION ), camp_tcl_varDef, 
                        (ClientData)VAR_SELECTION, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_STRUCTURE ), camp_tcl_varDef, 
                        (ClientData)VAR_STRUCTURE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INSTRUMENT ), camp_tcl_varDef, 
                        (ClientData)VAR_INSTRUMENT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_LINK ), camp_tcl_varDef, 
                        (ClientData)VAR_LINK, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_SET ), camp_tcl_varSet, 
                        (ClientData)VAR_SET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_DOSET ), camp_tcl_varSet,
                        (ClientData)VAR_DOSET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_READ ), camp_tcl_varRead, 
                        (ClientData)VAR_READ, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_LNK_SET ), camp_tcl_lnkSet, 
                        (ClientData)LNK_SET, NULL );

    /*
     *  var utilities
     */
    Tcl_CreateCommand( interp, "varTestTol", camp_tcl_varTest,
                        (ClientData)VAR_TESTTOL, NULL );
    Tcl_CreateCommand( interp, "varTestAlert", camp_tcl_varTest,
                        (ClientData)VAR_TESTALERT, NULL );

    /*
     *  varGet commands
     */
    Tcl_CreateCommand( interp, "varGetIdent", camp_tcl_varGet, 
                        (ClientData)VAR_IDENT, NULL );
    Tcl_CreateCommand( interp, "varGetPath", camp_tcl_varGet, 
                        (ClientData)VAR_PATH, NULL );
    Tcl_CreateCommand( interp, "varGetVarType", camp_tcl_varGet, 
                        (ClientData)VAR_VARTYPE, NULL );
    Tcl_CreateCommand( interp, "varGetAttributes", camp_tcl_varGet, 
                        (ClientData)VAR_ATTRIBUTES, NULL );
    Tcl_CreateCommand( interp, "varGetTitle", camp_tcl_varGet, 
                        (ClientData)VAR_TITLE, NULL );
    Tcl_CreateCommand( interp, "varGetHelp", camp_tcl_varGet, 
                        (ClientData)VAR_HELP, NULL );
    Tcl_CreateCommand( interp, "varGetStatus", camp_tcl_varGet, 
                        (ClientData)VAR_STATUS, NULL );
    Tcl_CreateCommand( interp, "varGetStatusMsg", camp_tcl_varGet, 
                        (ClientData)VAR_STATUSMSG, NULL );
    Tcl_CreateCommand( interp, "varGetTimeLastSet", camp_tcl_varGet, 
                        (ClientData)VAR_TIMELASTSET, NULL );
    Tcl_CreateCommand( interp, "varGetPollInterval", camp_tcl_varGet, 
                        (ClientData)VAR_POLLINTERVAL, NULL );
    Tcl_CreateCommand( interp, "varGetLogInterval", camp_tcl_varGet, 
                        (ClientData)VAR_LOGINTERVAL, NULL );
    Tcl_CreateCommand( interp, "varGetLogAction", camp_tcl_varGet, 
                        (ClientData)VAR_LOGACTION, NULL );
    Tcl_CreateCommand( interp, "varGetAlarmAction", camp_tcl_varGet, 
                        (ClientData)VAR_ALARMACTION, NULL );
    Tcl_CreateCommand( interp, "varGetVal", camp_tcl_varGet, 
                        (ClientData)VAR_VAL, NULL );

    /*
     *  varNumGet commands
     */
    Tcl_CreateCommand( interp, "varNumGetTimeStarted", camp_tcl_varNumGet, 
                        (ClientData)VAR_TIMESTARTED, NULL );
    Tcl_CreateCommand( interp, "varNumGetNum", camp_tcl_varNumGet, 
                        (ClientData)VAR_NUM, NULL );
    Tcl_CreateCommand( interp, "varNumGetLow", camp_tcl_varNumGet, 
                        (ClientData)VAR_LOW, NULL );
    Tcl_CreateCommand( interp, "varNumGetHi", camp_tcl_varNumGet, 
                        (ClientData)VAR_HI, NULL );
    Tcl_CreateCommand( interp, "varNumGetSum", camp_tcl_varNumGet, 
                        (ClientData)VAR_SUM, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumSquares", camp_tcl_varNumGet, 
                        (ClientData)VAR_SUMSQUARES, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumCubes", camp_tcl_varNumGet, 
                        (ClientData)VAR_SUMCUBES, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumOffset", camp_tcl_varNumGet, 
                        (ClientData)VAR_OFFSET, NULL );
    Tcl_CreateCommand( interp, "varNumGetTol", camp_tcl_varNumGet, 
                        (ClientData)VAR_TOL, NULL );
    Tcl_CreateCommand( interp, "varNumGetTolType", camp_tcl_varNumGet, 
                        (ClientData)VAR_TOLTYPE, NULL );
    Tcl_CreateCommand( interp, "varNumGetUnits", camp_tcl_varNumGet, 
                        (ClientData)VAR_UNITS, NULL );

    /*
     *  varSelGet commands
     */
    Tcl_CreateCommand( interp, "varSelGetValLabel", camp_tcl_varSelGet, 
                        (ClientData)SEL_VALLABEL, NULL );

    /*
     *  varArr commands
     */
    Tcl_CreateCommand( interp, "varArrSetVal", camp_tcl_varArr,
                       (ClientData)ARR_SETVAL, NULL );
    Tcl_CreateCommand( interp, "varArrResize", camp_tcl_varArr,
                       (ClientData)ARR_RESIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetVal", camp_tcl_varArr,
                       (ClientData)ARR_GETVAL, NULL );
    Tcl_CreateCommand( interp, "varArrGetVarType", camp_tcl_varArr,
                       (ClientData)ARR_GETVARTYPE, NULL );
    Tcl_CreateCommand( interp, "varArrGetElemSize", camp_tcl_varArr,
                       (ClientData)ARR_GETELEMSIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetDim", camp_tcl_varArr,
                       (ClientData)ARR_GETDIM, NULL );
    Tcl_CreateCommand( interp, "varArrGetDimSize", camp_tcl_varArr,
                       (ClientData)ARR_GETDIMSIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetTotElem", camp_tcl_varArr,
                       (ClientData)ARR_GETTOTELEM, NULL );

    /*
     *  insGet commands
     */
    Tcl_CreateCommand( interp, "insGetLockHost", camp_tcl_varInsGet, 
                        (ClientData)INS_LOCKHOST, NULL );
    Tcl_CreateCommand( interp, "insGetLockPid", camp_tcl_varInsGet, 
                        (ClientData)INS_LOCKPID, NULL );
    Tcl_CreateCommand( interp, "insGetDefFile", camp_tcl_varInsGet, 
                        (ClientData)INS_DEFFILE, NULL );
    Tcl_CreateCommand( interp, "insGetIniFile", camp_tcl_varInsGet, 
                        (ClientData)INS_INIFILE, NULL );
    Tcl_CreateCommand( interp, "insGetDataItemsLen", camp_tcl_varInsGet, 
                        (ClientData)INS_DATAITEMS_LEN, NULL );
    Tcl_CreateCommand( interp, "insGetTypeIdent", camp_tcl_varInsGet, 
                        (ClientData)INS_TYPEIDENT, NULL );
    Tcl_CreateCommand( interp, "insGetInstance", camp_tcl_varInsGet, 
                        (ClientData)INS_TYPEINSTANCE, NULL );
    Tcl_CreateCommand( interp, "insGetClass", camp_tcl_varInsGet, 
                        (ClientData)INS_LEVELCLASS, NULL );
    Tcl_CreateCommand( interp, "insGetIfStatus", camp_tcl_varInsGet, 
                        (ClientData)INS_IF_STATUS, NULL );
    Tcl_CreateCommand( interp, "insGetIfTypeIdent", camp_tcl_varInsGet, 
                        (ClientData)INS_IF_TYPEIDENT, NULL );
    Tcl_CreateCommand( interp, "insGetIfDelay", camp_tcl_varInsGet, 
                        (ClientData)INS_IF_DELAY, NULL );
    Tcl_CreateCommand( interp, "insGetIf", camp_tcl_varInsGet,
                        (ClientData)INS_IF, NULL );

    /*
     *  varGetIfRs232 commands
     */
    Tcl_CreateCommand( interp, "insGetIfRs232Name", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_NAME, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Baud", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_BAUD, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Data", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_DATABITS, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Parity", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_PARITY, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Stop", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_STOPBITS, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232ReadTerm", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232WriteTerm", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_WRITETERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Timeout", 
                        camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_READTIMEOUT, NULL );

    /*
     *  varGetIfGpib commands
     */
    Tcl_CreateCommand( interp, "insGetIfGpibAddr", 
                        camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_ADDR, NULL );
    Tcl_CreateCommand( interp, "insGetIfGpibReadTerm", 
                        camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfGpibWriteTerm", 
                        camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_WRITETERM, NULL );

    /*
     *  varGetIfCamac commands
     */
    Tcl_CreateCommand( interp, "insGetIfCamacB", 
                        camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_B, NULL );
    Tcl_CreateCommand( interp, "insGetIfCamacC", 
                        camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_C, NULL );
    Tcl_CreateCommand( interp, "insGetIfCamacN", 
                        camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_N, NULL );

    /*
     *  varGetIfVme commands
     */
    Tcl_CreateCommand( interp, "insGetIfVmeBase", 
                        camp_tcl_varGetIfVme, 
                        (ClientData)INS_CAMP_IF_VME_BASE, NULL );

    /*
     *  varGetIfTcpip commands
     */
    Tcl_CreateCommand( interp, "insGetIfTcpipAddress", 
                        camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_ADDRESS, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipPort", 
                        camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_PORT, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipReadTerm", 
                        camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipWriteTerm", 
                        camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_WRITETERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipTimeout", 
                        camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_TIMEOUT, NULL );

    /*
     *  varGetIfIndpak commands
     */
    Tcl_CreateCommand( interp, "insGetIfIndpakSlot", 
                        camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_SLOT, NULL );
    Tcl_CreateCommand( interp, "insGetIfIndpakType", 
                        camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_TYPE, NULL );
    Tcl_CreateCommand( interp, "insGetIfIndpakChannel", 
                        camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_CHANNEL, NULL );

    /*
     *  varLnkGet commands
     */
    Tcl_CreateCommand( interp, "lnkGetVarType", camp_tcl_lnkGet, 
                        (ClientData)LNK_VARTYPE, NULL );
    Tcl_CreateCommand( interp, "lnkGetPath", camp_tcl_lnkGet, 
                        (ClientData)LNK_PATH, NULL );

    /*
     *  CAMAC commands
     */
    Tcl_CreateCommand( interp, "cdreg", camp_tcl_camac_cdreg, 
                        (ClientData)CAMAC_CDREG, NULL );
    Tcl_CreateCommand( interp, "cfsa", camp_tcl_camac_single, 
                        (ClientData)CAMAC_CFSA, NULL );
    Tcl_CreateCommand( interp, "cssa", camp_tcl_camac_single, 
                        (ClientData)CAMAC_CSSA, NULL );
    Tcl_CreateCommand( interp, "cfubc", camp_tcl_camac_block, 
                        (ClientData)CAMAC_CFUBC, NULL );
    Tcl_CreateCommand( interp, "cfubr", camp_tcl_camac_block, 
                        (ClientData)CAMAC_CFUBR, NULL );
    Tcl_CreateCommand( interp, "csubc", camp_tcl_camac_block, 
                        (ClientData)CAMAC_CSUBC, NULL );
    Tcl_CreateCommand( interp, "csubr", camp_tcl_camac_block, 
                        (ClientData)CAMAC_CSUBR, NULL );

    /*
     *  GPIB commands
     */
    Tcl_CreateCommand( interp, "gpibClear", camp_tcl_gpib_clear, 
                        (ClientData)GPIB_CLEAR, NULL );
    Tcl_CreateCommand( interp, "gpibTimeout", camp_tcl_gpib_timeout, 
                        (ClientData)GPIB_TIMEOUT, NULL );

    /*
     *  VME commands (May apply to more than VXWORKS)
     */
    Tcl_CreateCommand( interp, "vmeRead", camp_tcl_vme_io, 
                        (ClientData)VME_READ, NULL );
    Tcl_CreateCommand( interp, "vmeWrite", camp_tcl_vme_io, 
                        (ClientData)VME_WRITE, NULL );


#ifdef VXWORKS

    /* Tip850 DAC/ADC commands - used only in VxWorks image with
       Industry Packs on carrier board     */

    Tcl_CreateCommand(interp, "DacSet", camp_tcl_tip850, 
		      (ClientData) DAC_SET, NULL);
    Tcl_CreateCommand(interp, "DacRead", camp_tcl_tip850, 
		      (ClientData) DAC_READ, NULL);
    Tcl_CreateCommand(interp, "AdcRead", camp_tcl_tip850, 
		      (ClientData) ADC_READ, NULL);
    Tcl_CreateCommand(interp, "GainSet", camp_tcl_tip850, 
		      (ClientData) GAIN_SET, NULL);


    /*
     * Te28 Motor Controller commands
     */

    Tcl_CreateCommand(interp, "MotorReset", camp_tcl_te28, 
		      (ClientData) MOTOR_RESET, NULL);
    Tcl_CreateCommand(interp, "MotorVel", camp_tcl_te28, 
		      (ClientData) MOTOR_VEL, NULL);
    Tcl_CreateCommand(interp, "MotorAccel", camp_tcl_te28, 
		      (ClientData) MOTOR_ACC, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKP", camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KP, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKI", camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KI, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKD", camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KD, NULL);
    Tcl_CreateCommand(interp, "MotorFilterIL", camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_IL, NULL);
    Tcl_CreateCommand(interp, "MotorFilterSI", camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_SI, NULL);
    Tcl_CreateCommand(interp, "MotorStop", camp_tcl_te28, 
		      (ClientData) MOTOR_STOP, NULL);
    Tcl_CreateCommand(interp, "MotorAbort", camp_tcl_te28, 
		      (ClientData) MOTOR_ABORT, NULL);
    Tcl_CreateCommand(interp, "MotorSlope", camp_tcl_te28, 
		      (ClientData) MOTOR_SLOPE, NULL);
    Tcl_CreateCommand(interp, "MotorOffset", camp_tcl_te28, 
		      (ClientData) MOTOR_OFFSET, NULL);
    Tcl_CreateCommand(interp, "MotorEncoder", camp_tcl_te28, 
		      (ClientData) MOTOR_ENCODER, NULL);
    Tcl_CreateCommand(interp, "MotorLimit", camp_tcl_te28, 
		      (ClientData) MOTOR_LIMIT, NULL);
    Tcl_CreateCommand(interp, "MotorMove", camp_tcl_te28, 
		      (ClientData) MOTOR_MOVE, NULL);
    Tcl_CreateCommand(interp, "MotorPosition", camp_tcl_te28, 
		      (ClientData) MOTOR_POSITION, NULL);

#endif

    /*
     *  misc commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_MSG ), camp_tcl_msg, 
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "sleep", camp_tcl_sleep, 
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "gettime", camp_tcl_gettime, 
                        (ClientData)0, NULL );

    return( interp );
}


/*
 *  campTcl_DeleteInterp
 *
 *  Delete a CAMP Tcl interpreter
 */
void
campTcl_DeleteInterp( Tcl_Interp* interp )
{
    Tcl_DeleteInterp( interp );
}


/*
 *  Name:       campTcl_list_each
 *
 *  Purpose:    Generate a Tcl result listing (the path of) each variable
 *              currently holding (any of) the specified attribute(s)
 */

void
campTcl_list_each( Tcl_Interp* interp, u_long attr, CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        if( pVar->core.status & attr )
        {
            Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
        }            
        if( pVar->pChild != NULL ) 
        {
	    campTcl_list_each( interp, attr, pVar->pChild );
        }
    }
    return;
}

void
campTcl_each_log( Tcl_Interp* interp, CAMP_VAR* pVar_start, int num_act, char* actions[] )
{
    CAMP_VAR* pVar;
    int n;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        if( pVar->core.status & CAMP_VAR_ATTR_LOG )
        {
            if( num_act <= 0 ) /* List all logged vars, whatever log action */
            {
                Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
            }
            else /* Must match specified log action */
            { 
                for( n=0; n<num_act; n++ )
                {
                    if( ! strncmp( pVar->core.logAction, actions[n], 32 ) )
                    {
                        Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
                        break;
                    }
                }
            }
        }
        if( pVar->pChild != NULL ) 
        {
            campTcl_each_log( interp, pVar->pChild, num_act, actions );
        }
    }
    return;
}


/*
 *  Name:       camp_tcl_sys
 *
 *  Purpose:    Process "sys*" Tcl commands
 *
 *  Provides:
 *              sysUpdate
 *              sysShutdown
 *              sysReboot
 *              sysLoad
 *              sysSave
 *              sysDir
 *              sysGetDir
 *              sysGetInsTypes
 *              sysGetAlarmActs
 *              sysGetLogActs
 *              sysGetIfTypes
 *              sysGetIfConf
 *              sysAddAlarmAct
 *              sysAddLogAct
 *              sysAddIfType
 *              sysAddInsType
 *              sysAddInsAvail
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_sys( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    RES* pRes;
    FILE_req fileReq;
    INS_TYPE* pInsType;
    ALARM_ACT* pAlarmAct;
    LOG_ACT* pLogAct;
    CAMP_IF_t* pIF_t;
    char* endptr;
    ALARM_ACT** ppAlarmAct;
    LOG_ACT** ppLogAct;
    CAMP_IF_t** ppIF_t;
    INS_TYPE** ppInsType;
    INS_AVAIL** ppInsAvail;
    char message[256];
    FILE_req file_req;
    DIRENT* pDirEnt;
    DIR_res* pDirRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pHead;

    switch( (SYS_PROCS)clientData )
    {
      case SYS_UPDATE:
        pRes = campsrv_sysupdate_13( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysUpdate", TCL_STATIC );
            free( (void*)pRes );
            return( TCL_ERROR );
        }
        free( (void*)pRes );
        break;

      case SYS_RUNDOWN:
        pRes = campsrv_sysrundown_13( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysShutdown", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case SYS_REBOOT:
        pRes = campsrv_sysreboot_13( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysReboot", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case SYS_LOAD:
        sprintf( message, "usage: %s <filename> [<flag>]", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        fileReq.filename = argv[1];
        if( argc > 2 )
        {
            fileReq.flag = strtol( argv[2], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                usage( message );
                return( TCL_ERROR );
            }
        }
        else
        {
            fileReq.flag = 0;
        }

        pRes = campsrv_sysload_13( &fileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysLoad", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case SYS_SAVE:
        sprintf( message, "usage: %s <filename> [<flag>]", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        fileReq.filename = argv[1];
        if( argc > 2 )
        {
            fileReq.flag = strtol( argv[2], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                usage( message );
                return( TCL_ERROR );
            }
        }
        else
        {
            fileReq.flag = 0;
        }

        pRes = campsrv_syssave_13( &fileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysSave", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case SYS_DIR:
        sprintf( message, "usage: %s <filespec>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        fileReq.filename = argv[1];

        pDirRes = campsrv_sysdir_13( &fileReq, get_current_rqstp() );
        if( _failure( pDirRes->status ) )
        {
            Tcl_SetResult( interp, "failed sysDir", TCL_STATIC );
            _free( pDirRes );
            return( TCL_ERROR );
        }
        _free( pDirRes );

        /* FALL THROUGH */

      case SYS_GETDIR:
        Tcl_SetResult( interp, "", TCL_VOLATILE );
        for( pDirEnt = pSys->pDir; 
             pDirEnt != NULL; 
             pDirEnt = pDirEnt->pNext )
        {
            Tcl_AppendResult( interp, pDirEnt->filename, " ", (char*)NULL );
        }
        break;

      case SYS_GETINSTYPES:
        Tcl_SetResult( interp, "", TCL_VOLATILE );
        for( pInsType = pSys->pInsTypes; 
             pInsType != NULL; 
             pInsType = pInsType->pNext )
        {
            Tcl_AppendResult( interp, pInsType->ident, " ", (char*)NULL );
        }
        break;

      case SYS_GETALARMACTS:
        Tcl_SetResult( interp, "", TCL_VOLATILE );
        for( pAlarmAct = pSys->pAlarmActs; 
             pAlarmAct != NULL; 
             pAlarmAct = pAlarmAct->pNext )
        {
            Tcl_AppendResult( interp, pAlarmAct->ident , " ", (char*)NULL );
        }
        break;

      case SYS_GETLOGACTS:
        Tcl_SetResult( interp, "", TCL_VOLATILE );
        for( pLogAct = pSys->pLogActs; 
             pLogAct != NULL; 
             pLogAct = pLogAct->pNext )
        {
            Tcl_AppendResult( interp, pLogAct->ident , " ", (char*)NULL );
        }
        break;

      case SYS_GETIFTYPES:
        Tcl_SetResult( interp, "", TCL_VOLATILE );
        for( pIF_t = pSys->pIFTypes; 
             pIF_t != NULL; 
             pIF_t = pIF_t->pNext )
        {
            Tcl_AppendResult( interp, pIF_t->ident , " ", (char*)NULL );
        }
        break;

      case SYS_GETIFCONF:
        sprintf( message, "usage: %s <typeIdent>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

	pIF_t = camp_ifGetpIF_t( argv[1] );
        if( pIF_t == NULL )
        {
            Tcl_SetResult( interp, "invalid interface type", TCL_VOLATILE );
            return( TCL_ERROR );
        }

        Tcl_SetResult( interp, pIF_t->conf, TCL_VOLATILE );
        break;

      case SYS_GETINSNAMES:
        Tcl_SetResult( interp, "", TCL_VOLATILE );

	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
        {
            Tcl_AppendResult( interp, pVar->core.ident, " ", (char*)NULL );
        }

        break;

      case SYS_GETVARSONPATH:
        sprintf( message, "usage: %s <path>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        Tcl_SetResult( interp, "", TCL_VOLATILE );

        pVar = camp_varGetp( argv[1] );
        if( pVar == NULL )
        {
            sprintf( message, "invalid path: %s", argv[1] );
            Tcl_SetResult( interp, message, TCL_VOLATILE );
            return( TCL_ERROR );
        }

	for( pVar = pVar->pChild; pVar != NULL; pVar = pVar->pNext )
        {
            Tcl_AppendResult( interp, pVar->core.ident, " ", (char*)NULL );
        }

        break;

      case SYS_GETALERTVARS:
        Tcl_SetResult( interp, "", TCL_VOLATILE );

        campTcl_list_each( interp, CAMP_VAR_ATTR_ALERT, pVarList );

        break;

      case SYS_GETLOGGEDVARS:
        Tcl_SetResult( interp, "", TCL_VOLATILE );

        campTcl_each_log( interp, pVarList, argc-1, argv+1 );

        break;

      case SYS_ADDALARMACT:
        sprintf( message, "usage: %s <ident> <proc>", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( ppAlarmAct = &pSys->pAlarmActs; 
             *ppAlarmAct != NULL; 
             ppAlarmAct = &((*ppAlarmAct)->pNext) ) ;

        *ppAlarmAct = (ALARM_ACT*)zalloc( sizeof( ALARM_ACT ) );
        (*ppAlarmAct)->ident = strdup( argv[1] );
        (*ppAlarmAct)->proc = strdup( argv[2] );

        break;

      case SYS_ADDLOGACT:
        sprintf( message, "usage: %s <ident> <proc>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( ppLogAct = &pSys->pLogActs; 
             *ppLogAct != NULL; 
             ppLogAct = &((*ppLogAct)->pNext) ) ;

        *ppLogAct = (LOG_ACT*)zalloc( sizeof( LOG_ACT ) );
        (*ppLogAct)->ident = strdup( argv[1] );

	if( argc >= 3 ) 
	{
	    (*ppLogAct)->proc = strdup( argv[2] );
	}
	else
 	{
	    (*ppLogAct)->proc = strdup( "" );
	}

        break;

      case SYS_ADDIFTYPE:
        sprintf( message, "usage: %s <ident> <conf> <defaultDefn>", argv[0] );

        if( argc < 4 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( ppIF_t = &pSys->pIFTypes; 
             *ppIF_t != NULL; 
             ppIF_t = &((*ppIF_t)->pNext) ) ;

        *ppIF_t = (CAMP_IF_t*)zalloc( sizeof( CAMP_IF_t ) );
        (*ppIF_t)->ident = strdup( argv[1] );
	(*ppIF_t)->conf = strdup( argv[2] );
	(*ppIF_t)->defaultDefn = strdup( argv[3] );

	if( streq( argv[1], "none" ) )
	{
            (*ppIF_t)->typeID = CAMP_IF_TYPE_NONE;
            (*ppIF_t)->procs.onlineProc = if_none_online;
	    (*ppIF_t)->procs.offlineProc = if_none_offline;
	    (*ppIF_t)->procs.readProc = if_none_read;
            (*ppIF_t)->procs.writeProc = if_none_write;
            status = CAMP_SUCCESS;
	}
	else if( streq( argv[1], "rs232" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_RS232;
	    (*ppIF_t)->procs.onlineProc = if_rs232_online;
	    (*ppIF_t)->procs.offlineProc = if_rs232_offline;
	    (*ppIF_t)->procs.readProc = if_rs232_read;
	    (*ppIF_t)->procs.writeProc = if_rs232_write;
	    status = if_rs232_init();
	}
	else if( streq( argv[1], "gpib" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_GPIB;
	    (*ppIF_t)->procs.onlineProc = if_gpib_online;
	    (*ppIF_t)->procs.offlineProc = if_gpib_offline;
	    (*ppIF_t)->procs.readProc = if_gpib_read;
	    (*ppIF_t)->procs.writeProc = if_gpib_write;
	    status = if_gpib_init();
	}
	else if( streq( argv[1], "camac" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_CAMAC;
	    (*ppIF_t)->procs.onlineProc = if_camac_online;
	    (*ppIF_t)->procs.offlineProc = if_camac_offline;
	    (*ppIF_t)->procs.readProc = if_camac_read;
	    (*ppIF_t)->procs.writeProc = if_camac_write;
	    status = if_camac_init();
	}
	else if( streq( argv[1], "vme" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_VME;
	    (*ppIF_t)->procs.onlineProc = if_vme_online;
	    (*ppIF_t)->procs.offlineProc = if_vme_offline;
	    (*ppIF_t)->procs.readProc = if_vme_read;
	    (*ppIF_t)->procs.writeProc = if_vme_write;
	    status = if_vme_init();
	}
	else if( streq( argv[1], "tcpip" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_TCPIP;
	    (*ppIF_t)->procs.onlineProc = if_tcpip_online;
	    (*ppIF_t)->procs.offlineProc = if_tcpip_offline;
	    (*ppIF_t)->procs.readProc = if_tcpip_read;
	    (*ppIF_t)->procs.writeProc = if_tcpip_write;
	    status = if_tcpip_init();
	}
	else if( streq( argv[1], "tics" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_TICS;
	    status = CAMP_FAILURE;
	}
	else if( streq( argv[1], "indpak" ) )
	{
	    (*ppIF_t)->typeID = CAMP_IF_TYPE_INDPAK;
	    (*ppIF_t)->procs.onlineProc = if_indpak_online;
	    (*ppIF_t)->procs.offlineProc = if_indpak_offline;
	    (*ppIF_t)->procs.readProc = if_indpak_read;
	    (*ppIF_t)->procs.writeProc = if_indpak_write;
	    status = if_indpak_init();
	}
	else
	{
	    status = CAMP_FAILURE;
	}

	if( _failure( status ) )
	{
	    _xdr_free( xdr_CAMP_IF_t, *ppIF_t );
            Tcl_AppendResult( interp, "failed to add iftype ", argv[1], (char*)NULL );
	    return( TCL_ERROR );
	}

        break;

      case SYS_ADDINSTYPE:
        sprintf( message, "usage: %s <ident>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( ppInsType = &pSys->pInsTypes; 
             *ppInsType != NULL; 
             ppInsType = &((*ppInsType)->pNext) ) ;

	/*
	 *  Only Tcl drivers
	 */
        *ppInsType = (INS_TYPE*)zalloc( sizeof( INS_TYPE ) );
        (*ppInsType)->ident = strdup( argv[1] );
        /*
         *  Important to set driverType - checked in campsrv_insadd
         */
        (*ppInsType)->driverType = strdup( "Tcl" );
        (*ppInsType)->procs.initProc = ins_tcl_init;
        (*ppInsType)->procs.deleteProc = ins_tcl_delete;
        (*ppInsType)->procs.onlineProc = ins_tcl_online;
        (*ppInsType)->procs.offlineProc = ins_tcl_offline;
        (*ppInsType)->procs.setProc = ins_tcl_set;
        (*ppInsType)->procs.readProc = ins_tcl_read;

        break;

      case SYS_ADDINSAVAIL:
        sprintf( message, "usage: %s <type> <ident>", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( ppInsAvail = &pSys->pInsAvail; 
             *ppInsAvail != NULL;
             ppInsAvail = &(*ppInsAvail)->pNext )
        {
            if( streq( (*ppInsAvail)->ident, argv[2] ) )
            {
                Tcl_SetResult( interp, "", TCL_VOLATILE );
                Tcl_AppendResult( interp, "instrument \"", argv[2],
                                  "\" is already available", (char*)NULL );
            }
        }

        *ppInsAvail = (INS_AVAIL*)zalloc( sizeof( INS_AVAIL ) );
        (*ppInsAvail)->typeIdent = strdup( argv[1] );
        (*ppInsAvail)->ident = strdup( argv[2] );

        if( argc > 3 )
        {
            (*ppInsAvail)->specInitProc = strndup( argv[3], LEN_PROC );
        }
        else
        {
            (*ppInsAvail)->specInitProc = strdup( "" );
        }

        break;

    }
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_ins
 *
 *  Purpose:    Process "ins*" Tcl commands
 *
 *  Provides:   
 *              insAdd
 *              insDel
 *              insSet
 *              insLoad
 *              insSave
 *              insIfOn
 *              insIfOff
 *              insIfRead
 *              insIfWrite
 *              insIfReadVerify
 *              insIfWriteVerify
 *              insIfDump
 *              insIfUndump
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *              19-Jan-2001   DA   insIfDump & insIfUndump
 *
 */
int
camp_tcl_ins( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    RES* pRes;
    INS_ADD_req insAddReq;
    INS_LOCK_req insLockReq;
    INS_LINE_req insLineReq;
    INS_IF_req insIfReq;
    DATA_req dataReq;
    INS_FILE_req insFileReq;
    INS_READ_req insReadReq;
    INS_WRITE_req insWriteReq;
    INS_DUMP_req insDumpReq;
    INS_UNDUMP_req insUndumpReq;
    CAMP_IF* pIf;
    CAMP_IF_t* pIF_t;
    TOKEN tok;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char* endptr;
    int status;
    int tries, max_tries;
    char* fmt;
    char* argv2[64];
    bool_t done;
    int i, j, n;
    double f;
    double tol;
    char* val;
    u_char uc;
    char* s;
    char message[256];
    char buf[256];

    switch( (INS_PROCS)clientData )
    {
      case INS_ADD:
        sprintf( message, "usage: %s <type> <ident>", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insAddReq.typeIdent = argv[1];
        insAddReq.ident = argv[2];

        pRes = campsrv_insadd_13( &insAddReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insAdd", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_SET:
        sprintf( message, "usage: %s <ins> [...]", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }
        
        pVar = camp_varGetp( argv[1] );
        if( pVar == NULL ) 
        {
            sprintf( str, "invalid path %s", argv[1] );
            Tcl_SetResult( interp, str, TCL_VOLATILE );
            return( TCL_ERROR );
        }
        pIns = pVar->spec.CAMP_SPEC_u.pIns;

        for( i = 2; i < argc; i++ )
        {
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }

            switch( tok.kind )
            {
              case TOK_OPT_LOCK:
                if( ++i >= argc ) break;
                if( !findToken( argv[i], &tok ) || 
                    ( ( tok.kind != TOK_ON ) && ( tok.kind != TOK_OFF ) ) )
                {
                    sprintf( str, "invalid token %s", argv[i] );
                    Tcl_SetResult( interp, str, TCL_VOLATILE );
                    return( TCL_ERROR );
                }

                insLockReq.dreq.path = argv[1];
                insLockReq.flag = ( tok.kind == TOK_ON );
                pRes = campsrv_inslock_13( &insLockReq, get_current_rqstp() );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed insLock", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );
                break;

              case TOK_OPT_LINE:
                if( ++i >= argc ) break;
                if( !findToken( argv[i], &tok ) || 
                    ( ( tok.kind != TOK_ON ) && ( tok.kind != TOK_OFF ) ) )
                {
                    sprintf( str, "invalid token %s", argv[i] );
                    Tcl_SetResult( interp, str, TCL_VOLATILE );
                    return( TCL_ERROR );
                }

                insLineReq.dreq.path = argv[1];
                insLineReq.flag = ( tok.kind == TOK_ON );
                pRes = campsrv_insline_13( &insLineReq, get_current_rqstp() );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed insLine", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );
                break;

              case TOK_OPT_ONLINE:
                insLineReq.dreq.path = argv[1];
                insLineReq.flag = 1;
                pRes = campsrv_insline_13( &insLineReq, get_current_rqstp() );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed ins online", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );
                break;

              case TOK_OPT_OFFLINE:
                insLineReq.dreq.path = argv[1];
                insLineReq.flag = 0;
                pRes = campsrv_insline_13( &insLineReq, get_current_rqstp() );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed ins offline", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );
                break;

              case TOK_OPT_IF_SET:
                bzero( (char*)&insIfReq, sizeof( INS_IF_req ) );

                insIfReq.dreq.path = argv[1];

                if( ++i >= argc ) break;
                insIfReq.IF.typeIdent = argv[i];

                pIF_t = camp_ifGetpIF_t( insIfReq.IF.typeIdent );
                if( pIF_t == NULL ) 
                {
                    sprintf( str, "invalid interface type %s", 
                             insIfReq.IF.typeIdent );
                    Tcl_SetResult( interp, str, TCL_VOLATILE );
                    return( TCL_ERROR );
                }

                if( ++i >= argc ) break;
                insIfReq.IF.accessDelay = (float)strtod( argv[i], &endptr );
                if( isgraph( *endptr ) )
                {
                    usage( message );
                    return( TCL_ERROR );
	        }

                *str = '\0';

                n = 0;
                switch( pIF_t->typeID )
                {
                  case CAMP_IF_TYPE_NONE:
                    n = 0;
                    break;

                  case CAMP_IF_TYPE_RS232:
                    n = 8;
                    break;

                  case CAMP_IF_TYPE_GPIB:
                    n = 3;
                    break;

                  case CAMP_IF_TYPE_CAMAC:
                    n = 3;
                    break;

                  case CAMP_IF_TYPE_VME:
                    n = 1;
                    break;

                  case CAMP_IF_TYPE_TCPIP:
                    n = 5;
                    break;

                  case CAMP_IF_TYPE_INDPAK:
                    n = 3;
                    break;

                }

                for( j = 0; ( j < n ) && ( ++i < argc ); j++ )
                {
                    strcat( str, argv[i] );
                    strcat( str, " " );
                }
                if( *str != '\0' ) str[strlen(str)-1] = '\0';

		insIfReq.IF.defn = str;

                pRes = campsrv_insifset_13( &insIfReq, get_current_rqstp() );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed insIfSet", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );

                break;

              case TOK_OPT_IF_MOD:

                if( pIns->pIF == NULL ) 
                {
                    Tcl_SetResult( interp, 
                        "cannot modify undefined interface", TCL_STATIC );
                    if( ++i >= argc ) break;
                    break;
                }

                bzero( (char*)&insIfReq, sizeof( INS_IF_req ) );
                insIfReq.dreq.path = argv[1];
                insIfReq.IF.typeIdent = strdup( pIns->pIF->typeIdent );
                /* Sept 29, 2000;  DJA adds: */
                insIfReq.IF.accessDelay = pIns->pIF->accessDelay;
		insIfReq.IF.defn = strdup( pIns->pIF->defn );

		strcpy( str, "" );

                switch( pIns->pIF->typeID )
                {
                  case CAMP_IF_TYPE_RS232:
		    s = strchr( insIfReq.IF.defn, ' ' );
                    if( ++i >= argc ) break;
		    strcpy( str, argv[i] );   /* Port */
		    strcat( str, s );

		    free( insIfReq.IF.defn );
		    insIfReq.IF.defn = strdup( str );
                    break;

                  case CAMP_IF_TYPE_GPIB:
                    s = strchr( insIfReq.IF.defn, ' ' );
                    if( ++i >= argc ) break;
                    strcpy( str, argv[i] );   /* Addr */
                    strcat( str, s );

                    free( insIfReq.IF.defn );
                    insIfReq.IF.defn = strdup( str );
                    break;

                  case CAMP_IF_TYPE_CAMAC:
                    if( ++i >= argc ) break;
		    strcpy( str, argv[i] );   /* B */
		    strcat( str, " " );
                    if( ++i >= argc ) break;
		    strcat( str, argv[i] );   /* C */
		    strcat( str, " " );
                    if( ++i >= argc ) break;
		    strcat( str, argv[i] );   /* N */

		    free( insIfReq.IF.defn );
		    insIfReq.IF.defn = strdup( str );
                    break;

                  case CAMP_IF_TYPE_VME:
                    if( ++i >= argc ) break;
		    strcpy( str, argv[i] );   /* Base */
		    strcat( str, " " );

		    free( insIfReq.IF.defn );
		    insIfReq.IF.defn = strdup( str );
                    break;

                  case CAMP_IF_TYPE_TCPIP:
                    if( ++i >= argc ) break;
		    strcpy( str, argv[i] );   /* Addr */
		    strcat( str, " " );
                    if( ++i >= argc ) break;
		    strcat( str, argv[i] );   /* Port */
		    strcat( str, " " );

		    free( insIfReq.IF.defn );
		    insIfReq.IF.defn = strdup( str );
                    break;

                  case CAMP_IF_TYPE_INDPAK: /* Only last param of INDPAK defn can be changed on the fly */
            	    strcpy( str, insIfReq.IF.defn );   /* Slot */
                    s = strchr( str, ' ' );
                    s ++;
                    s = strchr( s, ' ' );
                    s ++;
                    if( ++i >= argc ) break;
                    strcpy( s, argv[i] );

                    free( insIfReq.IF.defn );
                    insIfReq.IF.defn = strdup( str );
                    break;
                }
                pRes = campsrv_insifset_13( &insIfReq, get_current_rqstp() );
                free( insIfReq.IF.typeIdent );
                free( insIfReq.IF.defn );
                if( _failure( pRes->status ) )
                {
                    Tcl_SetResult( interp, "failed insIfSet", TCL_STATIC );
                    _free( pRes );
                    return( TCL_ERROR );
                }
                _free( pRes );
                break;
            }
        }
        break;

      case INS_DEL:
        sprintf( message, "usage: %s <ins>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        dataReq.path = argv[1];

        pRes = campsrv_insdel_13( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insDel", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_LOAD:
        sprintf( message, "usage: %s <ins> <filename> [<flag>]", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insFileReq.dreq.path = argv[1];
        insFileReq.datFile = argv[2];
        if( argc > 3 )
        {
            insFileReq.flag = strtol( argv[3], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                usage( message );
                return( TCL_ERROR );
            }
        }
        else
        {
            insFileReq.flag = 0;
        }

        pRes = campsrv_insload_13( &insFileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insLoad", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_SAVE:
        sprintf( message, "usage: %s <ins> <filename> [<flag>]", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insFileReq.dreq.path = argv[1];
        insFileReq.datFile = argv[2];
        if( argc > 3 )
        {
            insFileReq.flag = strtol( argv[3], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                usage( message );
                return( TCL_ERROR );
            }
        }
        else
        {
            insFileReq.flag = 0;
        }

        pRes = campsrv_inssave_13( &insFileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insSave", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_IF_ON:
        sprintf( message, "usage: %s <ins>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        dataReq.path = argv[1];

        pRes = campsrv_insifon_13( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfOn", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_IF_OFF:
        sprintf( message, "usage: %s <ins>", argv[0] );

        if( argc < 2 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        dataReq.path = argv[1];

        pRes = campsrv_insifoff_13( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfOff", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        break;

      case INS_IF_READ:
        sprintf( message, "usage: %s <ins> <cmd> <buf_len>", argv[0] );

        if( argc < 4 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insReadReq.dreq.path = argv[1];
        insReadReq.cmd.cmd_val = argv[2];
        insReadReq.cmd.cmd_len = strlen( insReadReq.cmd.cmd_val );
        insReadReq.buf_len = strtol( argv[3], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        pRes = campsrv_insifread_13( &insReadReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfRead", TCL_STATIC );
            free( pRes );
            return( TCL_ERROR );
        }

        free( (void*)pRes );

        Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE );
        /*
         *  Set campMsg to empty
         *  campMsg is set to the reading in campsrv_msg
         */
        camp_setMsg( "" );

        return( TCL_OK );
        break;

      case INS_IF_WRITE:
        sprintf( message, "usage: %s <ins> <cmd>", argv[0] );

        if( argc < 3 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insWriteReq.dreq.path = argv[1];
        insWriteReq.cmd.cmd_val = argv[2];
        insWriteReq.cmd.cmd_len = strlen( insWriteReq.cmd.cmd_val );

        pRes = campsrv_insifwrite_13( &insWriteReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfWrite", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        return( TCL_OK );
        break;

      case INS_IF_DUMP:
        sprintf( message, "usage: %s <ins> <filename> <cmd> <buf_len> <echo> ", argv[0] );

        if( argc < 6 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insDumpReq.dreq.path = argv[1];
        insDumpReq.fname.fname_val = argv[2];
        insDumpReq.fname.fname_len = strlen( insDumpReq.fname.fname_val );
        insDumpReq.cmd.cmd_val = argv[3];
        insDumpReq.cmd.cmd_len = strlen( insDumpReq.cmd.cmd_val );
        insDumpReq.skip.skip_val = argv[5];
        insDumpReq.skip.skip_len = strlen( insDumpReq.skip.skip_val );
        insDumpReq.buf_len = strtol( argv[4], &endptr, 0 ) + insDumpReq.skip.skip_len;

        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        pRes = campsrv_insifdump_13( &insDumpReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfDump", TCL_STATIC );
            free( pRes );
            return( TCL_ERROR );
        }

        free( (void*)pRes );

        Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE );
        /*
         *  Set campMsg to empty
         *  campMsg is set to the reading in campsrv_msg
         */
        camp_setMsg( "" );

        return( TCL_OK );
        break;

      case INS_IF_UNDUMP:
        sprintf( message, "usage: %s <ins> <filename> <cmd> <buff_len>", argv[0] );

        if( argc < 5 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        insUndumpReq.dreq.path = argv[1];
        insUndumpReq.fname.fname_val = argv[2];
        insUndumpReq.fname.fname_len = strlen( insUndumpReq.fname.fname_val );
        insUndumpReq.cmd.cmd_val = argv[3];
        insUndumpReq.cmd.cmd_len = strlen( insUndumpReq.cmd.cmd_val );
        insUndumpReq.buf_len = strtol( argv[4], &endptr, 0 );

        pRes = campsrv_insifundump_13( &insUndumpReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            Tcl_SetResult( interp, "failed insIfUndump", TCL_STATIC );
            _free( pRes );
            return( TCL_ERROR );
        }
        _free( pRes );
        return( TCL_OK );
        break;

      case INS_IF_READ_VERIFY:
        sprintf( message, 
                 "usage: %s <ins> <cmd> <buf_len> <var> <fmt> <max_tries>", 
                 argv[0] );

        if( argc < 7 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        pVar = camp_varGetp( argv[4] );
        if( pVar == NULL ) 
        {
            sprintf( str, "invalid path %s", argv[4] );
            Tcl_SetResult( interp, str, TCL_VOLATILE );
            return( TCL_ERROR );
        }

        fmt = argv[5];

        max_tries = strtol( argv[6], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( tries = 0; tries < max_tries; tries++ )
        {
            status = camp_tcl_ins( (ClientData)INS_IF_READ, interp, 4, argv );
            if( status == TCL_OK )
            {
	        strcpy( buf, interp->result );

                argv2[0] = "scan";
                argv2[1] = buf;
                argv2[2] = fmt;
                argv2[3] = "val";

                status = Tcl_ScanCmd( 0, interp, 4, argv2 );
                if( ( status == TCL_OK ) && streq( interp->result, "1" ) )
                {
                    argv2[0] = toktostr( TOK_VAR_DOSET );
                    argv2[1] = pVar->core.path;
                    argv2[2] = toktostr( TOK_VALUE_FLAG );
                    argv2[3] = Tcl_GetVar( interp, "val", 0 );

                    status = camp_tcl_varSet( (ClientData)VAR_DOSET, interp,
                                4, argv2 );
                    if( status == TCL_OK )
                    {
		      /*
		       *  Set result to the reading
		       */
		      Tcl_SetResult( interp, buf, TCL_VOLATILE );
		      
		      /*
		       *  Everything worked, so clear
		       *  campMsg.  This will get set to the 
		       *  reading by campsrv_msg
		       */
		      camp_setMsg( "" );
		      break;
                    }
                }
		else
		  {
		    camp_appendMsg( "error parsing reading, got '%s'", 
				buf );
		  }
            }
        }

        if( tries == max_tries )
        {
            Tcl_SetResult( interp, "failed insIfReadVerify", TCL_STATIC );
            return( TCL_ERROR );
        }

        return( TCL_OK );
        break;

      case INS_IF_WRITE_VERIFY:
        sprintf( message, 
"usage: %s <ins> <w_cmd> <r_cmd> <buf_len> <var> <fmt> <max_tries> <val> [<tol>]", 
                 argv[0] );

        if( argc < 9 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        pVar = camp_varGetp( argv[5] );
        if( pVar == NULL ) 
        {
            sprintf( str, "invalid path %s", argv[5] );
            Tcl_SetResult( interp, str, TCL_VOLATILE );
            return( TCL_ERROR );
        }

        max_tries = strtol( argv[7], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        for( tries = 0; tries < max_tries; tries++ )
        {
            if( !streq( argv[2], "" ) )
            {
                /*
                 *  Issue the Write command
                 */
                argv2[0] = toktostr( TOK_INS_IF_WRITE );
                argv2[1] = argv[1];
                argv2[2] = argv[2];

                status = camp_tcl_ins( (ClientData)INS_IF_WRITE, 
                                       interp, 3, argv2 );
                if( status == TCL_ERROR )
                {
                    continue;
                }
            }

            /*
             *  Readback the result
             */
            argv2[0] = toktostr( TOK_INS_IF_READ );
            argv2[1] = argv[1];
            argv2[2] = argv[3];
            argv2[3] = argv[4];

            status = camp_tcl_ins( (ClientData)INS_IF_READ, interp, 4, argv2 );
            if( status == TCL_ERROR )
            {
                continue;
            }

            /*
             *  Parse the result string
             */
            argv2[0] = "scan";
            argv2[1] = strdup( interp->result );
            argv2[2] = argv[6];
            argv2[3] = "val";
            status = Tcl_ScanCmd( 0, interp, 4, argv2 );
            if( !( ( status == TCL_OK ) && streq( interp->result, "1" ) ) )
            {
                sprintf( str, "error parsing reading, got '%s'", interp->result );
		camp_appendMsg( str );
                _free( argv2[1] );
                continue;
            }
            _free( argv2[1] );

            /*
             *  Set the value of the variable accordingly
             */
            argv2[0] = toktostr( TOK_VAR_DOSET );
            argv2[1] = argv[5];
            argv2[2] = toktostr( TOK_VALUE_FLAG );
            argv2[3] = Tcl_GetVar( interp, "val", 0 );
            status = camp_tcl_varSet( (ClientData)VAR_DOSET, interp,
                        4, argv2 );
            if( status == TCL_ERROR )
            {
                continue;
            }

            /*
             *  Compare the read value with the target value
             */
            val = argv[8];

            done = FALSE;
            switch( pVar->core.varType )
            {
              case CAMP_VAR_TYPE_INT:
                i = strtol( val, &endptr, 0 );
                if( isgraph( *endptr ) )
                {
                    usage( message );
                    return( TCL_ERROR );
                }

                if( (int)pVar->spec.CAMP_SPEC_u.pNum->val == i )
                {
                    done = TRUE;
                }
                break;

              case CAMP_VAR_TYPE_FLOAT:
                f = strtod( val, &endptr );
                if( isgraph( *endptr ) )
                {
                    usage( message );
                    return( TCL_ERROR );
                }

                if( argc > 9 )
                {
                    tol = strtod( argv[9], &endptr );
                    if( isgraph( *endptr ) )
                    {
                        usage( message );
                        return( TCL_ERROR );
                    }
                }
                else
                {
                    tol = 0.0;
                }

                if( ( pVar->spec.CAMP_SPEC_u.pNum->val >= (f-tol) ) &&
                    ( pVar->spec.CAMP_SPEC_u.pNum->val <= (f+tol) ) )
                {
                    done = TRUE;
                }
                break;

              case CAMP_VAR_TYPE_STRING:
                if( streq( pVar->spec.CAMP_SPEC_u.pStr->val, val ) )
                {
                    done = TRUE;
                }
                break;

              case CAMP_VAR_TYPE_SELECTION:
                i = strtol( val, &endptr, 0 );
                if( isgraph( *endptr ) )
                {
                    /*
                     *  Assume the value was given by its label
                     */
                    status = varSelGetLabelID( pVar, val, &uc );
                    if( _failure( status ) )
                    {
                        usage( message );
                        return( TCL_ERROR );
                    }

                    i = (int)uc;
                }

                if( pVar->spec.CAMP_SPEC_u.pSel->val == i )
                {
                    done = TRUE;
                }
                break;
            }

            if( done ) return( TCL_OK );
        }

	Tcl_SetResult( interp, "failed insIfWriteVerify", TCL_STATIC );
        return( TCL_ERROR );
    }
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varDef
 *
 *  Purpose:    Process variable definition Tcl commands
 *
 *              Interpret the variable definition command and add the newly
 *              created variable to the CAMP instrument database.
 *              Normally called from a CAMP Tcl instrument driver only.
 *
 *  Provides:   
 *              CAMP_INT
 *              CAMP_FLOAT
 *              CAMP_SELECT
 *              CAMP_STRING
 *              CAMP_ARRAY
 *              CAMP_STRUCT
 *              CAMP_INSTRUMENT
 *              CAMP_LINK
 *        
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_varDef( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    int status;
    char str[LEN_STR+1];
    char ident[LEN_IDENT+1];
#ifdef MULTITHREADED
    pthread_mutexattr_t mutex_attr;
#endif /* MULTITHREADED */
    char message[256];

    sprintf( message, "usage: %s <var> [...]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = (CAMP_VAR*)zalloc( sizeof( CAMP_VAR ) );

    switch( (VAR_PROCS)clientData )
    {
      case VAR_INT:
        pVar->core.varType = CAMP_VAR_TYPE_INT;
        pVar->spec.CAMP_SPEC_u.pNum = 
            (CAMP_NUMERIC*)zalloc( sizeof( CAMP_NUMERIC ) );
        break;
      case VAR_FLOAT:
        pVar->core.varType = CAMP_VAR_TYPE_FLOAT;
        pVar->spec.CAMP_SPEC_u.pNum = 
            (CAMP_NUMERIC*)zalloc( sizeof( CAMP_NUMERIC ) );
        break;
      case VAR_STRING:
        pVar->core.varType = CAMP_VAR_TYPE_STRING;
        pVar->spec.CAMP_SPEC_u.pStr = 
            (CAMP_STRING*)zalloc( sizeof( CAMP_STRING ) );
        break;
      case VAR_SELECTION:
        pVar->core.varType = CAMP_VAR_TYPE_SELECTION;
        pVar->spec.CAMP_SPEC_u.pSel = 
            (CAMP_SELECTION*)zalloc( sizeof( CAMP_SELECTION ) );
        break;
      case VAR_ARRAY:
        pVar->core.varType = CAMP_VAR_TYPE_ARRAY;
        pVar->spec.CAMP_SPEC_u.pArr = 
            (CAMP_ARRAY*)zalloc( sizeof( CAMP_ARRAY ) );
        break;
      case VAR_STRUCTURE:
        pVar->core.varType = CAMP_VAR_TYPE_STRUCTURE;
        pVar->spec.CAMP_SPEC_u.pStc = 
            (CAMP_STRUCTURE*)zalloc( sizeof( CAMP_STRUCTURE ) );
        break;
      case VAR_INSTRUMENT:
        pVar->core.varType = CAMP_VAR_TYPE_INSTRUMENT;
        pVar->spec.CAMP_SPEC_u.pIns = 
            (CAMP_INSTRUMENT*)zalloc( sizeof( CAMP_INSTRUMENT ) );
        break;
      case VAR_LINK:
        pVar->core.varType = CAMP_VAR_TYPE_LINK;
        pVar->spec.CAMP_SPEC_u.pLnk = 
            (CAMP_LINK*)zalloc( sizeof( CAMP_LINK ) );
        break;
    }

    /*
     *  Initializations
     */
    /* Static */
    camp_pathExpand( argv[1], str );
    pVar->core.path = strndup( str, LEN_PATH );
    camp_pathGetLast( str, ident );
    pVar->core.ident = strndup( ident, LEN_IDENT );
    pVar->core.title = strndup( "", LEN_TITLE );
    pVar->core.help = strndup( "", LEN_HELP );
    pVar->core.readProc = strndup( "", LEN_PROC );
    pVar->core.writeProc = strndup( "", LEN_PROC );
    /* Dynamic */
    pVar->core.logAction = strndup( "", LEN_PROC );
    pVar->core.alarmAction = strndup( "", LEN_PROC );
    pVar->core.statusMsg = strndup( "", LEN_STATUS_MSG );

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        pVar->spec.CAMP_SPEC_u.pNum->tol = -1.0;
        pVar->spec.CAMP_SPEC_u.pNum->units = strndup( "", LEN_UNITS );
        break;
      case CAMP_VAR_TYPE_STRING:
        pVar->spec.CAMP_SPEC_u.pStr->val = strndup( "", LEN_STRING );
        break;
      case CAMP_VAR_TYPE_SELECTION:
        break;
      case CAMP_VAR_TYPE_ARRAY:
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        pVar->spec.CAMP_SPEC_u.pIns->lockHost = strndup( "", LEN_NODENAME );
        pVar->spec.CAMP_SPEC_u.pIns->lockOs = strndup( "", LEN_IDENT );
        pVar->spec.CAMP_SPEC_u.pIns->defFile = strndup( "", LEN_FILENAME );
        pVar->spec.CAMP_SPEC_u.pIns->iniFile = strndup( "", LEN_FILENAME );
        pVar->spec.CAMP_SPEC_u.pIns->typeIdent = strndup( "", LEN_IDENT );
        pVar->spec.CAMP_SPEC_u.pIns->initProc = strndup( "", LEN_PROC );
        pVar->spec.CAMP_SPEC_u.pIns->deleteProc = strndup( "", LEN_PROC );
        pVar->spec.CAMP_SPEC_u.pIns->onlineProc = strndup( "", LEN_PROC );
        pVar->spec.CAMP_SPEC_u.pIns->offlineProc = strndup( "", LEN_PROC );
#ifdef MULTITHREADED
        /*
	 *  Create Tcl interpreter private to instrument
	 */
        pVar->spec.CAMP_SPEC_u.pIns->interp = campTcl_CreateInterp();

	/* 
	 *  Initialize the mutex handle 
	 */
        if( pthread_mutexattr_create( &mutex_attr ) != 0 )
        {
            camp_logMsg( "error creating instrument mutex attribute" );
        }

        if( pthread_mutexattr_setkind_np( &mutex_attr, MUTEX_RECURSIVE_NP ) 
            != 0 )
        {
            camp_logMsg( "error setting instrument mutex attribute" );
        }

	if( pthread_mutex_init( &(pVar->spec.CAMP_SPEC_u.pIns->mutex), 
                                mutex_attr ) != 0 )
        {
            camp_logMsg( "error initializing instrument mutex" );
        }

        if( pthread_mutexattr_delete( &mutex_attr ) != 0 )
        {
            camp_logMsg( "error deleting instrument mutex attribute" );
        }
#endif /* MULTITHREADED */
        break;
      case CAMP_VAR_TYPE_LINK:
        pVar->spec.CAMP_SPEC_u.pLnk->path = strndup( "", LEN_PATH );
        break;
    }

    pVar->spec.varType = pVar->core.varType;

    status = varAdd( argv[1], pVar );

    status = camp_tcl_varSet( (ClientData)VAR_DEF, interp, argc, argv );
    return( status );
}


/*
 *  Name:       camp_tcl_varSet
 *
 *  Purpose:    Process variable setting Tcl commands
 *
 *              Can be called in different contexts.  When called by
 *              camp_tcl_varDef, parameters relating only to variable
 *              creation are in effect.  When called by varDoSet
 *              (or camp_tcl_varDef) parameters normally set only by the
 *              Tcl instrument driver are in effect.  Additionally,
 *              a value set command from "varDoSet" or camp_tcl_varDef
 *              initiates an immediate change of the value in the CAMP
 *              database.  When called with "varSet", however, a value
 *              set command (parameter "-v") initiates the varSetReq
 *              (variable set request) routine which eventually calls the
 *              variable's "-writeProc" in the Tcl driver.  This is also
 *              true when setting certain alarm related parameters.  Thus,
 *              a "varDoSet" is an immediate hard set of the CAMP database,
 *              while a "varSet" is a higher level command requesting
 *              instrument communication.  "varSet -v" is parallel to the
 *              "varRead" command.
 *
 *  Provides:   
 *              varSet
 *              varDoSet
 *              also processes setting parameters passed to camp_tcl_varDef
 *
 *              These commands have many parameters which set many aspects
 *              of CAMP variables, as described in the CAMP programmer's
 *              guide.
 *
 *  Called by:  Tcl interpreter and camp_tcl_varDef
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_varSet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_SPEC spec;
    TOKEN tok;
    TOKEN tok2;
    int i, j;
    bool_t dopoll;
    bool_t poll_flag;
    float poll_interval;
    bool_t dolog;
    bool_t log_flag;
    char* log_action;
    bool_t doalarm;
    bool_t alarm_flag;
    char* alarm_action;
    int valArg;
    CAMP_SELECTION_ITEM** ppItem;
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];
    char* endptr;
    char message[256];
    CAMP_ARRAY* pArr;
    int argc2;
    char** argv2;

    sprintf( message, "usage: %s <var> [...]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    if( ( pVar = camp_varGetp( argv[1] ) ) == NULL )
    {
        sprintf( buf, "invalid var %s", argv[1] );
        Tcl_SetResult( interp, buf, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    status = camp_checkInsLock( pVar, get_current_rqstp() );
    if( _failure( status ) ) 
    {
        Tcl_SetResult( interp, "cannot set, instrument locked", TCL_STATIC );
        return( TCL_ERROR );
    }

    /*
     * Some things (poll, alarm, log, value) depend on multiple switches (-p on -p_int 10)
     * so delay processing them until after the whole command is parsed.  The following 
     * variables are used to hold on to the parameters/values to be done at the end.
     */
    valArg = 0;

    dopoll = FALSE;
    poll_flag = pVar->core.status & CAMP_VAR_ATTR_POLL;
    poll_interval = pVar->core.pollInterval;

    dolog = FALSE;
    log_flag = pVar->core.status & CAMP_VAR_ATTR_LOG;
    log_action = pVar->core.logAction;

    doalarm = FALSE;
    alarm_flag = pVar->core.status & CAMP_VAR_ATTR_ALARM;
    alarm_action = pVar->core.alarmAction;

    for( i = 2; i < argc; i++ )
    {
        bzero( (char*)&tok, sizeof( TOKEN ) );
        if( !findToken( argv[i], &tok ) )
        {
            sprintf( str, "invalid token %s", argv[i] );
            Tcl_SetResult( interp, str, TCL_VOLATILE );

            /*
             *  18-Dec-2000  TW  Make this an error
             */
            return( TCL_ERROR );
            /*  continue;  */
        }

        switch( tok.kind )
        {
          case TOK_SHOW_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_SHOW;
            }
            break;

          case TOK_SET_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_SET;
            }
            break;

          case TOK_READ_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_READ;
            }
            break;

          case TOK_POLL_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_POLL;
            }
            break;

          case TOK_LOG_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_LOG;
            }
            break;

          case TOK_ALARM_ATTR:
            if( (int)clientData == VAR_DEF )
            {
                pVar->core.attributes |= CAMP_VAR_ATTR_ALARM;
            }
            break;

          case TOK_TITLE:
            if( ++i >= argc ) break;
            if( (int)clientData == VAR_DEF )
            {
                _free( pVar->core.title );
                pVar->core.title = strndup( argv[i], LEN_TITLE );
            }
            break;

          case TOK_HELP:
            if( ++i >= argc ) break;
            if( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) )
            {
                _free( pVar->core.help );
                pVar->core.help = strndup( argv[i], LEN_HELP );
            }
            break;

          case TOK_INSTYPE:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( (int)clientData == VAR_DEF ) )
            {
                _free( pVar->spec.CAMP_SPEC_u.pIns->typeIdent );
                pVar->spec.CAMP_SPEC_u.pIns->typeIdent = 
                    strndup( argv[i], LEN_IDENT );
            }
            break;

          case TOK_OPT_INITPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->spec.CAMP_SPEC_u.pIns->initProc );
                pVar->spec.CAMP_SPEC_u.pIns->initProc = 
                    strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_OPT_DELETEPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->spec.CAMP_SPEC_u.pIns->deleteProc );
                pVar->spec.CAMP_SPEC_u.pIns->deleteProc = 
                    strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_OPT_ONLINEPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->spec.CAMP_SPEC_u.pIns->onlineProc );
                pVar->spec.CAMP_SPEC_u.pIns->onlineProc = 
                    strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_OPT_OFFLINEPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->spec.CAMP_SPEC_u.pIns->offlineProc );
                pVar->spec.CAMP_SPEC_u.pIns->offlineProc = 
                    strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_OPT_READPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->core.readProc );
                pVar->core.readProc = strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_OPT_WRITEPROC:
          case TOK_OPT_SETPROC:
            if( ++i >= argc ) break;
            if( ( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT ) &&
                ( ( (int)clientData == VAR_DEF ) || ( (int)clientData == VAR_DOSET ) ) )
            {
                _free( pVar->core.writeProc );
                pVar->core.writeProc = strndup( argv[i], LEN_PROC );
            }
            break;

          case TOK_SELECTIONS:
            if( ( pVar->core.varType == CAMP_VAR_TYPE_SELECTION ) &&
                ( (int)clientData == VAR_DEF ) )
            {
                ppItem = &pVar->spec.CAMP_SPEC_u.pSel->pItems;
                while( ( (i+1) < argc ) && ( argv[i+1][0] != '-' ) )
                {
                    i++;
                    *ppItem = (CAMP_SELECTION_ITEM*)zalloc( 
                                    sizeof( CAMP_SELECTION_ITEM ) );
                    (*ppItem)->label = strndup( argv[i], LEN_SELECTION_LABEL );
                    pVar->spec.CAMP_SPEC_u.pSel->num++;
                    ppItem = &(*ppItem)->pNext;
                }
            }
            break;

          case TOK_ARRAYDEF:
            if( ( pVar->core.varType == CAMP_VAR_TYPE_ARRAY ) &&
                ( (int)clientData == VAR_DEF ) )
            {
                pArr = pVar->spec.CAMP_SPEC_u.pArr;

                if( ++i >= argc ) break;

                if( !findToken( argv[i], &tok2 ) )
                {
                    Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
                    return( TCL_ERROR );
	        }

                switch( tok2.kind )
	        {
                  case TOK_INT:
                    pArr->varType = CAMP_VAR_TYPE_INT;
                    break;
                  case TOK_FLOAT:
                    pArr->varType = CAMP_VAR_TYPE_FLOAT;
                    break;
                  case TOK_STRING:
                    pArr->varType = CAMP_VAR_TYPE_STRING;
                    break;
                  default:
                    Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
                    return( TCL_ERROR );
		}

                if( ++i >= argc ) break;

                pArr->elemSize = strtoul( argv[i], &endptr, 0 );
                if( isgraph( *endptr ) )
                {
                    Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
                    return( TCL_ERROR );
	        }

                if( ++i >= argc ) break;

                pArr->dim = strtoul( argv[i], &endptr, 0 );
                if( isgraph( *endptr ) )
                {
                    Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
                    return( TCL_ERROR );
	        }

                if( pArr->dim > MAX_ARRAY_DIM )
                {
                    Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
                    return( TCL_ERROR );
	        }

                if( ++i >= argc ) break;

		status = Tcl_SplitList( interp, argv[i], &argc2, &argv2 );
		if( status == TCL_ERROR )
		{
                    Tcl_SetResult( interp, "invalid arrayDef list", TCL_STATIC );
		    return( TCL_ERROR );
		}

		if( pArr->dim != argc2 )
		{
                    free( argv2 );
		    Tcl_SetResult( interp, "conflicting array dimension parameters", TCL_STATIC );
		    return( TCL_ERROR );
		}

		pArr->totElem = 1;
                for( j = 0; j < pArr->dim; j++ )
                {
		  if( Tcl_GetInt( interp, argv2[j], &(pArr->dimSize[j]) ) == TCL_ERROR )
		  {
		      free( argv2 );
		      return( TCL_ERROR );
		  }

		  pArr->totElem *= pArr->dimSize[j];
	        }

 		free( argv2 );

		pArr->pVal = (caddr_t)malloc( pArr->elemSize*pArr->totElem );
	    }
            else
	    {
                Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
		return( TCL_ERROR );
	    }
            break;

          case TOK_VALUE_FLAG:
            if( ++i >= argc ) break;

            /*
             * Save setting of value for last.  This is particularly pertinent for
             * cases like CAMP_SELECT -v 2 -selections foo bar
             */
            valArg = i;
            break;

          case TOK_ALARM_TOL:
            if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != 
                CAMP_VAR_TYPE_NUMERIC )
                break;
            if( ++i >= argc ) break;

            bzero( (char*)&spec, sizeof( CAMP_SPEC ) );
            spec.varType = pVar->core.varType;
            spec.CAMP_SPEC_u.pNum = 
                (CAMP_NUMERIC*)zalloc( sizeof( CAMP_NUMERIC ) );
            bcopy( (char*)pVar->spec.CAMP_SPEC_u.pNum, (char*)spec.CAMP_SPEC_u.pNum, 
                   sizeof( CAMP_NUMERIC ) );
            spec.CAMP_SPEC_u.pNum->tol = strtod( argv[i], &endptr );
            if( isgraph( *endptr ) )
            {
                free( spec.CAMP_SPEC_u.pNum );
                Tcl_SetResult( interp, "invalid alarm tolerance", TCL_STATIC );
                return( TCL_ERROR );
	    }

            if( ( (int)clientData == VAR_DOSET ) || ( (int)clientData == VAR_DEF ) )
            {
                status = varSetSpec( pVar, &spec );
            }
            else
            {
                status = varSetReq( pVar, &spec );
            }
/*  CAREFUL
            xdr_free( xdr_CAMP_SPEC, (char*)&spec );
*/
            _free( spec.CAMP_SPEC_u.pNum );

            if_failure_status_return( status );

            break;

          case TOK_ALARM_TOLTYPE:
            if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != 
                CAMP_VAR_TYPE_NUMERIC )
                break;
            if( ++i >= argc ) break;

            bzero( (char*)&spec, sizeof( CAMP_SPEC ) );
            spec.varType = pVar->core.varType;
            spec.CAMP_SPEC_u.pNum = 
                (CAMP_NUMERIC*)zalloc( sizeof( CAMP_NUMERIC ) );
            bcopy( (char*)pVar->spec.CAMP_SPEC_u.pNum, (char*)spec.CAMP_SPEC_u.pNum, 
                   sizeof( CAMP_NUMERIC ) );

            spec.CAMP_SPEC_u.pNum->tolType = strtol( argv[i], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                _free( spec.CAMP_SPEC_u.pNum );
                Tcl_SetResult( interp, "invalid alarm tolerance type", TCL_STATIC );
                return( TCL_ERROR );
            }

            if( ( (int)clientData == VAR_DOSET ) || ( (int)clientData == VAR_DEF ) )
            {
                status = varSetSpec( pVar, &spec );
            }
            else
            {
                status = varSetReq( pVar, &spec );
            }

/*  CAREFUL
            xdr_free( xdr_CAMP_SPEC, (char*)&spec );
*/
            _free( spec.CAMP_SPEC_u.pNum );

            if_failure_status_return( status );

            break;

          case TOK_UNITS:

            if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != 
                CAMP_VAR_TYPE_NUMERIC )
                break;

            if( ++i >= argc ) break;

            _free( pVar->spec.CAMP_SPEC_u.pNum->units );
            pVar->spec.CAMP_SPEC_u.pNum->units = strndup( argv[i], LEN_UNITS );

            break;

          case TOK_SHOW_FLAG:
            if( ++i >= argc ) break;
            if( ( (int)clientData != VAR_DEF ) && ( (int)clientData != VAR_DOSET ) )
            {
                break;
            }
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            status = varSetShow( pVar, 
                        ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );
            if_failure_status_return( status );
            break;

          case TOK_SET_FLAG:
            if( ++i >= argc ) break;
            if( ( (int)clientData != VAR_DEF ) && ( (int)clientData != VAR_DOSET ) )
            {
                break;
            }
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            status = varSetSet( pVar, 
                        ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );
            if_failure_status_return( status );
            break;

          case TOK_READ_FLAG:
            if( ++i >= argc ) break;
            if( ( (int)clientData != VAR_DEF ) && ( (int)clientData != VAR_DOSET ) )
            {
                break;
            }
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            status = varSetRead( pVar, 
                        ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );
            if_failure_status_return( status );
            break;

          case TOK_LOG_FLAG:
            if( ++i >= argc ) break;
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            log_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
            dolog = TRUE;
            break;

          case TOK_LOG_ACTION:
            if( ++i >= argc ) break;
            log_action = argv[i];
            dolog = TRUE;
            break;

          case TOK_POLL_FLAG:
            if( ++i >= argc ) break;
            if( !findToken( argv[i], &tok ) )
            {
                sprintf( str, "invalid token %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            poll_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
            dopoll = TRUE;
            break;

          case TOK_POLL_INTERVAL:
            if( ++i >= argc ) break;
            poll_interval = (float)strtod( argv[i], &endptr );
            if( isgraph( *endptr ) )
            {
                Tcl_SetResult( interp, "invalid poll interval", TCL_STATIC );
                return( TCL_ERROR );
	    }

            dopoll = TRUE;
            break;

          case TOK_ALARM_FLAG:
            if( ++i >= argc ) break;
            if( !findToken( argv[i], &tok ) )
            {
                Tcl_SetResult( interp, "invalid alarm flag", TCL_STATIC );
                return( TCL_ERROR );
            }
            alarm_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
            doalarm = TRUE;
            break;

          case TOK_ALARM_ACTION:
            if( ++i >= argc ) break;
            alarm_action = argv[i];
            doalarm = TRUE;
            break;

          case TOK_ZERO_FLAG:
            status = varZeroStats( pVar );
            if_failure_status_return( status );
            break;

          case TOK_ALERT_FLAG:
            if( ++i >= argc ) break;
            if( ( (int)clientData != VAR_DEF ) && ( (int)clientData != VAR_DOSET ) )
            {
                break;
            }
            if( !findToken( argv[i], &tok ) )
            {
                Tcl_SetResult( interp, "invalid alert flag", TCL_STATIC );
                return( TCL_ERROR );
            }
            status = varSetAlert( pVar, 
                        ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );
            if_failure_status_return( status );
            break;

          case TOK_MSG_FLAG:
            if( ++i >= argc ) break;
            if( ( (int)clientData != VAR_DEF ) && ( (int)clientData != VAR_DOSET ) )
            {
                break;
            }
            status = varSetMsg( pVar, argv[i] );
            if_failure_status_return( status );
            break;

          default:
            break;
        }
    }

    /* 
     * Now the whole command is parsed and we perform the settings that we delayed
     */
    /*
     * Perform value setting at last (value index saved in valArg)
     */
    if( valArg > 0 )
    {
        i = valArg;
        bzero( (char*)&spec, sizeof( CAMP_SPEC ) );
        spec.varType = pVar->core.varType;

        switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
        {
          case CAMP_VAR_TYPE_NUMERIC:
            spec.CAMP_SPEC_u.pNum = 
              (CAMP_NUMERIC*)zalloc( sizeof( CAMP_NUMERIC ) );
            bcopy( (char*)pVar->spec.CAMP_SPEC_u.pNum, (char*)spec.CAMP_SPEC_u.pNum, 
                   sizeof( CAMP_NUMERIC ) );

            spec.CAMP_SPEC_u.pNum->val = strtod( argv[i], &endptr );
            if( isgraph( *endptr ) )
            {
                free( spec.CAMP_SPEC_u.pNum );
                sprintf( str, "invalid numeric value %s", argv[i] );
                Tcl_SetResult( interp, str, TCL_VOLATILE );
                return( TCL_ERROR );
            }
            break;

          case CAMP_VAR_TYPE_SELECTION:
            spec.CAMP_SPEC_u.pSel = 
              (CAMP_SELECTION*)zalloc( sizeof( CAMP_SELECTION ) );
            spec.CAMP_SPEC_u.pSel->val = 
              (u_char)strtol( argv[i], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                /*
                 *  Assume the value was given by its label
                 */
                status = varSelGetLabelID( pVar, argv[i], 
                                           &spec.CAMP_SPEC_u.pSel->val );
                if( _failure( status ) )
                {
                    Tcl_SetResult( interp, "invalid selection value", TCL_STATIC );
                    return( TCL_ERROR );
                }
            }
            break;

          case CAMP_VAR_TYPE_STRING:
            spec.CAMP_SPEC_u.pStr = 
              (CAMP_STRING*)zalloc( sizeof( CAMP_STRING ) );
            spec.CAMP_SPEC_u.pStr->val = argv[i];
            break;

          default:
            break;
        }

        if( ( (int)clientData == VAR_DOSET ) || ( (int)clientData == VAR_DEF ) )
        {
            status = varSetSpec( pVar, &spec );
        }
        else
        {
            status = varSetReq( pVar, &spec );
        }

        _free( spec.CAMP_SPEC_u.pNum );

        if_failure_status_return( status );
    }

    /* 
     * perform poll setting
     */
    if( dopoll )
    {
        status = varSetPoll( pVar, poll_flag, poll_interval );
        if( _failure( status ) ) 
        {
            Tcl_SetResult( interp, "unexpected poll flag", TCL_STATIC );
            return( TCL_ERROR );
        }
    }

    /*
     * perform alarm setting
     */
    if( doalarm )
    {
        status = varSetAlarm( pVar, alarm_flag, alarm_action );
        if( _failure( status ) ) 
        {
            Tcl_SetResult( interp, "unexpected alarm flag", TCL_STATIC );
            return( TCL_ERROR );
        }
    }

    /*
     * perform log setting
     */
    if( dolog )
    {
        varSetLog( pVar, log_flag, log_action );
        if( _failure( status ) ) 
        {
            Tcl_SetResult( interp, "unexpected log flag", TCL_STATIC );
            return( TCL_ERROR );
        }
    }

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varRead
 *
 *  Purpose:    Process variable read Tcl command
 *
 *              This routine calls the RPC entry point campsrv_varread_13,
 *              thereby making a high level request to read the variable
 *              value from an instrument.  This eventually calls the
 *              variable's "-readProc" in the Tcl driver.
 *
 *  Provides:   
 *              varRead
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_varRead( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    DATA_req dataReq;
    RES* pRes;
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    dataReq.path = argv[1];
    pRes = campsrv_varread_13( &dataReq, get_current_rqstp() );
    if( _failure( pRes->status ) )
    {
        Tcl_SetResult( interp, "failed varRead", TCL_STATIC );
        _free( pRes );
        return( TCL_ERROR );
    }
    _free( pRes );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varTest
 *
 *  Purpose:    Process variable test Tcl command
 *
 *              For Numeric variable types only this routine tests whether
 *              a variable is within some tolerance or whether it is in
 *              alert state.  These functions relate to CAMP alarms.
 *
 *  Provides:   
 *              varTestTol
 *              varTestAlert
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 * 
 *              The Tcl result string is a boolean integer indicating status.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varTest( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    char str[LEN_STR+1];
    double val;
    int status;
    long int n;
    char message[256];
    char* endptr;

    sprintf( message, "usage: %s <var> [...]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid path %s", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    sprintf( message, "usage: %s <var> <val>", argv[0] );
    if( argc < 3 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
      {
      case CAMP_VAR_TYPE_NUMERIC: 
        val = strtod( argv[2], &endptr );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
	}

        switch( (VAR_UTILPROCS)clientData )
        {
          case VAR_TESTTOL:
            status = camp_varNumTestTol( argv[1], val );
            break;
          case VAR_TESTALERT:
            status = varNumTestAlert( pVar, val );
            break;
          default:
            status = TRUE;
            break;
        }
        break;

      case CAMP_VAR_TYPE_SELECTION:

        n = strtol( argv[2], &endptr, 10 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
	}
        val = (double)n;
        
        switch( (VAR_UTILPROCS)clientData )
        {
          case VAR_TESTTOL:
            status = camp_varNumTestTol( argv[1], val );
            break;
          case VAR_TESTALERT:
            status = varNumTestAlert( pVar, val );
            break;
          default:
            status = TRUE;
            break;
        }
        break;

      default:
        status = TRUE;
        break;
    }

    if( status ) 
    {
        Tcl_SetResult( interp, itoa( 1 ), TCL_VOLATILE );
    }
    else
    {
        Tcl_SetResult( interp, itoa( 0 ), TCL_VOLATILE );
    }

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_lnkSet
 *
 *  Purpose:    Process Link variable set Tcl command
 *
 *              For Link variable types only this routine sets the path
 *              of a link variable.  The command "varSet -v" for a link
 *              variable will in contrast initiate a setting of the variable
 *              that the link "points to".
 *
 *  Provides:   
 *              lnkSet
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_lnkSet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    DATA_SET_req dataSetReq;
    RES* pRes;
    char message[256];

    sprintf( message, "usage: %s <var> <val>", argv[0] );

    if( argc < 3 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    dataSetReq.dreq.path = argv[1];
    dataSetReq.spec.varType = CAMP_VAR_TYPE_LINK;
    dataSetReq.spec.CAMP_SPEC_u.pLnk = 
        (CAMP_LINK*)zalloc( sizeof( CAMP_LINK ) );
    dataSetReq.spec.CAMP_SPEC_u.pLnk->path = argv[2];

    pRes = campsrv_varlnkset_13( &dataSetReq, get_current_rqstp() );
    if( _failure( pRes->status ) )
    {
        Tcl_SetResult( interp, "failed lnkSet", TCL_STATIC );
        _free( pRes );
        _free( (void*)dataSetReq.spec.CAMP_SPEC_u.pLnk );
        return( TCL_ERROR );
    }
    _free( pRes );

    _free( (void*)dataSetReq.spec.CAMP_SPEC_u.pLnk );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varGet
 *
 *  Purpose:    Process "varGet*" Tcl commands
 *
 *              These commands are available to all variable types, although
 *              not meaningful for some.
 *
 *  Provides:   
 *              varGetIdent
 *              varGetPath
 *              varGetVarType
 *              varGetAttributes
 *              varGetTitle
 *              varGetHelp
 *              varGetStatus
 *              varGetStatusMsg
 *              varGetTimeLastSet
 *              varGetPollInterval
 *              varGetLogInterval
 *              varGetLogAction
 *              varGetAlarmAction
 *              varGetVal
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    int status;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable %s", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    switch( (VAR_GETPROCS)clientData )
    {
      case VAR_IDENT:
        Tcl_SetResult( interp, pVar->core.ident, TCL_VOLATILE );
        break;
      case VAR_PATH:
        Tcl_SetResult( interp, pVar->core.path, TCL_VOLATILE );
        break;
      case VAR_VARTYPE:
        sprintf( str, "%d", pVar->core.varType );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_ATTRIBUTES:
        sprintf( str, "%d", pVar->core.attributes );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_TITLE:
        Tcl_SetResult( interp, pVar->core.title, TCL_VOLATILE );
        break;
      case VAR_HELP:
        Tcl_SetResult( interp, pVar->core.help, TCL_VOLATILE );
        break;
      case VAR_STATUS:
        sprintf( str, "%d", pVar->core.status );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_STATUSMSG:
        Tcl_SetResult( interp, pVar->core.statusMsg, TCL_VOLATILE );
        break;
      case VAR_TIMELASTSET:
        sprintf( str, "%d", pVar->core.timeLastSet.tv_sec );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_POLLINTERVAL:
        sprintf( str, "%g", pVar->core.pollInterval );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_LOGINTERVAL:
        sprintf( str, "%g", pVar->core.logInterval );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        break;
      case VAR_LOGACTION:
        Tcl_SetResult( interp, pVar->core.logAction, TCL_VOLATILE );
        break;
      case VAR_ALARMACTION:
        Tcl_SetResult( interp, pVar->core.alarmAction, TCL_VOLATILE );
        break;
      case VAR_VAL:
        switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
        {
          case CAMP_VAR_TYPE_NUMERIC:
            varNumGetValStr( pVar, str );
/*            sprintf( str, "%g", pVar->spec.CAMP_SPEC_u.pNum->val );
*/
            Tcl_SetResult( interp, str, TCL_VOLATILE );
            break;

          case CAMP_VAR_TYPE_STRING:
            Tcl_SetResult( interp, pVar->spec.CAMP_SPEC_u.pStr->val, 
                           TCL_VOLATILE );
            break;

          case CAMP_VAR_TYPE_SELECTION:
/*
            status = varSelGetIDLabel( pVar, 
                pVar->spec.CAMP_SPEC_u.pSel->val, str );
            if( _failure( status ) )
            {
                Tcl_SetResult( interp, "bad selection value", TCL_STATIC );
                return( TCL_ERROR );
            }
*/
            sprintf( str, "%d", pVar->spec.CAMP_SPEC_u.pSel->val );
            Tcl_SetResult( interp, str, TCL_VOLATILE );
            break;

          default:
            Tcl_SetResult( interp, "var has no value", TCL_STATIC );
            return( TCL_ERROR );
            break;
        }
        break;
    }
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varNumGet
 *
 *  Purpose:    Process "varNumGet*" Tcl commands
 *
 *              These commands are only available for Numeric variable types.
 *              They return information specific to Numeric variables.
 *
 *  Provides:   
 *              varNumGetTimeStarted
 *              varNumGetNum
 *              varNumGetLow
 *              varNumGetHi
 *              varNumGetSum
 *              varNumGetSumSquares
 *              varNumGetSumCubes
 *              varNumGetSumOffset
 *              varNumGetTol
 *              varNumGetTolType
 *              varNumGetUnits
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varNumGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_NUMERIC* pNum;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }
    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != 
        CAMP_VAR_TYPE_NUMERIC )
    {
        Tcl_SetResult( interp, "bad vartype, expecting numeric", TCL_STATIC );
        return( TCL_ERROR );
    }

    pNum = pVar->spec.CAMP_SPEC_u.pNum;

    switch( (VAR_NUMGETPROCS)clientData )
    {
      case VAR_TIMESTARTED:
        sprintf( str, "%d", pNum->timeStarted.tv_sec );
        break;
      case VAR_NUM:
        sprintf( str, "%d", pNum->num );
        break;
      case VAR_LOW:
        numGetValStr( pVar->core.varType, pNum->low, str );
        break;
      case VAR_HI:
        numGetValStr( pVar->core.varType, pNum->hi, str );
        break;
      case VAR_SUM:
        numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sum, str );
        break;
      case VAR_SUMSQUARES:
        numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sumSquares, str );
        break;
      case VAR_SUMCUBES:
        numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sumCubes, str );
        break;
      case VAR_OFFSET:
        numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->offset, str );
        break;
      case VAR_TOL:
        sprintf( str, "%g", pNum->tol );
        break;
      case VAR_TOLTYPE:
        sprintf( str, "%d", pNum->tolType );
        break;
      case VAR_UNITS:
        sprintf( str, "%s", pNum->units );
        break;
    }

    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varSelGet
 *
 *  Purpose:    Process "varSelGet*" Tcl commands
 *
 *              These commands are only available for Selection variable types.
 *              They return information specific to Selection variables.
 *
 *  Provides:   
 *              varSelGetValLabel
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varSelGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_SELECTION* pSel;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid variable \"%s\"", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_SELECTION )
    {
        Tcl_SetResult( interp, "bad vartype, expecting selection", TCL_STATIC );
        return( TCL_ERROR );
    }

    pSel = pVar->spec.CAMP_SPEC_u.pSel;

    switch( (VAR_SELGETPROCS)clientData )
    {
      case SEL_VALLABEL:
        status = varSelGetIDLabel( pVar, pSel->val, str );
        if( _failure( status ) )
	  {
            Tcl_SetResult( interp, "failed getting selection value label", TCL_STATIC );
            return( TCL_ERROR );
	  }
        break;
    }

    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varArr
 *
 *  Purpose:    Process "varArr*" Tcl commands
 *
 *              These commands are only available for Array variable types.
 *              They return information specific to Array variables.
 *
 *  Provides:   
 *              varArrSetVal
 *              varArrResize
 *              varArrGetVal
 *              varArrGetVarType
 *              varArrGetElemSize
 *              varArrGetDim
 *              varArrGetDimSize
 *              varArrGetTotElem
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varArr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    CAMP_VAR* pVar;
    CAMP_ARRAY* pArr;
    char str[LEN_STR+1];
    char message[256];
    int argc2;
    char** argv2;
    int j;
    int dimInd[MAX_ARRAY_DIM];
    int elem;
    int ival;
    double fval;

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid variable \"%s\"", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_ARRAY )
    {
        Tcl_SetResult( interp, "bad vartype, expecting array", TCL_STATIC );
        return( TCL_ERROR );
    }

    pArr = pVar->spec.CAMP_SPEC_u.pArr;

    switch( (VAR_ARR_PROCS)clientData )
      {
      case ARR_SETVAL:
	sprintf( message, "usage: %s <var> <indexList> <val>", argv[0] );
	if( argc < 4 )
	  {
	    usage( message );
	    return( TCL_ERROR );
	  }

	status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
        if( status == TCL_ERROR )
	  {
	    return( TCL_ERROR );
	  }

	if( pArr->dim != argc2 )
	  {
            free( argv2 );
	    Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	    return( TCL_ERROR );
	  }

	elem = 0;
	for( j = 0; j < pArr->dim; j++ )
	  {
	    if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
	      {
		free( argv2 );
		return( TCL_ERROR );
	      }
            if( j != 0 ) elem *= pArr->dimSize[j];
            elem += dimInd[j];
	  }

	free( argv2 );

	switch( pArr->varType )
	  {
	  case CAMP_VAR_TYPE_INT:
	    if( Tcl_GetInt( interp, argv[3], &ival ) == TCL_ERROR )
	      {
		return( TCL_ERROR );
	      }
	    switch( pArr->elemSize )
	      {
	      case 1:
		((char*)pArr->pVal)[elem] = (char)ival;
		break;
	      case 2:
		((short*)pArr->pVal)[elem] = (short)ival;
		break;
	      case 4:
		((long*)pArr->pVal)[elem] = (long)ival;
		break;
	      }
	    break;
	  case CAMP_VAR_TYPE_FLOAT:
	    if( Tcl_GetDouble( interp, argv[3], &fval ) == TCL_ERROR )
	      {
		return( TCL_ERROR );
	      }
	    switch( pArr->elemSize )
	      {
	      case 4:
		((float*)pArr->pVal)[elem] = (float)fval;
		break;
	      case 8:
		((double*)pArr->pVal)[elem] = (double)fval;
		break;
	      }
	    break;
	  case CAMP_VAR_TYPE_STRING:
	    strncpy( &((pArr->pVal)[elem]), argv[3], pArr->elemSize );
	    break;
	  }

	return( TCL_OK );
	break;

      case ARR_RESIZE:
	sprintf( message, "usage: %s <var> <dimSizeList>", argv[0] );
	if( argc < 3 )
	  {
	    usage( message );
	    return( TCL_ERROR );
	  }

	status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
        if( status == TCL_ERROR )
	  {
	    return( TCL_ERROR );
	  }

	if( pArr->dim != argc2 )
	  {
            free( argv2 );
	    Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	    return( TCL_ERROR );
	  }

	for( j = 0; j < pArr->dim; j++ )
	  {
	    if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
	      {
		free( argv2 );
		return( TCL_ERROR );
	      }
	  }

	free( argv2 );

	pArr->totElem = pArr->dimSize[0] = dimInd[0];
	for( j = 1; j < pArr->dim; j++ )
	  {
	    pArr->totElem *= dimInd[j];
	    pArr->dimSize[j] = dimInd[j];
	  }

        pArr->pVal = realloc( pArr->pVal, pArr->totElem*pArr->elemSize );

        return( TCL_OK );
	break;

      case ARR_GETVAL:
	sprintf( message, "usage: %s <var> <indexList>", argv[0] );
	if( argc < 3 )
	  {
	    usage( message );
	    return( TCL_ERROR );
	  }

	status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
        if( status == TCL_ERROR )
	  {
	    return( TCL_ERROR );
	  }

	if( pArr->dim != argc2 )
	  {
            free( argv2 );
	    Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	    return( TCL_ERROR );
	  }

	elem = 0;
	for( j = 0; j < pArr->dim; j++ )
	  {
	    if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
	      {
		free( argv2 );
		return( TCL_ERROR );
	      }
            if( j != 0 ) elem *= pArr->dimSize[j];
            elem += dimInd[j];
	  }

	free( argv2 );

	switch( pArr->varType )
	  {
	  case CAMP_VAR_TYPE_INT:
	    switch( pArr->elemSize )
	      {
	      case 1:
		sprintf( str, "%d", ((char*)pArr->pVal)[elem] );
		break;
	      case 2:
		sprintf( str, "%d", ((short*)pArr->pVal)[elem] );
		break;
	      case 4:
		sprintf( str, "%d", ((long*)pArr->pVal)[elem] );
		break;
	      }
	    break;
	  case CAMP_VAR_TYPE_FLOAT:
	    switch( pArr->elemSize )
	      {
	      case 4:
		Tcl_PrintDouble( interp, ((float*)pArr->pVal)[elem], str );
		break;
	      case 8:
		Tcl_PrintDouble( interp, ((double*)pArr->pVal)[elem], str );
		break;
	      }
	    break;
	  case CAMP_VAR_TYPE_STRING:
	    strncpy( str, &((pArr->pVal)[elem]), pArr->elemSize );
	    str[pArr->elemSize] = '\0';
	    break;
	  }

	break;

      case ARR_GETVARTYPE:
	sprintf( str, "%d", pArr->varType );
	break;

      case ARR_GETELEMSIZE:
	sprintf( str, "%d", pArr->elemSize );
	break;

      case ARR_GETDIM:
	sprintf( str, "%d", pArr->dim );
	break;

      case ARR_GETDIMSIZE:
	sprintf( str, "%d", pArr->dimSize[0] );
	for( j = 1; j < pArr->dim; j++ )
	  {
	    sprintf( str, "%s %d", str, pArr->dimSize[j] );
	  }
	break;

      case ARR_GETTOTELEM:
	sprintf( str, "%d", pArr->totElem );
	break;
      }

    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varInsGet
 *
 *  Purpose:    Process "insGet*" Tcl commands
 *
 *              These commands are only available for Instrument variable
 *              types.  They return information specific to Instrument
 *              variables.
 *
 *  Provides:   
 *              insGetLockHost
 *              insGetLockPid
 *              insGetDefFile
 *              insGetIniFile
 *              insGetDataItemsLen
 *              insGetTypeIdent
 *              insGetInstance
 *              insGetClass
 *              insGetIfStatus
 *              insGetIfTypeIdent
 *              insGetIfDelay
 *              insGetIf
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varInsGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    switch( (VAR_INSGETPROCS)clientData )
    {
      case INS_LOCKHOST:
        strncpy( str, pIns->lockHost, LEN_STR );
        break;
      case INS_LOCKPID:
        sprintf( str, "%d", pIns->lockPID );
        break;
      case INS_DEFFILE:
        strncpy( str, pIns->defFile, LEN_STR );
        break;
      case INS_INIFILE:
        strncpy( str, pIns->iniFile, LEN_STR );
        break;
      case INS_DATAITEMS_LEN:
        sprintf( str, "%d", pIns->dataItems_len );
        break;
      case INS_TYPEIDENT:
        strncpy( str, pIns->typeIdent, LEN_STR );
        break;
      case INS_TYPEINSTANCE:
        sprintf( str, "%d", pIns->typeInstance );
        break;
      case INS_LEVELCLASS:
        sprintf( str, "%d", pIns->class );
        break;
      case INS_IF_STATUS:
        if( pIns->pIF == NULL )
        {
            Tcl_SetResult( interp, "interface undefined", TCL_STATIC );
            return( TCL_ERROR );
        }
        else
        {
            sprintf( str, "%d", pIns->pIF->status );
        }
        break;

      case INS_IF_TYPEIDENT:
        if( pIns->pIF == NULL )
        {
            Tcl_SetResult( interp, "interface undefined", TCL_STATIC );
            return( TCL_ERROR );
        }
        else
        {
            strncpy( str, pIns->pIF->typeIdent, LEN_STR );
        }
        break;

      case INS_IF_DELAY:
        if( pIns->pIF == NULL )
        {
            Tcl_SetResult( interp, "interface undefined", TCL_STATIC );
            return( TCL_ERROR );
        }
        else
        {
            sprintf( str, "%f", pIns->pIF->accessDelay );
        }
        break;

      case INS_IF:
        if( pIns->pIF == NULL )
        {
            Tcl_SetResult( interp, "interface undefined", TCL_STATIC );
            return( TCL_ERROR );
        }
        else
        {
            strncpy( str, pIns->pIF->defn, LEN_STR );
        }
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varGetIfRs232
 *
 *  Purpose:    Process "insGetIfRs232*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              an RS232 interface defined.  They return information
 *              specific to an RS232 interface definition.
 *
 *  Provides:   
 *              insGetIfRs232Name
 *              insGetIfRs232Baud
 *              insGetIfRs232Data
 *              insGetIfRs232Parity
 *              insGetIfRs232Stop
 *              insGetIfRs232ReadTerm
 *              insGetIfRs232WriteTerm
 *              insGetIfRs232Timeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfRs232( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    
    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_RS232 )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_RS232_NAME:
	camp_getIfRs232Port( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_RS232_BAUD:
        sprintf( str, "%d", camp_getIfRs232Baud( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_RS232_DATABITS:
        sprintf( str, "%d", camp_getIfRs232Data( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_RS232_PARITY:
	camp_getIfRs232Parity( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_RS232_STOPBITS:
        sprintf( str, "%d", camp_getIfRs232Stop( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_RS232_READTERM:
	camp_getIfRs232ReadTerm( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_RS232_WRITETERM:
	camp_getIfRs232WriteTerm( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_RS232_READTIMEOUT:
        sprintf( str, "%d", camp_getIfRs232Timeout( pIns->pIF->defn ) );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varGetIfGpib
 *
 *  Purpose:    Process "insGetIfGpib*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a GPIB interface defined.  They return information
 *              specific to a GPIB interface definition.
 *
 *  Provides:   
 *              insGetIfGpibAddr
 *              insGetIfGpibReadTerm
 *              insGetIfGpibWriteTerm
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfGpib( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_GPIB )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_GPIB_ADDR:
        sprintf( str, "%d", camp_getIfGpibAddr( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_GPIB_READTERM:
    	camp_getIfGpibReadTerm( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_GPIB_WRITETERM:
        camp_getIfGpibWriteTerm( pIns->pIF->defn, str );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varGetIfCamac
 *
 *  Purpose:    Process "insGetIfCamac*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a CAMAC interface defined.  They return information
 *              specific to a CAMAC interface definition.
 *
 *  Provides:   
 *              insGetIfCamacB
 *              insGetIfCamacC
 *              insGetIfCamacN
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfCamac( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_CAMAC )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_CAMAC_B:
        sprintf( str, "%d", camp_getIfCamacB( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_CAMAC_C:
        sprintf( str, "%d", camp_getIfCamacC( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_CAMAC_N:
        sprintf( str, "%d", camp_getIfCamacN( pIns->pIF->defn ) );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}

/*
 *  Name:       camp_tcl_varGetIfVme
 *
 *  Purpose:    Process "insGetIfVme*" Tcl command(s)
 *
 *              Return information specific to a VME interface definition.
 *              (Only one command yet.)
 *
 *  Provides:   
 *              insGetIfVmeBase
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history: 
 *              10-Nov-2003       DA     Created
 *
 */
int
camp_tcl_varGetIfVme( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_VME )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_VME_BASE:
        sprintf( str, "%d", camp_getIfVmeBase( pIns->pIF->defn ) );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_varGetIfTcpip
 *
 *  Purpose:    Process "insGetIfTcpip*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              an TCPIP interface defined.  They return information
 *              specific to an TCPIP interface definition.
 *
 *  Provides:   
 *              insGetIfTcpipAddress
 *              insGetIfTcpipPort
 *              insGetIfTcpipReadTerm
 *              insGetIfTcpipWriteTerm
 *              insGetIfTcpipTimeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *              17-Mar-2006   DA   Created
 *
 */
int
camp_tcl_varGetIfTcpip( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    
    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_TCPIP )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_TCPIP_ADDRESS:
	camp_getIfTcpipAddr( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_TCPIP_PORT:
        sprintf( str, "%d", camp_getIfTcpipPort( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_TCPIP_READTERM:
	camp_getIfTcpipReadTerm( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_TCPIP_WRITETERM:
	camp_getIfTcpipWriteTerm( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_TCPIP_TIMEOUT:
        sprintf( str, "%.1f", camp_getIfTcpipTimeout( pIns->pIF->defn ) );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}

/*
 *  Name:       camp_tcl_varGetIfIndpak
 *
 *  Purpose:    Process "insGetIfIndpak*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a Industry Pack (Indpak) interface defined.  They return information
 *              specific to a Indpak interface definition.
 *
 *  Provides:   
 *              insGetIfIndpakSlot
 *              insGetIfIndpakType
 *              insGetIfIndpakChannel
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfIndpak( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetp( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
        return( TCL_ERROR );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( pIns->pIF == NULL )
    {
        Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
        return( TCL_ERROR );
    }

    if( pIns->pIF->typeID != CAMP_IF_TYPE_INDPAK )
    {
        Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
        return( TCL_ERROR );
    }

    switch( (VAR_GETIFPROCS)clientData )
    {
      case INS_CAMP_IF_INDPAK_SLOT:
        sprintf( str, "%d", camp_getIfIndpakSlot( pIns->pIF->defn ) );
        break;
      case INS_CAMP_IF_INDPAK_TYPE:
        camp_getIfIndpakType( pIns->pIF->defn, str );
        break;
      case INS_CAMP_IF_INDPAK_CHANNEL:
        sprintf( str, "%d", camp_getIfIndpakChannel( pIns->pIF->defn ) );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_lnkGet
 *
 *  Purpose:    Process "lnkGet*" Tcl commands
 *
 *              These routines are available for Link variable types only.
 *              They return information specific to Link variables.
 *
 *  Provides:   
 *              lnkGetVarType
 *              lnkGetPath
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_lnkGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    CAMP_LINK* pLnk;
    char str[LEN_STR+1];
    char message[256];

    sprintf( message, "usage: %s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    pVar = camp_varGetTrueP( argv[1] );
    if( pVar == NULL )
    {
        sprintf( str, "invalid: variable (%s)", argv[1] );
        Tcl_SetResult( interp, str, TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_LINK )
    {
        Tcl_SetResult( interp, "bad vartype, expecting link", TCL_STATIC );
        return( TCL_ERROR );
    }

    pLnk = pVar->spec.CAMP_SPEC_u.pLnk;

    switch( (VAR_GETLNKPROCS)clientData )
    {
      case LNK_VARTYPE:
        sprintf( str, "%d", pLnk->varType );
        break;
      case LNK_PATH:
        strncpy( str, pLnk->path, LEN_STR );
        break;
    }
    Tcl_SetResult( interp, str, TCL_VOLATILE );
    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_msg
 *
 *  Purpose:    Process "msg" Tcl command
 *
 *              This command causes the CAMP server to log a string message.
 *
 *  Provides:   
 *              msg
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_msg( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[256];

    sprintf( message, "usage: %s <msg>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    camp_logMsg( argv[1] );
 
    sprintf( message, "Message '%s' logged", argv[1] );
    Tcl_SetResult( interp, message, TCL_VOLATILE );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_sleep
 *
 *  Purpose:    Process "sleep" Tcl command
 *
 *              This command causes the executing process (or thread)
 *              to delay execution for a specified time (floating point
 *              parameter).
 *
 *              NOTE:  this command sends the current thread to sleep
 *              for the specified (floating-point) time.  By default,
 *              the other threads (if multithreaded) are allowed to
 *              proceed.  Using the "lock" flag, however, turns the
 *              global lock on, so that no other threads may execute
 *              while the current thread is sleeping.
 *
 *              CAUTION:  turning the global lock on for long periods
 *              of time (i.e., seconds) causes unpredictable problems
 *              in both the VAX/VMS and VxWorks versions.
 *
 *  Provides:   
 *              sleep
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_sleep( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    double delay;
    char* endptr = NULL;
    char message[256];
    bool_t lockOff = TRUE;

    sprintf( message, "usage: %s <float> [lock]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    if( ( argc > 2 ) && ( streq( argv[2], "lock" ) ) )
    {
        lockOff = FALSE;
    }

    delay = strtod( argv[1], &endptr );
    if( isgraph( *endptr ) )
    {
        usage( message );
        return( TCL_ERROR );
    }

#ifdef MULTITHREADED
    if( lockOff ) thread_unlock_global_np();
#endif /* MULTITHREADED */

    camp_fsleep( delay );

#ifdef MULTITHREADED
    if( lockOff ) thread_lock_global_np();
#endif /* MULTITHREADED */

    sprintf( message, "Server slept for %f seconds", delay );
    Tcl_SetResult( interp, message, TCL_VOLATILE );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_gettime
 *
 *  Purpose:    Process "gettime" Tcl command
 *
 *              This command causes the server to query the computer
 *              system time and report the time back as a floating point
 *              number of seconds since Jan. 1, 1970 (the default time
 *              zero for C libraries).
 *
 *  Provides:
 *              gettime
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_gettime( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char buf[128];
    timeval_t tv;
    double dt;

    gettimeval( &tv );
    dt = tv.tv_sec + tv.tv_usec/1e6;
    sprintf( buf, "%.6f", dt );
    Tcl_SetResult( interp, buf, TCL_VOLATILE );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_camac_cdreg
 *
 *  Purpose:    Process "cdreg" Tcl command
 *
 *              Calls a CAMAC cdreg command.
 *
 *  Provides:
 *              cdreg
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The "ext" result of the command is set in the variable
 *              passed in the ext_var parameter.
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_cdreg( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    long ext;
    long b, c, n, a;
    char message[256];
    char* endptr;

    switch( (CAMAC_PROCS)clientData )
    {
      case CAMAC_CDREG:
        sprintf( message, "usage: %s <ext_var> <b> <c> <n> <a>", argv[0] );
        if( argc < 6 )
        {
            usage( message );
            return( TCL_ERROR );
        }

        b = strtol( argv[2], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        c = strtol( argv[3], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        n = strtol( argv[4], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        a = strtol( argv[5], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

#if CAMP_DEBUG
	if( camp_debug > 0 ) printf( "calling cdreg..." );
#endif /* CAMP_DEBUG */
#ifdef MULTITHREADED
	thread_unlock_global_np();
#endif /* MULTITHREADED */

	status = TCL_OK;
        if( !cdreg( &ext, &b, &c, &n, &a ) ) status = TCL_ERROR;

#ifdef MULTITHREADED
	thread_lock_global_np();
#endif /* MULTITHREADED */
#if CAMP_DEBUG
	if( camp_debug > 0 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

	if( status == TCL_ERROR ) 
	  {
	    Tcl_SetResult( interp, "Failed cdreg, check connections to the CAMAC crate", TCL_STATIC );
	    return( status );
	  }

        Tcl_SetVar( interp, argv[1], itoa( ext ), 0 );

        break;
    }

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_camac_single
 *
 *  Purpose:    Process CAMAC single data Tcl commands
 *
 *              Calls a CAMAC cfsa or cssa command.
 *
 *  Provides:
 *              cfsa
 *              cssa
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The resulting "q" is set in the Tcl variable passed as the
 *              q_var parameter.  For read operations, the resulting data
 *              is set in the Tcl variable passed as the data_var parameter.
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_single( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    long ext;
    long f;
    u_long data = 0;
    u_long data32 = 0;
    u_short data16 = 0;
    char q = 0;
    char* p;
    char message[256];
    char* endptr;
    
    sprintf( message, "usage: %s <f> <ext> <data_var> <q_var>", argv[0] );
    if( argc < 5 )
      {
	usage( message );
	return( TCL_ERROR );
      }
    
    f = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
      {
	usage( message );
	return( TCL_ERROR );
      }
    
    ext = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
      {
	usage( message );
	return( TCL_ERROR );
      }
    
    if( ( f >= 16 ) && ( f <= 23 ) )
      {
	if( ( p = Tcl_GetVar( interp, argv[3], 0 ) ) == NULL )
	  {
	    Tcl_AppendResult( interp, "no such variable \"", 
			     argv[3], "\"", NULL );
	    return( TCL_ERROR );
	  }
	
	data = strtol( p, &endptr, 0 );
	if( isgraph( *endptr ) )
	  {
	    usage( message );
	    return( TCL_ERROR );
	  }
      }
    
#if CAMP_DEBUG
    if( camp_debug > 0 ) printf( "calling cfsa/cssa..." );
#endif /* CAMP_DEBUG */
#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    status = TCL_OK;

    switch( (CAMAC_PROCS)clientData )
      {
      case CAMAC_CFSA: 
	data32 = data; /* if it's a write */
	if( !cfsa( &f, &ext, &data32, &q ) ) status = TCL_ERROR;
	data = data32; /* if it's a read */
	break;
      case CAMAC_CSSA: 
	data16 = data;
	if( !cssa( &f, &ext, &data16, &q ) ) status = TCL_ERROR;
	data = data16;
	break;
      }

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */
#if CAMP_DEBUG
    if( camp_debug > 0 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

    if( status == TCL_ERROR ) 
      {
	Tcl_SetResult( interp, "Failed camac data function", TCL_STATIC );
	return( status );
      }

    Tcl_SetVar( interp, argv[4], itoa( (int)q ), 0 );
    
    if( ( f >= 0 ) && ( f <= 7 ) )
      {
	Tcl_SetVar( interp, argv[3], itoa( data ), 0 );
      }

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_camac_block
 *
 *  Purpose:    Process CAMAC block data Tcl commands
 *
 *              Calls a CAMAC command.
 *
 *  Provides:
 *              cfubc
 *              cfubr
 *              csubc
 *              csubr
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              The data_var parameter should refer to a CAMP array variable
 *              which will be used to maintain the data of block transfers
 *              (reading or writing).
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The tally_var parameter will be set to the number of
 *              transfers tallied by the call.  The result of a block read
 *              will be set in the data_var variable which must refer to
 *              a valid CAMP array variable.
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_block( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    long ext;
    long f;
    char* pData;
    char message[256];
    char* endptr;
    CAMAC_CB cb;
    CAMP_VAR* pVar;

    sprintf( message, "usage: %s <f> <ext> <data_var> <count> <tally_var> <lam_ident> <channel>", argv[0] );
    if( argc < 8 )
    {
        usage( message );
        return( TCL_ERROR );
    }

        f = strtol( argv[1], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        ext = strtol( argv[2], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        /*
         *  Check this size (are array variables dynamic?)
         */
        cb.count = (short)strtol( argv[4], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        cb.lam_ident = (short)strtol( argv[6], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        cb.channel = (short)strtol( argv[7], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            return( TCL_ERROR );
        }

        if( ( ( f >= 16 ) && ( f <= 23 ) ) ||
            ( ( f >= 0 ) && ( f <= 7 ) ) )
        {
            /*
	     *  Set pData to CAMP array variable
	     */
            pVar = camp_varGetp( argv[3] );
            if( pVar == NULL )
            {
                Tcl_AppendResult( interp, "no such CAMP variable \"", 
                                  argv[3], "\"", NULL );
                return( TCL_ERROR );
	    }

            pData = pVar->spec.CAMP_SPEC_u.pArr->pVal;
        }
        else
        {
            /*
	     *  Not read or write
	     */
	    pData = NULL;
	}

#if CAMP_DEBUG
	if( camp_debug > 0 ) printf( "calling camac block data routine..." );
#endif /* CAMP_DEBUG */
#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */

    status = TCL_OK;

    switch( (CAMAC_PROCS)clientData )
      {
      case CAMAC_CFUBC: 
	if( !cfubc( &f, &ext, (long*)pData, &cb ) ) status = TCL_ERROR;
	break;
      case CAMAC_CFUBR: 
	if( !cfubr( &f, &ext, (long*)pData, &cb ) ) status = TCL_ERROR;
	break;
      case CAMAC_CSUBC: 
	if( !csubc( &f, &ext, (short*)pData, &cb ) ) status = TCL_ERROR;
	break;
      case CAMAC_CSUBR: 
	if( !csubr( &f, &ext, (short*)pData, &cb ) ) status = TCL_ERROR;
	break;
      }

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */
#if CAMP_DEBUG
    if( camp_debug > 0 ) printf( "done\n" );
#endif /* CAMP_DEBUG */

    if( status == TCL_ERROR ) 
      {
	Tcl_SetResult( interp, "Failed camac block data function", TCL_STATIC );
	return( status );
      }

    Tcl_SetVar( interp, argv[5], itoa( (int)cb.tally ), 0 );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_vme_io
 *
 *  Purpose:    Process vme reads and writes
 *
 *  Provides:
 *              vmeRead
 *              vmeWrite
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters. Tcl args:
 *              vmeWrite: base_address offset_address data_type value
 *              vmeread:  base_address offset_address data_type ?buffer_size?
 *              (An omitted buffer size defaults to 16, which is more than
 *              enough for any numerics.)  Note that there are no floating-
 *              point number data types.
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status.  Message returns value string or error message.
 *
 *  Revision history:
 *              10-Nov-2003     DA     Created
 *
 */
int
camp_tcl_vme_io( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    VME_IO_CODE   status;
    void* base;
    int   offset;
    int   nreq;
    char  dtype[16] = { 0 };
    int*  typetok;
    char  message[256];
    char* endptr;
    char* buff = 0;
    long  buffsize = 0;
    VME_IO_CODE stat;
    
    if ( VME_READ == (VME_PROCS)clientData )
      {
        sprintf( message, "usage: %s <base_addr> <addr_offset> <data_type> ?<buffer_size>?", argv[0] );
        nreq = 4;
      }
    else
      {
        sprintf( message, "usage: %s <base_addr> <addr_offset> <data_type> <value>", argv[0] );
        nreq = 5;
      }

    if( argc < nreq )
      goto vmeioerr;

    base = (void*)strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) || base == 0 )
      goto vmeioerr;

    offset = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
      goto vmeioerr;

    strncpy( dtype, argv[3], 15 );

    switch( (VME_PROCS)clientData ) 
      {
      case VME_READ:
        if( argc < 5 && strncmp( argv[3],"string", 4 ) ) 
          { /* read: not a string, no buffer specified  --  use 16 */
            buffsize = 16;
          }
        else
          {
            buffsize = 0;
            if( argc < 5 ) 
              goto vmeioerr;
            else
              {
                buffsize = strtol( argv[4], &endptr, 0 );
                if( isgraph( *endptr ) ) 
                  goto vmeioerr;
              }
          }
        buff = (char*) zalloc( buffsize+1 );

        stat = vmeRead( base, offset, (int*)dtype, buff, buffsize );

        switch (stat) {
        case VME_IO_UNKNOWN:
          sprintf( message, "Failed VME read: unknown data type \"%s\"", dtype );
          goto vmeioerr;
        case VME_IO_OVERFLOW:
          sprintf( message, "Failed VME read: data overflows buffer (%d)", buffsize );
          goto vmeioerr;
        case VME_IO_ACCESS_ERR:
          sprintf( message, "Failed VME read: access violation (bad address)" );
          goto vmeioerr;
        default:
          buff[buffsize] = '\0';
          Tcl_SetResult( interp, buff, TCL_VOLATILE );
          free( buff );
          return( TCL_OK );
        }
        break;

      case VME_WRITE:
        buffsize = strlen( argv[4] );

        stat = vmeWrite( base, offset, (int*)dtype, argv[4], buffsize );

        switch (stat) {
        case VME_IO_UNKNOWN:
          sprintf( message, "Failed VME write: unknown data type \"%s\"", dtype );
          goto vmeioerr;
        case VME_IO_OVERFLOW:
          sprintf( message, "Failed VME write: numeric overflow for type %s", dtype );
          goto vmeioerr;
        case VME_IO_INVAL_NUM:
          sprintf( message, "Failed VME write: invalid numeric value %.16s", dtype );
          goto vmeioerr;
        case VME_IO_ACCESS_ERR:
          sprintf( message, "Failed VME write: access violation (bad address)" );
          goto vmeioerr;
        default:
          buff[buffsize] = '\0';
          Tcl_SetResult( interp, buff, TCL_VOLATILE );
          free( buff );
          return( TCL_OK );
        }
        break;
      }

 vmeioerr:
    _free( buff );
    usage( message );
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_gpib_clear
 *
 *  Purpose:    Process general GPIB Tcl command
 *
 *              Called using command "gpibClear <GPIB address>"
 *
 *              Calls the function gpib_clear() which should be implemented for
 *              each GPIB interface as a high level GPIB SDC.  That is, it
 *              should set up the talker and listener properly, and then send
 *              an SDC to the listener.  See the MVIP300 implementation called
 *              dvclr() for an example.
 *
 *  Provides:
 *              gpibClear
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              The GPIB address is any valid GPIB bus address.  It does
 *              not have to refer to the address of any valid CAMP instrument.
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *    21-Dec-1999  TW  Creation
 *
 */
int
camp_tcl_gpib_clear( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int status;
    int addr;
    char message[256];
    char* endptr;

    sprintf( message, "usage: %s <GPIB addr>", argv[0] );
    if( argc < 2 )
      {
	usage( message );
	return( TCL_ERROR );
      }

    addr = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
      {
	usage( message );
	return( TCL_ERROR );
      }

    if( camp_gpib_clear( addr ) == CAMP_FAILURE ) return( TCL_ERROR );

    return( TCL_OK );
}


/*
 *  Name:       camp_tcl_gpib_timeout
 *
 *  Purpose:    Set the gpib interface timeout
 *
 *              Called using command "gpibTimeout <time>"
 *
 *              Calls the function camp_gpib_timeout() which should be implemented for
 *              each GPIB interface.  Currently only implemented for the MVIP300.
 *
 *  Provides:
 *              gpibTimeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              Integer timeout value in seconds
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *    13-Dec-2000  TW  Creation
 *
 */
int
camp_tcl_gpib_timeout( ClientData clientData, Tcl_Interp* interp,
                       int argc, char* argv[] )
{
    int status;
    int timeoutVal;
    char message[256];
    char* endptr;

    sprintf( message, "usage: %s <seconds>", argv[0] );
    if( argc < 2 )
      {
	usage( message );
	return( TCL_ERROR );
      }

    timeoutVal = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
      {
	usage( message );
	return( TCL_ERROR );
      }

    if( camp_gpib_timeout( timeoutVal ) == CAMP_FAILURE ) return( TCL_ERROR );

    return( TCL_OK );
}


#ifdef VXWORKS


/*
camp_tcl_tip850
Purpose:  Interfaces tcl calls to the TIP850 DAC/ADC module
Inputs:   clientData - indentifies type of call DAC or ADC
          interp - pointer to tcl interpreter
	  argc - number of arguments passed to function
	  argv - array or pointers to char arguments
	  argv[0] - function name
	  argv[1] - position of industry pack
	  argv[2] - channel number
	  argv[3] - data passed to or returned from function
Precond:  The tcl interpreter must be successfully made
Outputs:  int - returns success or failure of function TCL_OK or TCL_ERROR
          Other outputs are returned in argv[3]
Postcond: The DAC value may be changed
Note:     It is unclear what some of the tests and error returns are for.
          These functions were copied from other undocumented code.
          They do not cause problems, and may be useful.
*/



int camp_tcl_tip850(ClientData clientData, Tcl_Interp * interp,
		    int argc, char * argv[])
{
  TIP850_DEV * tip850Dev;
  int iPosition;
  int iChannel;
  char message[256];
  char * endptr;
  char * pcData;
  int iData = 0;
  double dData;

  sprintf(message, "usage: %s <position> <channel> <data>", argv[0]);
  if (argc < 4)
  {
    usage(message);
    return(TCL_ERROR);
  }

  /* Extract the position type and channel from the char array */

  iPosition = strtol(argv[1], &endptr, 0);
  iChannel = strtol(argv[2], &endptr, 0);

  /* select the function requested by caller
     Get the device pointer from the position and channel data. Determine the type from
     the clientData passed to routine. Write argv[3] or read and place in argv[3] */

  switch((DACADC_PROCS) clientData)
  {
    case DAC_SET: 
      tip850Dev = tip850GetChannel(iPosition, TIP850_DAC, iChannel);

      if ((pcData = Tcl_GetVar( interp, argv[3], 0)) == NULL)
      {
	Tcl_AppendResult( interp, "no such variable \"", argv[3], "\"", NULL );
	return( TCL_ERROR );
      }
	
      iData = (int) strtol(pcData, &endptr, 0);
      if (isgraph(*endptr))
      {
	usage( message );
	return( TCL_ERROR );
      }

      dData = (double) iData;
      tip850Ioctl(tip850Dev, TIP850_WRITE, &dData);
      break;

    case DAC_READ:
      tip850Dev = tip850GetChannel(iPosition, TIP850_DAC, iChannel);

      if (tip850Dev == NULL)
	return(TCL_ERROR);

      tip850Ioctl(tip850Dev, TIP850_READ, &dData);
      iData = (int) dData;
      Tcl_SetVar(interp, argv[3], itoa(iData), 0);
      break;

    case ADC_READ:
      tip850Dev = tip850GetChannel(iPosition, TIP850_ADC, iChannel);
      if (tip850Dev == NULL)
	return (TCL_ERROR);

      tip850Ioctl(tip850Dev, TIP850_READ, &dData);
      iData = (int) dData;
      Tcl_SetVar(interp, argv[3], itoa(iData), 0);
      break;

    case GAIN_SET: 
      tip850Dev = tip850GetChannel(iPosition, TIP850_ADC, iChannel);

      if ((pcData = Tcl_GetVar( interp, argv[3], 0)) == NULL)
      {
	Tcl_AppendResult( interp, "no such variable \"", argv[3], "\"", NULL );
	return( TCL_ERROR );
      }
	
      iData = (int) strtol(pcData, &endptr, 0);
      if (isgraph(*endptr))
      {
	usage( message );
	return( TCL_ERROR );
      }

      dData = (double) iData;
      tip850Ioctl(tip850Dev, TIP850_GAIN, &dData);
      break;
  }

  return(TCL_OK);
}



/*
camp_tcl_te28
Purpose:  Interfaces tcl calls to the TE28 Servo Motor module
Inputs:   clientData - indentifies type of call 
          interp - pointer to tcl interpreter
	  argc - number of arguments passed to function
	  argv - array or pointers to char arguments
	  argv[0] - function name
	  argv[1] - position of industry pack
	  argv[2] - channel number
	  argv[3] - data passed to or returned from function
Precond:  The tcl interpreter must be successfully made
Outputs:  int - returns success or failure of function TCL_OK or TCL_ERROR
          Other outputs are returned in argv[3]
Postcond: The DAC value may be changed
Note:     It is unclear what some of the tests and error returns are for.
          These functions were copied from other undocumented code.
          They do not cause problems, and may be useful.
*/


int camp_tcl_te28(ClientData clientData, Tcl_Interp * interp,
		    int argc, char * argv[])
{
  TE28_DEV * te28Dev;
  int iPosition;
  int iChannel;
  char message[256];
  char * endptr;
  char * pcData;
  long lValue = 0;
  long llim = 0;
  int iTemp;
  double dValue;

  sprintf(message, "usage: %s <position> <channel> <data>", argv[0]);
  if (argc < 4)
  {
    usage(message);
    return(TCL_ERROR);
  }

  /* Extract the position and channel from the char array and get pointer*/

  iPosition = strtol(argv[1], &endptr, 0);
  iChannel = strtol(argv[2], &endptr, 0);
  te28Dev = te28GetChannel(iPosition, iChannel);
  if (te28Dev == NULL)
    return (TCL_ERROR);

  /* select the function requested by caller
     Write value argv[3] or read and place result in argv[3] */

  switch((MOTOR_PROCS) clientData)
  {
    case MOTOR_RESET:
      te28Ioctl(te28Dev, TE28_RESET_AXIS, lValue);
      te28Set(te28Dev, TE28_ENABLE, 1);	 /* make sure the motor is enabled */
      te28Set(te28Dev, TE28_LIMITS, 0, 0);	/* Ignore default limit state with no connection */
      te28Set(te28Dev, TE28_LIMITS_ENABLE, 1);  /* Enable them but above makes them always not hit */
      break;

    case MOTOR_ENCODER:
      lValue = strtol(argv[3], &endptr, 0);
      if (isgraph(*endptr))
      {
	usage(message);
	return(TCL_ERROR);
      }
      iTemp = TE28_ENCODER;
      te28Set(te28Dev, iTemp, lValue);
      break;

    case MOTOR_LIMIT:
      /*  0 to disable, neg for active low, pos for active high */
      lValue = strtol(argv[3], &endptr, 0);
      if (isgraph(*endptr))
      {
	usage(message);
	return(TCL_ERROR);
      }
      llim = (lValue == 0 ? 0 : 1 );
      iTemp = TE28_LIMITS_ENABLE;
      te28Set(te28Dev, iTemp, llim);

      if ( llim == 0 ) break;

      llim = (lValue > 0 ? 1 : 0 );
      iTemp = TE28_LIMITS;
      te28Set(te28Dev, iTemp, llim);
      break;

    case MOTOR_FILTER_KP:
    case MOTOR_FILTER_KI:
    case MOTOR_FILTER_KD:
    case MOTOR_FILTER_IL:
    case MOTOR_FILTER_SI:
      lValue = strtol(argv[3], &endptr, 0);
      if (isgraph(*endptr))
      {
	usage(message);
	return(TCL_ERROR);
      }

      switch((MOTOR_PROCS) clientData)
      {
	case MOTOR_FILTER_KP:
	  iTemp = TE28_FILTER_KP;
	  break;
	case MOTOR_FILTER_KI:
	  iTemp = TE28_FILTER_KI;
	  break;
	case MOTOR_FILTER_KD:
	  iTemp = TE28_FILTER_KD;
	  break;
	case MOTOR_FILTER_IL:
	  iTemp = TE28_FILTER_IL;
	  break;
	case MOTOR_FILTER_SI:
	  iTemp = TE28_FILTER_SI;
	  break;
      }

      te28Ioctl(te28Dev, iTemp, lValue);
      break;

    case MOTOR_STOP:
      te28Ioctl(te28Dev, TE28_STOP_MOVE, lValue);
      break;
      
    case MOTOR_ABORT:
      te28Ioctl(te28Dev, TE28_ABORT_MOVE, lValue);
      break;

    case MOTOR_VEL:
    case MOTOR_ACC:
    case MOTOR_SLOPE:
    case MOTOR_OFFSET:
    case MOTOR_MOVE:
      dValue = strtod(argv[3], &endptr);
      if (isgraph(*endptr))
      {
	usage(message);
	return(TCL_ERROR);
      }
      switch((MOTOR_PROCS) clientData)
      {
        case MOTOR_VEL:
	  iTemp = TE28_VELOCITY;
	  break;
        case MOTOR_ACC:
	  iTemp = TE28_ACCELERATION;
	  break;
        case MOTOR_SLOPE:
	  iTemp = TE28_SLOPE;
	  break;
        case MOTOR_OFFSET:
	  iTemp = TE28_OFFSET;
	  break;
	case MOTOR_MOVE:
	  iTemp = TE28_MOVE;
	  break;
      }
      te28Set(te28Dev, iTemp, dValue);
      break;

    case MOTOR_POSITION:
      te28Get(te28Dev, TE28_POSITION, &dValue);

      sprintf(message, "%f", dValue);
      Tcl_SetVar(interp, argv[3], message, 0);
      break;
  }
  return(TCL_OK);
}


#endif /* VXWORKS */


int
camp_tcl_alarmTdMusr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int flag;
    int b;
    int c;
    int cbsw_m_mon;
    long have_camac;
    CAMP_IF_t* pIF_t;
    char message[256];
    char* endptr;

#if defined( VMS )
    sprintf( message, "usage: %s <flag> <b> <c> <cbsw>", argv[0] );
    if( argc < 5 )
    {
        usage( message );
        return( TCL_ERROR );
    }

    /*
     *  Test whether CAMAC is online
     */
    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL )
    {
        have_camac = 0;
    }
    else
    {
    	have_camac = pIF_t->priv;
    }

    flag = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        return( TCL_ERROR );
    }

    b = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        return( TCL_ERROR );
    }

    c = strtol( argv[3], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        return( TCL_ERROR );
    }

    cbsw_m_mon = strtol( argv[4], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        return( TCL_ERROR );
    }

    if( _failure( cb_log_utils( &have_camac, &b, &c ) ) )
    {
        camp_appendMsg( "alarmTdMusr: TD not halted (failed init cb)" );
        return( TCL_ERROR );
    }

    if( flag )
    {
        if( _failure( cb_log_set( &cbsw_m_mon ) ) )
	{
            camp_appendMsg( "alarmTdMusr: TD not halted (failed set cb)");
            return( TCL_ERROR );
	}
    }
    else
    {
        if( _failure( cb_log_clr( &cbsw_m_mon ) ) )
	{
            camp_appendMsg( "alarmTdMusr: TD not resumed (failed clear cb)");
            return( TCL_ERROR );
	}
    }
#elif defined( VXWORKS )
    /*
     *  Not implemented for VxWorks
     */
#else 
    /*
     *  Not implemented
     */
#endif /* VMS */

    return( TCL_OK );
}


int
camp_tcl_alarmIMusr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    /*
     *  Not implemented
     */

    return( TCL_OK );
}


static void  
camp_tcl_statmsg( Tcl_Interp *interp, int stat )
{
    char* msg;
    char cstat[32];

    switch( stat )
      {
      case CAMP_FAILURE:
        msg = "operation failed";
        break;
      case CAMP_INVAL_VAR:
        msg = "no such variable";
        break;
      case CAMP_INVAL_VAR_TYPE:
        msg = "invalid variable type for operation";
        break;
      case CAMP_INVAL_VAR_ATTR:
        msg = "invalid variable attribute";
        break;
      case CAMP_INVAL_INS_TYPE:
        msg = "invalid instrument type";
        break;
      case CAMP_INVAL_IF:
        msg = "undefined interface";
        break;
      case CAMP_INVAL_SELECTION:
        msg = "invalid selection index";
        break;
      default:
        sprintf( cstat, "failure (status: %u)", stat );
        msg = cstat;
        break;
    }

    Tcl_AppendResult( interp, msg, (char*)NULL );
}
