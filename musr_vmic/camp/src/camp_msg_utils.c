/*
 *  Name:       camp_msg_utils.c
 *
 *  Purpose:    Routines to maintain a global message string
 *
 *              The global message string is used to concatenate messages
 *              regarding the state of a call to the CAMP server.
 *              The global message is also used more generally by the
 *              CAMP Tcl interpreter to contain the result of certain
 *              operations (e.g., ASCII readings from instruments).
 *
 *              For a multithreaded server global messages are implemented
 *              as a separate message in each thread's private structure
 *              (see get_thread_msg and set_thread_msg in camp_srv_svc_mod.c).
 *
 *              This facility has no relation to camp_msg_priv.c
 *
 *  Revision history:
 *    20-Dec-1999  TW  Want non-printable characters for binary dumps
 *                     from some instruments
 *                     WARNING:  the above revision could have unforeseen
 *                     implications for some instruments.  The global message
 *                     string is used to contain the results of ASCII readings
 *                     from instruments as noted above.  Some instruments
 *                     are known to return non-printable characters as part
 *                     of a normal string response.  These characters could
 *                     interfere with string interpretation in the Tcl
 *                     interpreter.  The stoprint routine was used to remove
 *                     the chance of such misinterpretation.
 *    14-Dec-2000  TW  Replace vsprintf with homemade format interpretation.
 *                     Less versatile, and a little dangerous if %f
 *                     doesn't correspond to a 'double' type (etc.), but
 *                     allows us to check whether there is actually enough
 *                     room in the string for the printed arguments.
 *                     This was causing memory access violations for very large
 *                     messages.
 *
 */

#include <stdio.h>
#include <stdarg.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */
#ifdef VMS
#include <starlet.h>
#include <lib$routines.h>
#endif /* VMS */

#if !defined( RPC_SERVER ) || !defined( MULTITHREADED )
static char current_msg[MAX_LEN_MSG+1] = "";
#define get_thread_msg() current_msg
#define set_thread_msg(msg) strncpy( current_msg, msg, MAX_LEN_MSG )
#endif /* !RPC_SERVER || !MULTITHREADED */


char*
msg_getHelpString( MSG_TYPE type )
{
    switch( type )
    {
      case CAMP_INVAL_INS_TYPE:
        return( "error: invalid instrument type \"%s\"" );
      case CAMP_INVAL_IF:
        return( "error: interface undefined for instrument \"%s\"" );
      case CAMP_INVAL_IF_TYPE:
        return( "error: invalid interface type \"%s\"" );
      case CAMP_INVAL_PARENT:
        return( "error: couldn't find instrument for var \"%s\"" );
      case CAMP_INVAL_VAR:
        return( "error: variable \"%s\" doesn't exist" );
      case CAMP_INVAL_VAR_TYPE:
        return( "error: wrong variable type \"%s\"" );
      case CAMP_INS_LOCKED:
	return( "failure: instrument \"%s\" is locked" );
      case CAMP_NOT_CONN:
	return( "failure: interface not online \"%s\"" );
    }
}


/*
 *  Return a pointer to the current message string
 */
char*
camp_getMsg( void )
{
    return( get_thread_msg() );
}


/*
 *  Test whether there is a non-empty message string
 */
bool_t
camp_isMsg( void )
{
    char* msg = camp_getMsg();

    if( ( msg != NULL ) && ( strlen( msg ) > 0 ) ) return( TRUE );
    else return( FALSE );
}


#define declare_add_msg() \
  char *add_msg = (char*)NULL, *pAdd_msg; int len_add_msg, max_len_add_msg;

#define free_add_msg() \
  if( add_msg != (char*)NULL ){ free( add_msg ); add_msg = (char*)NULL; }

#define init_add_msg() \
  len_add_msg = 0;\
  max_len_add_msg = MAX_LEN_MSG;\
  add_msg = (char*)malloc( max_len_add_msg+1 );\
  pAdd_msg = add_msg+len_add_msg;\
  *pAdd_msg = '\0';

#define add_to_add_msg( add, len ) \
  while( len_add_msg+(len) > max_len_add_msg )\
  {\
     max_len_add_msg += MAX_LEN_MSG;\
     add_msg = realloc( add_msg, max_len_add_msg+1 );\
     pAdd_msg = add_msg+len_add_msg;\
  }\
  bcopy( (add), pAdd_msg, (len) );\
  len_add_msg += (len);\
  pAdd_msg = add_msg+len_add_msg;\
  *pAdd_msg = '\0';

/*
 *  Reset the global message string to a new value
 *
 *  The parameters are variable in number - the same as those taken
 *  by the vsprintf C library routine.  See the documentation on vsprintf.
 */
void
camp_setMsg( char* fmt, ... )
{
    va_list args;
    char c, *s;
    int d;
    double f;
    char str[2048];
    declare_add_msg()

    va_start( args, fmt );

    /*
     *  14-Dec-2000  TW  Replace vsprintf with homemade format interpretation
     *                   Less versatile, and a little dangerous if %f
     *                   doesn't correspond to a 'double' type (etc.), but
     *                   allows us to check whether there is actually enough
     *                   room in the string for the printed arguments.
     *                   This was causing memory access violations for very large
     *                   messages.
     */
/*    vsprintf( add_msg, fmt, args );*/

    init_add_msg();

    while( *fmt )
      {
	if( *fmt == '%' )
	  {
	    fmt++;
	    switch( *fmt++ )
	      {
	      case 's':
		s = va_arg( args, char* ); break;
	      case 'c':
		sprintf( str, "%c", (c = (char)va_arg( args, int )) ); s = str; break;
	      case 'd':
		sprintf( str, "%d", (d = va_arg( args, int )) ); s = str; break;
	      case 'x':
		sprintf( str, "%x", (d = va_arg( args, int )) ); s = str; break;
	      case 'f':
		sprintf( str, "%f", (f = va_arg( args, double )) ); s = str; break;
	      case 'g':
		sprintf( str, "%g", (f = va_arg( args, double )) ); s = str; break;
	      case 'e':
		sprintf( str, "%e", (f = va_arg( args, double )) ); s = str; break;
	      default:
		goto done;  /* Stop on this string if we encounter unknown format */
	      }
	    add_to_add_msg( s, strlen( s ) );
	  }
	else
	  {
	    add_to_add_msg( fmt, 1 ); fmt++;
	  }
      }
  done:
    va_end( args );

    set_thread_msg( add_msg );
    free_add_msg();
}

/*
 *  Append a string to the global message string
 *
 *  The parameters are variable in number - the same as those taken
 *  by the vsprintf C library routine.  See the documentation on vsprintf.
 */
void
camp_appendMsg( char* fmt, ... )
{
    va_list args;
    char separator[4];
    int new_len;
/*    char add_msg[MAX_LEN_MSG+1];    /* Make it big to be safer */
    char* whole_msg;
    int len_whole_msg;
/*    int len_add_msg;*/
    int len_separator;
    int len_remaining;
    char c, *s;
    int d;
    double f;
    char str[2048];
    declare_add_msg()

    va_start( args, fmt );

    /*
     *  14-Dec-2000  TW  Replace vsprintf with homemade format interpretation
     *                   Less versatile, and a little dangerous if %f
     *                   doesn't correspond to a 'double' type (etc.), but
     *                   allows us to check whether there is actually enough
     *                   room in the string for the printed arguments.
     *                   This was causing memory access violations for very large
     *                   messages.
     */
/*    vsprintf( add_msg, fmt, args );*/

    init_add_msg();

    while( *fmt )
      {
	if( *fmt == '%' )
	  {
	    fmt++;
	    switch( *fmt++ )
	      {
	      case 's':
		s = va_arg( args, char* ); break;
	      case 'c':
		sprintf( str, "%c", (c = (char)va_arg( args, int )) ); s = str; break;
	      case 'd':
		sprintf( str, "%d", (d = va_arg( args, int )) ); s = str; break;
	      case 'x':
		sprintf( str, "%x", (d = va_arg( args, int )) ); s = str; break;
	      case 'f':
		sprintf( str, "%f", (f = va_arg( args, double )) ); s = str; break;
	      case 'g':
		sprintf( str, "%g", (f = va_arg( args, double )) ); s = str; break;
	      case 'e':
		sprintf( str, "%e", (f = va_arg( args, double )) ); s = str; break;
	      default:
		goto done;  /* Stop on this string if we encounter unknown format */
	      }
	    add_to_add_msg( s, strlen( s ) );
	  }
	else
	  {
	    add_to_add_msg( fmt, 1 ); fmt++;
	  }
      }
  done:
    va_end( args );

    /*
     *  Make all characters printable
     */
/*  20-Dec-1999  TW  Want non-printable characters for binary dumps
                     from some instruments
    stoprint( add_msg, add_msg );
*/

    whole_msg = camp_getMsg();

    if( whole_msg[0] == '\0' ) 
    {
        strcpy( separator, "" );
    }
    else 
    {
        strcpy( separator, " | " );
    }

    len_whole_msg = strlen( whole_msg );
    len_add_msg = strlen( add_msg );
    len_separator = strlen( separator );

    len_remaining = MAX_LEN_MSG - len_whole_msg;
    strncat( whole_msg, separator, len_remaining );
    if( len_remaining <= len_separator )
    {
        whole_msg[MAX_LEN_MSG] = '\0';
        return;
    }
    else
    {
        whole_msg[len_whole_msg+len_separator] = '\0';
    }

    len_remaining -= len_separator;
    strncat( whole_msg, add_msg, len_remaining );
    free_add_msg();

    if( len_remaining <= len_add_msg )
    {
        whole_msg[MAX_LEN_MSG] = '\0';
        return;
    }
    else
    {
        whole_msg[len_whole_msg+len_separator+len_add_msg] = '\0';
    }
}


/*
 *  Append a string to the global message string based on a VMS message
 *  number.
 */
#ifdef VMS
void 
camp_appendVMSMsg( u_long numMsg )
{
    int status;
    char strMsg[LEN_MSG+1];
    u_short lenMsg;
    s_dsc dscMsg;
    u_long flags;
    char dummy_byte_array[4];
    char* p;

    setdsctobuf( &dscMsg, strMsg, LEN_MSG );
    lenMsg = 0;

#define TEXT 1
#define IDENT 2
#define SEVERITY 4
#define FACILITY 8

    /*
     *        Get the corresponding Msg string
     */
    flags = FACILITY | SEVERITY | IDENT | TEXT;
    status = lib$sys_getmsg( &numMsg, &lenMsg, &dscMsg, 
                             &flags, dummy_byte_array );
    if( _failure( status ) )
    {
        camp_appendMsg( "camp_appendVMSMsg: failed sys$getmsg" );
        return;
    } 
    else
    {
        strMsg[lenMsg] = '\0';
	/*
	 *  Get rid of characters special to printf
         *  (printf is used in appendMsg)
	 */
        for( p = strMsg; *p != '\0'; p++ ) 
	  {
	    if( *p == '%' ) *p = ' ';
	  }
        camp_appendMsg( strMsg );
    }
}
#endif /* VMS */


