$ on warning   then goto err_exit
$ !
$ home = f$environment ("default")
$ src :== 'p1'
$ root :== 'p2'
$ !
$ !  Create the directories
$ !
$ if f$search( "''root'[000000]camp.dir" ) .eqs. "" then -
    create/directory 'root'[camp]
$ if f$search( "''root'[camp]dat.dir" ) .eqs. "" then -
    create/directory 'root'[camp.dat]
$ if f$search( "''root'[camp]lib.dir" ) .eqs. "" then -
    create/directory 'root'[camp.lib]
$ if f$search( "''root'[camp]log.dir" ) .eqs. "" then -
    create/directory 'root'[camp.log]
$ !
$ !  Set ownership and protection
$ !
$ set file 'root'[000000]camp.dir /owner=online/prot=w:re 
$ set file 'root'[camp...]*.* /owner=online/prot=w:re
$ set file 'root'[camp]log.dir,dat.dir /prot=w:rwe
$ !
$ !  Copy the files
$ !
$ delete 'root'[camp]*.*;* /exc=(*.dir,*.dat)
$ copy 'src'*.* 'root'[camp] /exc=(*.dir,*.dat)
$ !
$ set default 'home'
$ exit
$err_exit:
$ write sys$output "Error detected in camp_install.com"
$ exit
$ !
$ !  camp_install.com  -  CAMP installation
$ !
$ !  p1 :  source camp: directory
$ !  p2 :  destination root (i.e., dua0:[apps.])
$ !
$ !  Revision history:
$ !   v1.0  23-Jun-1994  [TW] Initial
$ !   v1.0a 24-Jun-1994  [TW] Delete all in target camp: dir
$ !
