/*
 *  Name:       xdr_timeval_t
 *
 *  Purpose:    Routine that performs RPC XDR functions on the timeval_t
 *              structure that is standard in C libraries.
 *
 *              This structure is used by CAMP to maintain time information.
 *
 *  Called by:  camp_types_xdr_mod.c
 * 
 *  Revision history:
 *    21-Apr-1994  [T. Whidden] added asctimeval
 *    06-Jun-1994  [T. Whidden] added tvtof
 *
 */

#ifdef VMS

#ifdef MULTINET
#include <rpc/types.h>
#include <rpc/xdr.h>
#include <sys/time.h>
#endif /* MULTINET */

#ifdef UCX
#include <ucx$rpcxdr.h>
#endif /* UCX */

#else /* !VMS */

#include <rpc/types.h>
#include <rpc/xdr.h>

#endif /* VMS */

#include "timeval.h"

bool_t
xdr_timeval_t(xdrs, objp)
	XDR *xdrs;
	timeval_t *objp;
{
	if (!xdr_long(xdrs, &objp->tv_sec)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->tv_usec)) {
		return (FALSE);
	}
	return (TRUE);
}

