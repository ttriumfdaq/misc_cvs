#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from continue button
# 
# invoke this script with cmd
#              include                    experiment      hold   beamline
#                path                                     flag
# continue.pl /home/bnmr/online/mdarc/perl    bnmr2         n      bnmr
#
# Sets a flag in odb (sis mcs area) so that frontend will stop sending the histograms, but otherwise
#   nothing changes. 
#
# $Log: continue.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.12  2003/01/08 18:36:26  suz
# add polb
#
# Revision 1.11  2002/04/12 20:30:29  suz
# Add extra parameter for include_path
#
# Revision 1.10  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.9  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.8  2001/09/14 19:08:50  suz
# add 1 param; use strict;imsg now msg
#
# Revision 1.7  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.6  2001/05/03 21:33:37  suz
# support BNMR1
#
# Revision 1.5  2001/05/01 23:29:10  suz
# add message for bnmr1
#
# Revision 1.4  2001/03/01 19:20:12  suz
# output sent to /var/log/midas rather than /tmp
#
# Revision 1.3  2001/02/23 20:39:21  suz
# use new subroutine open_output_file
#
# Revision 1.2  2001/02/23 20:09:47  suz
# Add use lib so require can work. Change file permissions on output file so world can write
#
# Revision 1.1  2001/02/23 17:53:33  suz
# initial version
#
#
use strict;
##################### G L O B A L S ####################################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#######################################################################
$|=1; # flush output buffers

# input parameters:
my ($inc_dir,$expt, $hold_flag, $beamline ) = @ARGV;
my $name = "continue";
# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


my $nparam=4;
my $outfile = "/var/log/midas/continue.txt";
my ($transition, $run_state, $path, $key, $status);
my $parameter_msg = "include_path, experiment; hold flag; beamline\n";
my ($flags_path,$len) ;
my $debug=$FALSE;
my $eqp_name;
#
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

$nparam = $nparam + 0;  #make sure it's an integer
#
# odb  msg cmd: 
#                 use 1=error 2=info  32=talk
#
# Check the parameter:
#
#
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile);
$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}
unless ($expt) 
{ 
    # print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  No experiment supplied \n";
    die  "$name:  FAILURE -  No experiment supplied ";
} 


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; hold flag = $hold_flag\n";
#

if ($hold_flag eq "y") { $hold_flag = $TRUE;} 
else { $hold_flag = $FALSE ; }

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)|| ($beamline =~ /polb/i ) )
{
# BNMR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
    $flags_path = "/Equipment/$eqp_name/sis mcs/flags/";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
    $flags_path = "/Equipment/$eqp_name/v680/flags/";
} 
print FOUT "flags_path = $flags_path\n";

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state == $STATE_RUNNING)
{   # Run in progress

    # check state of hold flag (supplied as a parameter)
    unless ($hold_flag) 
    {
        odb_cmd ( "msg","$MINFO","","$name", "INFO: hold flag is already cleared" ) ;
	print FOUT  "$name: hold flag is already cleared  \n";
        die "$name: Hold flag is already cleared";
    }

    ($status) = odb_cmd ( "set","$flags_path","hold" ,"n") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error clearing hold flag.\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE clearing hold flag" ) ;
        die "$name: Failure clearing hold flag";
    }
    print FOUT "INFO: Success - Hold flag has been cleared \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Hold flag has been cleared " );
    print "Success - Hold flag has been cleared \n";
}
else
{    # not running; no action
    print FOUT "Not running. Continue has no action. \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Run is not in progress. Continue has no action" );
    print "Not running. Continue has no action. \n";
}
exit;

