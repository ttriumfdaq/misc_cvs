#!/usr/bin/perl 
# above is magic first line to invoke perl
#
#     get_next_run_number.pl
#
############################################################################
#                                                                          #
#          This program should be called from mdarc                        #
#             since it may reset the current run number!!                 #
#                                                                          #
#          DO NOT RUN THIS SCRIPT UNLESS INVOKED BY MDARC                  #
#             or problems may occur with saved run files!!!                #
#              (except for testing!)                                       #
############################################################################
#
#
# invoke this with cmd    from mdarc of course!)      (1) when mdarc is started
#                                                     (2) on begin-of-run
#                                                     (3) if toggle flag is set 
#                  
#   get_next_run_number.pl
#
#                                        Input Parameters:   
#                          include                      expt    rn    equipment     check   beamline   
#                          directory                                   name       for holes
# get_next_run_number.pl /home/bnmr/online/mdarc/perl   bnmr2    0    FIFO_acq      0/1       bnmr
#
#  this script will read current run_number, run mode & saved directory from odb 
#
# Note - run_number parameter rn=0 for dummy run (see tr_start in  mdarc.c), i.e.
#       for a real begin run rn !=0

# e.g.  get_next_run_number.pl suz2   30356   FIFO_acq   0  bnmr
#
# Note - need eqp_name to determine the path of mdarc area, as this will
# be different for bnmr, musr etc.

#
# Output goes into file $outfile
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $next_run      next run number   - writes this to odb location for communication
#
# $Log: get_next_run_number.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.13  2003/01/08 18:36:17  suz
# add polb
#
# Revision 1.12  2002/05/08 01:16:58  suz
# fix some comments
#
# Revision 1.11  2002/04/12 21:44:54  suz
# check on writing to saved data dir now returns a warning
#
# Revision 1.9  2001/12/07 22:05:04  suz
# take out message about holes in run numbering
#
# Revision 1.8  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.7  2001/11/01 17:54:54  suz
# change variable msg to message
#
# Revision 1.6  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.5  2001/09/14 19:18:42  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.4  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.3  2001/04/30 19:58:30  suz
# Add support for midas logger
#
# Revision 1.2  2001/03/01 19:07:00  suz
# Check saved_data directory is writable for mdarc. Output file written in
# /var/log/midas rather than /tmp.
#
# Revision 1.1  2001/02/23 17:59:53  suz
# initial version
#
#
use_strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running# status = 0 is success for odb
# FOUT # file handle 
############################################################################

use vars qw($MAX_TEST $MIN_TEST $MAX_BNMR $MIN_BNMR $MAX_M20 $MIN_M20);
use vars qw($MAX_DEV $MIN_DEV $MAX_M9B $MIN_M9B $MAX_M15 $MIN_M15);
use vars qw($MAX_POLB $MIN_POLB);

# Globals needed by  this file and to run_number_check:
$MAX_TEST = 30499; 
$MIN_TEST = 30000;

# BNMR values
$MAX_BNMR = 44499; 
$MIN_BNMR = 40000;
#other beamlines
$MIN_M20  =     0;
$MAX_M20  =  4999;
$MIN_M15  =  5000;
$MAX_M15  =  9999;
$MIN_M9B  = 15000;
$MAX_M9B  = 19999;
$MIN_DEV  = 20000;
$MAX_DEV  = 29999;
$MIN_POLB = 45000;
$MAX_POLB = 49999; 
# The following only used if $check_for_holes is true.
use vars qw( $FOUND_HOLE $LIST_OF_HOLES $CHECK_FOR_HOLES);
$FOUND_HOLE = $FALSE;  # flag indicates if there are holes in the run numbers
$LIST_OF_HOLES;  
############################################################################

$|=1; # flush output buffers


# input parameters
my ( $inc_dir, $expt, $rn, $eqp_name, $check_for_holes, $beamline ) = @ARGV;
my $name = "get_run_number";

unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/run_number_check.pl";
require "$inc_dir/odb_access.pl";
require "$inc_dir/set_run_number.pl";

# local variables:

my $nparam = 6; # need 6 input parameters
my $parameter_msg = "include_path,  experiment , rn, equipment_name, check_for_holes,  beamline";

my ($run_number,  $run_type,  $saved_dir ); # read directly from odb
my ($transition, $run_state, $path, $key, $status);
my ($len,$next_run, $info, $process);

my $outfile = "/var/log/midas/get_run_number.txt"; 
my $msg_flag = $FALSE;
my $debug =  $FALSE;
my ($m1,$m2);
#---------------------------
# Run number information
#
my $max_test_run = 30499;
my $min_test_run = 30000;
# BNMR specific for now
my $max_real_run = 44499;
my $min_real_run = 40000;
#---------------------------
#


$nparam = $nparam + 0;  #make sure it's an integer

if ($expt) { $EXPERIMENT = $expt; } # for msg
open_output_file($name, $outfile); # FOUT

$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}


print FOUT    "get_run_number  starting with parameters:  \n";
print FOUT    "     Experiment = $expt;  Equipment name = $eqp_name  run number = $rn; \n";
print FOUT    "     Check for holes = $check_for_holes;  beamline = $beamline \n";  

#
#          Check the parameters
#
unless( ($EXPERIMENT =~ /bnmr2/i)  ||  ($EXPERIMENT =~ /bnmr1/i) || ($EXPERIMENT =~ /suz2/i )
 ||  ($EXPERIMENT =~ /musr2/i)  ||  ($EXPERIMENT =~ /musr/i)|| ($EXPERIMENT =~ /polb/i) )
{
    print FOUT "$name does not support experiment type: $EXPERIMENT \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Experiment type $EXPERIMENT not supported " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name does not support experiment type: $EXPERIMENT \n";
}

# check for holes parameter
unless ($check_for_holes) { $check_for_holes = $FALSE } ;


# rn will be set to run number for genuine tr_start, or 0 for dummy.
# can't use "unless ($rn) " since it will catch rn = 0 
$rn = $rn + 0; # make sure rn is an integer
 
unless ($eqp_name) 
{
    print FOUT "$name: Equipment name is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Equipment name not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Equipment name is not supplied";
}

#
#    
#              Read parameters from the odb
#
#
#
if( ($EXPERIMENT =~ /bnmr1/i)  )
{
# Type 1 Saved directory 
#   Read the  MLOGGER saved data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /logger",  "data dir " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading saved data directory from for Midas logger odb\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read Midas logger saved data directory from odb" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "$name: Error reading Midas logger saved data directory from odb";
    }
    ($saved_dir, $message) = get_string ($key);
    print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
}

else
{
#
# Type 2 Saved directory for BNMR and  all MUSR experiments
#   Read the  MDARC saved data directory from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "saved_data_directory " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading Mdarc saved data directory from odb\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read Mdarc saved data directory from odb" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "$name: Error reading Mdarc saved data directory from odb";
    }
    ($saved_dir,$message) = get_string ( $key);
    print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
}

# Just check that this saved directory is the same for the other type experiment
# for BNMR only
if( ($EXPERIMENT =~ /bnmr/i)  )
{
    $status=check_other_expt($eqp_name, $saved_dir);
    unless ($status)
    {
        print FOUT "FAILURE: Saved directories between Type 1 and Type 2 experiments are not identical\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  Saved directories between Type 1 and Type 2 expts are not identical " ) ; 
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  Automatic Run Numbering cannot proceed " ) ; 
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }  
        die "FAILURE: Saved directories between Type 1 and Type 2 experiments are not identical\n";
    }
    elsif ($ANSWER =~ /defined/i)
    {
        print FOUT "INFO: Saved directories between Type 1 and Type 2 expts cannot be checked since other type not defined\n";
        ($status)=odb_cmd ( "msg","$MINFO","","$name","INFO: Saved directories between Type 1 and 2 expts cannot be checked since other type not defined");
    }
    else
    {
        print FOUT "$name: SUCCESS: Saved directories between Type 1 and Type 2 experiments ARE identical (or one may be blank)\n";
        if ($debug) { print  "$name: SUCCESS: Saved directories between Type 1 and Type 2 experiments ARE identical (or one may be blank)\n";}
    }
}

print FOUT  "$name: Current directory for saved files = $saved_dir\n";
unless ($saved_dir)
{
    # This is an ERROR condition. Saved_dir must be supplied.
    print FOUT " Saved directory is a blank string \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Saved directory is a blank string " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "FAILURE: Saved directory is a blank string."; 
}


# Check we have  write access to the saved directory (or mdarc/mlogger will fail to write files to it)
unless (-w $saved_dir ) 
{ 
    $process= getpwuid($<);
    print FOUT "WARNING: this process (username=$process) cannot write to saved data directory: $saved_dir \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "WARNING :This process (username=$process) cannot write to  $saved_dir " ) ; 
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }  
} 

#
#    
#              Read more parameters from the odb
#
#
#
# read the current run number
#
($status, $path, $key) = odb_cmd ( "ls"," /Runinfo"," /Run number  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading run number from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read run number from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading run number from odb";
} 
$run_number = get_int ( $key);
print FOUT  "$name: Current run number = $run_number\n";
#
# read the run type from odb
#
($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc  "," /run type  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading key  /Equipment/$eqp_name/mdarc/run type  from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read run type from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading  /Equipment/$eqp_name/mdarc/run type from odb";
} 
($run_type,$message) = get_string ( $key);
print FOUT "$name: after get_string, run_type=$run_type\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }

#  Check the run type is valid
unless ($run_type)
{
    print FOUT  "$name: FAILURE - Empty string for run type has been supplied (expect test/real) \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Cannot determine run type (empty string)" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: FAILURE - Empty string for run type has been supplied (expect test/real) ";
}
unless( ($run_type =~ /test/i) ||  ($run_type =~ /real/i) )
{
    print FOUT "$name: FAILURE - Run type ($run_type)  must be test or real\n";
    ($status)=odb_cmd ( "msg","$MERROR","","get_run_number", "FAILURE: Invalid run type supplied." ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: FAILURE: Run type ($run_type)  must be test or real";
}
# Set check for holes to be TRUE for REAL runs
if ($run_type =~ /real/i)
{
    $check_for_holes = $TRUE;
}
else
{
    $check_for_holes = $FALSE;
}
#
#
#           Call run_number_check:
#
#
print FOUT   "Calling run_number_check for a $run_type run... \n";
print   "$name: Calling run_number_check for a $run_type run... \n";
#   FOUT is a global so we don't need to pass it.
($status, $next_run, $message  ) = run_number_check (  $saved_dir, $run_type, $check_for_holes, $beamline);
# note - run_number_check recycles test run numbers and deletes any old TEST saved run files for $next_run

unless ($status) 
{            #  FAILURE status from run_number_check   
    print FOUT   "$name: Failure status returned from run_number_check \n"; 
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE status from run_number_check" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    if ( $next_run ==  -2)      # -2 indicates a message
    { 
	# see if message should be split into 2 lines (contains a + )
	($m1,$m2) = split (/\+/,$message);
	print("m1 :$m1\n");
	print("m2 :$m2\n");
	if($m1)
	{
	    ($status)=odb_cmd (  "msg","$MERROR","","$name","$m1"); # relay the message to the user
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	}
	if($m2)
	{
	    ($status)=odb_cmd (  "msg","$MERROR","","$name","$m2"); # relay the message to the user
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	}


   } 
    die "$name: Failure status returned from run_number_check";
}
print FOUT   "Success from  run_number_check. Next $run_type run should be = $next_run\n"; 
if ($debug)
 { print "Success from  run_number_check. Next $run_type run should be = $next_run\n"; } 


if ($FOUND_HOLE)
  {
    print    "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n"; 
    print FOUT   "$name: ** WARNING: Run_number_check has found holes in the sequence of saved files for REAL run numbers  ** \n"; 
    ###
###  T E M P
### temporary - remove warning about HOLES 
#    ($status)=odb_cmd ( "msg","$MINFO","","$name", "** WARNING: Run_number_check has found HOLES in the sequence of saved files for REAL run numbers: **" );
#    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 

    print "$name: $LIST_OF_HOLES\n";
    print FOUT "$name: $LIST_OF_HOLES\n";
###    ($status)=odb_cmd ( "msg","$MINFO","","$name", "$LIST_OF_HOLES" );
###    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }     
  }
print FOUT "$name: Info - present run number is $run_number\n";
#
#
# 
if( $EXPERIMENT =~ /2$/i || $EXPERIMENT =~ /musr/i )  # musr experiment is Type 2
  {
    print FOUT "$name: detected type 2 experiment\n";
    print FOUT "$name: calling set_rn_type2 with parameters $experiment, $rn, $eqp_name, $next_run,  $run_number, $saved_dir\n" ;
    ($status) = set_rn_type2($rn, $eqp_name, $next_run,  $run_number, $saved_dir );
  }
elsif ( $EXPERIMENT =~ /1$/  || $EXPERIMENT =~ /polb/i )# polb experiment is Type 1
  { 
    print FOUT "$name: detected type 1 experiment\n";
    print FOUT "$name: calling set_rn_type1 with parameters $rn, $eqp_name, $next_run,  $run_number, $saved_dir\n" ;
    ($status) = set_rn_type1 ( $rn, $eqp_name, $next_run,  $run_number, $saved_dir );
  }
else
  {
    print FOUT "$name: Unknown type for experiment name: $EXPERIMENT \n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: unknown type for experiment name: $EXPERIMENT" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
    die "$name: Unknown type for experiment name: $EXPERIMENT";
  }
exit;                           


sub check_process
{
# piece of code to look for mdarc in ps  - later found it was easier to use odb cmd scl
    my $mdarc_is_running = $FALSE;

    open ( PS, "ps -auwx |") || die "ps fails\n";   # pipe into a file handle
    while ( <PS> )  # read from PS
    {
        chomp;
        unless (/mdarc/i) {next;}    # look for string "mdarc"
        
        print "$_\n";
        if ( /(-e+)\W+(\w+)/i)       # look for string e.g. "-e suz2" and load it into $1 $2 (with brackets)
        { 
            print "matched  $1 and $2\n"; 
            if ( $2=~ /$EXPERIMENT/i )   # now match $2 with this experiment
            {
                $mdarc_is_running = $TRUE;
                print "found $EXPERIMENT\n"; 
            }
        }
    }
    close (PS);
    return ($mdarc_is_running);
}

sub print_file
{
    my $filename = shift;
    my $name =   print_file;

    open FOUT, "$filename" or die "$name: couldn't open output file $filename" ;
    while (<FOUT>) 
    { 
        print "$_" ;
    }
}

sub check_last_saved_file
{
#
# Input
#      Current saved_dir  equipment_name
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $run_last_saved     run number of last valid saved file
#   $info          TRUE if a final saved file, otherwise false
#
# Output goes into file handle FOUT
    
#
# 
    my ( $saved_dir, $eqp_name ) = @_;
    
    my ($path, $key, $status);
    
    my $name = "check_last_saved_file";
    my ($last_saved_filename, $ext, $num, $info, $a, $b);
    my (@file, @dir);
    
    my $debug = $TRUE;
    
    print FOUT    "\n        = = = = $name = = = =    \n";
    print FOUT    "$name  starting with parameters:  \n";
    print FOUT    "     saved directory: $saved_dir \n";
    print FOUT    "     equipment name : $eqp_name \n";
    
#
# Check the parameters
#
#
    check_one ($saved_dir, "saved data directory", $name); 
    check_one ($eqp_name, "equipment name", $name);
    
    
#   Read the  last saved filename  from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /Equipment/$eqp_name/mdarc/",  "last_saved_filename " ) ;
    unless ($status) 
    { 
        print FOUT "$name: Error reading last_saved_filename from odb\n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read key mdarc/last_saved_filename from odb" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        if($debug) { print  "$name: Error reading last_saved_filename from odb\n"; }
        return ($FAILURE);
    }
    ($last_saved_filename,$message) = get_string ( $key);
    print FOUT "$name: after get_string, last saved filename=$last_saved_filename\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }
    if($debug) { print FOUT  "Read parameter last_saved_filename from odb = $last_saved_filename\n"; }
#print "saved_dir = $saved_dir\n";
    
    
# Check file exists
    unless (-f $last_saved_filename)  # includes directory
    { 
        print FOUT "$name: last saved file from odb key $last_saved_filename has not been found\n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:last saved file from odb key $last_saved_filename has not been found " ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        return ($FAILURE);  # strange error. No file found
    }
    
#
#  Check directory of last_saved_filename  is the same as saved_dir
#
    @file = split (/\//,$last_saved_filename); # split by /
    @dir  = split (/\//,$saved_dir);           # split by /
    
# check array lengths - @file should be one element longer
    if (  ($#dir+1) != ($#file + 0) ) 
    { 
        print FOUT "$name: array lengths of saved_dir ($saved_dir) and directory part of last_saved_filename ($last_saved_filename) don't match\n";
        return ($FAILURE);  # no information available on last saved run
    }
    
    $filename = pop (@file);  # pop the filename
    if ($debug) { print FOUT " filename =  $filename\n"; }

    
# make sure that directories are the same.
    
    while ( $a = pop (@dir) )   # pop last element from @dir  
    { 
        $b = pop (@file);       # pop last element from @file 
        
#    print "array element from dir = $a, & from filename  = $b\n ";
        unless ($a eq $b) 
        { 
            print FOUT "$name: no match between $a and $b (elements of saved_dir % last_saved_file strings)\n"; 
            return ($FAILURE);  # no information available on last saved run
        }
    } 

if ($debug) {print FOUT "directories are matching\n";}

# get the run number % extension out of the filename
($num, $ext)  = split (/\./,$filename);
if ($debug) { print FOUT "$name:  num = $num, extension = $ext\n"; }
$num = $num + 0;  # make sure it's an integer

# set flag for final saved file
unless ( $ext =~ /_v/i) { $info = $TRUE ;  } # final saved file
else                    { $info = $FALSE; }  # intermediate
 
if ($debug)
{
    if ($info) { print FOUT "$name: Found a final saved file for run $num \n"; }
    else { print FOUT "$name: Found an intermediate saved file for run $num \n"; }
}
return ($SUCCESS, $num, $info);  # return 

}
sub check_one
{
    # inputs:   value  string  name
    
    my ($value, $string, $name) =  @_;
    unless ($value) 
    {
        print FOUT "$name: $string is not supplied\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: $string not supplied. " ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die      "$name: $string is not supplied";
    }
    return;
}

sub check_other_expt($eqp_name, $saved_dir)
{
    my $name="check_other_expt";
    my ($command,$cmd,$key,$path);
    my ($orig_cmd,$orig_path,$orig_key,$value1,$value2);
    my ($other_expt, $other_dir);
    my $message;
    my ($eqp_name , $saved_dir) = @_;

    print FOUT "$name starting with equipment name: $eqp_name\n";
    print FOUT "      and saved_dir: $saved_dir\n";

    if ($EXPERIMENT =~ /2$/i) # match 2 at end of string  
    {
	print FOUT "$name: detected type 2 experiment - checking saved directory is identical for type 1 \n";
	# read the saved directory for type 1
	$orig_key = "Data dir";
	$orig_path = " /logger";
        $other_expt = $EXPERIMENT;
	$other_expt =~ s/2$/1/;
        print FOUT "$name: other_expt: $other_expt\n";
    }
    elsif ( $EXPERIMENT =~ /1$/)  # match 1 and end of string
    { 
	print FOUT "$name: detected type 1 experiment - checking saved directory is identical for type 2 \n";
# read the saved directory for type 2
	$orig_key = "saved_data_directory";
	$orig_path="/Equipment/$eqp_name/mdarc";
        $other_expt = $EXPERIMENT;
	$other_expt =~ s/1$/2/;
        print FOUT "$name: other_expt: $other_expt\n";
    } 
 
    else
    {   # shouldn't happen since checked previously
	print FOUT "$name:  does not support experiment type: $EXPERIMENT \n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE $name:  does not support experiment type: $EXPERIMENT " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "$name does not support experiment type: $EXPERIMENT \n";
    }
	
#   don't use odb_cmd since I don't want to change global $EXPERIMENT
    $orig_cmd = "ls";
    ( $command, $cmd, $path, $key)  = get_command_string($orig_cmd,$orig_path,$orig_key,$value1,$value2);
    print FOUT    "After get_command_string, command:$command\n"; 
    print FOUT    "       and cmd=$cmd; path =$path; key =$key;\n";
    $cmd  ="`odb -e $other_expt  -c $command` ";  # save this in case of error

    $ANSWER =`odb -e $other_expt -c $command`;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
# print information for now:
    print FOUT "command: $command\n";
    print FOUT "answer : $ANSWER\n"; 

    if($status != $ODB_SUCCESS) 
    {
	print FOUT   "$name: Failure status returned from odb \n";
    } 

#       For ls, odb should reply with the key at the beginning of the string.
    unless ($ANSWER=~/^$key/i)
    {
#       check for "Experiment not defined"
        if ($ANSWER =~ /Experiment not defined/i)
        {
            print FOUT "$name: $ANSWER ($other_expt) \n"; # unknown command
            return($SUCCESS);
        }
	print FOUT   "$name: Unexpected reply with command ls after reading $key \n";
	print FOUT   "         odb replied: $ANSWER\n";
	return($FAILURE);
    } 
    ($other_dir, $message) = get_string($key);
    print FOUT "$name: after get_string, other dir=$other_dir\n";
    unless ($message eq "") { print FOUT "         and message=$message\n"; }

    unless ($other_dir)
    {
	print FOUT "Saved directory for other experiment $other_expt is a blank string. Cannot check directories are identical\n";
	return ($SUCCESS);  # logging turned off maybe
    }
    print FOUT "$name: calling check_directories with saved directory for $other_expt = $other_dir\n";
    $status = check_directories($other_dir,$saved_dir);

    return ($status);
}




























