#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from hold button
# 
# invoke this script with cmd
#           include                    experiment      hold   toggle  beamline
#              path                                    flag   flag
# hold.pl /home/bnmr/online/mdarc/perl    bnmr2           n      y       bnmr
#
# Sets a flag in odb (sis mcs area) so that frontend will stop sending the histograms, but otherwise
#   nothing changes. 
#
# $Log: hold.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.12  2003/01/08 18:35:56  suz
# add polb
#
# Revision 1.11  2002/04/12 20:32:02  suz
# add parameter include_path
#
# Revision 1.10  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.9  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.8  2001/09/14 19:19:53  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.7  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.6  2001/05/03 21:33:52  suz
# support BNMR1
#
# Revision 1.5  2001/05/01 23:28:52  suz
# add message for bnmr1
#
# Revision 1.4  2001/03/01 19:20:35  suz
# output sent to /var/log/midas rather than /tmp
#
# Revision 1.3  2001/02/23 20:39:21  suz
# use new subroutine open_output_file
#
# Revision 1.2  2001/02/23 19:55:39  suz
# add toggle as a parameter. Change file permissions on output file so world can write
#
# Revision 1.1  2001/02/23 18:01:57  suz
#  initial version
#
#
use strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#########################################################
$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt, $hold_flag, $toggle_flag, $beamline ) = @ARGV;
my $name = "hold";
# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

my $nparam=5;
my $outfile = "/var/log/midas/hold.txt";
my ($transition, $run_state, $path, $key, $status);

my ($len,$flags_path, $eqp_name);
my $parameter_msg = "include_path, experiment; hold flag; toggle flag; beamline\n";
my $debug=$FALSE;
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

$nparam = $nparam + 0;  #make sure it's an integer
# Check the parameter:
#
#
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile);

$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

unless ($expt) 
{ 
    # print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  No experiment supplied \n";
    die  "$name:  FAILURE -  No experiment supplied ";
} 


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; hold flag = $hold_flag; toggle flag = $toggle_flag; beamline = $beamline\n";
#

if ($hold_flag eq "y") { $hold_flag = $TRUE;} 
else { $hold_flag = $FALSE ; }

if ($toggle_flag eq "y") { $toggle_flag = $TRUE;} 
else { $toggle_flag = $FALSE ; }
unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i) || ($beamline =~ /polb/i) )
{
# BNMR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
    $flags_path = "/Equipment/$eqp_name/sis mcs/flags/";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
    $flags_path = "/Equipment/$eqp_name/v680/flags/";
} 
print FOUT "flags path: $flags_path\n";


if(( $EXPERIMENT =~ /bnmr1/i)  || ($beamline =~ /polb/i))
{
    # Toggle not implemented yet for Type 1 experiments. Make sure it's FALSE
    print FOUT "$name: note - setting toggle false for bnmr1\n";
    $toggle_flag = $FALSE;
}

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state == $STATE_RUNNING)
{          #      Running

# check state of toggle flag  (supplied as a parameter)
    if ($toggle_flag) 
    { 
        print FOUT "Toggle flag is set. Run cannot be put on hold until toggle is complete.\n";
        ($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - Toggle flag is set. Hold not allowed" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die "Toggle flag is set. Run cannot be put on hold";
    }


    # check state of hold flag (supplied as a parameter)
    if ($hold_flag) 
    {
        odb_cmd ( "msg","$MINFO","","$name", "INFO: hold flag is already set" ) ;
        print FOUT  "$name: hold flag is already set  \n";
        die "$name: Hold flag is already set";
    }
    
    ($status) = odb_cmd ( "set","$flags_path","hold" ,"y") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting hold flag.\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting hold flag" ) ;
        die "$name: Failure setting hold flag";
    }
    print FOUT "INFO: Success - Hold flag has been set \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Hold flag has been set " );
    print "Success - Hold flag has been set \n";
}
else
{    # not running; no action
    print FOUT "Not running. Hold has no action. \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: Run is not in progress. Hold has no action" );
    print "Not running. Hold has no action. \n";
}
exit;

