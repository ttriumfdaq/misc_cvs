#!/usr/bin/perl 
# above is magic first line to invoke perl
#
#     delete_empty_mid_file.pl
#
############################################################################
#                                                                          #
#          This program should be invoked by start-all                     #
#
############################################################################
#
#
# invoke this with cmd        
#   get_next_run_number.pl
#
#                          Input Parameters:   
#                          include directory            expt  equipment name   delete                             
# get_next_run_number.pl /home/bnmr/online/mdarc/perl   bnmr1  FIFO_acq         1/0
#
# if delete is 1, the unlink is actually done. Can be set to 0 for testing
#
#  this script will read current run_number & saved directory from odb 
#
# Output goes into file $outfile
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $next_run      next run number   - writes this to odb location for communication
#
# $Log: delete_empty_mid_file.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.2  2003/01/08 18:26:46  suz
# change a message
#
# Revision 1.1  2002/07/11 21:50:10  suz
# original
#
#
#
use_strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running# status = 0 is success for odb
# FOUT # file handle 
############################################################################

$|=1; # flush output buffers


# input parameters
my ( $inc_dir, $expt, $eqp_name, $delete) = @ARGV;
my $name = "delete_empty_mid_file";

unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
#require "$inc_dir/run_number_check.pl";
require "$inc_dir/odb_access.pl";
#require "$inc_dir/set_run_number.pl";

# local variables:

my $nparam = 4; # need 4 input parameters
my $parameter_msg = "include_path,  experiment, equipment name, delete_flag ";
my $eqp_name = "FIFO_acq"; # bnmr1 equipment name
my ($run_number, $saved_dir ); # read directly from odb
my ( $path, $key, $status);
my $len;
my $next_run;
my $outfile = "/var/log/midas/delete_empty_mid_file.txt"; 
my $debug =  $FALSE;
my ($age,$filename);

$nparam = $nparam + 0;  #make sure it's an integer

if ($expt) { $EXPERIMENT = $expt; } # for msg
open_output_file($name, $outfile); # FOUT

$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}


print FOUT    "$name  starting with parameters:  \n";
print FOUT    " Include_dir = $inc_dir    Experiment = $expt equipment name = $eqp_name delete_flag $delete\n";  

#
#          Check the parameters
#
unless(  ($EXPERIMENT =~ /bnmr1/i) ||  ($EXPERIMENT =~ /musr/i)  )
### TEMP unless( $EXPERIMENT =~ /bnmr1/i) 
{
    print FOUT "$name does not support experiment type: $EXPERIMENT \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Experiment type $EXPERIMENT not supported " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name does not support experiment type: $EXPERIMENT \n";
}
unless ($eqp_name) 
{
    print FOUT "$name: Equipment name is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Equipment name not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Equipment name is not supplied";
}

$delete = $delete + 0;

unless ( ($delete) || $delete == 0 ) # specifically detect delete=0 
{
    print FOUT "$name: Delete flag is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Delete flag not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    die      "Delete flag is not supplied";
}
unless ($delete ) { print "Delete flag is false - no files will actually be deleted\n";}
#
#    
#              Read parameters from the odb
#
#
#

# Type 1 Saved directory 
#   Read the  MLOGGER saved data directory from odb
($status, $path, $key) = odb_cmd ( "ls"," /logger",  "data dir " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading saved data directory from Midas logger area in odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read Midas logger saved data directory from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading odb key \"/logger/data dir\"";
}
($saved_dir, $message) = get_string ($key);
print FOUT "$name: after get_string, saved_dir=$saved_dir\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }



print FOUT  "$name: Current directory for Midas saved files = $saved_dir\n";
unless ($saved_dir)
{
    # This is an ERROR condition. Saved_dir must be supplied.
    print FOUT " Saved directory is a blank string \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Saved directory is a blank string " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "FAILURE: Saved directory is a blank string."; 
}


# Check we have  write access to the saved directory (or mdarc/mlogger will fail to write files to it)
unless (-w $saved_dir ) 
{ 
    $process= getpwuid($<);
    print FOUT "WARNING: this process (username=$process) cannot write to saved data directory: $saved_dir \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "WARNING :This process (username=$process) cannot write to  $saved_dir " ) ; 
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }  
} 

#
#    
#              Read more parameters from the odb
#
#
#
# read the current run number
#
($status, $path, $key) = odb_cmd ( "ls"," /Runinfo"," /Run number  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading run number from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read run number from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading run number from odb";
} 
$run_number = get_int ( $key);
print FOUT  "$name: Current run number = $run_number\n";

#
# read the current filename
#
($status, $path, $key) = odb_cmd ( "ls"," /logger/channels/0/settings"," /current filename  " ) ;
unless ($status) 
{ 
    print FOUT "$name: Error reading current saved filename from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read current saved filename from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading current saved filename from odb key \"/logger/channels/0/settings\"";
} 
($filename, $message) = get_string ($key);

print FOUT "$name: after get_string, filename = $filename\n";
unless ($message eq "") { print FOUT "         and message=$message\n"; }


unless ( chdir ("$saved_dir"))
{
    print FOUT  "$name: FAILURE - cannot change directory to $saved_dir\n";
    ($status)=odb_cmd ( "msg","$MERROR", "", "$name: FAILURE - cannot change directory to $saved_dir");
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: FAILURE - cannot change directory to $saved_dir";
}

print FOUT "$name: changed directory to  $saved_dir\n";

$next_run = $run_number + 1; # run number may have been reset to one less by run number checker, expection mlogger
                             # to delete empty file.
if( $filename =~ /$run_number/)
{
#    print "$name: Current filename $filename is for current run number\n";
    print FOUT "$name: Current filename $filename  is for current run number\n";
    
}

elsif( $filename =~ /$next_run/)
{
#    print "$name: Current filename $filename will be for next run number ($next_run) \n";
    print FOUT "$name: Current filename  $filename  will be for next run number ($next_run)\n";
}
else
{
#   print "$name: exiting since current file $filename is not the same as the run number $run_number\n";
   print FOUT "$name: exiting since current file $filename is not the same as the run number $run_number; no action needed\n";
   exit;
}


# check the size (and other attributes)  of this midas file
if ( -e $filename ) # if file exists
{
    $age = ( -M $filename);
#    print   "$name:  file $filename:  modification age in days: $age\n"; 
    print FOUT  "$name:  file $filename:  modification age in days: $age\n"; 
    

    $size = (-s $filename);
#    print "$name: file $filename exists (and has size $size)\n";
    print FOUT "$name: file $filename exists (and has size $size)\n";
    
    if ( $size <=32768 ) # size of empty file or file with no data
    {

	if ($debug) { print "$name: File $filename is empty and will be deleted \n"; } # midas logger will overwrite this
	print FOUT "File $filename is empty and will be deleted \n"; # midas logger will overwrite this

# Check if we actually want to delete the file
	if ($delete)
	{
	    unless (  unlink ($filename))
	    {
		print FOUT "$name: failure deleting empty saved file  $filename in directory $saved_dir\n";
		($status)=odb_cmd ( "msg","$MERROR","","$name", "Could not delete empty saved file $filename" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		unless ( -w $filename) 
		{
		    printf FOUT "$name: this process does not have write permission to file\n";
		    printf  "$name: this process does not have write permission to file\n";
		}
		die "$name: failure deleting empty saved file  $filename in directory $saved_dir";
	    }
	    else 
	    { 
		print FOUT "$name: Successfully deleted empty saved  file $filename\n"; 
		print  "$name: Successfully deleted empty saved  file $filename\n"; 
		($status)=odb_cmd ( "msg","$MINFO","","$name", "Successfully deleted empty saved file $filename" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    }
	}
	else
	{
	    print FOUT "$name: Would have deleted empty saved  file $filename (delete flag is false) \n"; 
	    print  "$name: Would have deleted empty saved  file $filename (Delete flag is false) \n"; 
	}
    }
    else  { print FOUT "$name: $filename has a valid size; no action taken \n";     }
}
else {  print FOUT "$name: $filename does not exist; no action taken \n"; }
exit;
