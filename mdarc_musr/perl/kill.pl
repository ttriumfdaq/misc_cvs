#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#  kill.pl
# 
# invoke this perlscript with cmd  e.g. for Type 2
#        include    expt  run #   saved dir               disable_run   purge  hold  beam toggle    
#        path                                              _number_check  flag   flag  line  flag    
# kill.pl  *       bnmr2 30102 /home/bnmr/online/dlog/2001    n           y      n   bnmr   n
#
#                   and  for Type 1 :
#         include  experiment  run #   saved dir               disable_run   purge  hold  beam     
#         path                                             _number_check  flag   flag  line      
# kill.pl   *        bnmr1   30102 /home/bnmr/online/dlog/2001    n           y      n   bnmr
#
#   *  where include_path might be /home/bnmr/online/mdarc/perl
#
# $Log: kill.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.14  2003/01/08 18:35:37  suz
# add polb
#
# Revision 1.13  2002/04/15 19:43:25  suz
# Fix bug in messages about file permissions
#
# Revision 1.12  2002/04/15 17:15:52  suz
# add parameter include_path and support for musr pause
#
# Revision 1.11  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.10  2001/11/01 17:55:22  suz
# change parameter message
#
# Revision 1.9  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.8  2001/09/14 19:20:49  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.7  2001/06/19 18:20:29  suz
# run must now be on hold to kill
#
# Revision 1.6  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.5  2001/04/30 20:01:05  suz
# Add support for midas logger
#
# Revision 1.4  2001/03/29 17:49:01  suz
# Fix minor bug (replaced dir by saved_dir)
#
# Revision 1.3  2001/03/01 19:46:35  suz
# Check write access rather than ownership of saved directory.
#
# Revision 1.2  2001/03/01 19:03:53  suz
# Sets purge & rename flag before ending run, so file is not archived. Access to
# archive_dir removed, checks all files are intermediate or symlink before
# deleting. Checks permissions on files. Output file is now written to
# /var/log/midas  rather than /tmp.
#
# Revision 1.1  2001/02/22 22:36:57  suz
# Initial version
#
#
use strict; 
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING $STATE_PAUSED);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
$STATE_PAUSED =2; # Run state is paused (MUSR only)
#########################################################

$|=1; # flush output buffers

# input parameters:
my($inc_dir, $expt, $run_number, $saved_dir, $dis_rn_check, $purge_flag, $hold_flag, $beamline, $toggle_flag) =@ARGV;
# BNMR2: path  BNMR2  30100       (mdarc)        no              yes         yes        bnmr        no
# BNMR1: path  BNMR1  30100       (mlogger)      no              yes         yes        bnmr     not used presently
# MUSR:  path  MUSR   30100       (mdarc)        no              yes         yes        dev         no

# note: saved directory in /script/kill is a link to mdarc's (type 2) or mlogger's (type 1). These directories should be the same (except possibly for testing)

my $name = "kill";
# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/check_params.pl";

my $param_2 = 9;
my $param_1 = 8;
my $outfile = "/var/log/midas/kill.txt";
my ($status, $len);
my ( $old_run, $eqp_name);
my $mdarc_path = "/Equipment/FIFO_acq/mdarc/";
my $parameter_msg = "include_dir, expt , run #, saved dir, disable_rn_check flag, 
purge flag , hold flag, beamline, toggle flag  (toggle flag type 2 only)";
my ($transition, $run_state);
# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
# send imsg only if $expt is defined
if ($expt) { $EXPERIMENT = $expt; } # for msg
open_output_file($name, $outfile);

#
#
# Check the parameters
#
#
# check number of supplied parameters
$len = $#ARGV;  
$len += 1;
print FOUT "$name: No. parameters suppled = $len\n";

# only msg if there's an experiment
unless ($expt) {    die  "$name:  FAILURE -  No experiment supplied \n"; }

if ( $expt =~ /2$/ ||  $expt =~ /musr$/i   )  # Type 2 experiment ( bnmr2 suz2 musr2 or musr )
{
    unless ($len >= $param_2 )  # no. parameters for Type 2  
    {
	print FOUT "Too few input parameters supplied ($len); expect $param_2 for $expt \n";
	print FOUT "   $parameter_msg\n"; 
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - Too few input parameters supplied" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	print "Too few input parameters supplied ($len); expect $param_2 for $expt \n";
	die   "   $parameter_msg\n"; 
    }
}
elsif(  ( $expt =~ /1$/)|| ( $expt =~ /polb/i))  # Type 1 experiment ( bnmr1  ) 
{
    unless ($len == $param_1 )
    {
	print FOUT "Too few input parameters supplied ($len); expect $param_1  for $expt \n";
	print FOUT "   $parameter_msg\n"; 
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - Too few input parameters supplied" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	print "Too few input parameters supplied ($len); expect  $param_1 for $expt \n";
	die   "   $parameter_msg\n"; 
    }
}
else
{
    print FOUT "Unknown experiment type $expt  \n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - Unknown experiment type $expt " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Unknown experiment type $expt \n";
}


print FOUT  "$name: Arguments supplied:  @ARGV\n";
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; Run number= $run_number  \n";
print FOUT  "Paths:  Saved file directory  = $saved_dir; Disable automatic run number check = $dis_rn_check\n"; 
print FOUT  "Purge flag = $purge_flag; hold flag = $hold_flag; beamline = $beamline; toggle flag = $toggle_flag \n";

unless ($dis_rn_check eq "n") 
{
        print FOUT "FAILURE: Automatic run numbering is DISABLED. \n"; 
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Automatic run numbering is DISABLED - automatic kill not allowed " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die  "FAILURE:  Automatic run numbering is DISABLED. \n"; 
}


$status = check_params($expt, $run_number, $saved_dir, $name);
unless ($status) 
{
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - problem with input parameters" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print      "Invoke this perl script $name with the parameters:\n";
    die      "   $parameter_msg\n"; 
}
unless ($beamline)
{
    print FOUT "FAILURE: beamline supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name:  Failure status after odb_cmd (msg)\n"; } 
        die  "FAILURE:  beamline not supplied \n";
}

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)||  ($beamline =~ /polb/i) )
{
#   BNMR uses the hold flag to pause the run
  unless ($hold_flag eq "y") 
    {
      print FOUT "FAILURE: Run must be on HOLD to be killed. \n"; 
      ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run must be put on HOLD before it can be killed.  " ) ;
      unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
      die  "FAILURE:  Run must be on HOLD to be killed  \n"; 
    }
  
  # BNMR experiments use equipment name of FIFO_ACQ
  $eqp_name = "FIFO_ACQ";
}
else
  {
    # MUSR experiments used midas PAUSE
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    if ($run_state != $STATE_PAUSED)
      {   # Run is NOT paused
	print FOUT "INFO: Kill not allowed UNLESS  run is PAUSED \n"; 
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run must be PAUSEd before it can be killed.  " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "INFO: Kill not allowed UNLESS run is PAUSED  \n"; 
      }

    # All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
  } 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";


#
#              Type 2 Experiment
#
if ( $expt =~ /2$/ || $expt =~ /musr$/i)  # Type 2 experiment ( bnmr2 suz2 musr2 or musr ) 
{ 
    ($status) = kill_run_type_2( $saved_dir, $toggle_flag, $purge_flag);
}
else
{     # bnmr1, polb
    ($status) = kill_run_type_1($saved_dir, $purge_flag);
}
#
#
#
#
#      Now decrement run number ( starting next run will increment it)
#
#
    
$old_run = $run_number;  # remember old run for message 
$run_number = $run_number -1;

unless (set_run_number($run_number))  { die " $name:  Error return from set_run_number";}

# success
print FOUT  "$name: Successfully killed run $old_run \n";
($status)=odb_cmd ( "msg","$MINFO","","$name", "SUCCESS -  run $old_run killed  " ) ;
unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
print "$name:  Successfully killed run $old_run \n";
exit;


##########################################################################################################
#
#      Type 2
#
##########################################################################################################

sub kill_run_type_2($saved_dir,$toggle_flag, $purge_flag)
{
# parameters
#    $toggle_flag
#    $purge_flag

    my $name="kill_run_type_2";
    my ($saved_dir, $toggle_flag, $purge_flag) = @_;
    my ($status, $nextname, $found_files_flag);
    my ($process,$run_state,$transition);

    print "$name: parameters saved_dir: $saved_dir; toggle_flag: $toggle_flag; purge_flag: $purge_flag\n";
    print FOUT "$name: parameters saved_dir: $saved_dir; toggle_flag: $toggle_flag; purge_flag: $purge_flag\n";
# toggle_flag is true if not supplied 
    if ($toggle_flag eq "n") { $toggle_flag = $FALSE; }
    else { $toggle_flag = $TRUE ; }  
    
# set purge flag true if not supplied
    if ($purge_flag eq "n") { $purge_flag = $FALSE;} 
    else { $purge_flag = $TRUE ; } # if parameter is not supplied, assume purge is set.
    
#
# Check whether mdarc is running (after $EXPERIMENT is set up by check_params)
#
    unless (mdarc_running())   # subroutine to check if mdarc is running
    {
#  Don't kill without mdarc (no saved files) & run number not been validated 
	print FOUT "Mdarc is not running. It controls & validates the run numbers \n";
	print FOUT "Cannot kill the run unless mdarc is running since it involves deleting run files.\n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Kill button is only valid when mdarc is running" );
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }      
	die      "$name: Cannot kill run while mdarc is not running";
    }
    
    
# check state of toggle flag
    if ($toggle_flag) 
    { 
	print FOUT "Toggle flag is set. Run cannot be killed until toggle is complete.\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - Toggle flag is set. Kill not allowed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "Toggle flag is set. Run cannot be killed.";
    }
# check if this process owns the directory containing the saved files
# saved_dir is not blank (checked on entry as an input parameter).
    unless (-w $saved_dir )
    { 
	$process= getpwuid($<);
	print FOUT "$name: This process ($process) does not have write access to $saved_dir. Kill cannot proceed.\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:This process ($process) cannot write to $saved_dir. Run $run_number not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "This process does not have write access to saved directory $saved_dir. Kill cannot proceed. ";
    }
    else {     print FOUT "INFO - this process does have write access to directory $saved_dir \n"; } 
    
    
# check whether run is in progress
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    
    if($transition ) 
    { 
	print FOUT "Run is in transition. Try again later \n";
	print FOUT "Or set /Runinfo/Transition in progress to 0\n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run is in transition. Run cannot be killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "Run is in transition. Try again later" ;
    }
    
    
    if ($DEBUG) { print "Run state = $run_state \n"; }
    if ($run_state != $STATE_STOPPED) 
    {   # Run is not stopped; 
	
#   prevent archiving (and renaming) of final saved files
	
	print FOUT  "Attempting to clear mdarc parameter end_of_run_purge_rename\n";
	
	($status) = odb_cmd ( "set","$mdarc_path","end_of_run_purge_rename" ,"n") ;
	unless($status)
	{ 
	    print FOUT "$name: Failure from odb_cmd (set); Error clearing purge/rename flag.\n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE clearing purge/rename flag" ) ;
	    die "$name: Failure clearing purge/rename flag";
	}
	print FOUT "INFO: Success - Purge/rename flag has been cleared \n";
	odb_cmd ( "msg","$MINFO","","$name", "INFO: Purge/rename flag has been cleared " );
	print "Success - Purge/rename flag has been cleared \n";
	
#   stop the run
	print FOUT  "Attempting to stop run $run_number (run_state=$run_state) \n";
	
	($status) = odb_cmd ( "stop" ) ;
	unless ($status) { exit_with_message($name); }
	($run_state,$transition) = get_run_state();
	get_run_state($transition,$run_state);
	#if($transition) { print FOUT  "Run is in transition ($trans) \n"; }
	if($run_state != $STATE_STOPPED ) 
	{
	    print FOUT " FAILURE: Can't stop the run \n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Can't stop the run. Run not killed" ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "Can't stop the run.  " ;
	}
    }
    else
    {
	# run is already stopped. A stopped run can only be killed if 
	# it has the current run number, or risk destroying an earlier saved file...
	# but mdarc is running so it should have checked the run number ... maybe it is OK
	
	# but for now ...
	print FOUT "Run is already stopped. Can only kill a run that is in progress\n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run is already stopped. Run not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "Run is already stopped. Cannot kill run $run_number." ;
	
    }
    
#
# Run is now stopped. 
#
# Reset purge/rename flag to original state
    if($purge_flag)
    {
	print FOUT  "Attempting to reset mdarc parameter end_of_run_purge_rename\n";
	
	($status) = odb_cmd ( "set","$mdarc_path","end_of_run_purge_rename" ,"y") ;
	unless($status)
	{ 
	    print FOUT "$name: Failure from odb_cmd (set); Error resetting purge/rename flag.\n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE resetting purge/rename flag" ) ;
	    die "$name: Failure resetting purge/rename flag";
	}
	print FOUT "INFO: Success - Purge/rename flag has been reset \n";
	odb_cmd ( "msg","$MINFO","","$name", "INFO: Purge/rename flag has been reset " );
	print "Success - Purge/rename flag has been reset \n";
    }
    
#
#  Now find and delete any saved files
# saved_dir is not blank  (checked on entry as an input parameter).
    if ($DEBUG) { print FOUT  "saved_dir ($saved_dir) is not blank\n";}
    unless ( chdir ("$saved_dir") )
    {
	print FOUT "cannot change directory to $saved_dir";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Can't change directory. Run not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "FAILURE: Can't change directory to $saved_dir. Run not killed. " ;
    }
    
    $found_files_flag = $FALSE; # flag to indicate found any run files
# Expect to delete intermediate saved files (versioned or a symlink thanks to purge/rename flag being cleared)
# No archived files will be deleted.
    while ($nextname = <*$run_number.msr*>) 
    {
	print FOUT  "found saved  file  $nextname ( and about to delete it ) \n";
	$found_files_flag = $TRUE;
#   make sure this is NOT a final saved file
	if ( $nextname =~ /msr$/i)   # anchored search for .msr files
	{
	    unless (-l $nextname)
	    {
		print FOUT "ERROR: found a final saved file $nextname which is NOT a symlink\n";
		print FOUT "       It will not be deleted\n";
		($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE - found FINAL saved run file $nextname. It will NOT be deleted. Run not killed");
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		die "Final saved file found $nextname. Not deleting it. Run not killed";
	    }
	    else 
	    { print FOUT "INFO - found a file $nextname that is a symlink\n"; }
	}
#   check we are the owner
	unless (-o $nextname)
	{
	    print FOUT "$name: ERROR - this process is not the owner of $nextname\n"; 
	    odb_cmd ( "msg","$MERROR","","$name", "ERROR: this process is not the owner of $nextname. Run not killed" ) ;
	    die "$name: this process does not own  $nextname\n"; 
	}
	
# check the permissions on the file
	unless (-w $nextname)
	{   # add write permissions
	    print FOUT "INFO: changing permission on file $nextname to allow deletion\n";
	    unless (chmod(0600,$nextname) )
	    { 
		print FOUT "$name: ERROR -  Couldn't change permissions on $nextname\n"; 
		odb_cmd ( "msg","$MERROR","","$name", "ERROR: changing permissions on file $nextname. Run not killed" ) ;
		die "$name:  Couldn't change permissions on $nextname\n"; 
	    }
	}
	unless (  unlink ($nextname))
	{
	    print FOUT "failure deleting $nextname. Run not killed\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE deleting saved run file $nextname. Run not killed " ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "failure deleting $nextname in $saved_dir. Run not killed";
	}
	else
	{
	    print FOUT "Deleted saved run file $nextname\n";
	    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Deleted saved run file $nextname " ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	}
	
    }
    unless ($found_files_flag)
    {
	print FOUT "Info - No saved run files were found for run $run_number in $saved_dir\n";
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO:  No saved run files were found for run $run_number" ) ;
    }
    
    return;
}

##########################################################################################################
#
#      Type 1
#
##########################################################################################################
sub kill_run_type_1($saved_dir, $purge_flag)
{
# parameters
#    $saved_dir
#
#  Assumes midas logger channel is 0

    my $name="kill_run_type_1";
    my ($saved_dir, $purge_flag) = @_;

     my ($run_state,$transition, $process);
     my ($status, $nextname, $found_files_flag);

    my $debug=$FALSE;

    if($debug) { print "$name: parameters midas data dir: $saved_dir; purge_flag: $purge_flag  \n"; }
    print FOUT "$name: parameters  midas data dir: $saved_dir ; purge_flag: $purge_flag \n";


# Midas logger
#
#    Make sure  mlogger is running and logging is turned on, or no saved files  
    unless (mlogger_running() )   # subroutine to check if midas logger is running
    {
	print FOUT "Midas logger is not running or is disabled in odb. Kill button not enabled\n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Kill button is only valid when midas logger is enabled" );
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }      
	die      "$name: Midas logger (mlogger) is not running or is disabled in odb. Kill button not enabled";
     }

    

# check if this process owns the directory containing the saved files
# saved_dir is not blank (checked on entry as an input parameter).
    unless (-w $saved_dir )
    { 
	$process= getpwuid($<);
	print FOUT "$name: This process ($process) does not have write access to $saved_dir. Kill cannot proceed.\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:This process ($process) cannot write to $saved_dir. Run $run_number not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "This process does not have write access to saved directory $saved_dir. Kill cannot proceed. ";
    }
    else {     print FOUT "INFO - this process does have write access to directory $saved_dir \n"; } 
# check whether run is in progress
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    
    if($transition ) 
    { 
	print FOUT "Run is in transition. Try again later \n";
	print FOUT "Or set /Runinfo/Transition in progress to 0\n";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run is in transition. Run cannot be killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "Run is in transition. Try again later" ;
    }
    
    # set purge flag true if not supplied
    if ($purge_flag eq "n") { $purge_flag = $FALSE;} 
    else { $purge_flag = $TRUE ; } # if parameter is not supplied, assume purge is set.
    
   
    if ($DEBUG) { print "Run state = $run_state \n"; }
    if ($run_state != $STATE_STOPPED) 
    {   # Run is not stopped; 

#   prevent archiving (and renaming) of final saved files
	print FOUT  "Attempting to clear mdarc parameter end_of_run_purge_rename\n";
	($status) = odb_cmd ( "set","$mdarc_path","end_of_run_purge_rename" ,"n") ;
	unless($status)
	{ 
	    print FOUT "$name: Failure from odb_cmd (set); Error clearing purge/rename flag.\n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE clearing purge/rename flag" ) ;
	    die "$name: Failure clearing purge/rename flag";
	}
	print FOUT "INFO: Success - Purge/rename flag has been cleared \n";
	odb_cmd ( "msg","$MINFO","","$name", "INFO: Purge/rename flag has been cleared " );
	print "Success - Purge/rename flag has been cleared \n";
	
#   stop the run
	print FOUT  "Attempting to stop run $run_number (run_state=$run_state) \n";
	
	($status) = odb_cmd ( "stop" ) ;
	unless ($status) { exit_with_message($name); }
	($run_state,$transition) = get_run_state();
	get_run_state($transition,$run_state);
	#if($transition) { print FOUT  "Run is in transition ($trans) \n"; }
	if($run_state != $STATE_STOPPED ) 
	{
	    print FOUT " FAILURE: Can't stop the run \n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Can't stop the run. Run not killed" ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "Can't stop the run.  " ;
	}
    }
    else
    {
	# run is already stopped. 
	print FOUT "Run is already stopped. Can only kill a run that is in progress";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run is already stopped. Run not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "Run is already stopped. Cannot kill run $run_number." ;
	
    }
    
#
# Run is now stopped. 
#
#
# Reset purge/rename flag to original state
    if($purge_flag)
    {
	print FOUT  "Attempting to reset mdarc parameter end_of_run_purge_rename\n";
	
	($status) = odb_cmd ( "set","$mdarc_path","end_of_run_purge_rename" ,"y") ;
	unless($status)
	{ 
	    print FOUT "$name: Failure from odb_cmd (set); Error resetting purge/rename flag.\n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE resetting purge/rename flag" ) ;
	    die "$name: Failure resetting purge/rename flag";
	}
	print FOUT "INFO: Success - Purge/rename flag has been reset \n";
	odb_cmd ( "msg","$MINFO","","$name", "INFO: Purge/rename flag has been reset " );
	print "Success - Purge/rename flag has been reset \n";
    }
    
#  Now find and delete any saved files
# saved_dir is not blank  (checked on entry as an input parameter).
    unless ( chdir ("$saved_dir") )
    {
	print FOUT "cannot change directory to $saved_dir";
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Can't change directory. Run not killed" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "FAILURE: Can't change directory to $saved_dir. Run not killed. " ;
    }
    
    $found_files_flag = $FALSE; # flag to indicate found any run files
    while ($nextname = <*$run_number.m*>)    # MIDAS file, .msr as well  
    {
	print FOUT  "found saved  file  $nextname ( and about to delete it ) \n";
	$found_files_flag = $TRUE;
#   check we are the owner
	unless (-o $nextname)
	{
	    print FOUT "$name: ERROR - this process is not the owner of $nextname\n"; 
	    odb_cmd ( "msg","$MERROR","","$name", "ERROR: this process is not the owner of $nextname. Run not killed" ) ;
	    die "$name: this process does not own  $nextname\n"; 
	}
	
# check the permissions on the file
	unless (-w $nextname)
	{   # add write permissions
	    print FOUT "INFO: changing permission on file $nextname to allow deletion\n";
	    unless (chmod(0600,$nextname) )
	    { 
		print FOUT "$name: ERROR -  Couldn't change permissions on $nextname\n"; 
		odb_cmd ( "msg","$MERROR","","$name", "ERROR: changing permissions on file $nextname. Run not killed" ) ;
		die "$name:  Couldn't change permissions on $nextname\n"; 
	    }
	}
	unless (  unlink ($nextname))
	{
	    print FOUT "failure deleting $nextname. Run not killed\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE deleting saved run file $nextname. Run not killed " ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "failure deleting $nextname in $saved_dir. Run not killed";
	}
	else
	{
	    print FOUT "Deleted saved run file $nextname\n";
	    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Deleted saved run file $nextname " ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	}
	
    }
    unless ($found_files_flag)
    {
	print FOUT "Info - No saved run files were found for run $run_number in $saved_dir\n";
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO:  No saved run files were found for run $run_number" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    
    return;
}










