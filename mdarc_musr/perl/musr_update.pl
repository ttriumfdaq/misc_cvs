#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
# 
#
# invoke this with cmd e.g :
#                         include_path             experiment   beamline  
# musr_update.pl  /home/musrdaq/online/mdarc/perl      musr       dev
#
#
# $Log: musr_update.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.1  2002/04/14 03:46:03  suz
# original: updates histo bin params
#
#
#
# Purpose: called by mdarc to update bin parameters from values in musr area if necessary
#      
use strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#########################################################
$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt, $beamline ) = @ARGV;
my $name = "musr_update";

# Inc_dir needed because if script is invoked by browser it can't find the
# code for require (may never be needed for musr but keep all scripts similar)

unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


my $outfile = "/var/log/midas/musr_update.txt";
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path, $eqp_name, $musr_path);
my ($cmd, $len);
my $debug=$FALSE;  # local debug
my $parameter_msg = "include_path, experiment ,   beamline";
my $debug=$FALSE;
my $nparam = 3;  # no. of input parameters
my $update_flag;

my ($val, $from_path, $to_path, $update);
# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#
$nparam = $nparam + 0;  #make sure it's an integer

# Check the parameters:
#
#
if ($expt) { $EXPERIMENT = $expt; } # for msg
open_output_file($name, $outfile, $TRUE); # suppress to stop message about opening file
                 # appearing in odb. It will appear in mdarc's window 


$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; beamline = $beamline \n";
#
#
# Does not matter whether data logger is running or not
# But automatic run numbering must be enabled
#
#

unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
#  $eqp_name = "FIFO_ACQ";
    print FOUT "$name: no action for beamline $beamline \n";
    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO - no action for beamline $beamline" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die " no action for beamline $beamline \n";
}

# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
$mdarc_path= "/Equipment/$eqp_name/mdarc";
$musr_path = "/Equipment/$eqp_name/Settings";
print FOUT "mdarc path: $mdarc_path; musr_path: $musr_path\n";


## decided not to check, as this script will only be called
## from mdarc if we have data (therefore when run is going)

# check whether run is in progress

#($run_state,$transition) = get_run_state();
#if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 


#if ($run_state != $STATE_RUNNING)
#{
#    print FOUT "$name: no action as run is stopped \n";
#    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO - no action as run is stopped" ) ;
#    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
#    die " no action as run is stopped \n";
#}

# Run is going

$to_path = "$mdarc_path/histograms";
$from_path =   "$musr_path/mode/histograms";

$update_flag = $FALSE;
#  copies any elements across that have changed
$update_flag = update_array( $from_path, $to_path, "bin zero");
$update_flag = update_array( $from_path, $to_path, "first good bin");
$update_flag = update_array( $from_path, $to_path, "last good bin") ;
$update_flag = update_array( $from_path, $to_path, "first background bin");
$update_flag = update_array( $from_path, $to_path, "last background bin") ;

if ($update_flag) 
{ 
    if($debug) { print "$name: parameters have been updated\n";}
    print FOUT "$name: parameters have been updated\n";
}
else 
{  
    print "$name: no mdarc bin parameters required updating\n";
    print FOUT "$name:  bin parameters NOT updated\n";
}
exit;




sub update_array ($from_path, $to_path, $item)
{
# Copies elements of arrays across if they are not identical
#
# Input:   from_path  path of key from where array values are read
#          to_path    path of key to which array values are written
#          item       key name of array to be copied
#                      ( key name must be identical in each directory)   
#
# Output:  update_flag  set TRUE if any parameter needed to be updated
# 
    my $name = "update_array";
    my ($status, $path, $key) ;
    my ($val, $index, $update_flag);
    my $debug;
    my @from_array;

    my ($from_path, $to_path, $item)   = @_;
    unless ( $from_path && $to_path && $item)
    {
        print FOUT  "$name: bad or missing parameters supplied \n";
        ($status)=odb_cmd ( "msg","$MERROR","$name", " bad or missing parameters supplied");
        die      "$name: bad or missing parameters supplied\n ";
    }
    
    print FOUT "\n$name: update_array starting for $item \n";
    print FOUT "       from_path=$from_path \n to_path=$to_path  \n"; 

    $update_flag = $FALSE;
    
    ($status, $path, $key) = odb_cmd ( "ls","$from_path","$item","" ) ;
    unless($status)
    { 
        print FOUT "$name: Error reading key $from_path/$item\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $from_path/$item " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Error reading key $from_path/$item\n";
    }
    else 
    { 
        print FOUT "$name: Success -  read key $from_path/$item \n";
        if($debug) { print  "$name: Success -  read key $from_path/$item \n"; }
    }

    # $DEBUG=1;  #debug get_array
    get_array ($key, $ANSWER);  # returns array contents in global @ARRAY
    @from_array = @ARRAY;
    if($debug)
    {
        print "from_array: @from_array\n";
        print FOUT "from_array: @from_array\n";
        print "array length: $#from_array\n";
        print FOUT "array length: $#from_array\n";
    }

    ($status, $path, $key) = odb_cmd ( "ls","$to_path","$item","" ) ;
    unless($status)
    { 
        print FOUT "$name: Error reading key $to_path/$item\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $to_path/$item " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Error reading key $to_path/$item\n";
    }
    else 
    { 
        if($debug) { print FOUT "$name: Success -  read key $to_path/$item \n"; }
        print FOUT "$name: Success -  read key $to_path/$item \n"; 
    }
    # $DEBUG=1;  debug get_array
    get_array ($key, $ANSWER);  # returns array contents in global @ARRAY
    if($debug)
    {
        print "ARRAY:      @ARRAY\n";
        print FOUT "ARRAY:      @ARRAY\n";
        print "array length: $#ARRAY\n";
        print FOUT "array length: $#ARRAY\n";
    }
    if( $#from_array !=  $#ARRAY )
    {
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE - array lengths are not the same " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}        

        print FOUT "$name: ERROR - array lengths are not the same - from: ($#from_array) & to ($#ARRAY)  \n";
        die "$name: ERROR - array lengths are not the same  - from: ($#from_array) & to ($#ARRAY) \n";
    }

# $#ARRAY gives index of highest element
    for $index (0..$#ARRAY)
    {
        if($debug) { print ("array index  $index =  $ARRAY[$index]\n"); }
        $val = $from_array[$index];
        if ($val != $ARRAY[$index])
        {
#            if ($debug) { print "$name:updating element $index of array $item\n"; }
            print "$name:updating  mdarc's array \"$item [$index]\" to $val\n"; 
            print FOUT "$name:updating element $index of mdarc array $item to $val\n";

            ($status, $path, $key) = odb_cmd ( "set","$to_path","$item\[$index\]","$val" ) ;
            unless($status)
            { 
                print FOUT "$name: Error setting key $to_path/$item\[$index\]\n";
                ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading key $to_path/$item\[$index\] " ) ;
                unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
                die "$name: Error setting key $to_path/$item\[$index\]\n";
            }
            $update_flag = $TRUE;
        }
    }
    if($update_flag)
    {
#  'scalar @ARRAY'   gives the number of elements  
        if($debug) { print "$name: successfully updated array $item from $from_path to $to_path (",scalar @ARRAY, " elements) \n";}
        print FOUT "$name: successfully updated array $item from $from_path to $to_path (",scalar @ARRAY, " elements) \n";
    }
    else
    {
        if($debug) { print "$name: array $item did not need updating \n";}
        print FOUT "$name: array $item did not need updating \n";
    }
    return($update_flag);
}
    
    
    
    
