#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd
#                 include             experiment  disable auto run  beamline
#                  path                           numbering flag
# real.pl  /home/bnmr/online/mdarc/perl   bnmr1          n            bnmr
#
# Set a parameter (run type) in odb (mdarc area) for a real run,
# and call get_next_run_number.pl to supply the next valid run number.
#
# $Log: real_run.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.7  2003/01/08 18:35:25  suz
# add polb
#
# Revision 1.6  2002/04/12 21:45:57  suz
# add parameter include_path
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.3  2001/09/14 19:23:38  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.2  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.1  2001/04/30 19:55:17  suz
# Initial version
#
#
#
use strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#########################################################

$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt,$dis_rn_check, $beamline ) = @ARGV;
my $name = "real";

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


my $parameter_msg = "include_path, experiment, flag (disable run number check),  beamline";
my $outfile = "/var/log/midas/real_run.txt";
my ($transition, $run_state, $path, $key, $status);

my $mdarc_path ;
my ($cmd,$len, $eqp_name);
my $debug=$FALSE;
my $nparam = 4;  # no. of input parameters

# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#

# Check the parameters:
#
#
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile);

$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

if($expt)  { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied \n";
    die  "$name:  FAILURE -  Invalid experiment supplied \n";
}

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt;  Flag to disable run number check = $dis_rn_check\n";
#
#
# Does not matter whether data logger is running or not
# But automatic run numbering must be enabled
#
unless ($dis_rn_check eq "n") 
{
        print FOUT "FAILURE: Automatic run numbering is DISABLED . \n"; 
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Automatic run numbering is DISABLED. " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die  "FAILURE:  Automatic run numbering is DISABLED \n"; 
}

unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i) || ($beamline =~ /polb/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 

$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";

#
# check whether run is in progress
#

($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going

# return
    if ($EXPERIMENT =~ /2$/i) # match 2 at end of string (e.g. bnmr2, suz2) 
    {
	print FOUT "Run in progress. Use Toggle button to change run type \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - run is in progress. Use Toggle button to change run type" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die " Run is in progress. Use Toggle button to change the run type\n";
    }
    else  # Type 1
    {
	print FOUT "Run in progress.  Run type cannot be changed while run is in progress  \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - Run type cannot be changed while run is in progress " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "  Run type cannot be changed while run is in progress\n";
    }
}
else
{       # run is stopped
        
    ($status) = odb_cmd ( "set","$mdarc_path","run type" ,"real") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting run type\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting key run type " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Failure setting key run type\n";
    }
    else 
    { 
        print FOUT "$name: Success -  key run type has been set to real in odb\n"; 
    }
    # now launch get_next_run_number - set check_for_holes TRUE for a real run
    print FOUT "Calling get_next_run_number with parameters: $expt 0 $eqp_name 1 $beamline\n";
#    $cmd = sprintf("/home/bnmr/online/mdarc/perl/get_next_run_number.pl %s 0 %s 1 %s",$expt,$eqp_name,$beamline);
    $cmd = sprintf("$inc_dir/get_next_run_number.pl %s %s 0 %s 1 %s",
		   $inc_dir,$expt,$eqp_name,$beamline);
#    print "command: $cmd\n";
#    print FOUT "command: $cmd\n";
    print      "Output from get_next_run_number is in /var/log/midas/get_run_number.txt\n";
    print FOUT "Output from get_next_run_number is in /var/log/midas/get_run_number.txt\n";
    $status=system "$cmd";
    if( $status == 0)
    {
	print FOUT "Success after system command, status=$status\n";
	print "Success after system command, status=$status\n";
    }
    else
    {
	print FOUT "Failure after system command, status=$status\n";
	print      "Failure after system command, status=$status\n";
	if ($status == -1 )
	{      # there is a message in errno
	    print FOUT "Error message: $!\n"; # errno is put in $! for system cmd
	    print      "Error message: $!\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number due to: $! " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
	else
	{
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number (status=$status) " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)"; }
	}
    }
}
exit;








