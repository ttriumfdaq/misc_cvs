#!/usr/bin/perl
# above is magic first line to invoke perl
# type echo $? to get the status after running the perl
#
######################################################################
# Perlscript executed by fe_camp to check validity of camp paths etc.
#
#  $Log: camp.pl,v $
#  Revision 1.1  2003/10/07 22:18:22  suz
#  original for mdarc_musr cvs tree
#
#  Revision 1.4  2003/01/16 20:55:26  suz
#  add path to require odb_access
#
#  Revision 1.3  2003/01/08 01:43:04  suz
#  add code to return numerical value from camp_cmd
#
#  Revision 1.2  2002/05/08 01:15:14  suz
#  add require path as first parameter
#
#  Revision 1.1  2002/04/29 17:27:45  suz
#  version 1.4 moved from bnmr1/clients to perl directory
#
#  Revision 1.4  2001/05/09 17:34:24  suz
#  Change odb command msg to imsg to avoid speaker problem
#
#  Revision 1.3  2001/05/01 21:29:49  suz
#  minor change
#
#  Revision 1.2  2001/04/26 17:52:51  suz
#  Fix use lib name
#
#  Revision 1.1  2001/04/25 19:23:43  suz
#  Added to cvs
#
# 
######################################################################
#                             Input Parameters:   
#           include                    experiment    equipment       
#           directory                                  name
# camp.pl  /home/bnmr/online/mdarc/perl   bnmr1      fifo_acq 
#
# An output file (opened on FOUT) is used for informational messages

     
#
use strict;
##################### G L O B A L S ####################################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#######################################################################
use vars  qw($CAMP_SUCCESS $CAMP_VAR_ATTR_POLL $CAMP_IF_ONLINE);
use vars  qw($CAMP_ODB_PATH $MDARC_PATH @ARRAY);
#status = 2304 is success for camp
$CAMP_SUCCESS = 2304;
$CAMP_VAR_ATTR_POLL = 8; # these are in camp.h
$CAMP_IF_ONLINE = 1 ; 
# status = 0 is success for odb

# paths
$CAMP_ODB_PATH = "/equipment/camp/settings";
$MDARC_PATH =  " "; # "/equipment/$eqp_name/mdarc"

# ARRAY    #array contents used by get_array
#
#############################################
$|=1; # flush output buffers

# input parameters
my ( $inc_dir, $expt, $eqp_name ) = @ARGV;
my $name = "camp_perl";
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash

require "$inc_dir/odb_access.pl";

# local variables:

my $nparam = 3; # need 3 input parameters
my $parameter_msg = "include_path, experiment , equipment name";

# local variables
my ($status,$path,$key);  # for call to odb_cmd
my ($camp_status, $camp_path, $camp_answer); # for camp_cmd
my ($camp_host,$n_camp);
my (@poll_int, $poll_int, $pollint);
my (@camp_path_array, $camp_path_array);
my ($len, $junk,$elem, $i, $command, $msg);
my ($units,$title);
my $outfile = "/var/log/midas/camp.txt"; 
my $camp_cmd ="/home/musrdaq/musr/camp/linux/camp_cmd";
my $debug=0; # local debug
my $return_val;
#
# odb  msg cmd: 
#                 use 1=error 2=info  32=talk
#
#
if($DEBUG) { $debug=1;}

$nparam = $nparam + 0;  #make sure it's an integer
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile); # FOUT

$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 

# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}
if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}

print FOUT    "$name  starting with parameters:  \n";
print FOUT    "     Experiment = $expt;  Equipment name = $eqp_name  \n";
print     "$name  starting with parameters:  \n";
print     "     Experiment = $expt;  Equipment name = $eqp_name  \n";

# Check parameters
unless ($eqp_name) 
{
    print FOUT "$name: Equipment name is not supplied\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Equipment name not supplied. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 

    print "Invoke this perl script $name with the parameters:\n";
    print "   $parameter_msg\n"; 
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n";     
    die      "Equipment name is not supplied";
}

$MDARC_PATH =  "/equipment/$eqp_name/mdarc"; # assign global MDARC_PATH
if ( $eqp_name=~/MUSR_I_/i)
{
    $MDARC_PATH =  "/equipment/musr_td_acq/mdarc"; # assign global MDARC_PATH as TD equipment
}
if ($debug) { print   "MDARC_PATH : $MDARC_PATH\n";}

# ------------------------------------------------------------
# get number of camp variables
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "n_var_logged " ) ;
unless ($status) 
{ 
    print  FOUT "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read number of camp variables logged from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
$n_camp= get_int ( $key);

print FOUT  "Number of camp variables logged : $n_camp\n"; 
if ($debug) { print "Number of camp variables logged : $n_camp\n"; }
# TEMP for Testing
##print "TEMP Setting n_camp to 1\n";
##$n_camp =1;
unless ($n_camp)
{
    print  FOUT "$name: No camp variables are to be logged (no checks done)\n";
    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO: No camp variables are to be logged" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print  "$name: No camp variables are to be logged (no checks done) \n";
    exit;
}
# ------------------------------------------------------------
# get camp host name
# ------------------------------------------------------------
if ( $eqp_name=~/MUSR/i)
{
    ($status, $path, $key) = odb_cmd ( "ls"," $MDARC_PATH/camp",  "camp hostname " ) ;
}
else
{
    ($status, $path, $key) = odb_cmd ( "ls"," $MDARC_PATH",  "camp hostname " ) ;
}
unless ($status) 
{ 
    print FOUT "$name: Error reading camp host name from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp host name from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "$name: Error reading camp host name from odb";
}
($camp_host, $msg) = get_string($key);
print FOUT "Camp hostname:  $camp_host \n";
print      "Camp hostname:  $camp_host \n";


# ------------------------------------------------------------
# get the camp paths
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "var_path " ) ;
unless ($status) 
{ 
    print FOUT  "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp variable paths from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
# process  $ANSWER into $camp_path_array
&get_array($key);  # fills ARRAY

@camp_path_array=@ARRAY; # transfer contents
#    print      "camp paths:  = @camp_path_array\n";

# the first $n_camp are valid (#array = last index)
$#camp_path_array=$n_camp - 1 ;
if ($debug) { print      "Valid camp paths : @camp_path_array\n";}
# ------------------------------------------------------------
# get the camp polling intervals
# ------------------------------------------------------------
($status, $path, $key) = odb_cmd ( "ls"," $CAMP_ODB_PATH ",  "polling_interval " ) ;
unless ($status) 
{ 
    print FOUT  "$name: Error reading $path/$key from odb\n";
    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read camp polling interval from odb" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die  "$name: Error reading $path/$key from odb\n";
}
# process  $ANSWER into $pol_int
&get_array($key);  # fills ARRAY

@poll_int=@ARRAY; # transfer contents
#print"polling interval array  = @poll_int\n";

# the first $n_camp are valid (#array = last index)
$#poll_int=$n_camp - 1 ;
if  ($debug) 
{
    print      "Valid polling intervals: @poll_int\n";
    print      "\n";
    print      "About to check camp paths:\n";
}


#----------------------------------------------------------------------
# Access CAMP
#----------------------------------------------------------------------
if  ($debug)  { print "Info - accessing camp for each path...\n"; }

for ($i=0; $i<$n_camp; $i++)  # main loop on no. of camp variables to poll
{
    print FOUT "Accessing camp path $i = $camp_path_array[$i] ... \n\n";
    print      "Accessing camp path $i = $camp_path_array[$i] ... \n\n";

    $camp_path=$camp_path_array[$i];
    if($debug)
    {
	$command = " -node $camp_host \"varGetIdent $camp_path\" "; 
	print "command = $command\n";
    }
# check the path
    $camp_answer=`$camp_cmd -node $camp_host \"varGetIdent $camp_path\"`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    $return_val=check_camp_status($camp_status,$camp_answer);
#    print("return val = $return_val\n");
    if ($camp_status != $CAMP_SUCCESS)
    {
        print FOUT "Camp variable $camp_path not found. Check the path is correct \n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Camp variable $camp_path not found" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
        die "Camp variable $camp_path not found. Check the path is correct \n";
        
    }

    if  ($debug) { print "Info: path  $camp_path has been checked \n";}
    if (0) 
    {       # don't do this any more - not needed
# read the title from camp
	$camp_answer=`$camp_cmd -node $camp_host \"varGetTitle $camp_path\"`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
 	$return_val = check_camp_status($camp_status,$camp_answer);
	print("return val = $return_val\n");
	if ($camp_status != $CAMP_SUCCESS)
	{
	    print FOUT "Could not get the title for $camp_path \n";
	    die "Could not get the title for $camp_path \n";
	    
	}
	$title=$camp_answer;
	if ($debug) {  print "Read the title from camp for $camp_path : $title\n";}
	
# write the title to odb
	($status) = odb_cmd ( "set"," $CAMP_ODB_PATH ",  "title[$i]", "$title" ) ;
	unless ($status) 
	{ 
	    print  FOUT "$name: Error setting odb key $CAMP_ODB_PATH/title[$i] to $camp_answer \n";
	    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not set title in odb for $camp_path" ) ;
	    unless ($status) { print  "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "$name: Error setting odb key $CAMP_ODB_PATH/title[$i] to $camp_answer \n";
	}
	
# read the units from camp
	$camp_answer=`$camp_cmd -node $camp_host \"varNumGetUnits $camp_path\"`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
	$return_val=check_camp_status($camp_status,$camp_answer);
	print("return val = $return_val\n");
	if ($camp_status != $CAMP_SUCCESS)
	{
	    die "Could not get the units for $camp_path \n";
	    
	}
	$units = $camp_answer;
	if ($DEBUG) { print "received answer from camp for $camp_path units: $units\n";}
	
# write the units to odb
	($status) = odb_cmd ( "set"," $CAMP_ODB_PATH ",  "units[$i]", "$units" ) ;
	unless ($status) 
	{ 
	    print FOUT "$name: Error setting odb key $CAMP_ODB_PATH/units[$i] to $units \n";
	    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not set units in odb for $camp_path" ) ;
	    unless ($status) { print  "$name: Failure status after odb_cmd (msg)\n"; } 
	    die "$name: Error setting odb key $CAMP_ODB_PATH/units[$i] to $units \n ";
	}
	
	print FOUT "Wrote title $title and units $units to odb for path $camp_path\n";
    } # end of if (0)

# check if this path is  pollable ;
    if  ($debug) 
    {        
	print "****   Info: checking if path is pollable \n";
	$command = " -node $camp_host \"varGetAttributes $camp_path\" "; 
	print "command = $command\n";
    }
    $camp_answer=`$camp_cmd -node $camp_host "varGetAttributes  $camp_path "`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    $return_val=check_camp_status($camp_status,$camp_answer);
    if ($debug)
    { 
	print("camp_answer = $camp_answer;  return val = $return_val\n");  # numerical part of camp_answer
    }
    if ($camp_status != $CAMP_SUCCESS)
    {
        print FOUT "Error return from camp while checking if $camp_path is pollable \n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: checking if $camp_path is pollable" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
        die "Error return from camp while checking if $camp_path is pollable \n";
    }
    $return_val = $return_val & $CAMP_VAR_ATTR_POLL; # return_val = numerical part of the answer
    
    if($debug) { print("CAMP_VAR_ATTR_POLL = $CAMP_VAR_ATTR_POLL.  AND with $return_val ->  return_val becomes $return_val\n");}
    if(!$return_val) 
    {
        print FOUT "$name:  Warning - camp variable $camp_path is NOT pollable\n";
        print  "$name:  Warning - camp variable $camp_path is NOT pollable\n";
        ($status)=odb_cmd ( "msg","$MERROR", "","$name", "WARNING: Camp variable $camp_path is NOT pollable" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
        die "$name: Error - camp variable $camp_path is NOT pollable\n";
    }
    else
    {
	print FOUT "$name: INFO: camp variable $camp_path IS pollable\n";
	print "$name: INFO: camp variable $camp_path IS pollable\n";
    }

#               Check polling interval

    if  ($debug) { print "reading polling interval\n"; }
    $camp_answer=`$camp_cmd -node $camp_host "varGetPollInterval $camp_path"`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    $return_val=check_camp_status($camp_status,$camp_answer);
    if($debug) { print("numerical return val = $return_val\n"); }
    if ($camp_status == $CAMP_SUCCESS)
    {
        if  ($debug) {  print "Read back polling interval for $camp_path : $return_val seconds \n"; }
    }
    else
    {
        print"Warning - problem reading polling interval for $camp_path \n";
       
    }

#       Use polling intervals from odb 
    $pollint=$poll_int[$i];
    
    if  ($debug) { print "Polling interval will be set to $pollint\n";}
    if($pollint < 20)
    {
        print "WARNING: you have attempted to set a polling interval for path $camp_path less than minimum poll interval (20s)\n";
        print FOUT "WARNING: you have attempted to set a polling interval for path $camp_path less than minimum poll interval (20s)\n";
	if($return_val > 20)
	{
	    print "The polling interval will remain at present value of $return_val seconds\n";
	    print FOUT "The polling interval will remain at present value of $return_val seconds\n";
	    $pollint = $return_val ;
	}
	else
	{
	    print "The polling interval will be set to a value of 30 seconds\n";
	    print FOUT "The polling interval will be set to a value of 30 seconds\n";
	    $pollint = 30 ;
	}
	($status)=odb_cmd ( "msg","$MINFO", "","$name", 
			    "WARNING: Invalid polling interval in odb for $camp_path. A value of $pollint seconds will be used ") ;
    }
        
    unless( $pollint == $return_val)  # set polling interval if it is not already correct
    {  
	print FOUT " Setting polling interval to $pollint for $camp_path\n";
	$camp_answer=`$camp_cmd -node $camp_host "varSet $camp_path -p on -p_int $pollint "`;
	$camp_status=$?;
	chomp $camp_answer;  # strip trailing linefeed
	$return_val=check_camp_status($camp_status,$camp_answer);
	
	if ($camp_status != $CAMP_SUCCESS)
	{
	    print FOUT "camp.pl: Failure attempting to set polling interval to $pollint for camp path $camp_path\n";
	    ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE attempting to set polling interval $pollint for $camp_path" ) ;
	    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
	    die "camp.pl: Failure attempting to set polling interval to $pollint for camp path $camp_path\n";
	}
    }
    
# check the instrument is online
#  magic incantation to get 1st element of path 
    ($junk,$elem) = split("/",$camp_path);
    $elem = "/$elem";
    if($debug)
    {
	print " First element of path is elem=$elem \n";
	print "Checking if instument is online \n";
	$command = "camp_cmd -node $camp_host \"insGetIfStatus $elem\" "; 
	print "command = $command\n";
    }
    $camp_answer=`$camp_cmd -node $camp_host "insGetIfStatus  $elem"`;
    $camp_status=$?;
    chomp $camp_answer;  # strip trailing linefeed
    if($debug) { print " camp_answer=$camp_answer \n"; }
    $return_val=check_camp_status($camp_status,$camp_answer);
    if ($debug) 
    { 
	print " camp_answer=$camp_answer \n";
	print("return val = $return_val\n");   # numerical value
    }
    
    if ($camp_status == $CAMP_SUCCESS)
    {
        if ($return_val != 1)
        {
            print FOUT "WARNING: CAMP  instrument $elem is offline\n";
            print FOUT "Use CAMP to set instrument online\n";
            ($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Camp instrument is offline. Use CAMP to set it online" ) ;
            unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
            
            print "WARNING: the CAMP instrument $elem is offline\n";
            die "Use CAMP to set instrument online\n";
        }
	else { print "Instrument $elem is online\n" ; }
    }
    else
    {
	print FOUT "$name: Error while checking if instrument $elem  is online\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE:  Error while checking if instrument $elem  is online" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
	die "$name: Error while checking if instrument $elem  is online\n";
    }    
    print "\n";
}  # end of loop on all camp paths

print FOUT "Successful exit from camp.pl\n";
($status)=odb_cmd ( "msg","$MINFO", "","$name", "SUCCESS: Camp perl script exiting after checking camp variables" ) ;
unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; }
exit;  


##############################################################
sub get_yn
###############################################################
{
#   input:  string to ask question
#   output: return value is TRUE or FALSE depending on $ANSWER
#
    my $ans;
    my $string= $_[0];
    my $status;

    while($TRUE)
    {
        print "$string?\n";
        $_=<STDIN>;
        $status=$?;
        print "status = $status\n";
        tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
        ($ans)=split / /;
        if ( $ans=~/^y/i || $ans=~/^t/i || $ans=~/^1/ ) 
        {
            print "get_yn finds yes \n";
            return ($TRUE);
        }
        elsif  ( $ans=~/^n/i || $ans=~/^f/i || $ans=~/^0/ ) 
        {
            print "get_yn finds no \n";
            return ($FALSE);
        }
        else
        {
            print "get_yn finds invalid reply\n";
        }
    }
}

sub check_camp_status
{
# Checks the status of last camp command, looks for "invalid" and returns any numerical value or -1
#
# Input parameters:
#      $camp_status
#      $camp_answer

# Returns any numerical value found  or -1 if no numerical value

    my $camp_status = shift; # get the input parameters 
    my $camp_answer = shift; 
    my @fields;
    my $len;
    my $i;

    if($debug)
    {
	print("check_camp_status: camp_answer = \"$camp_answer\", camp_status=$camp_status\n"); 
    }
    if($camp_status != $CAMP_SUCCESS) 
    {
        print STDERR "camp.pl: error status from camp_cmd: $camp_status\n";
    } 
    if($camp_answer=~/^invalid/i)
    {
        print STDERR "camp.pl: camp_cmd returns message: $camp_answer \n";
    } 
    @fields=split("\n",$camp_answer);
    $len = $#fields; # array length
    #print("No. of fields found: $len+1\n");
      
    if($debug)
    {
	for ($i=0; $i<$len+1; $i++)  
	{
	    print "field $i: $fields[$i]\n";
	}
    }
# Any numerical reply seems to be in field[$len];
    $fields[$len] =~ /([\d]+)$/; # find digits anchored to end of string -> $1 
    if (defined $1)
    {
	if($debug){ print ("check_camp_status: Returning numerical return value: $1\n"); }
	return $1;
    }
    else
    {
# No numerical value anchored to the end of the string
	if($debug) { print ("check_camp_status: no numerical return value found\n"); }
	return -1;
    }
}


