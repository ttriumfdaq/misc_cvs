#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd
#             include  experiment    disable flag    hold   enable   beamline
#              path                run_number_check  flag   logging
# toggle.pl     *       bnmr2             n            n       y       bnmr
#  * = /home/bnmr/online/mdarc/perl
#
# Set a flag in odb (mdarc area) so that mdarc will toggle between real and test runs, deleting
# old run files in the process.
#
# $Log: toggle.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.11  2002/04/16 20:17:12  suz
# forgot enable logging in a message
#
# Revision 1.10  2002/04/16 20:02:38  suz
# add extra parameter: enable mdarc logging
#
# Revision 1.9  2002/04/15 17:16:35  suz
# add parameter include_path and support for musr pause
#
# Revision 1.8  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.7  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.6  2001/09/14 19:34:19  suz
# add MUSR support and 2 param; toggle not allowed if run on hold; use strict;imsg now msg
#
# Revision 1.5  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.4  2001/04/30 20:03:07  suz
# Add checks on Type 2 and automatic run number flag
#
# Revision 1.3  2001/03/01 19:20:25  suz
# output sent to /var/log/midas rather than /tmp
#
# Revision 1.2  2001/02/23 20:39:21  suz
# use new subroutine open_output_file
#
# Revision 1.1  2001/02/23 18:01:08  suz
# initial version
#
#
use strict;
######### G L O B A L S ##################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING $STATE_PAUSED);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
$STATE_PAUSED=2;  # Run state is Paused (MUSR only)
#########################################################

$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt, $dis_rn_check, $hold_flag, $enable_logging, $beamline ) = @ARGV;
my $name = "toggle";

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
my $nparam = 6; # expect 6 parameters
my $outfile = "/var/log/midas/toggle.txt";
my ($transition, $run_state, $path, $key, $status);
my $mdarc_path ;
my ($hold, $len, $eqp_name);
my $parameter_msg= "include_path; experiment; disable_run_number_check flag; hold flag; enable logging flag; beamline";
# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#
$nparam = $nparam + 0;  #make sure it's an integer
# Check the parameters:
#
#
if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  No experiment supplied \n";
    die  "$name:  FAILURE -  No experiment supplied \n";
}

open_output_file($name, $outfile);

print FOUT  "$name starting with parameters:  \n";
print FOUT  "    Experiment = $expt  Disable run number checking flag = $dis_rn_check\n";
print FOUT  "    Hold flag = $hold_flag; enable logging = $enable_logging; beamline: $beamline\n";
#
# check number of supplied parameters
$len = $#ARGV;  
$len += 1;
unless ($len == $nparam ) # expect nparam  parameters
{
    print      "$name:   Supplied parameters: @ARGV\n";
    print FOUT "$name:   Supplied parameters: @ARGV\n";
    print FOUT "Incorrect number of input parameters supplied ($len); expect $nparam  \n";
	print FOUT "   $parameter_msg\n"; 
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE - Incorrect no. of parameters supplied" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	print "Incorrect no. of input parameters supplied ($len); expect $nparam \n";
	die   "   $parameter_msg\n"; 
}
unless ($dis_rn_check eq "n") 
{
        print FOUT "INFO: Automatic run numbering is DISABLED. Cannot toggle \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: Automatic run numbering is DISABLED - toggle not allowed " ) ;
        die  "INFO:  Automatic run numbering is DISABLED. Toggle not allowed. \n"; 
}
unless ($enable_logging eq "y") 
{
        print FOUT "INFO: Logging is disabled. Cannot toggle \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: Logging is DISABLED - toggle not allowed " ) ;
        die  "INFO: Logging is DISABLED. Toggle not allowed. \n"; 
}
unless ( $expt =~ /2$/ || $expt =~ /musr$/i   )  # Type 2 experiment ( bnmr2, musr2  or musr ) 
{
        print FOUT "INFO: Toggle is supported only for Type 2 experiments \n"; 
        odb_cmd ( "msg","$MINFO","","$name", "INFO: toggle  is supported only for Type 2 experiments" ) ;
        die  "INFO: Toggle  is supported only for Type 2 experiments \n"; 
}
#
#
#
if( ($beamline =~ /bnmr/i)  )
  {
    # don't allow run on hold 
    #  (problem if the run is later stopped with hold and toggle on - no run file for new run number)  
    unless ($hold_flag eq "n") 
      {
	print FOUT "INFO: Toggle not allowed if run is on HOLD \n"; 
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run must be CONTINUEd before it can be toggled.  " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "INFO: Toggle not allowed if run is on HOLD  \n"; 
      }
  }
else
  {
    # MUSR experiments use midas PAUSE
    # check whether run is in progress
    ($run_state,$transition) = get_run_state();
    if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
    if ($run_state == $STATE_PAUSED)
      {   # Run is paused
	print FOUT "INFO: Toggle not allowed if run is PAUSED \n"; 
	($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run must be RESUMEd before it can be toggled.  " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "INFO: Toggle not allowed if run is PAUSED  \n"; 
      }
  }
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)  )
{
# BNMR experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";

#
# Check whether mdarc is running (after $EXPERIMENT is set up )
#
unless (mdarc_running() )   # subroutine to check if mdarc is running
{
    # no point in setting toggle

    print FOUT "Mdarc is not running. Toggle not valid. \n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Toggle valid only when mdarc is running" );     
    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    die      "$name: No toggle action if mdarc is not running";
}

# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going

    ($status) = odb_cmd ( "set","$mdarc_path","toggle" ,"y") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting toggle.\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting toggle" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Failure setting toggle\n";
    }
    else 
    { 
        print FOUT "$name: Success - toggle bit has been set in odb\n"; 
        ($status)=odb_cmd ( "msg","$MINFO","","$name", "Toggle bit has been set in odb" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    }
}
else
{    # run is stopped; no action
    print FOUT "Not running. Toggle has no action. \n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Run is not in progress. Toggle has no action" );
    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    print "Not running. Toggle has no action. \n";
}
exit;
