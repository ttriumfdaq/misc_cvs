#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from user button
# 
# invoke this script with cmd e.g.
#                  include_path                 experiment   select_mode      
# change_mode.pl  /home/bnmr/online/mdarc/perl     bnmr2         2a
#
#
# Links e.g.
# /alias/PPG2a& to  /ppg/PPG2a
#
#  Note: assumes & is appended to all /alias/ppg* to suppress new window
#     will not detect /alias/ppg1a without &
#
# $Log: change_mode.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.8  2003/01/20 22:44:59  suz
# add mode 10 (dummy freq scan)
#
# Revision 1.7  2003/01/08 18:37:10  suz
# add polb
#
# Revision 1.6  2002/06/07 20:51:50  suz
# add 1d
#
# Revision 1.5  2002/04/12 20:32:25  suz
# add parameter include_path
#
# Revision 1.4  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.3  2001/11/01 17:53:07  suz
# add cvs tag
#
#
#
######### G L O B A L S ##################
# status = 0 is success for odb
#our $ODB_SUCCESS = 0;
#our $EXPERIMENT = ""; 
#our $FAILURE = $FALSE = 0;
#our $SUCCESS = $TRUE = 1;
#our $DEBUG = $FALSE; # set to 1 for debug, 0 for no debug
#our $ANSWER = " ";   #reply from odb command
#our @ARRAY ;   #array contents used by get_array
#our $COMMAND = " "; # copy of command sent be odb_cmd (for error handling)
# run states:
#our $STATE_STOPPED = 1; # Run state is stopped
#our $STATE_PAUSED  = 2; # Run state is paused (error conditions for bnmr)
#our $STATE_RUNNING = 3; # Run state is running

#########################################################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
# cannot declare them on same line
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
#######################################################################
$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt, $select_mode ) = @ARGV;
my $name = "change_mode" ;

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/do_link.pl";

my $outfile = "/var/log/midas/change_mode.txt";

my $parameter_msg = "include path , experiment , select_new_mode";
my $nparam = 3;  # no. of input parameters
my ($transition, $run_state, $path, $key, $status);
my $input_path = "/Equipment/FIFO_acq/sis mcs/input/";
my $old_expt;
my ($ppg_path,$string,$len);
my ($process,$run_state,$transition);

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

$nparam = $nparam + 0;  #make sure it's an integer

# Check the parameter:
#
#
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile);


$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
    print FOUT "   Supplied parameters: @ARGV\n";
    print FOUT "Invoke this perl script $name with the parameters:\n";
    print FOUT "   $parameter_msg\n"; 
   
# only msg if there's an experiment
    if ($expt) 
    { 
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
    die  "$name:  FAILURE -  Too few parameters supplied ";
}

if ($expt) { $EXPERIMENT = $expt; }
else 
{ 
# print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  Invalid experiment supplied: $expt \n";
    die  "$name:  FAILURE -  Invalid experiment supplied ";
}

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt, select new mode = $select_mode\n";

unless ($select_mode)
{
    print FOUT "FAILURE: selected mode  not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  selected mode not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure:  selected mode not supplied \n"; } 
        die  "FAILURE:  selected mode  not supplied \n";
}

$select_mode =~ tr/A-Z/a-z/; # lower case

# make a few sanity checks:
if ( $expt =~ /2$/    )  # Type 2 experiment ( bnmr2 suz2 ) 
{
    unless ($select_mode =~ /^2/ || $select_mode =~ /^00/ )
    {
	print FOUT "$name: Incompatible mode name ($select_mode) and experiment type ($expt) \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Incompatible mode name ($select_mode) and experiment type ($expt) " ) ;
	die "$name: Failure:  Incompatible mode name ($select_mode) and experiment type ($expt) ";
    }
    unless ($select_mode=~/[abc]/  || $select_mode =~ /^00/ ) # modes 2a 2b 2c 00 exist
    {
	print FOUT "$name: Unknown  mode  ($select_mode) \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Unknown  mode ($select_mode) " ) ;
	die "$name: Failure:  Unknown  mode  ($select_mode)  ";
    }

}
elsif ( $expt =~ /1$/    )  # Type 1 experiment ( bnmr1, polb1 ) 
{
    unless ($select_mode =~ /^1/)
    {
    print FOUT "$name: Incompatible mode name ($select_mode) and experiment type ($expt) \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Incompatible mode name ($select_mode) and experiment type ($expt) " ) ;
    die "$name: Failure:  Incompatible mode name ($select_mode) and experiment type ($expt) ";
    }
    unless ($select_mode=~/[abcfnd0]/  )  # modes 1a 1b 1n 1f 1c 1d 10 exist (but will not all appear on status page for POLB1)
    {
	print FOUT "$name: Unknown  mode  ($select_mode) \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Unknown  mode ($select_mode) " ) ;
	die "$name: Failure:  Unknown  mode  ($select_mode)  ";
    }
}
elsif ( $expt =~ /pol$/i    )  #Polarimeter expt
{
    unless ($select_mode =~ /^1/)
    {
    print FOUT "$name: Incompatible mode name ($select_mode) and experiment type ($expt) \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Incompatible mode name ($select_mode) and experiment type ($expt) " ) ;
    die "$name: Failure:  Incompatible mode name ($select_mode) and experiment type ($expt) ";
    }
    unless ($select_mode=~/[nd0]/  )  # modes 1n 1d 10 exist
    {
	print FOUT "$name: Unknown  mode  ($select_mode) \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Unknown  mode ($select_mode) " ) ;
	die "$name: Failure:  Unknown  mode  ($select_mode)  ";
    }
}
elsif ( $expt =~ /polb/i    )  #PolarimeterB (or polb1)  expt (c.f. bnmr1)
{
    unless ($select_mode =~ /^1/)
    {
    print FOUT "$name: Incompatible mode name ($select_mode) and experiment type ($expt) \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Incompatible mode name ($select_mode) and experiment type ($expt) " ) ;
    die "$name: Failure:  Incompatible mode name ($select_mode) and experiment type ($expt) ";
    }
    unless ($select_mode=~/[0ndc]/  )  # modes 1n 1d 1f 1c exist
    {
	print FOUT "$name: Unknown  mode  ($select_mode) \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Unknown  mode ($select_mode) " ) ;
	die "$name: Failure:  Unknown  mode  ($select_mode)  ";
    }
}
else
{
    print FOUT "$name: Experiment $expt is not supported \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE: experiment $expt is not supported " ) ;
    die "$name: Failure:  experiment $expt is not supported ";
}



#
# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if($transition ) 
{ 
    print FOUT "Run is in transition. Try again later \n";
    print FOUT "Or set /Runinfo/Transition in progress to 0\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run is in transition. " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in transition. Try again later" ;
}

if ($run_state != $STATE_STOPPED) 
{   # Run is not stopped; 
    print FOUT "Run is in progress. Cannot change experimental mode\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run in progress. Cannot change experimental mode" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in  progress. Cannot change experimental mode " ;
}

 
($status, $path, $key) = odb_cmd ( "ls","$input_path","Experiment name " ) ;
unless ($status)
{ 
    print FOUT "$name: Failure from odb_cmd (ls); Error getting $input_path experiment name \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE getting experiment name" ) ;
    die "$name: Failure getting $input_path experiment name  ";
}
$old_expt=get_string($key);
$old_expt =~ tr/A-Z/a-z/; # lower case

print "Present experiment name =$old_expt\n";
print FOUT "Present experiment name =$old_expt\n";


# remove links from any of the other defined experiments if present
# type 2
if ( $expt =~ /2$/    )  # Type 2 experiment ( bnmr2 suz2 musr2 ) 
{
    unless ($select_mode =~ /2a/)
    {
	print FOUT "\n -------- removing link for 2a ------------\n";
	$status =  remove_a_link ("2a");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 2a\n"; 
	}
    }
    unless ($select_mode =~ /2b/)
    {
	print FOUT "\n -------- removing link for 2b ------------\n";
	$status =  remove_a_link ("2b");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 2b\n"; 
	}
    }
    unless ($select_mode =~ /2c/)
    {
	print FOUT "\n -------- removing link for 2c ------------\n";
	$status =  remove_a_link ("2c");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 2c\n"; 
	}
    }
    unless ($select_mode =~ /00/)
    {
	print FOUT "\n -------- removing link for 00 ------------\n";
	$status =  remove_a_link ("00");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 00\n"; 
	}
    }
}
elsif ( ($expt =~ /pol/i) ||  ($expt =~ /polb$/i)   )  #Polarimeter expt pol.polb
{
    unless ($select_mode =~ /10/)
    {
	print FOUT "\n -------- removing link for 10 ------------\n";
	$status =  remove_a_link ("10");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 10\n"; 
	}
    }
    unless ($select_mode =~ /1n/)
    {
	print FOUT "\n -------- removing link for 1n ------------\n";
	$status =  remove_a_link ("1n");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1n\n"; 
	}
    }
    unless ($select_mode =~ /1d/)
    {
	print FOUT "\n -------- removing link for 1d ------------\n";
	$status =  remove_a_link ("1d");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1d\n"; 
	}
    }
    unless ($select_mode =~ /1c/)
    {
	print FOUT "\n -------- removing link for 1c ------------\n";
	$status =  remove_a_link ("1c");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1c\n"; 
	}
    }

}
else    # type 1   BNMR1  POLB1
{
    unless ($select_mode =~ /1a/)
    {
	print FOUT "\n -------- removing link for 1a ------------\n";
	$status =  remove_a_link ("1a");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1a\n"; 
	}
    }
    unless ($select_mode =~ /1b/)
    {
	print FOUT "\n -------- removing link for 1b ------------\n";
	$status =  remove_a_link ("1b");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1b\n"; 
	}
    }
    unless ($select_mode =~ /1f/)
    {
	print FOUT "\n -------- removing link for 1f ------------\n";
	$status =  remove_a_link ("1f");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1f\n"; 
	}
    }
    unless ($select_mode =~ /1n/)
    {
	print FOUT "\n -------- removing link for 1n ------------\n";
	$status =  remove_a_link ("1n");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1n\n"; 
	}
    }
    unless ($select_mode =~ /1d/)
    {
	print FOUT "\n -------- removing link for 1d ------------\n";
	$status =  remove_a_link ("1d");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1d\n"; 
	}
    }
    unless ($select_mode =~ /1c/)
    {
	print FOUT "\n -------- removing link for 1c ------------\n";
	$status =  remove_a_link ("1c");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 1c\n"; 
	}
    }
    unless ($select_mode =~ /10/)
    {
	print FOUT "\n -------- removing link for 10 ------------\n";
	$status =  remove_a_link ("10");
	unless($status) 
	{    # write a message to file and continue 
	    print FOUT "$name: Error return from remove_a_link for expt 10\n"; 
	}
    }
}


# check if same expt. is already set up (check underscore though this should now be obsolete)
if($old_expt eq $select_mode) # both are lower case
{ 
    print FOUT "\n -------- old and new modes are the same  ------------\n";
    $status = test_a_link ($select_mode);
    unless ($status) 
    { 
    print FOUT "\n -------- making a link for $select_mode  ------------\n";
	$status =  make_a_link ($select_mode);
	unless($status) 
	{    
	    print FOUT "$name: Error return from make_a_link for $select_mode\n"; 
	    die "$name: Error return from make_a_link for $select_mode\n ";
	}
    }
    else
    {
	print FOUT "$name: Experiment $old_expt is set up already\n";
	print      "$name: Experiment $old_expt is set up already\n";
	odb_cmd ( "msg","$MINFO","","$name","INFO: experiment $old_expt is set up already");
    }
}
else
{
# make a new link for this experiment
    print FOUT "\n -------- making new link for $select_mode  ------------\n";
    $status =  make_a_link ($select_mode);
    unless($status) 
    {    # don't change the experiment name 
	print FOUT "$name: Error return from make_a_link\n"; 
	die "$name: Error return from make_a_link\n ";
    }

    print FOUT "\n -------- update experiment name  ------------\n";
    ($status) = odb_cmd ( "set","$input_path","Experiment name" ,$select_mode) ;
    unless($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $input_path experiment name \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting experiment name" ) ;
	die "$name: Failure setting $input_path experiment name  ";
    }
    print FOUT "INFO: Success -  experiment (& ppg mode) $select_mode is now selected \n";
    odb_cmd ( "msg","$MINFO","","$name", "INFO: experiment (& ppg mode) $select_mode is now selected " );
    print  "INFO: Success - experiment (& ppg mode)  $select_mode is now selected  \n";
}

exit;




















