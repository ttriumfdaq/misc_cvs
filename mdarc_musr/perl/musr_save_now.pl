#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#    MUSR only - save the histograms now
#
#   Normally invoked from "Save" button
# 
# invoke this script with cmd
#                   include_path                    experiment   beamline
# musr_save_now.pl  /home/musrdaq/online/mdarc/perl      musr2     dev
#
# Writes to "save now" in mdarc's musr area in odb.
# "save now" is hot linked in mdarc.c so that the histograms will be
# saved immediately. It doesn't matter what is written to "save now".
#
# $Log: musr_save_now.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.1  2002/04/14 03:48:17  suz
# original: causes mdarc to save histos now
#

use strict;


##################### G L O B A L S ####################################
#  "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
#our $ODB_SUCCESS = 0;
#our $EXPERIMENT = ""; 
#our $FAILURE = our $FALSE = 0;
#our $SUCCESS = our $TRUE  = 1;
#our $DEBUG = $FALSE; # set to 1 for debug, 0 for no debug
#our $ANSWER = " ";   #reply from odb command
#our @ARRAY ;   #array contents used by get_array
#our $COMMAND = " "; # copy of command sent be odb_cmd (for error handling)
# run states:
#our $STATE_STOPPED = 1; # Run state is stopped
#our $STATE_RUNNING = 3; # Run state is running
#
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw
# cannot declare them on same line

use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING $STATE_PAUSED);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);
$MERROR=1; # error
$MINFO=2;  # info
$MTALK=32; # talk
$FALSE=$FAILURE=0;
$TRUE=$SUCCESS=1;
$ODB_SUCCESS=0;   # status = 0 is success for odb
$DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
$EXPERIMENT=" ";
$ANSWER=" ";      # reply from odb_cmd
$COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
$STATE_STOPPED=1; # Run state is stopped
$STATE_RUNNING=3; # Run state is running
$STATE_PAUSED=2;  # Run state is paused (MUSR only)
#######################################################################

#########################################################

$|=1; # flush output buffers

# input parameters:
my ($inc_dir, $expt, $beamline ) = @ARGV;
my $name = "save_now";
# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 


my $nparam=3;
my $outfile = "/var/log/midas/save_now.txt";
my ($transition, $run_state, $path, $key, $status);

my $parameter_msg = "include_path; experiment; beamline\n";
my ($mdarc_path,$len) ;
my $debug=$FALSE;
my $eqp_name;
my $value = $TRUE;
#
# Output will be sent to file $outfile
# because this is for use with the browser and STDOUT and STDERR get set to null

$nparam = $nparam + 0;  #make sure it's an integer
#
# odb  msg cmd:
#                 use 1=error 2=info  32=talk
#
# Check the parameters:
#
#
if ($expt) { $EXPERIMENT = $expt; }
open_output_file($name, $outfile);
$len = $#ARGV; # array length
$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters suppled = $len\n";}
unless ($len == $nparam)
{
# print to file and to screen, then exit. Cannot msg without an experiment. 
  print  "$name:   Supplied parameters: @ARGV\n";
  print  "Invoke this perl script $name with the parameters:\n";
  print  "   $parameter_msg\n"; 
  print FOUT "$name:  FAILURE -  Too few input parameters supplied \n";
  print FOUT "   Supplied parameters: @ARGV\n";
  print FOUT "Invoke this perl script $name with the parameters:\n";
  print FOUT "   $parameter_msg\n"; 
  
  # only msg if there's an experiment
  if ($expt) 
    { 
      ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
      unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
  die  "$name:  FAILURE -  Too few parameters supplied ";
}
unless ($expt) 
  { 
    # print to file and to screen, then exit. Cannot msg without an experiment. 
    print FOUT "$name:  FAILURE -  No experiment supplied \n";
    die  "$name:  FAILURE -  No experiment supplied ";
  } 


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; beamline = $beamline\n";
#

# write to (hot-linked) save now
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bnmr/i)  )
  {
    # Not supported for BNMR experiment
    odb_cmd ( "msg","$MINFO","","$name", "INFO: No support for BNMR experiments" ) ;
    print FOUT "$name: no support for BNMR experiment";
    die "$name: no support for BNMR experiment";
  }

# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
$mdarc_path = "/Equipment/$eqp_name/mdarc/histograms/musr/";

print FOUT "mdarc_path = $mdarc_path\n";

($status) = odb_cmd ( "set","$mdarc_path","save now" ,"$value") ;
unless($status)
  { 
    print FOUT "$name: Failure from odb_cmd (set); Error updating $mdarc_path save now.\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE updating $mdarc_path save now flag" ) ;
    die "$name: Failure updating $mdarc_path save now";
  }
print FOUT "INFO: Success - save now flag has been touched \n";
odb_cmd ( "msg","$MINFO","","$name", "INFO: save now flag has been touched " );
print "Success - Save now flag has been touched \n";

exit;

