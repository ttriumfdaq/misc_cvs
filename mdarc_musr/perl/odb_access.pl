# subroutines for odb access
#
###############################################################################################
#    standard odb access subroutines
#
# Note: these subroutines are included by the main perl script with the command:
#
#                   require "odb_access.pl";
#
#       The main program is expected to define the following globals:
##            ######### G L O B A L S ##################
##            status = 0 is success for odb
##            our $ODB_SUCCESS = 0;
##            our $EXPERIMENT = ""; 
##            our $FAILURE = $FALSE = 0;
##            our $SUCCESS = $TRUE = 1;
##            our $DEBUG = $FALSE; # set to 1 for debug, 0 for no debug
##            our $ANSWER = " ";   #reply from odb command
##            our @ARRAY    #array contents used by get_array
##            our $COMMAND = " "; # copy of command sent be odb_cmd (for error handling)
##            # run states:
##            our $STATE_STOPPED = 1; # Run state is stopped
##            our $STATE_PAUSED  = 2; # Run state is paused (error condition for bnmr, MUSR uses this)
##            our $STATE_RUNNING = 3; # Run state is running
##################################################################################################
#
# $Log: odb_access.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.15  2002/04/16 17:42:02  suz
# fix a debug statement
#
# Revision 1.14  2002/04/15 18:48:59  suz
# add pause for musr; add suppress to open_output_file
#
# Revision 1.13  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.12  2001/11/01 17:56:15  suz
# handle another appended string in get_string
#
# Revision 1.11  2001/10/04 19:22:00  suz
# fix bug adding trailing / to path when key is blank
#
# Revision 1.10  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.9  2001/09/14 19:22:52  suz
# use strict;imsg now msgmodify get_string
#
# Revision 1.8  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.7  2001/04/30 20:01:47  suz
# Add support for midas logger
#
# Revision 1.6  2001/03/30 18:59:35  suz
# add a debug statement only
#
# Revision 1.5  2001/03/01 18:58:56  suz
# Changes to open_output_file. Delete any old copy of outfile, and change
# permissions to allow others to delete it.
#
# Revision 1.4  2001/02/23 20:37:14  suz
# print statement changed in open_output_file
#
# Revision 1.3  2001/02/23 20:32:22  suz
# add subrouting open_output_file
#
# Revision 1.2  2001/02/23 19:56:27  suz
# ensure get_bool returns an integer value
#
# Revision 1.1  2001/02/23 17:55:04  suz
# initial version
#
#
use strict;
# globals
#   "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
#our ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
#our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS) ;
#our ( $DEBUG, @ARRAY);
#our ( $STATE_STOPPED, $STATE_RUNNING, $STATE_PAUSED);

 ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
use vars  qw ( $TRUE $FALSE $FAILURE $SUCCESS) ;
use vars  qw ( $DEBUG @ARRAY);
use vars  qw ( $STATE_STOPPED $STATE_RUNNING $STATE_PAUSED);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);

sub odb_cmd
{
# executes an odb command
#
#  input parameters:
#   $cmd    odb command to execute e.g. ls
#   $path   full path of directory of odb variable 
#                  e.g. /equipment/MUSR_TD_ACQ/Settings/mode/histograms
#   $key    name of odb variable
#   $value  value to write to odb parameter ( for odb command "set")
#
#   $EXPERIMENT global  experiment
#
#   output parameters:
#   globals:
#   $ANSWER  global   string returned from odb
#   $COMMAND command string for error messaging
#
#   status           SUCCESS or FAIL
#   $path   
#   $key
#   
#   subroutine returns 1 for SUCCESS,  0 for FAIL

# note :  $cmd    cleaned-up parameters from get_command_string

#    my $cmd  = $_[0];
#    my $path = $_[1];
#    my $key  = $_[2];
#    my $value1 = $_[3];
#    my $value2 = $_[3];   (only used for msg cmd at present )

    my ($orig_cmd, $orig_path, $orig_key, $value1, $value2 ) = @_; # get the parameters
    my ($command,$path,$key,$cmd,$status);
    my $debug=0;
    if($debug)    
    { 
        print FOUT  "sub odb_cmd receives parameters:\n";
        print FOUT  "path=$orig_path; key = $orig_key; cmd = $orig_cmd; value1 = $value1; value2 =$value2\n";
    }
    
#   get_command_string cleans any leading/trailing spaces, extra slashes out of $path,$key, $cmd
    ( $command, $cmd, $path, $key)  = get_command_string($orig_cmd,$orig_path,$orig_key,$value1,$value2);
    if($debug)
    { 
        print FOUT  "After get_command_string, command:$command\n"; 
        print FOUT  "       and cmd=$cmd; path =$path; key =$key;\n";
    }
    
    $COMMAND ="`odb -e $EXPERIMENT -c $command` ";  # save this in case of error

    $ANSWER=`odb -e $EXPERIMENT -c $command`;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed

# print information for now:
    print FOUT "command: $COMMAND\n";
    print FOUT "answer : $ANSWER\n"; 

    if($status != $ODB_SUCCESS) 
    {
        print FOUT   "odb_cmd: Failure status returned from odb \n";
        return($FAILURE, $path, $key);
    } 
    
# look for the most common error strings (these return with $ODB_SUCCESS) and flag them as failures

    $_=$ANSWER;
    if ( /usage/i)
    {
        print FOUT "odb_cmd: detected key word : usage; -> unknown comand\n"; # unknown command
        return($FAILURE, $path, $key);
    }
    elsif ( /unknown/i)
    {
        print FOUT "odb_cmd: detected key word : unknown; -> unknown comand\n"; # unknown command
        return($FAILURE, $path, $key);
    }
    elsif (/\?/i) 
    { 
        print FOUT "odb_cmd: detected key word : ?; odb probably stuck...\n"; # odb probably stuck waiting for a reply
        return($FAILURE, $path, $key);
    }  
    elsif (/not found/i) 
    {
        unless (/analyzer/i) # analyzer not found is OK (make command) 
        {
            print FOUT "odb_cmd: detected key words \"not found\" in reply\n";
            print FOUT "  so command may not have worked as expected\n";
            return($FAILURE, $path, $key);
        } 
    }
    elsif (/is of type/i) 
    {
	print FOUT "odb_cmd: detected key words \"is of type\" in reply\n";
	print FOUT "  so command probably did not work as expected\n";
	return($FAILURE, $path, $key);
    }

 

#       For ls, odb should reply with the key at the beginning of the string.
    if($cmd eq "ls")  # use eq for strings
    {
        
        #unless ($ANSWER=~/^$key/i)
        unless (/^$key/i)
        {
            print FOUT   "odb_cmd: Unexpected reply with command ls after reading $key \n";
   #         print FOUT   "         odb replied: $ANSWER\n";
            return($FAILURE , $path, $key); 
        } 
    }
    
    #print FOUT  "returning success from odb_cmd\n";
    return ($SUCCESS, $path, $key);
}

##############################################################
sub get_array
##############################################################
{
#   removes key from $ANSWER and splits contents into @ARRAY
#
#   input:   key
#            $ANSWER (global)
#   output:  @ARRAY  (global)
#
    my $key   = $_[0];

    if ($DEBUG) { print FOUT  "get_array: key= $key\n"; }
    if($DEBUG) { print FOUT "answer = $ANSWER\n"; }
    $key =~ tr/A-Z/a-z/; # translate to lower case (in case key is not correct case)
    $_=$ANSWER; # move into default variable
  # use i flag instead  tr/A-Z/a-z/; # translate ANSWER to lower case
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER, ignoring case 


    @ARRAY = split / /;  # fill global ARRAY
    if($DEBUG) { print FOUT "array = @ARRAY\n"; }
    return;
}
##############################################################
sub get_int
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: return value is the integer contents of $ANSWER
#
    my $key   = $_[0];
    my $integer = -1;
    my $debug=0; # no debug
    if($debug) 
    { 
        print FOUT "get_int starting with key: $key\n"; 
        print FOUT "        and ANSWER: $ANSWER\n"; 
    }
   # $key =~ tr/A-Z/a-z/; # translate to lower case  (in case key is not correct case)
    $_=$ANSWER; # move into default variable
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER (case independent) 
    split / /;
    $_=$_+0; #make sure it's an integer by doing an integer add
    if($debug) { print FOUT "get_int returning value=$_\n"; }
    return ($_);
}

##############################################################
sub get_bool
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: return value is TRUE or FALSE depending on $ANSWER
#
    my $key   = $_[0];

    if($DEBUG) 
    { 
        print FOUT "get_bool starting with key: $key\n"; 
        print FOUT "        and ANSWER: $ANSWER\n"; 
    }
    $key =~ tr/A-Z/a-z/; # translate to lower case  (in case key is not correct case)
    $_=$ANSWER; # move into default variable
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER,ignoring case 
    split / /;
    s/y/$TRUE/;
    s/n/$FALSE/;
    $_=$_+0; #make sure it's an integer by doing an integer add
    if($DEBUG) { print FOUT "get_bool returning value = $_\n"; }
    return ($_);
}


##############################################################
sub get_string
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: string contents of $ANSWER (with key stripped off)
#           message (that may be appended to end of $ANSWER string) 
#
    my $key   = $_[0];
    my $string = "blank";
    my $msg = "";

    if($DEBUG) 
    { 
        print FOUT "get_string starting with key: $key\n"; 
        print FOUT "        and ANSWER: $ANSWER\n"; 
    }
    $_=$ANSWER; # move into default variable

    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER, ignoring case 

# test only
##    $_ = $_ . "  cannot blah blah  "; # add rubbish to string for testing $msg
##    $_ = $_ . " 11:34:56 garbage  "; # add rubbish to string for testing $msg


# sometimes $ANSWER will have a string like this appended:
# "Cannot open message log file /home/bnmr/online/bnmr2/midas.log"
# or
# "11:14:49 [ODBEdit] Warning: The RF has tripped"
# strip these off into $msg
#
#   may find other messages occur - if so add them in later


#   adds ";;" to split string - hopefully no normal strings use ";;"

#   look for time stamp e.g. 11:14:49
    if( /\d\d:\d\d:\d\d/ )
    {
	if($DEBUG) { print FOUT "found date string appended \n"; }
	s /(\d\d:\d\d:\d\d)/;;\1/; # save string with parentheses -> \1
	if($DEBUG) { print FOUT "string:\'$_\'\n"; }
    }
#   look for "Cannot "
    elsif (/Cannot /i)
    {
	if($DEBUG) { print FOUT "found \'Cannot \' string\n"; }
	s /(Cannot )/;;\1/i;  # save string with parentheses -> \1
	if($DEBUG) { print "string:\'$_\'\n"; }
    }
    ($_,$msg)=split(/;;/); # split by added ";;"

    if($DEBUG)
    {
	print FOUT  "After split, string:\'$_\'\n";
	print FOUT  " and msg:\'$msg\'\n";
    }
    s/^ *//; # strip spaces from beginning of string
    s/ *$//; # and end
    
    print FOUT "get_string returns:\"$_\" \n";

    unless($msg eq "")
    {
	print FOUT "   and msg:\"$msg\" \n";              
    }
    
    return ( $_, $msg );
}


#===============================================================================
#
# get_command_string
#
#===============================================================================

sub get_command_string
{
  my ($cmd, $path, $key, $value1, $value2) = @_; # get the parameters
  my ($command,$value,$fullpath);

  my $debug=0; # no debug

  if($debug)
  {
      print FOUT  "get_command_string - Input values:\n";
      print FOUT  "path =$path ";
      print FOUT  "key =$key\n";
      print FOUT  "cmd=$cmd ";
      print FOUT  "value1 =$value1";
      print FOUT  "value2 =$value2\n";
  }
#Strip leading/trailing blanks from cmd
$cmd =~ s/ *$//; #remove trailing spaces
$cmd =~ s/^ *//; # and leading spaces

# and from $value1 & 2
$value1 =~ s/ *$//; #remove trailing spaces
$value2 =~ s/ *$//; #remove trailing spaces
$value1 =~ s/^ *//; # and leading spaces
$value2 =~ s/^ *//; # and leading spaces


if ($value2)
{
    if ($value1) {  $value = "$value1\" \"$value2";} # something in $value
    else { $value = $value2; }
}

else
{
    if ($value1) {   $value = $value1; } # something in $value1 only
    else { $value=""; }
}

if ($debug) { print FOUT  "value:$value\n";}

# and from $value1 & 2
$value =~ s/ *$//; #remove trailing spaces
$value =~ s/ *$//; #remove trailing spaces

# Remove spaces from key and path as well
$path =~ s/ *$//; #remove trailing spaces
$path =~ s/^ *//; # and leading spaces
$key =~ s/ *$//; #remove trailing spaces
$key =~ s/^ *//; # and leading spaces

# Sort out path :
if($debug) { print FOUT  "path =$path\n";}
if($path)
{
    if($key) # if key is blank, do not add trailing slash to path
    {        # path & key are not blank
	unless($path=~ m@/$@)   # if no trailing slash ...
	{ 
	    if ($debug) { print FOUT   "Adding a trailing slash\n";} 
	    $path=~ s@$@/@;
	}
	else { if($debug) { print FOUT  "matched a trailing slash\n";} }
    }
    else { if ($debug) { print FOUT  "key is blank; NOT adding trailing slash to path\n";} }
}
if($debug) { print FOUT  "path =$path\n";}

# sort out $key :
if ($debug) { print FOUT  "key =$key\n"; }

if ($key)
{       # if key is not blank
    # if path is not blank, do not allow leading slash 
    if ($path) { $key=~ s@^/@@; } 
    else { if ($debug) { print FOUT  "path is blank; allowing a leading / on key\n";} }
}
if ($debug) { print FOUT  "key =$key\n"; }

$fullpath = "$path$key";
if ($debug) {print FOUT  "fullpath =$fullpath\n";}

if ($fullpath)
{       # fullpath exists
    if($value)  { $command = "\'$cmd \"$fullpath\" \"$value\"\'";} # something in $value
    else  {  $command = "\'$cmd \"$fullpath\" \' "; } # value is blank
}
else
{       # fullpath is blank
    if ($value)  { $command = "\'$cmd  \"$value\" \'";}  # there is something in $value
    else  { $command = "\'$cmd\'";}  # $value is blank
}
if($debug)
{ 
    print FOUT  "get_command_string returns command=$command\n";
    print FOUT  "     and cmd=$cmd; key=$key; path=$path;\n";
}

return ($command,$cmd,$path,$key);
}

########################################################################################
sub get_run_state
##############################################################
{
# is a run in progress?
#
# inputs:
#     none
# outputs:
#     run state  
#     transition  
#
#
# run state  :   1 = not running  3 = running
# transition in progress  = 1 if true
    my $this_path = "/Runinfo";
    my $key_state = "state";
    my $key_trans = "Transition in progress";
    
    my $run_state = 0; #initialize
    my $trans = 0; 
    my $debug = 0; # set debug

    my ($status,$path,$key,$cmd);
    
    print FOUT  "get_run_state starting...\n";
    ($status,$path,$key) = odb_cmd ( "ls","$this_path","$key_state" );
    unless  ($status) { die "Failure accessing $path/$key from odb_cmd\n"; }
    $run_state = get_int ( $key);
    
    ($status,$path,$key)= odb_cmd ( "ls","$path","$key_trans" );
    unless ($status ) { die "Failure accessing $path/$key_trans from odb_cmd\n"; }
    $trans = get_int ( $key);
    
    if ($debug) { print FOUT  "Transition = $trans; Run state = $run_state \n"; }
    
    if ($trans == 1 ) { if ($DEBUG) { print FOUT  "Transition in progress";}  }
    $_[0]=$trans;    
    $_[1]=$run_state;
    
    if($debug)
    {
        if ($run_state == $STATE_STOPPED)  { print FOUT  "Run is stopped     \n"; }
        else                               {  print FOUT  "Run is not stopped     \n";  }
    }
    print FOUT  "get_run_State returning run_state=$run_state, trans=$trans\n";
    return ($run_state, $trans) ;
}


sub set_run_number
{
#  inputs:   new_run_number
#
#  returns   status
#
#
    my $new_run_number = shift;
    my $name = "set_run_number";
    my ($status,$rn,$key,$path);

    print FOUT  "$name:  Attempting to set run number to $new_run_number\n"; 
    
# set the new run number
    ($status) = odb_cmd ( "set"," /Runinfo"," /Run number  " ,"$new_run_number" ) ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error resetting run number to $new_run_number\n";
        print "$name: Could not set new run number.";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE resetting run number" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        return ($FAILURE);
    }
    
# Read it back to check
    ($status,$path,$key) = odb_cmd ( "ls"," /Runinfo"," /Run number  "  ) ;
    unless($status)
    {
        print FOUT "$name: Failure from odb_cmd (ls); Error reading new run number from odb\n";
        print  "$name: Error reading back new run number from $path/$key";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE reading back new run number " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        return ($FAILURE);
    }
    
    $rn = get_int ( $key);
    if ($rn != $new_run_number)
    {
        
        print FOUT  "Unexpected run number read back. Got $rn, expected $new_run_number\n";
        print "Unexpected run number read back. Got $rn, expected $new_run_number\n";
        ($status)= odb_cmd ( "msg","$MERROR","","$name", "FAILURE - Unexpected run number read back " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        return ($FAILURE);
    }
    return ($SUCCESS);
}

sub mdarc_running
{
    # check whether mdarc is running

# $EXPERIMENT must be set up before running this
# also uses globals $ANSWER, $TRUE, $FALSE

    my $name = "mdarc_running";
    my $status;

    ($status) = odb_cmd ( "scl" ) ;
#    print "status = $status\n";
    unless ($status) { exit_with_message($name);}
    
# print "after scl, answer = $ANSWER\n"; 
    if ( $ANSWER =~/mdarc/i) { return( $TRUE); }
    else { return ($FALSE) ;}
}

sub mlogger_running
{
    # check whether mlogger is running & it is enabled
    # in odb

# $EXPERIMENT must be set up before running this
# also uses globals $ANSWER, $TRUE, $FALSE

    my $name = "mlogger_running";
    my ($status,$path,$key);
    my $mlogger_path = "/logger/Channels/0/Settings";
    my ($write_data,$active);

    ($status) = odb_cmd ( "scl" ) ;
#    print "status = $status\n";
    unless ($status) { exit_with_message($name);}
    
# print "after scl, answer = $ANSWER\n"; 
    unless ( $ANSWER =~/logger/i) { return( $FALSE); }


#  Make sure the logger is turned on

    #   Read the status of the midas logger from odb
    ($status, $path, $key) = odb_cmd ( "ls"," /logger",  "write data " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading key /logger/write data from odb\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read /logger/write data from odb" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die  "$name: Error reading key /logger/write data  from odb";
    }
    $write_data = get_bool($key);
    unless ( $write_data )
    {
	print FOUT "$name: odb key /logger/write data is set false. Midas logger is not enabled\n";
    }

    #   Read also status of the midas logger channel 0 from odb
    ($status, $path, $key) = odb_cmd ( "ls"," $mlogger_path ",  "Active " ) ;
    unless ($status) 
    { 
	print FOUT "$name: Error reading key  $mlogger_path/Active  from odb\n";
	($status)=odb_cmd ( "msg","$MERROR", "","$name", "FAILURE: Could not read  $mlogger_path/Active  from odb" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "$name: Error reading key $mlogger_path/Active from odb";
    }
    $active = get_bool($key);
    unless ( $active )
    {
	print FOUT "$name: odb key  /logger/Channels/0/Settings/Active is set false. Midas logger is not enabled\n";
    }
    unless ($active && $write_data)
    {
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING: Midas logger is not enabled in odb " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	return ($FALSE) ;
    }
    return ($TRUE);
}

sub exit_with_message
{
    my $name = shift;
    my $status;

    unless ($name) { $name = "perl script";}
    print FOUT "\n $name is exiting with an error condition\n";
# send a message (and hope it works) 
    ($status)=odb_cmd ( "msg","$MINFO", "","$name", "INFO: Exiting after an error condition" ) ;
    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    die "$name exiting with an error condition\n";
}

sub open_output_file
{
# parameters:
#       $name     name of calling subroutine
#       $outfile  name of file to open
#       $suppress TRUE if odb message about output file location is to be suppressed
#                 - useful when perlscript NOT run from the browser
#           

    my $name    = shift; 
    my $outfile = shift ;
    my $suppress = shift ;

    my $found_flag=0;
    my $deleted_flag=0;
    my $debug = $FALSE;
    my $status;


    if ($debug) { print   ("open_output_file starting with name = $name, outfile = $outfile, suppress = $suppress\n"); }
    if(-e  $outfile) # test for existence of a file
    {
        if ($debug) { print  "open_output_file: found previous copy of $outfile\n"; }
        $found_flag=1;
        unless (unlink ($outfile)) 
	{ 
	    if($debug) { print "open_output_file:  Couldn't delete old copy of $outfile\n"; }
	}
        else 
        { 
            if ($debug) { print  "open_output_file:  Deleted old copy of $outfile\n"; } 
            $deleted_flag=1;
        }
    }
    else
    {
        if ($debug) { print  "open_output_file:  Found no previous copy of $outfile\n"; }
    }       
    
    unless (open (FOUT,">$outfile"))
    {
        ($status) = odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Could not open output file $outfile" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "open_output_file:   Failure opening output file $outfile"; 
    }
    if ($debug)
    {
        if($found_flag) { print FOUT "open_output_file: found previous copy of $outfile\n";}
        else            { print FOUT "open_output_file:  Found no previous copy of $outfile\n"; }
        if($deleted_flag) { print FOUT "open_output_file: deleted previous copy of $outfile\n";}
        else            { print FOUT "open_output_file: Could not delete  previous copy of $outfile\n"; }
    }


    print "$name:  All output will be sent to file $outfile\n";
    unless ($suppress)
    {
        ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: All informational output is in file $outfile" ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
    }   
# change permissions to world read and write so others can run these perlscripts without error deleting the file
    unless ( chmod(0666,$outfile) ) 
    { 
        print "open_output_file:  Couldn't change permissions on $outfile\n"; 
        print FOUT "open_output_file:  Couldn't change permissions on $outfile\n"; 
        odb_cmd ( "msg","$MINFO","","$name", "WARNING: Could not change permissions on file $outfile." ) ;
    }
    return;
}
# 
#  Check two directory strings are the same
#
sub check_directories($dir_A,$dir_B)
{
    my $name="check_directories";
    my ($dir_A, $dir_B) = @_;
    my (@elem_B,@elem_A,$a,$b);
    my $debug=$FALSE;

    if ($debug) { print "$name: parameters dir_A: $dir_A, midas dir_B: $dir_B\n"; }
    print FOUT "$name: parameters dir_A: $dir_A, midas dir_B: $dir_B \n";
    
#
#  Check directory of last_saved_filename  is the same as dir_A
#
    @elem_B = split (/\//,$dir_B); # split by /
    @elem_A  = split (/\//,$dir_A);           # split by /
    
# check array lengths 
    if (  ($#elem_A + 0 ) != ($#elem_B + 0) ) 
    { 
        print FOUT "$name: array lengths of directories $dir_A and $dir_B don't match\n";
        return ($FAILURE);  # they must match
    }
    
# make sure that directories are the same.
    
    while ( $a = pop (@elem_A) )   # pop last element from @elem_A  
    { 
        $b = pop (@elem_B);       # pop last element from @elem_B
        
	if ($debug) { print "array element from dir_A = $a, & from  dir_B  = $b\n "; }
        unless ($a eq $b) 
        { 
        print FOUT "$name: no match between $a and $b (elements of the two directory stings)\n"; 
        return ($FAILURE);  # no information available on last saved run
    }
} 

    print FOUT "$name: Success: saved directories are matching\n";
    return ($SUCCESS);
}

# IMPORTANT
# this 1 is needed at the end of the file so require returns a true value   
1;   


