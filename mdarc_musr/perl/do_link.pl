# do_link.pl 
#
# subroutines to make and remove links
#     called by change_mode.pl
#
#
# $Log: do_link.pl,v $
# Revision 1.1  2003/10/07 22:18:22  suz
# original for mdarc_musr cvs tree
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/11/01 17:53:58  suz
# add cvs tag
#
#
use strict;
# globals (main program defines these)
# our not supported
#our ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
#our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS) ;
#our ( $DEBUG, @ARRAY);
#our ( $STATE_STOPPED, $STATE_RUNNING);
#
use vars  qw($ODB_SUCCESS $EXPERIMENT $FALSE $FAILURE $SUCCESS $TRUE $FALSE);
use vars  qw($DEBUG $ANSWER @ARRAY $COMMAND $STATE_STOPPED $STATE_RUNNING);
# for odb  msg cmd:
use vars  qw($MERROR $MINFO $MTALK);

#our $debug =0;
use vars '$debug';
$debug=0;

sub make_a_link 
{
#
# If expt is "1b_", calls 
#            remove_a_link to remove /alias/ppg1b if present,
# then   makes links /ppg/ppg1b to  /alias/ppg1b
# 
# input parameter:  experiment_name
# returns           $SUCCESS or $FAILURE
    
    my ($expt_name) = @_; # get the input parameters
    my ($ppg_name,$status);
    my $name="make_a_link";
    
    
    if($debug) { print ("$name: input params: expt name = $expt_name\n");}
    print FOUT ("$name: input params: expt name = $expt_name\n");
    $expt_name =~ s/_$//;  # remove any trailing underscore
    
    
# check if link exists - if so, remove it
    ($status)= remove_a_link ($expt_name);
    unless ($status)
    {
	print FOUT "$name: Failure returned from remove_a_link for $expt_name \n";
	print      "$name: Failure returned from remove_a_link for $expt_name \n";
	return ($status);
    }
    
# make the link
    
    $ppg_name=sprintf("PPG$expt_name"); # no appended & here! 
    ($status) = odb_cmd ( "ln","/ppg","/$ppg_name" ,"/alias/$ppg_name&") ;
    
#print "status=$status\n";
    unless($status)
    { 
	print FOUT "$name: Failure from odb_cmd (ln). Could not link /ppg/$ppg_name to /alias/$ppg_name& \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE making link for $ppg_name " ) ;
	print "$name: Failure linking   /ppg/$ppg_name to /alias/$ppg_name& \n";
	return ($status);
    }
    else
    {
	print FOUT "$name: Successfully linked   /ppg/$ppg_name to /alias/$ppg_name& \n";
	#odb_cmd ( "msg","$MINFO","","$name", "SUCCESS making link for $ppg_name  " ) ;
	print "$name: Success linking  /ppg/$ppg_name to /alias/$ppg_name&  \n";
    }   
    return($status);
}



sub remove_a_link
{
#
# If expt is "1b_", checks if link /alias/ppg1b& is present and removes it
# 

    # input parameters:
    my ($expt_name) = @_; # get the input parameters
# returns $SUCCESS or $FAILURE
    
    my ($ppg_name,$status,$string,$key,$temp_name);
    my $name="remove_a_link";
    my $msg;

    if($debug) { print ("$name: input params: expt name = $expt_name\n");}
    print FOUT ("$name: input params: expt name = $expt_name\n");
    $expt_name =~ s/_$//;  # remove any trailing underscore
    
    $ppg_name=sprintf("ppg$expt_name&");
    if($debug) { print ("$name:  ppg_name = $ppg_name\n");}
    print FOUT ("$name:  ppg_name = $ppg_name\n");

    
#  Check if link exists e.g.  /alias/ppg2b& (does not detect /alias/ppg2b)

   ($status) = odb_cmd ( "ls","/alias/$ppg_name","") ;
    ($string,$msg) = get_string($key);
    if($debug) 
    { 
	print "$name: returned string for \'ls /alias/$ppg_name\':\"  $string\"\n"; 
	unless ($msg eq "") { print "  and msg =$msg\n";}
    }
    if($status)  
    {              # found the link
	
	if($debug) { print "$name: found a link for /alias/$ppg_name and about to remove it\n"; }
	print FOUT "$name: found a link for /alias/$ppg_name and about to remove it\n"; 
	($status) = odb_cmd ( "rm","/alias/$ppg_name"," ") ;
	unless($status)
	{          # failure removing link
	    print FOUT "$name: Failure from odb_cmd (rm); Error removing old link /alias/$ppg_name \n";
	    odb_cmd ( "msg","$MERROR","","$name", "FAILURE  removing old link /alias/$ppg_name " ) ;
	    print "$name: Failure   removing old link /alias/$ppg_name\n ";
	    return($status);
	}
	else       # success removing link
	{
	    print FOUT "name: Success - old link /alias/$ppg_name has been removed \n";
	    #odb_cmd ( "msg","$MINFO","","$name", "INFO:  old link /alias/$ppg_name  has been removed  " );
	    print  "$name: Success -  old link /alias/$ppg_name  has been removed \n";
	}
    }  
    else 
    {             # no link found
	print "$name: info - no link found for /alias/$ppg_name \n" ;
	print FOUT "$name: info - no link found for /alias/$ppg_name \n" ;
    }
    return($SUCCESS);
}

sub test_a_link
{
# test if there is a link for expt_name

    # input parameters:
    my ($expt_name) = @_; # get the input parameters
# returns 1 (TRUE) for a link, 0 (FALSE)  for no link 
    
    my ($ppg_name,$status,$string,$key);
    my $name="test_a_link";
    my $msg;

    
    print FOUT ("$name: input params: expt name = $expt_name\n");
    print ("$name: input params: expt name = $expt_name\n");
    $expt_name =~ s/_$//;  # remove any trailing underscore
    
    $ppg_name=sprintf("ppg$expt_name&");  # & to suppress new window
    print FOUT  ("$name:  ppg_name = $ppg_name\n");
    if($debug) { print ("$name:  ppg_name = $ppg_name\n"); }
    
    
#  Check if link exists 
    ($status) = odb_cmd ( "ls","/alias/$ppg_name"," ") ;
    ($string,$msg) =get_string($key);
    if($debug) { print FOUT  "$name: returned string for \'ls /alias/$ppg_name\':\"  $string\"\n"; }
    if($status)  
    {              # found the link
	print "$name: found /alias/$ppg_name \n";
	print FOUT "$name: found /alias/$ppg_name \n";
	return($TRUE);
    }
    else
    {             # no link found
	print "$name: no link found for /alias/$ppg_name \n" ;
	print FOUT "$name: no link found for /alias/$ppg_name \n" ;
    }
    return($FALSE);
}


# IMPORTANT
# this 1 is needed at the end of the file so require returns a true value   
1;   












