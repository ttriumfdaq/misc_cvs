/*
 $Log: bnmr_darc.h,v $
 Revision 1.1  2003/10/07 22:05:37  suz
 original for mdarc_musr cvs tree

 Revision 1.6  2002/12/04 20:52:09  suz
 add function prototypes

 Revision 1.5  2002/10/02 22:17:20  asnd
 Implement dynamic run headers for temperature and field

 Revision 1.4  2001/03/07 17:58:31  suz
 Some prototypes removed to darc_files.h

 Revision 1.3  2000/10/11 04:10:50  midas
 added cvs log

*/
#ifndef _BNMR_DARC_TD_H_
#define _BNMR_DARC_TD_H_


DWORD bnmr_darc(caddr_t pHistData);
DWORD darc_get_odb(D_ODB *p_odb_data);
DWORD darc_write_odb(D_ODB *p_odb_data);
INT darc_check_files(D_ODB *p_odb_data, INT keep,   INT *next_version);
INT odb_save( INT run_number, char *odb_save_dir);
long DARC_write( D_ODB* p_odb_data, caddr_t pHistData);
int DARC_runDesc( D_ODB* p_odb_data, MUD_SEC_GEN_RUN_DESC* pMUD_desc );
int DARC_scalers( D_ODB* p_odb_data, MUD_SEC_GRP* pMUD_scalGrp );  
int DARC_hists( D_ODB *p_odb_data, MUD_SEC_GRP* pMUD_histGrp,
	    caddr_t pHistData );
int DARC_hist( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	   MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, 
	   MUD_SEC_GEN_HIST_DAT* pMUD_histDat );
void DARC_camp_var ( CAMP_VAR *pVar );
void DARC_release_camp(void);

static INT DARC_camp_connect ( char * camp_host );
static long DARC_get_dynamic_headers( D_ODB *p_odb_data );
static void DARC_head_num( char* str, int len, double val, double err, char* units );


#endif
