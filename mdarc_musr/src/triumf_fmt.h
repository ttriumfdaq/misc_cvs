/*
 * Run Parameters block for TD_muSR data.

 $Log: triumf_fmt.h,v $
 Revision 1.1  2003/11/13 19:21:25  suz
 same as bnmr version

 Revision 1.2  2000/10/11 04:10:50  midas
 added cvs log


*/

typedef struct {
	  short	     mrun;
	  short	     mhists;
	  short	     msclr;
	  short	     msupd;
	  long	     jtsc[18];
	  long	     jdsc[18];
	  short	     mmin;
	  short	     msec;
	  short	     mtnew[6];
	  short	     mtend[6];
	  short	     mlston[4];
	  short	     mcmcsc;
	  short	     mlocsc[2][6];
	  short	     mrsta;
	  long	     acqtsk;
	  char	     logfil[10];
	  short	     muic;
	  long	      nevtot;
	  short	     mhsts;
	  short	     mbins;
	  short	     mshft;
	  short	     mspare[7];
	  char	     title[40];
	  char	     sclbl[72];
	  char	     coment[144];
} TMF_F_HDR;

typedef struct {
	union {
	    struct {
		short	     ihist;
		short	     length;
		long	     nevtot;
		short	     ntpbin;
		long	     mask;
		short	     nt0;
		short	     nt1;
		short	     nt2;
		char	     htitl[10];
		char	     id[2];
		char	     fill[32];
		short	     head_bin;
	    } h;
	    short	     data[256];
	} u;
} TMF_H_RECD;

