/********************************************************************\
  Name:        bnmr_darc.c
  Created by:   

  Contents:
  
 $Log: bnmr_darc.c,v $
 Revision 1.1  2003/10/07 21:58:49  suz
 original for mdarc_musr cvs tree

 Revision 1.27  2002/12/04 20:51:43  suz
 move function prototypes to bnmr_darc.h

 Revision 1.26  2002/11/18 20:49:42  suz
 add camp status to message on failure

 Revision 1.25  2002/11/01 08:06:00  asnd
 Fix auto-field bug: Gauss * 10000.

 Revision 1.24  2002/10/02 22:17:20  asnd
 Implement dynamic run headers for temperature and field

 Revision 1.23  2002/09/27 00:49:05  asnd
 Ensure update of Camp variables stats

 Revision 1.22  2002/07/10 19:01:37  suz
 add conditional on camp_hostname

 Revision 1.21  2002/06/24 23:21:33  asnd
 Change TD-MUSR to TD-

 Revision 1.20  2002/06/24 20:35:00  suz
 debug statements; changed method to TD-MUSR for MUSR

 Revision 1.19  2002/06/12 18:16:56  suz
 Camp_hostname now under mdarc/camp in odb

 Revision 1.18  2002/05/10 17:55:34  suz
 Add a parameter to db_get_value for Midas 1.9

 Revision 1.17  2002/04/17 20:19:37  suz
 many changes for MUSR, scalers in particular

 Revision 1.16  2001/09/28 19:31:31  suz
 sort out run descriptor strings

 Revision 1.15  2001/09/14 18:56:54  suz
 support MUSR with ifdef MUSR

 Revision 1.14  2001/03/30 18:58:14  suz
 minor change - a few comments only

 Revision 1.13  2001/03/29 19:47:15  suz
 Add warning message about maximum no. of scalers

 Revision 1.12  2001/03/07 17:56:48  suz
 Make darc_files.c by moving subroutines from bnmr_darc.c so cleanup need not link with bnmr_darc.
 Include darc_files.h for headers.

 Revision 1.11  2001/03/06 21:37:03  suz
 Added Support for toggle and kill buttons. Ping camp host to avoid timeout problem if powered down. Access no. bins,
 no. histograms from mdarc area written by rf_config.

 Revision 1.10  2001/01/12 20:07:38  suz
 Fix small bug.

 Revision 1.9  2001/01/08 19:10:55  suz
 Fix malloc packing bug (with TW). Open/close camp connection at begin/end of run only.

 Revision 1.8  2000/11/09 20:25:17  midas
 "mdarc" & "sis mcs" trees now created independently.
 The trees moved to /equipment/FIFO_ACQ (no more settings). New version
 of odb make cmd creates an experim.h with these trees separate.

 Revision 1.7  2000/11/03 21:06:04  midas
 Add his_total and total_save to odb

 Revision 1.6  2000/10/27 19:25:22  midas
 No. histograms increased from 4 to 16. SIS directory tree under
 settings moved under "sis mcs" and "histograms" tree moved from
 sis tree to under mdarc tree.

 Revision 1.5  2000/10/13 20:26:38  midas
 add DB_OPEN_RECORD to write_message1

 Revision 1.4  2000/10/11 04:54:37  midas
 Fix the BOOL definition problem

 Revision 1.3  2000/10/11 04:10:50  midas
 added cvs log

 
\********************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

/* camp includes */
#include "camp_clnt.h"

#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif

/* midas includes */
#include "midas.h"
#include "msystem.h"
#include "ybos.h"

//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "c_utils.h"
#include "triumf_fmt.h"

/* darc includes */
#include "experim.h" /* odb structure */
#include "darc_odb.h"
#include "bnmr_darc.h"  /* prototypes */
#include "darc_files.h"  /* prototypes */

#include "mdarc.h" /* common header for mdarc & bnmr_darc */
// note caddr_t is defined as char*


void  hot_toggle (INT, INT, void * ); /* shared with mdarc.c */

/* bnmr_darc globals */
#define VERSION 1.0
#define MAX_PURGE_FILES  50

time_t timbuf;
char timbuf_ascii[30];
MUD_SEC_GRP* pMUD_indVarGrp = NULL;
int numIndVar = 0;  /* global for recursion SD/TW */

#ifdef MUSR
HNDLE hMDarc;
HNDLE hFS;
MUSR_TD_ACQ_MDARC fmdarc;
#else    // (BNMR)
/* these to access sis_mcs tree */
HNDLE  hFS;
FIFO_ACQ_SIS_MCS fifo_set;

/* these to access mdarc tree */
HNDLE hMDarc; // handle for mdarc
FIFO_ACQ_MDARC fmdarc;
#endif


/*INT firstTime = TRUE;  now a global (firstxTime) in mdarc.h. */
BOOL campFail = FALSE; /* flag a failure from camp to avoid camp hanging next
                          time */
int campInit  = FALSE;
BOOL campUpdated  = FALSE;

/* pre-processor macro for getting run-header string */

#define get_run_header_item(item) \
  size = sizeof(p_odb_data->item); \
  status = db_get_value(hDB, hSet, #item, &p_odb_data->item, &size, TID_STRING, FALSE); \
  if (status == DB_TRUNCATED) \
  { \
    cm_msg(MINFO,"darc_get_odb","truncated string %s/" #item, str); \
    write_message1(status,"darc_get_odb"); \
  } \
  else if (status != DB_SUCCESS) \
  { \
    cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/" #item, str); \
    write_message1(status,"darc_get_odb"); \
    return status; \
  }




DWORD bnmr_darc(caddr_t pHistData)
{
  /* bnmr_darc also uses globals  
         nHistBanks   no. of histograms defined  
         nHistBins    no. of bins per histogram 
     (according to Bank information)
  */

  DWORD status;
  D_ODB *p_odb_data;
  D_ODB odb_data;
  INT i;
  INT run_state;
  HNDLE hktmp;
  
  p_odb_data = &odb_data;

  //if(debug)printf ("bnmr_darc:  Version %0.0f starting in debug mode \n",VERSION);
  
  if (bGotEvent)
  {
    bGotEvent = FALSE;

    if(pHistData == NULL)
    {
       printf("bnmr_darc: No Histogram data available; pHistData is NULL\n");
       return;
    }

    if ( nHistBanks <= 0)   /* nHistBanks and nHistBins are globals */
      {
      cm_msg(MERROR,"bnmr_darc","Invalid number of histogram banks (%d)",nHistBanks);
      return(DB_INVALID_PARAM);
      }
    if ( nHistBins <= 0)   /* nHistBanks and nHistBins are globals */
      {
      cm_msg(MERROR,"bnmr_darc","Invalid number of histogram bins (%d)",nHistBins);
      return(DB_INVALID_PARAM);
      }
    if(debug)printf("bnmr_Darc: starting with %d histograms of %d bins\n",nHistBanks, nHistBins); 


    if(debug_dump)
    {
      printf("bnmr_darc: pHistData = %p\n",pHistData);
      for (i = dump_begin; i < dump_begin + dump_length ; i++)
      {
        printf(" data[%d] = 0x%x  or %d (dec)\n",i,((long*)pHistData)[i],((long*)pHistData)[i]);
      }
    }

    
    status = darc_get_odb(p_odb_data);
    if (status != DB_SUCCESS)  
    {
      cm_msg(MERROR,"bnmr_darc","Failure to get the odb data needed");
      return(status);
    }
    if(debug)
    {
      printf("bnmr_darc: Success from darc_get_odb\n");
      printf("bnmr_darc: Calling DARC_write \n");
    }

    campUpdated = FALSE;

    status = DARC_get_dynamic_headers(p_odb_data );

    status = DARC_write(&odb_data,pHistData);

    if(status != SUCCESS)
    {
      cm_msg(MERROR,"bnmr_darc","failure to save the histogram data");
      if (toggle)    /* don't allow another toggle after error */
        cm_msg(MERROR,"bnmr_darc","toggle did not delete old run files after error ");
      return( FAILURE);
    }
    if (toggle)
    {    /* delete old run files and reset toggle */
      printf ("bnmr_darc:Toggle is set; deleting any run files for old run number %d \n",old_run_number);
      status = darc_kill_files(old_run_number,  p_odb_data->save_dir);

      toggle = FALSE;  /* set global toggle false */
      /* turn toggle bit off in odb  */
      size = sizeof(toggle);
      status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
        cm_msg(MERROR, "bnmr_darc", "cannot clear toggle flag ");

      if(debug) printf ("bnmr_darc:  Toggle is cleared \n");
      /* restore the hot link on toggle  */

      if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
      {
        size = sizeof(fmdarc.toggle);
        status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for toggle (%d)", status );
          write_message1(status,"bnmr_darc");
          return(status);
        }      
      }
    }  // if toggle
  }    // if bGotEvent
  else
    {  // bGotEvent is false
    if (debug)
      printf ("bnmr_darc: Called with bGotEvent false; no valid data available\n");
    /* get the run state to see if run is going */
    size = sizeof(run_state);
    status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"bnmr_darc","key not found /Runinfo/State");
      write_message1(status,"bnmr_darc");
      return (status);
    }
    //if (debug) printf("run_state = %d (3=running)\n",run_state);

#ifdef MUSR
    if(run_state == STATE_PAUSED)
      {
	if(debug) printf("bnmr_darc: Run %d is paused; data not saved\n",run_number);
      }
    else if (run_state == STATE_RUNNING)
      cm_msg(MINFO,"bnmr_darc","No valid Histogram data is available at this time; data not saved");
    else  /* not running */
      if(debug) printf("bnmr_darc: Not running; no data found; data not saved\n");    
    
#else
    if(run_state == STATE_RUNNING)  /* running; we should have data unless hold flag is on (BNMR only) */
    {
         BOOL flag;
         char str[128];

       /* is  "sis mcs" area present?  (for BNMR hold flag)  */
       sprintf(str, "/Equipment/%s/sis mcs",eqp_name);  
       status = db_find_key(hDB, 0, str, &hFS);
       sprintf(str,
               "No valid Histogram data is available at this time; data not saved");
       
       if (status == DB_SUCCESS)   // hold flag is available
       {
         /* run may be on hold */
         size = sizeof(flag);
         status = db_get_value(hDB,hFS, "flags/hold",  &flag, &size, TID_BOOL, FALSE);
         if (status != DB_SUCCESS)
         {
           status=cm_msg(MERROR,"bnmr_darc","Could not access sis mcs key: flags/hold ");
           write_message1(status,"bnmr_darc");
         }
         else
         {
           if (flag)
             sprintf(str,"Run %d is on hold; data not saved",run_number);
         }
      }
       cm_msg(MINFO,"bnmr_darc","%s",str); /* send appropriate message */
    }
        
    else  /* not running */
      if(debug) printf("bnmr_darc: Not running; no data found; data not saved\n");    

#endif
    } // end of bGotEvent is false


  return( SUCCESS );
}


DWORD darc_get_odb(D_ODB *p_odb_data)
{

  INT   status, size, i, j, itemp;
  char str[128];
  HNDLE hSet, hScaler, hRigM;
  KEY   hKScaler;
  double temp_scalers[MAX_NSCALS];
  char  *s;
  char  mdarc[32];
  INT rn;  /* temp for run number check */
  BOOL enabled;
  
#ifdef MUSR
  char  cmd[132];
  char outfile[]="/var/log/midas/musr_update.txt";
  char errfile[]="/var/log/midas/musr_update.lasterr";
#endif

  if(debug) printf("darc_get_odb starting ... \n");

/* get the run state */
  size = sizeof(p_odb_data->run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &p_odb_data->run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/State");
    write_message1(status,"darc_get_odb");
    return (status);
  }

  /* get the time the run started */
  size = sizeof(p_odb_data->start_time_binary);
  status = db_get_value(hDB, 0, "/Runinfo/Start time binary", &p_odb_data->start_time_binary, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Start time binary");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  
  size = sizeof(p_odb_data->start_time_str);
  status = db_get_value(hDB,0, "/Runinfo/Start time",  &p_odb_data->start_time_str, &size, TID_STRING, FALSE);

  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Start time");
    write_message1(status,"darc_get_odb");
    return (status);
  }

  /* and the time the run last stopped */
  size = sizeof(p_odb_data->stop_time_binary);
  status = db_get_value(hDB, 0, "/Runinfo/Stop time binary", &p_odb_data->stop_time_binary, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found  /Runinfo/Stop time binary");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  
  size = sizeof(p_odb_data->stop_time_str);
  status = db_get_value(hDB,0, "/Runinfo/Stop time",  &p_odb_data->stop_time_str, &size, TID_STRING, FALSE);

  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Stop time ");
    write_message1(status,"darc_get_odb");
    return (status);
  }


// The run number is a global value, got by tr_start
  p_odb_data->run_number = run_number;

//  get the run number from /Runinfo in case some misguided person has changed it!
  size = sizeof(rn);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &rn, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    status=cm_msg(MERROR,"darc_get_odb","key not found  /Runinfo/Run number");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  if (run_number != rn)
  {
    status = cm_msg(MINFO,"darc_get_odb",
                    "Run number has changed. Automatic run numbering is in operation.");
    status  = cm_msg(MINFO,"darc_get_odb","Setting run number back to  %d",run_number);

/*  reset run number to previous value */
    rn=run_number;
    status = db_set_value(hDB,0, "/Runinfo/Run number",  &rn, size, 1, TID_INT);
    if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"darc_get_odb","error resetting run number");
      write_message1(status,"darc_get_odb");
      return (status);
    }
  }

  /* these global values have been set up and  checked in tr_start and process_event
     They cannot be changed during a run.
  */
  p_odb_data->his_n    = nHistBanks; /* no. of histogram banks found in this event (global)  */
  p_odb_data->his_nbin = nHistBins;  /* no. of histogram bins found in this event (global)   */ 

/*       G E T     M D A R C     R E C O R D   */

/* mdarc area must be present for all front end clients (mdarc.c has already checked this) */
  sprintf(str, "/Equipment/%s/mdarc",eqp_name);  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key %s not found (%d)", str,status);
    write_message1(status,"darc_get_odb");
    return status;
  }

  
  /* The histogram parameters in the mdarc area (i.e. first good bin etc.) are allowed 
     to be changed during the run */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record  */
  
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","Failed to get the whole record for %s (%d)",str,status );
    write_message1(status,"darc_get_odb");
    return(status);
  }
  
/*       M U S R  only   */

// MUSR only : update bin parameters from musr area
#ifdef MUSR

  /* bin parameters may change during the run */
  if(debug)
    printf("darc_get_odb: Calling musr_update.pl to check if bin parameters need updating... \n");
  unlink(outfile); /* delete any old copy of perl output file in case of failure */

  sprintf(cmd,"%s/musr_update.pl %s %s %s",perl_path, perl_path, expt_name,
          fmdarc.histograms.musr.beamline );
  if(debug) printf("darc_get_odb: sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  if (status)
    /* cannot update parameters */
  {
    printf (" ========================================================================================\n");
    cm_msg (MINFO,"darc_get_odb",
            "Bad status from perl script musr_update.pl (%d). Bin params may not be current",status);
    unlink(errfile); /* delete old copy if present */
    status=print_file(outfile);   
    if(status)
      cm_msg (MINFO,"darc_get_odb","There may be compilation errors in the perl script"); // no output file
    else
      { // make a copy of the error file as the outfile is overwritten each time
	sprintf(cmd,"cp %s %s",outfile,errfile);
	status = system(cmd);
	if(status)cm_msg(MINFO,"darc_get_odb","failure from system command: %s",cmd);
      }
  }
  else
  {
    if(debug)printf("darc_get_odb: successfully updated mode params\n");
    // get the record again now it has been updated
    size = sizeof(fmdarc);
    status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record  */
    
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","Failed to again get the whole record for %s (%d)",str,status );
      write_message1(status,"darc_get_odb");
      return(status);
    }
  }

  p_odb_data->dwell_time = (float) fmdarc.histograms.resolution_code; 

  /* These are needed in the histogram header for the mud files 
      get area,rig,mode */

  sprintf(p_odb_data->area,fmdarc.histograms.musr.beamline); // overwritten later anyway according to run number
  sprintf(p_odb_data->rig,fmdarc.histograms.musr.current_rig);
  sprintf(p_odb_data->mode,fmdarc.histograms.musr.current_mode); 

#else
/*       B N M R  only   */
  
/* These are needed in the histogram header for the mud files 
   Fill header values for BNMR
*/
  sprintf(p_odb_data->area,"ISAC"); // overwritten later anyway according to run number
  sprintf(p_odb_data->rig,"%s", expt_name); //bnmr2
  strncpy(p_odb_data->mode, fifo_set.input.experiment_name, 32); // ppg mode 


/*    Values specific to SIS multichannel scaler (i.e. BNMR experiment)
     check client name for now */
  
  /* BNMR uses dwell time (from mdarc  area) */
  p_odb_data->dwell_time = fmdarc.histograms.dwell_time__ms_; 
  
  /* find  "sis mcs" area  */
  sprintf(str, "/Equipment/%s/sis mcs",eqp_name);  
  status = db_find_key(hDB, 0, str, &hFS);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","key %s not found (%d)", str,status);
      write_message1(status,"darc_get_odb");
      return status;
    }
  
  size = sizeof(fifo_set);
  status = db_get_record (hDB, hFS, &fifo_set, &size, 0);/* get the whole record */
  
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","Failed to get the whole record for %s (%d)",str,status );
      write_message1(status,"darc_get_odb");
      return(status);
    }
  

  if (debug)
    {
      if(! fifo_set.sis_test_mode.test_mode)
        printf("darc_get_odb: sis REAL mode is true\n");
      else
        printf("darc_get_odb: sis TEST mode is true\n");
    }

#endif
  if (debug)
  {
    printf("Current settings:\n");
    printf("  Beamline: %s\n",p_odb_data->area);
    printf("  Rig: %s    Mode: %s\n",
	   p_odb_data->rig,p_odb_data->mode);    
    printf("  No. bins: %d   No. histograms: %d\n",
	   p_odb_data->his_nbin,p_odb_data->his_n);

#ifdef MUSR
  printf("  Resolution code =  %f\n", p_odb_data->dwell_time);
#else
  printf("  Dwell time (ms) %f\n", p_odb_data->dwell_time);
#endif
  } // end of if(debug)
  
  for(i=0; i<p_odb_data->his_n; i++)
  {
    strncpy (p_odb_data->his_tits[i], fmdarc.histograms.titles[i], 10);
    if(debug) printf("p_odb_data->his_tits[%d]=%s\n",
                     i,p_odb_data->his_tits[i] );
  }
  
  for (i=0; i<p_odb_data->his_n ; i++) 
  {
    p_odb_data->his_bzero[i]      =  fmdarc.histograms.bin_zero[i];
    p_odb_data->his_good_begin[i] =  fmdarc.histograms.first_good_bin[i];
    p_odb_data->his_good_end[i]   =  fmdarc.histograms.last_good_bin[i];
    p_odb_data->his_first_bkg[i]  =  fmdarc.histograms.first_background_bin[i];
    p_odb_data->his_last_bkg[i]   =  fmdarc.histograms.last_background_bin[i];
    
    if (p_odb_data->his_good_end[i] > p_odb_data->his_nbin)      
    {
      cm_msg(MINFO,"darc_get_odb"," Warning - histogram %d has last good bin (%d)  > # bins (%d)",
             i,p_odb_data->his_good_end[i],p_odb_data->his_nbin);
      printf("darc_get_odb","last good bin will be set to # bins\n");
      p_odb_data->his_good_end[i] = p_odb_data->his_nbin;
    }
  }


#ifdef MUSR
    //  MUSR Scalers //
  /* extract the scaler names from /Equipment/scaler/Settings  */
  sprintf(str, "/Equipment/scaler");
  status = db_find_key(hDB, 0, str, &hSet);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb"," key %s not found", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  size = sizeof(enabled);
  status = db_get_value(hDB, hSet, "/Settings/enabled", &enabled, &size, TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","Cannot retrieve  %s/Settings/enabled", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  if (enabled)
  {
    printf("  Scaler is enabled\n");
    
    size = sizeof(p_odb_data->scaler_titles);
    status = db_get_value(hDB, hSet, "/Settings/Titles", p_odb_data->scaler_titles, &size, TID_STRING, FALSE);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","Cannot retrieve scaler names from  %s/Settings/Titles", str);
      write_message1(status,"darc_get_odb");
      return status;
    }
  
  
    size = sizeof(temp_scalers); /* these are stored as DOUBLE in odb */
    status = db_get_value(hDB, hSet, "/Variables/SCLR", temp_scalers, &size, TID_DOUBLE, FALSE);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","cannot retrieve scaler data from %s/Variables/SCLR", str);
      write_message1(status,"darc_get_odb");
      return status;
    }
  
    status = db_find_key(hDB, hSet, "/Variables/SCLR", &hScaler);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","cannot find key %s/Variables/SCLR", str);
      write_message1(status,"darc_get_odb");
      return status;
    }
    status = db_get_key(hDB, hScaler, &hKScaler);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","cannot get key %s/Variables/SCLR", str);
      write_message1(status,"darc_get_odb");
      return status;
    }
    
    p_odb_data->nscal = hKScaler.num_values; /* find out total no. of scalers
                                                defined by Front End  */
    if (debug)printf("darc_get_odb: nscal = %d\n", hKScaler.num_values);
    for (i=0; i< p_odb_data->nscal ; i++)
    {
      p_odb_data->scaler_save[i] = (long) temp_scalers[i];
      if(debug) printf("scaler_titles[%d] = %s  data = %d \n",i,p_odb_data->scaler_titles[i],p_odb_data->scaler_save[i] );
    }
  }
  else
  {
    printf("  Scaler is disabled\n");
    p_odb_data->nscal=0;
  }
#else
  //  BNMR Cycle Scalers //
  /* extract the scaler names from /Equipment/Cycle_scalers/Settings  */
  sprintf(str, "/Equipment/Cycle_scalers");
  status = db_find_key(hDB, 0, str, &hSet);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb"," key %s not found", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  size = sizeof(p_odb_data->scaler_titles);
  status = db_get_value(hDB, hSet, "/Settings/names", p_odb_data->scaler_titles, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","Cannot retrieve scaler names from  %s/Settings/names", str);
    write_message1(status,"darc_get_odb");
    cm_msg(MERROR,"darc_get_odb",
           "# scalers defined by frontend may be > maximum currently allowed by mdarc");
    return status;
  }
  
  
  size = sizeof(temp_scalers); /* these are stored as DOUBLE in odb */
  status = db_get_value(hDB, hSet, "/Variables/HSCL", temp_scalers, &size, TID_DOUBLE, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot retrieve scaler data from %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  status = db_find_key(hDB, hSet, "/Variables/HSCL", &hScaler);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot find key %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  status = db_get_key(hDB, hScaler, &hKScaler);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot get key %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  p_odb_data->nscal = hKScaler.num_values; /* find out total no. of scalers
                                              defined by Front End  */
  if (debug)printf("darc_get_odb: nscal = %d\n", hKScaler.num_values);
  for (i=0; i< p_odb_data->nscal ; i++)
  {
    p_odb_data->scaler_save[i] = (long) temp_scalers[i];
    if(debug) printf("scaler_titles[%d] = %s  data = %d \n",i,p_odb_data->scaler_titles[i],p_odb_data->scaler_save[i] );
  }  
#endif

/* Get parameters from /Experiment/Edit on start */
  sprintf(str,"/Experiment/Edit on start");
  status = db_find_key(hDB, 0, str, &hSet);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","darc_get_odb: key %s not found", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  

  /* get experiment number */
  size = sizeof(p_odb_data->experiment_number);
  status = db_get_value(hDB, hSet, "experiment number", &p_odb_data->experiment_number, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/experiment number", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  /* get run title (was run subtitle on VMS) */

  get_run_header_item(run_title);

  /* get experimenter, temperature, orientation, field and sample (was run_title on VMS) */ 

  get_run_header_item(experimenter);
  get_run_header_item(temperature);
  get_run_header_item(orientation);
  get_run_header_item(field);
  get_run_header_item(sample);

  /* get camp host name  */
#ifdef MUSR
  strcpy(p_odb_data->camp_host, fmdarc.camp.camp_hostname);
  strcpy(p_odb_data->temperature_variable, fmdarc.camp.temperature_variable);
  strcpy(p_odb_data->field_variable, fmdarc.camp.field_variable);
#else
  strcpy(p_odb_data->camp_host, fmdarc.camp_hostname);
#endif
  if(debug) printf("DARC_get_odb: Camp hostname = %s\n",p_odb_data->camp_host);
  
/*      get mdarc parameters */
  
  p_odb_data->purge_after =  fmdarc.num_versions_before_purge;
  strcpy(p_odb_data->save_dir, fmdarc.saved_data_directory);
  trimBlanks(p_odb_data->save_dir,p_odb_data->save_dir);
  /* if there is a trailing '/', remove it */
  s = strrchr(p_odb_data->save_dir,'/');
  i= (int) ( s - p_odb_data->save_dir );
  j= strlen( p_odb_data->save_dir );
  if (debug_check)
    printf("darc_get_odb: string length of saved_dir %s  = %d, last occurrence of / = %d\n", p_odb_data->save_dir, j,i);
  if ( i == (j-1) )
  {
    if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
    p_odb_data->save_dir[i]='\0';
  }
  /* check purge_after >= 2 (see darc_check_params) */
  if(p_odb_data->purge_after < 2)
  {
    cm_msg(MINFO,"darc_get_odb"," parameter %s/num_versions_before_purge is out of range",
           p_odb_data->purge_after);
    cm_msg(MINFO,"darc_get_odb","It must be greater than 1;  a value of 2 will be used");
    p_odb_data->purge_after = 2 ;
  }
  
  if(debug || debug_check)
  {
    printf("darc_get_odb:       Mdarc variables:\n");
    printf("Directory for saving the data: %s\n",p_odb_data->save_dir);
    printf("Number of versions of saved files kept for each run will be %d\n",p_odb_data->purge_after);
  }
  return(SUCCESS); 
}

DWORD darc_write_odb(D_ODB *p_odb_data)
{
  INT  status, size, i,j;
  char str[128];
  HNDLE hKeyH;
  KEY key;

  /* write time file was saved */ 
  if (debug || debug_check)
    printf("darc_write_odb: Writing time file was saved = %s to the odb \n", p_odb_data->time_save);
  size = sizeof(p_odb_data->time_save);
  status = db_set_value(hDB, hMDarc, "time_of_last_save", &p_odb_data->time_save, size,1, TID_STRING);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot update key /Equipment/%s/mdarc/time_of_last_save (%d)",
	   eqp_name,status);
    write_message1(status,"darc_write_odb");
    return status;
  }
  
  /* write filename  */ 
  if (debug || debug_check)
    printf("darc_write_odb: Writing to odb saved filename = %s  \n", p_odb_data->file_save);
  size = sizeof(p_odb_data->file_save);
  status = db_set_value(hDB, hMDarc, "last_saved_filename", &p_odb_data->file_save, size,1, TID_STRING);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot update key /Equipment/%s/last_saved_filename (%d)",eqp_name,status);
    write_message1(status,"darc_write_odb");
    return status;
    
  }

  /* write temperature and field header values in case they were dynamic */

  status = db_find_key(hDB, 0, "/Experiment/Edit on start", &hKeyH);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot find key %s", "/Experiment/Edit on start");
    write_message1(status,"darc_write_odb");
    return status;
  }
  
  if (debug || debug_check)
    printf("darc_write_odb: Writing to odb temperature = %s  \n", p_odb_data->temperature);
  size = sizeof(p_odb_data->temperature);
  status = db_set_value(hDB, hKeyH, "temperature", &p_odb_data->temperature, size,1, TID_STRING);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot update key /Experiment/Edit on start/temperature",eqp_name,status);
    write_message1(status,"darc_write_odb");
    return status;
  }
  
  if (debug || debug_check)
    printf("darc_write_odb: Writing to odb field = %s  \n", p_odb_data->field);
  size = sizeof(p_odb_data->field);
  status = db_set_value(hDB, hKeyH, "field", &p_odb_data->field, size,1, TID_STRING);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot update key /Experiment/Edit on start/field",eqp_name,status);
    write_message1(status,"darc_write_odb");
    return status;
  }


  /* write histogram totals */

  sprintf(str,"/Equipment/%s/mdarc/histograms/output/histogram totals",eqp_name);
  db_find_key(hDB, 0, str, &hKeyH);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_write_odb","cannot find key %s (%d)",str,status);
    write_message1(status,"darc_write_odb");
    return status;
  }
  if(debug_dump)
  {
    for (j=0; j< MAX_HIS; j++)
      printf(" his_total[%d] = %f \n", j, p_odb_data->his_total[j]);
  }
  /* presently can't use a double in odb under settings so these are float ... */
  if(debug)
  printf("darc_write_odb: Writing histogram totals to odb key %s\n",str);    
  db_set_data(hDB,hKeyH, p_odb_data->his_total,  MAX_HIS * sizeof(float), 
                MAX_HIS, TID_FLOAT);
  
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot set data for key %s (%d)",str,status);
      write_message1(status,"darc_write_odb");
      return status;
    }

  sprintf(str,"/Equipment/%s/mdarc/histograms/output/total saved",eqp_name);
  db_set_value(hDB, 0, str, &p_odb_data->total_save, sizeof(float), 
	      1, TID_FLOAT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_write_odb","cannot set data for key %s (%d)",str,status);
      write_message1(status,"darc_write_odb");
      return status;
    }
 
  return(SUCCESS); 
}

/* ------------------------ DARC_write ----------------------------------------*/
long
DARC_write( D_ODB* p_odb_data, caddr_t pHistData)
            
{
    
  long status;
  char outFile[128];
  FILE* fout;
  MUD_SEC_GRP* pMUD_fileGrp;
  MUD_SEC_GEN_RUN_DESC* pMUD_desc;
  MUD_SEC_GRP* pMUD_scalGrp;
  MUD_SEC_GRP* pMUD_histGrp;
  MUD_SEC_GRP* pMUD_cmtGrp;
  int i, j, k;
  long offset;
  char command[256];
  char cmtFile[128];
  FILE* fin;
  char ctemp[LEN_NODENAME+1];
  int len;
  INT nfile, next_version;
  INT no_purge=0;
  char str[126];
  
  /*
   *  Compute sums of bin quantities
   */
  
  p_odb_data->total_save = 0;
  
  
  offset = 0 ; /* initial offset into data array */
  
  
  if (debug || debug_mud) printf("DARC_write: Starting with pHistData = %p\n",pHistData);  
  for( i = 0; i <  p_odb_data->his_n; i++ )
  {
    if (debug_mud)printf("DARC_write: His %d  pHistData = %p\n",i,pHistData);
    p_odb_data->his_total[i] = 0.0;
    if(debug_mud)printf("DARC_write: his %d  offset= %d\n",i,offset);
    for( j = 0; j < p_odb_data->his_nbin; j++ )
      {
	if(debug_dump)
        {          
          if( j >= dump_begin && j < (dump_begin+dump_length) )
            {
            printf("Adding on data index %d = 0x%x (%u) at offset(%d); his_total[%d](%f)\n",
		 j, ((unsigned long*)pHistData)[offset+j],  ((unsigned long*)pHistData)[offset+j],
                 (offset+j), i, p_odb_data->his_total[i]);
          }
        }
	p_odb_data->his_total[i] += (float)  ((unsigned long*)pHistData)[offset+j] ;

      }

#ifndef MUSR
    if(debug_dump)
    {
      if ( fifo_set.sis_test_mode.test_mode ) 
      {
        /* for test data only, approx total should be nbins * 1st data value */
        float approx;
        approx= ( (float) p_odb_data->his_nbin) * ((float) ((unsigned long*)pHistData)[offset+0]);
        printf("Rough total for histogram %d should be %d * %u =  %f\n",i,
               p_odb_data->his_nbin, ((unsigned long*)pHistData)[offset+0], approx);
      }
    }
#endif
    p_odb_data->total_save += p_odb_data->his_total[i];
    if(debug_dump || debug_mud)
      printf("DARC_write: his %d his_total[%d]= %f, total_save=%f\n",
             i,i,p_odb_data->his_total[i], p_odb_data->total_save ) ;
    
    offset = offset + p_odb_data->his_nbin; /* point to next histogram */
  }
  /* end of loop on all histograms, zeroing totals for unused histograms */
  for( i = p_odb_data->his_n; i< MAX_HIS; i++ )
    p_odb_data->his_total[i] = 0.0;
  
// linux add a cast (MUD_SEC_GRP*)
  pMUD_fileGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_FMT_TRI_TD_ID );
  if (debug_mud) printf(" DARC_write: After MUD_new, pMUD_fileGrp = %p\n",pMUD_fileGrp ); 
  
  /*
   *  Convert the run description
   */
// linux add a cast (MUD_SEC_GEN_RUN_DESC*)  
  pMUD_desc =  (MUD_SEC_GEN_RUN_DESC*) MUD_new( MUD_SEC_GEN_RUN_DESC_ID, 1 );
  DARC_runDesc( p_odb_data, pMUD_desc );
  
  /*
   *  Convert the scalers
   */
// linux add a cast  (MUD_SEC_GRP*)   
  pMUD_scalGrp =  (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_SCALER_ID );
  DARC_scalers( p_odb_data, pMUD_scalGrp );
  
  /*
   *  Convert the histograms
   */
// linux add a cast (MUD_SEC_GRP*)    
  pMUD_histGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_TRI_TD_HIST_ID );
 
  if( DARC_hists( p_odb_data, pMUD_histGrp, pHistData) == FAILURE )
    {
      cm_msg(MERROR,"DARC_write","DARC_hists: failed to create histograms\n" );
      return(FAILURE);
    }
  
  /*
   *  Assemble the first level sections
   */
/* keep this code in case we need to time something */  
/*  if (timer)  
  {   
    time(&timbuf);
    strcpy(timbuf_ascii,ctime(&timbuf));         
    printf (
      "DARC_write (TIMER) Assembling 1st level sections  %s\n",timbuf_ascii);
  }
*/  
  MUD_addToGroup( pMUD_fileGrp, pMUD_desc );
  MUD_addToGroup( pMUD_fileGrp, pMUD_scalGrp );
  MUD_addToGroup( pMUD_fileGrp, pMUD_histGrp );
  
  /*
   *  Add  comments if they exist
   */
  
  sprintf( cmtFile, "%s/%06d.cmt",p_odb_data->save_dir,p_odb_data->run_number );
  if(debug) printf("DARC_write: Looking for any comments in file %s\n",cmtFile);
  fin = MUD_openInput( cmtFile );
  if( fin != NULL )
  {
    printf("DARC_write","using comment file %s\n", cmtFile );
    
    pMUD_cmtGrp = MUD_readFile( fin );
    if( pMUD_cmtGrp != NULL ) 
    {
      if( MUD_instanceID( pMUD_cmtGrp ) != MUD_GRP_CMT_ID )
      {
        cm_msg(MINFO,"DARC_write","failed to read comment file %s",
               cmtFile );
        MUD_free( pMUD_cmtGrp );
      }
      else
      {
        MUD_addToGroup( pMUD_fileGrp, pMUD_cmtGrp );
      }
    }
    else
    {
      cm_msg(MINFO,"DARC_write","failed to read comment file %s", cmtFile );
    }
    
    fclose( fin );
  }
  else
  {
    if(debug) printf("DARC_write: no comment file found\n" );
  }
  
  /*
   *  Add CAMP sections if there is anything to log
   */

  DARC_camp_connect( p_odb_data->camp_host );

  if( campInit )
    {
      /*
       *  Get an update of the CAMP database
       */
      if( !campUpdated )
	{
	  if(debug) printf("Calling camp_clntUpdate()\n");
	  status = camp_clntUpdate();
	  if( status != CAMP_SUCCESS )
	    cm_msg(MERROR,"DARC_write","failed call to update CAMP data (%d)",status );
	  else
	    campUpdated = TRUE ;
	}
      
      if(campUpdated) 
	{
	  status = campSrv_varGet( "/", CAMP_XDR_ALL );
	  if( status != CAMP_SUCCESS )
	    cm_msg(MERROR,"DARC_write","failed call to retrieve CAMP data (%d)",status );
	  else
	    {
	      /*
	       *  Call proc DARC_camp_var for each variable
	       */
	      if(debug || debug_mud)printf("Calling camp_varDoProc\n");
	      camp_varDoProc_recursive( DARC_camp_var, pVarList );
	      
	      /*
	       *  Add CAMP group to file group if any loggable variables found
	       *  Note that pMUD_indVarGrp is global
	       */
	      if(debug_mud)printf("Calling MUD_addToGroup\n");
	      
	      if( pMUD_indVarGrp != NULL ) 
		MUD_addToGroup( pMUD_fileGrp, pMUD_indVarGrp );
	      
	      /*
	       *  End CAMP connection (free memory)
	       */
	      //   camp_clntEnd();    call this only when mdarc exits
	    }
	} // campUpdated 

    } // if (campInit)
  else  /* Camp failed to connect */
    cm_msg(MINFO,"DARC_write","Failed initialize call to CAMP");
  
  if(debug) printf("End of any Camp\n");

  /*
   *  Write the file
   */
  /* check if there is any REGULAR file with no version no. at all (symlink is OK) */
  if(debug_check)printf("Calling darc_find_saved_file \n");
  nfile = darc_find_saved_file (p_odb_data->run_number, p_odb_data->save_dir);
  if (nfile == 1 )  /* returns 1 if a REGULAR file */
    {
      cm_msg(MERROR,"DARC_write","final run file already exists (%s/%06d.msr)",
	     p_odb_data->save_dir,p_odb_data->run_number);
      printf("DARC_write: Cannot save histograms\n");
      return(FAILURE);
    }

  /* see if there are any files already with this run number to find the next version */  
  if(debug_check)printf("Calling darc_check_files with keep=%d\n",no_purge);
  nfile = darc_check_files (p_odb_data, no_purge , &next_version);
  
// mdarc may have been restarted so there may be files already for this run 
  if(debug_check)printf("Found %d files for this run. Next file version will be %d\n",
                        nfile,next_version);  
  if(next_version == 0)
  {
    if(debug_check)
    {
      printf("Darc_write: next_version from darc_check_files is zero (error)");
      printf("DARC_write: Number of saved files for this run found on disk: %d",nfile);
    }
/*  darc_check_files  gives this message
    printf("DARC_write: Too many files to sort (%d); can't determine next version\n",nfile);
*/   
    
    cm_msg(MERROR,"DARC_write","Cannot save histograms");
    return(FAILURE);
  }    

  sprintf( outFile, "%s/%06d.msr_v%d",
           p_odb_data->save_dir,p_odb_data->run_number, next_version );
  
  /* OK to proceed */
  fout = MUD_openOutput( outFile );
  if( fout == NULL ) 
  {
    cm_msg(MERROR,"DARC_write","failed to open file %s", outFile );
    return( FAILURE );
  }


  if (debug_mud) 
    printf("Darc_write: about to call MUD_writeFile with pMUD_fileGrp=%p\n",pMUD_fileGrp);
  status = MUD_writeFile( fout, pMUD_fileGrp );
  
  fclose( fout );

  

  if (debug_mud) printf("Darc_write: about to call MUD_free with pMUD_fileGrp=%p\n",pMUD_fileGrp);

    MUD_free( pMUD_fileGrp );
  pMUD_indVarGrp =  (MUD_SEC_GRP*)  NULL; /* free pointer for recursion  SD/TW*/
  numIndVar = 0;  /* global for recursion SD/TW */

  if( status != SUCCESS  )
  {
    cm_msg(MERROR,"DARC_write","failed to write file %s", outFile );
    return( FAILURE );
  }

  /* set up symbolic link for analysis programs 
             darc_unlink returns 1 for regular file found */
  if( darc_unlink (p_odb_data->run_number, p_odb_data->save_dir) != 1)
  {
    if(darc_set_symlink(p_odb_data->run_number, p_odb_data->save_dir,
                        outFile)==SUCCESS)
    {
      if (debug_check) printf("DARC_write: symbolic link successfully set up\n");
    }
    else   // not fatal
      printf("DARC_write: Info - couldn't set up symbolic link\n");
  }
  else
    printf("DARC_write: Info darc_unlink found regular saved data file (expected symbolic link)\n");
  /*
   *  Update odb too.
   */
  time(&timbuf);
  strcpy(str, ctime(&timbuf));
  str[24] = 0;
  strncpy(p_odb_data->time_save, str, sizeof(p_odb_data->time_save));/* save time saved */
  strncpy( p_odb_data->file_save, outFile,sizeof(p_odb_data->file_save) ); /* save file name */
  darc_write_odb(p_odb_data);

  cm_msg(MINFO,"DARC_write","*** data saved in file %s at %s ***", outFile,str );

  /*      Purge the files   */
  if(debug_check)
    printf("Calling darc_check_files with  keep=%d\n",p_odb_data->purge_after); 
  nfile = darc_check_files(p_odb_data, p_odb_data->purge_after, &next_version);

  if(debug_check) printf("DARC_write: darc_check_files returns nfile=%d\n",nfile);
  if(next_version == 0)
  {
    if(debug)
      printf("Darc_write: next_version from darc_check_files is zero (error)\n");
    cm_msg(MINFO,"DARC_write","WARNING - Number of saved files found on disk: %d",nfile);
    cm_msg(MINFO,"DARC_write","Can't determine next version for saved file. Cannot purge histograms");
    /* not a fatal error but there may be a problem next time */
  }    
 
  return( SUCCESS );
}


int DARC_hists( D_ODB *p_odb_data, MUD_SEC_GRP* pMUD_histGrp,
	    caddr_t pHistData )
{

    int i,k;
    MUD_SEC_GEN_HIST_HDR* pMUD_histHdr;
    MUD_SEC_GEN_HIST_DAT* pMUD_histDat;
    int offset;
    int bytes;
    int m;

    bytes = sizeof(long);
    for( i = 0; i < p_odb_data->his_n; i++ ) /* loop on all histograms */
    {
// linux add two casts      
      pMUD_histHdr = (MUD_SEC_GEN_HIST_HDR*) MUD_new( MUD_SEC_GEN_HIST_HDR_ID, i+1 );
      pMUD_histDat = (MUD_SEC_GEN_HIST_DAT*) MUD_new( MUD_SEC_GEN_HIST_DAT_ID, i+1 );
      
/* separate this so we can check for failure from malloc 
   pMUD_histDat->pData = (char*)zalloc( 4*p_odb_data->his_nbin ); 
*/
      
      /*
       *  TW/SD  21-Dec-2000  Must allocate for worst case when packing.
       *                      In worst case there can be 3 bytes in front of
       *                      each data word (2 for number of data to follow,
       *                      1 for the data size in bytes).  So, malloc
       *                      3 extra bytes per number of bins to be safe.
       */
      pMUD_histDat->pData = (char*)malloc( (bytes+3) * p_odb_data->his_nbin);
      if (pMUD_histDat->pData == NULL)
      {
        cm_msg(MERROR,"DARC_hists","malloc fails to allocate %d bytes",
                 bytes * p_odb_data->his_nbin);
//        cm_msg(MERROR,"DARC_hists","Not all the histogram data can be saved");
        printf("DARC_hists: You must reduce # of histograms and/or # of bins\n");

        return(FAILURE);
      }
      else
      {

        if(debug)printf("DARC_hists: malloc successful, allocates %d bytes \n",
                 bytes * p_odb_data->his_nbin);

      }
      /* zero the array */
      memset((char*)pMUD_histDat->pData,0,bytes*p_odb_data->his_nbin );
      
      
      offset = i  *  p_odb_data->his_nbin;
        
      if(debug_mud || debug || debug_dump )
      {
        printf("DARC_hists:About to call DARC_hist with offset=%d, (caddr_t)pHistData = %p\n"
               ,offset,pHistData);
	if(debug_dump)
	{
          /* dump some data */
          for (m=offset + dump_begin ; m < dump_begin + dump_length +offset; m++)
            printf("Array element %d  =  0x%x or %d (dec)  \n", 
                   m, ((long *)pHistData)[m], ((long *)pHistData)[m]);
	}
      }
      // add extra cast back to caddr_t for unix conversion
      DARC_hist( i, p_odb_data, (caddr_t)(&((long*)pHistData)[offset]), 
                 pMUD_histHdr, pMUD_histDat );
      
      
      if(debug_mud)printf("DARC_hists: calling MUD_addtoGroup for histogram header\n");
      MUD_addToGroup( pMUD_histGrp, pMUD_histHdr );
      if(debug_mud)printf("DARC_hists: calling MUD_addtoGroup for histogram data\n");
      MUD_addToGroup( pMUD_histGrp, pMUD_histDat );
    }
    
    return( SUCCESS );
}

int
DARC_hist( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	   MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, 
	   MUD_SEC_GEN_HIST_DAT* pMUD_histDat )
{
  char tempString[256];
  long nt0, nt00, nt01, nmid, ncl;
  double n, rfsPerBin;
  long* h;
  int i;
  
  
  if(debug || debug_mud )  
    printf("DARC_hist starting with pHistData = %p \n",pHistData);
/*
  if(debug_dump)  /* if needed add debug_dump to if ( debug ... )  above   
                     {
                     for (i = dump_begin; i < dump_begin + dump_length ; i++)
                     printf("Array element  (long *) pHistData[%d] = 0x%x or %d (dec) \n",
                     i,((long *)pHistData)[i], ((long *)pHistData)[i]);
                     }
*/               
  
  strncpy( tempString, p_odb_data->his_tits[ind], 10 );
  tempString[10] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_histHdr->title = strdup( tempString );    
  if(debug_mud)
  {
    printf(
      "DARC_hist: Working on histogram  %s \n",pMUD_histHdr->title);      
    printf("DARC_hist: index = %d, histogram title = %s \n",
           ind, pMUD_histHdr->title);
  }
  pMUD_histHdr->histType = MUD_SEC_TRI_TD_HIST_ID;

  /*
   *  TW/SD  TEMP  21-Dec-2000  Set the bytesPerBin to 4 to turn off packing
   *                            By default, the MUD hist header is created with
   *                            bytesPerBin = 0, which is a flag to the packing
   *                            routine to do packing.
   *                            For now, leave packing turned ON.
   *                            Note that the malloc of the pMUD_histDat->pData
   *                            above may cause memory allocation problems because
   *                            of allocating (nbins * bytes+3) bytes 
   */
  /*  pMUD_histHdr->bytesPerBin = 4; /* turns off packing */


  pMUD_histHdr->nEvents = (UINT32) p_odb_data->his_total[ind];
  if(debug_mud)
    printf("(UINT32)pMUD_histHdr->nEvents = %u\n",pMUD_histHdr->nEvents);
  pMUD_histHdr->nBins = p_odb_data->his_nbin;
  
/* BNMR - specific values:  check client name  */
  if (strncmp(feclient,"febnmr",6) == 0)
  {
    
    /* B-NMR SIS cannot use fsPerBin - use nsPerBin instead
       - convert  dwell_time (ms)  to ns
       note dwell_time is a real number. 
    */
    
    rfsPerBin = (double)  (p_odb_data->dwell_time) * (double)1000000; /* ns !!*/
    pMUD_histHdr->fsPerBin = (UINT32) rfsPerBin;
    if(debug_mud)
      printf("DARC_hist: real dwell_time = %f (ms), ns/bin= %d (mud stores as fs/bin) \n",
             p_odb_data->dwell_time, pMUD_histHdr->fsPerBin);
  }
  else /* non-BNMR clients : dwell time is actually the resolution code */
  {
    /* codes < 16 are from camac TDC now obsolete */
    if ( p_odb_data->dwell_time  < 16)
    {
      cm_msg(MERROR,"DARC_hist","TDC resolution code for obsolete CAMAC TDC detected (%d)",
             p_odb_data->dwell_time);
      return(DB_INVALID_PARAM);
    }
    else 
    {      /* codes > 16 are from VME TDC */
      rfsPerBin = 48828.1250 * pow( (double)2.0, 
                                    (double) p_odb_data->dwell_time - 16);
      /* for codes 16,17,18  use code values */
      if ( p_odb_data->dwell_time < 19) 
        pMUD_histHdr->fsPerBin = (UINT32)  p_odb_data->dwell_time ;
      else
        pMUD_histHdr->fsPerBin = (UINT32) rfsPerBin ;
    }
    if (debug_mud)printf("DARC_hist: VME TDC resolution code tdc_res= %d, fs/bin= %d\n",
                         p_odb_data->dwell_time, pMUD_histHdr->fsPerBin);    
    
  }
  nt0 = p_odb_data->his_bzero[ind];
  if( nt0 == 0 )
  {
    if(debug_mud)
      printf("DARC_hist: his_bzero[%d] is zero; calculation in progress\n",ind);
    
    h = (long*)pHistData;
    
    nt0 = pMUD_histHdr->nBins/2;
    ncl = 0;
    while( h[nt0-1] > ncl )
    {
      nt0 /= 2;
      ncl = h[nt0-1];
    }
    nt00 = nt0;
    nt01 = 2*nt0;
    nmid = ncl/3;
    for( nt0 = nt00; nt0 < nt01; nt0++ )
      if( h[nt0-1] > nmid ) break;
  }                        
  
  pMUD_histHdr->t0_bin = nt0;
  pMUD_histHdr->goodBin1 = p_odb_data->his_good_begin[ind];
  pMUD_histHdr->goodBin2 = p_odb_data->his_good_end[ind];
  pMUD_histHdr->t0_ps = 0.001*rfsPerBin*( (double)nt0 - 0.5 ) + 0.5;
  pMUD_histHdr->bkgd1 = p_odb_data->his_first_bkg[ind];
  pMUD_histHdr->bkgd2 = p_odb_data->his_last_bkg[ind];
  
  
  if(debug_mud)
  {
    printf("DARC_hist: pMUD_histHdr->t0_bin = %d\n",pMUD_histHdr->t0_bin);
    printf("DARC_hist: pMUD_histHdr->goodBin1 = %d\n",pMUD_histHdr->goodBin1);
    printf("DARC_hist: pMUD_histHdr->goodBin2 = %d\n",pMUD_histHdr->goodBin2);
    printf("DARC_hist: pMUD_histHdr->t0_ps = %d\n",pMUD_histHdr->t0_ps);
  }
  if ( pMUD_histHdr->bkgd1 == 0 && pMUD_histHdr->bkgd2 == 0)
  {
    if(debug)
      {
	printf(" nt0 = %d\n",nt0);
	printf("DARC_hist: using calculated bckgnd values for histo \"%s\" (index %d) (none supplied)\n",
	       pMUD_histHdr->title,ind );
      }
  }
  else 
  {
    if(debug_mud)printf 
                   ("DARC_hist: Supplied values of bkgd1,bkgd2 will be used\n");
  }
  
  
  if( pMUD_histHdr->bkgd1 == 0 && pMUD_histHdr->bkgd2 == 0 && nt0 > 1 )
  {
    long kmid, kb1, kb2;
    long nb;
    double avg, diff, err, val;
    long* h;
    
    
    if(debug_mud) printf("DARC_hist:  Calculating bkgd1 and bkgd2 values...\n");
    
    h = (long*)pHistData;
    kmid = nt0/2;
    nb = 0;
    avg = ( h[kmid-2] + h[kmid-1] + h[kmid] )/3;
    for( kb1 = kmid-1; kb1 >= 0; kb1-- )
    {
      val = fabs( (double)h[kb1] );
      diff = fabs( val - avg );
      err = _max( 1.0, sqrt( avg ) );
      if( diff > 5*err ) break;
      avg = nb*avg + val;
      nb++;
      avg = avg/nb;
    }
    kb1 = _min( kb1 + 5, kmid-1 );
    
    for( kb2 = kmid-1; kb2 < nt0; kb2++ )
    {
      val = fabs( (double)h[kb2] );
      diff = fabs( val - avg );
      err = _max( 1.0, sqrt( avg ) );
      if( diff > 5*err ) break;
      avg = nb*avg + val;
      nb++;
      avg = avg/nb;
    }
    kb2 = _max( kmid-1, kb2 - 5 );
    
    pMUD_histHdr->bkgd1 = kb1 + 1;
    pMUD_histHdr->bkgd2 = kb2 + 1;
  }
  if(debug_mud)
  {
    printf("DARC_hist: pMUD_histHdr->bkgd1 = %d\n",pMUD_histHdr->bkgd1);
    printf("DARC_hist: pMUD_histHdr->bkgd2 = %d\n",pMUD_histHdr->bkgd2);
  }
  
  pMUD_histHdr->nBytes = MUD_SEC_GEN_HIST_pack( pMUD_histHdr->nBins,
                                                4, pHistData,
                                                pMUD_histHdr->bytesPerBin, pMUD_histDat->pData );
  pMUD_histDat->nBytes = pMUD_histHdr->nBytes;
  
  return( SUCCESS );
}

void
DARC_release_camp()
{
  if(campInit)
  {
    if(debug || debug_mud)
      printf("DARC_release_camp: calling camp_clntEnd()\n");
    camp_clntEnd();    /* call this only when mdarc exits */
    if(debug_mud) printf("DARC_release_camp: end of run or mdarc exiting: releasing camp connection\n");
    campInit=FALSE;
  }
  else
    if(debug_mud)
      printf("DARC_release_camp: end of run or mdarc exiting: camp connection had not been established\n");
}

void
DARC_camp_var( CAMP_VAR* pVar )
{
  //    static int numIndVar = 0; global now for recursion SD/TW
    MUD_SEC_GEN_IND_VAR* pMUD_indVar;
    CAMP_NUMERIC* pNum;

    /*
     *  Check if the variable is to be logged
     */
    if( !( pVar->core.status & CAMP_VAR_ATTR_LOG ) ) return;
    if( !streq( pVar->core.logAction, "log_td_musr" ) ) return;

    /*
     *  Create the group when the first logged variable is found
     */
    if( pMUD_indVarGrp == NULL )
    {
      //linux add cast (MUD_SEC_GRP*)
	pMUD_indVarGrp = (MUD_SEC_GRP*) MUD_new( MUD_SEC_GRP_ID, MUD_GRP_GEN_IND_VAR_ID );
	if(debug_mud)printf("DARC_camp_var: logging CAMP sections\n" );
    }

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
	numIndVar++;
      // linux add cast  (MUD_SEC_GEN_IND_VAR*)
	pMUD_indVar = (MUD_SEC_GEN_IND_VAR*) MUD_new( MUD_SEC_GEN_IND_VAR_ID, numIndVar );

	pNum = pVar->spec.CAMP_SPEC_u.pNum;

	pMUD_indVar->name = strdup( pVar->core.path );
	pMUD_indVar->description = strdup( pVar->core.title );
	pMUD_indVar->units = strdup( pNum->units );
	pMUD_indVar->low = pNum->low;
	pMUD_indVar->high = pNum->hi;
        if( pNum->num < 2 )
        {
          pMUD_indVar->mean = pNum->val;
          pMUD_indVar->stddev = 0.0;
          pMUD_indVar->skewness = 0.0;
        }
        else
        {
          camp_varNumCalcStats( pVar->core.path, &pMUD_indVar->mean, 
                   &pMUD_indVar->stddev, &pMUD_indVar->skewness );
        }
	MUD_addToGroup( pMUD_indVarGrp, pMUD_indVar );
	break;

      default:
	break;
    }
}

int
DARC_runDesc( D_ODB* p_odb_data, MUD_SEC_GEN_RUN_DESC* pMUD_desc )
{
  long status;
  int i;
  long tempTime[6];
  char tempString[256];
  long timadr[2];
  short timbuf[7];
  char buffer[128];
  char* pos;

  if(debug_mud)printf ("DARC_runDesc: starting for run %d  \n",p_odb_data->run_number);
  
  pMUD_desc->runNumber = p_odb_data->run_number;
  pMUD_desc->exptNumber = p_odb_data->experiment_number;
  
  
  
  /* for linux use binary run start time directly */
  pMUD_desc->timeBegin = p_odb_data->start_time_binary ;    
  if( p_odb_data->run_state == STATE_RUNNING)
  {
    time( &pMUD_desc->timeEnd ); /* linux for now assume run ends now */        
    if(debug_mud) printf ("DARC_runDesc: Run in progress - using current time for timeEnd \n");
  }
  else
  {
    pMUD_desc->timeEnd = p_odb_data->stop_time_binary ; /* run ended */
    if(debug_mud) printf ("DARC_runDesc: Run NOT in progress - using saved time for timeEnd \n");
  }
  
  pMUD_desc->elapsedSec = pMUD_desc->timeEnd - pMUD_desc->timeBegin;
  
#ifdef MUSR
  pMUD_desc->method = strdup( "TD-�SR" ); /* MUSR */
#else
  pMUD_desc->method = strdup( "TD-Bnmr" ); /* Different name!! */
#endif
  pMUD_desc->lab = strdup( "TRIUMF" );
  pMUD_desc->das = strdup( "MIDAS" ); /* different DAQ !! */

  if( pMUD_desc->runNumber < 5000 ) pMUD_desc->area = strdup( "M20" );
  else if( pMUD_desc->runNumber < 10000 ) pMUD_desc->area = strdup( "M15" );
  else if( pMUD_desc->runNumber < 15000 ) pMUD_desc->area = strdup( "M13" );
  else if( pMUD_desc->runNumber < 20000 ) pMUD_desc->area = strdup( "M9B" );
  else if( pMUD_desc->runNumber < 25000 )
    pMUD_desc->area = strdup( "DEV" );
  else if( pMUD_desc->runNumber >= 30001 && pMUD_desc->runNumber <= 30499 )
    pMUD_desc->area = strdup( "TEST" );
  else if( pMUD_desc->runNumber > 40000 ) pMUD_desc->area = strdup( "BNMR" );
  
  //  strncpy( tempString, &p_odb_data->run_title[0], 80 );
  strncpy( tempString, &p_odb_data->run_title[0], 128 ); /* people are using >80 */
  tempString[80] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_desc->title = strdup( tempString );

  strncpy( tempString, &p_odb_data->sample[0],15);
  tempString[15] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_desc->sample = strdup( tempString );

  strncpy( tempString, &p_odb_data->temperature[0], 15 );
  tempString[15] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_desc->temperature = strdup( tempString );

  strncpy( tempString, &p_odb_data->field[0], 15 );
  tempString[15] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_desc->field = strdup( tempString );

  strncpy( tempString, &p_odb_data->orientation[0], 15 );
  tempString[15] = '\0';
  trimBlanks( tempString, tempString );
  pMUD_desc->orient = strdup( tempString );

// use rig name from ODB
  //  strcpy(buffer,p_odb_data->rig);

  
  //printf("p_odb_data->rig:%s\n",p_odb_data->rig);
  strncpy( tempString,&p_odb_data->rig[0], 32 );
  tempString[32] = '\0';

  trimBlanks( tempString, tempString );
  //printf("tempString:%s\n",tempString);
  pMUD_desc->apparatus = strdup( tempString );

  strncpy(tempString,p_odb_data->mode, 32);
  tempString[32] = '\0';

  trimBlanks( tempString, tempString );
  pMUD_desc->insert = strdup( tempString );

  strcpy(tempString,p_odb_data->experimenter);   
  tempString[32] = '\0';

  trimBlanks( tempString, tempString );
  pMUD_desc->experimenter = strdup( tempString );

  if(debug_mud)
  {
    printf("Run descriptor:\n");
    printf("pMUD_desc->title = %s\n",pMUD_desc->title );
    printf("pMUD_desc->sample = %s\n",pMUD_desc->sample );
    printf("pMUD_desc->temperature = %s\n",pMUD_desc->temperature );
    printf("pMUD_desc->field = %s\n",pMUD_desc->field );
    printf("pMUD_desc->orient = %s\n",pMUD_desc->orient );
    printf("pMUD_desc->apparatus = %s\n",pMUD_desc->apparatus );
    printf("pMUD_desc->insert = %s\n",pMUD_desc->insert );
  }
  return( SUCCESS );
}



int
DARC_scalers( D_ODB* p_odb_data, MUD_SEC_GRP* pMUD_scalGrp )
{
  char str[256];
  int i;
  MUD_SEC_GEN_SCALER* pMUD_scal;
  char* s;
  
  if(debug_mud) printf("DARC_scalers starting with nscal=%d\n",p_odb_data->nscal);
  
  for( i = 0; i < p_odb_data->nscal; i++ )
  {
// linux add a cast  (MUD_SEC_GEN_SCALER*)      
    pMUD_scal = (MUD_SEC_GEN_SCALER*)MUD_new( MUD_SEC_GEN_SCALER_ID, i+1);
    pMUD_scal->counts[0] = (UINT32)p_odb_data->scaler_save[i];
    pMUD_scal->label = strdup(p_odb_data->scaler_titles[i]);
    
    
    if(debug_mud) 
	printf("DARC_scalers: Scaler %d label=%s contents (uint)=%u\n",
                      i, pMUD_scal->label, pMUD_scal->counts[0] );
    MUD_addToGroup( pMUD_scalGrp, pMUD_scal );
  }
  return( SUCCESS );
}
/*------------------------------------------------------------------*/
  

INT
darc_check_files(D_ODB *p_odb_data, INT keep, INT *next_version)
{
/*
  This function deals with files of the form *_v* (i.e. version files)  
  
  For run = run_number, returns no. of files  on the disk, and the next version
  number.  Purges files depending on the value of parameter keep.

  Input   
         D_OBD *p_odb_data   pointer to odb data
         INT   keep          no. of files to keep for this run.
               if keep = 0   NO PURGE, returns no. of files found
               keep > 1   Files will be purged, retaining keep files on the disk.           
                           
                             
   Output
         INT   *next_version    Version number of the next run file.
         returns                no. of files on the disk for this run.

   Note - on error, next_version = 0 (too many files to sort)      
*/


  
  char dir[128];
  int nfile,i,j, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];
  char sorted[MAX_PURGE_FILES][25];
  char tmparray[MAX_PURGE_FILES][25];
  char *s;
  int versions[MAX_PURGE_FILES];
  int max, index;


  if(debug_check)
  {
    printf("\n");
    if (keep>0 )
      printf("darc_check_files starting, will purge if more than %d files for this run \n",keep);
    else
      printf("darc_check_files starting, will NOT purge \n");
  }
  *next_version=0;  /* indicates error */
  max=0; /* gives next_version = 1 if no files are found */
  sprintf(filename,"%06d.msr_v*",p_odb_data->run_number);
  sprintf(dir,"%s/",p_odb_data->save_dir);
  if(debug_check)
    {
      printf("p_odb_data->run_number=%d\n",p_odb_data->run_number);
      printf("p_odb_data->save_dir=%s\n",p_odb_data->save_dir);               
      printf("Looking for files: %s%s\n",dir,filename);
    } 
  nfile = ss_file_find(dir, filename, &list);
  if(debug_check)  printf ("Found  %d files for this run \n",nfile);

  if (nfile > MAX_PURGE_FILES)
  {
    cm_msg(MERROR,"darc_check_files","Too many files to sort. Cannot return next version");
    return (nfile);
  }
  
  for (j=0;j<nfile;j++)
  {
    pt = list+j*MAX_STRING_LENGTH;
//    printf ("list[%i]:%s\n",j, list+j*MAX_STRING_LENGTH);
    sprintf (tmparray[j],"%s", pt);
    if(debug_check)    printf("tmparray[%d]:%s\n",j,tmparray[j]);
    s = strchr(pt,'v');
    s++;
    
    versions[j] = atoi(s);
    if(debug_check)    printf("versions[%d] = %d\n",j,versions[j]);
    if(versions[j] > max ) max=versions[j];      
  }
  if(debug_check)printf("max = %d\n",max);
  *next_version= ++max;
  
  if(debug_check)   printf("returning next_version= %d\n",*next_version);
  if(keep == 0) return(nfile);  /* keep=0  no purge wanted  */
  if (nfile <= keep)
  {
    if(debug_check)
      printf("returning as not enough files to purge, nfile=%d keep=%d\n",nfile,keep);      
    return (nfile); /* not enough files to purge */  
  }
  /* sort the files by version number */
  for(i=0; i<nfile; i++)
  {
    max=0; /* reset max */  
    for(j=0; j<nfile; j++)
    {
      if(versions[j] >= max )
      {
        max=versions[j];
        index=j;
      }      
    }
   strncpy(sorted[i],tmparray[index],25);
    if(debug_check)printf("found max = %d, index=%d  sorted[%d] = %s\n",
                          versions[index],index, i, sorted[i]);
    versions[index]=0; /* reset version to zero */
    
  }
  /* now purge the files */
  for (j=keep; j<nfile; j++)
  {
    sprintf(outFile,"%s%s",dir,sorted[j]);
    if (debug_check) printf("darc_check_files: About to delete file %s (index=%d)\n",outFile,j);

    status = ss_file_remove(outFile);
    if (status)
    {
      cm_msg(MERROR,"darc_check_files","Failure deleting file %s (return status = %d)",outFile,status);
      return(nfile); /* return */
    }
    else    
      if (debug_check) printf("darc_check_files: Success from deleting file %s \n",outFile);   
  }
  return(nfile);
}

/*-------------------------------------------------------------------------------------------
  
  odb_save is modified from function in /midas/src/mlogger.c
  
*/
INT
odb_save ( INT run_number, char *odb_save_dir)
{
  int  size;
  char filename[20];
  char path[256];
  INT status; 
  
  if(debug || debug_check)
  {
    printf("\n");
    printf("odb_save: Starting with run_number: %d & odb_save_dir: %s\n",run_number,odb_save_dir);
  }
  strcpy(path,odb_save_dir); 
  if (path[0] != 0)
    if (path[strlen(path)-1] != DIR_SEPARATOR)
      strcat(path, DIR_SEPARATOR_STR);
  
  sprintf(filename,"%06d.odb",run_number);  
  strcat(path, filename);
  cm_msg(MINFO,"odb_save","Saving odb into %s",path); 
  
  status = db_save(hDB, 0, path, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MINFO,"odb_save","cannot save online data base");
    write_message1(status,"odb_save");
  }
  return(status);
}


/*-------------------------------------------------------------------------------------------
  
  DARC_camp_connect
  
*/
static INT
DARC_camp_connect ( char * camp_host )
{
  long status;
  int len, j;
  int camp_running;
  char ctemp[LEN_NODENAME+1];
  char serverName[LEN_NODENAME+1];
  /* Important globals: campInit, campFail, firstxTime  */

  camp_running = FALSE ; 
  if (campFail)  /* camp has failed initialize. Retrying often leads to a hang */
  {
    cm_msg(MINFO,"DARC_write","Camp failed to initialize. Camp sections cannot be logged");
  }
  else
  {
    len = strlen(camp_host);
    if(debug) printf("DARC_write : camp hostname  = %s, string length= %d \n",camp_host, len);
    if (len > 0)    /* a name is specified */
    { 
      strcpy (ctemp,camp_host );    
      for  (j=0; j< LEN_NODENAME ; j++) ctemp[j] = toupper (ctemp[j]); /* convert to upper case */                
      trimBlanks (ctemp,ctemp);
      if (strncmp(ctemp,"NONE",4) != 0)
      {
        strcpy( serverName, ctemp);
        if(debug)printf("DARC_write : Camp hostname: %s\n",serverName );
        camp_running  = TRUE   ; /* camp is running */
      }
    }
  }

  if( camp_running )
  {
    
    /* ping the camp host because if someone turns off the crate, there is
       no timeout and it can waste a lot of time  */ 
    {
      char cmd[256];
      sprintf(cmd,"ping -c 1 %s &>/dev/null",camp_host);
      if(debug) printf(" pinging camp host using command: %s\n",cmd);
      status=system(cmd);
    }
    if(status)
      cm_msg(MERROR,"DARC_write","Camp host %s is unreachable. Camp data cannot be saved",
             camp_host);
    else
    {

      /*
       *  Initialize CAMP connection  
       *  (timeout of 10 sec.)
       */   
      if(firstxTime)
      {
        if(debug)printf("firstxTime through: calling camp_clntInit\n");
        status = camp_clntInit( serverName, 10 );
        if(status == CAMP_SUCCESS)
        {
          campInit = TRUE;
          firstxTime = FALSE ;
          if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS. Set campInit TRUE & firstxTime FALSE\n");
        }
        else
        {
          cm_msg(MERROR,"DARC_write","Failed initialize call to CAMP");
          printf("Returned from camp_clntInit with CAMP_FAILURE\n");
          campInit = FALSE;
          campFail = TRUE; /* set a flag to prevent retry */
        }
      }
    } // end of camp_host reachable
  }  // end of camp_running
  else   /* camp not running */
  {  /* (if campFail is true, a message has already been written) */
    if (!campFail) cm_msg(MINFO,"DARC_write","Warning: CAMP is not running");
    campInit = FALSE;
  }

  return (campInit) ;

}


/* 
 * DARC_get_dynamic_headers: fill the temperature and/or field run header
 * with the average value from a Camp variable.  All failures are silently
 * ignored -- always return SUCCESS.
 */

static long DARC_get_dynamic_headers( D_ODB *p_odb_data )
{
  CAMP_VAR* pVar;
  CAMP_NUMERIC* pNum;
  long status;
  double val, stddev, skewness;
  char units[16], str[32];
  int n;

  if( p_odb_data->field_variable[0] == '/' || p_odb_data->temperature_variable[0] == '/' )
  {

    DARC_camp_connect( p_odb_data->camp_host );

    if( campInit )
    {

      if( ! campUpdated ) 
      {
        status = camp_clntUpdate();
        if( status == CAMP_SUCCESS )
          campUpdated = TRUE ;
	else
	  cm_msg(MERROR,"DARC_get_dynamic_headers","failed call to update CAMP data (%d)",status );
      }

      /* First, handle field header */
      if( campUpdated && p_odb_data->field_variable[0] == '/' )
      {
        status = campSrv_varGet( p_odb_data->field_variable, CAMP_XDR_NO_CHILD );
        if( status == CAMP_SUCCESS )
        {
          pVar = camp_varGetp( p_odb_data->field_variable );
          if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) == CAMP_VAR_TYPE_NUMERIC )
          {
            pNum = pVar->spec.CAMP_SPEC_u.pNum;

            if( pNum->num < 2 )
            {
              val = pNum->val;
              stddev = 0.0;
            }
            else
            {
              camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
            }
            /* Got field value.  Now do units conversion.
             * (see also title_validate in td_extra.tcl)
             */
            strcpy( units, "G");
            if( pNum->units )
            {
              strncpy( units, pNum->units, 4 );
              units[0] = toupper( units[0] );
              units[1] = toupper( units[1] );
              units[3] = '\0';
              if( units[0] == 'G' || units[0] == 'O' )
              {
                strcpy( units, "G");
              }
              else if( units[0] == 'T' )
              {
                val = val*10000. ;
                strcpy( units, "G");
              }
              else if( units[0] == 'M' && units[1] == 'T' )
              {
                val = val*10.0 ;
                strcpy( units, "G");
              }
              else if( units[0] == 'K' && ( units[1] == 'G' || units[1] == 'O' ) )
              {
                val = val*1000.0 ;
                strcpy( units, "G");
              }
              else if( units[0] == 'M' && ( units[1] == 'G' || units[1] == 'O' ) )
              {
                val = val/1000.0 ;
                strcpy( units, "G");
              }
	      else if( isalpha(*pNum->units ))
	      {
		strncpy( units, pNum->units, 15 );
		units[15] = '\0';
	      }
              else 
              {
                strcpy( units, "G" );
              }
	    }

            DARC_head_num( str, 31, val, stddev, units ) ;

            n = sizeof(p_odb_data->field) - 1 ;
            strncpy( p_odb_data->field, str, n );
            p_odb_data->field[n] = '\0';
          }
          else
          {
            cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s is not numeric", 
                   p_odb_data->field_variable );
          }
        }
        else
        {
          cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s does not exist", 
                 p_odb_data->field_variable );
        }
      }  /* finished field variable */

      /* Now handle temperature header */
      if( campUpdated && p_odb_data->temperature_variable[0] == '/' )
      {
        status = campSrv_varGet( p_odb_data->temperature_variable, CAMP_XDR_NO_CHILD );
        if( status == CAMP_SUCCESS )
        {
          pVar = camp_varGetp( p_odb_data->temperature_variable );
          if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) == CAMP_VAR_TYPE_NUMERIC )
          {
            pNum = pVar->spec.CAMP_SPEC_u.pNum;

            if( pNum->num < 2 )
            {
              val = pNum->val;
              stddev = 0.0;
            }
            else
            {
              camp_varNumCalcStats( pVar->core.path, &val, &stddev, &skewness );
            }
            /* Got temperature value.  Now do units conversion.
             * (see also title_validate in td_extra.tcl)
             */
            strcpy( units, "K");
            if( pNum->units )
            {
              strncpy( units, pNum->units, 4 );
              units[0] = toupper( units[0] );
              units[1] = toupper( units[1] );
              units[3] = '\0';
              if( units[0] == 'C' )
              {
                val = val+273.15 ;
                strcpy( units, "K");
              }
              else if( units[0] == 'M' && units[1] == 'K' )
              {
                val = val/1000.0 ;
                strcpy( units, "K");
              }
              else if( isalpha(*pNum->units ))
              {
                strncpy( units, pNum->units, 15 );
                units[15] = '\0';
              }
            }
            DARC_head_num( str, 31, val, stddev, units ) ;

            n = sizeof(p_odb_data->temperature) - 1 ;
            strncpy( p_odb_data->temperature, str, n );
            p_odb_data->temperature[n] = '\0';
          }
          else
          {
            cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s is not numeric", 
                   p_odb_data->temperature_variable );
          }
        }
        else
        {
          cm_msg(MERROR,"DARC_get_dynamic_headers","Camp variable %s does not exist", 
                 p_odb_data->temperature_variable );
        }
      }  /* finished temperature variable */
    }
  }
  return( SUCCESS );
}

/*
 *  Produce appropriately rounded character representation of val
 */

static void DARC_head_num( char* str, int len, double val, double err, char* units )
{
  double av, ae;
  int n;
  av = abs(val);
  ae = abs(err);

  if( av >= 1000000.0 )
    n = sprintf( str, "%.5g", val);
  else if( av >= 1000.0 || err >= 0.15 )
    n = sprintf( str, "%.1f", val);
  else if( av >= 100.0 || err >= 0.015 )
    n = sprintf( str, "%.2f", val);
  else 
    n = sprintf( str, "%.3f", val);

  strncat( str, units, len-n-1 ); 
  str[len-1] = '\0';
  return;
}
