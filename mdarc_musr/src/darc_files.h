/*
 $Log: darc_files.h,v $
 Revision 1.1  2003/10/07 22:07:00  suz
 original for mdarc_musr cvs tree

 Revision 1.2  2001/12/18 19:37:49  suz
 added darc_archive_file for cleanup.c

 Revision 1.1  2001/03/06 21:46:20  suz
 Original. Prototypes for subroutines now in darc_files.c. These prototypes
 mostly were in bnmr_darc.h formerly.


*/

void write_message(int status); /* old version - may use again */
void write_message1(int status, char* name);
INT darc_find_saved_file(INT run_number, char *save_dir);
INT darc_kill_files(INT killed_run_number, char *save_dir);
INT darc_rename_file(INT run_number, char *save_dir, char *archive_dir, char *saved_filename);
INT darc_set_symlink(INT run_number, char *save_dir, char *filename);
INT darc_unlink(INT run_number, char *save_dir);
void write_link_msg(INT status);
void write_rename_msg(INT status);
void write_remove_msg(INT status);
INT msg_print(const char *msg);
INT darc_archive_file(INT run_number, INT make_backup, char *save_dir, char *archive_dir);
