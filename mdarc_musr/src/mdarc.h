/*  mdarc.h
    common (global) variables for mdarc & bnmr_darc.c

    $Log: mdarc.h,v $
    Revision 1.1  2003/10/07 22:08:01  suz
    original for mdarc_musr cvs tree

    Revision 1.10  2002/04/17 20:20:55  suz
    add perl_path and expt_name

    Revision 1.9  2001/09/14 18:58:17  suz
    support MUSR with ifdef MUSR

    Revision 1.8  2001/04/30 20:13:29  suz
    run_type[32] -> run_type[5]

    Revision 1.7  2001/03/07 01:28:09  suz
    Global run number, run type & add toggle. Needed for automatic run numbering.

    Revision 1.6  2001/01/08 19:17:33  suz
    Add global firstxTime for opening/closing camp connection only at BOR/EOR.
    Use this version with mdarc.c bnmr_darc.c cvs version 1.9 .

    Revision 1.5  2000/10/27 19:26:48  midas
    Add nhistBins and nhistBanks to allow variable no. of histograms

    Revision 1.4  2000/10/11 20:13:44  midas
    add feclient name

    Revision 1.3  2000/10/11 04:10:50  midas
    added cvs log

*/

caddr_t pHistData;
INT     nHistBanks;
INT     nHistBins;
BOOL     firstxTime;

HNDLE   hDB, hKey;
BOOL    bGotEvent;
char    feclient[32], eqp_name[32];
INT     size;
char expt_name[HOST_NAME_LENGTH];

/* make the run number and run type global so
   if user changes them mid-run it is ignored. */
INT    run_number;
INT    old_run_number;
char   run_type[5];  /* test or real */
BOOL   toggle;

/* debugs */
BOOL debug ;
BOOL debug_mud;
BOOL debug_dump;
BOOL debug_check ;
INT  dump_begin;
INT  dump_length;
//BOOL timer ;    /* timing */
char perl_path[80]; /* perlscript path */

#ifdef MUSR
DWORD scaler_counts[16];
HNDLE hMusrSet;
#endif
