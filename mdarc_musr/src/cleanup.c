/********************************************************************\
  
  Name:         cleanup.c
  Created by:   Suzannah Daviel (from mdarc.c)
  
  Contents:  Cleanup BNMR saved files
  
  Previous revision history
  $Log: cleanup.c,v $
  Revision 1.1  2003/10/07 22:04:38  suz
  original for mdarc_musr cvs tree

  Revision 1.7  2002/05/14 17:58:01  suz
  Add ifdefs for MUSR version

  Revision 1.6  2002/05/10 17:55:34  suz
  Add a parameter to db_get_value for Midas 1.9

  Revision 1.5  2001/12/18 19:36:57  suz
  now archives files if needed (both types) using cpbnmr

  Revision 1.4  2001/03/29 19:54:03  suz
  Remove mdarc debug levels to simplify

  Revision 1.3  2001/03/06 21:49:57  suz
  Uses subroutines from darc_files.c rather than bnmr_darc

  Revision 1.2  2000/11/09 20:22:22  midas
  "mdarc" & "sis mcs" trees now created independently.
  These trees moved to /equipment/FIFO_acq/ instead of
  /equipment/FIFO_acq/settings, since new version of odb make allows experim.h to produce these trees independently.

  Revision 1.1  2000/11/08 00:49:54  midas
  Update to use experim.h and latest odb with path "../settings/sis mcs".
  Add to cvs.

  add to cvs
 

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#include "ybos.h"

#include "mdarc.h" /* common header for mdarc & bnmr_darc */
#include "experim.h" 
#include "darc_files.h" 

#define MAX_PURGE_FILES  50
#define FAILURE 0

INT setup_mdarc(); /* cd setup_hotlink in mdarc */
//void  hot_toggle (INT, INT, void * ); /* dummy here - should never be called */

HNDLE hDB;
INT rn;   // global run number to clean up
INT status;

HNDLE hMDarc; /* handle for /equipment/<eqp_name>/mdarc */
#ifdef MUSR
MUSR_TD_ACQ_MDARC fmdarc;
#else
FIFO_ACQ_MDARC fmdarc;
#endif

INT cleanup_init(void)
{
  INT   size;
  char  str[128];
  KEY   hKey;
  HNDLE hSet;
  HNDLE hArea;
  char save_dir[20];
  char archive_dir[20];  
  char filename[50]; 
  INT run_state;
  INT run_number;

  printf ("Cleanup starting ... \n");
  
  cm_get_experiment_database(&hDB, NULL);


  /* Make sure we are not going to cleanup the current run ... */

//  get the CURRENT run number
    size = sizeof(run_number);
    status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"tr_start","key not found  /Runinfo/Run number");
      write_message1(status,"tr_start");
      return (status);
    }
    if (rn == run_number)
      {

	/* get the run state to see if run is going */
	size = sizeof(run_state);
	status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"bnmr_darc","key not found /Runinfo/State");
	    write_message1(status,"bnmr_darc");
	    return (status);
	  }
	if(run_state == 3)
	  {
	    cm_msg(MERROR,"mdarc_cleanup","Run %d is still in progress; bad idea to clean it up now");
	    return(0);
	  }
      }

     status = setup_mdarc();
    if(status != DB_SUCCESS) return(0);

    printf(" cleanup: purging & renaming saved files for run  %d\n",rn);
    printf("        (i.e. end-of-run procedure) \n");


/*  Run cleanup procedure from end of run */
    
       
    if(debug)
    {
      printf("cleanup: saved_data_directory: %s\n",fmdarc.saved_data_directory);
      printf("cleanup: archived_data_directory: %s\n",fmdarc.archived_data_directory);
      printf("calling darc_rename with run number %d   \n",rn);
 
    }
    if(debug_check)printf("debug_check is true\n");
    status = darc_rename_file(rn,fmdarc.saved_data_directory, fmdarc.archived_data_directory, filename);
    if (status == SUCCESS)
    {
      if(debug) printf("cleanup: success from darc_rename_file; saved file:%s\n",filename);
      return (status);
    }
    else   /* may be no versions to purge. Check for .msr and if found, archive it */
      {
	printf("Cleanup: checking for a MUD  (.msr) file to Archive ...\n"); 
	if( rn < 30000 || rn >= 40000)
	  status = darc_archive_file(rn, FALSE, fmdarc.saved_data_directory, fmdarc.archived_data_directory  );
	else
	  printf("cleanup: test runs are not archived\n");
      }

    return (status);
}
/*---- setup_mdarc  ---------------------------------------------------*/

INT setup_mdarc()
{
  char str[128];
  INT stat;

#ifdef MUSR
  MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);
#else
  FIFO_ACQ_MDARC_STR(acq_mdarc_str); 
#endif
  if(debug) printf("setup_mdarc starting\n");


  /* Create record for mdarc area */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  
  status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
  if (status != DB_SUCCESS)
    {
       /* mdarc may be running - it has hotlinks on some mdarc records
	 which prevents create record from working */
       if ( cm_exist("mdarc",FALSE)== CM_NO_CLIENT)
	 {
	/* mdarc is NOT running so create record should have worked */
	  cm_msg(MERROR,"setup_mdarc","Failed to create record for %s  (%d).", str,status);
	  write_message1(status,"setup_mdarc");
	  return(status);
	}
    }	  
  /* get the key hMDarc  */
 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_mdarc", "key %s not found (%d)", str,status);
    write_message1(status,"setup_mdarc");
    return (status);
  }



  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */





  
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_mdarc","Failed to retrieve %s record  (%d)",str);
    write_message1(status,"setup_mdarc");
    return(status);
  }
  return(status);
}  

 





/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status;
  DWORD  j, i, last_time_kb;
  HNDLE  hDB, hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg;
  BOOL   cr=FALSE;
  char   clean_run[25];
  //  char   dbg[25];
  int    bug;


  /* set defaults for debug */
  debug = FALSE;
  debug_check = FALSE;

  /* set defaults for other globals */
   toggle = FALSE;  /* initial value */
   run_number = -1; /* global run number */


/*  Midas 9.1  cm_get_environment (host_name, expt_name); */
   cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);    

  /* initialize global variables */
#ifdef MUSR
  sprintf(eqp_name,"MUSR_TD_acq");
  sprintf(feclient,"fev680");
#else  
  sprintf(eqp_name,"FIFO_acq");
  sprintf(feclient,"febnmr2");
#endif
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {

    if (argv[i][0] == '-'&& argv[i][1] == 'd')
      debug = debug_check = TRUE;
    else if  (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
        goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i],"-q",2)==0)
        strcpy(eqp_name, argv[++i]);
      else if (strncmp(argv[i],"-f",2)==0)
        strcpy(feclient, argv[++i]);
       else if (strncmp(argv[i],"-r",2)==0)
      {
        strcpy(clean_run, argv[++i]);
        cr=TRUE;
      }
      
    }
    else
    {
   usage:
#ifdef MUSR
      printf("\ncleanup: purges, renames (if necessary) and archives MUSR run files\n\n"); 
#else
      printf("\ncleanup: purges, renames (Type2) and archives (Types 1&2) MUSR run files\n\n"); 
#endif
      printf("usage: cleanup  [-h Hostname] [-e Experiment]  [-r Runnumber]\n");
      printf("              [-f FE client name] (default:%s)\n",feclient);
      printf("              [-q FE equipment name] (default:%s)\n",eqp_name);
      printf("                      [-d ] (debug)   \n");
      return 0;
    }
    
  }
  if(cr)
  {
    rn = atoi(clean_run);
    if (rn <= 0)
    {
      printf("Invalid run number for cleanup; supplied run number was %s\n",clean_run);
      return 0;
    }
    else
      printf("About to run cleanup procedure for run %d\n",rn);
  }
  else
  {
    printf("Supply run number for cleanup\n");
    return 0;
  }
  if (debug)
    printf("debug is TRUE\n");    
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "mdarc_cleanup", 0);
  if (status != CM_SUCCESS)
    return 1;
  
  if(debug)
  cm_set_watchdog_params(TRUE, 0);

  
  
  /* turn off message display, turn on message logging */
  // cm_set_msg_print(MT_ALL, 0, NULL);
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);  

  /* connect to the database */
//  cm_get_experiment_database(&hDB, &hKey);
  
  status = cleanup_init();


  cm_disconnect_experiment();

}














