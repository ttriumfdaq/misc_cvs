/* Subroutines for use with mdarc

   Contents :
   darc_rename_file         write_link_msg
   darc_find_saved_file     write_rename_msg
   darc_set_symlink         write_remove_msg
   darc_unlink              write_message
   darc_kill_files          write_message1
   darc_archive_file        msg_print

 $Log: darc_files.c,v $
 Revision 1.1  2003/10/07 22:06:32  suz
 original for mdarc_musr cvs tree

 Revision 1.6  2002/07/18 19:12:21  suz
 stop archiving of test runs
 

*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
/* midas includes */
#include "midas.h"
#include "msystem.h" //MAX_STRING_LENGTH
#include "ybos.h" // MAX_FILE_PATH

#include "darc_files.h"
//#include "experim.h" /* odb structure */

#define MAX_PURGE_FILES  50
#define FAILURE 0

// for archiving
char archiver[]="/home/musrdaq/musr/mdarc/bin/cpbnmr" ;
char infofile[]="archiver.txt";

/* global debugs from mdarc.h (which is not included here) */
BOOL debug ;
BOOL debug_mud;
BOOL debug_dump;
BOOL debug_check ;
INT  dump_begin;
INT  dump_length;


INT
darc_rename_file(INT run_number, char *save_dir, char *archive_dir, char *saved_filename)
{
/*
  Called at end-of-run to purge and rename the saved files.
  Also archives data by copying the final run file to the directory given by
  archive_dir if archive_dir is not a null string. 
    
  Does not pass p_odb_data since tr_stop does not have access to this.
  
  Input
         INT  run_number     run number
         char *save_dir      directory where files are saved
         char *archive_dir   directory where files are archived
       
                             
   Output

         returns            SUCCESS or FAILURE
        saved_filename      filename of saved file
*/


  
  char dir[128];
  int nfile,i,j, len, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char sorted[MAX_PURGE_FILES][30];
  char tmparray[MAX_PURGE_FILES][30];
  char *s;
  int versions[MAX_PURGE_FILES];
  int max, index;
  struct stat stat_buf;
  char copy_string[128];
  char str[256];
  // for archiving
  FILE *errfile;
  INT first;
  char hdrstring[80];

  if(debug_check)
  {
    printf("\n");
    printf("darc_rename_file starting with run_number=%d\n",run_number);
    printf("      and save_dir=%s, archive_dir=%s\n",save_dir,archive_dir);    
  }
  max=0;
            
  sprintf(filename,"%06d.msr_v*",run_number);
  sprintf(dir,"%s",save_dir);
  if (dir[0] != 0)
    if (dir[strlen(dir)-1] != DIR_SEPARATOR)
      strcat(dir, DIR_SEPARATOR_STR);
  
  if(debug_check)  printf("darc_rename_file:Looking for files: %s%s\n",dir,filename);
  
  nfile = ss_file_find(dir, filename, &list);
  if(debug_check)  printf ("darc_rename_file:Found  %d files for this run \n",nfile);
  if(nfile <= 0 )
  {
    cm_msg(MERROR,"darc_rename_file","Found no saved files %s%s",dir,filename);
    return(FAILURE);
  }
  
  if (nfile > MAX_PURGE_FILES)
  {
    cm_msg(MERROR,"darc_rename_file","too many files to sort. Cannot rename file");
    return (FAILURE);
  }
  for (j=0;j<nfile;j++)
  {
    pt = list+j*MAX_STRING_LENGTH;    /* MAX_STRING_LENGTH (max string length for
                                         odb) defined as 256 in  msystem.h */
//    printf ("list[%i]:%s\n",j, list+j*MAX_STRING_LENGTH);
    sprintf (tmparray[j],"%s", pt);
    if(debug_check)    printf("darc_rename_file:tmparray[%d]:%s\n",j,tmparray[j]);
    s = strchr(pt,'v');
    s++;
    
    versions[j] = atoi(s);
    if(debug_check)    printf("darc_rename_file:versions[%d] = %d\n",j,versions[j]);
    if(versions[j] > max ) max=versions[j];      
  }
    
 
  /* sort the files by version number */
  for(i=0; i<nfile; i++)
  {
    max=0; /* reset max */  
    for(j=0; j<nfile; j++)
    {
      if(versions[j] >= max )
      {
        max=versions[j];
        index=j;
      }      
    }
    strncpy(sorted[i],tmparray[index],25);
    if(debug_check)    printf("darc_rename_file:found max = %d, index=%d  sorted[%d] = %s\n",
                              versions[index],index, i, sorted[i]);
    versions[index]=0; /* reset version to zero */
    
  }
  /* now purge the files, leaving one only */
  for (j=1; j<nfile; j++)
  {
    sprintf(outFile,"%s%s",dir,sorted[j]);
    if(debug_check)    
      printf("darc_rename_file: About to delete file %s (index=%d)\n",outFile,j);

    status = remove(outFile);
    if (status==-1)
    {
      cm_msg(MERROR,"darc_rename_file","Failure to delete  %s ",outFile,status);
      write_remove_msg(status);
      return(FAILURE); /* return */
    }
    else    
      if (debug_check) printf("darc_rename_files: Success from deleting file %s \n",outFile);   
  }


  /* Generate filename for rename   */
  if(debug_check) printf("darc_rename_file: Starting renaming procedure... \n");
  sprintf(filename,"%s%s",dir,sorted[0]);
  /* Find last occurrence of '_' in the string */
  s = strrchr(filename,'_');  /* strip off the "_v*" */
  len = (int)(s - &filename[0]);
  strncpy (outFile,filename,len);
  outFile[len]='\0'; /* terminate string */

 
  /* check for REGULAR file and SYMBOLIC LINK */
  if(debug_check)
  {
    printf("darc_rename_file: outFile = %s \n",outFile);
    printf("    - must correspond to the following line:\n");
    printf("    save_dir = %s and run_number=%d\n",save_dir,run_number);
    printf("darc_rename_file:Calling darc_unlink which calles darc_find_saved_file\n");  
  }

// darc_unlink calls darc_find_saved_file
  status = darc_unlink(run_number, save_dir);
  if (status == 1)
  {
    cm_msg(MERROR,"darc_rename_file","Regular saved file already exists for this run %d",run_number);
    cm_msg(MERROR,"darc_rename_file","Rename file at end-of-run FAILS. ");
    return(FAILURE);   
  }
  
  if(debug_check) printf(" darc_rename_file:  new file name will be : %s\n",outFile);
  status = rename(filename,outFile);
  if (status == -1 )
  {
    cm_msg(MERROR,"darc_rename_file","ERROR trying to rename file %s",filename);
    printf("darc_rename_file: to %s because\n",outFile);
    write_rename_msg(status);
    return(FAILURE);
  }
  
  printf("darc_rename_file: Renamed final saved file %s\n",filename);  /* SUCCESS */
  printf("                  to %s\n",outFile);
  strcpy (saved_filename,outFile);
  cm_msg(MINFO,"darc_rename_file","End-of-run final saved file: %s",outFile);
  if(debug_check) printf("darc_rename_file: Returning saved filename %s\n",saved_filename);      

 if ( run_number  >= 30000 && run_number <=30099  ) 
   return (SUCCESS);  // do not archive test runs
 
/* copy file to archive */
  if (strlen(archive_dir) > 0 )
    { 
      /* cpbnmr  submits a data file to the permanent archive */

      unlink (infofile); // delete any old version
      sprintf(copy_string,"%s %s %s &>%s",archiver,outFile, archive_dir, infofile);
      if(debug_check)printf("darc_rename_file : Sending system command cmd: %s\n",copy_string); 
      status =  system(copy_string);
      if ( status )
        {
	  errfile = fopen(infofile,"r");
	  sprintf(hdrstring,"ERROR archiving %s to %s",
		     outFile, archive_dir );
	  cm_msg(MERROR,"darc_rename_file",hdrstring);
	  printf("darc_rename_file: %s\n",hdrstring);

	  if (errfile != NULL)
	    {
	      while(fgets(str,256,errfile) != NULL)
 	        {
		  printf("%s\n",str);
		  cm_msg(MINFO, "darc_rename_file","%s",str );
		}
	      fclose(errfile);
	    } // if (errfile != NULL) 
	  return (FAILURE);
	} // if ( status )
      else
        {
	  printf("Successfully archived file %06d.msr\n",run_number);
	  cm_msg(MINFO,"darc_rename_file","Successfully archived file %06d.msr to %s",run_number,archive_dir);
	}	  	  
    }  // end of if archived data directory exists
  else
    cm_msg(MINFO,"darc_renamefile"," INFO - file not archived as archive directory is NULL\n");

  return(SUCCESS);
}

INT
darc_find_saved_file(INT run_number, char *save_dir)

{
/*
  Looks for a final saved file with no version string for this run.

  If it finds a file, it test to see if it is a soft link (using lstat) or
  a real file. 
  

  Does not pass p_odb_data (may be called from tr_stop which does not have access to
  p_odb_data).
  
  Input
         INT  run_number     run number
         char *save_dir      directory where files are saved

                             
   Output

         returns 0          if no files at all
         returns 1          if a REGULAR file is found.
                            (a symbolic link does not count!)
         returns 2          if a file is found that is a SYMBOLIC LINK

                 see  /usr/include/statbuf.h for struct stat 

*/  
  char filename[MAX_FILE_PATH];
  struct stat stat_buf; /* needed for lstat */
  int status;

  if(debug_check)
  {
    printf("\n");
    printf("darc_find_saved_file starting\n");
    printf("save_dir = %s and run_number=%d\n",save_dir,run_number);
  }
  sprintf(filename,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  if (debug_check) printf("darc_find_saved_file: running lstat on file: %s\n",filename);

  /* lstat the file */
  status=lstat (filename,&stat_buf) ;
  if (debug_check)printf("darc_find_saved_file: status after lstat: %d\n",status);
  if (status == -1)
  {
    if(debug_check)
      {
	printf("darc_find_saved_file: lstat bad status indicates file not found (errno=%d (0x%x)) \n",errno);
	write_link_msg(status);
      }
    return(0); /* no such file */
  }

  /* the file exists */
  
  /* test for symbolic link */
  status = S_ISLNK(stat_buf.st_mode);
  if (debug_check) printf("Testing for symbolic link, status = %d\n",status);
  if(status)
  {
    if (debug_check)printf("File is a symbolic link, S_ISLNK is true\n");
    return(2);
  }  
  /* test for real file */
  status = S_ISREG(stat_buf.st_mode);
  if (debug_check)printf("Testing for real file, status = %d\n",status);
  if(status)
  {
    if (debug_check)printf("File is a real file, S_ISREG is true\n");
    return(1);
  }  
  return(0); /* No file found or strange error  */
}

INT
darc_set_symlink(INT run_number, char *save_dir, char *filename)
{
/* set up a symbolic link for analysis pgms to always look for filename
     of type 040001.msr


     inputs:     run_number      use to generate name of 
                 save_dir        symbolic link
                 filename        name of regular file (e.g. 040001.msr_v32)

     returns:    SUCCESS or FAILURE

*/
  char symlink_name[MAX_FILE_PATH];
  int status;
  
  if(debug_check)
  {
    printf("\n");
    printf("darc_set_symlink starting\n");
  }
  sprintf(symlink_name,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  status=symlink(filename, symlink_name);
  if(status==-1)
  {
    cm_msg(MERROR,"darc_set_symlink","failure from symlink, (errno=%d)",errno);
   write_link_msg(status);
   return(FAILURE);
  }
  else if(debug || debug_check)
  {
    printf("darc_set_symlink: successfully linked %s \n",symlink_name);
    printf("darc_set_symlink: to %s \n",filename);
  }
  return(SUCCESS);
}

INT
darc_unlink(INT run_number, char *save_dir)
{
/* looks for a file of type  run_number.msr, checks it is
   a symlink (not a real file) and deletes it.
   

     inputs:     run_number      use to generate name of 
                 save_dir        symbolic link

     returns:    0  No files were found 
                 1  file is a REGULAR final saved file not a SYMLINK
                 2  SUCCESS file was a symlink & was deleted successfully
                 3  file was a symlink but delete failed
        
                 

*/
  char filename[MAX_FILE_PATH];
  int status;
  
  if(debug_check)
  {
    printf("darc_unlink starting\n");
    printf("\n");
  }
  sprintf(filename,"%s/%06d.msr",save_dir,run_number);  /* No version string */
  
  status =  darc_find_saved_file(run_number, save_dir) ;
  if (status == 0)
  {
    if(debug)printf("darc_unlink: no file %s was found\n",filename);
    return(status);
  }
  
  if (status == 1)
  {
    printf("darc_unlink: A REGULAR final saved file already exists\n ");
    return(status);
  }
  
  /* this is the usual case */
  if( status  == 2)
  {
    if(debug_check)printf("INFO - a SYMBOLIC LINK already exists for this run (%d)\n",run_number);
    status = unlink(filename); /* deletes symbolic link */
    if (status == -1)
    {
      printf("darc_unlink: deleting symlink %s failed due to \n",filename);
      write_link_msg(status); //unlink
      return(3); /* status = 3 couldn't delete symlink */
    }
  }
  return(2); /* success */
}



INT
darc_kill_files(INT killed_run_number, char *save_dir)

{
/*
  Called if (global) toggle is set.
  After run number is toggled, and data is saved under the new run number, this routine
  is called to delete the saved files.

  Note - there will be no archived file for the old run number since they are copied on end-of-run.
  Toggle is only permitted if running.
  
  
  
  Input
         INT  killed_run_number run number of killed run
         char *save_dir      directory where files are saved
       
                             
   Output
         returns            SUCCESS or FAILURE
*/


  
  char dir[128];
  int nfile,i,j, len, status;
  char * list = NULL;
  char outFile[128];
  char *pt;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char sorted[MAX_PURGE_FILES][30];
  char tmparray[MAX_PURGE_FILES][30];
  char *s;
  int versions[MAX_PURGE_FILES];
  int  index;
  struct stat stat_buf;
  char copy_string[128];

  
  if(debug || debug_check )
  {
    printf("\n");
    printf("darc_kill_files starting with killed run number=%d & save_dir=%s\n",killed_run_number,save_dir);    
  }

            
  sprintf(filename,"%06d.msr*",killed_run_number);
  sprintf(dir,"%s",save_dir);
  if (dir[0] != 0)
    if (dir[strlen(dir)-1] != DIR_SEPARATOR)
      strcat(dir, DIR_SEPARATOR_STR);
  
  if(debug_check)  printf("darc_kill_files:Looking for files: %s%s\n",dir,filename);
  
  nfile = ss_file_find(dir, filename, &list);
  if(debug_check)
    printf ("darc_kill_files: Found  %d files for killed run %d \n",nfile,killed_run_number);
  if(nfile <= 0 )
  {
    cm_msg(MINFO,"darc_kill_files","Found no saved files for killed run  %s%s",dir,filename);
    return(SUCCESS);
  }
  
  /* now delete the files */
  for (j=0; j<nfile; j++)
  {
    pt = list+j*MAX_STRING_LENGTH;    /* MAX_STRING_LENGTH (max string length for
                                         odb) defined as 256 in  msystem.h */
    //printf ("list[%i]:%s\n",j, list+j*MAX_STRING_LENGTH);
    sprintf (tmparray[j],"%s", pt);
    
    sprintf(outFile,"%s%s",dir,tmparray[j]);
    if(debug_check)    
      printf("darc_kill_files: About to delete file %s (index=%d)\n",outFile,j);

    status = remove(outFile);
    if (status==-1)
    {
      cm_msg(MERROR,"darc_kill_files","Failure to delete  %s ",outFile,status);
      write_remove_msg(status);
      return(FAILURE); /* return */
    }
    else    
       cm_msg(MINFO,"darc_kill_filess","Successfully deleted old run file %s",outFile);   
  }

  return(SUCCESS);
}

void
write_link_msg(INT status)
  /* translate error codes returned by unlink and symlink */
{
  if (errno ==EEXIST)   // no unlink 
    printf("     a symlink with this name already exists\n"); 
  else if (errno == ENOENT)
     printf("     Non-existent dir component or dangling symbolic link\n");
  else if (errno == ENOTDIR)
    printf("     component used as dir is not a directory\n");
   else if (errno == ENAMETOOLONG) //unlink,symlink  OK
    printf("     name too long\n");
  else if (errno == EISDIR)  // unlink OK;  
    printf("     pathname refers to a directoy\n");
  else if (errno == EPERM) //bad for unlink; symlink OK
    printf("     filesystem doesn't support symbolic links\n");
  else if (errno == EFAULT)  // unlink, symlink  OK
    printf("     filename(s) point outside accessible address space\n");
  else if (errno == EACCES) // unlink, symlink  OK
    printf("     write access or search permission not allowed in one of directories\n");
  else if (errno == ENAMETOOLONG) // unlink ,symlink OK
    printf("     pathname too long\n");
  else if (errno == EROFS)  // unlink,symlink  OK 
    printf("     pathname refers to a read-only filesystem\n");
  else if (errno == ELOOP)  // unlink symlink OK 
    printf("     too many symbolic links were encountered\n");
  else
    printf ("    an error not decoded by write_link_msg\n");
}




void
write_rename_msg(INT status)
  /* translate most common error codes returned by rename */
{
  if (errno ==EEXIST)    
    printf("     the new pathname contained a path prefix of the old\n");
  else if (errno == EISDIR)    
    printf("     newpath is an existing directory, but oldpath is not a directory\n");
  else if (errno == EXDEV)
    printf("     oldpath and newpath are not on the same filesystem\n");
  else if (errno == ENOTEMPTY)
    printf("     newpath is a non-empty directory\n");
  else if (errno == ENOTDIR)
    printf("     component used as dir is not a directory\n");
  else if (errno == EACCES) 
    printf("     write access or search permission not allowed in one of directories\n");
  else if (errno == EFAULT)  
    printf("     pathname(s) point outside accessible address space\n");
  else if (errno == ENAMETOOLONG) 
    printf("     pathname(s) were too long\n");
  else if (errno == ENOENT)
    printf("    non-existent dir component or dangling symbolic link\n");
  else if (errno == EROFS) 
    printf("     the file is on a read-only filesystem\n");
  else if (errno == ELOOP)   
    printf("     too many symbolic links were encountered\n");  
  else if (errno == ENOSPC)  
    printf("     device has no room for the new directory entry \n");


  else
    printf ("     an error not decoded by write_rename_msg\n");
}


void
write_remove_msg(INT status)
  /* translate error codes returned by remove */
{
  if (errno == EFAULT) 
    printf("     pathname points outside accessible address space\n");
  else if (errno == EACCES)
    {
      printf("     write access  to the directory containing pathname is not allowed\n"); 
      printf("     for the process's effective uid,  or  one of the directories in\n");
      printf("     pathname did not allow search (execute) permission.\n");
    }   
  else if (errno == EPERM)
    {
      printf("     directory containing pathname has the sticky-bit (S_ISVTX) set and the\n");
      printf("     process's effective uid is neither the uid of the file to be deleted\n");
      printf("     nor that of the directory containing it.\n"); 
    }
  else if (errno == ENAMETOOLONG) 
    printf("     pathname too long\n");
  else if (errno == ENOENT)
    printf("     non-existent dir component or dangling symbolic link\n");
   else if (errno == ENOTDIR)
    printf("     component used as dir in pathname is not a directory\n");
  else if (errno == ENOMEM)
    printf("  Insufficient kernel memory was available.\n");   
  else if (errno == EROFS)   
    printf("     pathname refers to a read-only filesystem\n");
  else
    printf ("    an error not decoded by write_remove_msg\n");
}



void
write_message(INT status)
{
  char str[60];
  str[0]= '\0';
 printf("write_message .... \n"); 
  if (status == DB_INVALID_HANDLE) sprintf(str,"    because of invalid database or key handle\n"); 
  else if (status == DB_NO_KEY) sprintf (str,"    because   key_name does not exist\n");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"    because type does not match type in ODB\n");
  else if (status == DB_TRUNCATED) sprintf(str,"    because  data does not fit in buffer and has been truncated\n"); 

}
void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}


/*---- msg_print ---------------------------------------------------*/
INT msg_print(const char *msg)
{
  /* print message to system log */
  ss_syslog(msg);

  /* print message to stdout */
  return puts(msg);
}

INT
darc_archive_file(INT run_number, INT make_backup, char *save_dir, char *archive_dir)
{
/*
  Called by cleanup  (mdarc uses darc_rename_file for archiving)

  Archives data by copying the final run file to the directory given by
  archive_dir if archive_dir is not a null string. 

  If the file already exists in the archived directory, 
     if make_backup is true : copies new file making a backup of the existing file 
     else                     does not copy file to archive
  
  Input
         INT  run_number     run number
         INT  make_backup    true or false (see above)
	 char *save_dir      directory where files are saved
         char *archive_dir   directory where files are archived
       

                             
   Output

         returns            SUCCESS or FAILURE
        saved_filename      filename of saved file
*/


  
  char sdir[128];
  char adir[128];
  int nfile,i,j, len, status;
  char * list = NULL;
  char filename[MAX_FILE_PATH];  /* MAX_FILE_PATH is defined as 128  in /midas/include/ybos.h */
  char outFile[MAX_FILE_PATH];
  struct stat stat_buf;
  char copy_string[128];

  // for archiving
  FILE *errfile;
  INT first;
  char hdrstring[80];
  char str[256];

  if(debug_check)
  {
    printf("\n");
    printf("darc_archive_file starting with run_number=%d, make_backup=%d\n",run_number,make_backup);
    printf("      and save_dir=%s, archive_dir=%s\n",save_dir,archive_dir);    
  }
  if (strlen(archive_dir) <= 0 )
    {
      printf("darc_archive_file: INFO - file not archived as save directory is NULL\n");
      return(FAILURE);
    }
  else if  (strlen(save_dir) <= 0 )
    {
    printf("darc_archive_file: INFO - file not archived as archive directory is NULL\n");
    return(FAILURE);
    }
  else if  ( run_number  >= 30000 && run_number <=30099  )
    {
    printf("darc_archive_file: INFO - test files are not archived \n");
    return(FAILURE);
    }

            
  sprintf(filename,"%06d.msr",run_number);
  sprintf(sdir,"%s",save_dir);
  if (sdir[0] != 0)
    if (sdir[strlen(sdir)-1] != DIR_SEPARATOR)
      strcat(sdir, DIR_SEPARATOR_STR);
  
  sprintf(outFile,"%s%s",sdir,filename);
  if(debug_check)  printf("darc_archive_file:Looking for saved files: %s\n",outFile);
  

  sprintf(adir,"%s",archive_dir);
  if (adir[0] != 0)
    if (adir[strlen(adir)-1] != DIR_SEPARATOR)
      strcat(adir, DIR_SEPARATOR_STR);
  
  if(debug_check)  printf("darc_archive_file:Checking archive for files: %s%s\n",adir,filename);
   

  /* check for REGULAR  msr file only */
 
  nfile = darc_find_saved_file(run_number, save_dir);
  if (nfile == 1)
    {
      if(debug) printf("darc_archive_file: Found saved file (%06d.msr) ",run_number) ;
    }
  else
    {
      printf("No regular saved file (%06d.msr) found. Archive not done\n",run_number);
      return (SUCCESS);
    }
  
  /* DO NOT...
     check if there is a file already in the archive  *****
  nfile = darc_find_saved_file(run_number, archive_dir);
  if(debug)printf("darc_archive_file: after darc_find_saved_file, nfile=%d; make_backup=%d\n",nfile,make_backup);
  if (nfile == 1)
    {
      if (make_backup)
	printf("darc_archive_file: Warning - there is an existing archived file. A backup (%06d.msr~) will be made.\n",run_number) ;
      else
	{
	  printf("darc_archive_file: Warning - there is an existing archived file (%s%06d.msr). Archiving aborted\n",adir,run_number) ;
	  return(SUCCESS);
	}
    }
  printf("darc_archive_file: found no existing archived file\n");

  */

  /* cpbnmr submits a data file to the permanent archive */

  sprintf( copy_string,"%s %s %s", archiver, outFile, archive_dir );
  if(debug_check)printf("darc_archive_file : Sending system cmd: %s\n",copy_string); 

  printf("Archive file %06d.msr to %s\n",run_number,archive_dir);
  
  status =  system(copy_string);

  if ( status )
    {
      printf("darc_archive_file : FAILURE\n");
      return (FAILURE);
    }
  else
    return(SUCCESS);
}


