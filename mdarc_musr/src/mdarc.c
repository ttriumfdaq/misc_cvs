/********************************************************************\
  Name:         mdarc.c
  Created by:   
  Contents:    data logger for MODAS (Musr DAQ) run under MIDAS on linux


  $Log: mdarc.c,v $
  Revision 1.1  2003/10/07 21:59:24  suz
  original for mdarc_musr cvs tree

  Revision 1.25  2003/01/08 18:45:24  suz
  reset run number if no final data

  Revision 1.24  2002/07/18 19:15:18  suz
  reset last_time on save now so next save will be save_interval seconds from last save, whether triggered by save now or by automatic save

  Revision 1.23  2002/07/10 19:01:37  suz
  add conditional on camp_hostname

  Revision 1.22  2002/06/24 20:33:03  suz
  more changes for triggering histo event (MUSR). Ifdefs for BNMR

  Revision 1.21  2002/06/12 18:16:26  suz
  mdarc now requests histo events (musr). Changes to febnmr needed for bnmr to work with this version. Camp_hostname now under mdarc/camp in odb

  Revision 1.20  2002/05/10 17:55:34  suz
  Add a parameter to db_get_value for Midas 1.9

  Revision 1.19  2002/04/17 20:17:54  suz
  many changes for MUSR; perlscripts now take include_path

  Revision 1.18  2001/12/18 22:36:54  suz
  add flag write_data for edit_on_start parameter c.f. type 1

  Revision 1.17  2001/12/18 19:32:07  suz
  create mdarc record only if sizes are different

  Revision 1.16  2001/09/14 18:55:47  suz
  support MUSR with ifdef MUSR

  Revision 1.15  2001/06/29 19:24:33  suz
  add support for HM histos from second scaler

  Revision 1.14  2001/04/30 20:15:36  suz
  Add flag to disable automatic run numbering; move run_type from edit on start to mdarc area

  Revision 1.13  2001/03/30 18:56:45  suz
  get record from mdarc in tr_start to get current no. bins
  Revision 1.12  2001/03/29 19:45:25  suz
  For debugging, use perl script from appropriate directory

  Revision 1.11  2001/03/07 17:51:20  suz
  includes darc_files.h & msg_print moved to darc_files.c

  Revision 1.10  2001/03/06 21:32:36  suz
  Add support for automatic run numbering with get_next_run_number.pl and for toggle button.

  Revision 1.9  2001/01/08 19:13:21  suz
  Allow mdarc to run even if frontend is not. Camp connection opened/shut on BOR/EOR.
  Create record only if run is not going. Fix confusing messages for users.

  Revision 1.8  2000/11/09 20:24:53  midas
  "mdarc" & "sis mcs" trees now created independently.
  The trees moved to /equipment/FIFO_ACQ (no more settings). New version
  of odb make cmd creates an experim.h with these trees separate.

  Revision 1.7  2000/11/03 20:41:13  midas
  fix bug in memcpy data to pHistData array

  Revision 1.6  2000/10/27 18:59:33  midas
  Further generalization for use with frontend other than SIS.
  All directories in odb for SIS under ../settings moved under
  ../settings/sis mcs, to enable creating record separately for sis and
  for mdarc. No. of histograms no longer fixed at 4; get number of defined
  histograms and number of bins from the midas event.

  Revision 1.5  2000/10/13 20:11:23  midas
  generalize process_event to allow variable no. of histograms

  Revision 1.4  2000/10/11 20:01:57  midas
  - extend 4 histo to N histo in process_event()
  - moved setup_hotlink before tr_start in main
  - Added create record in setup_hotlink based on experim.h

  Revision 1.3  2000/10/11 04:10:50  midas
  added cvs log


  Version 2.0  July 2000  
               Extentively Modified for BNMR2.  
               Uses experim.h . Area/rig in odb has gone. Setup params are in Equipment/<eqp_name>/Settings

  Version 2.1  Aug 2000
               CAMP connection opened once at start of mdarc, closed on exit.
               No. bins fetched from "../output" in REAL mode or "../sis test mode" in TEST mode. 
 
  Version 2.3 Dec 4 2000
  Less confusing messages from mdarc. Camp connection opened/shut on BOR/EOR. Create record only
  if run isn't going. Allow mdarc to run even if frontend is not. 
         
  Version 1.18 (try setting version to go with CVS version - have to remember!)

\*****************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#include "ybos.h"

#include "mdarc.h" /* common header for mdarc & bnmr_darc */
#include "darc_files.h" /* for msg_print only */
#include "experim.h" 

// note caddr_t is defined as char*

//#ifdef MUSR
//#include "musr_subs.h"  // for get_musr_rec
//#endif

/* prototypes */
INT    tr_start(INT rn, char *error);
INT    tr_stop(INT rn, char *error);

INT    setup_hotlink();
INT    setup_hot_toggle();
void   update_Time(INT , INT , void *);
void  hot_toggle (INT, INT, void * );
INT print_file(char* filename);

#ifdef MUSR
// MUSR manually triggers events from frontend
INT get_client_name(char *pname);
BOOL trigger_histo_event(INT *state_running);
char ClientName[256];

// needed for MUSR save now command
INT    setup_hot_save();
void   hot_save (INT, INT, void * );
HNDLE  hSave;
#endif
BOOL   saving_now; // flag needed for MUSR hotlink, defined but not used by BNMR

// globals
//          (globals shared with bnmr_darc.c in mdarc.h)

float version = 1.18; /* mdarc version number - set to closest CVS version */ 

DWORD  time_save, last_time, nhist;
INT    hBufEvent;
INT    request_id;
INT   status;
BOOL  mdarc_need_update;
DWORD mdarc_last_call;
INT debug_proc; // global for debug of process_event

HNDLE hMDarc; // handles for mdarc
char beamline[6];
INT run_state;
BOOL write_data; // odb  mdarc flag enable_mdarc_logging

#ifdef MUSR
MUSR_TD_ACQ_MDARC fmdarc;
#else
FIFO_ACQ_MDARC fmdarc;
#endif

char perl_script[80]; /* get_run_number.pl  Directory now read from odb in setup_hotlink */
char outfile[]="/var/log/midas/get_run_number.txt";
char errfile[]="/var/log/midas/get_run_number.lasterr";
/* initialize values (in mdarc.h) */
BOOL   bGotEvent = FALSE;

/*----- start  ----------------------------------------------*/
INT tr_start(INT rn, char *error)
{
  char str[128];
  char string[256];
  HNDLE hRM,  hKeyTmp;
  char  cmd[132];
  int j;
  int stat;
  

  /* called every new run and as a dummy if mdarc is started */
  if(debug)printf("tr_start starting, setting firstxTime true\n");

  firstxTime = TRUE;  /* set global flag so bnmr_darc opens camp connection on begin run  */


  /* Use of cm_exist:

 Purpose: Check if a MIDAS client exists in current experiment

  Input:
    char   name             Client name
    BOOL   bUnique          If true, look for the exact client name.
                            If false, look for namexxx where xxx is
                            a any number

  Output:
    none

  Function value:
    CM_SUCCESS              Client found
    CM_NO_CLIENT            No client with that name


     e.g.   status=cm_exist("mdarc",TRUE);
     Compare status with CM_NO_CLIENT or CM_SUCCESS

     or call with FALSE and "febnmr"  
     e.g.   status = cm_exist("febnmr", FALSE);
     in this case it will return CM_SUCCESS for febnmr, febnmr2, febnmr1

     To check febnmr2 specifically have to specify:
            status = cm_exist("febnmr2", TRUE);

 */
  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "tr_start","Another copy of mdarc may be already running");
      return(CM_NO_CLIENT); /* return an error */
  }

  if (rn == 0)
    printf ("tr_start:  this is a dummy call for initialization\n");

  /* update the whole mdarc record first to reflect any changes */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_start","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"tr_start");
    return(status);
  }
  nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
  nHistBanks =  fmdarc.histograms.number_defined;  
  if(debug)printf("tr_start: Number of bins = %d;  no. defined histograms = %d\n",nHistBins,nHistBanks);

  /* Get the beamline */
#ifdef MUSR
  /* the beamline (should have been) copied into mdarc/histograms/musr/beamline
     by musr_config  */


  strcpy(beamline,fmdarc.histograms.musr.beamline);
  for (j=0; j<strlen(beamline); j++)
    beamline[j] = toupper (beamline[j]); /* convert to upper case */
  if(debug) printf(" beamline:  %s\n",beamline);
  

#else   //BNMR
  sprintf(beamline,"BNMR");  
#endif
  
  if (strlen(beamline) <= 0 )
  {
    printf("No valid beamline is supplied\n");
    return (DB_INVALID_PARAM);
  }

   // Remember: 
   //     db_get_value formerly creates the key if it doesn't exist (i.e. returns
   //           no error)
  // Midas 9.0 does not create key if last param is FALSE


  
  /* look for automatic run number check */
  if (fmdarc.disable_run_number_check)
  {
    cm_msg(MINFO,"tr_start","* * WARNING:  Automatic run numbering is disabled. This mode    * *");
    cm_msg(MINFO,"tr_start","* *           is NOT RECOMMENDED except for emergencies         * *");
      //  get the run number - this is now a global value in mdarc.h
    size = sizeof(run_number);
    status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"tr_start","key not found  /Runinfo/Run number");
      write_message1(status,"tr_start");
      return (status);
    }
    printf (" Run number = %d\n",run_number);
   
/* check run number against beamline when automatic run number checking is
   disabled  only for a genuine run start */
    //printf("rn=%d, run_state=%d\n",rn,run_state);
  
    if ( rn !=0  ||   run_state == STATE_RUNNING )    /* rn not 0 or  run is going already  */
    {      /* check run number against beamline */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      /* check for test run first */
      printf("Automatic run numbering is DISABLED: checking run number against beamline\n");
      if ( run_number  >= 30000 && run_number <=30099  ) 
      {
        /* test run for any beamline */
        if  (strncmp(run_type,"test",4) != 0)
        {
          cm_msg(MERROR,"tr_start","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
          cm_msg(MINFO,"tr_start","Test runs must be in range 30000-30499");
          cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number. ");
          return (DB_INVALID_PARAM);
        }
        cm_msg(MINFO,"tr_start","Starting a TEST data run (%d)",run_number);
      }
      
      else   /* real run */
      {   
        if(run_number >= 40000 && run_number < 45000)   //BNMR
        {
            if (strncmp(beamline,"BNMR",4) != 0)
            {
              cm_msg(MERROR,"tr_start",
                     "Mismatch between run number (%d) & beamline (%s); expect beamline = BNMR for this run number",run_number, beamline );
              cm_msg(MINFO,"tr_start", "Data logging cannot proceed with this run number.");
              return (DB_INVALID_PARAM);
            }
        }
#ifdef MUSR
        else if(run_number  < 5000 )        // M20
        {
          if (strncmp(beamline,"M20",3) != 0)
          {
            cm_msg(MERROR,"tr_start",
                   "Mismatch between run number (%d) & beamline (%s); expect beamline = M20 for this run number",run_number,beamline);
            cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number ");
            return (DB_INVALID_PARAM);
          }
        }
        else if(  run_number < 10000)      // M15
        {
          if (strncmp(beamline,"M15",3) != 0)
          {
            cm_msg(MERROR,"tr_start",
                   "Mismatch between run number (%d) & beamline (%s); expect beamline = M15 for this run number",run_number,beamline);
            cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number");
            return (DB_INVALID_PARAM);
          }
        }
        else if(  run_number < 15000)   // M13
        {
          cm_msg(MERROR,"tr_start",
                 "Run number (%d) matches obsolete beamline (M13)\n",run_number);
          cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number.");
          return (DB_INVALID_PARAM);
        }
        else if ( run_number  < 20000)
        {
          if (strncmp(beamline,"M9B",3) != 0)  // M9B
          {
            cm_msg(MERROR,"tr_start",
                   "Mismatch between run number (%d) & beamline (%s); expect beamline = M9B for this run number",run_number, beamline);
            cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number.");
            return (DB_INVALID_PARAM);
          }
        }
        else if ( run_number  < 25000 )    // DEV
        {
          if (strncmp(beamline,"DEV",3) != 0)
          {
            cm_msg(MERROR,"tr_start",
                   "Mismatch between run number (%d) & beamline (%s); expect beamline = DEV for this run number",run_number, beamline);
            cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number.");
            return (DB_INVALID_PARAM);
          }
        }
#endif
        else
        {
          cm_msg(MERROR,"tr_start","Run number (%d) does not match a valid beamline\n",run_number); 
          cm_msg(MINFO,"tr_start","Test runs must be in range 30000-30499");
          cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number.");
          return (DB_INVALID_PARAM);
        }
        printf("detected run %d for beamline %s \n",run_number,beamline);
                /*  real run */
        if  (strncmp(run_type,"real",4) != 0)
        {
          cm_msg(MERROR,"tr_start","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
          cm_msg(MINFO,"tr_start","Test runs must be in range 30000-30499");
          cm_msg(MINFO,"tr_start","Data logging cannot proceed with this run number.");
          return (DB_INVALID_PARAM);
        }
        else
          cm_msg(MINFO,"tr_start","Starting a REAL data run (%d), (beamline %s) ",run_number,beamline);  
      }
      
      
    } /* end of run number check on genuine tr_start or run already going */



  } 
  else    /* automatic run number checking is enabled */
  {
  
/*
  perl script to find next valid run number and write it to /Runinfo/run number
  It (should) handle all eventualities  including mdarc restarted midway through a
  run.  It uses run_type to determine what type of run (test/real) the user
  wants, and assigns a run number accordingly.
*/
    unlink(outfile); /* delete any old copy of perl output file in case of failure */
    sprintf(cmd,"%s %s %s %d %s 0, %s",perl_script, perl_path, expt_name,rn,eqp_name,beamline); 
    if(debug) printf("tr_start: Sending system command  cmd: %s\n",cmd);
    status =  system(cmd);
    
    
    if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"tr_start","Perl script get_next_run_number.pl returns failure status (%d)",status);
      unlink(errfile); /* error so delete last errfile */
      status = print_file(outfile);
      if(status)
        cm_msg (MERROR,"tr_start","There may be compilation errors in the perl script"); // no file to print
      else
	{ // make a copy of the error file as the outfile is overwritten each time
	  sprintf(cmd,"cp %s %s",outfile,errfile);
	  status = system(cmd);
	  if(status)cm_msg(MINFO,"tr_start","failure from system command: %s",cmd);
	}
      return (DB_INVALID_PARAM); // bad status from perl script
    }
    else
      if(debug)printf("tr_start : Success from perl script to get next run number\n");
    
    
    //  get the run number which may have been changed by get_next_run_number
    //  this is now a global value in mdarc.h
    size = sizeof(run_number);
      status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
      {
        status=cm_msg(MERROR,"tr_start","key not found  /Runinfo/Run number");
        write_message1(status,"tr_start");
        return (status);
      }
      
      /* and the run type (a global)  */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
      
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      
      
      /* get the number of bins & banks  from the mdarc area (updated by rf_config at
         begin-of-run for both real and test mode */
      
  } // end of automatic run number checking
  
  
  if(nHistBins <= 0)        /*  use the number of bins found from the actual histograms */
    cm_msg(MINFO, "tr_start", "Invalid number of bins (%d) from mdarc/histograms/num_bins", nHistBins);
  if(nHistBanks <= 0)        /*  use the number of banks found from the actual histograms */
    cm_msg(MINFO, "tr_start", "Invalid number of histograms (%d) from mdarc/histograms/number_defined", nHistBanks);
  

  if(rn == 0)
    cm_msg(MINFO,"tr_start","Making a DUMMY call to bnmr_darc for initialization");
  else
    cm_msg(MINFO,"tr_start","In the process of starting run %d",
           run_number);

  write_data = fmdarc.enable_mdarc_logging;
  if(write_data)
  {
    if(debug)printf ("tr_start: logging is enabled\n");

    /* set up hot link on toggle on a genuine run start (or if run is going) 
       hot link on toggle is closed by tr_stop
    */
    if (rn != 0  ||   run_state == STATE_RUNNING )
    {
      if(debug)printf("tr_start: Calling setup_hot_toggle\n");
      status = setup_hot_toggle();
      if(status!=DB_SUCCESS)
	printf("tr_start: Failure from setup_hot_toggle. Can still save data, but toggle won't work\n");
    
#ifdef MUSR
      /* set up hot link on "save now" on a genuine run start (or if run is going) 
	 hot link on is closed by tr_stop
      */
      
      if(debug)printf("tr_start: Calling setup_hot_save\n");
      status = setup_hot_save();
      if(status!=DB_SUCCESS)
	printf("tr_start: Failure from setup_hot_save. Can still save data, but hot key won't work\n");
#endif 
    }
  } /* if write_data */
#ifdef MUSR
  else
    printf ("tr_start: logging is currently disabled\n");

  status = get_client_name(ClientName);  // get the ACTUAL frontend client name (for trigger event)
  if(debug)printf("tr_start: client name is %s\n",ClientName);
#endif    
  return DB_SUCCESS;
}

/*----- stop  ----------------------------------------------*/
INT tr_stop(INT rn, char *error)
{
  
  char str[128], filename[128];/* standard length 128 */
  INT next, nfile;
  BOOL flag;
  HNDLE hktmp;

#ifdef MUSR
  /* remove hot link on "save now" */
  if(hSave) 
    {
      if(debug)
	printf("tr_stop: closing record for \"save now\", hSave=%d\n",hSave);
      db_close_record(hDB, hSave);
    }
#endif

  if(!write_data) return SUCCESS;

  if(debug)printf("tr_stop: In the process of stopping the run %d\n", rn);

 
  if (hBufEvent)
    {
      INT n_bytes, n_try=0;
      do
	{
	  bm_get_buffer_level(hBufEvent, &n_bytes);
	  cm_yield(100);
	} while (n_try++ < 5);
    }
  /* An event is sent by frontend at BOR - no need to trigger (MUSR) */
  if(bGotEvent)
    {
      // no need to set saving_now flag as hotlink is closed
      cm_msg(MINFO,"tr_stop","Calling bnmr_darc to save the final data for this run");
      status = bnmr_darc(pHistData);
    }
  else
    {
      INT rtmp;
      cm_msg(MINFO,"tr_stop","No final data available for this run (no event received from frontend)");
      /* reset the run number back ready for next run */
      rtmp=rn-1;
      printf("Resetting run number to %d\n",rtmp);
      size=sizeof(rtmp);
      status = db_set_value(hDB,0, "/Runinfo/Run number",  &rtmp, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"hot_toggle","key not found  /Runinfo/Run number");
	  write_message1(status,"hot_toggle");
	  return ;
	}
    }
  
  /* 
     if the toggle flag is set, we are in the middle of the toggle procedure,
     and haven't saved a file with the new run number 
     - this will happen if no valid data is available  */
  
  /* check whether toggle flag is still set  */
  if(toggle)
    {
      cm_msg(MINFO,"tr_stop","Warning - toggle is set. There may not be a final saved file\n");
      
      toggle = FALSE;  /* set global toggle false */
      /* turn toggle bit off in odb  */
      size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "tr_stop", "cannot clear toggle flag ");
    
    if(debug) printf ("tr_stop:  Toggle is cleared \n");
    
    
    /* DO NOT restore the hot link on toggle  */
    
    /*    if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	  {
	  size = sizeof(fmdarc.toggle);
	  status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
	  if (status != DB_SUCCESS)
	  {
	  cm_msg(MERROR,"tr_stop","Failed to open record (hotlink) for toggle (%d)", status );
	  write_message1(status,"bnmr_darc");
          return(status);
	  }      
	  }
    */
    }     // if toggle
  else   
    {
      /* remove hot link while the run is off */
      if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	{
	  if(debug)printf("tr_stop: Closing toggle hotlink\n");
	  db_close_record(hDB, hktmp);
	}
    }


  /* kill script also sets this flag */
  if( ! fmdarc.end_of_run_purge_rename)
    {
      if(debug) printf(" End of run purge/rename procedure flag (%d)  is FALSE\n"
		       ,fmdarc.end_of_run_purge_rename);
      cm_msg(MINFO,"tr_stop","Saved file WILL NOT be PURGED and RENAMED at end of run (flag is FALSE)");
      cm_msg(MINFO,"tr_stop","Note - kill button sets this flag while run is being killed");
      /*
	cm_msg(MINFO,"tr_stop","You may run 'cleanup' to execute end-of-run procedure for this run");
	cm_msg(MINFO,"tr_stop","once you have deleted any bad saved files");
      */
    }
  else
    {
      if(debug)
	{
	  printf(" End of run purge/rename procedure flag (%d)  is TRUE\n"
		 ,fmdarc.end_of_run_purge_rename);
	  printf("tr_stop: saved_data_directory: %s\n",fmdarc.saved_data_directory);
	  printf("tr_stop: archived_data_directory: %s\n",fmdarc.archived_data_directory);
	  printf("calling darc_rename with run number %d   \n",rn);
	}
      
      /* purge/rename final run file  & archive */    
      status = darc_rename_file(rn, fmdarc.saved_data_directory
				, fmdarc.archived_data_directory, filename);
      if (status == SUCCESS)
	{      
	  /* update filename in odb 
	     &  make sure we write correct length string to odb */
	  sprintf(fmdarc.last_saved_filename, "%s", filename);
	  sprintf(str,"last_saved_filename");
	  if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			   fmdarc.last_saved_filename, str);
	  size=sizeof(fmdarc.last_saved_filename);
	  status = db_set_value(hDB, hMDarc, str, fmdarc.last_saved_filename, size, 
				1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/settings/%s", eqp_name,str);
	      write_message1(status,"tr_stop");
	    }
	}
    }

  if (debug) 
    printf("tr_stop: releasing camp connection and saving odb\n");
  DARC_release_camp(); /* close the camp connection on end of run */
  odb_save(rn, fmdarc.saved_data_directory);  // save odb
  return SUCCESS;
}

/*----- process_event ----------------------------------------------*/
void  process_event(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[YB_STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  DWORD * pdata;
  WORD  * pWdata;
  INT    bn, ii, jj, i,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    num_HistBanks; /* local counter */
  INT    num_HistBins; /* local counter */
  char   type[10];
  char   str[128];
  
  pmbh = (BANK_HEADER *) pevent;

  if(debug_proc)
    printf("\n process_event: Starting with Ser: %ld, ID: %d, size: %ld and bGotEvent %d\n",
		  pheader->serial_number, pheader->event_id, pheader->data_size,bGotEvent);

  first = TRUE; 
  status = yb_any_event_swap(FORMAT_MIDAS, pheader);
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event: #banks:%i Bank list:-%s-\n", nbk, banklist);

  /*
    process histogram bank
  */
  
/* Assuming each bank name is 4 characters only &   all histogram banks begin with HI or HM.   
     All histogram banks have the same length.
     There may be banks present that are not histograms  */
  
  num_HistBanks=0; /* zero number of histogram banks in this event (local) */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
    {
      /* bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0) )
	{
	  /*if(debug_proc)printf ("Found bankname HI or HM , bk=%s\n",bk);*/
	  num_HistBanks++;
	  /* Use the length of the bank to allocate space */
	  
          if (first)  /* assumes all banks are the same length */
	    {
	      first = FALSE;
	      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
#ifdef MUSR
              if(debug_proc)
		printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
              if (bktyp != TID_WORD && bktyp != TID_DWORD )
		{
		  cm_msg(MERROR,"process_event","Error - Data type %d illegal for Histogram banks\n",bktyp);
		  return;
		}
#endif
	      num_HistBins = bk_locate(pmbh, bk, &pdata); /* bank length */
              if (num_HistBins == 0)
		{
		  cm_msg(MERROR,"process_event","Error - Bank %s length = 0; Bank not found", bk);
		  return;
		}    
	    }
          
	}
      /* else
	 if(debug_proc)printf ("bankname did NOT contain HI or HM , bk=%s\n",bk); */
      
    }


  if(debug_proc)printf("process_event: event contains %d front-end histogram banks\n",num_HistBanks);  

  if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
  {
    /* At BOR  process_event gets called before tr_start has updated the record (MUSR trigger event only) 
       Read the sizes from odb to check
    */
    if(debug_proc)printf("process_event: Front end has sent %d histogram banks. Expected %d\n",
			 num_HistBanks,nHistBanks);
    size = sizeof(nHistBanks);
    status = db_get_value(hDB, hMDarc, "histograms/number defined", &nHistBanks, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"process_event","could not get value for \"../mdarc/histograms/number defined\" (%d)",status);
	return;
      }
    if(debug_proc)printf("process_event: Reread nHistBanks from odb as %d\n",nHistBanks);
    /* now check again */
    if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
      {
	cm_msg(MERROR, "process_event", "Front end has sent %d histogram banks. Expected %d",
	       num_HistBanks,nHistBanks);
	return;
      }
  }
  
  if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
    {
      
      /* At BOR process_event gets called before tr_start has updated the record (MUSR only)
	 Read the sizes from odb to check
      */
      if(debug_proc)printf("process_event: Front end has sent %d histogram bins. Expected %d\n",
			   num_HistBins,nHistBins);
      size = sizeof(nHistBins);
      status = db_get_value(hDB, hMDarc, "histograms/num bins", &nHistBins, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"process_event","could not get value for \"../mdarc/histograms/num bins\" (%d)",status);
	  return;
	}
      if(debug_proc)printf("process_event: Reread nHistBins from odb as %d\n",nHistBins);
      /* now check again */
      
      if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
	{
	  cm_msg(MERROR, "process_event", "Front end has sent %d histogram bins. Expected %d",
		 num_HistBins,nHistBins);
	  return;
	}
    }
  
  /* assume histogram banks are all of length nHistBins. This will be checked later */
  if (pHistData) free(pHistData); pHistData = NULL;
  
  /* allocate full space once only
     Note for MUSR: data is stored as DWORD in pHistData even if the bank is WORD; 
     DARC_write only deals with DWORD - this ought to be changed so it can deal with WORD 
 */
  if(debug_proc) printf("Histogram allocation %d bytes \n",nHistBins * sizeof(DWORD) * nHistBanks);
  pHistData = (caddr_t) malloc(nHistBins * sizeof(DWORD) * nHistBanks ); /* nHistBanks histograms */
  if (pHistData == NULL)
    {
      cm_msg(MERROR,"process_event","Error allocating total space for histograms %d"
	     ,nHistBins * sizeof(DWORD) * nHistBanks);
      return;
    }
  
  /* trusting that each bank has always 4 char */
  hn=0;  /* initialize histogram counter */

  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4) 
    {
      /* get bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0) )
	{
	  /* if(debug_proc)printf ("Got bankname HI or HM , bk=%s\n",bk);*/
	  
	  if(debug_proc)printf("working on histogram bank %s\n",bk);
	  status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
	  bkitem = bk_locate(pmbh, bk, &pdata);
#ifdef MUSR
          if (bktyp == TID_WORD)
            sprintf(type,"TID_WORD");
          else if (bktyp == TID_DWORD)
            sprintf(type,"TID_DWORD");
          else
	    {
	      cm_msg(MERROR,"process_event",
		     "Error - unexpected bank type (%d) for bank %s", bktyp,bk);
	      return;
	    }
	  if (debug_proc) printf(
				 "process_event: found bank %s length %d type %s\n",
                                 bk, bkitem,type);
#endif
	  if (bkitem == 0)
	    {
	      cm_msg(MERROR,"process_event","Error - Bank %s length = 0; Bank not found", bk);
	      return;
	    }
	  
          if (bkitem != nHistBins)
	    {
	      cm_msg(MERROR,"process_event","Unexpected bank length (%d) for bank %s; expected # bins = %d",
		     bkitem, bk, nHistBins);
	      return;
	    }
	  
	  offset = hn * nHistBins; /* offset in words into pHistData */
	  hn++;      /* increment histogram number for next time */
	  if(debug_proc)printf("Offset in words into pHistData for histogram %s is %d\n",bk,offset);
#ifdef MUSR
	  if (bktyp == TID_DWORD)
	    memcpy(pHistData + offset*sizeof(long),  pdata, nHistBins*sizeof(DWORD) );
	  else   // WORD data copied to DWORD array
	    {
	      if(debug_proc) printf("Copying %d words to  (DWORD)pHistData\n",nHistBins);
	      (WORD*)pWdata = (WORD *) pdata;
	      for(i=0; i<nHistBins; i++)
		{
		  ( (long*)(pHistData + (offset*sizeof(long))))[i] = *pWdata;
		  pWdata++;
		}
	      //printf("copy done\n");
	    }
#else
          // BNMR always has DWORD data
	  memcpy(pHistData + offset*sizeof(long),  pdata, nHistBins*sizeof(DWORD) );          
#endif
          
	  if(debug_dump)
	    {
	      /* check parameters dump_begin, dump_length are valid */
	      if(dump_begin < 0) dump_begin = 0;
	      if( (dump_length + dump_begin) > bkitem)
		{
		  dump_length = bkitem + 1  - dump_begin;
		  printf("debug_dump - Invalid dump_length. Setting dump_length to %d\n",dump_length);
		}
	      printf("%s bank: offset in copied array will be: %d\n",bk ,offset); 
	      for (i=dump_begin; i< (dump_length + dump_begin) ;i++)
		{
		  printf("%s bank: Copied pdata[%d] = 0x%x to pHistData[%d]=0x%x\n",
                         bk , i, pdata[i], i+offset, ((long*)pHistData)[i+offset]);
		}
	    }
	}
      /*  else
	  if(debug_proc)printf("Bankname %s did not contain HM or HI\n",bk); */ 
    }
  if(hn != nHistBanks)
    {
      cm_msg(MERROR,"process_event","Unexpected error in # histograms detected. Expected %d, got %d",
	     nHistBanks,hn);
      return;
    }
  
  if(debug_proc)printf("process_event: setting bGotEvent TRUE\n");
  bGotEvent = TRUE;
  
  return;
}

/*---- HOT_LINKS ---------------------------------------------------*/
INT setup_hotlink()
{
  /* does all initialization including setting up hotlinks */
  char str[128];
  HNDLE hktmp;
  char  *s;
  INT i,j, struct_size;
  
#ifdef MUSR
  MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);
#else
  FIFO_ACQ_MDARC_STR(acq_mdarc_str); 
#endif

  if(debug) printf("setup_hotlink starting\n");

  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "setup_hotlink","Another mdarc client has been detected");
      return(CM_NO_CLIENT); /* return an error */
  }


  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","key not found /Runinfo/State (%d)",status);
    return(status);
  }

  /* find the key for mdarc */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("setup_hotlink: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hotlink","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hMDarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_hotlink", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}
#ifdef MUSR
      struct_size =   sizeof(MUSR_TD_ACQ_MDARC);
#else
      struct_size =  sizeof(FIFO_ACQ_MDARC);
#endif
      
      printf("setup_hotlink:Info - size of mdarc saved structure: %d, size of mdarc record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"setup_hotlink","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"setup_hotlink","Could not create mdarc record (%d)\n",status);
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }
  
  /* try again to get the key hMDarc  */
  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_hotlink", "key %s not found (%d)", str,status);
    write_message1(status,"setup_hotlink");
    return (status);
  }

  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"setup_hotlink");
    return(status);
  }
  
  /*
    get the path for the perl script run_number_check.pl
  */
  sprintf(perl_script,"%s",fmdarc.perlscript_path);
  trimBlanks(perl_script,perl_script);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
 
  //printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);
  if ( i == (j-1) )
  {
    //if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
    perl_script[i]='\0';
  }
  strcpy(perl_path,perl_script); /* save in global; needed later (also by bnmr_darc for MUSR) */
  strcat(perl_script,"/get_next_run_number.pl");
  if(debug)printf("setup_hotlink: using perl_script = %s\n",perl_script);


  /* set hot link on num_versions_before_purge  */
  if (db_find_key(hDB, hMDarc, "num_versions_before_purge", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.num_versions_before_purge);
    status = db_open_record(hDB, hktmp, &fmdarc.num_versions_before_purge, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for num_versions_before_purge (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
    else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for num_versions_before_purge   (%d)", status );

  
  /* set hot link on end_of_run_purge_rename  */
  if (db_find_key(hDB, hMDarc, "end_of_run_purge_rename", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.end_of_run_purge_rename);
    status = db_open_record(hDB, hktmp, &fmdarc.end_of_run_purge_rename, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for end_of_run_purge_rename (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for end_of_run_purge_rename (%d)", status );
  
    
  /* set hot link on save_interval  */
  if (db_find_key(hDB, hMDarc, "save_interval(sec)", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.save_interval_sec_);
    status = db_open_record(hDB, hktmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for save_interval (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for save_interval (%d)", status );

  /* saving previous save_Time for limit check later in update_Time */
  time_save = fmdarc.save_interval_sec_;
  if((INT)time_save < 1 || (INT)time_save > 24*3600 )                 /* time_save > 1sec, < 1 day */
  {
    fmdarc.save_interval_sec_ = time_save = 60;
    size=sizeof(fmdarc.save_interval_sec_);
    status = db_set_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
  }

  saving_now = FALSE; // initialize - only used for MUSR

#ifdef MUSR
  if (db_find_key(hDB, hMDarc, "histograms/musr/save now", &hSave) != DB_SUCCESS)
    {
      hSave=0;
      cm_msg(MERROR,"setup_hotlink","Failed to find key for \"save now\" (%d)", status );
      cm_msg(MINFO,"setup_hotlink","Hot link to save data is not available");
    }
  else
    if(debug)printf("setup_hotlink: got key for hSave=%d\n",hSave);
  /* hot link is actually opened at begin of run */
#endif


  if(debug)
  {
    printf("setup_hotlink: Updated parameters from odb\n");
    printf("    time_save = %d \n",time_save);
    printf("    arch_dir = %s\n", fmdarc.archived_data_directory);
    printf("    save_dir = %s\n", fmdarc.saved_data_directory);
    if(fmdarc.end_of_run_purge_rename)
      printf("  eor flag is set : %d\n", fmdarc.end_of_run_purge_rename);
    else
      printf("  eor flag is NOT set : %d\n", fmdarc.end_of_run_purge_rename);
  }

  return(status);
}
/*------------------------------------------------------------------*/
INT setup_hot_toggle()
{
  /* called by tr_start only on genuine run start, so toggle cannot be set before 
     a valid run number is established */
  char str[128];
  HNDLE hktmp;
  
  
  if(debug) printf("setup_hot_toggle starting\n");

  
/* set up hot link on toggle flag */
  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
  {
    /* make sure toggle bit is switched off to start with */
    toggle = FALSE ;  // global
    size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "setup_hot_toggle", "cannot initialize toggle flag ");
    
    status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hot_toggle","Failed to open record (hotlink) for toggle (%d)", status );
      write_message1(status,"setup_hot_toggle");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hot_toggle","Failed to find key for toggle (%d)", status );
  
  return(status);
}

#ifdef MUSR
/*------------------------------------------------------------------*/

INT setup_hot_save()

/* ----------------------------------------------------------------------------------------*/
{
  /* called after by tr_start on genuine run start
     to  set up hot link on "save now" flag */

  char str[128];
  BOOL off = FALSE;
  
  
  if(debug)printf("setup_hot_save starting with hSave=%d\n",hSave);
  
  /* global key hSave has been found in setup_hotlink */

  if (hSave != 0)
    {

      /* make sure global flag "save now"  is set off to start with */
      status = db_set_value(hDB, hMDarc, "histograms/musr/save now", &off, sizeof(BOOL), 1, TID_BOOL);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "setup_hot_save", "cannot initialize \"save now\" flag ");
    
      status = db_open_record(hDB, hSave, &fmdarc.histograms.musr.save_now, size, MODE_READ, hot_save , NULL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hot_save","Failed to open record (hotlink) for \"save now\" (%d)", status );
	  write_message1(status,"setup_hot_save");
	  return(status);
	}
    }
  else
    cm_msg(MERROR,"setup_hot_save","No key for \"save now\" (%d)", status );
  
  return(status);
}

/* ----------------------------------------------------------------------------------------*/

void hot_save (HNDLE hDB, HNDLE hSave ,void *info)

/* ----------------------------------------------------------------------------------------*/

{
/*  Save now is done by setting the flag ..mdarc/histograms/musr/save now
      later could add a user button for this   
 */ 
  
  // uses BOOL save_now - a global 
  char str[128];
  char cmd[256];
  BOOL off=FALSE;
  INT i,state_running;
  
  printf ("hot_save: starting .....\"save now\" has been touched\n");

  /* "save now"  has been touched - user wants to save the histograms now */
  if(!write_data)
    {
      cm_msg(MINFO,"hot_save","Hot save CANNOT SAVE DATA since data logging is disabled in odb");
      return;
    }


  if (saving_now)
    {
      cm_msg(MINFO,"hot_save","Bnmr_darc is already in the process of saving the data");
      return;
    }
  else
    {
      saving_now = TRUE;   // set a flag to say data is being saved
      last_time = ss_time(); // reset the time
      printf("hot_save: Calling trigger_histo_event as saving_now is %d\n",saving_now);
      if( trigger_histo_event(&state_running) ) // an event has been triggered (write_data MUST also be true)
	{
	  INT msg;
	  printf("hot_save: event has been triggered\n");
	  msg = cm_yield(1000); /* call yield */
	  ss_sleep(2000); // wait 2 seconds
	  
	  /* has process_event run ? */	  
	  i=0;
	  while(!bGotEvent)
	    {
	      i++;
	      printf("bGotEvent is false; waiting for process_event to run (%d).",i);
	      msg = cm_yield(1000); /* call yield */
	      ss_sleep(500); // sleep
	      if(i>10)
		{
		  printf("... given up waiting\n");
		  break;
		}
	    }
	  
	  if(!bGotEvent)
	    {
	    printf("bGotEvent is still false; no data available\n");
	    cm_msg(MINFO,"hot_save","No data is available to be saved");
	    }	  
	  else
	    {
	      printf("\n");
	      cm_msg(MINFO,"hot_save","Hot save is calling bnmr_darc to save the data");
	      if(debug)printf("hot_save: Calling bnmr_darc to save the data\n");
	  
	      /* save the event */ 
	      status = bnmr_darc(pHistData);
	    }
	} // if trigger_histo_event
      else
	printf("hot_save: could not trigger an event\n");
      saving_now = FALSE;
    }
  return;
}  
  
#endif   //  Save now procedure is defined for MUSR only

/*------------------------------------------------------------------*/
void update_Time(HNDLE hDB, HNDLE htmp, void *info)
{

  /* remove hot link */
  if(debug)printf("update_Time starting...present time_save=%d\n",time_save);
  
  db_close_record(hDB, htmp);
  
  /* get the new value */

  size = sizeof(fmdarc.save_interval_sec_);
  status = db_get_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, &size, TID_DWORD, FALSE);

  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"update_Time","could not get new value of save interval ");
      write_message1(status,"update_Time");
      return ;
    }
  if(debug)printf("update_Time: Time_save is to be updated to %d\n",fmdarc.save_interval_sec_);

  if((INT)fmdarc.save_interval_sec_ < 1
     || (INT)fmdarc.save_interval_sec_ > 24*3600 ) /* time_save > 1sec, < 1 day */
    {
      cm_msg(MINFO,"update_Time","Invalid time save interval (%d). Using previous value (%d sec)",
	     fmdarc.save_interval_sec_,time_save);
      fmdarc.save_interval_sec_ = time_save;
      
      /* update the odb with this value */
      if(debug)printf("update_Time: Restoring /Equipment/%s/mdarc/save_interval(sec) to %d\n"
		      , eqp_name, fmdarc.save_interval_sec_);
      size=sizeof(fmdarc.save_interval_sec_);
      status = db_set_value(hDB, hMDarc, "save_interval(sec)"
			    , &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"update_Time","cannot write to odb key /Equipment/%s/mdarc/save_interval(sec) %d"
		 , eqp_name, status );
	  write_message1(status,"update_Time");
	}   
    }  // end of bad value

  time_save = fmdarc.save_interval_sec_;
  if (debug)printf("update_Time: Time_save is now updated to %d\n",fmdarc.save_interval_sec_);


  /* restore Hot link */
  size = sizeof(fmdarc.save_interval_sec_);
  status = db_open_record(hDB, htmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
  if (status != DB_SUCCESS)
  {
      cm_msg(MERROR,"update_Time","Failed to open record (hotlink) for mdarc (%d)", status );
      write_message1(status,"setup_hotlink");
      return;
  }

   return;
}

/*------------------------------------------------------------------*/

void hot_toggle (HNDLE hDB, HNDLE hktmp ,void *info)
{
/* toggle is done by user button TOGGLE - calls a perlscript toggle.pl which
   sets odb toggle flag on certain conditions - including that automatic run
   numbering in enabled */ 
  
  // uses BOOL toggle - a global in mdarc.h 
  char str[128];
  char cmd[256];
  char old_type[10];
  
  
  /* toggle bit has been touched - user wants to toggle between real and test
     runs  */
  printf ("hot_toggle: starting ..... toggle has been touched.  \n");
  if(debug)
    printf ("hot_toggle: global toggle flag = %d, run number = %d,  closing record\n",toggle,run_number);
  /* remove hot link while we toggle the run */
  db_close_record(hDB, hktmp);
  
  if (toggle) /* shouldn't happen if hot link is closed */
    {
      printf("hot_toggle: unexpected error - toggle global flag is already set\n");
      return;
    }
  
  
  toggle = TRUE;   /* global flag */
  
  old_run_number=run_number; /* save run number */
  strcpy(old_type,run_type);
  
  if (debug)
    printf("hot_toggle: old run number = %d, and  run type = %s\n",
           old_run_number, old_type);
  
  /* determine the run type */
  if(debug) printf ("hot_toggle: run_type = %s \n",run_type);
  if  (strncmp(run_type,"real",4) == 0)
    {
      if(debug) printf ("hot_toggle: detected present run type as real \n");
      strcpy(run_type,"test"); /* run is real so we now want a test run */
      //if (debug) printf ("now should be test;  run type=%s \n",run_type);
    }
  else if  (strncmp(run_type,"test",4) == 0)
    {
      if (debug) printf ("hot_toggle: detected present run type as test \n");
      strcpy(run_type,"real"); /* run is test so we now want a real run */
      //if (debug) printf ("now should be real;  run type=%s \n",run_type);
    }
  else
    {
      cm_msg(MERROR,"hot_toggle","Unknown run type detected (%s). Cannot toggle run \n",run_type);
      return; 
    }
  if(debug) printf ("Writing new run_type = %s to odb\n",run_type);
  sprintf(str, "/Equipment/%s/mdarc/run type",eqp_name);
  size = sizeof(run_type);
  status = db_set_value(hDB, 0, str, run_type, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
	     str,status);
      write_message1(status,"hot_toggle");
      return ;
    }    
  
  /*
    Get a new run number with perl script
  */
  if(debug) printf ("hot_toggle: getting a run number for a %s run...\n",run_type);
  unlink(outfile); /* delete any old copy of perl output file in case of failure */

  /* add parameter perl_path */
  sprintf(cmd,"%s %s %s %d %s 0, %s",perl_script,perl_path,expt_name,run_number,eqp_name,beamline);
  if(debug) printf("Hot_toggle: sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  if (status)
    /* cannot get a new run number */
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR," hot_toggle",
	      "A status of %d has been returned from perl script get_next_run_number.pl",status);
      status=print_file(outfile);   
      if(status)
	cm_msg (MERROR,"hot_toggle","There may be compilation errors in the perl script");
      
      if(debug) printf(" hot_toggle: Resetting run type to previous value (%s)",old_type);
      
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}
      toggle = FALSE;   /* clear global flag */
      cm_msg (MERROR," hot_toggle","Cannot toggle run. Continuing with run %d",run_number);
      return;
    }
  
  //  get the new run number - this is now a global value in mdarc.h
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"hot_toggle","key not found  /Runinfo/Run number");
      write_message1(status,"hot_toggle");
      return ;
    }
  if (run_number == old_run_number)
    {
      status=cm_msg(MERROR,"hot_toggle","??? Strange error: Run number not toggled after perlscript ");
      /* rewrite old run type */
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}    
      return;
      
    }
  else
    status=cm_msg(MINFO,"hot_toggle",
                  "* * * *  hot_toggle:  Run number has been toggled from %d to %d ( %s to %s) * * * * ",
                  old_run_number,run_number,old_type, run_type);
  
  return;
}

INT print_file(char* filename)
{
  FILE *FIN;
  char str[256];

  printf ("\n------------------------------------------------------------------------\n");
  printf ("   print_file: Contents of information file %s:\n",filename);
  printf ("--------------------------------------------------------------------------\n");
  FIN = fopen (filename,"r");
  if (FIN == NULL)
  {
      printf ("print_file : Error opening file %s\n",filename);
      return(1);     
  }
  else
  {
    while(fgets(str,256,FIN) != NULL)
      printf("%s",str);
  }
  fclose (FIN);
  printf ("--------------------------------------------------------------------------\n");
  printf ("   print_file: End of information file %s: \n",filename);
  printf ("--------------------------------------------------------------------------\n\n");
  return(0);
}


/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT     size;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg, i;
  BOOL   daemon=FALSE;
  int j=0;  
  char   dbg[25];
  int    bug;
#ifdef MUSR
  INT run_state;
  BOOL event_triggered=FALSE;
#endif
  
#ifdef MUSR    
  printf("\n MUSR mdarc (version  %.2f) : Data Logger saves MIDAS data in MUD format \n\n",version); 
#else
  printf("\n BNMR mdarc (version  %.2f) : Data Logger saves MIDAS data in MUD format (Bnmr Type 2) \n\n",version); 
#endif  
  /* set defaults for globals */
  debug = debug_proc = debug_mud = debug_check = debug_dump = FALSE;
  saving_now = FALSE;
  dump_begin = 0;
  dump_length = 5;
  run_number = -1; /* initialize to a silly value for go_toggle */ 
  toggle = FALSE;  /* initial value */
  
  /* end defaults */
  
/*  Midas 9.1  cm_get_environment (host_name, expt_name); */
 cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);  
  /* initialize global variables */
#ifdef MUSR
  sprintf(eqp_name,"MUSR_TD_acq");
  sprintf(feclient,"fev680");
#else
  sprintf(eqp_name,"FIFO_acq");
  sprintf(feclient,"febnmr2");
#endif
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    /* Run as a daemon */    
    if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    
    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
        goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i],"-q",2)==0)
        strcpy(eqp_name, argv[++i]);
      else if (strncmp(argv[i],"-f",2)==0)
        strcpy(feclient, argv[++i]);
/*      else if (strncmp(argv[i],"-s",2)==0)   put time as odb param
        time_save = atoi(argv[++i]);
*/

      else if (strncmp(argv[i],"-d",2)==0)
      {
        strcpy(dbg, argv[++i]);  // dbg = debug level 
        debug=TRUE;
      }
      else if (strncmp(argv[i],"-b",2)==0)  /* in mdarc.h (these are debug parameters) */
        dump_begin =  atoi(argv[++i]);
      else if (strncmp(argv[i],"-l",2)==0)
        dump_length =  atoi(argv[++i]);
    }
    else
    {
   usage:
      printf("usage: mdarc  [-h Hostname] [-e Experiment]\n");
//      printf("              [-s time interval (sec)] to save data
//                                (def:60s)\n");
      printf("              [-f FE client name] (default:%s)\n",feclient);
      printf("              [-q FE equipment name] (default:%s)\n",eqp_name);
      printf("              [-D ] (become a daemon)\n");
      printf("              [-d debug level] (0=debug 1=mud/camp 2=save files 3=dump 4=process event 5=all\n");
      printf("              [-b begin] [-l length] (histo dump params for debug level=3. Default b=0 l=5. \n");
      printf(" e.g.  mdarc -h dasdevpc -e bnmr  \n");
      return 0;
    }
  }
 
  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
    }


/* convert expt_name to lower case */
  for (j=0; j<strlen(expt_name); j++) expt_name[j]=tolower (expt_name[j]); 
/* make sure we have the right version of mdarc for this experiment */
#ifdef MUSR
  if (  !strncmp(expt_name,"musr",4)  ||  !strncmp(expt_name,"kin",3) )
#else
  if (  !strncmp(expt_name,"bnmr",4)  ||  !strncmp(expt_name,"suz",3) )
#endif
  {
    if(debug)printf("This version of mdarc DOES  support experiment %s\n",expt_name);
  }
  else
  {
#ifdef MUSR
    printf("This version of mdarc for MUSR does  NOT support experiment %s\n",expt_name);
#else
    printf("This version of mdarc for BNMR does  NOT support experiment %s\n",expt_name);
#endif
    goto error;
  }

    
  if (debug)   
  {
    bug = atoi(dbg);
    switch (bug)
    {
    case 0:      /* level 0  Just debug */
      break;   
    case 1:      /* level 1  Just debug_mud  */
      debug_mud = TRUE;
      debug = FALSE;
      break;   
    case 2:      /* level 2 Just debug_check */
      debug_check = TRUE;
      debug = FALSE;
      break;
    case 3:
      debug_dump = TRUE;
      break;
    case 4:
      debug_proc = TRUE;
      break;
    case 5:
      debug_dump=TRUE;
      debug_mud=TRUE;
      debug_check=TRUE;
      debug_proc=TRUE;
      break;
    default: 
    }
  }
  if(debug_dump) printf("main: data dump will begin at bin = %d, length = %d\n",dump_begin,dump_length);

  /* connect to experiment */
  printf("calling cm_connect_experiment with host_name \"%s\", expt_name \"%s\"\n",host_name,expt_name);
  status = cm_connect_experiment(host_name, expt_name, "Mdarc", 0);
  if (status != CM_SUCCESS)
    goto error;

  /* turn off watchdog if in debug mode */
  if (debug)
    cm_set_watchdog_params(TRUE, 0);
  
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);
  
  /* open the "system" buffer, 1M size */
  bm_open_buffer("SYSTEM", EVENT_BUFFER_SIZE, &hBufEvent);

  /* set the buffer cache size */
  bm_set_cache_size(hBufEvent, 100000, 0);
  
  /* place a request for a specific event id */
#ifdef MUSR     // MUSR uses fragmented events so needs GET_ALL
  bm_request_event(hBufEvent, 2, TRIGGER_ALL, GET_ALL, &request_id, process_event);
#else
  bm_request_event(hBufEvent, 2 , TRIGGER_ALL, GET_SOME, &request_id, process_event);
#endif
  /* Register to the Transition 
     RP - Nov 2000, use TR_POSTSTART instead of TR_START*/
  if (cm_register_transition(TR_POSTSTART, tr_start) != CM_SUCCESS ||
      cm_register_transition(TR_POSTSTOP, tr_stop) != CM_SUCCESS)
  {
       
    printf("Failed to register\n");
    goto error;
  }

  /* connect to ODB */
  cm_get_experiment_database(&hDB, &hKey);

  /* Create and setup <eqp_name>/settings */
  if(debug)printf ("calling setup_hotlink\n");  
  status = setup_hotlink(); /* initialization including set up keys hMDarc,hSave and open hot links */
  if(status!=DB_SUCCESS)
  {
    cm_disconnect_experiment(); /* abort the program. Cannot save the data */
    return 0;
  }


  /* Init sequence through tr_start once */
  {
    char str[256];

    if(tr_start(0, str) !=DB_SUCCESS)
    {
      printf("Failure from tr_start\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
  }

  if(debug)
  {
    printf("\n");
    printf("mdarc parameters : \n");
#ifdef MUSR
    printf("Camp hostname: %s\n",fmdarc.camp.camp_hostname);
#else
   printf("Camp hostname: %s\n",fmdarc.camp_hostname);
#endif
    printf("Time of last save: %s\n",fmdarc.time_of_last_save);
    printf("Last saved filename: %s\n",fmdarc.last_saved_filename);
    printf("Saved data directory: %s\n",fmdarc.saved_data_directory);
    printf("# versions before purge: %d\n",fmdarc.num_versions_before_purge);
    printf("End of run purge/rename flag: %d\n",fmdarc.end_of_run_purge_rename);
    printf("Archived data directory: %s\n",fmdarc.archived_data_directory);
    printf("Save interval (sec): %d\n",fmdarc.save_interval_sec_);
  }

  
  /* initialize ss_getchar() */
  ss_getchar(0);
  
  last_time = 0;
#ifdef MUSR
  event_triggered=FALSE; /* no event has been triggered */
#endif 
 do
    {
      /* call yield once every second */
      msg = cm_yield(1000);
      
      /* time to save data */
      if (ss_time() - last_time > time_save )
	{
	  last_time = ss_time();
#ifdef MUSR
	  if(saving_now)
	    printf("Mdarc: flag is set to say data is ALREADY being saved (by hotlink \"save now\"\n");
	  else
	    {
	      if(write_data)  // don't request an event if mdarc is not writing it 
		{
		  saving_now = TRUE; // set a flag to tell hot_save that a save is in progress
		  if(debug)printf("mdarc: Calling trigger_histo_event; \n");
		  event_triggered = trigger_histo_event(&run_state);
		  if(debug)
		    printf("trigger_histo_event returns %d with run_state=%d\n",event_triggered,run_state);
		  if(event_triggered)  // an event has been triggered (write_data MUST also be true)
		    {
		      printf("event has been triggered\n");
		      event_triggered = FALSE; // reset flag
		      
		      /* has process_event run yet? (test on bGotEvent) */
		      i=0;
		      while(!bGotEvent)
			{
			  printf("mdarc: bGotEvent is false; waiting for process_event to run (%d)\n",i);
			  msg = cm_yield(1000); /* call yield */
			  ss_sleep(500); // sleep
			  i++;
			  if(i>5)
			    {
			      printf("...given up waiting for bGotEvent\n");
			      break; // event is not coming for some reason
			    }
			}
		      
		      if(!bGotEvent) 
			{
			  printf("mdarc:bGotEvent is still false after %d loops; cannot save data\n",i);
			  cm_msg(MINFO,"mdarc","No data is available at this time to save");
			}
		      else
			{
			  printf("Mdarc: Calling bnmr_darc to save the data\n");
			  status = bnmr_darc(pHistData);
			}
		    } // event_triggered
		  else
		    {  // event was not triggered
		      if (run_state == STATE_RUNNING)  // only send message if we are running
			cm_msg(MINFO,"Mdarc",
			       "Event has not been triggered from frontend  ... no data can be saved \n");
		    }
		} // end of if (write_data)
	      else // write_data is false
		if(debug)printf(" Mdarc: data logging is disabled in odb ... not calling bnmr_darc \n");

	      saving_now = FALSE;
	    } // end of saving_now
	
#else     // BNMR only
	  if(write_data)
	    {
	      if(debug)printf("     Mdarc: Calling bnmr_darc to save the data\n");
	      status = bnmr_darc(pHistData);
	    }
	  else
	    if(debug)printf(" Mdarc: data logging is disabled in odb ... not calling bnmr_darc \n");
#endif
	  fflush(stdout); /* output may be sent to a file */	  
	} // end of if(ss_time() ...  )

  
      /* check keyboard */
      ch = 0;
      if (ss_kbhit())
	{
	  ch = ss_getchar(0);
	  if (ch == -1)
	    ch = getchar();
	  if ((char) ch == '!')
	    break;
	}
    }
  while (msg != RPC_SHUTDOWN && msg != SS_ABORT);
  
 error:
  /* reset terminal */
  ss_getchar(TRUE);
  
  DARC_release_camp();
  cm_disconnect_experiment();
}



#ifdef MUSR
/*-----------------------------------------------------------------------------------------------*/
INT get_client_name(char *pname)
/*-----------------------------------------------------------------------------------------------*/
{
  char name[256],str[256];
  INT i,len;
  HNDLE  hKeyTmp,hSubkey;
  INT size;
  
  /* get the ACTUAL client name for the frontend */
  sprintf(name,"");
  len=strlen(feclient);
  if (cm_exist(feclient, FALSE) != CM_SUCCESS)
  {
    cm_msg(MERROR,"get_client_name","Frontend \"%s\" is not running! (%d)", feclient,status);
  }
  else
  {
    if(debug)printf("get_client_name: Client %s is running\n",feclient);
    status = db_find_key(hDB, 0, "System/Clients", &hKeyTmp);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "get_client_name", "cannot find System/Clients entry in database");
    else
    {
      /* search database for clients with transition mask set */
      for (i=0,status=0 ; ; i++)
      {
	status = db_enum_key(hDB, hKeyTmp, i, &hSubkey);
	if (status == DB_NO_MORE_SUBKEYS)
	  break;
	
	if (status == DB_SUCCESS)
	{
	  size = sizeof(name);
	  db_get_value(hDB, hSubkey, "Name", name, &size, TID_STRING, TRUE);
	  if(strncmp(feclient,name,len)==0)
	  {
	    if(debug)printf("get_client_name: found client name %s\n",name);
	    break;
	  }        
	}
      }
    }
  }
  
  strcpy(pname,name);
  if(debug)printf("get_client_name: returning client name as %s\n",pname);
 
  return;
}

/*-----------------------------------------------------------------------------------------------*/
BOOL trigger_histo_event(INT * pRunstate)
/*-----------------------------------------------------------------------------------------------*/
{
  HNDLE hconn;
  INT run_state;
BOOL event_triggered;
  
  event_triggered = FALSE;
  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"trigger_histo_event","key not found /Runinfo/State (%d)",status);
      return(FALSE); //Error
    }
  *pRunstate = run_state;
  if (run_state == STATE_RUNNING)
    {
      // Use the clientname found by get_client_name called from tr_start
      if(cm_exist(ClientName,TRUE))
	{
	  if(debug)printf("trigger_histo_event: trying to connect to the client %s\n",ClientName);
	  status = cm_connect_client (ClientName, &hconn);
	  if(status != RPC_SUCCESS)
	    cm_msg(MERROR,"trigger_histo_event","Cannot connect to frontend \"%s\" (%d)",ClientName,status);
	  else	    {  // successfully connected to frontend client
	    rpc_client_call(hconn, RPC_MANUAL_TRIG, 2); // trigger an event
	    if (status != CM_SUCCESS)
	      cm_msg(MERROR,"trigger_histo_event","Error triggering event from frontend (%d)",status);
	    else
	      {  // successfully triggered event
		if(debug)printf("trigger_histo_event: success from rpc_client_call to trigger event\n");
		event_triggered=TRUE;
		status =cm_disconnect_client(hconn, FALSE);
		if (status != CM_SUCCESS)
		  cm_msg(MERROR,"trigger_histo_event","Error disconnecting client after event trigger(%d)",status);
	      }
	  }
	} // end of cm_exist
      else
	cm_msg(MERROR,"trigger_histo_event","Frontend client %s not running",ClientName);
    } // end of if running
  return(event_triggered);
}
#endif
