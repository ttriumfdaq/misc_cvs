#ifndef _NEWDEF_H
#define _NEWDEF_H

/* These defines were originally used for localization.
   To avoid massive modification of the code, they are inserted here. */
#define _(String) String
#define N_(String) String

#endif
