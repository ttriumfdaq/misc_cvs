#ifndef _ERRLOG_H
#define _ERRLOG_H

/*
 *  20140312  Ted Whidden  dummy to allow compilation
 *                         original package did not include errlog.h
 *                         (which was at /home/ofs/ofs/include/errlog.h for the owners build)
 *                         - I can't find any OFS project
 */

#define errlog fprintf
#define IM_LOG stderr

#endif
