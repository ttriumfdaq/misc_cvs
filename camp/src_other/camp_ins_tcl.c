
#include <stdio.h>
#include "camp_srv.h"


int
ins_tcl_init( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->initProc == NULL ) || ( pIns->initProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
			 pIns->initProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "init failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_delete( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->deleteProc == NULL ) || ( pIns->deleteProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
			 pIns->deleteProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "delete failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_online( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->onlineProc == NULL ) || ( pIns->onlineProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
			 pIns->onlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "online failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_offline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    if( ( pIns->offlineProc == NULL ) || ( pIns->offlineProc[0] == '\0' ) )
      {
        return( CAMP_SUCCESS );
      }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
			 pIns->offlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
      {
	camp_appendMsg( "offline failure: %s", pIns->interp->result );
	return( CAMP_FAILURE );
      }

    return( CAMP_SUCCESS );
}


int
ins_tcl_set( CAMP_VAR* pInsVar, CAMP_VAR* pVar, CAMP_SPEC* pSpec )
{
    int status;
    CAMP_VAR setVar;
    char val[LEN_STRING+1];
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.writeProc == NULL ) || 
        ( pVar->core.writeProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
      {
      case CAMP_VAR_TYPE_NUMERIC:
	bcopy( pVar, &setVar, sizeof( CAMP_VAR ) );
	bcopy( pSpec, &setVar.spec, sizeof( CAMP_SPEC ) );
	varNumGetValStr( &setVar, val );
	break;
      case CAMP_VAR_TYPE_STRING:
	strcpy( val, pSpec->CAMP_SPEC_u.pStr->val );
	break;
      case CAMP_VAR_TYPE_SELECTION:
	if( pSpec->CAMP_SPEC_u.pSel->pItems != NULL )
	  {
	    if( pSpec->CAMP_SPEC_u.pSel->pItems->label == NULL )
	      {
		camp_appendMsg( "set failure: bad selection label" );
		return( CAMP_FAILURE );
	      }
	    strcpy( val, pSpec->CAMP_SPEC_u.pSel->pItems->label );
	  }
	else
	  {
	    sprintf( val, "%d", pSpec->CAMP_SPEC_u.pSel->val );
	  }
	break;
      }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
                          pVar->core.writeProc, 
			  " {",
                          pInsVar->core.ident, 
			  "} {",
                          val, 
                          "}",
                          NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
    {
        camp_appendMsg( "set failure: %s", pIns->interp->result );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
ins_tcl_read( CAMP_VAR* pInsVar, CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.readProc == NULL ) || 
        ( pVar->core.readProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    status = Tcl_VarEval( pIns->interp, 
                          pVar->core.readProc, 
                          " {",
                          pInsVar->core.ident, 
                          "}",
                          NULL );
    set_thread_interp( oldInterp );
    if( status == TCL_ERROR )
    {
        camp_appendMsg( "read failure: %s", pIns->interp->result );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


