
#include <math.h>
#include "camp_srv.h"


int 
REQ_add( REQ* pReq )
{
    REQ** ppReq;

    /*
     *  Find address of last pointer
     */
    for( ppReq = &pSys->pReqs; *ppReq != NULL; ppReq = &(*ppReq)->pNext ) ;

    *ppReq = pReq;
    (*ppReq)->pNext = NULL;

    return( CAMP_SUCCESS );
}


int
REQ_remove( REQ* pReq )
{
    REQ** ppReq;

    /*
     *  This bit isn't actually used
     */
    pReq->pVar->core.status &= ~CAMP_VAR_PEND_SET;

    for( ppReq = &pSys->pReqs; *ppReq != NULL; ppReq = &(*ppReq)->pNext ) 
    {
        if( *ppReq == pReq )
        {
            *ppReq = (*ppReq)->pNext;

            /*
             *  NOTE:  Just free pReq, don't xdr_free, since
             *         don't want to free the stuff below.
             */
            free( pReq );
            return( CAMP_SUCCESS );
        }
    }

    return( CAMP_FAILURE );
}


/*
 *  Cancel a pending request.
 *  Don't delete right away because it's possible
 *  that another thread (or asynch I/O if implemented)
 *  is in the middle of this request.
 */
void
REQ_cancel( CAMP_VAR* pVar, IO_TYPE type )
{
    REQ* pReq;

    for( pReq = pSys->pReqs; pReq != NULL; pReq = pReq->pNext )
    {
        if( ( pReq->pVar == pVar ) && ( pReq->spec.type == type ) )
        {
            pReq->cancel = TRUE;
        }
    }
}


/*
 *  Cancel all pending requests for a given instrument.
 *  Don't delete right away because it's possible
 *  that another thread (or asynch I/O if implemented)
 *  is in the middle of this request.
 */
void
REQ_cancelAllIns( CAMP_VAR* pVar )
{
    REQ* pReq;
    CAMP_VAR* pInsVar;

    for( pReq = pSys->pReqs; pReq != NULL; pReq = pReq->pNext )
    {
        if( !pReq->cancel )
        {
            pInsVar = varGetpIns( pReq->pVar );
            if( pVar == pInsVar ) pReq->cancel = TRUE;
        }
    }
}


void
REQ_addPoll( CAMP_VAR* pVar )
{
    REQ* pReq;
    timeval_t tv;
    double integ, rem;

    pReq = (REQ*)zalloc( sizeof( REQ ) );

    gettimeval( &tv );
    rem = modf( pVar->core.pollInterval, &integ );
    pReq->time.tv_sec = tv.tv_sec + (long)integ;
    pReq->time.tv_usec = tv.tv_usec + (long)(1e6*rem);
    pReq->pVar = pVar;
    pReq->procs.retProc = REQ_doPoll;
    pReq->spec.type = IO_POLL;

    pReq->pVar->core.status |= CAMP_VAR_PEND_SET;
    REQ_add( pReq );
}


int
REQ_doPoll( REQ* pReq )
{
    int status;
    timeval_t tv;
    double integ, rem;
    CAMP_VAR* pInsVar;
    CAMP_IF* pIF;

    if( pReq->cancel )
    {
        pReq->pending--;
        return( REQ_remove( pReq ) );
    }

    pInsVar = varGetpIns( pReq->pVar );
    if( pInsVar == NULL )
    {
        pReq->pending--;
        camp_appendMsg( msg_inval_parent, pReq->pVar->core.ident );
        return( CAMP_INVAL_VAR );
    }

    pIF = pInsVar->spec.CAMP_SPEC_u.pIns->pIF;
    if( pIF == NULL )
    {
        pReq->pending--;
        return( CAMP_INVAL_IF );
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        pReq->pending--;
        return( CAMP_NOT_CONN );
    }

    status = varRead( pReq->pVar );

    /*
     *  Next poll will occur in pollInterval seconds
     *  after the current time.
     *  This means that polling isn't really 
     *  happening every pollInterval seconds, since 
     *  the time for the varRead is significant, but
     *  reduces the chance of backlogs.
     */
    gettimeval( &tv );
    rem = modf( pReq->pVar->core.pollInterval, &integ );
    pReq->time.tv_sec = tv.tv_sec + (long)integ;
    pReq->time.tv_usec = tv.tv_usec + (long)(1e6*rem);

    pReq->pending--;

    return( status );
}


/*
 *  Note: only one type of request - polling requests
 */
void
REQ_doPending( void )
{
    int status;
    REQ* pReq;
    timeval_t tv;
    CAMP_VAR* pInsVar;
    CAMP_IF* pIF;

    /*
     *   Check requests and start threads inside global lock.
     *   Could consider unlocking (and yielding?) if this
     *   loop is found to take a long time.
     */
#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    for( pReq = pSys->pReqs; pReq != NULL; pReq = pReq->pNext )
    {
        /*
         *  Allow only one thread at a time per request
         */
        if( pReq->pending > 0 )
        {
            continue;
        }

        /* 
         *  Remove if cancel flag is set
         *  Careful not to remove if there is one 
         *  executing already (pending), hence the
         *  check above.
         */ 
        if( pReq->cancel )
        {
            REQ_remove( pReq );
            continue;
        }

        /*
         *  Check that the interface is online
         *  Don't go any further if it isn't
         *  (in THREADED mode cpu was being eaten)
         */
        pInsVar = varGetpIns( pReq->pVar );
        if( pInsVar == NULL ) continue;
        pIF = pInsVar->spec.CAMP_SPEC_u.pIns->pIF;
        if( pIF == NULL ) continue;
        if( !( pIF->status & CAMP_IF_ONLINE ) ) continue;

        gettimeval( &tv );

        if( difftimeval( &tv, &pReq->time ) >= 0.0 )
        {
            pReq->pending++;
#ifdef MULTITHREADED
            start_poll_thread( pReq );
#else
            status = (*pReq->procs.retProc)( pReq );
#endif /* MULTITHREADED */
        }
    }

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */
}



