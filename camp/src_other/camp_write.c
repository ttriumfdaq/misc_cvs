/*
 * camp_write.c, Parsing utils for CAMP Instrument Definition Files
 *
 *
 *  Revision history:
 *   v1.2  16-May-1994  [T. Whidden] Taken from the old camp_parse.c
 */

#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include "camp_srv.h"

static FILE *fout;                        /* file pointer of current input */
static char tabs[20];


bool_t
openOutput( char* filename )
{
#ifdef VMS
    fout = fopen( filename, "r+" );
    if( fout == NULL )
    {
#endif /* VMS */
        fout = fopen( filename, "w" );
        if( fout == NULL )
        {
            camp_appendMsg( "Unable to open file \"%s\" for output", filename );
            return( FALSE );
        }
#ifdef VMS
    }
#endif /* VMS */
    return( TRUE );
}


bool_t
closeOutput( void )
{
    if( fclose( fout ) == 0 )
    {
        fout = NULL;
        return( TRUE );
    }
    else
    {
        return( FALSE );
    }
}


void
add_tab( void )
{
    strcat( tabs, "\t" );
}


void
del_tab( void )
{
    tabs[ strlen( tabs ) - 1 ] = '\0';
}


void
print( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    vfprintf( fout, fmt, args );

    va_end( args );
}


void
print_tab( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    fputs( tabs, fout );
    vfprintf( fout, fmt, args );

    va_end( args );
}


/*
 * initialize the world
 */
void 
reinitialize( char* name, PFB parse_proc, char* line )
{
    tabs[0] = '\0';
}


