/*
 *  camp_var_priv.c,  Variable utilities
 */

#include <stddef.h>
#include <math.h>
#include <string.h>
#include "timeval.h"
#include "camp_srv.h"

/*
void
varInitPoll( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_POLL ) &&
        !( pVar->core.status & CAMP_VAR_PEND_SET ) )
    {
        REQ_addPoll( pVar );
    }
}
*/


      /*----------------------------------------*/
     /*   Data setting routines                */
    /*----------------------------------------*/

int
varRead( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;
    CAMP_VAR* pInsVar;

    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) 
    {
        camp_appendMsg( msg_inval_parent, pVar->core.ident );
        return( CAMP_INVAL_VAR );
    }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pInsVar->core.path );
        return( CAMP_INVAL_IF );
    }

    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pInsVar->core.ident );

    if( pInsType->procs.readProc != NULL )
    {
        status = (*pInsType->procs.readProc)( pInsVar, pVar );
        if( _failure( status ) )
        {
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  Request to set a variable to the 
 *  contents of pSpec.  Calls instrument driver.
 */
int
varSetReq( CAMP_VAR* pVar, CAMP_SPEC* pSpec )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;
    CAMP_VAR* pInsVar;

    if( pSpec == NULL ) return( CAMP_FAILURE );

    switch( pSpec->varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        varNumSetTol( pVar, pSpec->CAMP_SPEC_u.pNum->tolType, 
                      pSpec->CAMP_SPEC_u.pNum->tol );
        break;
      default:
        break;
    }

    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) 
    {
        camp_appendMsg( msg_inval_parent, pVar->core.ident );
        return( CAMP_INVAL_VAR );
    }

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pInsVar->core.path );
        return( CAMP_INVAL_IF );
    }

    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pInsVar->core.ident );

    if( pInsType->procs.setProc != NULL )
    {
        status = (*pInsType->procs.setProc)( pInsVar, pVar, pSpec );
        if( _failure( status ) )
        {
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  varSetSpec,  NOT recursive
 *  Actually set the variable to the 
 *  applicable contents of pSpec.
 */
int 
varSetSpec( CAMP_VAR* pVar, CAMP_SPEC* pSpec )
{
    if( pSpec == NULL ) return( CAMP_FAILURE );

    switch( pSpec->varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        varNumSetTol( pVar, pSpec->CAMP_SPEC_u.pNum->tolType, 
                               pSpec->CAMP_SPEC_u.pNum->tol );
        varNumSet( pVar, pSpec->CAMP_SPEC_u.pNum->val );
        break;
      case CAMP_VAR_TYPE_SELECTION:
        if( pSpec->CAMP_SPEC_u.pSel->pItems != NULL )
        {
            varSelSetLabel( pVar, 
                pSpec->CAMP_SPEC_u.pSel->pItems->label );
        }
        else
        {
            varSelSet( pVar, pSpec->CAMP_SPEC_u.pSel->val );
        }
        break;
      case CAMP_VAR_TYPE_STRING:
        varStrSet( pVar, pSpec->CAMP_SPEC_u.pStr->val );
        break;
      case CAMP_VAR_TYPE_ARRAY:
        varArrSet( pVar, pSpec->CAMP_SPEC_u.pArr->pVal );
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        break;
      case CAMP_VAR_TYPE_LINK:
        varLnkSet( pVar, pSpec->CAMP_SPEC_u.pLnk->path );
        break;
      default:
        return( CAMP_INVAL_VAR_TYPE );
    }
    return( CAMP_SUCCESS );
}


int
var_set( CAMP_VAR* pVar )
{
    varSetIsSet( pVar, TRUE );
    gettimeval( &pVar->core.timeLastSet );

    return( CAMP_SUCCESS );
}


int
varNumSet( CAMP_VAR* pVar, double val )
{
    CAMP_NUMERIC* pNum = pVar->spec.CAMP_SPEC_u.pNum;

    pNum->val = val;

    var_set( pVar );
    varDoStats( pVar );

    return( CAMP_SUCCESS );
}


int
varNumSetTol( CAMP_VAR* pVar, u_long tolType, float tol )
{
    CAMP_NUMERIC* pNum = pVar->spec.CAMP_SPEC_u.pNum;

    pNum->tol = tol;
    pNum->tolType = tolType;

    return( CAMP_SUCCESS );
}


int
varSelSet( CAMP_VAR* pVar, u_char val )
{
    CAMP_SELECTION* pSpec = pVar->spec.CAMP_SPEC_u.pSel;

    pSpec->val = val;

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varSelSetLabel( CAMP_VAR* pVar, char* label )
{
    int status;
    u_char val;

    status = varSelGetLabelID( pVar, label, &val );
    if( _failure( status ) ) return( status );

    varSelSet( pVar, val );

    return( CAMP_SUCCESS );
}


int
varStrSet( CAMP_VAR* pVar, char* val )
{
    CAMP_STRING* pSpec = pVar->spec.CAMP_SPEC_u.pStr;

    _free( pSpec->val );
    pSpec->val = strndup( val, LEN_STRING );

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varArrSet( CAMP_VAR* pVar, caddr_t pVal )
{
    CAMP_ARRAY* pSpec = pVar->spec.CAMP_SPEC_u.pArr;

    bcopy( pVal, pSpec->pVal, (pSpec->totElem)*(pSpec->elemSize) );

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varLnkSet( CAMP_VAR* pVar, char* path )
{
    CAMP_LINK* pSpec = pVar->spec.CAMP_SPEC_u.pLnk;

    _free( pSpec->path );
    pSpec->path = strndup( path, LEN_PATH );

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varSetShow( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SHOW ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_SHOW;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_SHOW;

    return( CAMP_SUCCESS );
}


int
varSetSet( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SET ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_SET;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_SET;

    return( CAMP_SUCCESS );
}


int
varSetRead( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_READ ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_READ;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_READ;

    return( CAMP_SUCCESS );
}


int
varSetPoll( CAMP_VAR* pVar, bool_t flag, float interval )
{
    int status;
    CAMP_VAR* pInsVar;
    bool_t wasPolled;

    if( !( pVar->core.attributes & CAMP_VAR_ATTR_POLL ) )
        return( CAMP_INVAL_VAR_ATTR );

    wasPolled = ( pVar->core.status & CAMP_VAR_ATTR_POLL );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_POLL;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_POLL;

    if( interval > 0.0 ) pVar->core.pollInterval = interval;

    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL )
    {
        status = CAMP_INVAL_VAR;
        return( status );
    }

    if( !wasPolled && flag )
    {
        REQ_addPoll( pVar );
    }
    else if( wasPolled && !flag )
    {
        REQ_cancel( pVar, IO_POLL );
    }

    return( CAMP_SUCCESS );
}


int
varSetLog( CAMP_VAR* pVar, bool_t flag, char* logAction )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_LOG ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_LOG;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_LOG;

    if( logAction != NULL )
    {
        _free( pVar->core.logAction );
        pVar->core.logAction = strndup( logAction, LEN_PROC );
    }

    return( CAMP_SUCCESS );
}


int
varSetAlarm( CAMP_VAR* pVar, bool_t flag, char* alarmAction )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( alarmAction != NULL )
    {
        _free( pVar->core.alarmAction );
        pVar->core.alarmAction = strndup( alarmAction, LEN_PROC );
    }

    if( flag ) 
    {
        pVar->core.status |= CAMP_VAR_ATTR_ALARM;
    }
    else       
    {
        pVar->core.status &= ~CAMP_VAR_ATTR_ALARM;
        varSetAlert( pVar, FALSE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  TRUE = within tolerance, alert off
 *  FALSE = out of tolerance, alert on
 */
bool_t
varNumTestAlert( CAMP_VAR* pVar, double val )
{
    if( !( pVar->core.status & CAMP_VAR_ATTR_ALARM ) )
    {
        return( TRUE );
    }

    if( camp_varNumTestTol( pVar->core.path, val ) )
    {
        varSetAlert( pVar, FALSE );
        return( TRUE );
    }
    else
    {
        varSetAlert( pVar, TRUE );
        return( FALSE );
    }
}


int
varSetAlert( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_ALERT;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_ALERT;

    camp_alarmCheck( pVar->core.alarmAction );

    return( CAMP_SUCCESS );
}


int
varSetIsSet( CAMP_VAR* pVar, bool_t flag )
{
/*
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SET ) &&
        !( pVar->core.attributes & CAMP_VAR_ATTR_READ ) )
        return( CAMP_INVAL_VAR_ATTR );
*/
    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_IS_SET;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_IS_SET;

    return( CAMP_SUCCESS );
}


int
varSetPendSet( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SET ) &&
        !( pVar->core.attributes & CAMP_VAR_ATTR_READ ) )
        return( CAMP_INVAL_VAR_ATTR );

    if( flag ) pVar->core.status |= CAMP_VAR_PEND_SET;
    else       pVar->core.status &= ~CAMP_VAR_PEND_SET;

    return( CAMP_SUCCESS );
}


int
varSetMsg( CAMP_VAR* pVar, char* msg )
{
    _free( pVar->core.statusMsg );
    pVar->core.statusMsg = strndup( msg, LEN_STATUS_MSG );

    return( CAMP_SUCCESS );
}


int
varZeroStats( CAMP_VAR* pVar )
{
    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) == CAMP_VAR_TYPE_NUMERIC )
    {
        varNumZeroStats( pVar->spec.CAMP_SPEC_u.pNum );
    }

    if( pVar->pChild != NULL ) 
    {
        for( pVar = pVar->pChild; pVar != NULL; pVar = pVar->pNext )
        {
            varZeroStats( pVar );
        }
    }

    return( CAMP_SUCCESS );
}


void 
varNumZeroStats( CAMP_NUMERIC* pNum )
{
    cleartimeval( &pNum->timeStarted );
    pNum->num = 0;
    pNum->low = 0.0;
    pNum->hi = 0.0;
    pNum->sum = 0.0;
    pNum->sumSquares = 0.0;
    pNum->sumCubes = 0.0;
    pNum->offset = 0.0;
}


int
varDoStats( CAMP_VAR* pVar )
{
    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) == CAMP_VAR_TYPE_NUMERIC )
        varNumDoStats( pVar->spec.CAMP_SPEC_u.pNum );

    return( CAMP_SUCCESS );
}


void 
varNumDoStats( CAMP_NUMERIC* pNum )
{
    double delta;
    timeval_t tv_zero = { 0, 0 };

    if( difftimeval( &pNum->timeStarted, &tv_zero ) == 0.0 )
    {
        /*
	 *  Stats were zeroed
	 */
        gettimeval( &pNum->timeStarted );
        pNum->offset = pNum->hi = pNum->low = pNum->val;
    }
    (pNum->num)++;
    pNum->low = _min( pNum->low, pNum->val );
    pNum->hi = _max( pNum->hi, pNum->val );
    delta = pNum->val - pNum->offset;
    pNum->sum += delta;
    pNum->sumSquares += pow( delta, (double)2.0 );
    pNum->sumCubes += pow( delta, (double)3.0 );
}


      /*----------------------------------------*/
     /*  Data deallocation                     */
    /*----------------------------------------*/

void
varFree( CAMP_VAR* pVar )
{
    pVar->pNext = NULL;
    set_current_xdr_flag( CAMP_XDR_ALL );
    _xdr_free( xdr_CAMP_VAR, pVar );
}


int
varAdd( char* path, CAMP_VAR* pVar )
{
    CAMP_VAR** ppVar;
    CAMP_VAR** ppVar_start;
    char parentPath[LEN_PATH+1];
    char* p;

    strcpy( parentPath, path );
    p = strrchr( parentPath, '/' );
    *p = '\0';

    if( parentPath[0] == '\0' )
    {
        ppVar_start = &pVarList;
    }
    else
    {
        ppVar_start = camp_varGetpp( parentPath );
        if( ppVar_start == NULL ) return( CAMP_INVAL_VAR );

        ppVar_start = &(*ppVar_start)->pChild;
    }

    for( ppVar = ppVar_start; *ppVar != NULL; ppVar = &(*ppVar)->pNext ) ;

    *ppVar = pVar;
    (*ppVar)->pNext = NULL;

    return( CAMP_SUCCESS );
}


/*
 *  Based on camp_varGetTruePP
 */
/*
CAMP_VAR** 
camp_varGetNewPP( char* path_req )
{
    char path_curr[LEN_PATH+1];
    char ident_next[LEN_IDENT+1];
    char ident_search[LEN_IDENT+1];
    CAMP_VAR** ppVar;

    camp_pathInit( path_curr );

    for( ppVar = &pVar_new; *ppVar != NULL; ppVar = &(*ppVar)->pChild )
    {
        if( camp_pathGetNext( path_req, path_curr, ident_next ) == NULL ) 
		{
            return( NULL );
		}

        if( strlen( ident_next ) == 0 ) 
		{
			return( NULL );
		}
	    else if( streq( ident_next, "~" ) ) 
		{
	        strcpy( ident_search, get_current_ident() );
		}
	    else 
		{
	        strcpy( ident_search, ident_next );
		}

        for( ; *ppVar != NULL; ppVar = &(*ppVar)->pNext )
		{
            if( streq( ident_search, (*ppVar)->core.ident ) ) 
			{
                break;
			}
		}

        if( *ppVar == NULL ) 
		{
			return( NULL );
		}

        camp_pathDown( path_curr, ident_next );

        if( streq( path_req, path_curr ) ) 
		{
			return( ppVar );
		}
    }
    return( NULL );
}
*/
/*
int
camp_varAddNew( char* path, CAMP_VAR* pVar )
{
    CAMP_VAR** ppVar;
    CAMP_VAR** ppVar_start;
    char parentPath[LEN_PATH+1];
    char* p;

    strcpy( parentPath, path );
    p = strrchr( parentPath, '/' );
    *p = '\0';

    if( parentPath[0] == '\0' )
    {
        ppVar_start = &pVar_new;
    }
    else
    {
        ppVar_start = camp_varGetNewPP( parentPath );
        if( ppVar_start == NULL ) return( CAMP_INVAL_VAR );

        ppVar_start = &(*ppVar_start)->pChild;
    }

    for( ppVar = ppVar_start; *ppVar != NULL; ppVar = &(*ppVar)->pNext ) ;

    *ppVar = pVar;
    (*ppVar)->pNext = NULL;

    return( CAMP_SUCCESS );
}
*/


