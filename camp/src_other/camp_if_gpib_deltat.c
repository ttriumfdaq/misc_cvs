/*
 *  camp_if_gpib_cam.c,  utilities for operating GPIB instruments
 *                       via CAMAC Kinetic Systems 3388 Module
 *
 *  INI file configuration string:
 *   {<b> <c> <n> <a> <timeout> <delay>}
 *    timeout - integer in seconds
 *    delay - float in seconds (delay between each byte transfered)
 *
 *  Meaning of private (driver-specific) variables:
 *   pIF_t->priv - GPIB semaphore address
 *               == 0 (gpib unavailable)
 *   pIF->priv   - not used
 */

#include <stdio.h>
#include "deltat_defs.h"
#include "camp_srv.h"

static u_char getTerm( char* term );

#define gpib_cam_ok  (pIF_t->priv!=0)
#define gpib_sem_addr (pIF_t->priv)


int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int prim_addr = 0, tmo = 0;
    float delay = 0;
    char str_tmo[16];
    s_dsc dsc_tmo;
    u_long sem_addr[2];
    long l;
    int b = 0, c = 0, n = 0;

    /*
     *  Need CAMAC to use this GPIB driver
     */
    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL)
    {
        camp_appendMsg( "gpib needs camac, gpib disabled" );
        return( CAMP_FAILURE );
    }

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /* 
     *  priv holds the semaphore address
     *  The value is zero if GPIB is unavailable
     */
    pIF_t->priv = 0; 

    sscanf( pIF_t->conf, " %d %d %d %d %d %f", 
                           &b, &c, &n, &prim_addr, &tmo, &delay );

    sprintf( str_tmo, "0 0:0:%d", tmo );
    setdsctostr( &dsc_tmo, str_tmo );

    /*
     *  "delay" is the delay between each data read 
     *  from the 3388 in seconds
     */
    status = GPIB_INIT( &b, &c, &n, &prim_addr, &dsc_tmo, &delay );
    if( _failure( status ) )
    {
        camp_appendMsg( "gpib initialization failed, gpib disabled" );
        return( CAMP_FAILURE );
    }

    l = MAP__CAMAC_SEMAPHORE;
    status = MAP_GLOBAL( &l, sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "mapping gpib semaphore failed, gpib disabled" );
        return( CAMP_FAILURE );
    }

    /*
     *  Compute address of GPIB semaphore
     */
    pIF_t->priv = sem_addr[0] + SEM__CAMAC_GPIB;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        status = if_gpib_init();
        if( _failure( status ) ) 
        {
            camp_appendMsg( "gpib unavailable" );
            return( CAMP_FAILURE );
        }
    }

    if( ( addr = camp_getIfGpibAddr( pIF->defn ) < 0 ) ||
        ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    int status2;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    s_dsc dsc_cmd;
    s_dsc dsc_term;
    s_dsc dsc_buf;
    long zero = 0;
    u_char term;
    char str[LEN_STR+1];
    u_long addr;
    char buf[512];

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    setdsctobuf( &dsc_cmd, pReq->spec.REQ_SPEC_u.read.cmd,
                 pReq->spec.REQ_SPEC_u.read.cmd_len );

    term = getTerm( camp_getIfGpibReadTerm( pIF->defn, str ) );
    setdsctobuf( &dsc_term, &term, 1 );

    addr = camp_getIfGpibAddr( pIF->defn );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }

    status = gpib_send( &addr, &dsc_cmd, &dsc_term );
    if( _failure( status ) )
    {
    	sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
        camp_appendMsg( buf );
        camp_appendVMSMsg( status );
    }

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
    {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
    }

    if( _failure( status ) )
    {
        /*
         *  BIG FAT CLUGE
         *  Sometimes gpib_cam gets stuck when CAMAC changes state.
         *  If the error is not just a wrong GPIB address,
         *  reinitializing CAMAC and then GPIB fixes this.
         */
        status = if_camac_init();
        if( _failure( status ) ) return( status );

        status = if_gpib_init();
        if( _failure( status ) ) return( status );
	
	status = SEM_CLAIM( gpib_sem_addr );
	if( _failure( status ) )
	  {
	    camp_appendMsg( "failed to claim gpib semaphore" );
	    camp_appendVMSMsg( status );
	    return( status );
	  }
	
        status = gpib_send( &addr, &dsc_cmd, &dsc_term );
	
	status2 = SEM_RELEASE( gpib_sem_addr );
	if( _failure( status2 ) )
	  {
	    camp_appendMsg( "failed releasing gpib semaphore" );
	    camp_appendVMSMsg( status2 );
	  }
	
        if( _failure( status ) )
        {
    	    sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
            camp_appendMsg( buf );
            camp_appendVMSMsg( status );
            return( status );
        }
    }

    setdsctobuf( &dsc_buf, pReq->spec.REQ_SPEC_u.read.buf, 
                 pReq->spec.REQ_SPEC_u.read.buf_len );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }

    status = gpib_enter( &dsc_buf, 
                         &pReq->spec.REQ_SPEC_u.read.read_len,
                         &addr, &dsc_term );

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
    {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
    }

    if( _failure( status ) )
    {
        camp_appendMsg( "failed receiving gpib data" );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status, status2;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    s_dsc dsc_cmd;
    s_dsc dsc_term;
    long zero = 0;
    u_long addr;
    u_char term;
    char str[LEN_STR+1];
    char buf[512];

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    setdsctobuf( &dsc_cmd, pReq->spec.REQ_SPEC_u.write.cmd,
                 pReq->spec.REQ_SPEC_u.write.cmd_len );
    term = getTerm( camp_getIfGpibReadTerm( pIF->defn, str ) );
    setdsctobuf( &dsc_term, &term, 1 );

    addr = camp_getIfGpibAddr( pIF->defn );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }
    
    status = gpib_send( &addr, &dsc_cmd, &dsc_term );
    if( _failure( status ) )
    {
        sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
        camp_appendMsg( buf );
        camp_appendVMSMsg( status );
    }

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
      {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
      }
    
    if( _failure( status ) )
    {
        /*
         *  BIG FAT CLUGE
         *  Sometimes gpib_cam gets stuck when CAMAC changes state.
         *  If the error is not is not just a wrong GPIB address,
         *  reinitializing CAMAC and then GPIB fixes this.
         */
        status = if_camac_init();
        if( _failure( status ) ) return( status );

        status = if_gpib_init();
        if( _failure( status ) ) return( status );
	
	status = SEM_CLAIM( gpib_sem_addr );
	if( _failure( status ) )
	  {
	    camp_appendMsg( "failed to claim gpib semaphore" );
	    camp_appendVMSMsg( status );
	    return( status );
	  }
	
        status = gpib_send( &addr, &dsc_cmd, &dsc_term );

	status2 = SEM_RELEASE( gpib_sem_addr );
	if( _failure( status2 ) )
	  {
	    camp_appendMsg( "failed releasing gpib semaphore" );
	    camp_appendVMSMsg( status2 );
	  }
	
        if( _failure( status ) )
        {
    	    sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
            camp_appendMsg( buf );
            camp_appendVMSMsg( status );
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


static u_char
getTerm( char* term )
{
    TOKEN tok;

    findToken( term, &tok );

    switch( tok.kind )
    {
        case TOK_LF:
            return( '\012' );
        case TOK_CR:
            return( '\015' );
        case TOK_CRLF:
            return( '\012' );
        case TOK_LFCR:
            return( '\015' );
        case TOK_NONE:
            return( 0 );
        default:
            return( 0 );
    }
}
