/*
 *  camp_if_gpib_gen.c,  utilities for operating GPIB instruments
 *                       generic
 *
 *   pIF_t->priv == 1 (gpib available)
 *               == 0 (gpib unavailable)
 *
 */

#include <stdio.h>
#include "camp_srv.h"

#define if_gpib_ok  (pIF_t->priv!=0)

static void getTerm( char* term_in, char* term_out, int* term_len );


int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    char cIpModuleIndex;
    int timeout;

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    pIF_t->priv = 0; 

    /*
     *  Check configuration data here
     */

    pIF_t->priv = 1;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    if( ( ( addr = camp_getIfGpibAddr( pIF->defn ) ) < 0 ) ||
          ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    /*
     *  Turn online here
     */

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;
    int status;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    /*
     *  Turn offline here
     */

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Write here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */


#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Read here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = 0;

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    /*
     *  Write here
     */

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    return( CAMP_SUCCESS );
}


/*
static void
getTerm( char* term_in, char* term_out, int* term_len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
        case TOK_LF:   strcpy( term_out, "\012" );     *term_len = 1; return;
        case TOK_CR:   strcpy( term_out, "\015" );     *term_len = 1; return;
        case TOK_CRLF: strcpy( term_out, "\015\012" ); *term_len = 2; return;
        case TOK_LFCR: strcpy( term_out, "\012\015" ); *term_len = 2; return;
        case TOK_NONE: strcpy( term_out, "" );         *term_len = 0; return;
        default:       strcpy( term_out, "" );         *term_len = 0; return;
    }
}
*/

