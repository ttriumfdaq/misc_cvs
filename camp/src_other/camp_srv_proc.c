
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#ifdef VMS
#include <unixio.h>
#endif /* VMS */

#include "timeval.h"
#include "camp_srv.h"


/*
 *  get_ins_thread_lock  -  get instrument-specific mutex
 *
 *  There are two locks (mutexes) used by the CAMP server for
 *  multithreading purposes:  global and instrument.  The
 *  global lock is (mainly) used to guard the threads against
 *  non-reentrant code.  The instrument mutex is used to
 *  make sure that only one thread is performing I/O to 
 *  an instrument at one time.
 *
 *  "lock" and "mutex" are used interchangeably.
 *
 *  Strategy:  The global lock will already be taken upon 
 *             entry.  Try getting the instrument lock.  If
 *             free, it's taken right away.  If not free,
 *             must unlock the global lock to allow the thread
 *             that has the instrument lock to continue (as 
 *             well as any others needing the global lock), 
 *             then wait indefinitely for the instrument lock.
 *             Once the instrument lock is available, then
 *             take it and wait again for the global mutex.
 */
void
get_ins_thread_lock( CAMP_VAR* pVar )
{
#ifdef MULTITHREADED
    if( pthread_mutex_trylock( &(pVar->spec.CAMP_SPEC_u.pIns->mutex) ) != 1 )
    {
        thread_unlock_global_np();

	camp_thread_mutex_lock( &(pVar->spec.CAMP_SPEC_u.pIns->mutex) );

        thread_lock_global_np();
    }
#endif /* MULTITHREADED */
}


void
release_ins_thread_lock( CAMP_VAR* pVar )
{
#ifdef MULTITHREADED
    pthread_mutex_unlock( &(pVar->spec.CAMP_SPEC_u.pIns->mutex) );
#endif /* MULTITHREADED */
}


RES* 
campsrv_sysrundown_13( void* dummy, struct svc_req* rq )
{
    RES* pRes;
    int status;

    pRes = (RES*)zalloc( sizeof( RES ) );

    pRes->status = camp_checkAllInssLock( rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( "CAMP Server shutdown failure: an instrument is locked" );
        return( pRes );
    }

    camp_logMsg( "shutdown requested" );
    printf( "shutdown requested\n" );

    status = campCfg_write( CAMP_SRV_AUTO_SAVE );
    if( _failure( status ) )
      {
	camp_logMsg( "shutdown: failed auto-save" );
	printf( "shutdown: failed auto-save\n" );
      }
    else
      {
	camp_logMsg( "shutdown: done auto-save" );
	printf( "shutdown: done auto-save\n" );
      }

    camp_setMsg( "CAMP Server successfully shutdown" );
    pRes->status = CAMP_SUCCESS;

#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "SHUTDOWN", 1 );
#endif /* VMS */

    /*
     *  Flag main thread to stop all threads
     *  and exit
     */
    set_shutdown();
    return( pRes );
}


RES* 
campsrv_sysreboot_13( void* dummy, struct svc_req* rq )
{
    RES* pRes;
    int status;

    pRes = (RES*)zalloc( sizeof( RES ) );

    pRes->status = camp_checkAllInssLock( rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( "CAMP Server restart failure: an instrument is locked" );
        return( pRes );
    }

    camp_logMsg( "restart requested" );
    printf( "restart requested\n" );

    status = campCfg_write( CAMP_SRV_AUTO_SAVE );
    if( _failure( status ) )
      {
	camp_logMsg( "restart: failed auto-save" );
	printf( "restart: failed auto-save\n" );
      }
    else
      {
	camp_logMsg( "restart: done auto-save" );
	printf( "restart: done auto-save\n" );
      }

    camp_setMsg( "CAMP Server successfully restarted" );
    pRes->status = CAMP_SUCCESS;

#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "RESTART", 1 );
#endif /* VMS */

    /*
     *  Flag main thread to stop all threads
     *  and exit
     */
    set_shutdown();
    return( pRes );
}


RES* 
campsrv_sysupdate_13( void* dummy, struct svc_req* rq )
{
    RES* pRes;

    pRes = (RES*)zalloc( sizeof( RES ) );

    pRes->status = sys_update();

    camp_logMsg( camp_getMsg() );

    return( pRes );
}


RES* 
campsrv_sysload_13( FILE_req* pFile_req, struct svc_req* rq )
{
    RES* pRes;
    FILE* fin;
    Tcl_Interp* interp = camp_tclInterp();

    pRes = (RES*)zalloc( sizeof( RES ) );

    if( pFile_req->flag == 1 )
    {
        pRes->status = camp_checkAllInssLock( rq );
        if( _failure( pRes->status ) ) 
        {
            camp_appendMsg( "campsrv_sysload: failure: an instrument is locked" );
            return( pRes );
        }
    }

    /*
     *  Make sure the file exists
     */
    fin = fopen( pFile_req->filename, "r" );
    if( fin == NULL )
    {
        camp_appendMsg( "campsrv_sysload: failure: open file %s", 
                        pFile_req->filename );
        pRes->status = CAMP_INVAL_FILE;
        return( pRes );
    }
    fclose( fin );

    if( pFile_req->flag == 1 )
    {
        /*
         *  Delete all current instruments
         */
        srv_delAllInss();
    }

    _free( pSys->pDyna->cfgFile );
    pSys->pDyna->cfgFile = strdup( pFile_req->filename );

    /*
     *  Interpret the file using Tcl
     *  Lock the global mutex while using the main interpreter
     */
    set_global_mutex_noChange( TRUE );
    pRes->status = Tcl_EvalFile( interp, pFile_req->filename );
    set_global_mutex_noChange( FALSE );
    if( pRes->status == TCL_ERROR )
    {
        camp_appendMsg( "system load failure: %s", interp->result );
        pRes->status = CAMP_FAILURE;
        return( pRes );
    }

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_syssave_13( FILE_req* pFile_req, struct svc_req* rq )
{
    RES* pRes;

    pRes = (RES*)zalloc( sizeof( RES ) );

    pRes->status = campCfg_write( pFile_req->filename );
    if( _failure( pRes->status ) )
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_syssave: failure: campCfg_write( %s )",
                        pFile_req->filename );
        }
        return( pRes );
    }

    _free( pSys->pDyna->cfgFile );
    pSys->pDyna->cfgFile = strdup( pFile_req->filename );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


CAMP_SYS_res* 
campsrv_sysget_13( void* dummy, struct svc_req* rq )
{
    CAMP_SYS_res* pRes;

    pRes = (CAMP_SYS_res*)zalloc( sizeof( CAMP_SYS_res ) );

    pRes->pSys = pSys;

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


SYS_DYNAMIC_res* 
campsrv_sysgetdyna_13( void* dummy, struct svc_req* rq )
{
    SYS_DYNAMIC_res* pRes;

    pRes = (SYS_DYNAMIC_res*)zalloc( sizeof( SYS_DYNAMIC_res ) );

    pRes->pDyna = pSys->pDyna;

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


DIR_res* 
campsrv_sysdir_13( FILE_req* pFile_req, struct svc_req* rq )
{
    DIR_res* pRes;

    pRes = (DIR_res*)zalloc( sizeof( DIR_res ) );

    /*
     *  Expand the filespec
     */
    pRes->status = sys_initDir( pFile_req->filename );
    if( _failure( pRes->status ) )
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_sysdir: failure: sys_initDir( %s )",
                        pFile_req->filename );
        }
        return( pRes );
    }

    pRes->pDir = pSys->pDir;

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insadd_13( INS_ADD_req* pInsAdd_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    char defFile[LEN_FILENAME];
    CAMP_INSTRUMENT* pIns;
    char path[LEN_PATH+1];
    INS_TYPE* pInsType;
    char* p;
    int status;
    INS_AVAIL* pInsAvail;
    bool_t isAvail;
    Tcl_Interp* interp;
    Tcl_Interp* oldInterp;
    FILE* fin = NULL;
    char buf[512];
    char* cmdBuf = NULL;
    int cmdLen;
    int cmdBufLen;
    int i;
    bool_t done;

    pRes = (RES*)zalloc( sizeof( RES ) );

    /*
     *  Make sure the instrument type exists
     */
    if( ( pInsType = camp_sysGetpInsType( pInsAdd_req->typeIdent ) ) == NULL )
    {
        camp_appendMsg( "campsrv_insadd: error: bad instrument type %s",
                        pInsAdd_req->typeIdent );
        pRes->status = CAMP_INVAL_INS_TYPE;
        return( pRes );
    }

    /*
     *  Make sure the instrument DOESN'T exist
     */
    camp_pathInit( path );
    camp_pathDown( path, pInsAdd_req->ident );
    if( camp_varExists( path ) )
    {
        camp_appendMsg( "campsrv_insadd: error: instrument %s already exists",
                        path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    /*
     *  check validity of ident
     */
    if( ( strlen( pInsAdd_req->ident ) < 1 ) || 
        ( strlen( pInsAdd_req->ident ) > LEN_IDENT ) )
    {
        camp_appendMsg( "campsrv_insadd: error: invalid ident %s", 
                        pInsAdd_req->ident );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    for( p = pInsAdd_req->ident; *p != '\0'; p++ )
    {
        if( !isalnum( *p ) && ( *p != '_' ) && ( *p != '-' ) )
        {
            camp_appendMsg( "campsrv_insadd: error: invalid ident %s", 
                        pInsAdd_req->ident );
            pRes->status = CAMP_INVAL_INS;
            return( pRes );
        }
    }

    /* 
     *  Check if this is an "available" instrument
     */
    isAvail = FALSE;
    for( pInsAvail = pSys->pInsAvail; 
         pInsAvail != NULL; 
         pInsAvail = pInsAvail->pNext )
    {
        if( ( pInsAvail->ident[0] == pInsAdd_req->ident[0] ) && 
            ( streq( pInsAvail->ident, pInsAdd_req->ident ) ) )
        {
            if( !streq( pInsAvail->typeIdent, pInsAdd_req->typeIdent ) )
            {
                camp_appendMsg( 
                    "campsrv_insadd: error: wrong type '%s' for known ident '%s'", 
                    pInsAdd_req->typeIdent, pInsAdd_req->ident );
                pRes->status = CAMP_FAILURE;
                return( pRes );
            }
            else
            {
                isAvail = TRUE;
                break;
            }
        }
    }

    /*
     *  NOTE: must set global variable "default_ident" to
     *              unique ident of instrument.  This is not known
     *              at time of writing the DEF file.
     */
    set_current_ident( pInsAdd_req->ident );

    /*
     *  determine instrument definition file name
     */
    /*
     *  Only Tcl drivers
     */
    if( streq( pInsType->driverType, "Tcl" ) )
    {
        sprintf( defFile, CAMP_TCL_PFMT, pInsAdd_req->typeIdent );
    }
    else
      {
	camp_appendMsg( "invalid instrument driver type %s",  
                        pInsType->driverType );
        pRes->status = CAMP_FAILURE;
        return( pRes );
      }

    fin = fopen( defFile, "r" );
    if( fin == NULL )
      {
	camp_appendMsg( "failed to open def'n file %s", defFile );
        pRes->status = CAMP_FAILURE;
        return( pRes );
      }

    cmdBufLen = 512;
    cmdBuf = (char*)zalloc( cmdBufLen );

    /*
     *  Interpret lines in the definition file until
     *  the instrument has been defined, then stop.
     *  This should be just the first noncomment line
     *  (including continuation lines).
     */
    for( pVar = NULL; pVar == NULL; pVar = camp_varGetp( path ) )
      {
	cmdLen = 0;
	cmdBuf[0] = '\0';
        /*
         *  Read a line (including continuation lines)
         */
	for(;;)
        {
            if( fgets( buf, 512, fin ) == NULL )
	    {
	        camp_appendMsg( "failed to add instrument: premature end-of-file in defn file" );
                goto error;
	    }
            cmdLen += strlen( buf );
            /*
	     *  Make buffer bigger if necessary
	     */
	    if( cmdLen + 1 > cmdBufLen )
	    {
	      cmdBufLen += 512;
	      cmdBuf = rezalloc( cmdBuf, cmdBufLen );
	    }
	    strcat( cmdBuf, buf );
            /*
	     *  Check if the line is continued
	     */
            for( i = cmdLen-1; ( i > 0 ) && ( !isgraph( cmdBuf[i] ) ); i-- ) ;
            if( cmdBuf[i] != '\\' ) break;
	}

	/*
	 *  Interpret the line using the main interpreter
	 *  Lock the global mutex while using the main interpreter
	 */
        if( cmdLen > 0 )
	  {
            interp = camp_tclInterp();

	    set_global_mutex_noChange( TRUE );
	    status = Tcl_Eval( interp, cmdBuf );
	    set_global_mutex_noChange( FALSE );
	    if( status == TCL_ERROR )
	      {
		camp_appendMsg( "failed to add instrument: %s", interp->result );
                goto error;
	      }
	  }
    }

    /*
     *  Instrument not fully defined, but get the lock
     *  to make sure nobody tries anything before it is
     *  defined.
     */
    get_ins_thread_lock( pVar );

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    /*
     *  Interpret the rest of the file
     *  (very similar to Tcl_EvalFile)
     */
    cmdLen = 0;
    cmdBuf[0] = '\0';

    /*
     *  Read the whole file
     *  Could improve this using read() with larger blocks
     */
    while( fgets( buf, 512, fin ) != NULL )
      {
	cmdLen += strlen( buf );
	/*
	 *  Make buffer bigger if necessary
	 */
	if( cmdLen + 1 > cmdBufLen )
	  {
	    cmdBufLen += 10240;
	    cmdBuf = rezalloc( cmdBuf, cmdBufLen );
	  }
	strcat( cmdBuf, buf );
      }

    /*
     *  Interpret the file using the instrument interpreter
     */
    if( cmdLen > 0 )
      {
	oldInterp = get_thread_interp();
	set_thread_interp( pIns->interp );
	interp = pIns->interp;
	
	status = Tcl_Eval( interp, cmdBuf );
	set_thread_interp( oldInterp );
	if( status == TCL_ERROR )
	  {
	    camp_appendMsg( "failed to add instrument: %s", interp->result );
            release_ins_thread_lock( pVar );
	    goto error;
	  }
      }

    fclose( fin );  fin = NULL;
    _free( cmdBuf );

    /*
     *  initialize things that aren't defined by def file
     */
    pIns->class = INS_ROOT;
    _free( pIns->defFile );        
    pIns->defFile = strdup( defFile );
    _free( pIns->typeIdent );        
    pIns->typeIdent = strdup( pInsAdd_req->typeIdent );
    pIns->typeInstance = sys_getTypeInstance( pInsAdd_req->typeIdent );

    gettimeval( &pSys->pDyna->timeLastInsChange );

    status = camp_insInit( pVar );
    if( _failure( status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insadd: warning: camp_insInit '%s'",
                        pVar->core.path );
        }
    }

    if( isAvail )
      {
	/*
	 *  Interpret the initProc defined in camp.ini
	 *  using the instrument interpreter
	 */
	oldInterp = get_thread_interp();
	set_thread_interp( pIns->interp );
	interp = pIns->interp;
	
        status = Tcl_Eval( interp, pInsAvail->specInitProc );
	set_thread_interp( oldInterp );
        if( status == TCL_ERROR )
	  {
            /*
             *  not fatal 
             */
            camp_appendMsg( "warning: %s", interp->result );
	  }
      }
    
    release_ins_thread_lock( pVar );
    pRes->status = CAMP_SUCCESS;
    return( pRes );

  error:
    if( fin != NULL ) { fclose( fin ); fin = NULL; }
    _free( cmdBuf );
    pRes->status = CAMP_FAILURE;
    return( pRes );
}


RES* 
campsrv_insdel_13( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVar_req->path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( path );
    if( pVar == NULL )
    {
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    /*
     *  Cancel all pending requests
     */
    REQ_cancelAllIns( pVar );

    /*
     *  Tell instrument driver to run down
     */
    pRes->status = camp_insDelete( pVar );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insdel: failure: camp_insDelete %s", 
                    pVar->core.path );
        }
/*
        pRes->status = CAMP_FAILURE;
        release_ins_thread_lock( pVar );
        return( pRes );
*/
    }

    release_ins_thread_lock( pVar );

    /*
     *  Free local memory and var list entry
     */
    pRes->status = camp_insDel( pVar->core.path );
    if( _failure( pRes->status ) )
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insdel: failure: camp_insDel %s", 
                    pVar->core.path );
        }
/*        pRes->status = CAMP_FAILURE;
        return( pRes );
*/
    }

    gettimeval( &pSys->pDyna->timeLastInsChange );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_inslock_13( INS_LOCK_req* pInsLock_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    struct authunix_parms* aup;
    CAMP_INSTRUMENT* pIns;
    char* path;
    char buf[128];

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsLock_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( pInsLock_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    aup = (struct authunix_parms*)rq->rq_clntcred;
    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    /*
     *  Lock instrument for calling PID and host (or unlock)
     */
    if( pInsLock_req->flag )
    {
#ifdef VMS
        /*
         *  Allow only local processes to lock (no remote)
         */
        if( !streq( pSys->hostname, stolower( aup->aup_machname ) ) )
        {
            camp_appendMsg( "cannot lock instrument from remote node" );
            pRes->status = CAMP_FAILURE;
            release_ins_thread_lock( pVar );
            return( pRes );
        }
#endif /* VMS */

	switch( aup->aup_gids[1] )
	  {
	  case CAMP_OS_VMS:
            _free( pIns->lockOs );
	    pIns->lockOs = strdup( "vms" );
	    break;
	  case CAMP_OS_BSD:
            _free( pIns->lockOs );
	    pIns->lockOs = strdup( "bsd" );
	    break;
	  default:
            camp_appendMsg( "unknown or invalid operating system on remote node" );
            pRes->status = CAMP_FAILURE;
            release_ins_thread_lock( pVar );
            return( pRes );
	  }
        pIns->lockPID = aup->aup_gids[0];
        _free( pIns->lockHost );
        pIns->lockHost = strdup( stolower( aup->aup_machname ) );

        pVar->core.status |= CAMP_INS_ATTR_LOCKED;
    }
    else
    {
        pIns->lockPID = 0;
        _free( pIns->lockHost );
        pIns->lockHost = strdup( "" );
        _free( pIns->lockOs );
        pIns->lockOs = strdup( "" );

        pVar->core.status &= ~CAMP_INS_ATTR_LOCKED;
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insline_13( INS_LINE_req* pInsLine_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsLine_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    if( pInsLine_req->flag )
    {
        pRes->status = camp_insOnline( pVar );
        if( _failure( pRes->status ) ) 
        {
            if( camp_debug > 1 )
            {
                camp_appendMsg( "campsrv_insline: failure: camp_insOnline %s",
                        pVar->core.path );
            }
            release_ins_thread_lock( pVar );
            return( pRes );
        }
    }
    else
    {
        pRes->status = camp_insOffline( pVar );
        if( _failure( pRes->status ) ) 
        {
            if( camp_debug > 1 )
            {
                camp_appendMsg( "campsrv_insline: failure: camp_insOffline %s",
                        pVar->core.path );
            }
            release_ins_thread_lock( pVar );
            return( pRes );
        }
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insload_13( INS_FILE_req* pInsFile_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    FILE* fin;
    char* path;
    Tcl_Interp* interp;
    Tcl_Interp* oldInterp;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsFile_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( pInsFile_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    if( ( pInsFile_req->datFile == NULL ) || 
        ( strlen( pInsFile_req->datFile ) == 0 ) )
    {
        camp_appendMsg( "campsrv_insload: invalid: filename %s", 
                    pInsFile_req->datFile );
        pRes->status = CAMP_INVAL_FILE;
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    /*
     *  Make sure the file exists
     */
    fin = fopen( pInsFile_req->datFile, "r" );
    if( fin == NULL )
    {
        camp_appendMsg( "campsrv_insload: failure: open file %s", 
                    pInsFile_req->datFile );
        pRes->status = CAMP_INVAL_FILE;
        release_ins_thread_lock( pVar );
        return( pRes );
    }
    fclose( fin );

    /*
     *  NOTE: must set global variable "default_ident" to
     *              unique ident of instrument.  This is not known
     *              at time of writing the INI file.
     */
    set_current_ident( pVar->core.ident );

    /*
     *  Use the instrument's Tcl interpreter
     */
    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    interp = pIns->interp;

    /*
     *  Interpret the file using Tcl
     */
    pRes->status = Tcl_EvalFile( interp, pInsFile_req->datFile );
    set_thread_interp( oldInterp );
    if( pRes->status == TCL_ERROR )
    {
        camp_appendMsg( "instrument load failure: %s", interp->result );
        pRes->status = CAMP_FAILURE;
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    /*
     *  Set the var file name
     */
    if( pInsFile_req->flag & (1<<0) )
    {
        _free( pIns->iniFile );
        pIns->iniFile = strdup( pInsFile_req->datFile );
    }

    /*
     *  Call init routine
     */
/*
    camp_insInit( pVar );
*/

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_inssave_13( INS_FILE_req* pInsFile_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsFile_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( pInsFile_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    /*
     *  Check if instrument locked
     */
/*
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }
*/

    if( ( pInsFile_req->datFile == NULL ) || 
        ( pInsFile_req->datFile[0] == '\0' ) )
    {
        camp_appendMsg( "campsrv_inssave: invalid: filename %s", 
                    pInsFile_req->datFile );
        pRes->status = CAMP_INVAL_FILE;
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    if( pInsFile_req->flag & (1<<1) )
    {
        pRes->status = campDef_write( pInsFile_req->datFile, pVar );
        if( _failure( pRes->status ) )
        {
            if( camp_debug > 1 )
            {
                camp_appendMsg( "campsrv_inssave: failure: campDef_write %s",
                        pInsFile_req->datFile );
            }
            pRes->status = CAMP_FAILURE;
            release_ins_thread_lock( pVar );
            return( pRes );
        }
    }
    else
    {
        pRes->status = campIni_write( pInsFile_req->datFile, pVar );
        if( _failure( pRes->status ) )
        {
            if( camp_debug > 1 )
            {
                camp_appendMsg( "campsrv_inssave: failure: campIni_write %s",
                        pInsFile_req->datFile );
            }
            pRes->status = CAMP_FAILURE;
            release_ins_thread_lock( pVar );
            return( pRes );
        }
    }

    /*
     *  Set the ins file name
     */
    if( pInsFile_req->flag & (1<<0) )
    {
        _free( pIns->iniFile );
        pIns->iniFile = strdup( pInsFile_req->datFile );
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insifset_13( INS_IF_req* pInsIF_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char* path;
    bool_t wasOnline;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsIF_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( pInsIF_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    /*
     *  If instrument is online, turn it offline
     *  while setting the interface
     */
    wasOnline = FALSE;
    if( pIns->pIF != NULL )
    {
        if( wasOnline = ( pIns->pIF->status & CAMP_IF_ONLINE ) )
        {
            pRes->status = camp_insOffline( pVar );
            if( _failure( pRes->status ) )
            {
                if( camp_debug > 1 )
                {
                    camp_appendMsg( "campsrv_insifset: failure: camp_insOffline %s",
                            pVar->core.path );
                }
                release_ins_thread_lock( pVar );
                return( pRes );
            }
        }
    }

    /*
     *  Set the interface
     */
    pRes->status = camp_ifSet( &pIns->pIF, &pInsIF_req->IF );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insifset: failure: camp_ifSet %s %s",
                    pVar->core.path, pInsIF_req->IF.typeIdent );
        }
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    /*
     *  If instrument had been online, turn it back online
     */
    if( wasOnline )
    {
        pRes->status = camp_insOnline( pVar );
        if( _failure( pRes->status ) )
        {
            if( camp_debug > 1 )
            {
                camp_appendMsg( "campsrv_insifset: failure: camp_insOnline %s",
                        pVar->core.path );
            }
            release_ins_thread_lock( pVar );
            return( pRes );
        }
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insifread_13( INS_READ_req* pInsRead_req, struct svc_req* rq )
{
    RES* pRes;
    char* buf = NULL;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    int read_len = 0;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsRead_req->dreq.path;

    /*
     *  Get pointer to var
     */
    pVar = camp_varGetp( pInsRead_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
/*
    if( ( pInsType = camp_sysGetpInsType( pIns->typeIdent ) ) == NULL )
    {
        camp_appendMsg( "campsrv_insifread: invalid: instrument type %s",
                        pIns->typeIdent );
        pRes->status = CAMP_INVAL_INS_TYPE;
        release_ins_thread_lock( pVar );
        return( pRes );
    }
*/
    buf = (char*)zalloc( pInsRead_req->buf_len + 1 );

    pRes->status = camp_ifRead( pIns->pIF, pVar, 
                      pInsRead_req->cmd.cmd_val, 
                      pInsRead_req->cmd.cmd_len, 
                      buf, pInsRead_req->buf_len, &read_len );
    if( _failure( pRes->status ) ) 
    {
        free( buf );
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insifread: failure: camp_ifRead" );
        }
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;

    /*
     *  Set the CAMP msg string to the reading
     */
    camp_setMsg( buf );

    free( buf );

    return( pRes );
}


RES* 
campsrv_insifwrite_13( INS_WRITE_req* pInsWrite_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pInsWrite_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pInsWrite_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    pRes->status = camp_ifWrite( pIns->pIF, 
                       pVar, 
                       pInsWrite_req->cmd.cmd_val, 
                       pInsWrite_req->cmd.cmd_len );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insifwrite: failure: camp_ifWrite %s", path );
        }
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insifon_13( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVar_req->path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVar_req->path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    pRes->status = camp_ifOnline( pIns->pIF );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insifon: failure: camp_ifOnline %s", 
                            path );
        }
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_insifoff_13( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVar_req->path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVar_req->path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
    {
        camp_appendMsg( msg_wrong_type, path );
        pRes->status = CAMP_INVAL_INS;
        return( pRes );
    }

    get_ins_thread_lock( pVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

    pRes->status = camp_ifOffline( pIns->pIF );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_insifoff: failure: camp_ifOffline %s", path );
        }
        release_ins_thread_lock( pVar );
        return( pRes );
    }

    release_ins_thread_lock( pVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varset_13( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVarSet_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Request setting of data
     */
    pRes->status = varSetReq( pVar, &pVarSet_req->spec );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varset: failure: varSetReq %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varread_13( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVar_req->path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVar_req->path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Request reading of data
     */
    pRes->status = varRead( pVar );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varread: failure: camp_varRead %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_vardoset_13( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVarSet_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Set the data
     */
    pRes->status = varSetSpec( pVar, &pVarSet_req->spec );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_vardoset: failure: varSetSpec %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varlnkset_13( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetTrueP( pVarSet_req->dreq.path );
    if( pVar == NULL ) 
    { 
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    if( pVar->core.varType != CAMP_VAR_TYPE_LINK )
    {
        pRes->status = CAMP_INVAL_VAR_TYPE;
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Set the data
     */
    pRes->status = varSetSpec( pVar, &pVarSet_req->spec );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varlnkset: failure: varSetSpec %s", 
                        path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varpoll_13( DATA_POLL_req* pVarPoll_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarPoll_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVarPoll_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Set the POLL parameters
     */
    pRes->status = varSetPoll( pVar, 
                                 pVarPoll_req->flag, 
                                 pVarPoll_req->pollInterval );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varpoll: failure: varSetPoll %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varalarm_13( DATA_ALARM_req* pVarAlarm_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarAlarm_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVarAlarm_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Set the ALARM parameters
     */
    pRes->status = varSetAlarm( pVar, 
                                   pVarAlarm_req->flag, 
                                   pVarAlarm_req->alarmAction );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varalarm: failure: varSetAlarm %s", 
                        path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varlog_13( DATA_LOG_req* pVarLog_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVarLog_req->dreq.path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVarLog_req->dreq.path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    /*
     *  Set the LOG parameters
     */
    pRes->status = varSetLog(  pVar, 
                                  pVarLog_req->flag, 
                                  pVarLog_req->logAction );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varlog: failure: varSetLog %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_varzero_13( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    CAMP_VAR* pVar;
    CAMP_VAR* pInsVar;
    char* path;

    pRes = (RES*)zalloc( sizeof( RES ) );

    path = pVar_req->path;

    /*
     *  Get pointer to data
     */
    pVar = camp_varGetp( pVar_req->path );
    if( pVar == NULL ) 
    { 
        camp_appendMsg( msg_inval_var, path );
        pRes->status = CAMP_INVAL_VAR;
        return( pRes );
    }

    pInsVar = varGetpIns( pVar );
    get_ins_thread_lock( pInsVar );

    /*
     *  Check if instrument locked
     */
    pRes->status = camp_checkInsLock( pInsVar, rq );
    if( _failure( pRes->status ) ) 
    {
        camp_appendMsg( msg_ins_locked, path );
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    pRes->status = varZeroStats( pVar );
    if( _failure( pRes->status ) ) 
    {
        if( camp_debug > 1 )
        {
            camp_appendMsg( "campsrv_varzero: failure: varZeroStats %s", path );
        }
        release_ins_thread_lock( pInsVar );
        return( pRes );
    }

    release_ins_thread_lock( pInsVar );

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


CAMP_VAR_res* 
campsrv_varget_13( DATA_GET_req* pVarGet_req, struct svc_req* rq )
{
    CAMP_VAR_res* pRes;
    CAMP_VAR* pVar;
    char* path;

    pRes = (CAMP_VAR_res*)zalloc( sizeof( CAMP_VAR_res ) );

    path = pVarGet_req->dreq.path;

    set_current_xdr_flag( pVarGet_req->flag );

    if( camp_pathAtTop( pVarGet_req->dreq.path ) )
    {
        pVar = pVarList;
        if( pVar == NULL )
        {
            pRes->pVar = NULL;
            pRes->status = CAMP_SUCCESS;
            return( pRes );
        }
    }
    else
    {
        pVar = camp_varGetp( pVarGet_req->dreq.path );
        if( pVar == NULL )
        {
            camp_appendMsg( msg_inval_var, path );
            pRes->status = CAMP_INVAL_VAR;
            return( pRes );
        }

        if( pVarGet_req->flag & CAMP_XDR_CHILD_LEVEL )
        {
            pVar = pVar->pChild;
            if( pVar == NULL )
            {
                camp_appendMsg( 
                  "campsrv: error: child variable of %s doesn't exist", path );
                pRes->status = CAMP_INVAL_VAR;
                return( pRes );
            }
        }
    }

    pRes->pVar = pVar;
    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


RES* 
campsrv_cmd_13( CMD_req* pCMD_req, struct svc_req* rq )
{
    RES* pRes;
    Tcl_Interp* interp = camp_tclInterp();

    pRes = (RES*)zalloc( sizeof( RES ) );

    /*
     *  Parse the command line using Tcl
     */
    set_global_mutex_noChange( TRUE );
    pRes->status = Tcl_Eval( interp, pCMD_req->cmd );
    set_global_mutex_noChange( FALSE );
    if( pRes->status == TCL_ERROR ) 
    {
        pRes->status = CAMP_FAILURE;
    }
    else 
    {
        pRes->status = CAMP_SUCCESS;
    }

    /*
     *  In the case of insIfRead and insIfReadVerify
     *  campMsg should be empty to properly return
     *  the reading here
     */
    camp_appendMsg( interp->result );
/*    pRes->msg = interp->result;
*/
    return( pRes );
}


