/*
 *  camp_srv_main.c - CAMP server main
 *
 *  Revision history:
 *   v1.1  20-Apr-1994  TW  drivers in server, IFs generalized
 *   v1.3  23-Apr-1995  TW  Use _POSIX_THREADS to make server multithreaded
 */

#include <stdio.h>
#include "timeval.h"
#include "camp_srv.h"
#ifdef VXWORKS
#include <rpc/rpcGbl.h>    /* for svc_fdset */
#endif /* VXWORKS */

/*
 *  Server polling definitions
 */
#define SRV_POLL_INTERVAL 1
#ifdef VMS
#include psldef
#define SRV_POLL_TIMER_ID 1
#endif

/*
 *  Auto save definitions
 */
#define AUTO_SAVE_INTERVAL 600
#define do_auto_save TRUE

/*
 *  Linked list head definitions
 */
CAMP_SYS* pSys = NULL;
CAMP_VAR* pVarList = NULL;

/*
 *  Misc. globals
 */
int camp_debug = 0;

extern void camp_srv_13(struct svc_req *rqstp, SVCXPRT *transp);
extern void srv_init_thread_once( void );

#ifdef MULTINET
extern void svc_run_once( void );  /* does it return void? */
#else
static void svc_run_once( void );
#endif
static int srv_init( void );
static void srv_loop_thread( void );
static void srv_loop( void );


#ifdef VXWORKS
int 
campSrv( int camp_debug_on )
#else /* !VXWORKS */
int 
main( int argc, char* argv[] )
#endif /* VXWORKS */
{
    int status;
    SVCXPRT* svc_transport;
#ifdef MULTITHREADED
    pthread_once_t t_init_once_block = pthread_once_init;
#endif /* MULTITHREADED */

#ifdef VXWORKS
    camp_debug = camp_debug_on;
#else
    if( ( argc > 1 ) && ( streq( argv[1], "-d" ) ) )
    {
        camp_debug = 1;
    }

    if( ( argc > 2 ) )
    {
        camp_debug = atoi( argv[2] );
    }
#endif /* VXWORKS */

#ifdef VXWORKS
    /*
     *  Make sure global linked lists are freed in case
     *  CAMP Server was stopped uncleanly
     */
    _xdr_free( xdr_CAMP_VAR, pVarList );
    _xdr_free( xdr_CAMP_SYS, pSys );

/*    stdioInit(); */
/*    selectInit(); */
/*    taskVarInit(); */
    rpcTaskInit();
#endif /* VXWORKS */

    svc_transport = svctcp_create( RPC_ANYSOCK, 0, 0 );
    if( svc_transport == NULL )
    {
        fprintf( stderr, "failure: svctcp_create\n" );
        exit( CAMP_FAILURE );
    }

    pmap_unset( CAMP_SRV, CAMP_SRV_VERS );

    if( !svc_register( svc_transport, CAMP_SRV, CAMP_SRV_VERS, camp_srv_13, 
                       IPPROTO_TCP)) 
    {
        fprintf( stderr, "failure: svc_register\n" );
        exit( CAMP_FAILURE );
    }

#ifndef MULTITHREADED
    /* 
     *  Initialize exit handler
     */
    atexit( srv_rundown );
#endif /* MULTITHREADED */

    /*
     *  Global symbol to indicate exit status to shell
     */
#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "FAILURE", 1 );
#endif /* VMS */

    /*
     *  Direct messages to log file
     */
    status = camp_startLog( CAMP_SRV_LOG_FILE, TRUE );
    if( _failure( status ) )
    {
        fprintf( stderr, "failed to open log file\n" );
    }

#ifdef MULTITHREADED
    /* 
     *  Thread initialization
     *  sets up thread key data etc.
     */
    pthread_once( &t_init_once_block, srv_init_thread_once );

    /*
     *  Initialize the main thread.
     */
    status = srv_init_main_thread();
    if( _failure( status ) )
    {
        exit( status );
    }
#endif /* MULTITHREADED */

    /*
     *  Initialize server
     */
    srv_init();

    /*
     *  Go into server loop
     */
    srv_loop_thread();

    fprintf( stderr, "srv_loop_thread returned\n" );
    exit( CAMP_FAILURE );
}


static void
srv_loop_thread( void )
{
    for( ;; )
    {
	srv_loop();

	thread_lock_global_np();
        svc_run_once();
	thread_unlock_global_np();

        /*
         *  See if we should shutdown
         */
        if( check_shutdown() ) break;

        camp_fsleep( 0.1 );    /* 100ms */
    }
    srv_rundown();
    exit( 0 );
}


/*
 *  MultiNet has it's own "svc_run_once"
 *  (found by analyzing multinet rpc.olb (svc.obj))
 *  This is my version for Unix, etc.
 */
#ifndef MULTINET
static void
svc_run_once( void )
{
    int dtbsz;
    timeval_t tv = { 0, 0 };  /* poll */
    fd_set readfds;
#ifdef DEFUNCT_MULTINET
    /*
     *  Can't get select to work with MultiNet
     */
    extern SVCXPRT* svc_transport;
    fd_set svc_fdset;

    FD_ZERO( &svc_fdset );
    FD_SET( svc_transport->xp_sock, &svc_fdset );
#endif /* DEFUNCT_MULTINET */

#ifdef VXWORKS
#define getdtablesize()  FD_SETSIZE   /* check the maximum */
#define svc_fdset        taskRpcStatics->svc.svc_fdset
#endif /* VXWORKS */

    dtbsz = getdtablesize();

        readfds = svc_fdset;
        switch( select( dtbsz, &readfds, (int*)0, (int*)0, &tv ) )
        {
          case -1:
            if( errno == EINTR ) break;
            camp_logMsg( "svc: failed select" );
            break;
          case 0:
            break;
          default:
	    svc_getreqset( &readfds );
            break;
        }
}
#endif /* !MULTINET */


static int
srv_init( void )
{
    int status;
    RES* pRes;
    FILE_req req;

    /* 
     *  Tcl interpreter initialization
     */
    status = camp_tclInit();
    if( _failure( status ) )
    {
        camp_logMsg( camp_getMsg() );
        return( status );
    }

    /* 
     *  System structure initialization
     */
    status = sys_init();
    if( _failure( status ) ) 
    {
        camp_appendMsg( "srv_init: failure: sys_init" );
        camp_logMsg( camp_getMsg() );
        exit( status );
    }

    if( camp_isMsg() ) camp_logMsg( camp_getMsg() );
    camp_setMsg( "" );

    if( do_auto_save )
    {
        bzero( &req, sizeof( req ) );
        req.flag = 0;
        req.filename = CAMP_SRV_AUTO_SAVE;

        /* 
         *  Careful - some routines (reading/writing instruments)
         *            expect the global thread lock to be ON
         */
#ifdef MULTITHREADED
        thread_lock_global_np();
#endif /* MULTITHREADED */

        pRes = campsrv_sysload_13( &req, NULL );

#ifdef MULTITHREADED
        thread_unlock_global_np();
#endif /* MULTITHREADED */

        _free( pRes );

        if( camp_isMsg() ) camp_logMsg( camp_getMsg() );
        camp_setMsg( "" );
    }

    return( CAMP_SUCCESS );
}


void 
srv_rundown( void )
{
#ifdef MULTITHREADED
   /*
    *  Stop all other threads
    */
    kill_all_threads();
    camp_logMsg( "all threads stopped" );
    printf( "all threads stopped\n" );
#endif /* MULTITHREADED */

    /*
     *  Unregister RPC service
     */
    svc_unregister( CAMP_SRV, CAMP_SRV_VERS );

    /*
     *  Clean up
     */
    srv_delAllInss();
    sys_free();
    camp_tclEnd();
    srv_end_thread();
    camp_logMsg( "shutdown complete" );
    printf( "shutdown complete" );
    camp_stopLog();
}


static void
srv_loop( void )
{
    static time_t time_last_auto_save = 0;
    time_t time_now;
    int status;

    /*
     *  process all pending polling requests 
     *  thread lock done inside
     */
    REQ_doPending();

    if( do_auto_save )
    {
        /*
         *  Set initial time_last_auto_save so that an auto save
         *  is not done at startup
         */
        if( time_last_auto_save == 0 )
        {
            time( &time_last_auto_save );
        }
    
        if( ( time( &time_now ) - time_last_auto_save ) > AUTO_SAVE_INTERVAL )
        {
            time_last_auto_save = time_now;
    
#ifdef MULTITHREADED
            thread_lock_global_np();
#endif /* MULTITHREADED */

            status = campCfg_write( CAMP_SRV_AUTO_SAVE );
	    if( _failure( status ) )
	      {
		camp_logMsg( "failed auto-save" );
	      }

#ifdef MULTITHREADED
            thread_unlock_global_np();
#endif /* MULTITHREADED */
        }
    }
}
