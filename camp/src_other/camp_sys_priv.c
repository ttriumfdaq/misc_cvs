/*
 *  camp_sys.c,  Utilities for CAMP System data
 */

#include <stdio.h>
#include <string.h>
#ifdef VMS
/* homemade version */
#include "dirent.h"
#else
#include <dirent.h>
#endif /* VMS */
#include "timeval.h"
#include "camp_srv.h"


void
sys_free( void )
{
    _xdr_free( xdr_CAMP_SYS, pSys );
}


int 
sys_init( void )
{
    int status;
    char str[64];
    char hostname[LEN_NODENAME+1];

    _xdr_free( xdr_CAMP_SYS, pSys );
    pSys = (CAMP_SYS*)zalloc( sizeof( CAMP_SYS ) );

    gethostname( hostname, LEN_NODENAME );
    stolower( hostname );  /* This is for string comparisons */
    pSys->hostname = strdup( hostname );

    sprintf( str, "CAMP v%d.%1d", CAMP_SRV_VERS/10, CAMP_SRV_VERS%10 );
    pSys->prgName = strdup( str );

    status = sys_initDyna();
    if( _failure( status ) ) 
    {
        camp_appendMsg( "sys_init: failure: sys_initDyna" );
        return( status );
    }

    /*
     *  An error returned from sys_update is non-fatal.
     *  This allows CAMP to start up even when there is
     *  an error from the initialization file.
     */
    status = sys_update();

    return( CAMP_SUCCESS );
}


int
sys_update( void )
{
    int status;
    Tcl_Interp* interp = camp_tclInterp();

    _xdr_free( xdr_INS_TYPE, pSys->pInsTypes );
    _xdr_free( xdr_INS_AVAIL, pSys->pInsAvail );
    _xdr_free( xdr_ALARM_ACT, pSys->pAlarmActs );
    _xdr_free( xdr_LOG_ACT, pSys->pLogActs );
    _xdr_free( xdr_CAMP_IF_t, pSys->pIFTypes );

    status = Tcl_EvalFile( interp, CAMP_SRV_INI );
    if( status == TCL_ERROR )
    {
        gettimeval( &pSys->pDyna->timeLastSysChange );

        camp_appendMsg( "an error occurred while reading file %s", 
                        CAMP_SRV_INI );
	camp_appendMsg( interp->result );
        return( CAMP_FAILURE );
    }

    gettimeval( &pSys->pDyna->timeLastSysChange );

    return( CAMP_SUCCESS );
}


int
sys_initDyna( void )
{
    _xdr_free( xdr_SYS_DYNAMIC, pSys->pDyna );
    pSys->pDyna = (SYS_DYNAMIC*)zalloc( sizeof( SYS_DYNAMIC ) );

    pSys->pDyna->cfgFile = strdup( "" );

    return( CAMP_SUCCESS );
}


int 
sys_initDir( char* filespec_in )
{
/*
    u_int i;
    char** filenameArray;
    u_int numFiles;
    char filename[LEN_FILENAME];
*/
    DIRENT** ppDirEnt;
    char filespec[LEN_FILENAME];
    char dirname[LEN_FILENAME];
    char pattern[LEN_FILENAME];
    DIR* pDir;
    struct dirent* pDirent;

    if( ( filespec_in == NULL ) || ( strlen( filespec_in ) == 0 ) )
    {
        camp_appendMsg( "sysDir: invalid filespec" );
        return( CAMP_INVAL_FILESPEC );
    }

    /*
     *  Free last list
     */
    _xdr_free( xdr_DIRENT, pSys->pDir );

    /*
     *  Prepend default search directory if no directory given
     */
    file_nodir( filespec_in, filespec );
    if( streq( filespec_in, filespec ) )
    {
        sprintf( filespec, "%s%s", CAMP_DAT_DIR, filespec_in );
    }
    else
    {
        strcpy( filespec, filespec_in );
    }

    /*
     *  Add filespec as first entry in list
     */
    ppDirEnt = &pSys->pDir;
    *ppDirEnt = (DIRENT*)zalloc( sizeof( DIRENT ) );
    (*ppDirEnt)->filename = strdup( filespec );
    ppDirEnt = &(*ppDirEnt)->pNext;

    /*
     *  Get the directory part and open
     */
    file_dir( filespec, dirname );

    pDir = opendir( dirname );
    if( pDir == NULL ) 
    {
        camp_appendMsg( "sysDir: invalid dir" );
        return( CAMP_INVAL_FILESPEC );
    }

    /*
     *  Determine the filespec pattern
     */
      /* basename --> basename_tw for linux */
    basename_tw( filespec, pattern );
    if( streq( pattern, "" ) )
    {
        strcpy( pattern, "*" );
    }

    /*
     *  Read the directory
     */
    
    for( pDirent = readdir( pDir ); 
         pDirent != NULL; 
         pDirent = readdir( pDir ) )
    {
        /*
         *  Add file if it matches the pattern
         */
        if( strmatch( pDirent->d_name, pattern ) )
	{
            *ppDirEnt = (DIRENT*)zalloc( sizeof( DIRENT ) );
            (*ppDirEnt)->filename = strdup( pDirent->d_name );
            ppDirEnt = &(*ppDirEnt)->pNext;
	}

#ifdef VMS
        /*
	 *  VMS implementation allocates dynamically
	 */
        free( pDirent );
#endif /* VMS */
    }

    /*
     *  Close the directory
     *  Free all resources related to the 
     *  directory stream.
     *  (except VMS implementation which 
     *  allocates dir entries dynamically)
     */
    closedir( pDir );

    return( CAMP_SUCCESS );
}


u_long
sys_getTypeInstance( char* typeIdent )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    u_long typeInstance;

    typeInstance = 1;

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;

        if( streq( typeIdent, pIns->typeIdent ) )
            typeInstance = _max( typeInstance, pIns->typeInstance + 1 );
    }

    return( typeInstance );
}


