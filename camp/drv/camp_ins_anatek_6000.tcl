CAMP_INSTRUMENT /~ -D -T "Anatek 6000 Series DC Power Supply" \
    -H "Anatek 6000 Series DC Power Supply" -d on \
    -initProc anatek6000_init \
    -deleteProc anatek6000_delete \
    -onlineProc anatek6000_online \
    -offlineProc anatek6000_offline 
  proc anatek6000_init { ins } {
	insSet /${ins} -if gpib 0.5 5 20 LF LF
    } 
  proc anatek6000_delete { ins } {
	insSet /${ins} -line off
    }
  proc anatek6000_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
    }
  proc anatek6000_offline { ins } { insIfOff /${ins} }
    CAMP_FLOAT /~/voltage -D -S -R -T "Output voltage" -d on -s on -r off \
	-units V -writeProc anatek6000_v_w -readProc anatek6000_v_r
      proc anatek6000_v_w { ins target } {
	if { $target < 0.0 } { return -code error "negative values not allowed" }
	set val [format "%.3f" $target]
	insIfWrite /${ins} "S,V$val,GO"
        varDoSet /${ins}/voltage -v $val
      }
      proc anatek6000_v_r { ins } {
	insIfReadVerify /${ins} "T" 32 /${ins}/voltage "%*c %*c %fV %*fA" 2
      }
    CAMP_FLOAT /~/current -D -S -R -T "Output current" -d on -s on -r off \
	-units A -writeProc anatek6000_c_w -readProc anatek6000_c_r
      proc anatek6000_c_w { ins target } {
	if { $target < 0.0 } { return -code error "negative values not allowed" }
	set val [format "%.3f" $target]
	insIfWrite /${ins} "S,C$val,GO"
        varDoSet /${ins}/current -v $val
      }
      proc anatek6000_c_r { ins } {
	insIfReadVerify /${ins} "T" 32 /${ins}/current "%*c %*c %*fV %fA" 2
      }
    CAMP_SELECT /~/mode -D -S -R -T "Voltage/current mode" -d on -s on -r off \
	-selections voltage current \
	-writeProc anatek6000_mode_w -readProc anatek6000_mode_r
      proc anatek6000_mode_w { ins target } {
	insIfWrite /${ins} [format "S,MD%1s,GO" [lindex {V C} $target]]
	varDoSet /${ins}/mode -v $target
      }
      proc anatek6000_mode_r { ins } {
	set buf [insIfRead /${ins} T 32]
        set n [scan $buf "%*c %c %*fV %*fA" mode]
	if { $n != 1 } { return -code error "error reading Anatek 6000 mode" }
	switch $mode {
	  V { varDoSet /${ins}/mode -v 0 }
	  C { varDoSet /${ins}/mode -v 1 }
	  default { return -code error "bad mode retured from Anatek 6000" }
	}
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc anatek6000_id_r
          proc anatek6000_id_r { ins } {
		set status [catch {insIfRead /${ins} "T" 32} buf]
		if { $buf == "" } {
		    set id 1
		    varDoSet /${ins}/setup/id -m "Assuming option 15"
		} else {
		    set n [scan $buf " %*c %*c %fV %fA" volt curr]
		    if { $n != 2 } { 
		      set id 0 
		    } else {
		      set id 1
	 	      varDoSet /${ins}/voltage -r on
	 	      varDoSet /${ins}/current -r on
	 	      varDoSet /${ins}/mode -r on
		      varDoSet /${ins}/setup/id -m "Option 16"
		    }
		}
		varDoSet /${ins}/setup/id -v $id
	    }
        CAMP_FLOAT /~/setup/voltage_max -D -S -T "Maximum programmable voltage" \
	    -d on -s on -units V -writeProc anatek6000_mv_w
          proc anatek6000_mv_w { ins target } {
	    if { $target < 0.0 } { return -code error "negative values not allowed" }
	    set val [format "%.3f" $target]
	    insIfWrite /${ins} "S,MXV$val,GO"
            varDoSet /${ins}/setup/voltage_max -v $val
          }
        CAMP_FLOAT /~/setup/current_max -D -S -T "Maximum programmable current" \
	    -d on -s on -units A -writeProc anatek6000_mc_w
          proc anatek6000_mc_w { ins target } {
	    if { $target < 0.0 } { return -code error "negative values not allowed" }
	    set val [format "%.3f" $target]
	    insIfWrite /${ins} "S,MXC$val,GO"
            varDoSet /${ins}/setup/current_max -v $val
          }
