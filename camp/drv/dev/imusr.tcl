proc ImusrStart { ins } {
  global $ins
  ImusrInitScalers $ins
  ImusrInitOutReg $ins
  ImusrInitPrimDev $ins
  varSet /${ins}/state -v paused
  ImusrSetOutRegClear $ins
  varSet /${ins}/prv/startTime -v [gettime]
  varSet /${ins}/prv/currTime -v [varGetVal /${ins}/prv/startTime]
  ImusrArchiveStart $ins
  ImusrDoSweeps $ins
  ImusrArchiveEnd $ins
  varSet /${ins}/state -v stopped
}
proc ImusrDoSweeps { ins } {
  global $ins
  varSet /${ins}/prv/numPoints -v 0
  set sign [expr (([set ${ins}(endSP)]-[set ${ins}(startSP)]) > 0)]
  varSet /${ins}/prv/sweepSign -v $sign
  varSet /${ins}/prv/sweep -v 0
  varSet /${ins}/data/devs/sp0 -v [set ${ins}(startSP)]
  for {} { [set ${ins}(sweep)] < [set ${ins}(sweeps)] } {} {
    if {[set ${ins}(startSP)]<[set ${ins}(endSP)]} {
      set min [set ${ins}(startSP)]; set max [set ${ins}(endSP)]
    } else {
      set max [set ${ins}(startSP)]; set min [set ${ins}(endSP)]
    }
    for {} {([set ${ins}(sp0)>$min])&&([set ${ins}(sp0)]<$max)} {} {
      ImusrSetPrimDev $ins [set ${ins}(sp0)]
      sleep [set ${ins}(stepDelay)]
      ImusrClearSumScalers $ins
      for {set t 0} {$t<[set ${ins}(toggles)]} {incr t} {
        varSet /${ins}/prv/toggle -v $t
        ImusrDoToggle $ins
      }
      varSet /${ins}/prv/numPoints -v [set ${ins}(numPoints)+1]
      ImusrArchivePoint $ins
      if { [set ${ins}(requestState)] == 0 } {
        return 0
      }
      varSet /${ins}/data/devs/sp0 -v \
        [expr [set ${ins}(sp0)]+([set ${ins}(sweepSign)]*[set ${ins}(stepSP)])]
    }
    varSet /${ins}/data/devs/sp0 -v \
      [expr [set ${ins}(sp0)]-([set ${ins}(sweepSign)]*[set ${ins}(stepSP)])]
    varSet /${ins}/prv/sweepSign -v [expr -1*[set ${ins}(sweepSign)]]
    varSet /${ins}/prv/sweep -v [expr [set ${ins}(sweep)]+1]
  }
}
proc ImusrDoToggle { ins } {
  global $ins
  for {set i -1} {$i <= 1} {incr i 2} {
    if { [set ${ins}(softToggle)] } {
      ImusrSetPrimDev $ins [expr [set ${ins}(sp0)]+$i*[set ${ins}(toggleVal)]]
      sleep [set ${ins}(toggleDelay)]
    } else {
      ImusrSetOutRegModulation $ins [expr ($i==1)+1]
      sleep [set ${ins}(toggleDelay)]
      ImusrSetOutRegModulation $ins 0
    }
    for {set p 0} {$p < [set ${ins}(presets)]} {incr p} {
      varSet /${ins}/prv/preset -v $p
      ImusrDoPreset $ins
    }
  }
}
proc ImusrDoPreset { ins } {
  global $ins
 set done 0
 while {!$done} {
  varSet /${ins}/prv/initPauseTime -v [set ${ins}(currTime)]
  ImusrClearScalers $ins
  ImusrSetOutRegStart $ins 1
  ImusrWaitForCounts $ins
  ImusrSetOutRegStart $ins 0
  sleep 0.001
  ImusrSetOutRegStop $ins 1
  sleep 0.001
  ImusrSetOutRegStop $ins 0
  varSet /${ins}/prv/currTime -v [gettime]
  if { [set ${ins}(requestState)] == 2 } {
    varSet /${ins}/prv/totPauseTime -v \
      [expr [set ${ins}(totPauseTime)]+[set ${ins}(currTime)]- \
            [set ${ins}(initPauseTime)]]
    varSet /${ins}/state -v 2
  } elseif { ![ImusrCheckTolerance $ins] } {
    varSet /${ins}/prv/totPauseTime -v \
      [expr [set ${ins}(totPauseTime)]+[set ${ins}(currTime)]- \
            [set ${ins}(initPauseTime)]]
  } else {
    set done 1
  }
 }
 ImusrReadScalers $ins
}
proc ImusrCheckTolerance { ins } {
  global $ins
  set tol [expr [set ${ins}(tolerance)]/100.0]
  if { $tol == 0 } {
    varSet /${ins}/state -v 1
    return 1
  }
  set ticksNormal [set ${ins}(ticksNormal)]
  set ticks [set ${ins}(ticks)]
  set inTol [expr (($tol*$ticksNormal - abs($ticksNormal-$ticks)) > 0)]
  if { !$inTol } {
    varSet /${ins}/prv/lastInTol -v 0
    varSet /${ins}/state -v 2
    return 0
  }
  if { ![set ${ins}(lastInTol)] } {
    varSet /${ins}/prv/lastInTol -v 1
    sleep [set ${ins}(inTolDelay)]
    varSet /${ins}/state -v 1
    return 0
  }
  if { [set ${ins}(state)] != 1 } {
    varSet /${ins}/state -v 1
    return 0
  }
  return 1
}
proc ImusrInitScalers { ins } {
  ImusrClearScalers $ins
}
proc ImusrClearSumScalers { ins } {
  global $ins
  for {set i 0} {$i < 6} {incr i} {
    varSet /${ins}/data/eScaler/ecount${i} -v 0
    varSet /${ins}/data/eScaler/etotal${i} -v 0
    varSet /${ins}/data/aScaler/acount${i} -v 0
    varSet /${ins}/data/aScaler/atotal${i} -v 0
  }
}
proc ImusrReadScalers { ins } {
  global $ins
  set eslot [set ${ins}(eScalerSlot)]
  set enum [set ${ins}(numEScalers)]
  for {set i 0} {$i < $enum} {incr i} {
    varSet /${ins}/data/eScaler/ecount${i} -v [ScalerRead 0 0 $eslot $i]
    varSet /${ins}/data/eScaler/etotal${i} \
      -v [expr [set ${ins}(etotal${i})]+[set ${ins}(ecount${i})]]
  }
  set aslot [set ${ins}(aScalerSlot)]
  set anum [set ${ins}(numAScalers)]
  for {set i 0} {$i < $anum} {incr i} {
    varSet /${ins}/data/aScaler/acount${i} -v [ScalerRead 0 0 $aslot $i]
    varSet /${ins}/data/aScaler/atotal${i} \
      -v [expr [set ${ins}(atotal${i})]+[set ${ins}(acount${i})]]
  }
}
proc ImusrClearScalers { ins } {
  global $ins
  set eslot [set ${ins}(eScalerSlot)]
  set enum [set ${ins}(numEScalers)]
  for {set i 0} {$i < $enum} {incr i} {
    ScalerClear 0 0 $eslot $i
  }
  set aslot [set ${ins}(aScalerSlot)]
  set anum [set ${ins}(numAScalers)]
  for {set i 0} {$i < $anum} {incr i} {
    ScalerClear 0 0 $aslot $i
  }
}
proc ImusrInitPrimDev { ins } {
  global $ins
  set insType [set ${ins}(insType)]
  set primDevType [varSelGetValLabel /${ins}/setup/devs/primDev]
  set name ${ins}_${primDevType}
  # Make sure instrument of correct type exists
  if {![varExists /${name}]} {
    insAdd $insType $name
    if {![varExists /${name}]} {return 0}
  } else {
    if { [insGetTypeIdent /${name}] != $insType } {
      insDel /${name}
      insAdd $insType $name
      if {![varExists /${name}]} {return 0}
    }
  }
  # Make sure it's online
  if {!([insGetIfStatus /${name}]&$CAMP_IF_ONLINE)} {
    insSet /${name} -if_mod [set ${ins}(ifDefn)] -online
    # Give it a chance to come online
    sleep 5
    if {!([insGetIfStatus /${name}]&$CAMP_IF_ONLINE)} { return 0 }
  }
  # Special attention
  switch { $primDevType } {
    mag_ps {
      # mag_ps: Set ramp status to ramping
      varSet /${name}/ramp_status -v 1
    }
    mag_dac {}
    rf {}
    temp {}
  }
}
proc ImusrSetPrimDev { ins target } {
  global $ins
  set type [varSelGetValLabel /${ins}/setup/devs/primDev]
  set name ${ins}_${type}
  switch { $type } {
    mag_ps  {set var /${name}/fast_set}
    mag_dac {set var /${name}/dac_set}
    rf      {set var /${name}/freq_set}
    temp    {set var /${name}/control_set}
  }
  varSet $var -v $target
}
proc ImusrWaitForCounts( ins ) {
  global $ins
  set renormalize [set ${ins}(renormalize)]
  if {$renormalize} {
    set startTime [gettime]
  } else {
    set tol [expr [set ${ins}(tolerance)]/100.0]
    set tolTicks [expr (1+$tol)*[set ${ins}(ticksNormal)]]
    sleep [expr [set ${ins}(countsTime)]-0.01]
  }
  set timeout 0
  set eslot [set ${ins}(eScalerSlot)]
  set clockScaler [set ${ins}(clockScaler)]
  set newTicks [ScalerRead 0 0 $eslot $clockScaler]
  set oldTicks -1
  while {$oldTicks < $newTicks} {
    set oldTicks $newTicks
    sleep 0.05  # was 20 ms in original
    set newTicks [ScalerRead 0 0 $eslot $clockScaler]
    if {!$renormalize && ($newTicks > $tolTicks)} {set timeout 1; break}
  }
  varSet /${ins}/ticks -v $newTicks
  if {$renormalize} {
    varSet /${ins}/ticksNormal -v $newTicks
    varSet /${ins}/countsTime -v [expr [gettime]-$startTime]
  }
  return [expr !$timeout]
}
proc ImusrArchiveStart { ins } {
  #
  #  Write header
  #  Will this be necessary?
  #
}
proc ImusrArchivePoint { ins } {
  global $ins
  set enum [set ${ins}(numEScalers)]
  set anum [set ${ins}(numAScalers)]
  set dnum [set ${ins}(numDevs)]
  set num [set ${ins}(numPoints)]
  set ind [expr $num-1]
  #
  #  Check array sizes, resize if necessary (add 256 to array size)
  #
  set old_num [varArrGetTotElem /${ins}/data/eScaler/ecount0]
  if { $old_num < $num } {
    for {set i 0} {$i < $enum} {incr i} {
      varArrResize /${ins}/data/eScaler/eArr${i} {[expr $old_num+256]}
    }
    for {set i 0} {$i < $anum} {incr i} {
      varArrResize /${ins}/data/aScaler/aArr${i} {[expr $old_num+256]}
    }
    for {set i 0} {$i < $dnum} {incr i} {
      varArrResize /${ins}/data/devs/spArr${i} {[expr $old_num+256]}
    }
  }
  for {set i 0} {$i < $enum} {incr i} {
    varArrSetVal /${ins}/data/eScaler/eArr${i} {$ind} [set ${ins}(etotal${i})]
  }
  for {set i 0} {$i < $anum} {incr i} {
    varArrSetVal /${ins}/data/aScaler/aArr${i} {$ind} [set ${ins}(atotal${i})]
  }
  #
  #  To do: get actual set point from CAMP instrument
  #
  for {set i 0} {$i < $dnum} {incr i} {
    varArrSetVal /${ins}/data/devs/spArr${i} {$ind} [set ${ins}(sp${i})]
  }
  #
  #  To do: write to disk
  #
}
proc ImusrArchiveEnd { ins } {
  #
  #  Rewrite header with acq end time
  #  Will this be necessary?
  #
}

