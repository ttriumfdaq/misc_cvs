CAMP_INSTRUMENT /~ -D -T "I-MuSR data acquisition" -d on \
    -initProc imusr_init -deleteProc imusr_delete
    -onlineProc imusr_online -offlineProc imusr_offline
proc imusr_init { ins } {
  global ${ins}
  # 
  set ${ins}(ticks) 0
  set ${ins}(ticksNormal) 0
  set ${ins}(renormalize) 1
  set ${ins}(countsTime) 0.0
  # setup/
  set ${ins}(sweeps) 0
  set ${ins}(toggles) 0
  set ${ins}(presets) 0
  set ${ins}(startSP) 0.0
  set ${ins}(endSP) 0.0
  set ${ins}(stepSP) 0.0
  set ${ins}(toggleDelay) 0.0
  set ${ins}(stepDelay) 0.0
  set ${ins}(inTolDelay) 0.0
  set ${ins}(tolerance) 0.0
  set ${ins}(softToggle) 0
  set ${ins}(toggleVal) 0.0
  # setup/camac/
  set ${ins}(outregType) 0
  set ${ins}(outregSlot) 0
  set ${ins}(eScalerType) 0
  set ${ins}(numEScalers) 0
  set ${ins}(eScalerSlot) 0
  set ${ins}(forwPScaler) 0
  set ${ins}(forwMScaler) 0
  set ${ins}(backPScaler) 0
  set ${ins}(backMScaler) 0
  set ${ins}(totalScaler) 0
  set ${ins}(clockScaler) 0
  set ${ins}(aScalerType) 0
  set ${ins}(numAScalers) 0
  set ${ins}(aScalerSlot) 0
  # setup/devs/
  set ${ins}(numDevs) 0
  set ${ins}(primDev) 0
  set ${ins}(insType) ""
  set ${ins}(ifDefn) ""
  # escaler/
  set ${ins}(ecount0) 0
  set ${ins}(ecount1) 0
  set ${ins}(ecount2) 0
  set ${ins}(ecount3) 0
  set ${ins}(ecount4) 0
  set ${ins}(ecount5) 0
  set ${ins}(etotal0) 0
  set ${ins}(etotal1) 0
  set ${ins}(etotal2) 0
  set ${ins}(etotal3) 0
  set ${ins}(etotal4) 0
  set ${ins}(etotal5) 0
  # ascaler/
  set ${ins}(acount0) 0
  set ${ins}(acount1) 0
  set ${ins}(acount2) 0
  set ${ins}(acount3) 0
  set ${ins}(acount4) 0
  set ${ins}(acount5) 0
  set ${ins}(atotal0) 0
  set ${ins}(atotal1) 0
  set ${ins}(atotal2) 0
  set ${ins}(atotal3) 0
  set ${ins}(atotal4) 0
  set ${ins}(atotal5) 0
  # prv/
  set ${ins}(outregData) 0
}
proc imusr_delete { ins } {
  global ${ins}
  unset ${ins}
}
proc imusr_online { ins } {
  insIfOn /${ins}
}
proc imusr_offline { ins } {
  insIfOff /${ins}
}
CAMP_SELECT /~/requestState -D -S -d on -s on -setProc requestState_s \
  -selections stopped running paused
proc requestState_s { ins target } { 
  Root_s requestState $ins $target
  if { ${ins}(state) == 0 } {
    ImusrStart $ins
  }
}
CAMP_SELECT /~/state -D -d on -setProc {Root_s state} \
  -selections stopped running paused
CAMP_INT /~/ticks -D -d on -setProc {Root_s ticks}
CAMP_INT /~/ticksNormal -D -d on -setProc {Root_s ticksNormal}
CAMP_SELECT /~/renormalize -D -d on -setProc {Root_s renormalize} \
  -selections done requested
CAMP_FLOAT /~/countsTime -D -d on -setProc {Root_s countsTime}
CAMP_STRUCT /~/setup -D -T "Setup parameters" -d on
  CAMP_INT /~/setup/sweeps -D -S -d on -s on -setProc {Setup_s sweeps}
  CAMP_INT /~/setup/toggles -D -S -d on -s on -setProc {Setup_s toggles}
  CAMP_INT /~/setup/presets -D -S -d on -s on -setProc {Setup_s presets}
  CAMP_FLOAT /~/setup/startSP -D -S -d on -s on -setProc {Setup_s startSP}
  CAMP_FLOAT /~/setup/endSP -D -S -d on -s on -setProc {Setup_s endSP}
  CAMP_FLOAT /~/setup/stepSP -D -S -d on -s on -setProc {Setup_s stepSP}
  CAMP_FLOAT /~/setup/toggleDelay -D -S -d on -s on -setProc {Setup_s toggleDelay}
  CAMP_FLOAT /~/setup/stepDelay -D -S -d on -s on -setProc {Setup_s stepDelay}
  CAMP_FLOAT /~/setup/inTolDelay -D -S -d on -s on -setProc {Setup_s inTolDelay}
  CAMP_FLOAT /~/setup/tolerance -D -S -d on -s on -setProc {Setup_s tolerance}
  CAMP_SELECT /~/setup/softToggle -D -S -d on -s on -setProc {Setup_s softToggle}
  CAMP_FLOAT /~/setup/toggleVal -D -S -d on -s on -setProc {Setup_s toggleVal}
  CAMP_STRUCT /~/setup/camac -D -T "CAMAC hardware" -d on
   CAMP_SELECT /~/setup/camac/outregType -D -S -d on -s on \
       -setProc outregType_s -selections nd027
   proc outregType_s { ins target } { 
     Camac_s outregType $ins $target
     set label [varSelGetValLabel /${ins}/setup/camac/outregType]
     source "drv/imusr_outreg_${label}.tcl"
   }
   CAMP_INT /~/setup/camac/outregSlot -D -S -d on -s on -setProc {Camac_s outregSlot}
   CAMP_SELECT /~/setup/camac/eScalerType -D -S -d on -s on \
       -setProc eScalerType_s -selections ks3615
   proc eScalerType_s { ins target } { 
     Camac_s eScalerType $ins $target
     set label [varSelGetValLabel /${ins}/setup/camac/eScalerType]
     source "drv/scaler_${label}.tcl"
   }
   CAMP_INT /~/setup/camac/numEScalers -D -S -d on -s on -setProc {Camac_s numEScalers}
   CAMP_INT /~/setup/camac/eScalerSlot -D -S -d on -s on -setProc {Camac_s eScalerSlot}
   CAMP_INT /~/setup/camac/forwPScaler -D -S -d on -s on -setProc {Camac_s forwPScaler}
   CAMP_INT /~/setup/camac/forwMScaler -D -S -d on -s on -setProc {Camac_s forwMScaler}
   CAMP_INT /~/setup/camac/backPScaler -D -S -d on -s on -setProc {Camac_s backPScaler}
   CAMP_INT /~/setup/camac/backMScaler -D -S -d on -s on -setProc {Camac_s backMScaler}
   CAMP_INT /~/setup/camac/totalScaler -D -S -d on -s on -setProc {Camac_s totalScaler}
   CAMP_INT /~/setup/camac/clockScaler -D -S -d on -s on -setProc {Camac_s clockScaler}
   CAMP_SELECT /~/setup/camac/aScalerType -D -S -d on -s on \
       -setProc aScalerType_s -selections ks3615
   proc aScalerType_s { ins target } { 
     Camac_s aScalerType $ins $target
     set label [varSelGetValLabel /${ins}/setup/camac/aScalerType]
     source "drv/imusr_scaler_${label}.tcl"
   }
   CAMP_INT /~/setup/camac/numAScalers -D -S -d on -s on -setProc {Camac_s numAScalers}
   CAMP_INT /~/setup/camac/aScalerSlot -D -S -d on -s on -setProc {Camac_s aScalerSlot}
  CAMP_STRUCT /~/setup/devs -D -T "Devices" -d on
   CAMP_INT /~/setup/devs/numDevs -D -S -d on -s on -setProc {SetupDevs_s numDevs}
   CAMP_SELECT /~/setup/devs/primDev -D -S -d on -s on \
       -setProc {SetupDevs_s primDev} -selections mag_ps mag_dac rf temp
   CAMP_STRING /~/setup/devs/insType -D -S -d on -s on -setProc {SetupDevs_s insType}
   CAMP_STRING /~/setup/devs/ifDefn -D -S -d on -s on -setProc {SetupDevs_s ifDefn}
  CAMP_STRUCT /~/data/eScaler -D -T "E Scalers" -d on
   CAMP_INT /~/data/eScaler/ecount0 -D -S -d on -s on -setProc {Escaler_s ecount0}
   CAMP_INT /~/data/eScaler/ecount1 -D -S -d on -s on -setProc {Escaler_s ecount1}
   CAMP_INT /~/data/eScaler/ecount2 -D -S -d on -s on -setProc {Escaler_s ecount2}
   CAMP_INT /~/data/eScaler/ecount3 -D -S -d on -s on -setProc {Escaler_s ecount3}
   CAMP_INT /~/data/eScaler/ecount4 -D -S -d on -s on -setProc {Escaler_s ecount4}
   CAMP_INT /~/data/eScaler/ecount5 -D -S -d on -s on -setProc {Escaler_s ecount5}
   CAMP_INT /~/data/eScaler/etotal0 -D -S -d on -s on -setProc {Escaler_s etotal0}
   CAMP_INT /~/data/eScaler/etotal1 -D -S -d on -s on -setProc {Escaler_s etotal1}
   CAMP_INT /~/data/eScaler/etotal2 -D -S -d on -s on -setProc {Escaler_s etotal2}
   CAMP_INT /~/data/eScaler/etotal3 -D -S -d on -s on -setProc {Escaler_s etotal3}
   CAMP_INT /~/data/eScaler/etotal4 -D -S -d on -s on -setProc {Escaler_s etotal4}
   CAMP_INT /~/data/eScaler/etotal5 -D -S -d on -s on -setProc {Escaler_s etotal5}
   CAMP_ARRAY /~/data/eScaler/eArr0 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/eScaler/eArr1 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/eScaler/eArr2 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/eScaler/eArr3 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/eScaler/eArr4 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/eScaler/eArr5 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
  CAMP_STRUCT /~/data/aScaler -D -T "A Scalers" -d on
   CAMP_INT /~/data/aScaler/acount0 -D -S -d on -s on -setProc {Ascaler_s acount0}
   CAMP_INT /~/data/aScaler/acount1 -D -S -d on -s on -setProc {Ascaler_s acount1}
   CAMP_INT /~/data/aScaler/acount2 -D -S -d on -s on -setProc {Ascaler_s acount2}
   CAMP_INT /~/data/aScaler/acount3 -D -S -d on -s on -setProc {Ascaler_s acount3}
   CAMP_INT /~/data/aScaler/acount4 -D -S -d on -s on -setProc {Ascaler_s acount4}
   CAMP_INT /~/data/aScaler/acount5 -D -S -d on -s on -setProc {Ascaler_s acount5}
   CAMP_INT /~/data/aScaler/atotal0 -D -S -d on -s on -setProc {Ascaler_s atotal0}
   CAMP_INT /~/data/aScaler/atotal1 -D -S -d on -s on -setProc {Ascaler_s atotal1}
   CAMP_INT /~/data/aScaler/atotal2 -D -S -d on -s on -setProc {Ascaler_s atotal2}
   CAMP_INT /~/data/aScaler/atotal3 -D -S -d on -s on -setProc {Ascaler_s atotal3}
   CAMP_INT /~/data/aScaler/atotal4 -D -S -d on -s on -setProc {Ascaler_s atotal4}
   CAMP_INT /~/data/aScaler/atotal5 -D -S -d on -s on -setProc {Ascaler_s atotal5}
   CAMP_ARRAY /~/data/aScaler/aArr0 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/aScaler/aArr1 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/aScaler/aArr2 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/aScaler/aArr3 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/aScaler/aArr4 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
   CAMP_ARRAY /~/data/aScaler/aArr5 -D -S -d on -s on -arrayDef CAMP_INT 4 1 {0}
  CAMP_STRUCT /~/data/devs -D -T "Devices" -d on
   CAMP_FLOAT /~/data/devs/sp0 -D -S -d on -s on -setProc {Devs_s sp0}
   CAMP_FLOAT /~/data/devs/sp1 -D -S -d on -s on -setProc {Devs_s sp1}
   CAMP_FLOAT /~/data/devs/sp2 -D -S -d on -s on -setProc {Devs_s sp2}
   CAMP_FLOAT /~/data/devs/sp3 -D -S -d on -s on -setProc {Devs_s sp3}
   CAMP_ARRAY /~/data/devs/spArr0 -D -S -d on -s on -arrayDef CAMP_FLOAT 4 1 {0}
   CAMP_ARRAY /~/data/devs/spArr1 -D -S -d on -s on -arrayDef CAMP_FLOAT 4 1 {0}
   CAMP_ARRAY /~/data/devs/spArr2 -D -S -d on -s on -arrayDef CAMP_FLOAT 4 1 {0}
   CAMP_ARRAY /~/data/devs/spArr3 -D -S -d on -s on -arrayDef CAMP_FLOAT 4 1 {0}
proc ImusrSet_s { ins target var path } {
  global ${ins}
  varDoSet /${ins}/${path} -v $target
  set ${ins}(${var}) [varGetVal /${ins}/${path}]
}
proc Root_s { var ins target } { 
  ImusrSet_s $ins $target $var $var 
}
proc Setup_s { var ins target } { 
  ImusrSet_s $ins $target $var setup/$var 
}
proc Camac_s { var ins target } { 
  ImusrSet_s $ins $target $var setup/camac/$var 
}
proc SetupDevs_s { var ins target } { 
  ImusrSet_s $ins $target $var setup/devs/$var
}
proc Escaler_s { var ins target } { 
  ImusrSet_s $ins $target $var data/eScaler/$var 
}
proc Ascaler_s { var ins target } { 
  ImusrSet_s $ins $target $var data/aScaler/$var 
}
proc Devs_s { var ins target } {
  ImusrSet_s $ins $target $var data/devs/$var
}
CAMP_STRUCT /~/prv -D -T "Private parameters" -d on



