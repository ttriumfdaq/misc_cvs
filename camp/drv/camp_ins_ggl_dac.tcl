# camp_ins_ggl_dac.tcl
# Purpose:  This tcl script defines the DAC device in the TRIUMF muSR Gate
#           Generator Logic (GGL) board.
# Inputs:   None
# Precond:  A functioning tcl interpreter with VME interface.
# Outputs:  Returns a result code 
# Postcond: The driver will be created and available for access by the system

CAMP_INSTRUMENT /~ -D -T "GGL DAC" -d on \
    -initProc ggl_dac_init -deleteProc ggl_dac_delete \
    -onlineProc ggl_dac_online -offlineProc ggl_dac_offline

  proc ggl_dac_init { ins } { 
    insSet /${ins} -if vme 0.0 0.0 32768
  }
  proc ggl_dac_delete { ins } {
      insSet /${ins} -line off
  }
  proc ggl_dac_online { ins } {
      set range [varGetVal /${ins}/dac_range]
      insIfOn /${ins}
      if { [catch {
          if { $range < 6 } {
              # Existing instrument: apply desired range and setting.
              varSet /${ins}/dac_range -v $range
              varSet /${ins}/dac_set -v [varGetVal /${ins}/dac_set]
          }
          # read all
          varRead /${ins}/dac_volts
      } mes ] } {
	  insIfOff /${ins}
	  return -code error "Failed to read DAC state. $mes"
      }
  }
  proc ggl_dac_offline { ins } {
      insIfOff /${ins}
  }

CAMP_INT /~/dac_set -D -S -R -P -L -T "Set DAC" \
    -d on -s on -r on -p off -p_int 5 \
    -readProc ggl_dac_read -writeProc ggl_dac_set_w

  proc ggl_dac_set_w { ins dac_set } {
      insIfWrite /${ins} "0x06 word ${dac_set}"
      ggl_dac_read "${ins}"
  }

CAMP_FLOAT /~/dac_volts -D -S -R -P -L -T "DAC Voltage" \
    -d on -s on -r on -p off -p_int 5 -units V \
    -readProc ggl_dac_read -writeProc ggl_dac_volts_w

  proc ggl_dac_volts_w { ins volts_set } {
      varRead /${ins}/dac_range
      if { 2 > [scan [varSelGetValLabel /${ins}/dac_range] {%f to %f} min max] } {
	  return -code error "Problem using DAC range (bug)"
      }
      set dac_set [expr { round(65535.0 * (double($volts_set - $min)/($max - $min))) }]
      varSet /${ins}/dac_set -v $dac_set
  }
  proc ggl_dac_read { ins } {
      varRead /${ins}/dac_range
      if { 2 > [scan [varSelGetValLabel /${ins}/dac_range] {%f to %f} min max] } {
	  return -code error "Problem using DAC range (bug)"
      }
      insIfReadVerify /${ins} "0x06 word" 15 /${ins}/dac_set {%d} 1
      set dac_set [varGetVal /${ins}/dac_set]
      set volts_set [expr { $min + (($dac_set / 65535.0) * ($max - $min)) } ]
      varDoSet /${ins}/dac_volts -v [format %.7f $volts_set] 
  }

CAMP_INT /~/dac_incr -D -S -P -T "Increment DAC" -d on -s on -p off \
        -H "Change DAC setting by this value; \"set\" to set increment; Poll to perform increment" \
        -readProc ggl_dac_incr_r -writeProc ggl_dac_incr_w

  proc ggl_dac_incr_w { ins target } {
      varDoSet /${ins}/dac_incr -v $target
  }
  proc ggl_dac_incr_r { ins } {
    set i [varGetVal /${ins}/dac_incr]
    set d [varGetVal /${ins}/dac_set]
    varSet /${ins}/dac_set -v [expr $i+$d]
  }

# Tricky selections!  After "-selections" all ensuing arguments that do not begin with "-" are
# treated as selection values.  Therefore, a selection value *cannot* begin with "-" (bleach)!
# Add an extra, blank, selection to end, to indicate when no actual selection has been made
# or read.
CAMP_SELECT /~/dac_range -D -R -S -P -T "Voltage range" \
    -d on -r on -s on -p off -p_int 99 -v 6 \
    -selections {    0 to +5  V} {    0 to +10 V}  {   -5 to +5  V} \
                {  -10 to +10 V} { -2.5 to +2.5 V} { -2.5 to +7.5 V} { } \
    -readProc ggl_dac_range_r -writeProc ggl_dac_range_w

  proc ggl_dac_range_w { ins target } {
      if { $target == 6 } { return }
      insIfWrite /${ins} "0x05 byte ${target}"
      varDoSet /${ins}/dac_range -v $target
  }
  proc ggl_dac_range_r { ins } {
      set b [insIfRead /${ins} "0x05 byte" 15]
      varDoSet /${ins}/dac_range -v [expr { $b & 7 } ]
  }
