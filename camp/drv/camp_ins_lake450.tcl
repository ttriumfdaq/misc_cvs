CAMP_INSTRUMENT /~ -D -T "LakeShore 450" \
    -H "LakeShore 450 Gaussmeter" -d on \
    -initProc lake450_init \
    -deleteProc lake450_delete \
    -onlineProc lake450_online \
    -offlineProc lake450_offline
  proc lake450_init { ins } {
	insSet /${ins} -if rs232 0.2 3 none 9600 7 odd 1 CRLF CRLF
       #insSet /${ins} -if gpib 0.5 5 11 LF LF
  }
  proc lake450_delete { ins } {
	insSet /${ins} -line off
	unset $ins
  }
  proc lake450_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
        }
  }
  proc lake450_offline { ins } {
	insIfOff /${ins}
  }

  CAMP_FLOAT /~/field -D -R -P -L -A -T "Field reading" \
      -d on -r on -a off -p off -p_int 10 -units "?" \
      -H "Read the magnetic field; also reads actual units" \
      -readProc lake450_field_r
    proc lake450_field_r { ins } {
        # Do not use insIfReadVerify because we want to apply software polarity reversal
	# insIfReadVerify /${ins} "FIELD?" 32 /${ins}/field " %f" 2
	set buf [insIfRead /${ins} "FIELD?" 32]
	if { [scan $buf " %f%c" val xxx] != 1 } {
	    set buf [insIfRead /${ins} "FIELD?" 32]
	    if { [scan $buf " %f%c" val xxx] != 1 } {
		return -code error "Failed to read field, $buf"
	    }
	}
	set pol [varGetVal /${ins}/setup/polarity]
	set val [expr { ($pol ? -$val : $val ) }]
	varDoSet /${ins}/field -v $val
	# Read the units every time in case we are on autorange
	varRead /${ins}/actual_units
	# Test for alarms
	varTestAlert /${ins}/field [varGetVal /${ins}/nominal_field]
    }

  CAMP_STRING /~/actual_units -D -R -T "units with multiplier" \
      -d on -r on \
      -H "Read actual value of units e.g. mT (with powers of 10 included)" \
      -readProc lake450_actunits_r
    proc lake450_actunits_r { ins } {
	set status  [catch {insIfRead /${ins} "UNIT?" 32} buf]
	set ustring fail
	if { $status == 0 } {
	    set status [scan $buf " %s" ustring]
	}
	set str " "
	set status  [catch {insIfRead /${ins} "FIELDM?" 32} buf]
	if { $status == 0 } {
	    set status [scan $buf " %s" str]
	}
	append str [string trim $ustring]
	set str [string trim $str]
	varDoSet /${ins}/actual_units -v $str
	varDoSet /${ins}/field -units $str
    }

  CAMP_SELECT /~/set_units -D -S -R -T "Select Units" \
      -d on -s on -r on \
      -H "Select magnetic field units (Gauss/Tesla) then read actual_units" \
      -selections G T \
      -readProc lake450_units_r \
      -writeProc lake450_units_w
    proc lake450_units_w { ins target } { 
	set units $target
	switch $units {
	  0 { insIfWrite /${ins} "UNIT G" }
	  1 { insIfWrite /${ins} "UNIT T" }
	}
	varDoSet /${ins}/set_units -v $units
	varRead /${ins}/actual_units 
    } 
    proc lake450_units_r { ins } {
	insIfReadVerify /${ins} "UNIT?" 32 /${ins}/set_units " %s" 2
    }

  CAMP_FLOAT /~/nominal_field -D -S -T "Expected field" \
      -d on -s on -units "-" \
      -H "Set expected nominal magnetic field (used for alarms)" \
      -writeProc lake450_nominal_w
    proc lake450_nominal_w { ins target } {
	varDoSet /${ins}/nominal_field -v $target
    }

  CAMP_STRUCT /~/setup -D -d on

    CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	-H "ID used to check if Lakeshore 450 is responding" \
	-selections FALSE TRUE \
	-readProc lake450_id_r 
      proc lake450_id_r { ins } {
	set id 0
	set status [catch {insIfRead /${ins} "*IDN?" 32} buf]
	if { $status == 0 } {
	    set id [scan $buf " LSCI,MODEL450,%*d,%d" val]
	    if { $id != 1 } { set id 0 }
	}
	varDoSet /${ins}/setup/id -v $id
      }

    CAMP_SELECT /~/setup/fieldcomp -D -S -R  \
	-T "Field compensation" -d on -s on -r on \
	-H "Field compensation table in probe ignored if set OFF"\
	-selections 0FF ON \
	-readProc lake450_fieldcomp_r \
	-writeProc lake450_fieldcomp_w 
      proc lake450_fieldcomp_w { ins target } { 
	insIfWrite /${ins} "FCOMP ${target}"
	varDoSet /${ins}/setup/fieldcomp -v $target
      } 
      proc lake450_fieldcomp_r { ins } {
	insIfReadVerify /${ins} "FCOMP?" 32 /${ins}/setup/fieldcomp " %s" 2
      }                    

    CAMP_SELECT /~/setup/tempcomp -D -S -R  \
	-T "Temp compensation" -d on -s on -r on \
	-H "Temperature compensation table in probe ignored if set OFF" \
	-selections 0FF ON \
	-readProc lake450_tempcomp_r \
	-writeProc lake450_tempcomp_w 
      proc lake450_tempcomp_w { ins target } { 
	insIfWrite /${ins} "TCOMP ${target}"
	varDoSet /${ins}/setup/tempcomp -v $target
      } 
      proc lake450_tempcomp_r { ins } {
	insIfReadVerify /${ins} "TCOMP?" 32 /${ins}/setup/tempcomp " %s" 2
      }
    CAMP_SELECT /~/setup/fast_data_mode -D -S -R -T "Fast Data Mode" \
	-d on -s on -r on \
	-H "Increases analog output update rate - disables front panel display" \
	-selections OFF ON \
	-readProc lake450_fast_r \
	-writeProc lake450_fast_w 
      proc lake450_fast_w { ins target } { 
	insIfWrite /${ins} "FAST ${target}"
	varDoSet /${ins}/setup/fast_data_mode -v $target
      } 
      proc lake450_fast_r { ins } {
	insIfReadVerify /${ins} "FAST?" 32 /${ins}/setup/fast_data_mode " %s" 2
      }

    CAMP_SELECT /~/setup/zero_probe -D -S -T "Zero Probe" \
	-d on -s on \
	-H "Place probe in Zero Gauss Chamber first, then enter command" \
	-writeProc lake450_zp_w 
      proc lake450_zp_w { ins } {
	insIfWrite /${ins} "ZCAL" 
      }

    CAMP_STRING /~/setup/probe_type -D -R  -T "Probe type" \
	-d on -r on \
	-H "Queries probe type " \
	-readProc lake450_type_r
       proc lake450_type_r { ins } {
	set status  [catch {insIfRead /${ins} "TYPE?" 32} buf]
	if { $status == 0 } {
	    set status [scan $buf " %s" str]
	}
	switch $str {
	    0 { set ustring "high sensitivity" }
	    1 { set ustring "high stability" }
	    2 { set ustring "ultra-high sensitivity" }
	    default { set ustring $str }
	}
	varDoSet /${ins}/setup/probe_type -v $ustring
      }

    CAMP_STRUCT /~/setup/AC_field  -D -d on

      CAMP_SELECT /~/setup/AC_field/acdc -D -S -R  \
	  -T "AC or DC mag. field" -d on -s on -r on \
	  -H "Configure for AC or DC; if select AC, also select PEAK/RMS" \
   	  -selections DC AC \
    	  -readProc lake450_acdc_r -writeProc lake450_acdc_w

	proc lake450_acdc_w { ins target } { 
	    insIfWrite /${ins} "ACDC ${target}"
	    varDoSet /${ins}/setup/AC_field/acdc -v $target
	} 
	proc lake450_acdc_r { ins } {
	    insIfReadVerify /${ins} "ACDC?" 32 /${ins}/setup/AC_field/acdc " %s" 2
	}

      CAMP_SELECT /~/setup/AC_field/peak_or_rms -D -S -R \
	  -T "Peak/Rms AC field" \
	  -H "Select peak/rms field; first select AC field" \
	  -d on -s on -r on \
	  -selections RMS PEAK \
	  -readProc lake450_peak_or_rms_r \
	  -writeProc lake450_peak_or_rms_w
	proc lake450_peak_or_rms_w { ins target } { 
	    insIfWrite /${ins} "PRMS ${target}"
	    varDoSet /${ins}/setup/AC_field/peak_or_rms -v $target
	} 
	proc lake450_peak_or_rms_r { ins } {
	    set status  [catch {insIfRead /${ins} "PRMS?" 32} buf]
	    if { $status == 0 } {
		set status [scan $buf " %s" str]
	    }
	    switch $str {
		0 { set ustring "RMS" }
		1 { set ustring "PEAK" }
		default { set ustring $str }
	    }
	    varDoSet /${ins}/setup/AC_field/peak_or_rms -v $ustring
	}

    CAMP_STRUCT /~/setup/range -D -d on

      CAMP_SELECT /~/setup/range/autorange -D -S -R \
	  -T "auto or manual range" \
	  -H "If autorange set to OFF, select desired range with select_range"\
	  -d on -s on -r on \
	  -selections OFF ON \
	  -readProc lake450_auto_r \
	  -writeProc lake450_auto_w
	proc lake450_auto_w { ins target } { 
	    insIfWrite /${ins} "AUTO ${target}"
	    varDoSet /${ins}/setup/range/autorange -v $target
	} 
	proc lake450_auto_r { ins } {
	    insIfReadVerify /${ins} "AUTO?" 32 /${ins}/setup/range/autorange " %s" 2
	}

      CAMP_SELECT /~/setup/range/select_range -D -S -R  \
	  -T "select manual range" \
	  -H "If autorange is OFF, select desired manual range for probe"\
	  -d on -s on -r on \
	  -selections highest higher lower lowest\
	  -readProc lake450_select_r \
	  -writeProc lake450_select_w
	proc lake450_select_w { ins target } { 
	    insIfWrite /${ins} "RANGE ${target}"
	    varDoSet /${ins}/setup/range/select_range -v $target
	} 
	proc lake450_select_r { ins } {
	    insIfReadVerify /${ins} "RANGE?" 32 /${ins}/setup/range/select_range " %s" 2
	}

      CAMP_STRING /~/setup/range/actual_range -D -R  \
	  -T "Actual range" -d on -r on \
	  -H "Read actual range for this probe (valid when Autorange is OFF)" \
	  -readProc lake450_actual_range_r
	proc lake450_actual_range_r { ins } {
	    # first see if we are on auto range or manual range
	    set ustring "unknown"
	    set status  [catch {insIfRead /${ins} "AUTO?" 32} buf]
	    if { $status == 0 } {
		set status [scan $buf " %s" str]
		set ustring $str
		if { [string compare "1" $str] == 0} { 
		    set ustring "autorange"
		} 
		if { [string compare "0" $str] == 0} { 
		    # autorange is OFF -  read the probe type first 
		    # (to determine the actual range used)
		    set status  [catch {insIfRead /${ins} "TYPE?" 32} buf]
		    if { $status == 0 } {
			set status [scan $buf " %s" str]
		    }
		    switch $str {
		      0 { set ustring "high sensitivity" 
			  set ranges [ list {+/- 30kG} {+/- 3kG} {+/- 300G} {+/- 30G} ]
			}
		      1 { set ustring "high stability" 
			  set ranges [ list {+/- 300kG} {+/- 30kG} {+/- 3kG} {+/- 300G} ]
			}
		      2	{ set ustring "ultra-high sensitivity" 
			  set ranges [ list {+/- 30G} {+/- 3G} {+/- 300mG} {not defined} ]
			}
		      default {
			  set ustring $str
			}
		    }
		    varDoSet /${ins}/setup/probe_type -v $ustring
		    # now read the Range itself
		    set status  [catch {insIfRead /${ins} "RANGE?" 32} buf]
		    if { $status == 0 } {
			set status [scan $buf " %s" str]
		    }
		    set ustring $str
		    catch { set ustring [lindex $ranges $str] }
		}
	    }   
	    varDoSet /${ins}/setup/range/actual_range -v $ustring
	}

	CAMP_SELECT /~/setup/polarity -D -S -T "Polarity" -d on -s on \
	    -selections NORMAL REVERSE -v 0 \
	    -writeProc lake450_polarity_w
          proc lake450_polarity_w { ins target } {
	      varDoSet /${ins}/setup/polarity -v $target
	  }

    CAMP_STRUCT /~/setup/filter -D -T "display filter" -d on
      CAMP_SELECT /~/setup/filter/display_filter -D -S -R \
	  -T "Display filtering" \
	  -H "If ON, quiets display depending on points & filter_window settings"\
	  -d on -s on -r on \
	  -selections OFF ON \
	  -readProc lake450_dfilt_r \
	  -writeProc lake450_dfilt_w
	proc lake450_dfilt_w { ins target } {
	    insIfWrite /${ins} "FILT ${target}"
	    varDoSet /${ins}/setup/filter/display_filter -v $target
	} 
	proc lake450_dfilt_r { ins } {
	    insIfReadVerify /${ins} "FILT?" 32 /${ins}/setup/filter/display_filter " %d" 2
	}

      CAMP_INT /~/setup/filter/no_of_points -D -R -S   \
	  -T "Set no. filter points" \
	  -m "min=2 max=64"    \
	  -units " " \
	  -H "No. Filter points; linear (2-8) or exponential (9-64)" \
	  -d on -r on -s on  \
	  -readProc lake450_points_r \
	  -writeProc lake450_points_w
	proc lake450_points_r { ins } {
	    insIfReadVerify /${ins} "FNUM?" 32 /${ins}/setup/filter/no_of_points " %d" 2
	}
	proc lake450_points_w { ins target } {
	    set points $target
	    if { $points < 2 } { set points 2 }
	    if { $points > 64 } { set points 64 }
	    set target [format "%2d" $points]
	    insIfWriteVerify /${ins} "FNUM $target" "FNUM?" 32 \
		/${ins}/setup/filter/no_of_points  " %d" 2  $target
	}

      CAMP_INT /~/setup/filter/filter_window -D -R -S   \
	  -T "Set filter window " \
	  -m "Set 1-10% " \
	  -units "%" \
	  -H "Value (1-10%) reflects change in field that causes restart of filter"\
	  -d on -r on -s on  \
	  -readProc lake450_window_r \
	  -writeProc lake450_window_w
	proc lake450_window_r { ins } {
	    insIfReadVerify /${ins} "FWIN?" 32 /${ins}/setup/filter/filter_window " %d" 2
	}
	proc lake450_window_w { ins target } {
	    set window $target
	    if { $window < 1 } { set window 1 }
	    if { $window > 10 } { set window 10 } 
	    set t [format "%2d" $window]
	    insIfWriteVerify /${ins} "FWIN ${t}" "FWIN?" 32 \
		/${ins}/setup/filter/filter_window  " %d" 2  $t
	}


