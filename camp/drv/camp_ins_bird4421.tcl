CAMP_INSTRUMENT /~ -D -T "Bird Model 4421 RF Power Meter" \
    -H "Bird Model 4421 RF Power Meter" -d on \
    -initProc bird4421_init \
    -deleteProc bird4421_delete \
    -onlineProc bird4421_online \
    -offlineProc bird4421_offline 
  proc bird4421_init { ins } {
	global $ins
	insSet /${ins} -if rs232 0.5 2 txa0: 9600 8 none 1 CRLF CRLF
        set ${ins}(listfc) -1.
        set ${ins}(listrc) -1.
    } 
  proc bird4421_delete { ins } {
	global $ins
	insSet /${ins} -line off
	unset $ins
    }
  proc bird4421_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
    }
  proc bird4421_offline { ins } { insIfOff /${ins} }
    CAMP_FLOAT /~/forward -D -R -P -L -T "Forward carrier" -d on -r on \
	-units W -readProc bird4421_fc_r
      proc bird4421_fc_r { ins } {
        set status [scan [insIfRead /${ins} "FCENT" 32] "%*cFC %f %s" val units]
        if { $status != 2 } { return -code error "failed reading bird4421" }
	if { $units == "KW" } { set val [expr 1000*$val] }
        varDoSet /${ins}/forward -v $val
      }
    CAMP_FLOAT /~/forward_dbm -D -R -P -L -T "Forward dBm" -d on -r on \
        -units dBm -readProc bird4421_fd_r
      proc bird4421_fd_r { ins } {
	    insIfReadVerify /${ins} "FDENT" 32 /${ins}/forward_dbm "%*cFD %f %*s" 2
	}
    CAMP_FLOAT /~/forward_max -D -R -P -L -T "Max. forward" -d on -r on \
        -H "Give maximum of previous <num_to_max> readings." \
        -units W -readProc bird4421_maxfc_r
      proc bird4421_maxfc_r { ins } {
        global $ins
        varRead /${ins}/forward
        set val [varGetVal /${ins}/forward]
        # update list of readings
        set lis [set ${ins}(listfc)]
        set num [ expr [varGetVal /${ins}/setup/num_to_max] - 2 ]
        set lis [linsert [lrange $lis 0 $num] 0 $val]
        set ${ins}(listfc) $lis
        # select maximum
        set val [lindex [lsort -decreasing -real $lis] 0]
        varDoSet /${ins}/forward_max -v $val
      }
    CAMP_FLOAT /~/reflected -D -R -P -L -T "Reflected carrier" -d on -r on \
	-units W -readProc bird4421_rc_r
      proc bird4421_rc_r { ins } {
        set status [scan [insIfRead /${ins} "RCENT" 32] "%*cRC %f %s" val units]
        if { $status != 2 } { return -code error "failed reading bird4421" }
	if { $units == "KW" } { set val [expr 1000*$val] }
        varDoSet /${ins}/reflected -v $val
      }
    CAMP_FLOAT /~/reflected_dbm -D -R -P -L -T "Reflected dBm" -d on -r on \
	-units dBm -readProc bird4421_rd_r
      proc bird4421_rd_r { ins } {
	insIfReadVerify /${ins} "RDENT" 32 /${ins}/reflected_dbm "%*cRD %f %*s" 2
      }
    CAMP_FLOAT /~/reflected_max -D -R -P -L -T "Max. reflected" -d on -r on \
        -H "Give maximum of previous <num_to_max> readings." \
        -units W -readProc bird4421_maxrc_r
      proc bird4421_maxrc_r { ins } {
        global $ins
        varRead /${ins}/reflected
        set val [varGetVal /${ins}/reflected]
        # update list of readings
        set lis [set ${ins}(listrc)]
        set num [ expr [varGetVal /${ins}/setup/num_to_max] - 2 ]
        set lis [linsert [lrange $lis 0 $num] 0 $val]
        set ${ins}(listrc) $lis
        # select maximum
        set val [lindex [lsort -decreasing -real $lis] 0]
        varDoSet /${ins}/reflected_max -v $val
      }
    CAMP_FLOAT /~/swr -D -R -P -L -T "Standing wave ratio" -d on -r on \
	-readProc bird4421_sw_r
      proc bird4421_sw_r { ins } {
	insIfReadVerify /${ins} "SWENT" 32 /${ins}/swr "%*cSW %f %*s" 2
      }
    CAMP_FLOAT /~/return_loss -D -R -P -L -T "Return loss" -d on -r on \
	-units dBm -readProc bird4421_rl_r
      proc bird4421_rl_r { ins } {
	insIfReadVerify /${ins} "RLENT" 32 /${ins}/return_loss "%*cRL %f %*s" 2
      }
    CAMP_FLOAT /~/any_max -D -R -P -L -T "Maximum since reading" -d on -r on \
        -H "Read the maximum value of whatever was read last, since it was read" \
        -units ? -readProc bird4421_anymax_r
      proc bird4421_anymax_r { ins } {
        # forward-read with FCENT->MXENT in query, and FC->MX in parse
        set status [scan [insIfRead /${ins} "MXENT" 32] "%*cMX %f %s" val units]
        if { $status != 2 } { return -code error "failed reading bird4421" }
        varDoSet /${ins}/any_max -v $val -units $units
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
            -readProc bird4421_id_r
          proc bird4421_id_r { ins } {
		set id 0
		set status [catch {insIfWrite /${ins} "W  4421"}]
		if { $status == 0 } {
		    set status [catch {insIfRead /${ins} "U2ENT" 32} buf]
		    if { $status == 0 } {
		        set id [scan $buf " 4420 4421 %s RS232" val]
			if { $id != 1 } { set id 0 }
		    }
		}
		varDoSet /${ins}/setup/id -v $id
	    }
        CAMP_INT /~/setup/num_to_max -D -S -T "Number of readings per max" \
            -d on -s on -v 10 -writeProc bird4421_num_to_max_w 
          proc bird4421_num_to_max_w { ins target } { 
	    global $ins
            set ${ins}(listfc) -1.
            set ${ins}(listrc) -1.
            varDoSet /${ins}/setup/num_to_max -v $target
          }
	CAMP_STRING /~/setup/store -D -S -R -d on -s on -r on \
	    -readProc bird4421_store_r \
            -writeProc bird4421_store_w
          proc bird4421_store_r { ins } {
		insIfReadVerify /${ins} "U2ENT" 32 /${ins}/setup/store " %*s %s %*s %*s" 2
	    }
          proc bird4421_store_w { ins target } {
		set store [string range $target 0 5]
		set sendStore [format "%6s" $store]
		insIfWriteVerify /${ins} "W$sendStore" "U2ENT" 32 /${ins}/setup/store " %*s %s %*s %*s" 2 $target
	    }
