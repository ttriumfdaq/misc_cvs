# camp_ins_oxford_ilm200.tcl
# Camp Tcl instrument driver for Oxford ILM200 cryogen level meter
# Donald Arseneau,  TRIUMF
# Last revised   27-Apr-2001
#
# This driver is not fully generic, but is written to optimize convenience
# for the permanent installation for beta-NMR at ISAC (TRIUMF). 
#
# In view of this, should this device be called "bnmr_level" or similar?
# the trio of "oxford_ips120", oxford_itc4", and "oxford_ilm200" invites
# selection errors.
# 
CAMP_INSTRUMENT /~ -D -T "Oxford ILM200" \
    -H "Oxford Instruments ILM200 Cryogen Level Meter" \
    -d on \
    -initProc ox_ilm200_init     -deleteProc ox_ilm200_delete \
    -onlineProc ox_ilm200_online -offlineProc ox_ilm200_offline

    proc ox_ilm200_init { ins } {
	insSet /${ins} -if rs232 0.5 2 /tyCo/4 9600 8 none 2 CR CR
    }

    proc ox_ilm200_delete { ins } {
	global $ins
	insSet /${ins} -line off
	unset $ins
    }

    proc ox_ilm200_online { ins } {
	insIfOn /${ins}
#	# Set terminator CR; Set Remote and Unlocked
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id ] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
	insIfWrite /${ins} "\$Q0"
	insIfWrite /${ins} "\$C3"
	if { [catch {varRead /${ins}/setup/channels}] } {
	    insIfOff /${ins}
	    return -code error "failed to read status"
	}
    }
 
    proc ox_ilm200_offline { ins } {
	insIfOff /${ins}
    }

    CAMP_FLOAT /~/He_level -D -R -P -L -A -T "Helium Level" \
	    -d on -r on -units {%} -readProc ox_ilm200_He_read_r

      proc ox_ilm200_He_read_r { ins } {
	  ox_ilm200_read ${ins} He_level HeChan
      }

    CAMP_FLOAT /~/N2_level -D -R -P -L -A -T "Nitrogen Level" \
	    -d on -r on -units {%} -readProc ox_ilm200_N2_read_r

      proc ox_ilm200_N2_read_r { ins } {
	  ox_ilm200_read ${ins} N2_level N2Chan
      }

      proc ox_ilm200_read { ins var tag } {
	  global $ins
	  set ch [set ${ins}(${tag})]
	  set buf [ insIfRead /${ins} "R${ch}" 32 ]
	  if { [ scan $buf " R%f" val ] != 1 } {
	      return -code error "Bad readback: $buf "
	  }
	  set val [ expr {0.1*$val} ]
	  varDoSet /${ins}/${var} -v $val
	  set buf [ insIfRead  /${ins} "X" 16 ]
	  if { [scan $buf "X%1d%1d%1dS%2x%2x%2xR%2x" a b c u v w z ] < 6 } {
	      return 
	  }
#         # change $val if "low level" bit is set in status
	  set val [expr {$val + ( [ lindex "0 $u $v $w" $ch ] & 32 )}  ]
#         # then raise alarm if $val was changed
	  varTestAlert /${ins}/${var} $val
      }


    CAMP_SELECT /~/He_sampling -D -S -R -P -T "He sampling" \
	-H "Sampling mode for helium level, fast or slow" \
	-d on -s on -r on -p on -p_int 15 \
	-selections Slow Fast \
	-readProc ox_ilm200_status_r -writeProc ox_ilm200_sampling_w

      proc ox_ilm200_sampling_w { ins value } {
	  global $ins
	  set ch [set ${ins}(HeChan)]
	  if { $ch == 0 } {
	      return -code error "No helium sampling channel"
	  }
	  insIfWrite /${ins} [lindex [list S$ch T$ch] $value]
	  varRead /${ins}/He_sampling
	  if { [varGetVal /${ins}/He_sampling] != $value } {
	      return -code error "Setting failed"
	  }
      }


    CAMP_SELECT /~/Sample -D -S -P -T "Sample now" \
	-H "Set this to sample He level now" \
	-d on -s on -m "" -p off -p_int 7 \
	-selections "Sample now" \
	-readProc ox_ilm200_sampoff -writeProc ox_ilm200_sampon
 
      proc ox_ilm200_sampon { ins value } {
	  varRead /${ins}/He_sampling
	  set old [varGetVal /${ins}/He_sampling]
	  varSet /${ins}/He_sampling -v 1
	  if { $old == 0 } {
	      varDoSet /${ins}/Sample -p off
	      varDoSet /${ins}/Sample -p on -p_int 7
	  }
          varDoSet /${ins}/Sample -m "SAMPLING ... will have a new reading shortly"
      }

      proc ox_ilm200_sampoff { ins } {
	  varSet /${ins}/He_sampling -v 0
	  varDoSet /${ins}/Sample -p off -m ""
	  varRead /${ins}/He_level
      }

    CAMP_STRING /~/autofill -D -R -P -T "Autofill Status" \
	-d on -r on -p off -p_int 30  \
	-readProc ox_ilm200_status_r

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

    CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections false true -readProc ox_ilm200_id_r

      proc ox_ilm200_id_r { ins } {
	  set id 0
	  set buf x
	  catch {insIfRead /${ins} "V" 80} buf
	  set id [expr { [scan $buf " ILM200 Version %f (c) OXFORD %d" d1 d2] == 2 } ]
	  if { $id != 1 } {
#         # Failed match, so clear type-ahead and try again
	      while {[catch {insIfRead /${ins} "\$" 128} buf] == 0} {}
	      catch {insIfRead /${ins} "V" 80} buf
	      set id [expr { [scan $buf " ILM200 Version %f (c) OXFORD %d" d1 d2] == 2 } ]
	  }
	  varDoSet /${ins}/setup/id -v $id
      }

    CAMP_STRING /~/setup/channels -D -R -T "Get channels" -d on -r on \
	    -H "Identifies He level channel number (first) and N2 level channel (second)" \
 	    -v " " -readProc ox_ilm200_status_r

      proc ox_ilm200_status_r { ins } {
	  global $ins
	  set buf [ insIfRead  /${ins} "X" 16 ]
	  set stat [scan $buf "X%1d%1d%1dS%2x%2x%2xR%2x" a b c u v w z ]
	  if { $stat != 7 } {
	      varDoSet /${ins}/setup/channels -v " "
	      return -code error "failed reading ilm200 status" 
	  }
#         #  Find which channel is doing what
	  set N2Chan 0
	  set HeChan 0
	  set i 0
	  foreach cu [list $a $b $c] {
	      incr i
	      if { $cu == 1 } { set N2Chan $i }
	      if { $cu == 2 || $cu == 3 } { set HeChan $i }
	  }
          set fillN ""
	  if { $N2Chan } {
	      varDoSet /${ins}/N2_level -r on -m "Read N2 level (channel ${N2Chan})"
	      set cs [lindex [list 0 $u $v $w] $N2Chan]
	      set fs [expr ( $cs & (8|16) ) / 8]
              set fillN [lindex {"N2 Full" "" "N2 Filling" "N2 Start"} $fs]
	  } else {
	      varDoSet /${ins}/N2_level -r off -m "Read N2 level (disabled)"
	  }
          set fillH ""
	  if { $HeChan } {
	      varDoSet /${ins}/He_level -r on -m "Read He level (channel ${HeChan})"
	      set cs [lindex [list 0 $u $v $w] $HeChan]
	      varDoSet /${ins}/He_sampling -v [expr { ($cs & 2)/2 }]
	      set fs [expr ( $cs & (8|16) ) / 8]
              set fillH [lindex {"He Full" "" "He Filling" "He Start"} $fs]
	  } else {
	      varDoSet /${ins}/He_level -r off -m "Read He level (disabled)"
	  }
          set auf [varGetVal /${ins}/setup/autofill]
          if { $auf > 0 } {
              if { ($auf % 2) == 0 } { set fillN "" }
              if { ($auf/2) == 0 } { set fillH "" }
              if { $fillH == "" || $fillN == "" } {
                  set fills "$fillN$fillH"
              } else {
                  set fills "$fillN, $fillH"
              }
              if { $fills == "" } { set fills "Not Filling" }
          } else {
              set fills "None"
          }
          varDoSet /${ins}/autofill -v $fills
	  varDoSet /${ins}/setup/channels -v "$HeChan $N2Chan"
	  set ${ins}(HeChan) $HeChan
	  set ${ins}(N2Chan) $N2Chan
      }


    CAMP_SELECT /~/setup/autofill -D -S -T "autofill" -d on -s on \
	    -H "Set this to tell Camp which channels are using autofill" \
	    -selections None "N2 Only" "He Only" Both \
	    -writeProc ox_ilm200_autofill_w

      proc ox_ilm200_autofill_w { ins val } {
          varDoSet /${ins}/setup/autofill -v $val
          ox_ilm200_status_r $ins
      }
