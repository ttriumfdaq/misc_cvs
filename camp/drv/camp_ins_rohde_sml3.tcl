CAMP_INSTRUMENT /~ -D -T "Rohde&Schwarz SML 3 Signal Generator" \
    -H "Rohde&Schwarz SML 3 Signal Generator" -d on \
    -initProc sml3_init \
    -deleteProc sml3_delete \
    -onlineProc sml3_online \
    -offlineProc sml3_offline 
  proc sml3_init { ins } { 
    insSet /${ins} -if rs232 0.2 2 tt: 9600 8 none 1 LF CRLF
  }
  proc sml3_delete { ins } { insSet /${ins} -line off }
  proc sml3_online { ins } {
    insIfOn /${ins}
    insIfWrite /${ins} ""
    insIfWrite /${ins} [format %c 17]
    set status [catch {varRead /${ins}/setup/id}]
    if { ( $status != 0 ) || ( [varGetVal /${ins}/setup/id] == 0 ) } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc sml3_offline { ins } { insIfOff /${ins} }

    CAMP_FLOAT /~/frequency -D -S -R -P -L -T "Output frequency" \
        -d on -s on -r on -units MHz \
        -readProc sml3_freq_set_r -writeProc sml3_freq_set_w
      proc sml3_freq_set_r { ins } {
        set buf [insIfRead /${ins} "FREQ?" 32]
	if { [scan $buf { %g} frq] < 1 } {
	  return -code error "Invalid numeric readback: $buf"
	}
        set frq [expr {$frq*1.0e-6}]
        varDoSet /${ins}/frequency -v $frq
      }
      proc sml3_freq_set_w { ins target } {
	insIfWrite /${ins} "FREQ $target MHz"
        varDoSet /${ins}/frequency -v $target
      }

    CAMP_FLOAT /~/power_level -D -S -R -P -L -T "Output level" \
        -H "Output power level setting (equivalent to amplitude)" \
        -d on -s on -r on -units dBm \
        -readProc sml3_power_level_r -writeProc sml3_power_level_w
      proc sml3_power_level_w { ins target } {
        if { $target < -130.0 || $target > 25.0 } { 
          return -code error "Value outside valid range (-130 to 25)"
        }
        insIfWriteVerify /${ins} "POW $target" "POW?" 32 /${ins}/power_level " %f" 2 $target 0.1
        set A [expr { 223.6068 * pow(10.0, ($target/20.0))}]
	set A [format %.6f $A]
        varDoSet /${ins}/amplitude -v $A
      }
      proc sml3_power_level_r { ins } {
        insIfReadVerify /${ins} "POW?" 32 /${ins}/power_level " %f" 2
        set dBm [varGetVal /${ins}/power_level]
        set A [expr { 223.6068 * pow(10.0, ($dBm/20.0))}]
	set A [format %.6f $A]
        varDoSet /${ins}/amplitude -v $A
      }

    CAMP_FLOAT /~/amplitude -D -S -R -P -L -T "Output amplitude" \
        -H "Output amplitude setting (equivalent to power_level)" \
        -d on -s on -r on -units mV \
        -readProc sml3_power_level_r -writeProc sml3_amplitude_w
      proc sml3_amplitude_w { ins target } {
        if { $target < 0.00 || $target > 2800.0 } { 
          return -code error "Value outside valid range (0 to 2800)"
        }
        insIfWrite /${ins} "POW $target mV" $target
        varRead /${ins}/amplitude
      }

    CAMP_SELECT /~/rf_on -D -S -R -L -T "RF on/off" \
        -d on -s on -r on -selections OFF ON \
        -readProc sml3_rf_on_r -writeProc sml3_rf_on_w
      proc sml3_rf_on_w { ins target } {
	insIfWrite /${ins} "OUTP $target"
	varDoSet /${ins}/rf_on -v $target
      }
      proc sml3_rf_on_r { ins } {
        insIfReadVerify /${ins} "OUTP?" 32 /${ins}/rf_on " %d" 2
      }

    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

      CAMP_INT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -readProc sml3_id_r
        proc sml3_id_r { ins } {
          set ident [insIfRead /${ins} "*IDN?" 48]
          set n [scan $ident " ROHDE&SCHWARZ,SML%d,%d/%d,%f" ver id sub rev]
          if { $n != 4 } { set id 0 }
          varDoSet /${ins}/setup/id -v $id
        }

    CAMP_SELECT /~/setup/reset -D -S -T "Reset device" \
            -d on -s on -selections RESET "" \
            -writeProc sml3_reset_w
        proc sml3_reset_w { ins target } {
          if { $target == 0 } {
            insIfWrite /${ins} "*RST"
          }
        }
