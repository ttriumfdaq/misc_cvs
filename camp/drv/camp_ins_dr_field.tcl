# camp_ins_dr_field.tcl  --  Integrated system for DR field control
# Includes a lot from the "controller" meta-device.
#
# Donald Arseneau                last modified    07-Nov-2003

CAMP_INSTRUMENT /~ -D -T "DR Field Control" \
    -H "Integrated system for DR field control" \
    -d on \
    -initProc dr_init -deleteProc dr_delete \
    -onlineProc dr_online -offlineProc dr_offline

  proc dr_init { ins } {
      global ${ins}_CON
      insSet /${ins} -if none 0.0 0.0
      insIfOn /${ins}
      set ${ins}_CON(from) 0.0
      set ${ins}_CON(prev_hall) 1234.5678
      set ${ins}_CON(stabcount) 0
      set ${ins}_CON(failure) -1
      dr_pre_setup $ins
  }
  proc dr_delete { ins } {
      global ${ins}_CON
      unset  ${ins}_CON
      insSet /${ins} -line off
  }
  proc dr_online { ins } {
      insIfOn /${ins}
      dr_pre_setup $ins
  }
  proc dr_offline { ins } {
      insIfOff /${ins}
  }
  proc dr_pre_setup { ins } {
      global ${ins}_CON
      # enable alarms only after setup was completed at some point
      if { [set ${ins}_CON(failure)] == 0 } {
          set ${ins}_CON(failure) 1
      }
      varDoSet /${ins}/setup/init -v 0 -p on -p_int 2
      dr_state_poll $ins 0 20
      varDoSet /${ins}/set_field -s off
      return "Device set-up still required"
  }

  CAMP_FLOAT /~/set_field -D -S -L -T "Set field" \
        -H "Set the desired magnetic field, in Tesla" \
        -d on -s on -units "T" -writeProc dr_setfield_w

    proc dr_setfield_w { ins target } {
        if { [ varGetVal /${ins}/state ] == 0 } {
            return -code error [dr_pre_setup $ins]
        }
        dr_init_hall $ins
	set mag [varGetVal /${ins}/setup/mag_device]
        set cal [varGetVal ${mag}/setup/calibration]
        set mx [varGetVal ${mag}/setup/mag_max]
        set mn [varGetVal ${mag}/setup/mag_min]
        if { $target < $mn * $cal || $target > $mx * $cal } {
            return -code error "Field out of magnet range"
        }
        varDoSet /${ins}/set_field -v $target
        dr_state_poll $ins 2 1.0
    }

  CAMP_SELECT /~/refresh -D -S -T "Refresh magnet" \
        -d on -s on \
	-H "Refresh DR magnet persistent field now" \
        -selections { } {REFRESH} \
        -writeProc dr_refresh_w

    proc dr_refresh_w { ins target } {
	global ${ins}_CON
	if { $target } {
            set mag [varGetVal /${ins}/setup/mag_device]
            varSet ${mag}/refresh -v $target
            varDoSet /${ins}/state -v 8 -m [dr_state_mess 8]
            set ${ins}_CON(stabcount) 0
            set ${ins}_CON(failure) 1
        }
    }

  CAMP_SELECT /~/state -D -R -P -A -T "State" \
        -d on -r on -p on -p_int 5 \
	-H "State of DR field control" \
        -selections {Need Setup} Floating Set Ramping Degaussing Stabilizing Controlling Limit Refresh \
        -readProc dr_state_r -writeProc dr_state_w 

    proc dr_state_r { ins } {
        if { [ varGetVal /${ins}/setup/init ] == 0 } {
            dr_state_poll $ins 0 9
            varDoSet /${ins}/setup/init -v 0 -p on -p_int 2
        } else {
            if { [ catch { dr_process $ins } errm ] } {
                dr_state_poll $ins 0 9
                varDoSet /${ins}/state -m "Error: $errm"
                varDoSet /${ins}/setup/init -v 0 -p on -p_int 2
            }
        }
    }
    proc dr_state_w { ins val } {
        set old [varGetVal /${ins}/state]
        if { $old == 0 } {
            return -code error "Check set-up first"
        }
        if { $old < 1 } { 
            dr_pre_setup $ins
            return
        }
        if { $val == 1 || $val > 4 } {
            dr_state_poll $ins $val [varGetVal /${ins}/setup/interval]
            return
        }
        return -code error "Invalid state setting. Try setting mag_field instead."
    }
                

  CAMP_FLOAT /~/mag_field -D -R -L -T "Nominal magnet field" \
        -H "Nominal magnet field, based on current" \
        -d on -r on -units "T" -readProc dr_magfield_r
    proc dr_magfield_r { ins } {
        set mv "[varGetVal /${ins}/setup/mag_device]/mag_field"
        if { [catch {
            varDoSet /${ins}/mag_field -v [varGetVal $mv]
        } ] } {
            return -code error [dr_pre_setup $ins]
        }
    }

  CAMP_FLOAT /~/Hall_value -D -R -L -A -T "Hall Read" \
	-H "Hall probe readback (will be different from set_field)" \
        -d on -r on -units "T" -tol 0.0002 -readProc dr_readback_r
    proc dr_readback_r { ins } {
        set hp "[varGetVal /${ins}/setup/Hall_variable]"
        catch { varRead $hp }
        if { [catch {
            set x [varGetVal $hp]
        } ] } {
            return -code error [dr_pre_setup $ins]
        }
        # Apply function of $x
        set v 99.99999
        catch { expr [varGetVal /${ins}/setup/Hall_cal ] } v
        varDoSet /${ins}/Hall_value -v $v
        varTestAlert /${ins}/Hall_value [varGetVal /${ins}/Hall_target]
    }

  CAMP_FLOAT /~/Hall_target -D -S -L -T "Hall Setpoint" \
        -H "Hall probe target value" \
        -d on -s on -units "T" -writeProc dr_target_w
    proc dr_target_w { ins val } {
        varDoSet /${ins}/Hall_target -v $val
    }

  #  DAC_value is a float so that small fractional increments can accumulate
  #  over several cycles.
  CAMP_FLOAT /~/DAC_value -D -S -R -L -T "DAC correction output" \
        -d on -s on -r on -units " " \
        -writeProc dr_dac_w -readProc dr_dac_r
    proc dr_dac_r { ins } {
        set d "[varGetVal /${ins}/setup/DAC_variable]"
        if { [catch { varGetVal $d } val] } {
            return -code error [dr_pre_setup $ins]
	}
        varDoSet /${ins}/DAC_value -v $val
    }
    proc dr_dac_w { ins val } {
        set d "[varGetVal /${ins}/setup/DAC_variable]"
        if { [catch { varSet $d -v [expr { round($val) } ] } ] } {
            return -code error [dr_pre_setup $ins]
        }
        varDoSet /${ins}/DAC_value -v $val
    }

    proc dr_process { ins } {
        global ${ins}_CON
        set state [varGetVal /${ins}/state]
        set mag [varGetVal /${ins}/setup/mag_device]
        set target [varGetVal /${ins}/set_field]

        #  Update values
        varRead /${ins}/DAC_value
        varRead /${ins}/mag_field

        set dmx [varGetVal /${ins}/setup/DAC_max]
        set dmn [varGetVal /${ins}/setup/DAC_min]

        set valid 0
	if { [varGetPollInterval /${ins}/state] > 10 } {
            catch {
                varRead /${ins}/Hall_value
                set valid 1
                if { $state >= 5 } {
                    set rb [ varGetVal /${ins}/Hall_value ]
                    if { $rb < [varGetVal /${ins}/setup/Hall_min] || \
                         $rb > [varGetVal /${ins}/setup/Hall_max] } {
                        # Some other settings here ???
                        set valid 0
                    }
                }
            }
        }
        set curr [varGetVal ${mag}/mag_field]
	set from [set ${ins}_CON(from)]

        switch $state {
            0 { # Need-setup 
            }
            1 { # Float -- not controlling (Set poll interval)
                set i [varGetVal /${ins}/setup/interval]
                if { [varGetPollInterval /${ins}/state] != $i } {
                    dr_state_poll $ins 1 $i
                }
            }
            2 { # Set -- A new set point has been given:  set the starting
                # dac output and start "degauss"
                set ${ins}_CON(from) $curr
                set ampl [ expr abs( 0.03 * ( $target - $curr ) ) + 0.003 ]
                if { $ampl > 0.15 } { set ampl 0.15 }
                set cal [varGetVal ${mag}/setup/calibration]
                set mx [varGetVal ${mag}/setup/mag_max]
                set mn [varGetVal ${mag}/setup/mag_min]
                # fraction of range to start at:  0.1 <= f <= 0.9
                set f [expr { 0.4 * $target / (($mx-$mn)*$cal) + 0.5 } ]

                varSet /${ins}/DAC_value -v [expr { $f*$dmn + (1.0-$f)*$dmx } ]
                varSet ${mag}/degauss/amplitude -v $ampl
                varSet ${mag}/degauss/set_field -v $target
                varSet ${mag}/degauss/degauss -v 1
                incr state
                dr_state_poll $ins $state 15
            }
            3 -
            4 { # Ramp and degauss, pretty much the same
                if { abs( $from - $curr ) >= abs( $from - $target ) } {
                    set state 4
                }
                if { [varGetVal ${mag}/degauss/degauss] == 0 } {
                    set state 5
                    dr_state_poll $ins 5 [varGetVal /${ins}/setup/interval]
                    set ${ins}_CON(prev_hall) -987.654321
                }
            }
            5 { # Stabilize -- until persistent and readback not changing 
                if { $valid && [varGetVal ${mag}/ramp_status] == 3 } {
                    if { abs( ($rb - [set ${ins}_CON(prev_hall)]) * [varGetVal /${ins}/setup/DAC_cal])  \
                            < ($dmx - $dmn)*0.005 } {
                        set state 6
                        varSet /${ins}/Hall_target -v $rb
                        dr_state_poll $ins 6 [varGetVal /${ins}/setup/interval]
                    }
                    set ${ins}_CON(prev_hall) $rb
                }
            }
            6 -
            7 -
            8 { # Control, limited, refresh -- all control
                set ${ins}_CON(failure) 0
                set dv [ varGetVal /${ins}/DAC_value ]
                set sp [ varGetVal /${ins}/Hall_target ]
                set agg [expr { ($state == 6) ? [varGetVal /${ins}/setup/aggression] : 0.6 } ]
                set new [expr $dv + ( $sp - $rb ) * $agg * [varGetVal /${ins}/setup/DAC_cal] ]
                if { $new < $dmn } { set new $dmn }
                if { $new > $dmx } { set new $dmx }
                if { $new == $dmx || $new == $dmn } {
                    # Hit control limit.  If just did refresh, but now finished, (ramp_status
                    # is Persistent) and control won't come back in range (after 5 iteration) 
                    # then set state -> Float -- give up on control.
                    if { $state == 8 } {
                        if { [varGetVal ${mag}/ramp_status] == 3 } {
                            if { [incr ${ins}_CON(stabcount)] > 5 } {
                                set ${ins}_CON(failure) 1
                                set state 1
                            }
                        }
                    # Control -> Limit; Limit -> Refresh
                    } else {
                        incr $state
                        if { $state == 8 } {
                            varSet /${ins}/refresh -v 1
                        }
                    }
                } else {
                    # Controlling, and if magnet ramp status is "Persistent", then 
                    # put control status to "Control"
                    if { [varGetVal ${mag}/ramp_status] == 3 } { 
                        set state 6
                        set ${ins}_CON(failure) 0
                        varDoSet /${ins}/refresh -v 0
                    }
                }
                if { abs( $curr - $target ) > 0.0002 } {
                    # User changed setting on magnet directly -- stop control
                    set ${ins}_CON(failure) 0
                    set state 1
                    set new [expr round( 0.5 * ($dmx + $dmn) ) ]
                }
                varSet /${ins}/DAC_value -v $new
            }
        }
        varDoSet /${ins}/state -v $state -m [dr_state_mess $state]

        # Only issue alarms when there has been a failure of control.
        if { [set ${ins}_CON(failure)] } {
            varTestAlert /${ins}/state 6
        } else {
            varTestAlert /${ins}/state $state
        }
    }

proc dr_state_poll { ins val poll } {
    varDoSet /${ins}/state -v $val -p off -m [dr_state_mess $val]
    varDoSet /${ins}/state -p on -p_int $poll
}

proc dr_state_mess { val } {
    return [lindex [list \
            {Not functioning because magnet/hall probe/dac is not set up} \
            {Magnetic field is controlled by magnet PS alone} \
            {New field has just been set} \
            {Power supply is ramping the magnet} \
            {Magnet is being "degaussed"} \
            {Let Hall probe reading settle before choosing "target" to hold} \
            {Field control has hit a limit} \
            {Magnet is having its persistent field refreshed} ] $val ]
}

CAMP_STRUCT /~/setup -D -T "Setup" -d on 

  CAMP_SELECT /~/setup/init -D -S -R -P -T "Initialization" \
          -d on -s on -r on -p on -p_int 3 -v 0 -selections FALSE TRUE \
          -readProc dr_set_init_r -writeProc dr_set_init_w
    proc dr_set_init_r { ins } {
        set lm [ string length [varGetVal /${ins}/setup/mag_device] ]
        set lh [ string length [varGetVal /${ins}/setup/Hall_variable] ]
        set ld [ string length [varGetVal /${ins}/setup/DAC_variable] ]
        if { $lm && $lh && $ld } {
            set mag [varGetVal /${ins}/setup/mag_device]
            varDoSet /${ins}/set_field -s on -v [varGetVal ${mag}/mag_field]
            varDoSet /${ins}/setup/init -p off -v 1
            varDoSet /${ins}/state -v 1 -m [dr_state_mess 1]
            return
        }
        set inames [sysGetInsNames]
        foreach i $inames {
            set type [insGetTypeIdent /$i]
            switch $type {
                oxford_ips120 {
                    if { $lm == 0 } {
                        catch { varSet /${ins}/setup/mag_device -v /${i} }
                    }
                    set lm 1
                }
                hp34420a {
                    if { $lh == 0 } {
                        catch { varSet /${ins}/setup/Hall_variable -v /${i}/reading }
                    }
                    set lh 1
                }
                tip850_dac {
                    if { $ld == 0 } {
                        varSet /${ins}/setup/DAC_variable -v /${i}/dac_set
                    }
                    set ld 1
                }
            }
        }
        #  Now create any instruments that are undefined and could not be found
        if { $lm == 0 } {
            catch { 
                insAdd oxford_ips120 DR_magps
                insSet /DR_magps -if rs232 0.3 /tyCo/4 9600 8 none 2 CR CR 2 -online
                varSet /${ins}/setup/mag_device -v /DR_magps
            }
        }
        if { $lh == 0 } {
            catch { 
                insAdd hp34420a DR_hphall
                insSet /DR_hphall -if gpib 0.2 22 CRLF CRLF -online
                varSet /${ins}/setup/Hall_variable -v /DR_hphall/reading
            }
        }
        if { $ld == 0 } {
            catch { 
                insAdd tip850_dac DR_dac
                insSet /DR_dac -if none 0.0 0 -online
                varSet /${ins}/setup/DAC_variable -v /DR_dac/dac_set
            }
        }
    }
    proc dr_init_hall { ins } {
        set hpdev [lindex [split [varGetVal /${ins}/setup/Hall_variable] "/"] 1]
        catch { 
            varSet /${hpdev}/setup/num_aver -v 1
            if { [string compare "HallProbe" [varSelGetValLabel /${hpdev}/setup/sensor_type]] } {
                varSet /${hpdev}/setup/sensor_type -v "HallProbe"
            }
            varSet /${hpdev}/setup/how_read -v "continuously"
        }
        varSet /${hpdev}/reading -p off -r on
    }

  CAMP_STRING /~/setup/mag_device -D -S -T "Magnet Camp device" \
        -H "Identify Camp instrument name for magnet" \
        -d on -s on -writeProc dr_mag_device_w
    proc dr_mag_device_w { ins target } {
        set var /[lindex [ split [string trim $target "/"] "/" ] 0]
        set st [ catch {varGetVal $var/degauss/amplitude} ]
        if { $st != 0 } {
            return -code error "[ dr_badvar $var Magnet ]"
        }
        varDoSet /${ins}/setup/mag_device -v $var
    }

  CAMP_STRING /~/setup/Hall_variable -D -S -T "Hall probe variable" \
        -H "Identify Camp variable for field measurement" \
        -d on -s on -writeProc dr_hall_var_w
    proc dr_hall_var_w { ins target } {
        set var /[string trim $target "/"]
        set st [ catch {varGetVal $var} v ]
        if { $st == 0 } {
            set st [ catch { expr 0.001 + $v } ]
        }
        if { $st != 0 } {
            return -code error "[ dr_badvar $var {Hall probe reading} ]"
        }
        varDoSet /${ins}/setup/Hall_variable -v $var
    }

  CAMP_STRING /~/setup/DAC_variable -D -S -T "DAC variable" \
        -H "Identify Camp variable for field correction output" \
        -d on -s on -writeProc dr_dac_var_w
    proc dr_dac_var_w { ins target } {
        set var /[string trim $target "/"]
        set st [ catch {varGetVal $var} v ]
        if { $st == 0 } { set st [ catch { expr 0.0 + $v } ] }
        if { $st == 0 } { set st [ catch { varSet $var -v $v } ] }
        if { $st != 0 } {
            return -code error "[ dr_badvar $var {DAC setpoint} ]"
        }
        varDoSet /${ins}/setup/DAC_variable -v $var
    }

    proc dr_badvar { target what } {
	return "Device \"${target}\" does not exist, is not ready, or is inappropriate for a ${what}."
    }

  CAMP_FLOAT /~/setup/Hall_min -D -S -T "Minimum readback" \
        -H "Minimum acceptable readback value" \
        -d on -s on -v {-8.0} -units T -writeProc dr_hall_min_w
    proc dr_hall_min_w { ins target } {
        varDoSet /${ins}/setup/Hall_min -v $target
    }

  CAMP_FLOAT /~/setup/Hall_max -D -S -T "Maximum readback" \
        -H "Maximum acceptable readback value" \
        -d on -s on -v {8.0} -units T -writeProc dr_hall_max_w
    proc dr_hall_max_w { ins target } {
        varDoSet /${ins}/setup/Hall_max -v $target
    }

  CAMP_STRING /~/setup/Hall_cal -D -S -T "Hall calibration" \
        -H "Function of \$x to convert raw Hall probe reading (\$x) to Tesla" \
        -d on -s on -v {$x / 0.066} -writeProc dr_hall_cal_w
    proc dr_hall_cal_w { ins target } {
        set f $target
        if { [scan "$f;1" { ;%d} x] == 1 } {set f {$x}}
        set x 1.23
        set s1 [catch {expr 1.0+$f}]
        set x bad
        set s2 [catch {expr 1.0+$f}]
        if { $s1==1 || $s2==0 } {
            return -code error "calibration not a valid function of \$x"
        }
        varDoSet /${ins}/setup/Hall_cal -v $target
    }

  CAMP_FLOAT /~/setup/DAC_min -D -S -T "Minimum output" \
        -H "Minimum allowed output setting" \
        -d on -s on -v {-2040} -writeProc dr_dac_min_w
    proc dr_dac_min_w { ins target } {
        varDoSet /${ins}/setup/DAC_min -v $target
    }

  CAMP_FLOAT /~/setup/DAC_max -D -S -T "Maximum output" \
        -H "Maximum allowed output setting" \
        -d on -s on -v {2040} -writeProc dr_dac_max_w
    proc dr_dac_max_w { ins target } {
        varDoSet /${ins}/setup/DAC_max -v $target
    }

  #  DAC calibration.  Note that the default aqccounts for the 
  #  polarity being reversed from sensible!
  CAMP_FLOAT /~/setup/DAC_cal -D -S -T "Integral coeff" \
        -H "Output calibration of DAC + correction coil" \
        -d on -s on -v {-5180000.0} -units "DAC/T" -writeProc dr_dac_cal_w
    proc dr_dac_cal_w { ins target } {
        varDoSet /${ins}/setup/DAC_cal -v $target
    }

  CAMP_FLOAT /~/setup/aggression -D -S -T "Fraction of step" \
        -H "Fraction of correction to make (0-1) (lower smooths noise; responds slower)" \
        -d on -s on -v 0.15 -units "" -writeProc dr_aggress_w
    proc dr_aggress_w { ins target } {
        varDoSet /${ins}/setup/aggression -v $target
    }

  CAMP_FLOAT /~/setup/interval -D -S -T "Control cycle time" \
        -H "Poll interval for control cycle" \
        -d on -s on -v 100.0 -units "s" -writeProc dr_interval_w
    proc dr_interval_w { ins target } {
        if { $target < 15.0 } { set target 60.0 }
        varDoSet /${ins}/setup/interval -v $target
    }

