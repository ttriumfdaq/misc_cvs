CAMP_INSTRUMENT /~ -D -T "Metrolab PT3020 NMR Teslameter" -d on \
    -initProc pt3020_init \
    -deleteProc pt3020_delete \
    -onlineProc pt3020_online \
    -offlineProc pt3020_offline
  proc pt3020_init { ins } {
	insSet /${ins} -if rs232 0.5 txa0: 9600 8 none 1 CRLF none 2
  }
  proc pt3020_delete { ins } {
	insSet /${ins} -line off
  }
  proc pt3020_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
        varSet /${ins}/units -v 1
  }
  proc pt3020_offline { ins } {
	insIfOff /${ins}
  }
    CAMP_FLOAT /~/field -D -R -P -L -T "Field reading" -d on -r on \
	-readProc pt3020_field_r
      proc pt3020_field_r { ins } {
	    insIfReadVerify /${ins} "\x05" 16 /${ins}/field "%*1s%f%*1s" 2
      }
    CAMP_SELECT /~/units -D -R -S -T "Field units" -d on -r on -s on \
    	-selections MHz T \
	-readProc pt3020_units_r \
	-writeProc pt3020_units_w
      proc pt3020_units_r { ins } {
            scan [insIfRead /${ins} "S3" 8] "S%2d" val
    	    varDoSet /${ins}/units -v [expr {$val&0x1}]
      }
      proc pt3020_units_w { ins target } {
	    insIfWrite /${ins} "D$target" 
	    varRead /${ins}/units
      }
    CAMP_SELECT /~/lock_status -D -R -P -T "Signal lock status" -d on -r on \
    	-selections Unlocked Locked \
 	-readProc pt3020_lock_r
      proc pt3020_lock_r { ins } {
            scan [insIfRead /${ins} "\x05" 16] "%1s%*f%*1s" char
            switch $char { 
                L {set val 1} 
                default {set val 0} 
            }
    	    varDoSet /${ins}/lock_status -v $val
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc pt3020_setup_id_r
          proc pt3020_setup_id_r { ins } {
		set id 0
                set status [catch {insIfRead /${ins} "S3" 8} buf]
		if { $status == 0 } {
                    set id [expr {[scan $buf "S%2d" val]==1}]
		}
		varDoSet /${ins}/setup/id -v $id
	  }

