# camp_ins_ggl_alarm.tcl
# Camp driver for the ggl NIM-level used for alarms.

CAMP_INSTRUMENT /~ -D -T "GGL Alarm" -d on \
    -initProc ggl_alarm_init -deleteProc ggl_alarm_delete \
    -onlineProc ggl_alarm_online -offlineProc ggl_alarm_offline

  proc ggl_alarm_init { ins } { 
    insSet /${ins} -if vme 0.0 32768
  }
  proc ggl_alarm_delete { ins } {
      insSet /${ins} -line off
  }
  proc ggl_alarm_online { ins } {
      insIfOn /${ins}
      if { [catch {
	  varRead /${ins}/bit
      } ] } {
	  insIfOff /${ins}
	  return -code error "Failed to read bit state"
      }
  }
  proc ggl_alarm_offline { ins } {
      insIfOff /${ins}
  }

CAMP_INT /~/bit -D -S -R -P -L -T "Nim level bit" \
    -d on -s on -r on -p off -p_int 15 \
    -readProc ggl_alarm_bit_r -writeProc ggl_alarm_bit_w
  proc ggl_alarm_bit_w { ins val } {
      if { $val } { set val 1 }
      insIfWrite /${ins} "0x15 byte ${val}"
      ggl_alarm_bit_r ${ins}
  }
  proc ggl_alarm_bit_r { ins } {
      set val [insIfRead /${ins} "0x15 byte" 15]
      set val [expr {$val & 1}]
      varDoSet /${ins}/bit -v $val
  }

