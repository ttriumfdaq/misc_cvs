CAMP_INSTRUMENT /~ -D -T "LakeShore 234" \
    -H "LakeShore 234 Temperature Transmitter" -d on \
    -initProc lake234_init -deleteProc lake234_delete \
    -onlineProc lake234_online -offlineProc lake234_offline
  proc lake234_init { ins } {
	insSet /${ins} -if rs232 0.5 txa0: 9600 8 none 1 CRLF CRLF 2
  }
  proc lake234_delete { ins } {
	insSet /${ins} -line off
  }
  proc lake234_online { ins } {
        insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
  }
  proc lake234_offline { ins } {
	insIfOff /${ins}
  }
    CAMP_FLOAT /~/temp -D -R -P -L -T "Temperature" \
	-d on -r on -units K \
	-readProc lake234_temp_r
      proc lake234_temp_r { ins } {
        set res [string trim [insIfRead /${ins} "TEMP?" 32]]
        if { [string first OPEN $res] != -1 } {
          return -code error "Signal error"
 	} 
	if { [scan $res %f val] != 1 } { 
          return -code error "failed reading instrument $ins" 
        }
        varDoSet /${ins}/temp -v $val
      }
    CAMP_FLOAT /~/ohms -D -R -P -L -T "Sensor Resistance" \
	-d on -r on -units ohms \
	-readProc lake234_ohms_r
      proc lake234_ohms_r { ins } {
	set res [string trim [insIfRead /${ins} "OHMS?" 32]]
        if { [string first OPEN $res] != -1 } {
          return -code error "Signal error"
 	} 
	if { [scan $res %f val] != 1 } { 
          return -code error "failed reading instrument $ins" 
        }
        varDoSet /${ins}/ohms -v $val
      }
    CAMP_FLOAT /~/logr -D -R -P -L -T "Log10 of Sensor Resistance" \
	-d on -r on \
	-readProc lake234_logr_r
      proc lake234_logr_r { ins } {
	set res [string trim [insIfRead /${ins} "LOGR?" 32]]
        if { [string first OPEN $res] != -1 } {
          return -code error "Signal error"
 	} 
	if { [scan $res %f val] != 1 } { 
          return -code error "failed reading instrument $ins" 
        }
        varDoSet /${ins}/logr -v $val
      }
    CAMP_SELECT /~/current -D -S -T "Current Output" \
	-d on -s on -selections 0 4 20 \
        -writeProc lake234_current_w
      proc lake234_current_w { ins target } {
	insIfWrite /${ins} "DA$target"
      }
    CAMP_STRUCT /~/setup -D -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc lake234_id_r
          proc lake234_id_r { ins } {
		set id 0
		set status [catch {insIfRead /${ins} "*IDN?" 32} buf]
		if { $status == 0 } {
		    set id [scan $buf " LSCI,MODEL234,%*d,%d" val]
		    if { $id != 1 } { set id 0 }
		}
		varDoSet /${ins}/setup/id -v $id
	  }
        CAMP_INT /~/setup/range -D -R -T "Temperature Range" \
	    -d on -r on \
            -readProc lake234_range_r
          proc lake234_range_r { ins } {
 	    insIfReadVerify /${ins} "RANGE?" 32 /${ins}/setup/range " %1d" 2
          }
        CAMP_SELECT /~/setup/units -D -R -T "Output Units" \
	    -d on -r on -selections T R \
            -readProc lake234_units_r
          proc lake234_units_r { ins } {
 	    insIfReadVerify /${ins} "UNITS?" 32 /${ins}/setup/units " %s" 2
          }
        CAMP_INT /~/setup/scale -D -R -T "Resistance Scale" \
	    -d on -r on \
            -readProc lake234_scale_r
          proc lake234_scale_r { ins } {
 	    insIfReadVerify /${ins} "SCALE?" 32 /${ins}/setup/scale " %1d" 2
          }
        CAMP_SELECT /~/setup/cal -D -S -T "Calibrate Input" \
	    -d on -s on -selections 0 1 2 3 4 5 \
            -writeProc lake234_cal_w
          proc lake234_cal_w { ins target } {
 	    insIfWrite /${ins} "CAL$target"
          }
	CAMP_INT /~/setup/curve -D -R -S -T "Curve" \
	    -d on -r on -s on \
	    -readProc lake234_curve_r -writeProc lake234_curve_w
          proc lake234_curve_r { ins } { 
            varDoSet /${ins}/controls/do_read -p on -p_int 1
          }
	  proc lake234_curve_w { ins target } { 
            varDoSet /${ins}/setup/curve -v $target
            varDoSet /${ins}/controls/do_load -p on -p_int 1
          }
    CAMP_STRUCT /~/controls -D -d on
	CAMP_SELECT /~/controls/do_load -D -P -T "Do curve load" -d on \
	    -selections DONE LOADING -v 0 \
	    -readProc lake234_do_load_r
          proc lake234_do_load_r { ins } {
            varDoSet /${ins}/controls/do_load -v 1 -p off
            lake234_set_curve $ins [varGetVal /${ins}/setup/curve]
            varDoSet /${ins}/controls/do_read -p on -p_int 1
            varDoSet /${ins}/controls/do_load -v 0
          }
	CAMP_SELECT /~/controls/do_read -D -P -T "Do curve readback" -d on \
	    -selections DONE READING -v 0 \
	    -readProc lake234_do_read_r
          proc lake234_do_read_r { ins } {
            varDoSet /${ins}/controls/do_read -v 1 -p off
            lake234_read_curve $ins 
            varDoSet /${ins}/controls/do_read -v 0
          }
        CAMP_STRING /~/controls/curve_file -D -S -T "Curve filename" \
            -d on -s on -writeProc lake234_controls_file_w
          proc lake234_controls_file_w { ins target } {
            varDoSet /${ins}/controls/curve_file -v $target
          }
    CAMP_STRUCT /~/panic -D -d on
	CAMP_SELECT /~/panic/reset -D -S -T "Reset" -d on -s on \
	    -selections DO \
	    -writeProc lake234_reset_w
          proc lake234_reset_w { ins target } {	
            insIfWrite /${ins} "*RST" 
          }
proc lake234_set_curve { ins curve } {
    if { $curve < 0 } { return -code error "bad value \"$curve\"" }
    set buf [glob ./dat/*.spl ./dat/*.dat]
    set found 0
    foreach file $buf {
        set root [file rootname [file tail $file]]
        set extension [file extension $file]
	if { $root == $curve } { set found 1; break }
    }
    if { $found == 0 } { return -code error "couldn't find curve \"$curve\"" }
    insIfWrite /${ins} [format "SID %8d" $curve]
    set file [format "./dat/%d%s" $curve $extension]
    varSet /${ins}/controls/curve_file -v $file
    lake234_curve_file_w $ins $file
}
proc lake234_read_curve { ins } {
    set buf [insIfRead /${ins} "SID?" 32]
    if { [scan $buf " %d" curve] != 1 } { 
      varDoSet /${ins}/setup/curve -v -1
      return -code error "failed parsing curve data" 
    }
    set buf [glob ./dat/*.spl ./dat/*.dat]
    set found 0
    foreach file $buf {
        set root [file rootname [file tail $file]]
        set extension [file extension $file]
	if { $root == $curve } { set found 1; break }
    }
    if { $found == 0 } { return -code error "couldn't find curve \"$curve\"" }
    set file [format "./dat/%d%s" $curve $extension]
    varSet /${ins}/controls/curve_file -v $file
    set f [open $file r]
    # read first line (has the number of data points)
    if { [gets $f line] == -1 } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
	return -code error "unexpected end of file"
    }
    set n 0
    if { [scan $line " %d" n] != 1 } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
        return -code error "invalid file format: no number of points"
    }
    set v 0; set tf 0; set ti 0
    for { set i 0 } { $i < $n } { incr i } {
      if { [gets $f line] == -1 } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
        return -code error "unexpected end of file"
      }
      if { [scan $line " %f %f" tf v] != 2 } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
        return -code error "invalid file format: while scanning data line"
      }
      set tf [format "%.3f" $tf]
      set buf [insIfRead /${ins} [format "C?%.2f" $v] 32]
      if { [scan $buf " %f" ti] != 1 } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
        return -code error "error reading back curve point"
      }
      set ti [format "%.3f" $ti]
      if { $tf != $ti } {
        close $f
        varDoSet /${ins}/setup/curve -v -1
        return -code error [format "inconsistent readback: log(R)=%.2f T=%.3fK read=%.3fK" $v $tf $ti]
      }
    }
    close $f
    varDoSet /${ins}/setup/curve -v $curve
}
proc lake234_curve_file_w { ins target } {
    set file $target
    if { [string match "*.dat" $file] == 1 } {
	set file_type "dat"
    } elseif { [string match "*.spl" $file] == 1 } {
	set file_type "spl"
    } else {
	return -code error "invalid file type $file"
    }
    set f [open $file r]
    # read first line (has the number of data points)
    if { [gets $f line] == -1 } {
	return -code error "unexpected end of file"
    }
    # erase the curve in memory
    insIfWrite /${ins} "ERASE!"
    # found it necessary to wait a LONG time for the
    # erase to finish
    sleep 20.0
    # read and send each point
    set t 0; set v 0
    while { [gets $f line] >= 0 } {
	scan $line " %f %f" t v
	insIfWrite /${ins} [format "C%.2f,%.3f" $v $t]
    }
    close $f
}
