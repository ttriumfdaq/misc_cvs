# camp_ins_showcase.tcl  --  "Showcase of Camp variables"
#
# Donald Arseneau 
#
# Modified:  21-Feb-2000    DA     first releasable version
#
# This is a bunch of links with some routines for managing them.
# Unused links have a null link target, and are make invisible (-d off).
#
# Notes:  
# o lappend does not appear to work, merely concatenate strings instead:
#        set ${ins}LINKS(used) "[set ${ins}LINKS(used)] ${v}"
# o message texts of the form "alias: ..." cause camp_cui to display "..." 
#   as the apparent variable name (special support from camp_cui because
#   one can't rename camp variables).
# o unused links are linked to "" since there is no official way to unset
#   a link.
# o The construct [set ${ins}LINKS(used)] returns the value of the global
#   array element xxxLINKS(used), where "xxx" would be the camp instrument
#   name.  This use of global variable names avoids conflicts when more than
#   one showcase is set up in a non-multithreaded camp server.  (Both mvme
#   and VMS camp servers are multithreaded, so this may be wasted effort.)
#   In particular, study the example given above for appending.
# o The "info" variables contribute useful help messages, but their real
#   purpose is that the camp_cui fails when the first variable on a page
#   is not displayed, and can hang when there are no displayed variables.

CAMP_INSTRUMENT /~ -D -T "Showcase of Camp variables" \
    -d on \
    -initProc shc_init -deleteProc shc_delete \
    -onlineProc shc_init -offlineProc shc_delete
  proc shc_init { ins } {
      global ${ins}LINKS
      set ${ins}LINKS(integers) "link_i0 link_i1 link_i2 link_i3 link_i4 link_i5 link_i6 link_i7"
      set ${ins}LINKS(floats) "link_f0 link_f1 link_f2 link_f3 link_f4 link_f5 link_f6 link_f7 link_f8 link_f9"
      set ${ins}LINKS(i_avail) [set ${ins}LINKS(integers)]
      set ${ins}LINKS(f_avail) [set ${ins}LINKS(floats)]
      set ${ins}LINKS(used) ""
      insSet /${ins} -if none 0.0
      shc_online ${ins}
    }
  proc shc_delete { ins } {
      global ${ins}LINKS
      insIfOff /${ins}
      unset ${ins}LINKS
    }
  proc shc_online { ins } {
      insIfOn /${ins}
      shc_validate ${ins}
      varDoSet /${ins}/setup/validate -p on -p_int 120
    }
  proc shc_offline { ins } {
      insIfOff /${ins}
    }


CAMP_STRUCT /~/setup -D -T "Setup Links" -d on

  CAMP_STRING /~/setup/add -D -S -T "Add to showcase" \
        -H "Add a variable to the showcase; Please specify the full Camp path" \
        -d on -s on -writeProc shc_add_w
    proc shc_add_w { ins target } {
        global ${ins}LINKS
        if { [ string first "/" $target ] != 0 } { 
	    set target "/$target" 
	}
        if { [ catch {varNumGetSum $target} ] != 0 } {
            return -code error "Camp varible $target is not numeric or does not exist"
        }
	set t [varGetVal $target]
        if { [string first . $t] < 0 } {
            set t i
            set type integer
        } else {
            set t f
            set type float
	}
	if { [ llength [set ${ins}LINKS(${t}_avail)] ] < 1 } {
            return -code error "There are no availiable ${type} link variables"
	}
        set v [ lindex [set ${ins}LINKS(${t}_avail)] 0 ]
#       determine a useful alias name: the target variable's path with "_" instead of
#       "/".  If it's too long, use only trailing parts of name.  User can set it later.
	set tl [ split $target "/" ]
	for { set i 1 } { [ string length [ set ts [join [lrange $tl $i end] _ ] ] ] > 13 } { incr i } {}
	if { [ string length $ts ] > 0 } { varDoSet /${ins}/${v} -m "alias: $ts" }
	varDoSet /${ins}/${v} -d on
        lnkSet /${ins}/${v} $target
	varDoSet /${ins}/setup/remove/${v} -d on -s on -v $ts
	varDoSet /${ins}/setup/alias/${v} -d on -s on -v $ts
	set ${ins}LINKS(${t}_avail) [ lrange [set ${ins}LINKS(${t}_avail)] 1 end ]
        set ${ins}LINKS(used) "[set ${ins}LINKS(used)] ${v}"
    }

  CAMP_STRUCT /~/setup/remove -D -T "Remove Links" -d on

    CAMP_STRING /~/setup/remove/info -D -T "Information" -d on -v " " \
            -H "Remove variables from the showcase by setting any variables below to a null value (or anything)."

    CAMP_STRING /~/setup/remove/link_f0 -D -S -T "Unset link_f0" \
	    -d off -s off -v " " -writeProc shc_remove_f0_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f0_w { ins target } {
	  shc_remove $ins f 0
       }
    CAMP_STRING /~/setup/remove/link_f1 -D -S -T "Unset link_f1" \
	    -d off -s off -v " " -writeProc shc_remove_f1_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f1_w { ins target } {
	  shc_remove $ins f 1
       }
    CAMP_STRING /~/setup/remove/link_f2 -D -S -T "Unset link_f2" \
	    -d off -s off -v " " -writeProc shc_remove_f2_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f2_w { ins target } {
	  shc_remove $ins f 2
       }
    CAMP_STRING /~/setup/remove/link_f3 -D -S -T "Unset link_f3" \
	    -d off -s off -v " " -writeProc shc_remove_f3_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f3_w { ins target } {
	  shc_remove $ins f 3
       }
    CAMP_STRING /~/setup/remove/link_f4 -D -S -T "Unset link_f4" \
	    -d off -s off -v " " -writeProc shc_remove_f4_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f4_w { ins target } {
	  shc_remove $ins f 4
       }
    CAMP_STRING /~/setup/remove/link_f5 -D -S -T "Unset link_f5" \
	    -d off -s off -v " " -writeProc shc_remove_f5_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f5_w { ins target } {
	  shc_remove $ins f 5
       }
    CAMP_STRING /~/setup/remove/link_f6 -D -S -T "Unset link_f6" \
	    -d off -s off -v " " -writeProc shc_remove_f6_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f6_w { ins target } {
	  shc_remove $ins f 6
       }
    CAMP_STRING /~/setup/remove/link_f7 -D -S -T "Unset link_f7" \
	    -d off -s off -v " " -writeProc shc_remove_f7_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f7_w { ins target } {
	  shc_remove $ins f 7
       }
    CAMP_STRING /~/setup/remove/link_f8 -D -S -T "Unset link_f8" \
	    -d off -s off -v " " -writeProc shc_remove_f8_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f8_w { ins target } {
	  shc_remove $ins f 8
       }
    CAMP_STRING /~/setup/remove/link_f9 -D -S -T "Unset link_f9" \
	    -d off -s off -v " " -writeProc shc_remove_f9_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_f9_w { ins target } {
	  shc_remove $ins f 9
       }
    CAMP_STRING /~/setup/remove/link_i0 -D -S -T "Unset link_i0" \
	    -d off -s off -v " " -writeProc shc_remove_i0_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i0_w { ins target } {
	  shc_remove $ins i 0
       }
    CAMP_STRING /~/setup/remove/link_i1 -D -S -T "Unset link_i1" \
	    -d off -s off -v " " -writeProc shc_remove_i1_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i1_w { ins target } {
	  shc_remove $ins i 1
       }
    CAMP_STRING /~/setup/remove/link_i2 -D -S -T "Unset link_i2" \
	    -d off -s off -v " " -writeProc shc_remove_i2_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i2_w { ins target } {
	  shc_remove $ins i 2
       }
    CAMP_STRING /~/setup/remove/link_i3 -D -S -T "Unset link_i3" \
	    -d off -s off -v " " -writeProc shc_remove_i3_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i3_w { ins target } {
	  shc_remove $ins i 3
       }
    CAMP_STRING /~/setup/remove/link_i4 -D -S -T "Unset link_i4" \
	    -d off -s off -v " " -writeProc shc_remove_i4_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i4_w { ins target } {
	  shc_remove $ins i 4
       }
    CAMP_STRING /~/setup/remove/link_i5 -D -S -T "Unset link_i5" \
	    -d off -s off -v " " -writeProc shc_remove_i5_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i5_w { ins target } {
	  shc_remove $ins i 5
       }
    CAMP_STRING /~/setup/remove/link_i6 -D -S -T "Unset link_i6" \
	    -d off -s off -v " " -writeProc shc_remove_i6_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i6_w { ins target } {
	  shc_remove $ins i 6
       }
    CAMP_STRING /~/setup/remove/link_i7 -D -S -T "Unset link_i7" \
	    -d off -s off -v " " -writeProc shc_remove_i7_w \
	    -H "Remove by setting this entry (to any value at all)"
       proc shc_remove_i7_w { ins target } {
	  shc_remove $ins i 7
       }
       proc shc_remove { ins t v } {
	   global ${ins}LINKS
	   set var link_${t}${v}
	   lnkSet /${ins}/${var} ""
	   varDoSet /${ins}/${var} -m "" -d off
	   varDoSet /${ins}/setup/alias/${var} -d off -s off -v ""
           varDoSet /${ins}/setup/remove/${var} -d off -s off -v ""
	   set i [ lsearch -exact [set ${ins}LINKS(${t}_avail)] $var ]
	   if { $i < 0 } {# add entry to list of available links
	       set ${ins}LINKS(${t}_avail) "[set ${ins}LINKS(${t}_avail)] ${var}"
	   }
	   set i [ lsearch -exact [set ${ins}LINKS(used)] $var ]
           if { $i >= 0 } {# remove entry from list of used links
	       set ${ins}LINKS(used) [ lreplace [set ${ins}LINKS(used)] $i $i  ]
	   }
       }


  CAMP_STRUCT /~/setup/alias -D -T "Set aliases" -d on

    CAMP_STRING /~/setup/alias/info -D -T "Information" -d on -v " " \
            -H "Assign new display-names for your showcase variables by setting any of the variables below."

    CAMP_STRING /~/setup/alias/link_f0 -D -S -T "Alias for link_f0" \
	    -d off -s off -v " " -writeProc shc_alias_f0_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f0_w { ins target } {
	  shc_alias $ins f 0 $target
       }
    CAMP_STRING /~/setup/alias/link_f1 -D -S -T "Alias for link_f1" \
	    -d off -s off -v " " -writeProc shc_alias_f1_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f1_w { ins target } {
	  shc_alias $ins f 1 $target
       }
    CAMP_STRING /~/setup/alias/link_f2 -D -S -T "Alias for link_f2" \
	    -d off -s off -v " " -writeProc shc_alias_f2_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f2_w { ins target } {
	  shc_alias $ins f 2 $target
       }
    CAMP_STRING /~/setup/alias/link_f3 -D -S -T "Alias for link_f3" \
	    -d off -s off -v " " -writeProc shc_alias_f3_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f3_w { ins target } {
	  shc_alias $ins f 3 $target
       }
    CAMP_STRING /~/setup/alias/link_f4 -D -S -T "Alias for link_f4" \
	    -d off -s off -v " " -writeProc shc_alias_f4_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f4_w { ins target } {
	  shc_alias $ins f 4 $target
       }
    CAMP_STRING /~/setup/alias/link_f5 -D -S -T "Alias for link_f5" \
	    -d off -s off -v " " -writeProc shc_alias_f5_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f5_w { ins target } {
	  shc_alias $ins f 5 $target
       }
    CAMP_STRING /~/setup/alias/link_f6 -D -S -T "Alias for link_f6" \
	    -d off -s off -v " " -writeProc shc_alias_f6_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f6_w { ins target } {
	  shc_alias $ins f 6 $target
       }
    CAMP_STRING /~/setup/alias/link_f7 -D -S -T "Alias for link_f7" \
	    -d off -s off -v " " -writeProc shc_alias_f7_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f7_w { ins target } {
	  shc_alias $ins f 7 $target
       }
    CAMP_STRING /~/setup/alias/link_f8 -D -S -T "Alias for link_f8" \
	    -d off -s off -v " " -writeProc shc_alias_f8_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f8_w { ins target } {
	  shc_alias $ins f 8 $target
       }
    CAMP_STRING /~/setup/alias/link_f9 -D -S -T "Alias for link_f9" \
	    -d off -s off -v " " -writeProc shc_alias_f9_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_f9_w { ins target } {
	  shc_alias $ins f 9 $target
       }
    CAMP_STRING /~/setup/alias/link_i0 -D -S -T "Alias for link_i0" \
	    -d off -s off -v " " -writeProc shc_alias_i0_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i0_w { ins target } {
	  shc_alias $ins i 0 $target
       }
    CAMP_STRING /~/setup/alias/link_i1 -D -S -T "Alias for link_i1" \
	    -d off -s off -v " " -writeProc shc_alias_i1_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i1_w { ins target } {
	  shc_alias $ins i 1 $target
       }
    CAMP_STRING /~/setup/alias/link_i2 -D -S -T "Alias for link_i2" \
	    -d off -s off -v " " -writeProc shc_alias_i2_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i2_w { ins target } {
	  shc_alias $ins i 2 $target
       }
    CAMP_STRING /~/setup/alias/link_i3 -D -S -T "Alias for link_i3" \
	    -d off -s off -v " " -writeProc shc_alias_i3_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i3_w { ins target } {
	  shc_alias $ins i 3 $target
       }
    CAMP_STRING /~/setup/alias/link_i4 -D -S -T "Alias for link_i4" \
	    -d off -s off -v " " -writeProc shc_alias_i4_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i4_w { ins target } {
	  shc_alias $ins i 4 $target
       }
    CAMP_STRING /~/setup/alias/link_i5 -D -S -T "Alias for link_i5" \
	    -d off -s off -v " " -writeProc shc_alias_i5_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i5_w { ins target } {
	  shc_alias $ins i 5 $target
       }
    CAMP_STRING /~/setup/alias/link_i6 -D -S -T "Alias for link_i6" \
	    -d off -s off -v " " -writeProc shc_alias_i6_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i6_w { ins target } {
	  shc_alias $ins i 6 $target
       }
    CAMP_STRING /~/setup/alias/link_i7 -D -S -T "Alias for link_i7" \
	    -d off -s off -v " " -writeProc shc_alias_i7_w \
	    -H "Set this to change the displayed alias"
       proc shc_alias_i7_w { ins target } {
	  shc_alias $ins i 7 $target
       }
       proc shc_alias { ins t v alias } {
	   global ${ins}LINKS
	   set var link_${t}${v}
	   set p [lnkGetPath /${ins}/${var}]
	   lnkSet /${ins}/${var} ""
	   varDoSet /${ins}/${var} -m "alias: ${alias}"
	   lnkSet /${ins}/${var} $p
	   varDoSet /${ins}/setup/alias/${var} -v $alias
	   varDoSet /${ins}/setup/remove/${var} -v $alias
       }

CAMP_SELECT /~/setup/validate -D -S -R -P -T "Validate links" \
        -d on -s on -r on -p on -p_int 90 -selections "VALIDATE" \
        -H "Set read or poll this to validate all links" \
        -readProc shc_validate -writeProc shc_validate_w
      proc shc_validate_w { ins target } { shc_validate ${ins} }
      proc shc_validate { ins } {
	global ${ins}LINKS
	# loop over all links, either type; lv = link var, fi = "f" or "i", 
	# targ = link target var, 
        foreach lv "[set ${ins}LINKS(integers)] [set ${ins}LINKS(floats)]" {
	  scan $lv {link_%[fi]} fi
          set targ ""
	  set s [ catch {lnkGetPath /${ins}/${lv} } targ ]
	  varDoSet /${ins}/setup/validate -m "Try lnkGetPath /${ins}/${lv}: $s, $targ "
          if { [ string compare $targ "" ] == 0 } {
             set s 1
          } else {
             if { [ catch {varNumGetSum $targ} ] != 0 } {
                 set s 1
             }
          }
          if { $s == 0 } {# variable is linked, ensure it is listed as such
	      # Ensure it is in "used" list:
	      set i [ lsearch -exact [set ${ins}LINKS(used)] $lv ]
	      if { $i < 0 } {# not listed as used, so add it to list
		  set ${ins}LINKS(used) "[set ${ins}LINKS(used)] ${lv}"
	      }
	      # Ensure it is NOT in "available" list:
	      set i [ lsearch -exact [set ${ins}LINKS(${fi}_avail)] $lv ]
	      if { $i >= 0 } {# listed as available; remove it
		  set ${ins}LINKS(${fi}_avail) [ lreplace [set ${ins}LINKS(${fi}_avail)] $i $i ]
	      }
	      # Ensure it is displayed, and uses its alias
	      lnkSet /${ins}/${lv} ""
	      set al [varGetVal /${ins}/setup/alias/${lv}]
	      if { [string compare $al ""] } {# alias not null, so use it
		  set al "alias: $al"
	      }
	      varDoSet /${ins}/${lv} -d on -m $al
	      lnkSet /${ins}/${lv} $targ
	      # Ensure remove & alias functions are enabled
	      varDoSet /${ins}/setup/remove/${lv} -d on -s on
	      varDoSet /${ins}/setup/alias/${lv} -d on -s on
	  } else {# no link for this variable, remove it if necessary
	      varSet /${ins}/setup/remove/${lv} -v ""
          }
        }
      }

CAMP_LINK /~/link_f0 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f1 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f2 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f3 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f4 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f5 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f6 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f7 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f8 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_f9 -D -T "Float link" -d off -varType CAMP_FLOAT

CAMP_LINK /~/link_i0 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i1 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i2 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i3 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i4 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i5 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i6 -D -T "Integer link" -d off -varType CAMP_INT

CAMP_LINK /~/link_i7 -D -T "Integer link" -d off -varType CAMP_INT

