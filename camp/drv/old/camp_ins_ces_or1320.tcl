CAMP_INSTRUMENT /~ -D -T "CES OR1320 Output Register" \
    -H "CES OR1320 Output Register" -d on \
    -initProc or1320_init \
    -deleteProc or1320_delete \
    -onlineProc or1320_online \
    -offlineProc or1320_offline 
  proc or1320_init { ins } { insSet /${ins} -if camac 0.0 0 0 0 } 
  proc or1320_delete { ins } { insSet /${ins} -line off }
  proc or1320_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc or1320_offline { ins } { insIfOff /${ins} }
    CAMP_INT /~/output_set -D -S -R -L -P -T "Output setting" -d on -s on -r on \
      -writeProc or1320_set_w -readProc or1320_set_r
      proc or1320_set_w { ins target } {
	set target [expr $target & 0xffff]
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
	insIfWrite /${ins} "cssa 16 $ext target q"
        varRead /${ins}/output_set
      }
      proc or1320_set_r { ins } {
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
	insIfRead /${ins} "cssa 0 $ext val q" 0
        for { set i 0 } { $i <= 15 } { incr i } {
	  varDoSet /${ins}/output${i} -v [expr ($val>>$i)&1]
        }
	varDoSet /${ins}/output_set -v $val
      }
    for { set i 0 } { $i <= 15 } { incr i } {
      CAMP_INT /~/output${i} -D -S -R -L -P -T "Output $i" -d on -s on -r on \
	  -writeProc or1320_o${i}_w -readProc or1320_o${i}_r
        proc or1320_o${i}_r { ins } "varRead /\${ins}/output_set"
        proc or1320_o${i}_w { ins target } "or1320_o_w \$ins \$target $i"
    }
      proc or1320_o_w { ins target i } {
	varRead /${ins}/output_set
        set val [varGetVal /${ins}/output_set]
        if { $target } { set val [expr $val | (1<<$i)] 
        } else { set val [expr $val & ~(1<<$i)] }
	varSet /${ins}/output_set -v $val
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
      CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc or1320_id_r
        proc or1320_id_r { ins } {
          set val 0
          # Try to read output
          cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
          if { [catch { insIfRead /${ins} "cssa 0 $ext val q" 0 }] == 0 } { 
            set val 1
          }
	  varDoSet /${ins}/setup/id -v $val
        }
    CAMP_STRUCT /~/controls -D -T "Control variables" -d on
      CAMP_SELECT /~/controls/clear_all -D -S -T "Clear all outputs" \
        -d on -s on -selections do_it -writeProc or1320_clear_all
        proc or1320_clear_all { ins target } {
	  cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
          insIfWrite /${ins} "cssa 9 $ext val q"
	  varRead /${ins}/output_set
        }
