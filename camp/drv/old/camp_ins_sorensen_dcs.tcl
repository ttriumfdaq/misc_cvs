# camp_ins_sorensen_dcs.tcl 
# Camp Tcl instrument driver for Sorensen DCS series of power supplies.
# Donald Arseneau,  TRIUMF
#
# $Log$
# Revision 1.1  2007/02/25 03:33:04  asnd
# Initial version
#


CAMP_INSTRUMENT /~ -D -T "Sorensen DCS" \
    -H "Sorensen DCS power supply" \
    -d on \
    -initProc dcs_init \
    -deleteProc dcs_delete \
    -onlineProc dcs_online \
    -offlineProc dcs_offline

# Here are functions to do insIfReadVerify and insIfWriteVerify, but read and report
# instrument's error message on failure
#
proc dcsWriteVerify { pins setc readc buff var fmt ntry target } {
  if { [catch { insIfWriteVerify $pins "*CLS;$setc" $readc $buff $var $fmt $ntry $target } m ] } {
      if { [catch {insIfRead $pins "SYST:ERR?" 80} err] == 0} {
          if { [scan $err "%d, %\[^\r]" n m ] == 2 } {
              if { $n < 0 } { return -code error "($n) $m" }
          }
      }
      return -code error $m
  }
}

proc dcsReadVerify { pins readc buff var fmt ntry } {
  if { [catch { insIfReadVerify $pins "*CLS;$readc" $buff $var $fmt $ntry } m ] } {
      if { [catch {insIfRead $pins "SYST:ERR?" 80} err] == 0} {
          if { [scan $err "%d, %\[^\r]" n m ] == 2 } {
              if { $n < 0 } { return -code error "($n) $m" }
          }
      }
      return -code error $m
  }
}


proc dcs_init { ins } {
    insSet /${ins} -if tcpip 0.1 142.90.101.141 9221 CRLF CRLF 1
}
proc dcs_delete { ins } {
    insSet /${ins} -line off
}
proc dcs_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == "?" || [catch {varRead /${ins}/status}] } {
        insIfOff /${ins}
        return -code error "failed ID query, check interface definition and connections"
    }
}
proc dcs_offline { ins } {
    insIfOff /${ins}
}

# Polarity is ignored here because the units seem not to have polarity 
# reversal, despite having a remote "polarity" setting. To support
# polarity, must detect reversal of sign, in which case disable output,
# reverse polarity, enable output.

CAMP_FLOAT /~/curr_set -D -S -R -P -L -T "Set current" \
    -d on -s on -r on -units A \
    -readProc dcs_curr_set_r -writeProc dcs_curr_set_w

proc dcs_curr_set_r { ins } {
    dcsReadVerify /${ins} "SOUR:CURR?" 32 /${ins}/curr_set " %f" 2
}

proc dcs_curr_set_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /${ins}/setup/maximum_curr] } {
        return -code error "Current out of range"
    }
    set t [varGetVal /${ins}/setup/ramp_time]
    if { $t < 0.1 } {
        dcsWriteVerify /${ins} "SOUR:CURR $target" "SOUR:CURR?" 32 \
            /${ins}/curr_set " %f" 2 $target
    } else {
        set t [format $t %.1f]
        dcsWriteVerify /${ins} "SOUR:CURR:RAMP $target $t" "SOUR:CURR?" 32 \
            /${ins}/curr_set " %f" 2 $target
        # immediately read status to report ramping
        varRead /${ins}/status
    }
}

CAMP_FLOAT /~/curr_read -D -R -P -L -A -T "Read current" \
	-H "Read power supply output current" \
	-d on -r on -units A -tol 0.003 -readProc dcs_curr_read_r

proc dcs_curr_read_r { ins } {
    dcsReadVerify /${ins} "MEAS:CURR?" 32 /${ins}/curr_read " %f" 2
    varTestAlert /${ins}/curr_read [varGetVal /${ins}/curr_set]
}


CAMP_FLOAT /~/volt_set -D -S -R -P -L -T "Set voltage" \
    -d on -s on -r on -units V \
    -readProc dcs_volt_set_r -writeProc dcs_volt_set_w

proc dcs_volt_set_r { ins } {
    dcsReadVerify /${ins} "SOUR:VOLT?" 32 /${ins}/volt_set " %f" 2
}

proc dcs_volt_set_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /${ins}/setup/maximum_volt] } {
        return -code error "Voltage out of range"
    }
    set t [varGetVal /${ins}/setup/ramp_time]
    if { $t < 0.1 } {
        dcsWriteVerify /${ins} "SOUR:VOLT $target" "SOUR:VOLT?" 32 \
            /${ins}/volt_set " %f" 2 $target
    } else {
        set t [format $t %.1f]
        dcsWriteVerify /${ins} "SOUR:VOLT:RAMP $target $t" "SOUR:VOLT?" 32 \
            /${ins}/volt_set " %f" 2 $target
        # immediately read status to report ramping
        varRead /${ins}/status
    }
}

CAMP_FLOAT /~/volt_read -D -R -P -L -A -T "Read output voltage" \
	-H "Read power supply output voltage" \
	-d on -r on -units V -tol 0.003 -readProc dcs_volt_read_r

proc dcs_volt_read_r { ins } {
    dcsReadVerify /${ins} "MEAS:VOLT?" 32 /${ins}/volt_read " %f" 2
    varTestAlert /${ins}/volt_read [varGetVal /${ins}/volt_set]
}


CAMP_SELECT /~/output -D -S -R -P -T "Output" \
    -H "Output - enabled or disabled" \
    -selections {Disable} {Enable} \
    -d on -s on -r on \
    -readProc dcs_status_r -writeProc dcs_output_w

proc dcs_output_w { ins target } {
    dcsWriteVerify /${ins} "OUTP:STATE $target" "OUTP:STATE?" 32 \
            /${ins}/output " %d" 2 $target
}

CAMP_SELECT /~/status -D -R -P -T "Status" \
    -H "Indicator for power supply status" \
    -selections {Hold current} {Hold voltage} {Ramping} {Shutdown} {Overvoltage} {Overtemp} {Isolated} \
    -d on -r on -p on -p_int 10 \
    -readProc dcs_status_r

# Read status.  The status block contains most information in one readback
# (sf = status flags, sr = status register) but the ramp status must be read
# separately.

proc dcs_status_r { ins } {
    set buf [insIfRead /$ins "SOUR:STAT:BLOCK?" 200]
    if { [scan $buf {%d,%d,%x,%x,%x,%x,%x,%x,%[^,],%f,%f,%f,} cn ol sf sr as fmr fr er sn mv mc mov] != 12 } {
        return -code error "Failed to read status"
    }
    varDoSet /${ins}/setup/maximum_volt -v $mv
    varDoSet /${ins}/setup/maximum_curr -v $mc
    varDoSet /${ins}/output -v [expr { ($sf & 0x800) ? 1 : 0 }]
    if { $sr & 0x08 } {
        set stat "Overvoltage"
    } elseif { $sr & 0x10 } {
        set stat "Overtemp"
    } elseif { ($sr & 0x20) || ! ($sf & 0x800) } {
        set stat "Shutdown"
    } elseif { ($sf & 0x080) == 0 } {
        set stat "Isolated"
    } else { 
        # When ramping, *both* SOUR:CURR:RAMP and SOUR:VOLT:RAMP say so, so we
        # only need to read one of them.
        set buf [insIfRead /$ins "SOUR:CURR:RAMP?" 32]
        set r 0
        scan $buf %d r
        if { $r } {
            set stat "Ramping"
        } elseif { $sr & 0x01 } {
            set stat "Hold voltage"
        } else {
            set stat "Hold current"
        }
    }
    varDoSet /${ins}/status -v $stat
}

#
#----------------------  S E T U P  --------------------------------------------------
#
CAMP_STRUCT /~/setup -D -d on -T "Setup variables" \
    -H "Set power supply parameters here"

CAMP_STRING /~/setup/id -D -R -T "ID Query" -d on -r on \
    -v "?" -readProc dcs_id_r

proc dcs_id_r { ins } {
    set id 0
    set status [catch {insIfRead /${ins} "*IDN?" 80} buf]
    if { $status == 0 } {
        set id [scan $buf " SORENSEN,DCS%d-%d," V I]
    }
    if { $id != 2 } { # Retry once
        set status [catch {insIfRead /${ins} "*IDN?" 80} buf]
        if { $status == 0 } {
            set id [scan $buf " SORENSEN,DCS%d-%d," V I]
        }
    }
    if { $id == 2 } {
        varDoSet /${ins}/setup/id -v "DCS$V-$I"
    } else {
        varDoSet /${ins}/setup/id -v "?"
    }
}


CAMP_FLOAT /~/setup/curr_limit -D -S -R -T "Soft current limit" \
    -d on -s on -r on -units A \
    -readProc dcs_curr_lim_r -writeProc dcs_curr_lim_w

proc dcs_curr_lim_r { ins } {
    dcsReadVerify /${ins} "SOUR:CURR:LIMIT?" 80 /${ins}/setup/curr_limit " %f" 2
}

proc dcs_curr_lim_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /${ins}/setup/maximum_curr] } {
        return -code error "Current out of range"
    }
    dcsWriteVerify /${ins} "SOUR:CURR:LIMIT $target" "SOUR:CURR:LIMIT?" 32 \
        /${ins}/setup/curr_limit " %f" 2 $target
}

CAMP_FLOAT /~/setup/volt_limit -D -S -R -T "Soft voltage limit" \
    -d on -s on -r on -units V \
    -readProc dcs_volt_lim_r -writeProc dcs_volt_lim_w

proc dcs_volt_lim_r { ins } {
    dcsReadVerify /${ins} "SOUR:VOLT:LIMIT?" 80 /${ins}/setup/volt_limit " %f" 2
}

proc dcs_volt_lim_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /${ins}/setup/maximum_volt] } {
        return -code error "Voltage out of range"
    }
    dcsWriteVerify /${ins} "SOUR:VOLT:LIMIT $target" "SOUR:VOLT:LIMIT?" 32 \
        /${ins}/setup/volt_limit " %f" 2 $target
}

CAMP_FLOAT /~/setup/maximum_curr -D -T "Hard current limit" \
    -d on -units A

CAMP_FLOAT /~/setup/maximum_volt -D -T "Hard voltage limit" \
    -d on -units V


CAMP_FLOAT /~/setup/ramp_time -D -S -L -T "Ramp time" \
    -H "Time, in seconds, taken to apply any setting change (current or voltage)" \
    -d on -s on -v 0.0 -units "s" \
    -writeProc dcs_ramp_time_w

proc dcs_ramp_time_w { ins target } {
    if { $target > 99.0 || $target < 0.0 } {
        return -code error "Ramp times must be in range 0 to 99 sec"
    }
    varDoSet /${ins}/setup/ramp_time -v $target
}




