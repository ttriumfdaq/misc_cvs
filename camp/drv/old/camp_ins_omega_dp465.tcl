#  The Omega DP465 "Digital Thermometer" (thermocouple reader).
#
#  This device outputs a continuous stream of temperature values formatted 
#  like  "+ 310. C<CR>+ 311. C<CR>+ 311. C<CR>...", and the aim is to read
#  the freshest part of this stream.
#
#  The previous version of this driver used no read terminator and
#  a negative timeout in the interface. For every temperature reading,
#  it first read a 512 byte block of data to clear out the typeahead
#  buffer, and was very slow (because it doesn't finish until the buffer
#  is full!) 
# 
#  A zero or very small negative timeout (-0.1) should work for reading 
#  whatever is in the typeahead buffer quickly, but it necessarily returns
#  with an error status (timeout).  I can try that later if need be.
#
#  An ordinary positive timeout causes the type-ahead buffer to be flushed 
#  before reading, so I will use that.  We are likely to start reading in
#  the middle of a number, so I will us *no* terminator and read enough 
#  characters to ensure one whole readout is captured.
#
#  Donald Arseneau            May 2000
#
CAMP_INSTRUMENT /~ -D -T "Omega DP465 Digital Thermometer" -d on \
    -initProc dp465_init \
    -deleteProc dp465_delete \
    -onlineProc dp465_online \
    -offlineProc dp465_offline
  proc dp465_init { ins } {
#   default interface: rs232, zero delay, port 2 because port 1 can't
#   do two stop bits, 1200 bps, 7 bits per char, no parity, 2 stop bits
#   no read terminator, no write terminator, 2 second timeout
    insSet /${ins} -if rs232 0.0 /tyCo/2 1200 7 none 2 none none 2
  }
  proc dp465_delete { ins } { insSet /${ins} -line off }
  proc dp465_online { ins } { 
    insIfOn /${ins} 
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc dp465_offline { ins } { insIfOff /${ins} }

  CAMP_FLOAT /~/temp -D -R -P -L -T "Temp Reading" -d on -r on -units K \
	-readProc dp465_temp_r
      proc dp465_temp_r { ins } {
#       Read enough for two numbers:
        set buf [insIfRead /${ins} "" 18]
        set ind0 [string first {+} $buf]
        if { $ind0 == -1 } {
          set ind0 [string first "-" $buf]
          if { $ind0 == -1 } { return -code error "bad reading from DP465" }
        }
	set ind1 [expr $ind0+7]
        set message [string range $buf $ind0 $ind1]
        set status [scan $message {%[+-] %f C} sign val]
        if { $status != 2 } { return -code error "bad reading '$message' from DP465" }
	set val [expr 273 $sign $val]
        varDoSet /${ins}/temp -v $val
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
        CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
            -selections FALSE TRUE \
            -readProc dp465_id_r
          proc dp465_id_r { ins } {
              set status [catch {varRead /${ins}/temp}]
              varDoSet /${ins}/setup/id -v [expr !$status]
          }
