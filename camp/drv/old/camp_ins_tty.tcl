CAMP_INSTRUMENT /~ -D -T "TTY" \
    -H "Dumb terminal, for testing" \
    -d on \
    -initProc tty_init -deleteProc tty_delete \
    -onlineProc tty_online -offlineProc tty_offline
  proc tty_init { ins } {
	insSet /${ins} -if rs232 0.5 /tyCo/3 9600 8 none 1 CR CRLF 18
  }
  proc tty_delete { ins } {
	insSet /${ins} -line off
  }
  proc tty_online { ins } {
	global tty_f
	insIfOn /${ins}
	set tty_f [open "dat/dump/ttylog.dat" w]
  }
  proc tty_offline { ins } {
	global tty_f
	insIfOff /${ins}
	close $tty_f
  }
    CAMP_STRING /~/answer -D -R -T "Answer back" \
	-H "Read from Terminal" -m "The message" \
	-d on -r on -readProc tty_answer_r
      proc tty_answer_r { ins } {
	global tty_f
	set n [varGetVal /${ins}/num]
	incr n
	set s [insIfRead /${ins} "" 10]
	puts $tty_f "(${n}) $s"
        varDoSet /${ins}/num -v $n
        varDoSet /${ins}/answer -v $s -m "After ${n}th read"
      }

    CAMP_INT /~/num -D -T "Number" -d on -v 0
