CAMP_INSTRUMENT /~ -D -T "Rohde&Schwarz SMS 2 Signal Generator" \
    -H "Rohde&Schwarz SMS 2 Signal Generator" -d on \
    -initProc sms2_init \
    -deleteProc sms2_delete \
    -onlineProc sms2_online \
    -offlineProc sms2_offline 
  proc sms2_init { ins } { 
    insSet /${ins} -if rs232 0.5 txa0: 9600 8 none 1 CRLF CRLF 2
  }
  proc sms2_delete { ins } { insSet /${ins} -line off }
  proc sms2_online { ins } {
    insIfOn /${ins}
    set status [catch {varRead /${ins}/setup/id}]
    if { ( $status != 0 ) || ( [varGetVal /${ins}/setup/id] == 0 ) } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc sms2_offline { ins } { insIfOff /${ins} }
    CAMP_FLOAT /~/freq_set -D -S -T "Output frequency" -d on -s on \
	-units MHz -writeProc sms2_freq_set_w
      proc sms2_freq_set_w { ins target } {
	set val [format "%8f" $target]
	insIfWrite /${ins} "A$val,"
        varDoSet /${ins}/freq_set -v $val
      }
    CAMP_FLOAT /~/level_set -D -S -T "Output level" -d on -s on \
	-units V -writeProc sms2_level_set_w
      proc sms2_level_set_w { ins target } {
        if { $target >= 1.0 } { 
          return -code error "Value too high, must be less than 1 V"
        } elseif { $target < 0 } {
          return -code error "Negative values not allowed"
        } elseif { $target < 0.001 } {
          set uV [format %3d [expr round( $target*1e6 )]]
          insIfWrite /${ins} "P$uV,"
          varDoSet /${ins}/level_set -v [expr $uV/1.0e6]
        } else {
          set mV [format %3d [expr round( $target*1e3 )]]
          insIfWrite /${ins} "Q$mV,"
          varDoSet /${ins}/level_set -v [expr $mV/1.0e3]
        }
      }
    CAMP_SELECT /~/rf_on -D -S -T "RF on/off" -d on -s on \
	-selections off on -writeProc sms2_rf_on_w
      proc sms2_rf_on_w { ins target } {
	insIfWrite /${ins} "Y$target,"
	varDoSet /${ins}/rf_on -v $target
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
      CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc sms2_id_r
        proc sms2_id_r { ins } {
          # Nothing to read back
          varDoSet /${ins}/setup/id -v 1
        }
