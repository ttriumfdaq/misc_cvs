# This instrument driver closely resembles the Lakeshore 331.
# Maby FLOAT settings fail if the value has too many digits.

CAMP_INSTRUMENT /~ -D -T "LakeShore 340" \
    -H "LakeShore 340 Autotuning Temperature Controller" -d on \
    -initProc lake340_init -deleteProc lake340_delete \
    -onlineProc lake340_online -offlineProc lake340_offline
  proc lake340_init { ins } {
      insSet /${ins} -if gpib 0.1 16 CRLF CRLF
#     insSet /${ins} -if rs232 0.2 /tyCo/2 9600 7 odd 1 CRLF CRLF 2
  }
  proc lake340_delete { ins } {
      insSet /${ins} -line off
  }

# When going online, we might be able to connect with the wrong
# terminator settings, so read back the instrument setting and 
# use those terminators. Also, try with standard interface parameters
# before failing (useful for rs232 use).
# Use [expr] because no [eval] in old Tcl.

  proc lake340_online { ins } {
      insIfOn /${ins}
      set iftype [insGetIfTypeIdent /${ins}]
      set iface  [insGetIf /${ins}]
      set delay  [insGetIfDelay /$ins]
      if { $delay < 0.1 } { set delay 0.2 }
      catch {# provided interface parameters may make communication fail totally
          switch $iftype {
              "rs232" {
                  set stdif [concat [lindex $iface 0] 9600 7 odd 1 CRLF CRLF 2]
                  scan [insIfRead /$ins "COMM?\r\n\r" 12] " %d," t
                  set ts [lindex {none CRLF LFCR CR LF} $t]
                  insIfOff /${ins}
                  set iface [lreplace $iface 4 5 $ts $ts]
                  set icmd [concat insSet /$ins -if rs232 $delay $iface]
                  expr "\[$icmd\]"
                  insIfOn /${ins}
              }
              "gpib" {
                  set stdif $iface
                  scan [insIfRead /$ins "IEEE?\r\n\r" 12] " %d," t
                  set ts [lindex {none CRLF LFCR CR LF} $t]
                  insIfOff /${ins}
                  set iface [lreplace $iface 1 2 $ts $ts]
                  set icmd [concat insSet /$ins -if gpib $delay $iface]
                  expr "\[$icmd\]"
                  insIfOn /${ins}
                  set stdif $iface
              }
          }
      }      
      varRead /${ins}/setup/id
      if { [varGetVal /${ins}/setup/id] == 0 && [string compare $stdif $iface] != 0 } {
          insIfOff /${ins}
          set icmd [concat insSet /$ins -if $iftype $delay $stdif]
          expr "\[$icmd\]"
          insIfOn /${ins}
          varRead /${ins}/setup/id
      }
      if { [varGetVal /${ins}/setup/id] == 0 } {
          insIfOff /${ins}
          return -code error "failed ID query, check interface definition and connections"
      }
      catch { 
	  varRead /${ins}/setup/limits/max_current
	  varRead /${ins}/setup/
      }
  }
  proc lake340_offline { ins } {
      insIfOff /${ins}
  }

    CAMP_FLOAT /~/read_A -D -R -P -L -A -T "Channel A reading" \
	-d on -r on -units K -tol 0.5 \
	-readProc lake340_read_A_r
      proc lake340_read_A_r { ins } {
	set u [lindex {K C S} [varGetVal /${ins}/setup/A_units]]
	insIfReadVerify /${ins} "${u}RDG? A" 48 /${ins}/read_A " %f" 2
	if { [varGetVal /${ins}/setup/control_chan] == 0 } {
	    varTestAlert /${ins}/read_A [varGetVal /${ins}/setpoint]
	}
      }

    CAMP_FLOAT /~/read_B -D -R -P -L -A -T "Channel B reading" \
	-d on -r on -units K -tol 0.5 \
	-readProc lake340_read_B_r
      proc lake340_read_B_r { ins } {
	set u [lindex {K C S} [varGetVal /${ins}/setup/B_units]]
	insIfReadVerify /${ins} "${u}RDG? B" 48 /${ins}/read_B " %f" 2
	if { [varGetVal /${ins}/setup/control_chan] == 1 } {
	    varTestAlert /${ins}/read_B [varGetVal /${ins}/setpoint]
	}
      }

    CAMP_FLOAT /~/setpoint -D -R -S -L -T "Control set point" \
	-d on -r on -s on -units K \
	-readProc lake340_setpoint_r -writeProc lake340_setpoint_w
      proc lake340_setpoint_r { ins } {
	insIfReadVerify /${ins} "SETP? 1" 48 /${ins}/setpoint " %f" 2
      }
      proc lake340_setpoint_w { ins target } {
        if { [scan $target " %f %c" val xt] != 1 } {
            return -code error "Invalid setpoint value |$target|"
        }
        set err [expr {abs($val)/900.0}]
        if { [catch {
	    insIfWriteVerify /${ins} "SETP 1,$val" "SETP? 1" 48 /${ins}/setpoint " %g" 2 $val $err
        } msg ] } {
            varRead /${ins}/ramp
            if { [varGetVal /${ins}/ramp] } {
                varDoSet  /${ins}/setpoint -m "Caught error $msg while ramping"
                varDoSet /${ins}/ramp_status -p on -p_int 5
            } else {
                return -code error $msg
            }
        }
      }

    CAMP_SELECT /~/heat_range -D -R -S -P -T "Heater range" \
	-d on -r on -s on -p off -selections OFF MIN LOW MEDIUM HIGH MAX \
	-readProc lake340_heat_range_r -writeProc lake340_heat_range_w
      proc lake340_heat_range_r { ins } {
	    insIfReadVerify /${ins} "RANGE?" 48 /${ins}/heat_range " %d" 2
      }
      # Unlike the 331, it appears the 340 can verify heater setting immediately,
      # but the HEATERST? query does not give an error for an open load!

# CURRENTLY, I can't get heater to go on, so this code is tentative.

      proc lake340_heat_range_w { ins target } {
          if { [catch {
              insIfWriteVerify /${ins} "RANGE $target" "RANGE?" 48 /${ins}/heat_range " %d" 2 $target
          } buff ] } {
              set err 0
              scan [insIfRead /${ins} {HTRST?} 48] { %d} err
              if { $err } {
                  return -code error [lindex {{} {Over V} {Under V} {Out DAC Err} {Lim DAC Err} {Heater open load} {Heater Short}} $err]
              }
              set range [varGetVal /${ins}/heat_range]
              if { $range == 0 && $target > 0 } {
                  return -code error "Failed heater setting (open load or bad sensor?). $buff"
              } else {
                  return -code error "Failed heater setting. $buff"
              }
          }
          set range [varGetVal /${ins}/heat_range]
          if { [varGetVal /${ins}/setup/control_mode] == 3 } {
              set sc [varGetVal /${ins}/set_current]
              varRead /${ins}/setup/limits/max_current
              set mc [varGetVal /${ins}/setup/limits/max_current]
              if { $sc > $mc * [lindex {0.0 0.01 0.03162 0.1 0.3162 1.0} $range] } {
                  return -code error "Warning: set_current cannot be maintained in this heat_range"
              } else {
                  catch { lake340_setcurr_int $target $ins $sc }
              }
          }
      }

    CAMP_FLOAT /~/current_read -D -R -L -P -A -T "Output current" \
	-d on -r on -p off -units A -tol 0.01 \
        -H "Heater output in Amps.  (Limited by both heat_range and setup/limits/max_current)" \
	-readProc lake340_current_read_r 
      proc lake340_current_read_r { ins } {
          varRead /${ins}/heat_range
          set heat_range [lindex {0.0 0.01 0.03162 0.1 0.3162 1.0} [varGetVal /${ins}/heat_range] ]
          set buf [insIfRead /${ins} "HTR?" 48]
          if { [scan $buf " %d" percent] != 1 } { set percent -1.0 }
          if { ( $percent < 0.0 ) || ( $percent > 100.0 ) } {
              return -code error "failed parsing \"$buf\" from HTR? command"
          }
          set max_curr [varGetVal /${ins}/setup/limits/max_current]
          varDoSet /${ins}/current_read -v [expr {$max_curr*$heat_range*$percent/100.0}]
          if { [varGetVal /${ins}/setup/control_mode] == 3 } {
              varTestAlert /${ins}/current_read [varGetVal /${ins}/set_current]
          }
      }

# NOTE: lake 340 MOUT is documented as unspecified "manual output value" as percentage, 
# not "power" as erroneously stated for the 331, but it appears to be percentage of
# full current in both cases.

# See page 69 for the output ranges -- there are 5 ranges, and the scale of them is
# controlled by a settable maximum current.  See the CLIMIT and CLIMI commands.

# This is only displayed in camp_cui in manual output mode.

    CAMP_FLOAT /~/set_current -D -R -S -L -T "Set Output Current" \
	-d off -r off -s on -units "A" \
        -H "Constant heater current setting (only when not controlling temperature; see setup/control_mode)." \
	-readProc lake340_setcurr_r -writeProc lake340_setcurr_w
      proc lake340_setcurr_r { ins } {
          varRead /${ins}/heat_range
          set hr [lindex {0 0.01 0.03162 0.1 0.3162 1.0} [varGetVal /${ins}/heat_range]]
          varRead /${ins}/setup/limits/max_current
          set mc [varGetVal /${ins}/setup/limits/max_current]
          set buf [insIfRead /${ins} "MOUT? 1" 48]
          if { [scan $buf " %f" pct] != 1 } {
              return -code error "Invalid percent: $buf"
          }
          set curr [expr { $mc * $hr * $pct/100.0}]
          varDoSet /${ins}/set_current -v $curr
      }
      proc lake340_setcurr_w { ins target } {
          set mc [varGetVal /${ins}/setup/limits/max_current]
          if { $target < 0.0 || $target > $mc } {
              return -code error "Output current must be in range 0.0 to $mc A (see setup/limits/max_current)"
          }
          varRead /${ins}/heat_range
          lake340_setcurr_int [varGetVal /${ins}/heat_range] $ins $target
      }
      proc lake340_setcurr_int { hr ins target } {
          set mc [varGetVal /${ins}/setup/limits/max_current]
          set max [lindex {0.1e-08 0.01 0.03162 0.1 0.3162 1.0} $hr]
          if { $target > $max * $mc } {
              return -code error "Exceeds max current for '[lindex {OFF MIN LOW MED HIGH MAX} $hr]' range."
          }
          set pct [format %.4f [expr { 100.0 * $target / ($max * $mc) }]]
          #varDoSet /${ins}/set_current -m "pct $pct, hr $hr, mc $mc, max $max "
          set rb -2
          scan [insIfRead /${ins} "MOUT 1,$pct;MOUT? 1" 48] " %f" rb
          if { abs($rb-$pct) > 0.1 } {
              scan [insIfRead /${ins} "MOUT 1,$pct;MOUT? 1" 48] " %f" rb
          }
          if { abs($rb-$pct) > 0.1 } {
              return -code error "Setting did not stick: $rb vs $pct %"
          }
          varDoSet /${ins}/set_current -v $target
      }
    CAMP_SELECT /~/ramp -D -S -R -P -T "Ramp" \
	-d on -s on -r on -selections DISABLED ENABLED \
	-H "Enable or disable slow ramping of setpoint" \
	-readProc lake340_ramp_r -writeProc lake340_ramp_w
      proc lake340_ramp_r { ins } {
          set buf [insIfRead /${ins} "RAMP? 1" 48]
          if { [scan $buf " %d, %f" en rate] < 2 } {
              return -code error "Invalid Ramp reading $msg"
          }
          varDoSet /${ins}/ramp -v $en
          varDoSet /${ins}/ramp_rate -v $rate
        }
      proc lake340_ramp_w { ins target } {
          lake340_ramp_r $ins
          set cmd [format "RAMP 1,%d,%.1f" $target [varGetVal /${ins}/ramp_rate]]
          insIfWriteVerify /${ins} $cmd "RAMP? 1" 48 /${ins}/ramp " %d," 2 $target
	}
    CAMP_SELECT /~/ramp_status -D -R -P -T "Ramp status" \
	-d on -r on -p off -p_int 5 -selections DONE RAMPING \
	-readProc lake340_ramp_status_r
      proc lake340_ramp_status_r { ins } {
	    insIfReadVerify /${ins} "RAMPST? 1" 32 /${ins}/ramp_status " %d" 2
	    varRead /${ins}/setpoint
	    if { [varGetVal /${ins}/ramp_status] } {
		varDoSet /${ins}/ramp_status -p on -p_int 5
	    } else {
		varDoSet /${ins}/ramp_status -p off
	    }
	}
    CAMP_FLOAT /~/ramp_rate -D -S -R -L -P -T "Ramp rate" \
	-d on -s on -r on -p off -units {K/min} \
	-H "Temperature (setpoint) ramp rate (if ramp is enabled) in Kelvin per minute (from 0.1 to 99.9, or 0 for off)" \
	-readProc lake340_ramp_r -writeProc lake340_ramp_rate_w
      proc lake340_ramp_rate_w { ins target } {
          if { $target < 0.0 || $target > 99.91 } { 
              return -code error "Invalid ramp rate.  Must be 0 to 99.9 (K/min)"
          }
          set en [expr {$target > 0.0}]
          set cmd [format "RAMP 1,%d,%.1f" $en $target]
          insIfWriteVerify /${ins} $cmd "RAMP? 1" 48 /${ins}/ramp_rate " %*d, %f" 2 $target 0.2
          varDoSet /${ins}/ramp -v $en
      }


################################################################################
    CAMP_STRUCT /~/alarms -D -d on -T "Alarms and Relays"

        CAMP_SELECT /~/alarms/alarm_A -D -R -P -A -T "A Alarm state" \
                -d on -r on -p off -p_int 5 -a off \
                -H "Alarm state for input A" \
                -selections OK Low_Trip Hi_Trip Both_Trip \
                -v 0 -readProc {lake340_alarmst_r A}

        CAMP_SELECT /~/alarms/alarm_B -D -R -P -A -T "B Alarm state" \
                -d on -r on -p off -p_int 5 -a off \
                -H "Alarm state for input B" \
                -selections OK Low_Trip Hi_Trip Both_Trip \
                -v 0 -readProc {lake340_alarmst_r B}

             proc lake340_alarmst_r { ch ins } {
                 set buf [insIfRead /$ins "ALARMST? $ch" 16]
                 if { [scan $buf " %d, %d" hi lo] != 2 } {
                     return -code error "invalid status $buf"
                 }
                 varDoSet /${ins}/alarms/alarm_$ch -v [expr {$lo + 2*$hi}]
                 varTestAlert /${ins}/alarms/alarm_$ch 0
             }

        CAMP_SELECT /~/alarms/reset -D -S -T "Reset alarms" \
                -d on -s on -selections "" RESET -v 0 \
                -writeProc lake340_alarmreset_w

             proc lake340_alarmreset_w { ins target } {
                 if { $target } {
                     insIfWrite /$ins "ALMRST"
                 }
             }

        CAMP_SELECT /~/alarms/beep -D -S -R -T "Alarm beeps" \
                -d on -s on -r on -selections Quiet Beep \
                -readProc lake340_alarmbeep_r -writeProc lake340_alarmbeep_w

             proc lake340_alarmbeep_r { ins } {
                 insIfReadVerify /${ins} "BEEP?" 31 /${ins}/alarms/beep " %d" 2
             }
             proc lake340_alarmbeep_w { ins target } {
                 insIfWriteVerify /${ins} "BEEP $target" "BEEP?" 31 /${ins}/alarms/beep " %d" 2 $target 
             }

    foreach inch {A B} {

        CAMP_STRUCT /~/alarms/cfg_${inch} -D -d on -T "Configure Alarm ${inch}"

            CAMP_SELECT /~/alarms/cfg_${inch}/enable -D -S -R -T "Enable ${inch} Alarm" \
                -d on -s on -r on -selections Disable Enable \
                -readProc [list lake340_almcfg_r ${inch}] -writeProc [list lake340_almcfg_w ${inch} enable]
            CAMP_FLOAT /~/alarms/cfg_${inch}/low_limit -D -S -R -T "${inch} low limit" -d on -s on -r on \
                -readProc [list lake340_almcfg_r ${inch}] -writeProc [list lake340_almcfg_w ${inch} low_limit]
            CAMP_FLOAT /~/alarms/cfg_${inch}/high_limit -D -S -R -T "${inch} high limit" -d on -s on -r on \
                -readProc [list lake340_almcfg_r ${inch}] -writeProc [list lake340_almcfg_w ${inch} high_limit]
            CAMP_SELECT /~/alarms/cfg_${inch}/latching -D -S -R -T "${inch} alarm latching" \
                -d on -s on -r on -selections reset latch \
                -readProc [list lake340_almcfg_r ${inch}] -writeProc [list lake340_almcfg_w ${inch} latching]
    }

            proc lake340_almcfg_r { ch ins } {
                set buf [insIfRead /$ins "ALARM? $ch" 80]
                if { [scan $buf " %d,%d,%g,%g,%d,%d" enable source high low latch relay] != 6 } {
                    return -code error "invalid alarms config $buf"
                }
                varDoSet /${ins}/alarms/cfg_${ch}/enable -v $enable
                varDoSet /${ins}/alarms/cfg_${ch}/low_limit -v $low
                varDoSet /${ins}/alarms/cfg_${ch}/high_limit -v $high
                varDoSet /${ins}/alarms/cfg_${ch}/latching -v $latch
            }
            proc lake340_almcfg_w { ch var ins val } {
                set source [expr { [varGetVal /${ins}/setup/${ch}_units] + 1 }]
                set enable [varGetVal /${ins}/alarms/cfg_${ch}/enable]
                set low_limit [varGetVal /${ins}/alarms/cfg_${ch}/low_limit]
                set high_limit [varGetVal /${ins}/alarms/cfg_${ch}/high_limit]
                set latching [varGetVal /${ins}/alarms/cfg_${ch}/latching]
                set $var $val
                insIfWrite /${ins} "ALARM $ch,$enable,$source,$high_limit,$low_limit,$latching"
                varRead /${ins}/alarms/cfg_${ch}/$var
            }

	CAMP_SELECT /~/alarms/HI_relay_set -D -R -S -T "HI relay (1) setting" \
             -d on -s on -r on -selections Off On High_alarm \
             -readProc {lake340_relayact_r 1 HI} -writeProc {lake340_relayact_w 1 HI}
	CAMP_SELECT /~/alarms/LO_relay_set -D -R -S -T "LO relay (2) setting" \
             -d on -s on -r on -selections Off On Low_alarm \
             -readProc {lake340_relayact_r 2 LO} -writeProc {lake340_relayact_w 2 LO}

 #   On the 340, relays are not very configurable. One is always for the high alarm
 #   and one for the low alarm.
         proc lake340_relayact_r { id name ins } {
             set buf [insIfRead /$ins "RELAY? $id" 48]
             if { [scan $buf { %d,%d} mode offon] != 2 } {
                    return -code error "invalid relay config $buf"
             }
             if { $mode == 2 } {# manual setting
                 set mode $offon
             } elseif { $mode == 1 } {
                 set mode 2
             } else {
                 set mode 0
             }
             varDoSet /${ins}/alarms/${name}_relay_set -v $mode
         }
         proc lake340_relayact_w { id name ins target } {
             if { $target == 2 } {# alarm
                 insIfWrite /$ins "RELAY $id,1"
             } else {# manual
                 insIfWrite /$ins "RELAY $id,2,$target"
             }
             varDoSet /${ins}/alarms/${name}_relay_set -v $target
         }

################################################################################
    CAMP_STRUCT /~/setup -D -d on -T "Setup parameters"
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc lake340_id_r
	  proc lake340_id_r { ins } {
		set id 0
		set status [catch {insIfRead /${ins} "*IDN?" 48} buf]
		if { $status == 0 } {
		    set id [scan $buf { LSCI,MODEL340,%*d,%d} val]
		    if { $id != 1 } { set id 0 }
		}
		varDoSet /${ins}/setup/id -v $id
	  }
	CAMP_SELECT /~/setup/control_chan -D -R -S -T "Control channel" \
	    -d on -r on -s on -selections A B \
            -H "Input Channel controlling main heater output (loop 1)" \
	    -readProc lake340_control_chan_r -writeProc lake340_control_chan_w
	  proc lake340_control_chan_r { ins } {
                set buf [insIfRead /${ins} {CSET? 1} 64]
                if { [scan $buf { %[AB], %d,} cch cun] != 2 } {
                    return -code error $buf
                }
                varDoSet /${ins}/setup/control_chan -v $cch
                set run [varGetVal /${ins}/setup/${cch}_units]
                if { $run+1 != $cun } {
                    varSet /${ins}/heat_range -v 0
                    return -code error "DANGER: Setpoint units do not match channel $cch units"
                }
		varDoSet /${ins}/setpoint -m "Setpoint, controlling on channel $cch" \
                    -units [varNumGetUnits /${ins}/read_${cch}]
	  }
	  proc lake340_control_chan_w { ins target } {
		set chan [lindex {A B} $target]
		set unit [varGetVal /${ins}/setup/${chan}_units]
                insIfReadVerify /${ins} "CSET 1,${chan},[incr unit],1; CSET? 1" 64 \
		    /${ins}/setup/control_chan { %[AB]} 2 $chan
		varDoSet /${ins}/setpoint -m "Setpoint, controlling on channel $target" \
                    -units [varNumGetUnits /${ins}/read_${chan}]

	  }
	CAMP_SELECT /~/setup/control_mode -D -R -S -T "Control mode" \
	    -d on -r on -s on -selections "" "Manual PID" "Zone PID" "Constant Out" "AutoTune PID" "AutoTune PI" "AutoTune P" \
	    -readProc lake340_control_mode_r -writeProc lake340_control_mode_w
	  proc lake340_control_mode_r { ins } {
		insIfReadVerify /${ins} "CMODE? 1" 48 /${ins}/setup/control_mode " %d" 2
		if { [varGetVal /${ins}/setup/control_mode] == 3 } {
		    varDoSet /${ins}/set_current -d on -r on
		} else {
		    varDoSet /${ins}/set_current -d off -r off
		}
	  }
	  proc lake340_control_mode_w { ins target } {
		if { $target == 0 } { return }
		insIfWriteVerify /${ins} "CMODE 1,${target}" "CMODE? 1" 48 /${ins}/setup/control_mode " %d" 2 $target
		if { [varGetVal /${ins}/setup/control_mode] == 3 } {
		    varDoSet /${ins}/set_current -d on -r on
		} else {
		    varDoSet /${ins}/set_current -d off -r off
		}
	  }
	CAMP_FLOAT /~/setup/P -D -R -S -T "Gain setting (P)" \
	    -d on -r on -s on \
	    -readProc lake340_PID_r -writeProc lake340_P_w
	  proc lake340_PID_r { ins } {
		set buf [insIfRead /${ins} "PID? 1" 48]
		if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		    return -code error "Bad PID Readback \"$buf\""
		}
		varDoSet /${ins}/setup/P -v $p
		varDoSet /${ins}/setup/I -v $i
		varDoSet /${ins}/setup/D -v $d
	  } 
	  proc lake340_P_w { ins target } {
              if { $target < 0 || $target > 999 } {
                  return -code error "invalid value \"$target\""
              }
              set val [format %.4f $target]
              insIfWrite /${ins} "PID 1,${val},,"
              lake340_PID_r $ins
	  }
	CAMP_FLOAT /~/setup/I -D -R -S -T "Reset setting (I)" \
	    -d on -r on -s on \
	    -readProc lake340_PID_r -writeProc lake340_I_w
	  proc lake340_I_w { ins target } {
              if { $target < 0 || $target > 1000 } {
                  return -code error "invalid value \"$target\""
              }
              set val [format %.4f $target]
              insIfWrite /${ins} "PID 1,,${val},"
              lake340_PID_r $ins
	  }
	CAMP_FLOAT /~/setup/D -D -R -S -T "Rate setting (D)" \
	    -d on -r on -s on \
	    -readProc lake340_PID_r -writeProc lake340_D_w
	  proc lake340_D_w { ins target } {
              if { $target < 0 || $target > 1000 } {
                  return -code error "invalid value \"$target\""
              }
              set val [format %.4f $target]
              insIfWrite /${ins} "PID 1,,,${val}"
              lake340_PID_r $ins
	  }
	CAMP_SELECT /~/setup/A_units -D -S -T "Channel A units" \
	    -d on -s on -v 0 -selections K C Raw \
	    -writeProc {lake340_units_w A}
	CAMP_SELECT /~/setup/B_units -D -S -T "Channel B units" \
	    -d on -s on -v 0 -selections K C Raw \
	    -writeProc {lake340_units_w B}
	  proc lake340_units_w { chan ins target } {
	    set u [lindex {K C Raw} $target]
	    if { $target == 2 } {# Raw = sensor units
		varRead /${ins}/setup/${chan}_sensor
		switch [varGetVal /${ins}/setup/${chan}_sensor] {
		    1 - 2 { set u V }
		    default { set u Ohms }
		}
	    }
	    varDoSet /${ins}/setup/${chan}_units -v $target
	    varDoSet /${ins}/read_${chan} -units $u -z
	    if { [varSelGetValLabel /${ins}/setup/control_chan] == $chan } {
		varDoSet /${ins}/setpoint -units $u
		insIfWrite /${ins} "CSET 1,${chan},[incr target],1"
	    }
	  }
	CAMP_SELECT /~/setup/A_sensor -D -S -R -T "Channel A input type" \
	    -d on -s on -r on -selections " " Si GaAlAs PtR100 PtR1000 RhFe CGR Cernox RuOx Ge Cap TC \
	    -readProc {lake340_sensor_r A} -writeProc {lake340_sensor_w A}
	CAMP_SELECT /~/setup/B_sensor -D -S -R -T "Channel B input type" \
	    -d on -s on -r on -selections " " Si GaAlAs PtR100 PtR1000 RhFe CGR Cernox RuOx Ge Cap TC \
	    -readProc {lake340_sensor_r B} -writeProc {lake340_sensor_w B}
	  proc lake340_sensor_r { chan ins } {
		set buf [insIfRead /${ins} "INTYPE? ${chan}" 64]
		if { [scan $buf { %d,} it] } {
		    varDoSet /${ins}/setup/${chan}_sensor -v [lindex {0 1 2 3 3 4 5 6 7 8 9 10 11} $it]
		}
                # Also add more variables for setting all sensor parameters, and displaying the same, for "special" type.
	    }
	  proc lake340_sensor_w { chan ins target } {
	      lake340_sensor_r $chan $ins
	      set it [lindex {0 1 2 4 5 6 7 8 9 10 11 12} $target]
	      if { $target == 3 || $target == 4 } {
		  # Platinum selected -- first check with a low excitation (pretend Si)
		  set volts 9.
		  set okv [scan [insIfRead /${ins} "INTYPE ${chan},1; SRDG? ${chan}" 48] { %f} volts ]
		  if { $volts == 0.0 || $volts > 5.0 || $okv < 1 } {
		      set okv [scan [insIfRead /${ins} "INTYPE ${chan},1; SRDG? ${chan}" 48] { %f} volts ]
		  }
                  # *** Instead of using silicon, maybe use Pt but with manually-low excitation (thus "special")
		  if { $volts > 0.03 || $volts == 0.0 } {# MORE TESTS HERE!!!!
		      return -code error "Platinum Resistor Rejected.  Volts = $volts"
		      lake340_sensor_r $chan $ins
		  }
	      }
	      insIfWrite /${ins} "INTYPE ${chan},${it}"
	      lake340_sensor_r $chan $ins
              # Select curve automatically for some sensors
              set cn [lindex {0 2 0 4 4 0 0 0 0 0 0 0} $target]
              if { $cn } {
                  varSet /${ins}/setup/${chan}_curve -v $cn
              }
	  }
	CAMP_INT /~/setup/A_curve -D -R -P -S -T "Channel A curve" \
	    -d on -r on -s on -p off -p_int 0.1 \
	    -H "Load or read the A-channel calibration curve" \
            -readProc {lake340_curve_r A} -writeProc {lake340_set_curve A}
	CAMP_INT /~/setup/B_curve -D -R -P -S -T "Channel B curve" \
	    -d on -r on -s on -p off -p_int 0.1 \
	    -H "Load or read the B-channel calibration curve" \
	    -readProc {lake340_curve_r B} -writeProc {lake340_set_curve B}
	  proc lake340_curve_r { ch ins } {
              global CAMP_VAR_ATTR_POLL
              if { [varGetStatus /${ins}/setup/${ch}_curve] & $CAMP_VAR_ATTR_POLL } {# Asynchronous
                  varDoSet /${ins}/setup/${ch}_curve -p off
                  lake340_read_curve $ch 2 $ins
              } else {
                  set cn [lake340_read_curve $ch 0 $ins]; # 1 if validating
#                 Do not validate on read because it's too slow when user reads all (space bar)
              }
	  }

#####################################################################################
        CAMP_STRUCT /~/setup/limits -D -d on -T "Control Limits"

          proc lake340_limits_r { ins } {
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              set mc -1
              if { $mci == 5 } {
                  scan [insIfRead /$ins "CLIMI?" 32] " %f" mc
              } else {
                  catch { set mc [lindex {-1.0 0.25 0.5 1.0 2.0} $mci] }
              }
              varDoSet /${ins}/setup/limits/max_current -m "Debug $mci $mc"
              if { $mc < 0.0 } {
                  return -code error "Invalid maximum current readback: $mci, $mc"
              }
              varDoSet /${ins}/setup/limits/max_current -v $mc
              varDoSet /${ins}/setup/limits/max_heat_range -v $mri
              varDoSet /${ins}/setup/limits/setpoint_limit -v $splv
              varDoSet /${ins}/setup/limits/pos_slope -v $psv
              varDoSet /${ins}/setup/limits/neg_slope -v $nsv
          }

        CAMP_FLOAT /~/setup/limits/setpoint_limit -D -S -R -T "Maximum Setpoint" \
            -d on -s on -r on -units K \
	    -readProc lake340_limits_r -writeProc lake340_setpoint_limit_w
          proc lake340_setpoint_limit_w { ins target } {
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              insIfWrite /$ins "CLIMIT 1,$target,$psv,$nsv,$mci,$mri"
              lake340_limits_r
          }

        CAMP_FLOAT /~/setup/limits/max_current -D -S -R -L -T "Maximum Current" \
            -d on -s on -r on -v 1.0 -units A \
            -H "Maximum heater current on HI range; other ranges scale proportionately. Default 1 A." \
	    -readProc lake340_limits_r -writeProc lake340_maxcurr_w
          proc lake340_maxcurr_w { ins target } {
              if { $target < 0.1 || $target > 2.0 } { 
                  return -code error "Invalid current limit.  Must be in range 0.1 to 2.0 A"
              }
              set ix [lsearch -exact {0.000 0.250 0.500 1.000 2.000} [format {%.3f} $target]]
              if { $ix < 1 } { set ix 5 }
              insIfWrite /$ins "CLIMI $target" 
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              insIfWrite /$ins "CLIMIT 1,$splv,$psv,$nsv,$ix,$mri"
              lake340_limits_r
          }
        CAMP_SELECT /~/setup/limits/max_heat_range -D -R -S -T "Maximum heat range" \
	    -d on -r on -s on -selections OFF MIN LOW MEDIUM HIGH MAX \
	    -readProc lake340_limits_r -writeProc lake340_max_heat_range_w
          proc lake340_max_heat_range_w { ins target } {
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              insIfWrite /$ins "CLIMIT 1,$splv,$psv,$nsv,$mci,$target"
              lake340_limits_r
          }
        CAMP_FLOAT /~/setup/limits/pos_slope -D -S -R -T "Max increase output" \
            -d on -s on -r on -units {%} \
            -H "Maximum increase in output, as percent of full scale (0 for no limit)" \
	    -readProc lake340_limits_r -writeProc lake340_pos_slope_w
          proc lake340_pos_slope_w { ins target } {
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              insIfWrite /$ins "CLIMIT 1,$splv,$target,$nsv,$mci,$mri"
              lake340_limits_r
          }
        CAMP_FLOAT /~/setup/limits/neg_slope -D -S -R -T "Max decrease output" \
            -d on -s on -r on -units {%} \
            -H "Maximum decrease in output, as percent of full scale (0 for no limit)" \
	    -readProc lake340_limits_r -writeProc lake340_neg_slope_w
          proc lake340_neg_slope_w { ins target } {
              set buf [insIfRead /$ins "CLIMIT? 1" 64]
              if { [scan $buf { %g,%f,%f,%d,%d} splv psv nsv mci mri] != 5 } {
                  return -code error "Invalid limits readback: $buf"
              }
              insIfWrite /$ins "CLIMIT 1,$splv,$psv,$target,$mci,$mri"
              lake340_limits_r
          }

############################################################################

	CAMP_STRUCT /~/setup/filter -D -d on -T "Filtering"

	foreach inch {A B} {
	    CAMP_SELECT /~/setup/filter/${inch}_filter -D -R -S -T "${inch}-Filtering" \
		-H "Enable $inch channel filtering" \
		-d on -s on -r on -selections Off On \
		-readProc [list lake340_filter_r $inch] -writeProc [list lake340_filter_w $inch filter]
	    CAMP_INT /~/setup/filter/${inch}_points -D -R -S -T "${inch}-Points" \
		-H "Number of points for $inch channel filtering (2-64)" \
		-d on -s on -r on -units "" \
		-readProc [list lake340_filter_r $inch] -writeProc [list lake340_filter_w $inch points]
	    CAMP_INT /~/setup/filter/${inch}_window -D -R -S -T "${inch}-Window" \
		-H "Window for $inch channel filtering, as percentage of full scale (1-10%)" \
		-d on -s on -r on -units "%" \
		-readProc [list lake340_filter_r $inch] -writeProc [list lake340_filter_w $inch window]
	}
	proc lake340_filter_r { ch ins } {
	    set buf [insIfRead /$ins "FILTER? $ch" 48]
	    if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	    }
	    varDoSet /${ins}/setup/filter/${ch}_filter -v $o
	    varDoSet /${ins}/setup/filter/${ch}_points -v $p
	    varDoSet /${ins}/setup/filter/${ch}_window -v $w
	}
	proc lake340_filter_w { ch par ins val } {
	    set buf [insIfRead /$ins "FILTER? $ch" 48]
	    if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	    }
	    switch $par {
		filter {
		    set o $val
		}
		points {
		    if {$val < 2 || $val > 64 } {
			return -code error "Invalid number of points (must be 2-64)"
		    }
		    set p $val
		}
		window {
		    if {$val < 1 || $val > 10 } {
			return -code error "Invalid window (must be 1-10%)"
		    }
		    set w $val
		}
	    }
	    insIfWrite /$ins "FILTER $ch,$o,$p,$w"
	    lake340_filter_r $ch $ins
	}

############################################################################

	CAMP_STRUCT /~/setup/zone -D -d on -T "PID Zone configuration"
	    CAMP_SELECT /~/setup/zone/apply_zones -D -S -T "Apply zones" -d on -s on \
		  -H "Check and apply all zone settings" -selections APPLY \
		  -writeProc lake340_apply_zones_w
	    proc lake340_apply_zones_w { ins target } {
		set max 0.0
		set adj 0
		for { set i 1 } { $i <= 10 } { incr i } {
		    set z [varGetVal /${ins}/setup/zone/zone${i}]
		    # If this zone is blank, copy from previous.
		    if { [llength $z] == 0 && $i > 1 } {
			set z [varGetVal /${ins}/setup/zone/zone[expr {$i-1}]]
		    }
		    if { [llength $z] == 4 } { lappend z 0 }
		    if { [llength $z] == 5 } { lappend z 0 }
		    if { [llength $z] >= 6 } {
			set T [lindex $z 0]
			if { $T < $max && $T > 0.0 } {
			    if { $max < 300.0 } {
				return -code error "Invalid sequence of temperatures: must increase from zone1 to zone10"
			    }
			    set T [string trim [format %6.1f $max]]
			    set z [lreplace $z 0 0 $T]
			    varDoSet /${ins}/setup/zone/zone${i} -v $z
			}
			set max $T
                        # ZONE is the same, but the value for heater range is different
			insIfWrite /$ins "ZONE 1,$i,$T,[join [lrange $z 2 5] {,}],[lindex $z 1]"
		    } else {
			return -code error "Invalid values for zone $i."
		    }
		}
	    }
	    for { set i 1 } { $i <= 10 } { incr i } {
		CAMP_STRING /~/setup/zone/zone$i -D -S -R -T "Zone $i info" \
		    -d on -s on -r on \
		    -H "List of zone $i parameters: Top Temp, Heat range, P, I, D, Manual out%" \
		    -readProc "lake340_zone_r $i" -writeProc "lake340_zone_w $i"
	    }
	    proc lake340_zone_w { i ins target } {
		set z [join [split $target ,] " "]
		if { [llength $z] == 4 } { lappend z 0 }
		if { [llength $z] == 5 } { lappend z 0 }
		if { [llength $z] >= 5 } {
		    varDoSet /${ins}/setup/zone/zone${i} -v [lrange $z 0 5]
		} else {
		    return -code error "Invalid zone data.  Should be 6 numbers separated by spaces or commas."
		}
	    }
	    proc lake340_zone_r { i ins } {
		set z [insIfRead /$ins "ZONE? 1,$i" 80]
		if { [scan $z " %f,%f,%f,%f,%f,%d" T P I D M H] == 6 } {
		    foreach v {T P I D M} { # reduce round numbers to integers
			set vv [set $v]
			set $v [expr {($vv==round($vv)?round($vv):$vv)}]
		    }
		    varDoSet /${ins}/setup/zone/zone${i} -v [list $T $H $P $I $D $M]
		} else {
		    return -code error "Invalid zone readback: \"$z\""
		}
	    }

############################################################################
#  Variables for loading curves.
#  Any curve file may be loaded into any curve number using these variables.
#  Curve loading and validation are done asynchronously by polling variables here.
#  There are a number of execution sequences (S for synchronous, A for asynchronous)
#
#  Read curve (A or B): 
#    get curve info (S); [for user curve: validate curve data for A or B (A)]
#
#  Set curve (A or B): 
#    Evaluate curve number (S), then...
#    1  For standard curve:  attach chan to curve (S)
#    2  For existing user curve: 
#         attach chan to curve (S); validate curve data for A or B (A)
#    3  For missing user curve, or failure in 2: 
#         setup for loading (S); load (A); attach chan; validate curve data (A)
#  
#  Do load (no chan):   load (A); validate curve data (A)
#
#  Check curve (no chan): validate data (A)
#
#  Each transition from synchronous to asysynchronous stages is done by polling.
#  As such, there is no easy way to pass parameteres, not even which channel is
#  required or what stream of execution we are in.  To that end, there are variables
#  here under setup/curve to pass information, and variables to poll for different
#  execution paths.  The parameter variables are also set by the user to perform
#  loading and validation independent of channels A or B.  Polled variables detect
#  that they are being polled (actually, just that polling is on) and tyrn it off.
#
# The operations that are the first asynchronous in a path, and the polled variable:
# "validate curve data for A or B" --   setup/A_curve and setup/B_curve
# "load"   --    setup/curve/do_load (attach A or B or none determined heuristically)
# "validate data"  --   setup/curve/validate

	CAMP_STRUCT /~/setup/curve -D -d on -T "Curve Loading" \
               -H "Variables for loading curves (calibration tables)"
	    CAMP_INT /~/setup/curve/number -D -S -T "Curve number" -d on -s on -units "" \
		-writeProc lake340_curve_number_w
	      proc lake340_curve_number_w { ins target } {
		    if { ( $target < 21 ) || ( $target > 59 ) } {
			return -code error "invalid curve number $target"
		    }
		    varDoSet /${ins}/setup/curve/number -v $target
	      }
	    CAMP_STRING /~/setup/curve/file -D -S -R -T "Curve datafile" -d on -s on -r on \
		-readProc lake340_curve_header_r -writeProc lake340_curve_file_w
	      proc lake340_curve_file_w { ins target } {
		    varDoSet /${ins}/setup/curve/file -v $target
	      }
	    CAMP_SELECT /~/setup/curve/format -D -S -R -T "Curve format" -d on -s on -r on \
		-selections "" {K,mV} {K,V} {K,Ohm} {K,log Ohm} {log K,log Ohm} \
		-readProc lake340_curve_header_r -writeProc lake340_curve_format_w
	      proc lake340_curve_format_w { ins target } {
		    varDoSet /${ins}/setup/curve/format -v $target
	      }
	    CAMP_STRING /~/setup/curve/id -D -S -R -T "Curve ID (serial num)" -d on -s on -r on \
                -H "The curve ID/serial number. Reads the ID being used as curve \"number\" (above). Set it to what you want to do_load." \
		-readProc lake340_curve_header_r -writeProc lake340_curve_id_w
	      proc lake340_curve_id_w { ins target } {
		    varDoSet /${ins}/setup/curve/id -v [string trim [string tolower $target]]
	      }
	      proc lake340_curve_header_r { ins } {
		  set num [varGetVal /${ins}/setup/curve/number]
		  set buf [insIfRead /${ins} "CRVHDR? $num" 80]
		  if { [scan $buf {%[^,],%[^,],%d} name id fmt ] != 3 } {
		      return -code error "Could not read header for curve $num"
		  }
		  varDoSet /${ins}/setup/curve/file -v [string trim [string tolower $name]]
		  varDoSet /${ins}/setup/curve/id -v [string trim [string tolower $id]]
		  varDoSet /${ins}/setup/curve/format -v $fmt
	      }
	    CAMP_SELECT /~/setup/curve/chan -D -S -T "Channel loading" \
		-H "Channel to load curve for (A, B, or none). Choose blank to load curves for future use." \
		-s on -d on -selections  "" A B \
                -writeProc lake340_curve_chan_w
              proc lake340_curve_chan_w { ins ch } {
                  varDoSet /${ins}/setup/curve/chan -v $ch
              }
	    CAMP_SELECT /~/setup/curve/do_load -D -S -P -T "Do curve load" \
		-H "Start loading the curve" \
		-d on -s on -selections  "" "LOAD" \
		-readProc lake340_curve_doload_r -writeProc lake340_curve_doload_w
    # Since do_load is activated asynchronously by polling, capture
    # errors and emit them as messages
    proc lake340_curve_doload_r { ins } {
	set ch [varSelGetValLabel /${ins}/setup/curve/chan]
	if { [catch {lake340_curve_do_load $ins} msg] } {
	    varDoSet /${ins}/setup/curve/do_load -v 0 -p off -m $msg
	    if { [string match {[AB]} $ch] } {
                varDoSet /${ins}/setup/${ch}_curve -units FAIL -m "Failed to load curve data: $msg"
            } else {
                varDoSet /${ins}/setup/curve/do_load -m "Failed to load curve data: $msg"
            }
	    varDoSet /${ins}/setup/curve/chan -v 0
	    return
	}
        varDoSet /${ins}/setup/curve/do_load -v 0 -p off -m "Validating curve data"
	if { ! [catch { lake340_read_curve $ch 2 $ins } msg] } {
            set msg ""
        }
        varDoSet /${ins}/setup/curve/do_load -m ""
    }
    proc lake340_curve_doload_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	if { $target == 0 } {
	    varDoSet /${ins}/setup/curve/do_load -v $target -p off -m ""
	    return
	} else {
	    if { ([varGetStatus /${ins}/setup/curve/do_load] & $CAMP_VAR_ATTR_POLL) } {
		return -code error "Busy loading a curve already"
	    }
	    varDoSet /${ins}/setup/curve/do_load -v $target -p on -p_int 0.1 -m "Start loading curve"
	    #lake340_curve_do_load $ins
	}
    }
    proc lake340_curve_do_load { ins } {
	set file [varGetVal /${ins}/setup/curve/file]
	set num [varGetVal /${ins}/setup/curve/number]
	set fmt [varGetVal /${ins}/setup/curve/format]
	set coef [lindex "0 2 1 2 1" $fmt]
	set sn [varGetVal /${ins}/setup/curve/id]
	varDoSet /${ins}/setup/curve/do_load -v 1 -p off
	set f [open $file r]
	set curve_data ""
	# read first line (has the number of data points)
	if { [gets $f line] == -1 } {
	    close $f
	    return -code error "unexpected end of file"
	}
	# delete the curve in memory 
	insIfWrite /${ins} [format "CRVDEL %2d" $num]
	# send the new header
	insIfWrite /${ins} [format "CRVHDR %2d,%s,%s,%d,999,%d" $num $file $sn $fmt $coef]
	set cmd_line ""
	set i 0
	set fp 0
	set maxt -1.0
	while { [gets $f line] >= 0 } {
	    if { [scan $line " %f %f" t v] < 2 } {
		close $f
		return -code error "bad file format"
	    }
	    incr i
	    if { $t > $maxt } { set maxt $t }
	    if { $i > 1 } {
		if { ( $t != $tp ) && ( $v != $vp ) } {
		    set s [expr ($v - $vp ) / ( $t - $tp )]
		    if { ($s > 0.0) != ($coef - 1) } {
			close $f
			return -code error "bad file: slope changed sign at $i, $coef, $s = ($v - $vp ) / ( $t - $tp )"
		    }
		}
	    }
	    set tp $t ; set vp $v
	    set cmd [format "CRVPT %d,%d,%.5f,%.3f" $num $i $v $t]
	    set len_cmd [string length $cmd]
	    set len_cmd_line [string length $cmd_line]
	    if { ( $len_cmd + $len_cmd_line ) > 64 } {
		insIfWrite /${ins} $cmd_line
		puts stdout $cmd_line
		set cmd_line $cmd
	    } else {
		append cmd_line ";" $cmd
	    }
	}
	if { $cmd_line != "" } { insIfWrite /${ins} $cmd_line }
	#puts stdout $cmd_line
	close $f
	insIfWrite /${ins} [format "CRVHDR %2d,%s,%s,%d,%.3f,%d" $num $file $sn $fmt $maxt $coef]
    }


    CAMP_STRUCT /~/panic -D -d on
	CAMP_SELECT /~/panic/reset -D -S -T "Reset" -d on -s on \
	    -selections DO_RESET \
	    -writeProc lake340_reset_w
	  proc lake340_reset_w { ins target } { insIfWrite /${ins} "*RST" }
	CAMP_SELECT /~/panic/test -D -R -T "Self-test" -d on -r on \
	    -selections OK ERROR UNKNOWN \
	    -readProc lake340_test_r
	  proc lake340_test_r { ins } {
		set test [insIfRead /${ins} "*TST?" 16]
		switch $test {
		    0 {}
		    1 {}
		    default { set test 2 }
		}
		varDoSet /${ins}/panic/test -v $test
		if { $test != 0 } { insSet /${ins} -line off }
	    }

# For setting A_curve and B_curve.  May initiate polling of setup/curve/do_load
proc lake340_set_curve { ch ins curve } {
    varDoSet /${ins}/setup/${ch}_curve -m ""
    varDoSet /${ins}/setup/${ch}_curve -units B1
    if { $curve < 0 } {
	varDoSet /${ins}/setup/${ch}_curve -units "FAIL \"$curve\""
	return -code error "bad value \"$curve\""
    } elseif { $curve <= 59 } {# max curve index in the 340
    varDoSet /${ins}/setup/${ch}_curve -units B2
	set sn ""
	set buf [insIfRead /$ins "CRVHDR? $curve" 80]
	scan [insIfRead /$ins "CRVHDR? $curve" 80] {%[^,],%[^,],} name sn
	if { [string trim $sn] == "" } {
	    varDoSet /${ins}/setup/${ch}_curve -units "EMPTY" -m "Curve $curve is corrupt or empty"
	    return -code error "Curve $curve is corrupt or empty"
	}
    varDoSet /${ins}/setup/${ch}_curve -units B3
	set number $curve
	set curve $sn
    } else {# A sensor serial number
    varDoSet /${ins}/setup/${ch}_curve -units B4
	# First see if it is already loaded, in range 21-30
	set number 0; set unused 0
	for {set i 21} {$i<=30} {incr i} {
    varDoSet /${ins}/setup/${ch}_curve -units B5.$i
	    set buf [insIfRead /$ins "CRVHDR? $i" 80]
	    if { [scan $buf {%[^,],%[^,],%d} name sn fmt ] == 3 } {
		set sn [string trim $sn]
		if {[string compare $sn $curve]==0} {
		    set number $i
		    break
		}
		if {$sn == ""} { set unused $i }
	    }
	}
    varDoSet /${ins}/setup/${ch}_curve -units B6
	if { $number == 0 } {# User curve not already loaded
    varDoSet /${ins}/setup/${ch}_curve -units B6.0
	    if { $unused > 0 } {
		set number $unused
    varDoSet /${ins}/setup/${ch}_curve -units B6.1
	    } else {# Pick a curve slot not currently referenced in range 25-32
    varDoSet /${ins}/setup/${ch}_curve -units B6.2
		scan [insIfRead /${ins} "INCRV? A" 48] { %d} Acurve
    varDoSet /${ins}/setup/${ch}_curve -units B6.3
		scan [insIfRead /${ins} "INCRV? B" 48] { %d} Bcurve
    varDoSet /${ins}/setup/${ch}_curve -units B6.4
		set prev [varGetVal /${ins}/setup/curve/number]
		set number [expr { ($prev>=25 && $prev<30) ? $prev+1 : 25 }]
		if { $number == $Acurve || $number == $Bcurve } { incr number }
		if { $number == $Acurve || $number == $Bcurve } { incr number }
    varDoSet /${ins}/setup/${ch}_curve -units B6.5
	    }
    varDoSet /${ins}/setup/${ch}_curve -units B7
	    # now have number to load into.
	    lake340_setup_load $ins $ch $curve $number
    varDoSet /${ins}/setup/${ch}_curve -units B8
	    varDoSet /${ins}/setup/${ch}_curve -units "LOAD" -m "Loading curve \"$curve\" as number $number"
	    # Initiate the actual loading (asynchronously)
	    varDoSet /${ins}/setup/curve/do_load -p on -p_int 0.1
	    return
	}
    }
    varDoSet /${ins}/setup/${ch}_curve -units B9
    # Select curve (which we did not load)
    insIfWrite /${ins} "INCRV ${ch},$number"
    # Verify setting of curve number.  Since the Lakeshore340 will not switch 
    # to a defective curve, this check also verifies the load.  We will later
    # read out the curve to check more fully.
    set i -4
    varDoSet /${ins}/setup/${ch}_curve -units B10 -m "INCRV? ${ch}"
    while { $i < 0 } {
        catch {
            set buf [insIfRead /${ins} "INCRV? ${ch}" 48] 
            varDoSet /${ins}/setup/${ch}_curve -units B11.$i
            scan $buf { %d} i
        }
    }
    if { $number != $i } {# Failed to select curve
	set e "Could not select curve $number for channel $ch"
	varDoSet /${ins}/setup/${ch}_curve -units "FAIL" -m $e
	if { $number >= 21 } { # A user curve - try reloading
	    lake340_setup_load $ins $ch $curve $number
	    varDoSet /${ins}/setup/${ch}_curve -units "LOAD" -m "Reloading corrupt curve \"$curve\" as number $number"
	    # Initiate asynchronous reloading
	    varDoSet /${ins}/setup/curve/do_load -p on -p_int 0.1
	    return
	}
	return -code error $e
    }
    # read curve info without validation, but validate asynchronously
    lake340_read_curve $ch 1 $ins
    if { $number > 20 } {# schedule asynchronous validation
        varDoSet /${ins}/setup/${ch}_curve -p on -p_int 0.1
    }
}

proc lake340_setup_load { ins ch curve number} {
    set buf [glob ./dat/*.*]
    set found 0
    foreach file $buf {
	set root [file rootname [file tail $file]]
	if { $root == $curve } { set found 1; break }
    }
    if { $found == 0 } { 
	varDoSet /${ins}/setup/${ch}_curve -units "NO FILE" -m \
	    "Failed to find a data file for curve \"$curve\"."
	return -code error "couldn't find curve \"$curve\"" 
    }
    set ext [string tolower [file extension $file]]
    set type [expr [lsearch -exact ".tc .spl .tr .dat" $ext] + 1]
    # Put parameters in place to perform the loading asynchronously
    varSet /${ins}/setup/curve/number -v $number
    varSet /${ins}/setup/curve/format -v $type
    varSet /${ins}/setup/curve/id -v $curve
    varSet /${ins}/setup/curve/file -v [format "./dat/%s" [file tail $file]]
    varDoSet /${ins}/setup/curve/chan -v $ch
}
    
# ch is A or B or "", 
# validate is:
#   0 (just read header), 
#   1 (just read header now, maybe schedule validation asynchronously) 
#   3 (validate now)
proc lake340_read_curve { ch validate ins } {
    set number -1
    if { [string match {[AB]} $ch] } {
        set curvar "/${ins}/setup/${ch}_curve"
        varDoSet $curvar -units "A1"
        set buf [insIfRead /${ins} "INCRV? ${ch}" 48]
        if { [catch { expr "$buf > 0" }] == 0 } {
            set number [ expr "$buf" ]
        } else {
            set number -99
        }
        varDoSet $curvar -units "A2"
        if { !( ($number >= 0) && ($number <= 60) ) } {
            set e "failed to read curve number $number for channel $ch"
            varDoSet $curvar -units "FAIL" -m $e
            return -code error $e
        }
        varDoSet $curvar -units "A3"
        if { $number < 21 } {# preliminary set, in case 
            varDoSet $curvar -v $number -units "Num.$number" -m "Standard curve num $number"
            set curve $number
        }
        varDoSet $curvar -units "A4"
    } else {
        set curvar "/${ins}/setup/curve/id"
        set number [varGetVal /${ins}/setup/curve/number]
    }
    # Fetch curve header
    set buf [insIfRead /$ins "CRVHDR? $number" 80]
    if { [scan $buf {%[^,],%[^,],%d} name sn fmt ] != 3 } {
        varDoSet $curvar -v $number -m "Bad curve data for curve num $number"
        return -code error "failed parsing curve data"
    }
    set units [lindex {{} {mV/K} {V/K} {Ohm/K} {logOhm/K} {logOhm/logK}} $fmt]
    set name [string trim $name]
    set curve [string trim [string tolower $sn]]
    set file [string tolower $name]
    set fnam [file tail $file]
    varDoSet /${ins}/setup/curve/file -v $file
    varDoSet /${ins}/setup/curve/id -v $curve
    varDoSet /${ins}/setup/curve/format -v $fmt
    if { [string match {[AB]} $ch] } {
        varDoSet $curvar -units $units
    }
    if { $number < 21 } {
        varDoSet $curvar -v $number -m "Standard curve $name"
        return $number
    } elseif { $validate == 0 } {
        varDoSet $curvar -v $curve -m "Curve $number containing $curve ($fnam)"
        return $curve
    } else  {
        varDoSet $curvar -v $curve -m "Curve $number containing $curve ($fnam). Validating..."
        if { $validate != 2 } { return $curve }
    }
    # Validate curve data against file
    # Do glob here to find any case file name. No [file join] in old Tcl
    set flist [glob [file dirname $file]/*]
    foreach ff $flist {if {[string tolower $ff]==$file} {set file $ff; break} }
    set f [open $file r]
    set curve_data ""
    set uvcd "Unable to validate curve data"
    # read first line (has the number of data points)
    if { [gets $f line] == -1 } {
        close $f
        varDoSet $curvar -m "$uvcd (eof)"
        return -code error "unexpected end of file"
    }
    set n 0
    if { [scan $line " %d" n] != 1 } {
        close $f
        varDoSet $curvar -m "$uvcd (no number in $file)"
        return -code error "invalid file format: no number of points"
    }
    set tf 0; set vf 0
    for { set i 1 } { $i <= $n } { incr i } {
        if { [gets $f line] == -1 } {
	    close $f
	    varDoSet $curvar -m "$uvcd (eof in $file)"
	    return -code error "unexpected end of file"
        }
        if { [scan $line " %f %f" tf vf] < 2 } {
	    close $f
	    varDoSet $curvar -m "$uvcd (bad format in $file line $i)"
	    return -code error "bad file format while scanning data line $i of $file"
        }
        set vf [format "%.5f" $vf]
        set tf [format "%.3f" $tf]
        set buf [insIfRead /${ins} "CRVPT?$number,$i" 64]
        if { [scan $buf { %f,%f} vi ti] != 2 } {
	    close $f
	    varDoSet $curvar -v -1 -m \
		"Failed validation: bad readback of curve $number point $i"
	    return -code error "invalid readback of curve $number point $i "
        }
        set vi [format %.5f $vi]
        set ti [format %.3f $ti]
        if { $vf != $vi || $tf != $ti } {
	    close $f
	    varDoSet $curvar -v -1  -m \
		"Failed validation: inconsistent readback of curve point $i"
	    return -code error "inconsistent readback: $i: Vfile=$vf Vins=$vi Tfile=$tf Tins=$ti" 
        }
    }
    close $f
    varDoSet $curvar -v $curve -m "Curve $number containing $curve ($fnam)"
}
