# camp_ins_motor_lift.tcl
# 
# This is the tcl script camp device driver for a single-direction cryostat 
# positioning controller. The system is composed of an ADC channel in the 
# tip850 Industry Pack, and a motor channel in the te28 Industry Pack. The 
# ADC can read a pot to measure the absolute position, but there is no such
# pot yet on the actual device, and if any absolute readback is added in the
# future it will likely be a linear encoder of some sort.  The motor encoder
# which is actually mounted on a drive-chain cog, knows its relative position 
# only.  This driver is based closely on the "motor_valve" device, 
# with changes: hysteresis directionality is reversed (so cryostat is always
# positioned by raising), removed jiggle, find_closed, and calibrate functions, 
# added down_limit instead of 0.0 for the closed/bottom position.  The absolute
# position checking is retained so that it can be used in the future.
#
# Donald Arseneau.      
# Last modified            02-Apr-2009

CAMP_INSTRUMENT /~ -D -T "Motorized Lift" -d on \
    -H "Motorized cryostat lift" \
    -initProc mol_init \
    -deleteProc mol_delete \
    -onlineProc mol_online \
    -offlineProc mol_offline
  proc mol_init { ins } {
      insSet /${ins} -if none 0.0
      varDoSet /${ins}/setup/connect -p on -p_int 1
      insIfOn /${ins}
  }
  proc mol_delete { ins } { 
      insSet /${ins} -line off 
  }
  proc mol_online { ins } {
      insIfOn /${ins}
  }
  proc mol_offline { ins } {
      insIfOff /${ins}
  }

  CAMP_SELECT /~/status -D -R -P -T "Movement status" \
        -d on -r on -p off -p_int 10 -v 8 -H "Indicator for position and movement status" \
        -selections Stopped Down_limit Raising Lowering Up_limit Stuck Bottomed Timed_out Need_channel \
	-readProc mol_status_r
#   mol_status_r gives the status back as a return value, so I can use
#   set var [varRead /{$ins}/status] (if no error return)
    proc mol_status_r { ins } {
	set s [varGetVal /${ins}/status]
        if { $s == 8 } {
            catch { varRead /${ins}/setup/connect }
            set s [varGetVal /${ins}/status]
        }
        if { $s == 8 } {
            return $s
        }
	set olds $s
	varRead /${ins}/read_position
	set v [varGetVal /${ins}/read_position]
	set dv [expr { $v - [varGetVal /${ins}/internal/prev_position] } ]
	set m [varGetVal /${ins}/motor_position]
	set dm [expr { $m - [varGetVal /${ins}/internal/prev_motor] } ]
	set t [varGetVal /${ins}/setup/tolerance ]
	set ul [varGetVal /${ins}/setup/up_limit]
	set dl [varGetVal /${ins}/setup/down_limit]

#       if did not move enough to be sure, retain previous record
#       of previous position (so effectively larger step sizes).
#        if { abs( $dm ) > $t && abs( $dm ) > 0.0 } {
#        }
        varDoSet /${ins}/internal/prev_position -v $v
	varDoSet /${ins}/internal/prev_motor -v $m
#
        if { ($dm > 0.0) && ($dv > $dm - $t || $dv > $t) } { # raising
	    if { $v >= $ul } { # Reached up-limit, so stop
		mol_stop $ins
		varDoSet /${ins}/status -v 4 -p on -p_int 5
		return 4
	    } else { # Raising
		varDoSet /${ins}/status -v 2
		return 2
	    }
	}
        if { ($dm < 0.0) && ($dv < $dm + $t || $dv < -$t) } { # lowering
	    if { $v <= $dl } { # Reached down-limit, so stop
		mol_stop $ins
		varDoSet /${ins}/status -v 1 -p on -p_int 5
		return 1
	    } else { # Lowering
                varDoSet /${ins}/status -v 3
                return 3
            }
	}
	#  neither opening nor closing, so must be stopped or stuck:
	if { $s < 5 } { # only allow "stopped" if not already "stuck" or "timed out"
            #  stuck if motor is moving...
            set s [ expr ( abs( $dm ) > 0.3*$t ) ? 5 : 0 ]
	    #  and possibly at a limit...
	    if { ($s == 0) && ($v >= $ul) } { # upper limit
		set s 4
	    } elseif { ($s == 0) && ($v <= $dl) } { # down limit
		set s 1
	    }
	}
	varDoSet /${ins}/status -v $s
        if { $s >= 4 } { mol_stop $ins }
    }

  CAMP_FLOAT /~/set_position -D -S -L -T "Set movement to new position" \
        -d on -s off -units mm -writeProc mol_set_position_w
    proc mol_set_position_w { ins target } {
        if { [varGetVal /${ins}/motor/IPChan] < 0 } {
            return -code error "Please set /${ins}/setup/channel"
        }
	set ul [varGetVal /${ins}/setup/up_limit]
	set dl [varGetVal /${ins}/setup/down_limit]
	if { ($target < $dl) || ($target > $ul) } {
	    return -code error "Destination out of limits"
	}
	set dest $target
	mol_stop $ins
	set p [varGetVal /${ins}/motor_position]
	set v [varGetVal /${ins}/read_position]
        # Possibly modify destination to keep absolute_position within limits 
        # when there is a mis-registration.
	if { ($dest > $p) && ($dest+($v-$p) > $ul) } {
	    set dest [expr {$ul+$p-$v}]
	}
	if { ($dest < $p) && ($dest+($v-$p) < $dl) } {
	    set dest [expr {$dl+$p-$v}]
	}
        # Get needed amount of hysteresis cancellation
        set h [varGetVal /${ins}/setup/hysteresis]
        # For a move greater than 5mm or 5*hysteresis, move fast and always use (double)hysteresis
        if { abs($dest-$p) > 5*($h>1.0 ? $h : 1.0) } {
            set h [expr {2.0*$h}]
            varSet /${ins}/motor/vel -v [varGetVal /${ins}/setup/high_speed]
	} else {
            if { $dest >= $p } {
                set h [varGetVal /${ins}/internal/loose]
            }
            varSet /${ins}/motor/vel -v [varGetVal /${ins}/setup/low_speed]
        }
        varDoSet /${ins}/internal/loose -v $h
	varDoSet /${ins}/set_position -v $dest
        # Maybe modify (initial) destination to a lower value for hysteresis cancellation
        set dest [expr {$dest-$h < $dl ? $dl : $dest-$h}]
	if { $dest > $p } { # We want to raise
	    # force sequencer to see raising for at least one iteration.
	    set p -99.
	    varDoSet /${ins}/status -v Raising
	} else { # we want to lower (or the linkage is sloppy)
	    # force sequencer to see lowering for at least one iteration.
	    set p 99.
	    varDoSet /${ins}/status -v Lowering
	}
	varDoSet /${ins}/internal/prev_position -v $p
#	varDoSet /${ins}/internal/prev_motor -v $p
	# Calculate an appropriate poll interval for do_move based on the speed
 	# of movement and the expected uncertainty in readback (tolerance)
	set mpi [expr { abs(( 0.08 + [varGetVal /${ins}/setup/tolerance] )/
			    [varGetVal /${ins}/motor/clip_vel]) }]
        set mpi [expr { $mpi < 0.4 ? 0.4 : ( $mpi > 5 ? 5 : $mpi ) }]
	varSet /${ins}/motor/destin -v $dest
        varDoSet /${ins}/motor/destin -m "Initiate move to $dest and [varGetVal /${ins}/set_position]"
	# Start sequencer and schedule a timeout
	varDoSet /${ins}/internal/do_move -p on -p_int 2 -v MOVING
	varDoSet /${ins}/abort -p on -p_int [varGetVal /${ins}/setup/timeout]
    }

  CAMP_FLOAT /~/read_position -D -R -P -L -A -T "Absolute position" \
	-H "Read position from adc" \
        -d on -r off -units mm -tol 0.1 -p off \
	-readProc mol_read_position_r
    proc mol_read_position_r { ins } {
	varRead /${ins}/motor_position
	if { [varGetVal /${ins}/setup/pot] == 1 } {
	    varRead /${ins}/adc/adc_read
	    varDoSet /${ins}/read_position -v [ format %.4f [ varGetVal /${ins}/adc/adc_read ] ]
	} else { # broken pot
	    varDoSet /${ins}/read_position -v [ format %.4f [ varGetVal /${ins}/motor_position ] ]
	}
	# This alert tests that the motor has not slipped:
	varTestAlert /${ins}/read_position [varGetVal /${ins}/motor_position]
    }

  CAMP_FLOAT /~/motor_position -D -R -P -L -T "Display the motor position" \
        -d on -r off -units mm -p off \
	-readProc mol_motor_position_r
    proc mol_motor_position_r { ins } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	MotorPosition $pos $chan motor_pos
	varDoSet /${ins}/motor_position -v [format "%.4f" $motor_pos]
    }

  CAMP_SELECT /~/abort -D -S -R -P -T "Abort movement" \
	-d on -s on -r off \
	-selections "Abort Movement" " " "Never mind" "Timed out" \
	-readProc mol_abort_r -writeProc mol_abort_w
    proc mol_abort_r { ins } {
	mol_abort_w $ins 0
	varDoSet /${ins}/abort -v 3
    }
    proc mol_abort_w { ins target } {
	if { $target == 0 } {
	    mol_stop $ins
	    varDoSet /${ins}/abort -v 0
	} else {
	    varDoSet /${ins}/abort -v 2
	}
	varDoSet /${ins}/status -p on -p_int 5
    }

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 


  # Connect: Read this to initialize position and detect if a motor is hooked up,
  # and the channel has been assigned.  This is polled until a connection is made.
  # If status = "need_channel", then we are newly defined (never yet connected)
  # so the motor offset is arbitrary, so we perform "equate"

  CAMP_SELECT /~/setup/connect -D -R -P -T "Test connection" \
        -d on -r on -v 0 -p off -p_int 1 \
        -selections FALSE TRUE \
	-readProc mol_connect_r

    proc mol_connect_r { ins } {
        varDoSet /${ins}/setup/connect -v 0
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
        if { $chan >= 0 } {
	    if { [varGetVal /${ins}/status] == 8 } {# new connection
		insIfWrite /${ins} "MotorReset $pos $chan 0"
	    } 
            varSet /${ins}/adc/gain -v [varGetVal /${ins}/adc/gain]
            mol_apply_params $ins
            varDoSet /${ins}/read_position -r on
            varDoSet /${ins}/motor_position -r on
            varDoSet /${ins}/setup/define_pos -s on
            varDoSet /${ins}/setup/equate -s on
            if { [varGetVal /${ins}/setup/pot] == 0 } {
                # No pot, so preserve last motor position as "destination"
                varDoSet /${ins}/motor/destin -v [varGetVal /${ins}/motor_position]
            }
            if { ! ( [catch {varRead /${ins}/adc/adc_read}] ||
                     [catch {varRead /${ins}/motor_position}] ) } {
                varDoSet /${ins}/setup/connect -p off -v 1
                varDoSet /${ins}/set_position -s on
                mol_stop $ins
                varDoSet /${ins}/internal/prev_position -v [varGetVal /${ins}/read_position]
                varDoSet /${ins}/internal/prev_motor -v [varGetVal /${ins}/motor_position]
		varSet /${ins}/setup/equate -v 0
                varDoSet /${ins}/status -v 0 -p on -p_int 5
                return
            }
        }
    }

  CAMP_SELECT /~/setup/channel -D -S -T "Channel" \
        -d on -s on -selections A B \
	-H "Motor control output channel" \
	-writeProc mol_channel_w
    proc mol_channel_w { ins target } {
        varDoSet /${ins}/setup/channel -v $target
        # ADC channels 6 and 7 reserved for motor control
        varDoSet /${ins}/adc/IPChan   -v [expr {$target + 6}]
        varDoSet /${ins}/motor/IPChan -v $target
	varRead /${ins}/setup/connect
    }

  CAMP_SELECT /~/setup/which_system -D -S -T "Identify system" \
        -d on -s on \
	-selections Bnqr \
	-writeProc mol_which_system_w
    proc mol_which_system_w { ins target } {
	set file [lindex [string tolower "Bnqr"] $target ]
	source "./dat/mlift_${file}.ini"
        varSet /${ins}/adc/gain -v [varGetVal /${ins}/adc/gain]
	varDoSet /${ins}/setup/which_system -v $target
        varSet /${ins}/setup/equate -v 0
    }

  CAMP_INT /~/setup/timeout -D -S -T "Timeout period" \
	-d on -s on -v 180 -units seconds \
	-H "Set timeout limit (seconds) for movements." \
	-writeProc mol_timeout_w
    proc mol_timeout_w { ins target } {
	if { $target > 0 } {
	    varDoSet /${ins}/setup/timeout -v $target
	}
    }

  CAMP_FLOAT /~/setup/hysteresis -D -S -T "Hysteresis" \
	-d on -s on -v 0.5 -units mm \
	-H "How much looseness in the gears?" \
	-writeProc mol_hysteresis_w
    proc mol_hysteresis_w { ins target } {
	varDoSet /${ins}/setup/hysteresis -v $target
    }

  CAMP_FLOAT /~/setup/high_speed -D -S -T "High Speed" \
	-d on -s on -v 0.5 -units "mm/s" \
	-H "High speed to use for long moves" \
	-writeProc mol_hispeed_w
    proc mol_hispeed_w { ins target } {
	varDoSet /${ins}/setup/high_speed -v [expr { abs($target) }]
    }

  CAMP_FLOAT /~/setup/low_speed -D -S -T "Low Speed" \
	-d on -s on -v 0.5 -units "mm/s" \
	-H "Low speed to use for short moves" \
	-writeProc mol_lospeed_w
    proc mol_lospeed_w { ins target } {
	varDoSet /${ins}/setup/low_speed -v [expr { abs($target) }]
    }

  CAMP_FLOAT /~/setup/up_limit -D -S -T "Upper limit" \
	-d on -s on -v 500 -units mm \
	-H "Set upper limit for system position" \
	-writeProc mol_up_limit_w
    proc mol_up_limit_w { ins target } {
	varDoSet /${ins}/setup/up_limit -v $target
    }

  CAMP_FLOAT /~/setup/down_limit -D -S -T "Lower limit" \
	-d on -s on -v -25 -units mm \
	-H "Set lower limit for system position" \
	-writeProc mol_up_limit_w
    proc mol_up_limit_w { ins target } {
	varDoSet /${ins}/setup/up_limit -v $target
    }

  CAMP_SELECT /~/setup/limit_switch -D -S -T "Limit Switches" \
        -d on -s on -selections "None" "Active Low" "Active High" \
        -H "Configure the limit switches behavior" \
        -writeProc mol_limsw_w
    proc mol_limsw_w { ins target } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
        if { $pos < 0 || $chan < 0 } {
            return -code error "Need channel setup"
        }
        MotorLimit $pos $chan [lindex {0 -1 1} $target]
	varDoSet /${ins}/setup/limit_switch -v $target
    }

  CAMP_FLOAT /~/setup/tolerance -D -S -T "Position tolerance" \
	-d on -s on -v 0.01 -units mm \
	-H "Uncertainty of read_position" \
	-writeProc mol_tolerance_w
    proc mol_tolerance_w { ins target } {
	varDoSet /${ins}/setup/tolerance -v [expr {$target < 0.0 ? 0.0 : $target}]
    }

  CAMP_SELECT /~/setup/pot -D -S -T "Potentiometer" \
	-d on -s on -v 0 -selections "None" "OK" \
	-H "Say whether potentiometer is functioning" \
	-writeProc mol_pot_w
    proc mol_pot_w { ins target } { 
	varDoSet /${ins}/setup/pot -v $target
    }

# "equate" is to declare motor offset so it matches the pot position.
# If pot is not installed, though, we set the motor offset
# so it matches the "destination", which is useful when the Camp
# server reboots.
  CAMP_SELECT /~/setup/equate -D -S -T "Force motor = position" \
        -d on -s off -v 0 -selections "Equate" \
	-H "Declare motor_position = read_position. Do this after manual move or clutch slip" \
	-writeProc mol_equate_w
    proc mol_equate_w { ins target } {
	if { [varGetVal /${ins}/setup/pot] == 0 } { # Pot is not there
	    varRead /${ins}/motor_position
	    set p [varGetVal /${ins}/motor/destin]
	} else {
	    varRead /${ins}/read_position ; # includes reading motor_position
	    set p [varGetVal /${ins}/read_position]
	}
        varSet /${ins}/motor/offset -v [expr {
	    $p - [varGetVal /${ins}/motor_position] + [varGetVal /${ins}/motor/offset] } ]
	varRead /${ins}/read_position
	varDoSet /${ins}/set_position -v [varGetVal /${ins}/motor_position]
	varDoSet /${ins}/internal/prev_position -v [varGetVal /${ins}/read_position]
	varDoSet /${ins}/internal/prev_motor -v [varGetVal /${ins}/motor_position]
	varDoSet /${ins}/status -v 0
	varRead /${ins}/status
    }

  CAMP_FLOAT /~/setup/define_pos -D -S -T "Define position" \
        -d on -s off -v 0.0 -units mm \
	-H "Set the offset parameter so the present position will correspond to the given value." \
	-writeProc mol_define_pos_w
    proc mol_define_pos_w { ins target } {
	if { [varGetVal /${ins}/setup/pot] == 0 } { # Pot is claimed to be broken!
	    varDoSet /${ins}/motor/destin -v $target
	} else {
	    varRead /${ins}/read_position
	    set p [varGetVal /${ins}/read_position]
	    varSet /${ins}/adc/offset -v [expr { 
		 [varGetVal /${ins}/adc/offset] - [varGetVal /${ins}/read_position] + $target} ]
	}
	mol_equate_w ${ins} 0
	varDoSet /${ins}/status -v Stopped -p on -p_int 5
    }


CAMP_STRUCT /~/motor -D -T "Motor actions" -d on \
        -H "Motor actions.  Don't use directly."

  CAMP_FLOAT /~/motor/destin -D -S -T "Move motor" -d on -s on \
        -writeProc mol_destin_w
    proc mol_destin_w { ins target } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	insIfWrite /${ins} "MotorMove $pos $chan [expr $target]"
	varDoSet /${ins}/motor/destin -v $target
    }

  CAMP_SELECT /~/motor/stop -D -S -R -P -T "Motor stop" \
	-d on -s on -r on -p off -H "Stop the motor movement" \
	-selections STOP \
	-readProc mol_stop_r \
	-writeProc mol_stop_w
    proc mol_stop_r { ins } {
	varSet /${ins}/motor/stop -p off
	set status [catch {mol_stop_w /${ins} STOP }]
    }
    proc mol_stop_w { ins target } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	insIfWrite /${ins} "MotorStop $pos $chan 0"
    }

  CAMP_INT /~/motor/IPPos -D -T "Position of IP on carrier" \
        -d on -v 2 

  CAMP_INT /~/motor/IPChan -D -S -T "Channel within IP" \
        -d on -s on -v -1 \
	-writeProc mol_motor_IPChan_w
    proc mol_motor_IPChan_w { ins target } {
	varDoSet /${ins}/motor/IPChan -v $target
    }

  CAMP_FLOAT /~/motor/slope -D -S -T "Slope" \
	-d on -s on -v -0.472366 -units "enc.rev/mm" \
	-H "Conversion: encoder revolutions per movement mm" \
	-writeProc motor_slope_w
    proc motor_slope_w { ins target } {
        varDoSet /${ins}/motor/slope -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/encoder -D -S -T "Encoder" \
	-d on -s on -v 512 -units "tics/enc.rev" \
	-H "Conversion: ticks per encoder revolution" \
	-writeProc motor_encoder_w
    proc motor_encoder_w { ins target } {
        varDoSet /${ins}/motor/encoder -v $target
	mol_apply_params $ins
    }

  CAMP_FLOAT /~/motor/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 -units "mm" \
	-H "Conversion offset: offset to zero" \
	-writeProc motor_offset_w
    proc motor_offset_w { ins target } {
        varDoSet /${ins}/motor/offset -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/filter_kp -D -S -T "Filter - Proportional" \
	-d on -s on -v 100 \
        -H "Proportional parameter for PID loop in controller" \
	-writeProc motor_filter_kp_w
    proc motor_filter_kp_w { ins target } {
        varDoSet /${ins}/motor/filter_kp -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/filter_ki -D -S -T "Filter - Integral" \
	-d on -s on -v 0 \
	-H "Integral parameter for PID loop in controller" \
	-writeProc motor_filter_ki_w
    proc motor_filter_ki_w { ins target } {
        varDoSet /${ins}/motor/filter_ki -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/filter_kd -D -S -T "Filter - Differential" \
	-d on -s on -v 2000 \
	-H "Differential parameter for PID loop in controller" \
	-writeProc motor_filter_kd_w
    proc motor_filter_kd_w { ins target } {
        varDoSet /${ins}/motor/filter_kd -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/filter_il -D -S -T "Filter - il" \
	-d on -s on -v 0 \
	-writeProc motor_filter_il_w
    proc motor_filter_il_w { ins target } {
        varDoSet /${ins}/motor/filter_il -v $target
	mol_apply_params $ins
    }

  CAMP_INT /~/motor/filter_si -D -S -T "Filter - si" \
	-d on -s on -v 0 \
	-writeProc motor_filter_si_w
    proc motor_filter_si_w { ins target } {
        varDoSet /${ins}/motor/filter_si -v $target
	mol_apply_params $ins
    }

  CAMP_FLOAT /~/motor/accel -D -S -T "Acceleration" \
	-d on -s on -v 0.1 \
	-H "Parameter for motor acceleration" \
	-writeProc motor_accel_w
    proc motor_accel_w { ins target } {
        varDoSet /${ins}/motor/accel -v $target
	mol_apply_params $ins
    }

  CAMP_FLOAT /~/motor/vel -D -S -T "Velocity" \
	-d on -s on -v 0.05 -units "mm/sec" \
	-writeProc motor_vel_w
    proc motor_vel_w { ins target } {
        varDoSet /${ins}/motor/vel -v $target
	mol_apply_params $ins
    }

  CAMP_FLOAT /~/motor/clip_vel -D -T "Clipped Velocity" \
	-d on -units "mm/sec" \
	-H "Velocity, as limited by need to read encoder" 

CAMP_STRUCT /~/adc -D -T "ADC specific variables" -d on \
        -H "ADC variables.  Don't use directly."

  CAMP_SELECT /~/adc/IPPos -D -T "ADC IP Position" \
	-d on -v 3 -selections 0 1 2 3 \
	-H "Slot number in the Industry Pack occupied by ADC board"

  CAMP_INT /~/adc/IPChan -D -S -T "ADC Channel (0-7)" \
        -d on -s on -v -1 \
	-H "ADC input channel (0-7)" \
	-writeProc mol_adc_IPChan_w
    proc mol_adc_IPChan_w { ins target } {
	varDoSet /${ins}/adc/IPChan -v $target
    }

  CAMP_SELECT /~/adc/gain -D -S -T "ADC Gain" \
        -d on -s on -v 1 -selections x1 x2 x4 x8 \
	-writeProc mol_adc_gain_w
    proc mol_adc_gain_w { ins target } {
	set gain $target
	set pos [varGetVal /${ins}/adc/IPPos]
	set chan [varGetVal /${ins}/adc/IPChan]
        if { $chan >= 0 } {
            GainSet $pos $chan gain
            varDoSet /${ins}/adc/gain -v $gain
            # Gain setting takes some time to be effective
            sleep 0.25
        }
    }

# slope:  slope for converting "adc units" to distance
# value =  slope * a.u.  +  offset    or
# a.u. = ( displayed_value - offset ) / slope 

  CAMP_FLOAT /~/adc/slope -D -S -T "Slope" \
	-d on -s on -v 1.0 -units "mm/adc" \
        -H "Set slope to use in: position = slope * adc + offset" \
	-writeProc mol_adc_slope_w
    proc mol_adc_slope_w { ins slope } {
	varDoSet /${ins}/adc/slope -v $slope
    }

  CAMP_FLOAT /~/adc/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 -units mm \
	-writeProc mol_adc_offset_w
    proc mol_adc_offset_w { ins offset } {
	varDoSet /${ins}/adc/offset -v $offset
    }

  CAMP_FLOAT /~/adc/adc_read -D -R -P -T "Read ADC" \
	-d on -r on -units mm \
	-readProc  mol_adc_read_r
    proc mol_adc_read_r { ins } {
	set pos [varGetVal /${ins}/adc/IPPos]
	set chan [varGetVal /${ins}/adc/IPChan]
	set slope [varGetVal /${ins}/adc/slope]
	set offset [varGetVal /${ins}/adc/offset]

	AdcRead $pos $chan adc_read
	set value [expr {$adc_read * $slope + $offset}]
	varDoSet /${ins}/adc/adc_read -v $value
    }



CAMP_STRUCT /~/internal -D -T "Control procedures" -d on \
        -H "Internal control procedures and variables."

  CAMP_FLOAT /~/internal/prev_position -D -T "Previous absolute position" \
	-d on -v 0.0 -units mm 

  CAMP_FLOAT /~/internal/prev_motor -D -T "Previous motor position" \
	-d on -v 0.0 -units mm 

  CAMP_FLOAT /~/internal/loose -D -T "Looseness" \
	-d on -units mm -v 0.0
  
  CAMP_SELECT /~/internal/do_move -D -R -P -T "Movement sequencer" \
	-d on -r on -selections " " "MOVING" -readProc mol_do_move_r
    proc mol_do_move_r { ins } {
	varRead /${ins}/status
	set s [varGetVal /${ins}/status]
	if { ($s == 0) && [varGetVal /${ins}/internal/loose] > 0.0 } { 
            # now stopped, but gears loose: remove hysteresis
            varSet /${ins}/motor/vel -v [varGetVal /${ins}/setup/low_speed]
            varDoSet /${ins}/internal/loose -v 0.0
            varDoSet /${ins}/status -v Raising
            varSet /${ins}/motor/destin -v [varGetVal /${ins}/set_position]
	} else {
	    if { ($s != 2) && ($s != 3) } {
                # not raising or lowering: finished (for good or bad)
                varSet /${ins}/motor/vel -v [varGetVal /${ins}/setup/low_speed]
                varDoSet /${ins}/motor/destin -m "do_move says stop because stat=$s"
		mol_stop $ins
		set d [expr { [varGetVal /${ins}/set_position] - [varGetVal /${ins}/motor_position] } ]
		if { $d < [varGetVal /${ins}/setup/tolerance] } {
		    varDoSet /${ins}/internal/loose -v 0.0
		}
		varDoSet /${ins}/status -p on -p_int 5
	    }
	}
    }    


# Stop any active sequencers and position polling; cancel any pending timeout;
# stop the motor movement, if any, and read the present position.  This procedure
# is used by status, set_position, abort, connect, do_move.
#
proc mol_stop { ins } {
    varSet   /${ins}/motor/stop -v STOP
    varDoSet /${ins}/internal/do_move -p off -v 0
    varDoSet /${ins}/read_position -p off
    varDoSet /${ins}/abort -p off -v 1
    varDoSet /${ins}/status -p off
    varRead  /${ins}/read_position
}

proc mol_apply_params { ins } {
    set pos [varGetVal /${ins}/motor/IPPos]
    set chan [varGetVal /${ins}/motor/IPChan]
    if { $pos < 0 || $chan < 0 } { return }

    set slope [varGetVal /${ins}/motor/slope]
    set vel [expr { abs([varGetVal /${ins}/motor/vel]) }]
    set enc [varGetVal /${ins}/motor/encoder]

    set max [expr { 900000.0 / abs($slope*$enc*0.000256*65536) }]
    varDoSet /${ins}/motor/clip_vel -v [expr { $vel > $max ? $max : $vel }]

    MotorSlope $pos $chan $slope
    MotorVel $pos $chan $vel

    MotorFilterKP $pos $chan [varGetVal /${ins}/motor/filter_kp]
    MotorFilterKI $pos $chan [varGetVal /${ins}/motor/filter_ki]
    MotorFilterKD $pos $chan [varGetVal /${ins}/motor/filter_kd]
    MotorFilterIL $pos $chan [varGetVal /${ins}/motor/filter_il]
    MotorFilterSI $pos $chan [varGetVal /${ins}/motor/filter_si]
    MotorEncoder  $pos $chan [varGetVal /${ins}/motor/encoder]
    MotorOffset   $pos $chan [varGetVal /${ins}/motor/offset]
    MotorAccel    $pos $chan [expr { abs([varGetVal /${ins}/motor/accel]) }]
}
