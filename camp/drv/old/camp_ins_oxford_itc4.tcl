# camp_ins_oxford_itc4.tcl 
# Camp Tcl instrument driver for Oxford ITC4-10 temperature controller.
# Donald Arseneau,  TRIUMF
# Last revised Oct 24, 2000
#

CAMP_INSTRUMENT /~ -D -T "Oxford ITC4" \
    -H "Oxford Instruments ITC4 temperature controller" \
    -d on \
    -initProc ox_itc4_init -deleteProc ox_itc4_delete \
    -onlineProc ox_itc4_online -offlineProc ox_itc4_offline

    proc ox_itc4_init { ins } {
	insSet /${ins} -if rs232 0.3 none 9600 8 none 2 CR CR 2
    }
    proc ox_itc4_delete { ins } {
	insSet /${ins} -line off
    }
    proc ox_itc4_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id ] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
#	# Set Remote and Unlocked
	insIfWrite /${ins} "\$C3"
    }
    proc ox_itc4_offline { ins } {
	insIfOff /${ins}
    }

#   Routines used by most variables.  The itc4 reads and writes several parameters
#   as integers, even though they are non-integer; these do the scaling.

    proc ox_scaled_read { ins comm scale } {
        set cch [ string index $comm 0 ]
        set ret [ insIfRead /$ins $comm 64 ]
        if { [ scan $ret " ${cch}%d" val ] != 1 } {
            return -code error "Invalid readback: \"$ret\""
        }
        return [ expr $val * $scale ]
    }
    proc ox_scaled_write { ins val comm scale } {
        set ival [ expr round( $val / $scale ) ]
        set ret [ insIfRead /$ins "${comm}${ival}" 64 ]
        set ir 0
        if { [ scan "${ret}:42 " " ${comm} :%d" ir ] == 1 && $ir == 42 } {
            return [ expr $ival * $scale ]
        }
        return -code error "Invalid readback: \"$ret\""
    }

    CAMP_FLOAT /~/sample_temp -D -R -P -L -T "Sample reading" \
	-d on -r on -units K \
        -H "Temperature reading from \"sample\" sensor" \
	-readProc ox_itc4_sample_temp_r
      proc ox_itc4_sample_temp_r { ins } {
          set comm "R[ varGetVal /${ins}/setup/sample_sensor ]"
          varDoSet /${ins}/sample_temp -v [ ox_scaled_read $ins $comm .1 ]
      } 

    CAMP_FLOAT /~/control_temp -D -R -P -L -A -T "Control reading" \
	-d on -r on -units K \
        -H "Temperature reading from \"control\" sensor" \
	-readProc ox_itc4_control_temp_r
      proc ox_itc4_control_temp_r { ins } {
          set comm "R[ varGetVal /${ins}/setup/control_sensor ]"
          varDoSet /${ins}/control_temp -v [ ox_scaled_read $ins $comm .1 ]
          varTestAlert /${ins}/control_temp [ varGetVal /${ins}/set_temp ]
      }

    CAMP_FLOAT /~/set_temp -D -R -S -L -T "Control set point" \
	-d on -r on -s on -units K \
        -readProc ox_itc4_set_temp_r -writeProc ox_itc4_set_temp_w
      proc ox_itc4_set_temp_r { ins } {
          varDoSet /${ins}/set_temp -v [ ox_scaled_read $ins R0 .1 ]
      }
      proc ox_itc4_set_temp_w { ins target } {
          set val [ ox_scaled_write $ins $target T .1 ]
          varDoSet /${ins}/set_temp -v $val
      }

    CAMP_FLOAT /~/heat_out -D -R -S -L -P -T "Heater output %" \
	-d on -r on -s on -units "%" \
        -H "Read (or set) heater output, as a percent of heat_max" \
        -readProc ox_itc4_heat_out_r -writeProc ox_itc4_heat_out_w
      proc ox_itc4_heat_out_r { ins } {
          varDoSet /${ins}/heat_out -v [ ox_scaled_read $ins R5 .1 ]
      }
      proc ox_itc4_heat_out_w { ins target } {
          set val [ ox_scaled_write $ins $target O .1 ]
          varDoSet /${ins}/heat_out -v $val
      }

    CAMP_FLOAT /~/gas_flow -D -R -S -L -P -T "Gas flow" \
	-d on -r on -s on -units "%" \
        -readProc ox_itc4_gas_flow_r -writeProc ox_itc4_gas_flow_w
      proc ox_itc4_gas_flow_r { ins } {
          varDoSet /${ins}/gas_flow -v [ ox_scaled_read $ins R7 .1 ]
      }
      proc ox_itc4_gas_flow_w { ins target } {
          set val [ ox_scaled_write $ins $target G .1 ]
          varDoSet /${ins}/gas_flow -v $val
      }

    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

#
#   Check that an Oxford itc4 is hooked up and communicating.
#
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections false true -readProc ox_itc4_setup_id_r
	  proc ox_itc4_setup_id_r { ins } {
	    set id 0
	    set buf x
	    catch {insIfRead /${ins} "V" 80} buf
	    set id [expr { [scan $buf " ITC-4, Version %f (c) OXFORD %d" d1 d2] == 2 } ]
	    varDoSet /${ins}/setup/id -v $id
	  }

	CAMP_SELECT /~/setup/sample_sensor -D -S -T "Sample sensor" \
            -d on -s on -selections { } 1 2 3 -v 1 \
            -H "Choose sensor for \"sample\" readback (may be the same as \"control\")" \
            -writeProc ox_itc4_sampsens_w
	  proc ox_itc4_sampsens_w { ins target } {
              if { $target > 0 } { 
                  varDoSet /${ins}/setup/sample_sensor -v $target 
              }
	  }

	CAMP_SELECT /~/setup/control_sensor -D -S -T "Control sensor" \
            -d on -s on -selections { } 1 2 3 -v 1 \
            -H "Choose sensor for heater control (may be the same as \"sample\")" \
            -writeProc ox_itc4_contsens_w
	  proc ox_itc4_contsens_w { ins target } {
              if { $target > 0 } {
                  set v [ ox_scaled_write $ins $target H 1 ]
                  varDoSet /${ins}/setup/control_sensor -v $v
              }
          }

        CAMP_SELECT /~/setup/heat_control -D -S -R -T "Heater control" \
            -d on -s on -r on -selections manual auto -v 1 \
            -readProc ox_itc4_automan_r -writeProc ox_itc4_heatcont_w
          proc ox_itc4_heatcont_w { ins target } {
              ox_itc4_automan_w $ins $target [ varGetVal /${ins}/setup/gas_control ]
          }

        CAMP_FLOAT /~/setup/heater_max -D -S -T "Max heater V" \
            -d on -s on -units V \
            -H "Set maximum heater voltage" \
            -writeProc ox_itc4_heatmax_w
          proc ox_itc4_heatmax_w { ins target } {
              set v [ ox_scaled_write $ins $target M .1 ]
              varDoSet /${ins}/setup/heater_max -v $v
          }

        CAMP_FLOAT /~/setup/P -D -S -R -T "Prop band" \
            -d on -s on -r on -units "%" \
            -H "Heater control proportional band, in percent" \
            -readProc ox_itc4_P_r -writeProc ox_itc4_P_w
          proc ox_itc4_P_r { ins } {
              varDoSet /${ins}/setup/P -v [ ox_scaled_read $ins R8 .1 ]
          }
          proc ox_itc4_P_w { ins target } {
              if { $target > 199.9 } { set target 199.9 }
              if { $target < 0.0 } { set target 0.0 }
              set v [ ox_scaled_write $ins $target P .1 ]
              varDoSet /${ins}/setup/P -v $v
          }
              
        CAMP_FLOAT /~/setup/I -D -S -R -T "Int time" \
            -d on -s on -r on -units "min" \
            -H "Heater control integral time, in minutes" \
            -readProc ox_itc4_I_r -writeProc ox_itc4_I_w
          proc ox_itc4_I_r { ins } {
              varDoSet /${ins}/setup/I -v [ ox_scaled_read $ins R9 .1 ]
          }
          proc ox_itc4_I_w { ins target } {
              if { $target > 140.0 } { set target 140.0 }
              if { $target < 0.0 } { set target 0.0 }
              set v [ ox_scaled_write $ins $target I .1 ]
              varDoSet /${ins}/setup/I -v $v
          }
              
        CAMP_FLOAT /~/setup/D -D -S -R -T "Diff time" \
            -d on -s on -r on -units "min" \
            -H "Heater control differential time, in minutes" \
            -readProc ox_itc4_D_r -writeProc ox_itc4_D_w
          proc ox_itc4_D_r { ins } {
              varDoSet /${ins}/setup/D -v [ ox_scaled_read $ins R10 1 ]
          }
          proc ox_itc4_D_w { ins target } {
              if { $target > 273 } { set target 273 }
              if { $target < 0.0 } { set target 0.0 }
              set v [ ox_scaled_write $ins $target D 1 ]
              varDoSet /${ins}/setup/D -v $v
          }

        CAMP_SELECT /~/setup/gas_control -D -S -R -T "Gas control" \
            -d on -s on -r on -selections manual auto -v 0 \
            -readProc ox_itc4_automan_r -writeProc ox_itc4_gascont_w
          proc ox_itc4_gascont_w { ins target } {
              ox_itc4_automan_w $ins [ varGetVal /${ins}/setup/heat_control ] $target
          }
          proc ox_itc4_automan_w { ins h g } {
              ox_scaled_write $ins [ expr $h + 2 * $g ] A 1
              varDoSet /${ins}/setup/heat_control -v $h
              varDoSet /${ins}/setup/gas_control -v $g
          }

          proc ox_itc4_automan_r { ins } {
              set buf [insIfRead /${ins} "X" 32]
              set stat [scan $buf "X%1dA%1dC%1dS%2d" x a c s ]
              if { $stat != 4 } {
                  set buf [insIfRead /${ins} "X" 32]
                  set stat [scan $buf "X%1dA%1dC%1dS%2d" x a c s ]
                  if { $stat != 4 } { return -code error "failed reading Oxford itc4 status" }
              }
              varDoSet /${ins}/setup/gas_control -v [ expr $a / 2 ]
              varDoSet /${ins}/setup/heat_control -v [ expr $a % 2 ]
          }              
