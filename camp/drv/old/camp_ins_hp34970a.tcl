# camp_ins_hp34970a.tcl: 
# Camp driver for the Hewlett-Packard 34970A Data acquisition/switch unit.
#
# Donald Arseneau      revised     29-Jul-2003
#
CAMP_INSTRUMENT /~ -D -T "HP 34970A voltmeter" \
    -H "Hewlett-Packard 34970A Data acquisition / Switch unit" -d on \
    -initProc HP34970A_init \
    -deleteProc HP34970A_delete \
    -onlineProc HP34970A_online \
    -offlineProc HP34970A_offline
proc HP34970A_init { ins } {
# insSet /${ins} -if rs232 0.5 /tyCo/2 9600 8 none 2 CRLF LF 10
  insSet /${ins} -if gpib 0.1 9 LF LF
}
proc HP34970A_delete { ins } {
  insSet /${ins} -line off
}
proc HP34970A_online { ins } {
  global HPdasu
  insIfOn /${ins}
  #  Record interface type: rs232 or gpib
  set type [insGetIfTypeIdent /${ins}]
  set HPdasu(${ins},itype) $type
  varRead /${ins}/setup/id
  if { [varGetVal /${ins}/setup/id] == 0 } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
  foreach ch {A B C D E F} {
     catch {
        varSet /${ins}/setup/setup_${ch}/channel_num -v [varGetVal /${ins}/setup/setup_${ch}/channel_num]
  }  }
}
proc HP34970A_offline { ins } {
  insIfOff /${ins}
}



CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc HP34970A_id_r
  proc HP34970A_id_r {ins} {
      set id 0
      if { [catch {insIfRead /${ins} "*IDN?" 80} buf] == 0} {
	  set id [ expr [scan $buf " HEWLETT-PACKARD,34970A,%d," val] == 1 ]
      }
      varDoSet /${ins}/setup/id -v $id -m $buf
  }



# Loop defining multiple channel paths.  Only a few (6) are defined, and each of
# these can be mapped to one of the 300 multiplexer channels.  More can be added 
# easily by adding to the list of letters.  See the same list in HP34970A_online.

foreach ch {A B C D E F} {

    CAMP_FLOAT /~/read_$ch -D -d off -R -r off -P -p off -L \
            -T "ch $ch reading" -units "sensor?" \
            -H "Volts/ohms/etc Reading, $ch" \
            -readProc [list HP34970A_reading_r $ch]

    CAMP_STRUCT /~/setup/setup_${ch} -D -d on -T "Configure $ch"

    CAMP_INT /~/setup/setup_${ch}/channel_num -D -d on -T "Channel ${ch} number" \
            -H "Multiplexer channel number used for \"${ch}\". 101 to 322, or 0 for none" \
            -S -s on -v 0 \
            -writeProc [list HP34970A_channel_w $ch]

    CAMP_SELECT /~/setup/setup_${ch}/meas_type -D -d on -S -s on -T "Measurement type" \
            -H "Type of measurement used for read_${ch}" \
            -selections DC_Voltage DC_Current AC_Voltage AC_Current Resistance Frequency \
            -writeProc [list HP34970A_type_w $ch]

    CAMP_SELECT /~/setup/setup_${ch}/int_time -T "Integration time" \
            -D -d on -S -s off -R -r off -v 3 \
            -H "Integration time to read ${ch}, given in \"NPLC\" (1/60 sec)" \
            -selections 0.02 0.2 1.0 2.0 10.0 20.0 100.0 200.0 MIN MAX \
            -readProc [list HP34970A_integration_r $ch] -writeProc [list HP34970A_integration_w $ch]

#  More set-up parameters can be added here; even those specific to a particular
#  measurement type.

}


proc HP34970A_reading_r { ch ins } {
    global HPdasu
    set chnum [HP34970A_channum $ins $ch]
    if { [catch { insIfReadVerify /${ins} ":MEAS:$HPdasu($ins,$ch,sense)? (@${chnum})" \
            80 /${ins}/read_$ch " %g" 2 } ] } {
        HP34970A_check_err ${ins}
    }
}

proc HP34970A_channel_w { ch ins target } {
    if { $target <= 0 } {
        varDoSet /${ins}/setup/setup_${ch}/channel_num -v 0
        varDoSet /${ins}/read_${ch} -r off -d off -p off
    } elseif { $target > 100 && $target < 400 && fmod($target,100) <= 22 } {
        varDoSet /${ins}/setup/setup_${ch}/channel_num -v $target
        varDoSet /${ins}/setup/setup_${ch}/int_time -r on -s on
        varDoSet /${ins}/read_${ch} -r on -d on -z \
           -m "[varSelGetValLabel /${ins}/setup/setup_${ch}/meas_type] reading from multiplexer channel $target"
        # (re)apply measurement type to this new channel number:
        varSet /${ins}/setup/setup_${ch}/meas_type -v [varGetVal /${ins}/setup/setup_${ch}/meas_type]
    } else {
        return -code error "Invalid channel number: $target.  Use 101-122, 201-222, 301-322, or 0 for none"
    }
}


proc HP34970A_type_w { ch ins val } {
    global HPdasu
    set chnum [HP34970A_channum $ins $ch]

    varDoSet /${ins}/setup/setup_${ch}/meas_type -v $val
    set type [varSelGetValLabel /${ins}/setup/setup_${ch}/meas_type]
    set units {}
    switch $type {
	DC_Voltage {
            set units V
            set HPdasu($ins,$ch,sense) "VOLT:DC"
        }
	DC_Current {
            set units A
            set HPdasu($ins,$ch,sense) "CURR:DC"
        }
	AC_Voltage {
            set units V
            set HPdasu($ins,$ch,sense) "VOLT:AC"
        }
	AC_Current {
            set units A
            set HPdasu($ins,$ch,sense) "CURR:AC"
        }
	Resistance {
            set units Ohm
            set HPdasu($ins,$ch,sense) "RES"
	    insIfWrite /${ins} "RES:OCOMP ON, (@${chnum})"
        }
	Frequency {
            set units Hz
            set HPdasu($ins,$ch,sense) "FREQ"
        }
    }
    insIfWrite /${ins} "CONF:$HPdasu($ins,$ch,sense) (@${chnum})"
    insIfWrite /${ins} "FUNC \"$HPdasu($ins,$ch,sense)\",(@${chnum})"
    HP34970A_check_err ${ins}
    if { $val == 2 || $val == 3 } { # AC: can't use int_time
        varDoSet /${ins}/setup/setup_${ch}/int_time -r off -d off
    } else {
        varDoSet /${ins}/setup/setup_${ch}/int_time -r on -d on
        varRead /${ins}/setup/setup_${ch}/int_time
    }
    varDoSet /${ins}/read_${ch} -r on -d on -units $units -z \
        -m "$type reading from multiplexer channel $chnum"

}

# -selections 0.02 0.2 1.0 2.0 10.0 20.0 100.0 200.0 MIN MAX 
# Note that none are integers; otherwise "1" would indicate list index 1, which is "0.2"
proc HP34970A_integration_w { ch ins val } {
    global HPdasu
    set chnum [HP34970A_channum $ins $ch]
    varDoSet /${ins}/setup/setup_${ch}/int_time -v $val 
    set valstr [varSelGetValLabel /${ins}/setup/setup_${ch}/int_time]
    insIfWrite /${ins} ":$HPdasu($ins,$ch,sense):NPLC $valstr, (@${chnum})"
    HP34970A_check_err ${ins}
}
proc HP34970A_integration_r { ch ins } {
    global HPdasu
    set chnum [HP34970A_channum $ins $ch]
    set buf {}
    catch { insIfRead /${ins} ":$HPdasu($ins,$ch,sense):NPLC? (@${chnum})" 80 } buf
    if { [scan $buf { %f} itim] == 1 } {
        varDoSet /${ins}/setup/setup_${ch}/int_time -v $itim 
    } else {
        HP34970A_check_err ${ins}
        return -code error "Failed to read integration time: $buf"
    }
    HP34970A_check_err ${ins}
}
  
# The HP34970A meter does not return error codes or messages unless prompted,
# but will queue error messages waiting for retrieval.  HP34970A_check_err asks 
# for the messages until it gets the most recent.
proc HP34970A_check_err { ins } {
    set enum 0
    set err "No response from instrument"
    while { ([scan [set em [insIfRead /${ins} {:SYST:ERR?} 80] ] " %g," ec] == 1) } {
        if { $ec } {
            set enum $ec
            set err $em
        } else {
            if { $enum } {
                return -code error $err
            } else {
                return 0
            }
        }
    }
    return -code error $err
}

# Retrieve channel number.  Used in many procs.
proc HP34970A_channum { ins ch } {
    set chnum [varGetVal /${ins}/setup/setup_${ch}/channel_num]
    if { $chnum > 100 } { return $chnum }
    return -code error "You must choose a channel number first"
}    

