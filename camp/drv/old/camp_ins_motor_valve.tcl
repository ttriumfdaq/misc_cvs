# camp_ins_motor_valve.tcl
# 
# This is the tcl script camp device driver for a cryostat needle valve
# controller. The system is composed of an ADC channel in the tip850
# Industry Pack, and a motor channel in the te28 Industry Pack. The ADC
# reads a pot to measure the absolute position of the valve.  The motor
# controller does most of the work, but knows its relative position only.
#
# Donald Arseneau.      
# Last modified            13-Aug-2009

CAMP_INSTRUMENT /~ -D -T "Motorized Valve" -d on \
    -initProc moval_init \
    -deleteProc moval_delete \
    -onlineProc moval_online \
    -offlineProc moval_offline
  proc moval_init { ins } {
      insSet /${ins} -if none 0.0
      insIfOn /${ins}
      varDoSet /${ins}/setup/connect -p on -p_int 4
  }
  proc moval_delete { ins } { 
      insSet /${ins} -line off 
  }
  proc moval_online { ins } {
      insIfOn /${ins}
      varDoSet /${ins}/setup/connect -p on -p_int 1
  }
  proc moval_offline { ins } {
      insIfOff /${ins}
  }

  CAMP_SELECT /~/status -D -R -P -T "Valve movement status" \
        -d on -r on -p off -p_int 10 -v 8 -H "Indicator for valve status" \
        -selections Stopped Closed Opening Closing Open_limit Stuck Stuck_closed Timed_out Need_channel \
	-readProc moval_status_r
#   moval_status_r gives the status back as a return value, so I can use
#   set var [varRead /{$ins}/status] (if no error return)
    proc moval_status_r { ins } {
	set s [varGetVal /${ins}/status]
        if { $s == 8 } {
            catch { varRead /${ins}/setup/connect }
            set s [varGetVal /${ins}/status]
        }
        if { $s == 8 } {
            return $s
        }
	set olds $s
	varRead /${ins}/read_position
	set v [varGetVal /${ins}/read_position]
	set dv [expr { $v - [varGetVal /${ins}/internal/prev_valve] } ]
	set m [varGetVal /${ins}/motor_position]
	set dm [expr { $m - [varGetVal /${ins}/internal/prev_motor] } ]
	set t [varGetVal /${ins}/setup/tolerance ]
	set ol [varGetVal /${ins}/setup/open_limit]

#       if valve did not move enough to be sure, retain previous record
#       of previous position (so effectively larger step sizes).
#        if { abs( $dm ) > $t && abs( $dm ) > 0.0 } {
#        }
        varDoSet /${ins}/internal/prev_valve -v $v
	varDoSet /${ins}/internal/prev_motor -v $m
#
        if { ($dm > 0.0) && ($dv > $dm - $t || $dv > $t) } { # Opening
	    if { $v >= $ol } { # Reached open-limit, so stop
		moval_stop $ins
		varDoSet /${ins}/status -v 4 -p on -p_int 10
		return 4
	    } else { # Opening
		varDoSet /${ins}/status -v 2
		return 2
	    }
	}
        if { ($dm < 0.0) && ($dv < $dm + $t || $dv < -$t) } { # Closing
	    varDoSet /${ins}/status -v 3
	    return 3
	}
	#  neither opening nor closing, so must be stopped or stuck:
	if { $s < 5 } { # only allow "stopped" if not already "stuck" or "timed out"
            #  stuck if motor is moving...
            set s [ expr ( abs( $dm ) > 0.3*$t ) ? 5 : 0 ]
	    #  and possibly at a limit...
	    if { ($s == 0) && ($v >= $ol) } { # open limit
		set s 4
	    } elseif { $v <= $t } { # closed limit, so closed (1) or stuck closed (6)
		incr s
	    }
	}
	varDoSet /${ins}/status -v $s
        if { $s >= 4 } { moval_stop $ins }
    }

  CAMP_FLOAT /~/set_position -D -S -L -T "Set the valve (motor) position" \
        -d on -s off -units Turns -writeProc moval_set_position_w
    proc moval_set_position_w { ins target } {
        if { [varGetVal /${ins}/motor/IPChan] < 0 } {
            return -code error "Please set /${ins}/setup/channel"
        }
	set dest $target
	set ol [varGetVal /${ins}/setup/open_limit]
	if { ($dest < -.5) || ($dest > $ol) } {
	    return -code error "Destination out of limits"
	}
	moval_stop $ins
	set p [varGetVal /${ins}/motor_position]
	set v [varGetVal /${ins}/read_position]
	if { ($dest > $p) && ($v + $dest-$p >= $ol) } {
	    set dest [expr {$ol+$p-$v}]
	}
	if { $dest > $p } {
            set h [varGetVal /${ins}/setup/hysteresis]
	} else {
            set h [varGetVal /${ins}/internal/loose]
        }
        varDoSet /${ins}/internal/loose -v $h
	if { $v + $h >= $ol } { set h 0.0 }
	varDoSet /${ins}/set_position -v $dest
	# Be careful if a movement was interrupted that may have left the drive train loose
	if { $dest + $h > $p } { # We want to open (or the linkage is sloppy)
	    # force sequencer to see opening for at least one iteration.
	    set p -99.
	    varDoSet /${ins}/status -v Opening
	    # set first destination to be further open, by hysteresis amount
	    set dest [expr { $dest + $h } ]
	} else { # we want to close
	    # force sequencer to see closing for at least one iteration.
	    set p 99.
	    varDoSet /${ins}/status -v Closing
	}
	varDoSet /${ins}/internal/prev_valve -v $p
#	varDoSet /${ins}/internal/prev_motor -v $p
	# Calculate an appropriate poll interval for do_move based on the speed
 	# of valve rotation and the expected uncertainty in readback (tolerance)
	set mpi [expr { abs(( 0.08 + [varGetVal /${ins}/setup/tolerance] )/
			    [varGetVal /${ins}/motor/clip_vel]) }]
        set mpi [expr { $mpi < 0.6 ? 0.6 : ( $mpi > 10 ? 10 : $mpi ) }]
	varSet /${ins}/motor/destin -v $dest
	# Start sequencer and schedule a timeout
	varDoSet /${ins}/internal/do_move -p on -p_int $mpi -v MOVING
	varDoSet /${ins}/abort -p on -p_int [varGetVal /${ins}/setup/timeout]
    }

  CAMP_FLOAT /~/read_position -D -R -P -L -A -T "Valve position (pot reading)" \
	-H "Read valve position from potentiometer" \
        -d on -r off -units Turns -tol 0.1 -p off \
	-readProc moval_read_position_r
    proc moval_read_position_r { ins } {
	varRead /${ins}/motor_position
	if { [varGetVal /${ins}/setup/pot] == 1 } {
	    varRead /${ins}/adc/adc_read
	    varDoSet /${ins}/read_position -v [ format %.3f [ varGetVal /${ins}/adc/adc_read ] ]
	} else { # broken pot
	    varDoSet /${ins}/read_position -v [ format %.3f [ varGetVal /${ins}/motor_position ] ]
	}
	# This alert tests that the motor has not slipped away from the valve:
	varTestAlert /${ins}/read_position [varGetVal /${ins}/motor_position]
    }

  CAMP_FLOAT /~/motor_position -D -R -P -L -T "Display the motor position" \
        -d on -r off -units Turns -p off \
	-readProc moval_motor_position_r
    proc moval_motor_position_r { ins } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	MotorPosition $pos $chan motor_pos
	varDoSet /${ins}/motor_position -v [format "%.4f" $motor_pos]
    }

  CAMP_SELECT /~/abort -D -S -R -P -T "Abort valve movement" \
	-d on -s on -r off \
	-selections "Abort Movement" " " "Never mind" "Timed out" \
	-readProc moval_abort_r -writeProc moval_abort_w
    proc moval_abort_r { ins } {
	moval_abort_w $ins 0
	varDoSet /${ins}/abort -v 3
    }
    proc moval_abort_w { ins target } {
	if { $target == 0 } {
	    moval_stop $ins
	    varDoSet /${ins}/abort -v 0
	} else {
	    varDoSet /${ins}/abort -v 2
	}
	varDoSet /${ins}/status -p on -p_int 10
    }

  # Jiggle the valve (to clear crud):
  # Close by jiggle_amount (or hysteresis)
  # Open to jiggle_amount more than the setpoint
  # Close back to the setpoint
  # 
  # The action is performed by setting OR polling the jiggle variable.

  CAMP_SELECT /~/jiggle -D -S -R -P -T "Jiggle Valve" \
	-d on -s on -r off \
	-selections "Jiggle Valve" \
	-readProc moval_jiggle_r -writeProc moval_jiggle_w
    proc moval_jiggle_r { ins } {
        moval_jiggle_w $ins 0
    }
    proc moval_jiggle_w { ins target } {
        varRead /${ins}/status
        if { [ varGetVal /${ins}/status ] < 2 } {# If stopped or closed
            set h [varGetVal /${ins}/setup/hysteresis]
            set j [varGetVal /${ins}/setup/jiggle_amount]
            if { $j < $h } { set j $h }
            set m [varGetVal /${ins}/motor_position]
            set m [expr { $m - $j < 0.0 ? 0.0 : $m - $j }]
            # Calculate an appropriate poll interval
            set mpi [expr { abs(( 0.08 + [varGetVal /${ins}/setup/tolerance] )/
			    [varGetVal /${ins}/motor/clip_vel]) }]
            set mpi [expr { $mpi < 0.6 ? 0.6 : ( $mpi > 10 ? 10 : $mpi ) }]
            varDoSet /${ins}/internal/do_jiggle -p on -p_int $mpi 
            varSet /${ins}/motor/destin -v $m
        }
    }

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on \
  -H "Select this to configure motor valve."

  # Connect: Read this to initialize position and detect if a motor is hooked up,
  # and the channel has been assigned.  This is polled until a connection is made.
  # If status = "need_channel", then we are newly defined (never yet connected)
  # so the motor offset is arbitrary, so we perform "equate"

  CAMP_SELECT /~/setup/connect -D -R -P -T "Test connection" \
        -d on -r on -v 0 -p off -p_int 1 \
        -selections FALSE TRUE \
	-readProc moval_connect_r
    proc moval_connect_r { ins } {
        varDoSet /${ins}/setup/connect -v 0
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
        if { $chan >= 0 } {
	    if { [varGetVal /${ins}/status] == 8 } {# new connection
		insIfWrite /${ins} "MotorReset $pos $chan 0"
	    } 
            varSet /${ins}/adc/gain -v [varGetVal /${ins}/adc/gain]
            moval_apply_params $ins
            varDoSet /${ins}/read_position -r on
            varDoSet /${ins}/motor_position -r on
            varDoSet /${ins}/setup/set_zero -s on
            varDoSet /${ins}/setup/equate -s on
            if { ! ( [catch {varRead /${ins}/adc/adc_read}] ||
                     [catch {varRead /${ins}/motor_position}] ) } {
                varDoSet /${ins}/setup/connect -p off -v 1
                varDoSet /${ins}/set_position -s on
                moval_stop $ins
                varDoSet /${ins}/internal/prev_valve -v [varGetVal /${ins}/read_position]
                varDoSet /${ins}/internal/prev_motor -v [varGetVal /${ins}/motor_position]
		varSet /${ins}/setup/equate -v 0
                varDoSet /${ins}/status -v 0 -p on -p_int 10
                return
            }
        }
    }

  CAMP_SELECT /~/setup/channel -D -S -T "Channel" \
        -d on -s on -selections A B \
	-H "Motor control output channel" \
	-writeProc moval_channel_w
    proc moval_channel_w { ins target } {
        varDoSet /${ins}/setup/channel -v $target
        # ADC channels 6 and 7 reserved for motor control
        varDoSet /${ins}/adc/IPChan   -v [expr {$target + 6}]
        varDoSet /${ins}/motor/IPChan -v $target
	varRead /${ins}/setup/connect
    }

  CAMP_SELECT /~/setup/which_valve -D -S -T "Identify valve" \
        -d on -s on \
	-selections msPiggy HGCryo CFCryo HiTime Bnmr Bnqr \
	-writeProc moval_which_valve_w
    proc moval_which_valve_w { ins target } {
	set file [lindex [string tolower "msPiggy HGCryo CFCryo HiTime Bnmr Bnqr"] $target ]
	source "./dat/mval_${file}.ini"
	if { abs([varGetVal /${ins}/motor/slope]) < 0.01 || 
	       [varGetVal /${ins}/motor/vel] > 20 } {
	    return -code error "Invalid old instrument definition file."
	}
        varSet /${ins}/adc/gain -v [varGetVal /${ins}/adc/gain]
	varDoSet /${ins}/setup/which_valve -v $target
        varSet /${ins}/setup/equate -v 0
    }

  CAMP_INT /~/setup/timeout -D -S -T "Timeout period" \
	-d on -s on -v 15 -units seconds \
	-H "Set timeout limit (seconds) for movements." \
	-writeProc moval_timeout_w
    proc moval_timeout_w { ins target } {
	if { $target > 0 } {
	    varDoSet /${ins}/setup/timeout -v $target
	}
    }

  CAMP_FLOAT /~/setup/hysteresis -D -S -T "Hysteresis" \
	-d on -s on -v 0.03 -units Turns \
	-H "How much slop in the gears?" \
	-writeProc moval_hysteresis_w
    proc moval_hysteresis_w { ins target } {
	varDoSet /${ins}/setup/hysteresis -v $target
    }

  CAMP_FLOAT /~/setup/open_limit -D -S -T "Open limit" \
	-d on -s on -v 2.5 -units Turns \
	-H "Set maximum opening allowed for valve" \
	-writeProc moval_open_limit_w
    proc moval_open_limit_w { ins target } {
	varDoSet /${ins}/setup/open_limit -v [expr {$target < 0.0 ? 0.0 : $target}]
    }

  CAMP_FLOAT /~/setup/tolerance -D -S -T "Position tolerance" \
	-d on -s on -v 0.01 -units Turns \
	-H "Uncertainty of read_position" \
	-writeProc moval_tolerance_w
    proc moval_tolerance_w { ins target } {
	varDoSet /${ins}/setup/tolerance -v [expr {$target < 0.0 ? 0.0 : $target}]
    }

  CAMP_FLOAT /~/setup/jiggle_amount -D -S -T "Jiggle by" \
	-d on -s on -v 0.03 -units Turns \
	-H "The jiggle operation means momentarily moving back and forth by this amount" \
	-writeProc moval_jiggle_amt_w
    proc moval_jiggle_amt_w { ins target } {
	varDoSet /${ins}/setup/jiggle_amount -v $target
    }

  CAMP_SELECT /~/setup/pot -D -S -T "Potentiometer" \
	-d on -s on -v 1 -selections "Broken" "OK" \
	-H "Say whether potentiometer is functioning" \
	-writeProc moval_pot_w
    proc moval_pot_w { ins target } { 
	varDoSet /${ins}/setup/pot -v $target
    }

# "equate" is to declare motor offset so it matches the POT position.
# If pot has been declared broken, though, we set the motor offset
# so it matches the "destination", which is useful when the Camp
# server reboots.
  CAMP_SELECT /~/setup/equate -D -S -T "Force motor = position" \
        -d on -s off -v 0 -selections "Equate" \
	-H "Declare motor_position = read_position. Do this after manual move or clutch slip" \
	-writeProc moval_equate_w
    proc moval_equate_w { ins target } {
	if { [varGetVal /${ins}/setup/pot] == 0 } { # Pot is claimed to be broken!
	    varRead /${ins}/motor_position
	    set p [varGetVal /${ins}/motor/destin]
	} else {
	    varRead /${ins}/read_position ; # includes reading motor_position
	    set p [varGetVal /${ins}/read_position]
	}
        varSet /${ins}/motor/offset -v [expr {
	    $p - [varGetVal /${ins}/motor_position] + [varGetVal /${ins}/motor/offset] } ]
	varRead /${ins}/read_position
	varDoSet /${ins}/set_position -v [varGetVal /${ins}/motor_position]
	varDoSet /${ins}/internal/prev_valve -v [varGetVal /${ins}/read_position]
	varDoSet /${ins}/internal/prev_motor -v [varGetVal /${ins}/motor_position]
	varDoSet /${ins}/status -v 0
	varRead /${ins}/status
    }

  CAMP_SELECT /~/setup/set_zero -D -S -T "Zero position" \
        -d on -s off -v 0 -selections "Zero" \
	-H "Set parameters so the current position is zero (closed)." \
	-writeProc moval_set_zero_w
    proc moval_set_zero_w { ins target } {
	if { [varGetVal /${ins}/setup/pot] == 0 } { # Pot is claimed to be broken!
	    varDoSet /${ins}/motor/destin -v 0
	} else {
	    varRead /${ins}/read_position
	    set p [varGetVal /${ins}/read_position]
	    varSet /${ins}/adc/offset -v [expr { 
		 [varGetVal /${ins}/adc/offset] - [varGetVal /${ins}/read_position] } ]
	}
	moval_equate_w ${ins} 0
	varDoSet /${ins}/status -v Closed -p on -p_int 10
    }


CAMP_STRUCT /~/motor -D -T "Motor actions" -d on \
        -H "Motor actions.  Don't use directly."

  CAMP_FLOAT /~/motor/destin -D -S -T "Move motor" -d on -s on \
        -writeProc moval_destin_w
    proc moval_destin_w { ins target } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	insIfWrite /${ins} "MotorMove $pos $chan [expr $target]"
	varDoSet /${ins}/motor/destin -v $target
    }

  CAMP_SELECT /~/motor/stop -D -S -R -P -T "Motor stop" \
	-d on -s on -r on -p off -H "Stop the motor movement" \
	-selections STOP \
	-readProc moval_stop_r \
	-writeProc moval_stop_w
    proc moval_stop_r { ins } {
	varSet /${ins}/motor/stop -p off
	set status [catch {moval_stop_w /${ins} STOP }]
    }
    proc moval_stop_w { ins target } {
        set pos [varGetVal /${ins}/motor/IPPos]
        set chan [varGetVal /${ins}/motor/IPChan]
	insIfWrite /${ins} "MotorStop $pos $chan 0"
    }

  CAMP_INT /~/motor/IPPos -D -T "Position of IP on carrier" \
        -d on -v 2 

  CAMP_INT /~/motor/IPChan -D -S -T "Channel within IP" \
        -d on -s on -v -1 \
	-writeProc moval_motor_IPChan_w
    proc moval_motor_IPChan_w { ins target } {
	varDoSet /${ins}/motor/IPChan -v $target
    }

# slope:  slope for converting integer "engineering units" to Turns
# turns =  - e.u. / slope  +  offset    or

  CAMP_FLOAT /~/motor/slope -D -S -T "Slope" \
	-d on -s on -v "-200" -units "m.rev/v.rev" \
	-H "Conversion: motor revolutions per valve turn" \
	-writeProc motor_slope_w
    proc motor_slope_w { ins target } {
        varDoSet /${ins}/motor/slope -v $target
	moval_apply_params $ins
    }

  CAMP_FLOAT /~/motor/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 -units Turns \
	-H "Conversion offset: offset to zero when valve closed" \
	-writeProc motor_offset_w
    proc motor_offset_w { ins target } {
        varDoSet /${ins}/motor/offset -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/encoder -D -S -T "Encoder" \
	-d on -s on -v 2000 -units "tics/enc.rev" \
	-H "Conversion: ticks per encoder revolution" \
	-writeProc motor_encoder_w
    proc motor_encoder_w { ins target } {
        varDoSet /${ins}/motor/encoder -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/filter_kp -D -S -T "Filter - Proportional" \
	-d on -s on -v 100 \
        -H "Proportional parameter for PID loop in controller" \
	-writeProc motor_filter_kp_w
    proc motor_filter_kp_w { ins target } {
        varDoSet /${ins}/motor/filter_kp -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/filter_ki -D -S -T "Filter - Integral" \
	-d on -s on -v 0 \
	-H "Integral parameter for PID loop in controller" \
	-writeProc motor_filter_ki_w
    proc motor_filter_ki_w { ins target } {
        varDoSet /${ins}/motor/filter_ki -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/filter_kd -D -S -T "Filter - Differential" \
	-d on -s on -v 2000 \
	-H "Differential parameter for PID loop in controller" \
	-writeProc motor_filter_kd_w
    proc motor_filter_kd_w { ins target } {
        varDoSet /${ins}/motor/filter_kd -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/filter_il -D -S -T "Filter - il" \
	-d on -s on -v 0 \
	-writeProc motor_filter_il_w
    proc motor_filter_il_w { ins target } {
        varDoSet /${ins}/motor/filter_il -v $target
	moval_apply_params $ins
    }

  CAMP_INT /~/motor/filter_si -D -S -T "Filter - si" \
	-d on -s on -v 0 \
	-writeProc motor_filter_si_w
    proc motor_filter_si_w { ins target } {
        varDoSet /${ins}/motor/filter_si -v $target
	moval_apply_params $ins
    }

  CAMP_FLOAT /~/motor/accel -D -S -T "Acceleration" \
	-d on -s on -v 1.0 \
	-H "Parameter for motor acceleration" \
	-writeProc motor_accel_w
    proc motor_accel_w { ins target } {
        varDoSet /${ins}/motor/accel -v $target
	moval_apply_params $ins
    }

  CAMP_FLOAT /~/motor/vel -D -S -T "Velocity" \
	-d on -s on -v 0.1 -units "rev/sec" \
	-H "Valve rotation velocity" \
	-writeProc motor_vel_w
    proc motor_vel_w { ins target } {
        varDoSet /${ins}/motor/vel -v $target
	moval_apply_params $ins
    }

  CAMP_FLOAT /~/motor/clip_vel -D -T "Clipped Velocity" \
	-d on -units "rev/sec" \
	-H "Valve rotation velocity, as limited by frontend" 

CAMP_STRUCT /~/adc -D -T "ADC specific variables" -d on \
        -H "ADC variables.  Don't use directly."

  CAMP_SELECT /~/adc/IPPos -D -T "ADC IP Position" \
	-d on -v 3 -selections 0 1 2 3 \
	-H "Slot number in the Industry Pack occupied by ADC board"

  CAMP_INT /~/adc/IPChan -D -S -T "ADC Channel (0-7)" \
        -d on -s on -v -1 \
	-H "ADC input channel (0-7)" \
	-writeProc moval_adc_IPChan_w
    proc moval_adc_IPChan_w { ins target } {
	varDoSet /${ins}/adc/IPChan -v $target
    }

  CAMP_SELECT /~/adc/gain -D -S -T "ADC Gain" \
        -d on -s on -v 1 -selections x1 x2 x4 x8 \
	-writeProc moval_adc_gain_w
    proc moval_adc_gain_w { ins target } {
	set gain $target
	set pos [varGetVal /${ins}/adc/IPPos]
	set chan [varGetVal /${ins}/adc/IPChan]
        if { $chan >= 0 } {
            GainSet $pos $chan gain
            varDoSet /${ins}/adc/gain -v $gain
            # Gain setting takes some time to be effective
            sleep 0.25
        }
    }

# slope:  slope for converting "adc units" to Turns
# value =  slope * a.u.  +  offset    or
# a.u. = ( displayed_value - offset ) / slope 

  CAMP_FLOAT /~/adc/slope -D -S -T "Slope" \
	-d on -s on -v 1.0 -units "Turns/adc" \
        -H "position(turns) = slope * adc + offset" \
	-writeProc moval_adc_slope_w
    proc moval_adc_slope_w { ins slope } {
	varDoSet /${ins}/adc/slope -v $slope
    }

  CAMP_FLOAT /~/adc/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 -units Turns \
	-writeProc moval_adc_offset_w
    proc moval_adc_offset_w { ins offset } {
	varDoSet /${ins}/adc/offset -v $offset
    }

  CAMP_FLOAT /~/adc/adc_read -D -R -P -T "Read ADC" \
	-d on -r on -units Turns \
	-readProc  moval_adc_read_r
    proc moval_adc_read_r { ins } {
	set pos [varGetVal /${ins}/adc/IPPos]
	set chan [varGetVal /${ins}/adc/IPChan]
	set slope [varGetVal /${ins}/adc/slope]
	set offset [varGetVal /${ins}/adc/offset]

	AdcRead $pos $chan adc_read
	set value [expr {$adc_read * $slope + $offset}]
	varDoSet /${ins}/adc/adc_read -v $value
    }



CAMP_STRUCT /~/internal -D -T "Control procedures" -d on \
        -H "Internal control procedures and variables."

  CAMP_SELECT /~/internal/find_closed -D -S -T "Find the fully-closed position" \
	-d on -s on -v 1 -selections "Find_Closed" " " \
	-H "Drive the valve closed until it slips; set as zero" \
	-writeProc moval_find_closed_w
    proc moval_find_closed_w { ins target } {
        varDoSet /${ins}/internal/find_closed -v $target
	if { $target == 0 } {
	    moval_stop $ins
	    varDoSet /${ins}/internal/prev_motor -v [varGetVal /${ins}/motor_position]
	    varDoSet /${ins}/internal/prev_valve -v [varGetVal /${ins}/read_position]
	    varDoSet /${ins}/internal/do_find -p on -p_int 0.7 -v FINDING
	}
    }

# This is an aid to calibrating the parameters for new valves, or
# when valves have been modified.  It doesn't give totally accurate
# parameters though.

CAMP_SELECT /~/internal/calibrate -D -S -R -P -T "Calibrate the parameters" \
    -d on -s on -r on -p off -v 0 \
    -selections " " start did_close did_open did_engage_gears waiting a_bit_more \
    -readProc moval_do_calib_r -writeProc moval_do_calib_w \
    -H "To calibrate an unknown or new valve, set this to each value in turn, following instructions"
proc moval_do_calib_w { ins target } {
    global mv_calp0
    global mv_calstep
    global mv_calstage
    switch $target {
    1 {
        moval_stop $ins
        varDoSet /${ins}/internal/calibrate -v 1 -p off -m \
          "Manually close valve, then set me"
      }
    2 {
        varRead /${ins}/adc/adc_read
        set mv_calp0 [varGetVal /${ins}/adc/adc_read]
        varDoSet /${ins}/internal/calibrate -v 2 -m \
          "Now open valve 1 full turn, then set me"
      }
    3 {
        varRead /${ins}/adc/adc_read
        set p1 [varGetVal /${ins}/adc/adc_read] 
        set dp [expr $p1 - $mv_calp0 ]
        set sl [varGetVal /${ins}/adc/slope]
        if { abs($dp/$sl) < 9.0 } {
            varDoSet /${ins}/internal/calibrate -v 0 -m "Failed calibration"
            return -code error "Readback did not change.  Something is wrong."
        }
        set slope [expr $sl / $dp ]
        set offset [expr ( [varGetVal /${ins}/adc/offset] - $mv_calp0 ) / $dp ]
        varSet /${ins}/adc/slope -v $slope
        varSet /${ins}/adc/offset -v $offset
        varDoSet /${ins}/internal/calibrate -v 3 -m \
          "Now twist valve to engage gears, if necessary, then set me"
      }
    4 {
#       Ensure motor slope is in the right ballpark
        set slope [varGetVal /${ins}/motor/slope]
        if { abs( $slope ) > 10. } { set slope 10.0 }
        if { abs( $slope ) < 0.2 } { set slope 1.0 }
        varSet /${ins}/motor/slope -v $slope
#       Start up the motor calibtration loop:
        varRead /${ins}/read_position
        set mv_calstep .01
	set mv_calstage 0
        varDoSet /${ins}/internal/calibrate -v 5 -p on -p_int .2 -m \
          "Wait while I try calibrating the motor"
      }
    default {
        varDoSet /${ins}/internal/calibrate -v 0 -p off -m ""
        varDoSet /${ins}/status -p on -p_int 10
      }
    }
}
#
# This readProc performs the motor-position calibration.  It is invoked
# every 3 seconds by polling.  It moves the motor by increasing increments
# until it sees enough movement to make a calibration calculation.  It
# opens to almost two turns open, snugs up (guessed) hysteresis, then
# closes to less than .5 turn.  It sets the motor slope and offset to
# agree with the valve pot's over that closing move.
#
proc moval_do_calib_r { ins } {
    global mv_calp0
    global mv_calstep
    global mv_calstage

#   stop any unfinished movement and get position (adc_read is unrounded
#   so better than the value read_position)
    varSet /${ins}/motor/stop -v 0
    set mpre [varGetVal /${ins}/motor_position]
    varRead /${ins}/read_position
    set mpos [varGetVal /${ins}/motor_position]
    set vpos [varGetVal /${ins}/adc/adc_read]

    switch $mv_calstage {
     0 -
     2 {# initiate
	 varDoSet /${ins}/internal/prev_valve -v $vpos
	 varDoSet /${ins}/internal/prev_motor -v $mpos
	 set mv_calp0 $mpos
	 varSet /${ins}/motor/destin -v [expr $mv_calp0 + $mv_calstep]
         varDoSet /${ins}/internal/calibrate -p on -p_int 4.5
	 incr mv_calstage
       }
     1 {# opening to about 1.9
	 if { $mpos == $mv_calp0 } {# motor has not moved at all
             moval_stop $ins
	     varDoSet /${ins}/status -p on -p_int 10
	     varDoSet /${ins}/internal/calibrate -v 0 -p off -m "Failed motor calibration"
	     return
	 }
	 # total valve movement:
	 set moved [expr {$vpos - [varGetVal /${ins}/internal/prev_valve] } ]
	 # if closing, switch direction, and start over:
         if { $moved < -0.2 } { 
             set mv_calstep [expr {0.0 - $mv_calstep} ] 
             set mv_calstage 0
             return
         }
	 # if valve moved little, and motor moved as planned, then increase step size
	 if { ( abs( $moved ) < .1 ) && ( abs( $mpre - $mpos ) > .9*abs( $mv_calstep ) ) } {
	     set mv_calstep [expr $mv_calstep * 2.0 ]
	 }
	 if { $vpos > 1.8 } {# plenty open; snug up by calstep
	     set mv_calstep [expr {0.0 - $mv_calstep} ]
	     incr mv_calstage
	 }
	 varSet /${ins}/motor/destin -v [expr {$mpos + $mv_calstep}]
       }
     3 {# closing to < .5; rely on step sizes already determined.
	 if { [varGetVal /${ins}/read_position] < 0.5 } {# done, but wait a few sec before doing final reading
	     incr mv_calstage
	 } else {
	     varSet /${ins}/motor/destin -v [expr {$mpos + $mv_calstep}]
	 }
       }
     4 {# really done
	 set moved [expr { $vpos - [varGetVal /${ins}/internal/prev_valve ] } ]
	 set slope [expr { $moved * [varGetVal /${ins}/motor/slope] 
		     / ( $mpos - [varGetVal /${ins}/internal/prev_motor ] ) } ]
	 varSet /${ins}/motor/slope -v $slope
	 varSet /${ins}/setup/equate -v 0
	 varDoSet /${ins}/status -v 0 -p on -p_int 10
	 varDoSet /${ins}/internal/calibrate -v 0 -p off
       }
   }
}

  CAMP_FLOAT /~/internal/prev_valve -D -T "Previous valve position" \
	-d on -v 0.0 -units Turns 

  CAMP_FLOAT /~/internal/prev_motor -D -T "Previous motor position" \
	-d on -v 0.0 -units Turns 

  CAMP_FLOAT /~/internal/loose -D -T "Looseness" \
	-d on -units Turns -v 0.0
  
  CAMP_SELECT /~/internal/do_move -D -R -P -T "Movement sequencer" \
	-d on -r on -selections " " "MOVING" -readProc moval_do_move_r
    proc moval_do_move_r { ins } {
	varRead /${ins}/status
	set s [ varGetVal /${ins}/status ]
	if { ($s == 0) && [varGetVal /${ins}/internal/loose] > 0.0 } {
            # now stopped, but gears loose: remove hysteresis
            varSet /${ins}/motor/destin -v [varGetVal /${ins}/set_position]
            varDoSet /${ins}/internal/loose -v 0.0
            varDoSet /${ins}/status -v Closing
            varDoSet /${ins}/internal/prev_valve -v 98
	} else {
	    if { ($s != 2) && ($s != 3) } {
                # not opening or closing: finished (for good or bad)
		moval_stop $ins
		set d [expr { [varGetVal /${ins}/set_position] - \
                        [varGetVal /${ins}/motor_position] } ]
		if { $d < [varGetVal /${ins}/setup/tolerance] } {
		    varDoSet /${ins}/internal/loose -v 0.0
		}
		varDoSet /${ins}/status -p on -p_int 10
	    }
	}
    }    

  CAMP_SELECT /~/internal/do_jiggle -D -R -P -T "Jiggle sequencer" \
	-d on -r on -selections "Jiggle" \
	-readProc moval_do_jiggle_r
    proc moval_do_jiggle_r { ins } {
	varRead /${ins}/status
	set s [ varGetVal /${ins}/status ]
	if { $s != 3 } { # not closing
            set h [varGetVal /${ins}/setup/hysteresis]
            set j [varGetVal /${ins}/setup/jiggle_amount]
            if { $j < $h } { set j $h }
            varDoSet /${ins}/internal/loose -v $j
            varSet /${ins}/set_position -v [varGetVal /${ins}/set_position]
            varDoSet /${ins}/internal/do_jiggle -p off
        }
    }    

  CAMP_SELECT /~/internal/do_find -D -R -P -T "Find-closed sequencer" \
	-d on -selections " " "FINDING" -readProc moval_do_find_r
    proc moval_do_find_r { ins } {
	varDoSet /${ins}/internal/do_find -p on -p_int 1.0
	varRead /${ins}/status
# Stopped Closed Opening Closing Open_limit Stuck Stuck_closed Timed_out Need_channel
	switch [ varGetVal /${ins}/status ] {
	    0 -
            1 { # (stopped or `closed')
		varSet /${ins}/motor/destin -v [expr {[varGetVal /${ins}/motor/destin] - .1}]
	      }
	    2 { # (opening... huh?!)
		varDoSet /${ins}/internal/do_find -p off -v 0
		varDoSet /${ins}/internal/find_closed -v 1
	      }
	    3 { # (closing... do nothing)
	      }
	    default { # (finished)
		moval_stop $ins
		varDoSet /${ins}/internal/loose -v 0.0
		varSet /${ins}/setup/set_zero -v 0
	    }
	}
    }

# Stop any active sequencers and position polling; cancel any pending timeout;
# stop the motor movement, if any, and read the present position.  This procedure
# is used by set_position, abort, find_closed, do_move, do_find, and calibrate.
#
proc moval_stop { ins } {
    varSet   /${ins}/motor/stop -v STOP
    varDoSet /${ins}/internal/do_move -p off -v 0
    varDoSet /${ins}/internal/do_find -p off -v 0
    varDoSet /${ins}/internal/find_closed -v 1
    varDoSet /${ins}/read_position -p off
    varDoSet /${ins}/abort -p off -v 1
    varDoSet /${ins}/status -p off
    varRead  /${ins}/read_position
}

proc moval_apply_params { ins } {
    set pos [varGetVal /${ins}/motor/IPPos]
    set chan [varGetVal /${ins}/motor/IPChan]
    if { $pos < 0 || $chan < 0 } { return }

    set slope [varGetVal /${ins}/motor/slope]
    set vel [expr { abs([varGetVal /${ins}/motor/vel]) }]
    set enc [varGetVal /${ins}/motor/encoder]
    if { abs($slope) < 0.01 || $vel > 10 } {
        return -code error "Invalid old instrument definition"
    }
    set max [expr { 900000.0 / abs($slope*$enc*0.000256*65536) }]
    varDoSet /${ins}/motor/clip_vel -v [expr { $vel > $max ? $max : $vel }]

    MotorSlope $pos $chan $slope
    MotorVel $pos $chan $vel

    MotorFilterKP $pos $chan [varGetVal /${ins}/motor/filter_kp]
    MotorFilterKI $pos $chan [varGetVal /${ins}/motor/filter_ki]
    MotorFilterKD $pos $chan [varGetVal /${ins}/motor/filter_kd]
    MotorFilterIL $pos $chan [varGetVal /${ins}/motor/filter_il]
    MotorFilterSI $pos $chan [varGetVal /${ins}/motor/filter_si]
    MotorEncoder  $pos $chan [varGetVal /${ins}/motor/encoder]
    MotorOffset   $pos $chan [varGetVal /${ins}/motor/offset]
    MotorAccel    $pos $chan [expr { abs([varGetVal /${ins}/motor/accel]) }]
}

