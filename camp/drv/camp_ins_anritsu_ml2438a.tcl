# Anritsu ML2438A Power Meter
#

CAMP_INSTRUMENT /~ -D -T "Anritsu ML2438A Power Meter" \
    -d on \
    -initProc AML2438A_init \
    -deleteProc AML2438A_delete \
    -onlineProc AML2438A_online \
    -offlineProc AML2438A_offline
proc AML2438A_init { ins } {
  insSet /${ins} -if gpib 0.05 5 13 LF LF
}
proc AML2438A_delete { ins } {
  insSet /${ins} -line off
}
proc AML2438A_online { ins } {
  insIfOn /${ins}
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
  insIfWrite /$ins "MNMX 1,ON"
  insIfWrite /$ins "MNMX 2,ON"
}
proc AML2438A_offline { ins } {
  insIfOff /${ins}  # mandatory call
}

CAMP_FLOAT /~/max_pow_1 -D -d on -R -r on -P -L -A \
   -T "Channel 1 Max Power" -units "W" \
   -readProc AML2438A_mxp1_r
proc AML2438A_mxp1_r { ins } {
    insIfReadVerify /$ins {GMNMX 1} 40 /${ins}/max_pow_1 { %*f,%f} 2
    insIfWrite /$ins {MMRST 1}
}

CAMP_FLOAT /~/max_pow_2 -D -d on -R -r on -P -L -A \
   -T "Channel 2 Max Power" -units "W" \
   -readProc AML2438A_mxp2_r
proc AML2438A_mxp2_r { ins } {
    insIfReadVerify /$ins {GMNMX 2} 40 /${ins}/max_pow_2 { %*f,%f} 2
    insIfWrite /$ins {MMRST 2}
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc AML2438A_id_r
  proc AML2438A_id_r {ins} {
      set id 0
      if { [catch {insIfRead /${ins} "*IDN?" 80} buf] == 0} {
	  set id [ expr [scan $buf " ANRITSU,ML2438A,%d," val] == 1 ]
      }
      varDoSet /${ins}/setup/id -v $id
  }

