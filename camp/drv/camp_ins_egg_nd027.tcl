CAMP_INSTRUMENT /~ -D -T "EG&G ND027 Output Register" \
    -H "EG&G ND027 Output Register" -d on \
    -initProc nd027_init \
    -deleteProc nd027_delete \
    -onlineProc nd027_online \
    -offlineProc nd027_offline 
  proc nd027_init { ins } { insSet /${ins} -if camac 0.0 0.0 0 0 0 } 
  proc nd027_delete { ins } { insSet /${ins} -line off }
  proc nd027_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc nd027_offline { ins } { insIfOff /${ins} }
    CAMP_INT /~/output_set -D -S -L -T "Output setting" -d on -s on \
      -writeProc nd027_set_w
      proc nd027_set_w { ins target } {
	set target [expr $target & 0xfff]
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
	insIfWrite /${ins} "cfsa 16 $ext target q"
        for { set i 0 } { $i <= 11 } { incr i } {
	  varDoSet /${ins}/output${i} -v [expr ($target>>$i)&1]
        }
	varDoSet /${ins}/output_set -v $target
      }
    for { set i 0 } { $i <= 11 } { incr i } {
      CAMP_INT /~/output${i} -D -S -L -T "Output $i" -d on
    }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
      CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc nd027_id_r
        proc nd027_id_r { ins } {
          set id 0
          if {[catch {varSet /${ins}/output_set -v 0}] == 0} {set id 1}
	  varDoSet /${ins}/setup/id -v $id
        }
