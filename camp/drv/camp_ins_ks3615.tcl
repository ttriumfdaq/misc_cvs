CAMP_INSTRUMENT /~ -D -T "Kinetics Systems 3615 CAMAC Scaler Module" \
    -H "Kinetics Systems 3615 CAMAC Scaler Module" -d on \
    -initProc ks3615_init \
    -deleteProc ks3615_delete \
    -onlineProc ks3615_online \
    -offlineProc ks3615_offline 
  proc ks3615_init { ins } { insSet /${ins} -if camac 0.0 0.0 0 0 0 } 
  proc ks3615_delete { ins } { insSet /${ins} -line off }
  proc ks3615_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc ks3615_offline { ins } { insIfOff /${ins} }
    for { set i 1 } { $i <= 6 } { incr i } {
      CAMP_INT /~/scaler${i} -D -S -R -L -P -T "Scaler $i" -d on -s on -r on \
	-writeProc ks3615_s${i}_w -readProc ks3615_s${i}_r
        proc ks3615_s${i}_r { ins } "ks3615_s_r \$ins $i"
        proc ks3615_s${i}_w { ins target } "ks3615_s_w \$ins \$target $i"
    }
      proc ks3615_s_r { ins s } {
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] [expr $s-1]
        insIfRead /${ins} "cfsa 0 $ext val q" 0
	varDoSet /${ins}/scaler${s} -v $val
      }
      proc ks3615_s_w { ins target s } {
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] [expr $s-1]
        insIfWrite /${ins} "cfsa 9 $ext val q"
	varDoSet /${ins}/scaler${s} -v 0
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
      CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc ks3615_id_r
        proc ks3615_id_r { ins } {
          set val 0
          # Try to read scaler 1
          cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
          if { [catch { insIfRead /${ins} "cfsa 0 $ext val q" 0 }] == 0 } { 
            set val 1
          }
	  varDoSet /${ins}/setup/id -v $val
        }
    CAMP_STRUCT /~/controls -D -T "Control variables" -d on
      CAMP_SELECT /~/controls/clear_all -D -S -T "Clear all scalers" \
        -d on -s on -selections do_it -writeProc ks3615_clear_all
        proc ks3615_clear_all { ins target } {
          for { set i 1 } { $i <= 6 } { incr i } { 
            varSet /${ins}/scaler${i} -v 0
          }
        }
      CAMP_SELECT /~/controls/incr_all -D -S -T "Increment all scalers" \
        -d on -s on -selections do_it -writeProc ks3615_incr_all
        proc ks3615_incr_all { ins target } {
	  cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
          insIfWrite /${ins} "cfsa 25 $ext val q"
          for { set i 1 } { $i <= 6 } { incr i } { varRead /${ins}/scaler${i} }
        }
