# camp_ins_ggl_pulser.tcl
# Purpose:  This tcl script defines the pulser device in the TRIUMF muSR Gate
#           Generator Logic (GGL) board.
# Inputs:   None
# Precond:  A functioning tcl interpreter with VME interface.
# Outputs:  Returns a result code 
# Postcond: The driver will be created and available for access by the system

CAMP_INSTRUMENT /~ -D -T "GGL Pulser" -d on \
    -initProc ggl_p_init -deleteProc ggl_p_delete \
    -onlineProc ggl_p_online -offlineProc ggl_p_offline

  proc ggl_p_init { ins } { 
    insSet /${ins} -if vme 0.0 0.0 32768
  }
  proc ggl_p_delete { ins } {
      insSet /${ins} -line off
  }
  proc ggl_p_online { ins } {
      insIfOn /${ins}
      if { [catch {
          varRead /${ins}/enable
          varRead /${ins}/frequency
      } ] } {
	  insIfOff /${ins}
	  return -code error "Failed to read pulser state"
      }
  }
  proc ggl_p_offline { ins } {
      insIfOff /${ins}
  }

CAMP_SELECT /~/enable -D -R -S -P -T "Enable or diable" \
    -d on -r on -s on -p off -p_int 99 \
    -selections OFF ON \
    -readProc ggl_p_enable_r -writeProc ggl_p_enable_w

  proc ggl_p_enable_w { ins target } {
      insIfWrite /${ins} "0x14 byte ${target}"
      varDoSet /${ins}/enable -v $target
  }
  proc ggl_p_enable_r { ins } {
      set b [insIfRead /${ins} "0x14 byte" 15]
      varDoSet /${ins}/enable -v [expr { $b & 1 } ]
  }

CAMP_FLOAT /~/frequency -D -S -R -P -L -T "Pulser frequency" \
    -d on -s on -r on -p off -p_int 5 -units Hz \
    -readProc ggl_p_read -writeProc ggl_p_frequency_w

  proc ggl_p_frequency_w { ins freq } {
      varDoSet /${ins}/frequency -v [bounded $freq 0.77 2500.0]
      ggl_p_freq_duty $ins
  }

CAMP_FLOAT /~/duty_fac -D -S -R -P -L -T "Duty factor" \
    -d on -s on -r on -p off -p_int 5 -units {%} \
    -H "Duty factor: percent of time pulse is on" \
    -readProc ggl_p_read -writeProc ggl_p_duty_w

  proc ggl_p_duty_w { ins duty } {
      varDoSet /${ins}/duty_fac -v [bounded $duty 0.01 99.9999]
      ggl_p_freq_duty $ins
  }

  proc ggl_p_freq_duty { ins } {
      set freq [varGetVal /${ins}/frequency]
      set duty [varGetVal /${ins}/duty_fac]
      set ontime [expr {round(10000.0*(1.0/$freq)*($duty/100.0))}]
      set ontime [bounded $ontime 2 65530]
      set offtime [expr {round(10000.0*(1.0/$freq))-$ontime}]
      set offtime [bounded $offtime 2 65530]
      insIfWrite /${ins} "0x10 word ${ontime}"
      insIfWrite /${ins} "0x12 word ${offtime}"
      ggl_p_read $ins
  }

CAMP_FLOAT /~/on_time -D -S -R -P -L -T "Pulse duration" \
    -d on -s on -r on -p off -p_int 5 -units sec \
    -readProc ggl_p_read -writeProc ggl_p_ont_w

  proc ggl_p_ont_w { ins ont } {
      varDoSet /${ins}/on_time -v [bounded $ont 0.0002 6.55]
      ggl_p_times $ins
  }

CAMP_FLOAT /~/off_time -D -S -R -P -L -T "Pulse spacing" \
    -d on -s on -r on -p off -p_int 5 -units sec \
    -readProc ggl_p_read -writeProc ggl_p_offt_w

  proc ggl_p_offt_w { ins offt } {
      varDoSet /${ins}/off_time -v [bounded $offt 0.0002 6.55]
      ggl_p_times $ins
  }

  proc ggl_p_times { ins } {
      set ontime [expr {round(10000.0*[varGetVal /${ins}/on_time])}]
      set offtime [expr {round(10000.0*[varGetVal /${ins}/off_time])}]
      insIfWrite /${ins} "0x10 word ${ontime}"
      insIfWrite /${ins} "0x12 word ${offtime}"
      ggl_p_read $ins
  }

  proc ggl_p_read { ins } {
      set ontime [insIfRead /${ins} "0x10 word" 31]
      set ontime [expr {$ontime/10000.0}]
      varDoSet /${ins}/on_time -v $ontime
      set offtime [insIfRead /${ins} "0x12 word" 31]
      set offtime [expr {$offtime/10000.0}]
      varDoSet /${ins}/off_time -v $offtime
      set freq [format %.4f [expr { 1.0 / ($ontime + $offtime) }] ]
      varDoSet /${ins}/frequency -v $freq
      set duty [format %.4f [expr { 100.0 * $ontime / ($ontime + $offtime) }] ]
      varDoSet /${ins}/duty_fac -v $duty
  }

# Utility function used throughout:
proc bounded { x lo hi } {
    expr {($x)<($lo)? ($lo) : (($x)>($hi)? ($hi) : ($x)) }
}
