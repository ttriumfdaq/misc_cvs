/*
 *  Name:       camp_srv_utils.c
 *
 *  Purpose:    Some utilities for the CAMP server
 *              Routine to cleanly delete all instruments, routines to check
 *              instrument locking by users, and routines for time delay of
 *              execution.
 *
 *  Called by:  CAMP server routines only
 * 
 *  Revision history:
 *   v1.2  17-May-1994  TW
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef VMS
#include <ssdef.h>
#include <unixio.h>
#include "dirent.h" /* homemade version */
#else
#include <dirent.h>
#endif /* VMS */
#include "camp_srv.h"
#include "rpidok.h"
#ifdef VXWORKS
#include "sysLib.h" // sysClkRateGet
#endif // VXWORKS

static int checkPidOk( char* host_in, int pid, char* os );


/*
 *  Cleanly delete all instruments using the insdel RPC entry point.
 *  Called before adding a new system configuration and before the CAMP
 *  server shuts down.
 */
void 
srv_delAllInss( void )
{
    RES* pRes;
    DATA_req data_req;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	while( pVarList != NULL )
	{
	    data_req.path = pVarList->core.path;
	    pRes = campsrv_insdel_10400( &data_req, NULL );
	    _free( pRes );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock
}


/*
 *  camp_checkInsLock
 *
 *  Check to see if an instrument is locked by a client other than the
 *  caller of the current RPC call ().
 *
 *  Locking in this
 *  context refers to the CAMP facility for users (identified by a process
 *  ID on a particular internet host) to lock an instrument for private use.
 *  This is particularly useful when unmonitored client programs need to
 *  control an instrument and must be assured access without accidental
 *  tampering by another user.  In practice, however, it has been a rarely
 *  used aspect of CAMP.
 */
int 
camp_checkInsLock( CAMP_VAR* pVar, struct svc_req* rq )
{
    int camp_status = CAMP_INS_NOT_LOCKED;
    CAMP_VAR* pVar_ins;
    CAMP_INSTRUMENT* pIns;
    char host_client[LEN_NODENAME+1], lockHost[LEN_NODENAME+1];
    u_long pid_client;

       /*-----------------------------*/
      /*  Check if called by server  */
     /*  (server has privilege)     */
    /*-----------------------------*/
    if( rq == NULL ) 
      {
	camp_status = CAMP_INS_NOT_LOCKED;
	goto return_;
      }

    /*
     *  Get pointer to instrument data
     */
    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) 
      {
	camp_status = CAMP_INVAL_INS;
	goto return_;
      }

    if( !( pVar_ins->core.status & CAMP_INS_ATTR_LOCKED ) )
    {
	camp_status = CAMP_INS_NOT_LOCKED;
	goto return_;
    }

      /*--------------------------------------*/
     /*  Check if locked by another process  */
    /*--------------------------------------*/
    {
      struct authunix_parms* aup;
      aup = (struct authunix_parms*)rq->rq_clntcred;
      
      pid_client = aup->aup_gids[0];

      camp_strncpy( host_client, sizeof( host_client ), aup->aup_machname ); // strcpy( host_client, aup->aup_machname );
      stolower( host_client );
    }

    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;

    camp_strncpy( lockHost, sizeof( lockHost ), pIns->lockHost ); // strcpy( lockHost, pIns->lockHost );
    stolower( lockHost );

    if( ( pid_client != pIns->lockPID ) || // caller PID is not locker PID 
        !streq( host_client, lockHost ) ) // caller host is not locker host
    {
          /*-----------------------------------*/
         /*  Check if locking process exists  */
        /*-----------------------------------*/
        camp_status = checkPidOk( pIns->lockHost, pIns->lockPID, pIns->lockOs );
        if( camp_status == CAMP_JOB_EXISTS )
        {
            camp_status = CAMP_INS_LOCKED_OTHER;
        } 
        else if( camp_status == CAMP_JOB_NONEXISTANT ) 
        {
            pIns->lockPID = 0;
            camp_strdup( &pIns->lockHost, "" ); // free and strdup
	    camp_strdup( &pIns->lockOs, "" ); // free and strdup

            pVar_ins->core.status &= ~CAMP_INS_ATTR_LOCKED;
	    
	    camp_status = CAMP_INS_NOT_LOCKED;
        } 
        else
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed checkPidOk" ); }

	    camp_status = CAMP_INS_NOT_LOCKED; // 
        }
    } 
    else 
    {
          /*-----------------------*/
         /*  Requestor is locker  */
        /*-----------------------*/
        camp_status = CAMP_INS_LOCK_REQUESTOR;
    }

 return_:
    return( camp_status );
}


/*
 *  camp_checkInsLockAll
 *
 *  Check to see if any instruments are locked.  This is called before the
 *  CAMP server attempts a clean shutdown and before attempting to reload
 *  the CAMP configuration.
 */
int 
camp_checkInsLockAll( struct svc_req* rq )
{
    int camp_status = CAMP_INS_NOT_LOCKED;

    /*
     *  If rq==NULL then call is not remote and has privilege,   
     *  else don't do anything if any are locked
     */
    if( rq != NULL )
    {
	// mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;
	    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	    {
		camp_status = camp_checkInsLock( pVar, rq );
		if( _failure( camp_status ) ) break;
	    }
	}
	// mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock
    }

    return( camp_status );
}


/*
 *  checkPidOk - check that a PID exists on a host
 *  Doesn't do remote hosts for VMS
 */
static int 
checkPidOk( char* host_in, int pid, char* os )
{
#if defined( VMS )
    int vms_status;
    char host[LEN_NODENAME+1];
    char camp_server_host[LEN_NODENAME+1];

    camp_strncpy( host, sizeof( host ), host_in ); // strcpy( host, host_in );
    stolower( host );

    // mutex_lock_sys( 1 ); // warning: don't return inside mutex lock
    {
	camp_strncpy( camp_server_host, sizeof(camp_server_host), pSys->hostname );
    }
    // mutex_lock_sys( 0 ); // warning: don't return inside mutex lock

    /*
     *  Allow only local processes to lock (no remote)
     *  This check should actually be redundant here.
     */
    if( !streq( camp_server_host, host ) )
    {
        _camp_setMsg( "cannot lock instrument from remote node" );
        return( CAMP_FAILURE );
    }

    vms_status = sys_getjpiw_pid( pid );
    if( vms_status == SS$_NONEXPR ) return( CAMP_JOB_NONEXISTANT );
    else return( CAMP_JOB_EXISTS );

#else /* !VMS */

    /*
     *  Call my routine that uses rsh.
     *  Implicitly forces remote node to be trusted
     *  i.e., username of this process must have 
     *  r-service access to remote.
     */
    if( rpidok( host_in, pid, os ) ) return( CAMP_JOB_EXISTS );
    else return( CAMP_JOB_NONEXISTANT );

#endif /* VMS */

    return( CAMP_JOB_NONEXISTANT );
}


/*
 *  Delay processing by a time interval specified in a timeval_t structure
 *  (providing microsecond granularity).
 */

void
camp_sleep( timeval_t* ptv )
{
#if CAMP_MULTITHREADED

#if defined( linux )

    struct timespec ts;
    ts.tv_sec = ptv->tv_sec;
    ts.tv_nsec = 1000*ptv->tv_usec;
    nanosleep( &ts, (struct timespec*)NULL );

/* #elif defined( _POSIX_THREADS ) */
/*     struct timespec ts; */
/*     ts.tv_sec = ptv->tv_sec; */
/*     ts.tv_nsec = 1000*ptv->tv_usec; */
/*     pthread_delay_np( &ts ); /\* this "np" function is not really posix *\/ */

#elif defined( VXWORKS )

    /*
     *  Get units from sysClkRateGet(), normally 60 ticks/second 
     *  (i.e., minimum delay 1/60 seconds)
     */

    double delay = timeval_to_double( ptv ); 
    double ticks_per_second = (double)sysClkRateGet();
    double delay_ticks = ticks_per_second * delay;
    int idelay_ticks = (int)ceil( delay_ticks );

    //  have seen main thread go into taskDelay and stay there
    //  is taskDelay passed a bad argument?
    if( idelay_ticks < 0 || idelay_ticks > 600 )
    {
	_camp_log( "suspicious sleep time: delay=%f ticks_per_second=%f delay_ticks=%f idelay_ticks=%d", 
		   delay, ticks_per_second, delay_ticks, idelay_ticks );
    }

    taskDelay( idelay_ticks );

    /*   Select not working for VxWorks delay?  */
    /*    select( 1, NULL, NULL, NULL, ptv ); */

#endif /* linux */

#else  /* !CAMP_MULTITHREADED */

    /* 
     *  Use select for portable sleep 
     */
    select( 1, NULL, NULL, NULL, ptv );

#endif /* CAMP_MULTITHREADED */
}


/*
 *  Delay processing by a time specified by a floating point number.
 *  Microseconds are the smallest division.
 */
void
camp_fsleep( double delay )
{
    timeval_t tv = double_to_timeval( delay );

    camp_sleep( &tv );
}


/*
 *  pointer <-> integer conversion only used to store an integer in a user data pointer
 */


uintptr_t pointerToInteger( void* p )
{
    return (uintptr_t)p;
}


void* integerToPointer( uintptr_t i )
{
    return (void*)i;
}


/*
 *  fill ppDir with a glob of filespec_in
 */
int
camp_glob( DIRENT** ppDirEnt, const char* filespec_in )
{
    int camp_status = CAMP_SUCCESS;
    char filespec[LEN_FILENAME+1];
    char dirname[LEN_FILENAME+1];
    char dirname_specific[LEN_FILENAME+1];
    char pattern[LEN_FILENAME+1];

    if( ( filespec_in == NULL ) || ( strlen( filespec_in ) == 0 ) )
    {
        camp_status = CAMP_INVAL_FILESPEC;
	_camp_setMsg( "invalid filespec '%s'", ( (filespec_in != NULL) ? filespec_in: "" ) );
	goto return_;
    }

    {
	// DIRENT** ppDirEnt;
	DIR* pDir;
	struct dirent* pDirent;

	// _camp_log( "filespec_in:'%s'",  filespec_in );

	/*
	 *  Prepend default search directory if no directory given
	 */
	file_nodir( filespec_in, filespec );

	if( streq( filespec_in, filespec ) )
	{
	    camp_snprintf( filespec, sizeof( filespec ), "%s%s", camp_getFilemaskGeneric(CAMP_DAT_DIR), filespec_in );
	}
	else
	{
	    camp_strncpy( filespec, sizeof( filespec ), filespec_in ); // strcpy( filespec, filespec_in );
	}

	// _camp_log( "filespec:'%s'",  filespec );

	/*
	 *  Add filespec as first entry in list
	 */
	// ppDirEnt = ppDir;
	*ppDirEnt = (DIRENT*)camp_zalloc( sizeof( DIRENT ) );
	camp_strdup( &(*ppDirEnt)->filename, filespec ); // free and strdup
	ppDirEnt = &(*ppDirEnt)->pNext;

	/*
	 *  Get the directory part and open
	 */
	file_dir( filespec, dirname );

	camp_translate_generic_filename( dirname, dirname_specific, sizeof( dirname_specific ) ); // 20140325
	//camp_strncpy( dirname_specific, sizeof( dirname_specific ), dirname );

	// _camp_log( "opening dirname:'%s' dirname_specific:'%s'",  dirname, dirname_specific );

	pDir = opendir( dirname_specific );
	if( pDir == NULL ) 
	{
	    camp_status = CAMP_INVAL_FILESPEC;
	    _camp_setMsg( "invalid filespec '%s' -> '%s'", ( (filespec_in != NULL) ? filespec_in: "" ), dirname_specific );
	    goto return_;
	}

	/*
	 *  Determine the filespec pattern
	 */
	basename_tw( filespec, pattern );
	if( streq( pattern, "" ) )
	{
	    camp_strncpy( pattern, sizeof( pattern ), "*" ); // strcpy( pattern, "*" );
	}

	/* _camp_log( "filespec:'%s' pattern:'%s' sizeof(DIR):%d sizeof(struct dirent):%d %lu %lu %lu %lu %lu",  */
	/* 	       filespec, pattern */
	/* 	      , sizeof(DIR) */
	/* 	      , sizeof(struct dirent)  */
	/* 	      , sizeof(pDirent->d_ino)  */
	/* 	      , sizeof(pDirent->d_off)  */
	/* 	      , sizeof(pDirent->d_reclen)  */
	/* 	      , sizeof(pDirent->d_type)  */
	/* 	      , sizeof(pDirent->d_name)  */
	/*     ); */

	/*
	 *  Read the directory
	 *
	 *  20140326  TW  there was a problem with truncated pDirent->d_name's here,
	 *                the reason was that this file was including dirent.h from
	 *                libc_tw (which was meant for vms compability), and the
	 *                structures were defined slightly differently
	 */
    
	for( pDirent = readdir( pDir ); 
	     pDirent != NULL; 
	     pDirent = readdir( pDir ) )
	{
	    // printf( "%s: entry:'%s'\n", __FUNCTION__, pDirent->d_name );
	    // _camp_log( "entry:'%s'",  pDirent->d_name );

	    /*
	     *  Add file if it matches the pattern
	     */
	    if( strmatch( pDirent->d_name, pattern ) )
	    {
		*ppDirEnt = (DIRENT*)camp_zalloc( sizeof( DIRENT ) );
		camp_strdup( &(*ppDirEnt)->filename, pDirent->d_name ); // free and strdup
		ppDirEnt = &(*ppDirEnt)->pNext;

		// _camp_log( "entry:'%s' matches pattern:'%s'",  pDirent->d_name, pattern );
	    }
	    else
	    {
		// _camp_log( "entry:'%s' does not match pattern:'%s'",  pDirent->d_name, pattern );
	    }

#ifdef VMS
	    /*
	     *  VMS implementation allocates dynamically
	     */
	    free( pDirent );
#endif /* VMS */
	}

	/*
	 *  Close the directory
	 *  Free all resources related to the 
	 *  directory stream.
	 *  (except VMS implementation which 
	 *  allocates dir entries dynamically)
	 */
	closedir( pDir );
    }

return_:
    return( camp_status );
}
