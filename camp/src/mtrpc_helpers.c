/*
 *  mtrpc_helpers.c
 *
 *  helpers for linking against mtrpc
 */

#include <errno.h>

char* gpcPrgName = "camp_srv"; 

void __set_errno( int _errno )
{
    errno = _errno;
}
