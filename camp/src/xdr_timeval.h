
#ifdef VMS

#ifdef MULTINET
#include <rpc/types.h>
#include <rpc/xdr.h>
#include <sys/time.h>
#endif /* MULTINET */

#ifdef UCX
#include <ucx$rpcxdr.h>
#endif /* UCX */

#else /* !VMS */

#include <rpc/types.h>
#include <rpc/xdr.h>

#endif /* VMS */

#include "timeval.h"


bool_t xdr_timeval_t( XDR* xdrs, timeval_t* objp );
