/*
 *  Name:       camp_req.c
 *
 *  Purpose:    Maintain requests in the CAMP server (variable polling)
 *
 *              In the current implementation, requests are requests to
 *              perform a polling operation on one instrument variable.
 *
 *              Requests (variable polling reads) are queued and serviced
 *              when they reach an associated absolute time.  Polling
 *              requests are then requeued with an absolute time that is
 *              incremented by the polling interval for that variable.
 *
 *  Provides:
 *              REQ_addPoll - queue a variable poll request
 *              REQ_cancel  - cancel a variable poll request
 *              REQ_doPending - check all queued requests, if any have
 *                            reached their timestamp then service them.
 *              REQ_cancelAllIns - cancel all the requests for a given
 *                            instrument.  This is useful when setting an
 *                            offline or deleting it.
 *
 *  Called by:
 *              REQ_addPoll is called by varSetPoll (in camp_var_priv.c)
 *              REQ_cancel is called by varSetPoll (in camp_var_priv.c)
 *              REQ_doPending is called by srv_loop (in camp_srv_main.c)
 *              REQ_cancelAllIns is called by campsrv_insdel (in
 *                 camp_srv_proc.c)
 * 
 *  Revision history:
 *
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *
 */

#include <math.h>
#include "camp_srv.h"


REQ* 
REQ_createPoll( CAMP_VAR *pVar )
{
    REQ* pReq;

    pReq = (REQ*)camp_zalloc( sizeof( REQ ) );

    // pReq->pending = 0;
    pReq->cancel = FALSE;
    pReq->pVar = pVar;
    pReq->procs.retProc = REQ_doPoll;
    pReq->spec.type = IO_POLL;

    return( pReq );
}


/*
 *  called as a callback from poll thread
 */
int
REQ_doPoll( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    timeval_t time_current;
    // _mutex_lock_begin();

    if( pReq->cancel )
    {
	// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
	// return( REQ_remove( pReq ) ); // 20140219  TW  don't remove here, let main thread clean up in REQ_doPending
	camp_status = CAMP_SUCCESS;
	if( _camp_debug(CAMP_DEBUG_POLL) ) 
	{
	    _camp_log( "poll cancelled pReq:%x var:'%s'", pReq, pReq->pVar->core.path );
	}
	goto return_;
    }

    // _mutex_lock_varlist_on(); // for pReq->pVar // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar_ins;
	CAMP_VAR* pVar = pReq->pVar;

	pVar_ins = varGetpIns( pVar );
	if( pVar_ins == NULL )
	{
	    // pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
	    _camp_setMsgStat( CAMP_INVAL_PARENT, pVar->core.ident );
	    camp_status = CAMP_INVAL_VAR;
	    goto return_;
	}

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF;

	    if( pIF == NULL )
	    {
		// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
		camp_status = CAMP_INVAL_IF;
		goto return_;
	    }

	    if( !( pIF->status & CAMP_IF_ONLINE ) )
	    {
		// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
		camp_status = CAMP_NOT_CONN;

		if( _camp_debug(CAMP_DEBUG_POLL) ) 
		{
		    _camp_log( "not online pReq:%x var:'%s'", pReq, pReq->pVar->core.path );
		}
		goto return_;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	camp_status = varRead( pVar );

	if( camp_status != CAMP_SUCCESS )
	if( _camp_debug(CAMP_DEBUG_POLL) ) 
	{
	    _camp_log( "varRead status:%x pReq:%x var:'%s'", camp_status, pReq, pReq->pVar->core.path );
	}

	/*
	 *  Next poll will occur in pollInterval seconds
	 *  after the current time.
	 *  This means that polling isn't really 
	 *  happening every pollInterval seconds, since 
	 *  the time for the varRead is significant, but
	 *  reduces the chance of backlogs.
	 */
	gettimeval( &time_current );
 	// rem = modf( pVar->core.pollInterval, &integ ); */
 	// pReq->time.tv_sec = time_current.tv_sec + (long)integ; */
 	// pReq->time.tv_usec = time_current.tv_usec + (long)(1e6*rem); */
	pReq->time = addtimeval_double( &time_current, pVar->core.pollInterval );
    }
    // _mutex_lock_varlist_off(); // for pReq->pVar // warning: don't return inside mutex lock

    // pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter

return_:
    // _mutex_lock_end();
    return( camp_status );
}



