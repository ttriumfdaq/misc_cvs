/*
 *  Name:       camp_ins_utils.c
 *
 *  Purpose:    Provide routines for management of instrument structures
 *              and access to instrument specific parameters.
 *              Also provide FORTRAN entry points to the routines that access
 *              instrument information.
 *
 *              Note that all routines except camp_insDel are not used by
 *              the CAMP server, CUI or CMD, but are provided in the library
 *              for client applications.
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp_clnt.h"
#endif


/*
 *  Name:       camp_insDel
 *
 *  Purpose:    Delete an instrument from the variable list.
 *
 *  Called by:  campsrv_insdel (camp_srv_proc.c)
 * 
 *  Inputs:     CAMP path of the instrument
 *
 *  Outputs:    CAMP status
 *
 */
int
camp_insDel( const char* path )
{
    CAMP_VAR** ppVar;
    CAMP_VAR* pVar;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	/*
	 *  Get address of pointer to instrument data
	 */
	ppVar = camp_varGetpp( path /* , mutex_lock_varlist_check() */ );
	if( ppVar == NULL ) 
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    pVar = *ppVar;
	    *ppVar = (*ppVar)->pNext;
	    pVar->pNext = NULL;
	    set_current_xdr_flag( CAMP_XDR_ALL );
	    _xdr_free( xdr_CAMP_VAR, pVar );

	    camp_status = CAMP_SUCCESS;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  NOT USED
 */
int
camp_insGetLock( const char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );
	
	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    *pFlag = ( pVar->core.status & CAMP_INS_ATTR_LOCKED ) ? TRUE : FALSE;
	    camp_status = CAMP_SUCCESS;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  NOT USED
 */
int
camp_insGetLine( const char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    // mutex_lock_sys( 1 ); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL ) 
		{
		    *pFlag = FALSE;
		}
		else 
		{
		    *pFlag = ( pIF->status & CAMP_IF_ONLINE ) ? TRUE : FALSE;
		}
	    }
	    // mutex_lock_sys( 0 ); // warning: don't return inside mutex lock

	    camp_status = CAMP_SUCCESS;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  NOT USED
 */
int
camp_insGetFile( const char* path, char* filename, size_t filename_size )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    camp_strncpy( filename, filename_size, pIns->iniFile ); // strcpy( filename, pIns->iniFile );

	    camp_status = CAMP_SUCCESS;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  NOT USED
 */
int
camp_insGetTypeIdent( const char* path, char* typeIdent, size_t typeIdent_size )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    camp_strncpy( typeIdent, typeIdent_size, pIns->typeIdent ); // strcpy( typeIdent, pIns->typeIdent );

	    camp_status = CAMP_SUCCESS;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  NOT USED
 */
int
camp_insGetIfTypeIdent( const char* path, char* typeIdent, size_t typeIdent_size )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_INS;
	    _camp_setMsgStat( CAMP_INVAL_INS, path );
	}
	else
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
	    
	    // mutex_lock_sys( 1 ); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL ) 
		{
		    camp_status = CAMP_INVAL_IF;
		    _camp_setMsgStat( CAMP_INVAL_IF, path );
		}
		else 
		{
		    camp_strncpy( typeIdent, typeIdent_size, pIF->typeIdent ); // strcpy( typeIdent, pIF->typeIdent );

		    camp_status = CAMP_SUCCESS;
		}
	    }
	    // mutex_lock_sys( 0 ); // warning: don't return inside mutex lock
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}
