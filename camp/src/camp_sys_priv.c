/*
 *  Name:       camp_sys_priv.c
 *
 *  Purpose:    Utilities to manage the CAMP system database
 *              (the data not related to particular instruments).
 *
 *              These routines are used only by the CAMP server.
 *
 *  Called by:  CAMP server routines only.
 * 
 *  Revision history:
 *  2003-04-17  DA   basename -> basename_tw
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef linux
#include <unistd.h> // gethostname
#endif /* linux */
// #include "timeval.h" // include by camp.h
#include "camp_srv.h"


/*
 *  Free the entire CAMP system database using XDR routines
 */
void
sys_free( void )
{
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	_xdr_free( xdr_CAMP_SYS, pSys );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
}


/*
 *  Initialize the system database 
 *
 *  Called only once at system startup by srv_init
 */
int 
sys_init( void )
{
    int camp_status = CAMP_SUCCESS;
    char str[64];
    char hostname[LEN_NODENAME+1];
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	_xdr_free( xdr_CAMP_SYS, pSys );
	pSys = (CAMP_SYS*)camp_zalloc( sizeof( CAMP_SYS ) );

	gethostname( hostname, LEN_NODENAME );
	stolower( hostname );  /* This is for string comparisons */
	camp_strdup( &pSys->hostname, hostname ); // free and strdup

	camp_snprintf( str, sizeof( str ), "CAMP SRV %s", CAMP_SRV_VERSION_STRING );
	camp_strdup( &pSys->prgName, str ); // free and strdup
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    camp_status = sys_initDyna();
    if( _failure( camp_status ) ) 
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed sys_initDyna" ); }
        goto return_;
    }

    /*
     *  An error returned from sys_update is non-fatal.
     *  This allows CAMP to start up even when there is
     *  an error from the initialization file.
     */
    sys_update();

    camp_status = CAMP_SUCCESS;

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  sys_update - load camp.ini
 *
 *  Update the dynamic parts of the system database using the CAMP
 *  initialization file (camp.ini).  The file is written in the Tcl
 *  language and interpreted by the main thread Tcl interpreter.
 *
 *  The main thread Tcl interpreter must have been created (by camp_tclInit)
 *  before calling this routine.
 */
int
sys_update( void )
{
    int camp_status = CAMP_SUCCESS;
    _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	int tcl_status = TCL_OK;

	_xdr_free( xdr_ALARM_ACT, pSys->pAlarmActs );
	_xdr_free( xdr_CAMP_IF_t, pSys->pIFTypes );
	_xdr_free( xdr_INS_AVAIL, pSys->pInsAvail );
	_xdr_free( xdr_INS_TYPE, pSys->pInsTypes );
	_xdr_free( xdr_LOG_ACT, pSys->pLogActs );

	_mutex_lock_mainInterp_on(); // 20140219  TW
	{
	    Tcl_Interp* interp = camp_tclInterp();
	    char* tcl_message = (char*)malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	    tcl_status = Tcl_EvalFile( interp, camp_getFilemaskSpecific(CAMP_SRV_INI) );

	    assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

	    camp_setMsg( "%s", tcl_message );

	    if( tcl_status == TCL_ERROR )
	    {
		_camp_appendMsg( "error interpreting file '%s'", camp_getFilemaskSpecific(CAMP_SRV_INI) );
	    }

	    free( tcl_message );
	}
	_mutex_lock_mainInterp_off(); // 20140219  TW

	gettimeval( &pSys->pDyna->timeLastSysChange );

	if( tcl_status == TCL_ERROR )
	{
	    camp_status = CAMP_FAILURE;
	    goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    camp_status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( camp_status );
}


/*
 *  sys_initDyna
 *
 *  Allocate memory for the dynamic part of the system database
 */
int
sys_initDyna( void )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	_xdr_free( xdr_SYS_DYNAMIC, pSys->pDyna );
	pSys->pDyna = (SYS_DYNAMIC*)camp_zalloc( sizeof( SYS_DYNAMIC ) );
	
	camp_strdup( &pSys->pDyna->cfgFile, "" ); // free and strdup
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    camp_status = CAMP_SUCCESS;

//return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  sys_getTypeInstance
 *
 *  Get the number of instances of a particular instrument type.
 *
 *  This is called by campsrv_insadd when adding an instrument and saved
 *  in the instrument's data structure, but I don't think the value has any
 *  practical meaning anymore.
 */
u_long
sys_getTypeInstance( char* typeIdent )
{
    u_long typeInstance;
    // _mutex_lock_begin();

    typeInstance = 1;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    if( streq( typeIdent, pIns->typeIdent ) )
	    {
		typeInstance = _max( typeInstance, pIns->typeInstance + 1 );
	    }
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
    return( typeInstance );
}


