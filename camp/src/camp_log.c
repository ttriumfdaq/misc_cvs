/*
 *  Name:      camp_log.c
 *
 *  Purpose:   Implement a minimal logging facility whereby the CAMP
 *             server can log informational messages to a file.
 *
 *             This facility has no relation to camp_msg_utils.c
 *
 *  Provides:
 *             camp_startLog   - open the log file, optionally specifying
 *                               whether the file should be kept closed
 *                               between log entries
 *             camp_stopLog    - close the log file
 *             camp_log        - log a string message in the CAMP server
 *
 *  Called by:
 *             "main" (or campSrv for VxWorks) calls camp_startLog
 *             srv_rundown calls camp_stopLog
 *             camp_log is called in many parts of the CAMP server
  * 
 *  Preconditions:
 *             camp_startLog must be called before logging any messages
 *             with camp_log 
 *
 *  Postconditions:
 *             camp_stopLog should be called when finished with the log file.
 *
 *  Revision history:
 *  
 *    20140424  TW  Renamed from camp_msg_priv.c to camp_log.
 */
/*
 *  camp_log.c,  Log file routines
 */

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */

static char log_prog_name[LEN_TITLE+1];
static char log_prefix_once[LEN_TITLE+1];
static char log_filename[LEN_FILENAME+1];
static FILE* log_FILE;
static bool_t log_keep_closed;
/*
 *  20140321  TW  stop threads clashing over server log file
 */
#if CAMP_MULTITHREADED
static pthread_mutex_t log_mutex; // = PTHREAD_MUTEX_INITIALIZER;
#endif // CAMP_MULTITHREADED

static bool_t openLog ( void );
static bool_t reopenLog ( void );
static bool_t closeLog ( void );


int
camp_startLog( const char* prog_name, const char* filename, bool_t keepClosedFlag )
{
    int camp_status = CAMP_SUCCESS;

#if CAMP_MULTITHREADED
    if( pthread_mutex_init( &log_mutex, pthread_mutexattr_default ) != 0 ) // not recursive
    {
        fprintf( stderr, "error initializing log_mutex\n" );
        exit( CAMP_FAILURE );
    }
    //printf("Created log mutex at %p\n",log_mutex );

    pthread_mutex_lock( &log_mutex );
#endif // CAMP_MULTITHREADED

    log_FILE = NULL;

    if( prog_name != NULL )
    {
	camp_strncpy( log_prog_name, sizeof( log_prog_name ), prog_name ); 
    }
    else
    {
	log_prog_name[0] = '\0';
    }

    log_prefix_once[0] = '\0';

    if( filename == NULL ) { camp_status = CAMP_FAILURE; goto return_; }

    camp_translate_generic_filename( filename, log_filename, sizeof( log_filename ) ); 

    log_keep_closed = keepClosedFlag;
    if( !openLog() ) { camp_status = CAMP_FAILURE; goto return_; }
    if( log_keep_closed && !closeLog() ) { camp_status = CAMP_FAILURE; goto return_; }

return_:
#if CAMP_MULTITHREADED
    pthread_mutex_unlock( &log_mutex );
#endif // CAMP_MULTITHREADED
    return( camp_status );
}


int
camp_stopLog( void )
{
    int camp_status = CAMP_SUCCESS;

#if CAMP_MULTITHREADED
    pthread_mutex_lock( &log_mutex );
#endif // CAMP_MULTITHREADED

    if( !log_keep_closed && !closeLog() ) { camp_status = CAMP_FAILURE; goto return_; }

    log_FILE = NULL;

return_:
#if CAMP_MULTITHREADED
    pthread_mutex_unlock( &log_mutex );
    pthread_mutex_destroy( &log_mutex );
#endif // CAMP_MULTITHREADED
    return( camp_status );
}


static bool_t
openLog( void )
{
    log_FILE = fopen( log_filename, "a" );
    if( log_FILE == NULL )
    {
        camp_strncpy( log_filename, sizeof( log_filename ), "" ); 
        log_keep_closed = FALSE;
        return( FALSE );
    }
    return( TRUE );
}


static bool_t
reopenLog( void )
{
    log_FILE = fopen( log_filename, "a" );
    if( log_FILE == NULL )
    {
        camp_strncpy( log_filename, sizeof( log_filename ), "" ); 
        log_keep_closed = FALSE;
        return( FALSE );
    }
    return( TRUE );
}


static bool_t
closeLog( void )
{
    if( log_FILE != NULL )
    {
        if( fclose( log_FILE ) == -1 ) return( FALSE );
        log_FILE = NULL;
    }

    return( TRUE );
}


static void camp_vlog( const char* prefix, const char* format, va_list args );


void
camp_log( const char* format, ... )
{
    va_list args;

    va_start( args, format );
    camp_vlog( "", format, args );
    va_end( args );
}


void
camp_logPrefix( const char* prefix, const char* format, ... )
{
    va_list args;

    va_start( args, format );
    camp_vlog( prefix, format, args );
    va_end( args );
}


void camp_setLogPrefixOnce( const char* prefix )
{
    camp_strncpy( log_prefix_once, sizeof( log_prefix_once ), prefix );
}


void
camp_vlog( const char* prefix, const char* format, va_list args )
{
    char* msg = NULL;
    bool_t log_reopened = FALSE;

#if CAMP_MULTITHREADED
    pthread_mutex_lock( &log_mutex );
#endif // CAMP_MULTITHREADED

    if( log_keep_closed ) 
    {
	log_reopened = reopenLog();

	if( !log_reopened )
	{
	    goto return_;
	}
    }

    if( log_FILE == NULL )
    {
        goto return_;
    }

    msg = (char*)camp_malloc( MAX_LEN_MSG+1 ); // on heap for stack-limited vxworks
    camp_vsnprintf( msg, MAX_LEN_MSG+1, format, args ); 

    //  don't log a blank message

    if( msg[0] == '\0' ) 
    {
	goto return_;
    }

    //  timestamp
    {
	time_t t;
	struct tm tmb;
	char buf[64];

	time( &t );
	localtime_r( &t, &tmb );
	strftime( buf, sizeof(buf), "[%c] ", &tmb );
	fputs( buf, log_FILE );
    }

    //  optional program name prefix
    if( log_prog_name[0] != '\0' )
    {
	fputs( log_prog_name, log_FILE ); fputs( ": ", log_FILE );
    }

    //  optional function name prefix
    if( log_prefix_once[0] != '\0' )
    {
	fputs( log_prefix_once, log_FILE ); fputs( ": ", log_FILE );
    }

    //  optional function name prefix
    if( prefix[0] != '\0' )
    {
	fputs( prefix, log_FILE ); fputs( ": ", log_FILE );
    }

    //  msg
    {
	fputs( msg, log_FILE );
    }

    fputs( "\n", log_FILE );
    
    // fflush( log_FILE );

return_:
    if( log_keep_closed && log_reopened ) closeLog();
    log_prefix_once[0] = '\0'; // hack!
    _free( msg );
#if CAMP_MULTITHREADED
    pthread_mutex_unlock( &log_mutex );
#endif // CAMP_MULTITHREADED
}
