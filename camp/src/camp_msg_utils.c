/*
 *  Name:       camp_msg_utils.c
 *
 *  Purpose:    Routines to maintain a global message string
 *
 *              The global message string is used to concatenate messages
 *              regarding the state of a call to the CAMP server.
 *              The global message is also used more generally by the
 *              CAMP Tcl interpreter to contain the result of certain
 *              operations (e.g., ASCII readings from instruments).
 *
 *              For a multithreaded server global messages are implemented
 *              as a separate message in each thread's private structure
 *              (see get_thread_msg and set_thread_msg in camp_srv_svc_mod.c).
 *              For non-threaded there is the single global character array
 *              current_message defined below. In both cases the storage is
 *              a fixed size of MAX_LEN_MSG+1.
 *
 *              Any service request initialized the message to "". Error messages
 *              should be appended using camp_appendMsg. Successful results should
 *              use camp_setMsg.
 *
 *              WARNING: The first argument of camp_setMsg and camp_appendMsg
 *              is a printf format specifier, NOT an arbitray string.  DO NOT
 *              use like  camp_setMsg(some_var) which will fail when the variable
 *              string contains a "%"; use  camp_setMsg("%s",some_var) instead.
 *
 *  To do:      Make the storage have variable size allowing very large messages
 *              without wasting a lot of memory on small messages.
 *              Make the message handling binary-safe.  This could be done by
 *              switching all string handling to counted-length strings, or by 
 *              coding zero-bytes differently.  The latter should be done by
 *              using modern Tcl's internal string coding.
 *
 *  Revision history:
 *    20-Dec-1999  TW  Want non-printable characters for binary dumps
 *                     from some instruments
 *                     WARNING:  the above revision could have unforeseen
 *                     implications for some instruments.  The global message
 *                     string is used to contain the results of ASCII readings
 *                     from instruments as noted above.  Some instruments
 *                     are known to return non-printable characters as part
 *                     of a normal string response.  These characters could
 *                     interfere with string interpretation in the Tcl
 *                     interpreter.  The stoprint routine was used to remove
 *                     the chance of such misinterpretation.
 *    14-Dec-2000  TW  Replace vsprintf with homemade format interpretation.
 *                     Less versatile, and a little dangerous if %f
 *                     doesn't correspond to a 'double' type (etc.), but
 *                     allows us to check whether there is actually enough
 *                     room in the string for the printed arguments.
 *                     This was causing memory access violations for very large
 *                     messages.
 *    15-Oct-2012  DA  Use vsnprintf() instead. (Much simpler.)
 *    20140218     TW  bug fix: len_whole_msg/len_remaining were in/decremented
 *                     by 3 even when less than 3 separator characters were written
 *                     to whole_msg
 *                     Found a portable vsnprintf implementation (for VxWorks)
 *                     from http://www.ijs.si/software/snprintf
 *    20140220     TW  bug in camp_setMsg after Donald's change to vsnprintf
 *    20140415     TW  portable snprintf removed - it doesn't do real numbers
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */

#ifdef VMS
#include <starlet.h>
#include <lib$routines.h>
#endif /* VMS */

#if !defined( RPC_SERVER ) || !CAMP_MULTITHREADED
static char current_msg[MAX_LEN_MSG+1] = "";
#define get_thread_msg() current_msg
#define set_thread_msg(msg) camp_strncpy( current_msg, MAX_LEN_MSG+1, msg ); // strncpy( current_msg, msg, MAX_LEN_MSG )
#endif /* !RPC_SERVER || !CAMP_MULTITHREADED */


/*
 * Group some frequently-used literal messages in a function to reduce 
 * memory footprint.  These are usually referred to by macros defined 
 * in camp.h
 */
char*
msg_getHelpString( MSG_TYPE type )
{
    switch( type )
    {
	/*
	 *  warning
	 */
    case CAMP_CANT_SET_IF:
	return( "can't set instrument '%s' interface while online" );
    case CAMP_NOT_CONN:
	return( "instrument '%s' not online" );
    case CAMP_INS_LOCKED:
	return( "instrument '%s' is locked" );
    case CAMP_INS_LOCKED_OTHER:
	return( "instrument '%s' is locked by another process" );
    //case CAMP_INVAL_LOCKER:
    //  return( "invalid locker '%s'" );
    case CAMP_RS232_PORT_IN_USE:
	return( "RS232 port '%s' in use" );

	/*
	 *  error
	 */
    case CAMP_INVAL_INS:
        return( "invalid instrument '%s'" );
    case CAMP_INVAL_INS_TYPE:
        return( "instrument %s has invalid instrument type '%s'" );
    case CAMP_INVAL_LINK:
        return( "invalid link '%s'" );
    case CAMP_INVAL_VAR:
        return( "variable '%s' doesn't exist" );
    case CAMP_INVAL_VAR_TYPE:
        return( "invalid variable type '%s'" );
    case CAMP_INVAL_VAR_ATTR:
        return( "attribute '%s' invalid for var '%s'" );
    case CAMP_INVAL_FILE:
        return( "invalid file '%s'" );
    case CAMP_INVAL_FILESPEC:
        return( "invalid filespec '%s'" );
    case CAMP_INVAL_IF:
        return( "instrument '%s' interface undefined" );
    case CAMP_INVAL_IF_TYPE:
        return( "invalid interface type '%s'" );
    case CAMP_INVAL_SELECTION:
        return( "invalid selection for var %s" );
    case CAMP_INVAL_PARENT:
        return( "couldn't find instrument for var '%s'" );
    case CAMP_FAIL_DUMP:
        return( "failed dump instrument to file (%s)" );
    case CAMP_FAIL_UNDUMP:
        return( "failed dump file to instrument (%s)" );
    default:
	break;
    }
    return( "" );
}


/*
 *  Return a pointer to the current message string
 */
char*
camp_getMsg( void )
{
    return( get_thread_msg() );
}


/*
 *  Test whether there is a non-empty message string
 */
bool_t
camp_isMsg( void )
{
    char* msg = camp_getMsg();

    if( ( msg != NULL ) && ( strlen( msg ) > 0 ) ) return( TRUE );
    else return( FALSE );
}



/*
 *  Reset the global message string to a new value
 *
 *  The parameters are variable in number - the same as those taken
 *  by the vsprintf C library routine.  See the documentation on vsprintf.
 */
void 
camp_setMsg( const char* fmt, ... )
{
    va_list args;
    char* msg = (char*)camp_malloc( MAX_LEN_MSG+1 ); // on heap for stack-limited vxworks

    va_start( args, fmt );
    camp_vsnprintf( msg, MAX_LEN_MSG+1, fmt, args ); // terminates, size includes terminator
    va_end( args );

    set_thread_msg( msg );

    free( msg );
}


void 
camp_setMsgStat( int status, ... )
{
    va_list args;
    char* msg = (char*)camp_malloc( MAX_LEN_MSG+1 ); // on heap for stack-limited vxworks

    va_start( args, status );
    camp_vsnprintf( msg, MAX_LEN_MSG+1, msg_getHelpString( status ), args ); // terminates, size includes terminator
    va_end( args );

    set_thread_msg( msg );

    free( msg );
}

static void camp_vappendMsg( const char* format, va_list args, bool_t do_sep );


/*
 *  Append a string to the global message string
 *
 *  The parameters are variable in number - the same as those taken
 *  by the vsprintf C library routine.  See the documentation on vsprintf.
 *
 *  20140218  TW  bug fix: len_whole_msg/len_remaining were in/decremented
 *                by 3 even when less than 3 separator characters were written
 *                to whole_msg
 */
void
camp_appendMsg( const char* format, ... )
{
    va_list args;

    va_start( args, format );
    camp_vappendMsg( format, args, TRUE );
    va_end( args );
}


void
camp_appendMsgPrefix( const char* format, ... )
{
    va_list args;

    va_start( args, format );
    camp_vappendMsg( format, args, TRUE );
    va_end( args );

    camp_appendMsgNoSep( ":" );
}


void
camp_appendMsgNoSep( const char* format, ... )
{
    va_list args;

    va_start( args, format );
    camp_vappendMsg( format, args, FALSE );
    va_end( args );
}


void 
camp_appendMsgStat( int status, ... )
{
    va_list args;

    va_start( args, status );
    camp_vappendMsg( msg_getHelpString( status ), args, TRUE );
    va_end( args );
}


void
camp_vappendMsg( const char* format, va_list args, bool_t do_sep ) 
{
    char* msg;
    int msg_len;
    int msg_len_max;
    int msg_size = MAX_LEN_MSG+1;

    msg_len_max = msg_size-1;
    msg = camp_getMsg();
    // msg[msg_len_max] = '\0'; // unnecessary and unwanted
    msg_len = strlen( msg );

    /*
     *  append separator
     */
    if( do_sep && (msg_len < msg_len_max) && (msg_len > 0) ) 
    {
	int msg_len_remaining = msg_len_max - msg_len;
	bool_t appending_to_prefix = ( msg[msg_len-1] == ':' );
	char* separator = ( appending_to_prefix ) ? " " : " | ";
	int num_characters_added = _min( msg_len_remaining, strlen( separator ) );

	camp_strncat( msg, msg_size, separator ); 

	msg_len += num_characters_added;
    }
    
    /*
     *  append new message 
     */
    if( msg_len < msg_len_max )
    {
	camp_vsnprintf( msg + msg_len, msg_size - msg_len, format, args ); 
    }
}
