/*
 *  Name:       camp_path_utils.c
 *
 *  Purpose:    Routines to maintain CAMP variable paths
 *
 *              All CAMP variables (including instruments) are identified by
 *              a path that looks much like a Unix filename.  The first
 *              level of the path (e.g., /lake330_1) is unique to the
 *              instance of each instrument.  All the variables below this
 *              level (e.g., /lake330_1/reading) are common to the instrument
 *              type and are defined in the Tcl instrument driver.
 *
 *              There is one special path identifier.  When the instrument
 *              is referred to as "/~" this indicates that this should be
 *              expanded to the unique identifier of the instrument
 *              (e.g., /lake330_1) that is currently in context.  Normally
 *              this is only used in the Tcl instrument driver.  Since
 *              the instrument driver is general to many instruments of one
 *              type, the unique identifier must not be hardcoded in the
 *              Tcl driver.
 *
 *  Revision history:
 *    20140221  TW  pass sizes to string buffers
 *
 */

#include <stdio.h>
#include <string.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */


#define _camp_pathAtTop( p )  ( ( p[1] == '\0' ) ? TRUE : FALSE )


/*
 *  Test whether a path is at the root level (i.e., "/")
 */
bool_t 
camp_pathAtTop( const char* path )
{
    return( _camp_pathAtTop( path ) );
}


/*
 *  Initialize a path to "/"
 */
void 
camp_pathInit( char* path, size_t path_size )
{
    if( path_size >= 2 )
    {
	path[0] = '/';
	path[1] = '\0';
    }
}


/*
 *  Copy a path, expanding a tilde identifier "/~" with the unique
 *  identifier of the current instrument.
 *
 *  The ident of the current
 *  instrument is set (using set_current_ident) whenever the context
 *  of a high level operation in the CAMP server becomes associated
 *  with an instrument (including insadd, insload, insInit, insDelete,
 *  insOnline, insOffline, varRead and varSetReq).
 */
char*
camp_pathExpand( const char* path, char* expansion, size_t expansion_size )
{
    char* p;

    if( _camp_pathAtTop( path ) || ( path[1] != '~' ) )
    {
        camp_strncpy( expansion, expansion_size, path );
    } 
    else
    {
        camp_pathInit( expansion, expansion_size );
        camp_pathDown( expansion, expansion_size, get_current_ident() );
        p = strchr( &path[1], '/' );
        if( p != NULL )
        {
            camp_strncat( expansion, expansion_size, p );
        }
    }
    return( expansion );
}


/*
 *  Compare two paths, with expansion
 */
bool_t 
camp_pathCompare( const char* path1, const char* path2 )
{
    char temp_path1[LEN_PATH+1];
    char temp_path2[LEN_PATH+1];

    camp_pathExpand( path1, temp_path1, LEN_PATH+1 );
    camp_pathExpand( path2, temp_path2, LEN_PATH+1 );

    if( streq( temp_path1, temp_path2 ) ) return( TRUE );

    return( FALSE );
}


/*
 *  Get the first identifier in the path
 */
char*
camp_pathGetFirst( const char* path, char* ident, size_t ident_size )
{
    int ident_len;

    if( path == NULL ) 
    {
	return( NULL );
    }
    else if( path[0] == '\0' ) 
    {
	if( ident_size > 0 ) ident[0] = '\0';
    }
    else
    {
        /*
         *  Get the ident after the first delimeter
         */
        ident_len = strcspn( &path[1], "/" );
        camp_strncpy_num( ident, ident_size, &path[1], ident_len ); // strncpy( ident, &path[1], ident_len );
    }

    return( ident );
}


/*
 *  Get the last identifier in the path
 */
char*
camp_pathGetLast( const char* path, char* ident, size_t ident_size )
{
    if( path == NULL ) return( NULL );
    if( ident == NULL ) return( NULL );

    /*
     *  Get the ident after the last delimeter
     */
    camp_strncpy( ident, ident_size, strrchr( path, '/' ) + 1 );

    return( ident );
}


/*
 *  Get the next identifier in the path based on a current path string
 */
char*
camp_pathGetNext( const char* path, const char* path_curr, char* ident_next, size_t ident_next_size )
{
    if( path == NULL ) return( NULL );
    if( path_curr == NULL ) return( NULL );
    if( strlen( path_curr ) > strlen( path ) ) return( NULL );

    /*
     *  Get the next ident from the path
     */
    if( _camp_pathAtTop( path_curr ) ) 
    {
        return( camp_pathGetFirst( path, ident_next, ident_next_size ) );
    }
    else 
    {
        return( camp_pathGetFirst( &path[strlen(path_curr)], ident_next, ident_next_size ) );
    }

}


/*
 *  Move down the path based on a current path string.  The result is
 *  return in path_curr
 */
char*
camp_pathDownNext( const char* path, char* path_curr, size_t path_curr_size )
{
    char ident_next[LEN_IDENT+1];

    camp_pathGetNext( path, path_curr, ident_next, LEN_IDENT+1 );
    camp_pathDown( path_curr, path_curr_size, ident_next );

    return( path_curr );
}


/*
 *  Append an identifier to a path string
 */
char*
camp_pathDown( char* path, size_t path_size, const char* ident )
{
    /*
     *  Append ident to the path
     */
    if( !_camp_pathAtTop( path ) ) 
    {
	camp_strncat( path, path_size, "/" );
    }
    camp_strncat( path, path_size, ident );

    return( path );
}


/*
 *  Move up a level in the path.  The result is returned in "path".
 */
char*
camp_pathUp( char* path )
{
    char* pos;

    /*
     *  Terminate the string at the last occurence of the delimeter
     */
    pos = strrchr( path, '/' );
    if( pos == NULL ) // 20140221  TW 
    {
	// invalid path, no '/'
    }
    else if( pos == path ) 
    {
        *(++pos) = '\0'; // root '/'
    }
    else 
    {
        *pos = '\0'; // lower than root
    }

    return( path );
}


