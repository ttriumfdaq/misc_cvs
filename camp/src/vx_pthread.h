#ifndef _VX_PTHREAD_H_
#define _VX_PTHREAD_H_
/* 
 *  vx_pthread.h - a minimal implementation of pthreads in VxWorks
 */

#include <vxWorks.h>
#include <taskLib.h>
#include <taskVarLib.h>
#include <semLib.h>

typedef char* (*VX_ROUTINE)();
typedef void (*VX_VOIDROUTINE)();

#define pthread_t                   int
#define pthread_attr_t              TASK_DESC
#define pthread_startroutine_t      VX_ROUTINE
#define pthread_addr_t              void* // char* // 20140320  TW
#define pthread_key_t               char*
#define pthread_mutex_t             SEM_ID
#define pthread_cond_t              SEM_ID
#define pthread_mutexattr_t         int
#define pthread_condattr_t          int
#define pthread_mutexattr_default   SEM_Q_FIFO /* not used */
#define pthread_condattr_default    SEM_Q_FIFO /* not used */
#define pthread_once_t              int
#define PTHREAD_ONCE_INIT           0
#define pthread_destructor_t        VX_VOIDROUTINE
#define pthread_initroutine_t       VX_VOIDROUTINE

/*
 *  In VxWorks, scheduling is by default FIFO
 *  To set round-robin, use kernelTimeSlice() 
 *  which affects all tasks.
 */
#define SCHED_FIFO                  0  /* not used */
#define SCHED_RR                    0  /* not used */
#define SCHED_FG_NP                 0  /* not used */
#define SCHED_OTHER                 0  /* not used */
#define SCHED_BG_NP                 0  /* not used */

#define PRI_FIFO_MIN                255
#define PRI_FIFO_MAX                0
#define PRI_RR_MIN                  255
#define PRI_RR_MAX                  0
#define PRI_FG_MIN_NP               255
#define PRI_FG_MAX_NP               0
#define PRI_BG_MIN_NP               255
#define PRI_BG_MAX_NP               0
#define PRI_OTHER_MIN               255
#define PRI_OTHER_MAX               0

#define PTHREAD_CANCEL_ENABLE       1
#define PTHREAD_CANCEL_DISABLE      0

#define PTHREAD_INHERIT_SCHED       0

#define PTHREAD_CREATE_DETACHED     0

#define MUTEX_RECURSIVE             0  /* not used */
#define PTHREAD_MUTEX_RECURSIVE     0  /* not used */

#define pthread_setspecific( k, v ) vx_pthread_setspecific( &k, v )

struct sched_param { int sched_priority; } ;

extern pthread_attr_t pthread_attr_default;

int pthread_lock_global_np( void );
int pthread_unlock_global_np( void );
int pthread_once( pthread_once_t* pOnce, void (* init)(void) );
pthread_t pthread_self( void );
int pthread_attr_init( pthread_attr_t* pAttr );
int pthread_attr_destroy( pthread_attr_t* pAttr );
int pthread_attr_setinheritsched( pthread_attr_t* pAttr, int inherit );
int pthread_attr_setdetachstate( pthread_attr_t* pAttr, int detachstate );
int pthread_setschedparam( pthread_t thread, int scheduler, const struct sched_param *param );
//int pthread_create( pthread_t* pThread, pthread_attr_t attr, void* (*start_routine)(void*), void* arg );
int pthread_create( pthread_t* pThread, pthread_attr_t* pAttr, void* (*start_routine)(void*), void* arg );
int pthread_detach( pthread_t thread );
int pthread_cancel( pthread_t thread );
int pthread_setcancelstate( int state, int* oldstate );
void pthread_testcancel( void );
void pthread_exit( void* value );
int pthread_join( pthread_t thread, void** ppValue );
int pthread_key_create( pthread_key_t* pKey, void (*destructor)(void*) );
int pthread_key_delete( pthread_key_t key );
int vx_pthread_setspecific( pthread_key_t* pKey, void* value );
void* pthread_getspecific( pthread_key_t key );
int pthread_mutexattr_init( pthread_mutexattr_t* pAttr );
int pthread_mutexattr_destroy( pthread_mutexattr_t* pAttr );
int pthread_mutexattr_settype( pthread_mutexattr_t* pAttr, int type );
int pthread_mutexattr_gettype( pthread_mutexattr_t* pAttr, int* type );
int pthread_mutex_init( pthread_mutex_t* pMutex, pthread_mutexattr_t* pAttr );
int pthread_mutex_destroy( pthread_mutex_t* pMutex );
int pthread_mutex_lock( pthread_mutex_t* pMutex );
int pthread_mutex_trylock( pthread_mutex_t* pMutex );
int pthread_mutex_unlock( pthread_mutex_t* pMutex );
int pthread_condattr_init( pthread_condattr_t* pAttr );
int pthread_condattr_destroy( pthread_condattr_t* pAttr );
int pthread_cond_init( pthread_cond_t* pCond, pthread_condattr_t* pAttr );
int pthread_cond_destroy( pthread_cond_t* pCond );
int pthread_cond_signal( pthread_cond_t* pCond );
int pthread_cond_wait( pthread_cond_t* pCond, pthread_mutex_t* pMutex );

#endif /* _VX_PTHREAD_H_ */
