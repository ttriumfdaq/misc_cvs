/*
 *  Name:       camp_if_gpib_template.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *
 *              THIS FILE IS A TEMPLATE for interface implementation.
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *              and optionally:
 *                int if_gpib_clear ( int gpib_address );
 *                int if_gpib_timeout ( float timeoutSeconds );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *  Notes:
 *
 *      pIF_t->priv == 1 (gpib available)
 *                  == 0 (gpib unavailable)
 *
 *  Revision history:
 *
 *    20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *
 */


#include <stdio.h>
#include "camp_srv.h"

#define if_gpib_ok( pIF_t )  ( pointerToInteger( pIF_t->priv ) != 0 )


int
if_gpib_init( void )
{
    int camp_status = CAMP_SUCCESS;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	/*
	 *  Check configuration data here
	 */

	// remove when implemented
	{
	    camp_status = CAMP_INVAL_IF_TYPE;
	    _camp_setMsg( "gpib not implemented on this server" );
	    goto return_;
	}

	pIF_t->priv = integerToPointer( 1 );
    }

return_:
    return( camp_status );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    int addr;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	addr = camp_getIfGpibAddr( pIF->defn );

	if( ( addr < 0 ) || ( addr > 31 ) )
	{
	    _camp_setMsg( "invalid gpib address" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	/*
	 *  Turn online here
	 */
    }

return_:
    return( camp_status );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	/*
	 *  Turn offline here
	 */
    }

return_:
    return( camp_status );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;
	CAMP_IF_t* pIF_t;

	pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

    {
	int globalMutexLockCount = mutex_unlock_global_all();

	/*
	 *  Write here
	 */

	mutex_lock_global_all( globalMutexLockCount );
    }

    {
	int globalMutexLockCount = mutex_unlock_global_all();

	/*
	 *  Read here
	 */

	mutex_lock_global_all( globalMutexLockCount );
    }

    /*
     *  Number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = 0;

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    char* command;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;
	CAMP_IF_t* pIF_t;

	pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

    {
	int globalMutexLockCount = mutex_unlock_global_all();

	/*
	 *  Write here
	 */

	mutex_lock_global_all( globalMutexLockCount );
    }

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 *
 *  21-Dec-1999  TW  Implemented for VxWorks MVIP300 only.  Found
 *                   to increase reliability of LakeShore GPIB
 *                   devices.
 */
int 
if_gpib_clear( int gpib_addr )
{
    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 *  20140403     TW  changed argument from int to float
 */
int 
if_gpib_timeout( float timeoutSeconds )
{
    return( CAMP_SUCCESS );
}
