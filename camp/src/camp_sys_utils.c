/*
 *  Name:       camp_sys_utils.c 
 *
 *  Purpose:    Utilities to manage the CAMP system database
 *              (the data not related to particular instruments).
 *
 *              These routines are used by CAMP server and client programs.
 *
 *  Called by:  CAMP server and client routines
 * 
 *  Revision history:
 *
 */

#include <stdio.h>
#include <string.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */


/*
 *  camp_sysGetpInsType
 *
 *  Get the pointer to an instrument type structure corresponding to a
 *  instrument type string identifier.
 */
INS_TYPE* 
camp_sysGetpInsType( const char* ident /* , bool_t _mutex_lock_sys_check */ )
{
    INS_TYPE* pInsTypeList;

    if( ident == NULL ) return( NULL );

    for( pInsTypeList = pSys->pInsTypes; 
         pInsTypeList != NULL; 
         pInsTypeList = pInsTypeList->pNext )
    {
        if( streq( ident, pInsTypeList->ident ) ) 
	{
            return( pInsTypeList );
	}
    }

    return( NULL );
}


/*
 *  camp_sysGetpAlarmAct
 *
 *  Get the pointer to an alarm action structure corresponding to an
 *  alarm action identifier string.
 */
ALARM_ACT* 
camp_sysGetpAlarmAct( const char* ident /* , bool_t _mutex_lock_sys_check */ )
{
    ALARM_ACT* pAlarmAct;

    if( ident == NULL ) return( NULL );

    for( pAlarmAct = pSys->pAlarmActs; 
         pAlarmAct != NULL; 
         pAlarmAct = pAlarmAct->pNext ) 
    {
        if( streq( ident, pAlarmAct->ident ) )
	{
            return( pAlarmAct );
        }
    }

    return( NULL );
}


/*
 *  camp_sysGetpLogAct
 *
 *  Get the pointer to an log action structure corresponding to a
 *  log action identifier string.
 */
LOG_ACT* 
camp_sysGetpLogAct( const char* ident /* , bool_t _mutex_lock_sys_check */ )
{
    LOG_ACT* pLogAct;

    if( ident == NULL ) return( NULL );

    for( pLogAct = pSys->pLogActs; 
         pLogAct != NULL; 
         pLogAct = pLogAct->pNext ) 
    {
        if( streq( ident, pLogAct->ident ) )
	{
            return( pLogAct );
        }
    }

    return( NULL );
}


/*
 *  camp_sysGetpIFType
 *
 *  Get the pointer to an interface type structure corresponding to a
 *  interface type identifier string.
 */
CAMP_IF_t* 
camp_sysGetpIFType( const char* ident /* , bool_t _mutex_lock_sys_check */ )
{
    CAMP_IF_t* pIFType;

    if( ident == NULL ) return( NULL );

    for( pIFType = pSys->pIFTypes; 
         pIFType != NULL; 
         pIFType = pIFType->pNext ) 
    {
        if( streq( ident, pIFType->ident ) ) 
	{
            return( pIFType );
        }
    }

    return( NULL );
}


DIRENT* camp_sysGetpDirEnt_clnt( const char* filename ) { return camp_sysGetpDirEnt_clnt( filename ); }
INS_TYPE* camp_sysGetpInsType_clnt( const char* ident ) { return camp_sysGetpInsType_clnt( ident ); }
ALARM_ACT* camp_sysGetpAlarmAct_clnt( const char* ident ) { return camp_sysGetpAlarmAct_clnt( ident ); }
LOG_ACT* camp_sysGetpLogAct_clnt( const char* ident ) { return camp_sysGetpLogAct_clnt( ident ); }
CAMP_IF_t* camp_sysGetpIFType_clnt( const char* ident ) { return camp_sysGetpIFType_clnt( ident ); }
