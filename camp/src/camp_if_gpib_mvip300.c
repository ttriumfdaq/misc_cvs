/*
 *  Name:       camp_if_gpib_mvip300.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *              Routines to interface with GPIB instruments by way of an
 *              MVIP300 Industry Pack on an MVME processor.  Low level
 *              routines were supplied by a third-party (thank goodness).
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *              and optionally:
 *                int if_gpib_clear ( int gpib_address );
 *                int if_gpib_timeout ( float timeoutSeconds );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation a GPIB semaphore is used to lock
 *              access to the GPIB bus during communications.  During
 *              this time it is safe to unlock the global mutex (using
 *              thread_unlock_global_np).
 *
 *  Notes:
 *
 *     timeout = 1 to 17 (time = 1e((timeout+1)/2)usec, see ugpib.h)
 *
 *     INI file configuration string:
 *         none
 *
 *     Meaning of private (driver-specific) variables:
 *         pIF_t->priv == 1 (gpib available)
 *                     == 0 (gpib unavailable)
 *         pIF->priv   - pointer to GPIB_PHYS_DEV structure
 *
 *
 *  Revision history:
 *
 *   16-Dec-1999  TW  CAMP_DEBUG conditional compilation
 *   20-Dec-1999  TW  Added dvclr before every dvwrt call.  Found that
 *                    a clear before each write makes the LakeShore 330
 *                    much more reliable.  Haven't tested the affect on 
 *                    other instruments.
 *   21-Dec-1999  TW  Syd says that this is excessive.  Some devices
 *                    (e.g., HP devices) use a GPIB device clear as
 *                    more than an interface clear - it can affect the
 *                    operational state of the device.
 *                    Remove the dvclr, but consider as a separate Tcl
 *                    command to be called from instrument drivers.
 *   21-Dec-1999  TW  Most references to gpibStatus global variable
 *                    moved inside semGPIB take/give.  Should be all.
 *   13-Dec-2000  TW  Found that GPIB operations holding all other tasks
 *                    up on VxWorks.  Knocking down the priority during
 *                    read and write operations lets other tasks go.
 *                    The code for this was taken from gpibLib.c, where
 *                    it was already done.
 *   13-Dec-2000      priority_down, priority_set  
 *   19-Dec-2008  DA  The previous change was involved with Camp hangups
 *                    that leave all gpib communication locked even after  
 *                    a soft reboot (one needed to press the reset button 
 *                    or cycle the (vme) power on the camp server).  Moving
 *                    the priority change so that the priority is lowered 
 *                    before waiting for the gpib semaphore improves but
 *                    does not cure the problem. The priority-changing is 
 *                    removed.
 *   19-Dec-2008      Eliminate priority_down, priority_set  
 *   20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *   20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *   20140220     TW  camp_if_getTerm
 *   20140420     TW  don't use globals gpibStatus/gpibError, they could 
 *                    change outside of if_t mutex
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "camp_srv.h"
#include "camp_ugpib.h"
// #define GPIBLIB_H    /* bring in global variables */
#include "camp_gpibLib.h"
#include "taskLib.h"
#include "tickLib.h"
#include "sysLib.h" // sysClkRateGet


//#define DELAY_READ_AFTER_WRITE pIF->accessDelay
#define DELAY_READ_AFTER_WRITE 0.05

typedef unsigned char* faddr_t; /* far character pointer*/
extern int ibeos(int);
extern int ibtmo(int);
extern int dvclr(int);
extern int dvrd(int padsad,faddr_t buf,unsigned int cnt);
extern int dvwrt(int padsad,faddr_t buf,unsigned int cnt);

extern void mvip300SetTimerDelay( double delay );
extern int mvip300TimeoutSecondsToFlag( double seconds );
// extern double mvip300TimeoutFlagToSeconds( int flag );

#define if_gpib_ok( pIF_t )  ( pointerToInteger( pIF_t->priv ) == 1 )

/* static void getTerm( char* term_in, char* term_out, int* term_len ); */
/*
 * static int priority_down( int down_amount );
 * static void priority_set( int iPriority );
 * #define GPIB_PRIORITY_REDUCTION 10
 */

#define VXWORKS_SLEEP_USES_TICKGET
#define LOG_STATISTICS_INTERVAL 60.0

static void camp_vxworks_sleep_calibrate();
double camp_vxworks_sleep_counts_per_second = 0.0;

static void aggregate_statistics( GPIB_PHYS_DEV* pGpibPhysDev, int which );
static void print_statistics( const char* msg, GPIB_PHYS_DEV* pGpibPhysDev, int which, int where );

static double ticks_to_seconds( unsigned long ticks )
{
    return (double)ticks/(double)sysClkRateGet();
}

static char* strdup_stoprint_expand( const char* str_in, size_t str_out_size )
{
    char* str_out = (char*)camp_malloc( str_out_size );
    camp_stoprint_expand( str_in, str_out, str_out_size );
    return str_out;
}


int
if_gpib_init( void )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	/*
	 *  priv=1 indicates GPIB is available
	 *  Initialization (gpibInit) is called by MVME startup command file
	 */

	camp_vxworks_sleep_calibrate();

	pIF_t->priv = integerToPointer( 1 );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    int addr;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    addr = camp_getIfGpibAddr( pIF->defn );

    if( ( addr < 0 ) ||	( addr > 31 ) )
    {
	_camp_setMsg( "invalid gpib address %d", addr );
	camp_status = CAMP_FAILURE; goto return_;
    }

    pIF->priv = gpibPhysDevCreate( addr );
    if( pIF->priv == NULL )
    {
	_camp_setMsg( "gpib address %d already in use", addr );
	camp_status = CAMP_FAILURE; goto return_;
    }
    else
    {
	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    // GPIB_PHYS_DEV* pGpibPhysDev = (GPIB_PHYS_DEV*)pIF->priv;
	    /* _camp_log( "pGpibPhysDev:%p pGpibPhysDev->iPriBusId:%d", pGpibPhysDev, pGpibPhysDev->iPriBusId ); */
	}
    }

return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    // GPIB_PHYS_DEV* pGpibPhysDev = (GPIB_PHYS_DEV*)pIF->priv;
	    /* _camp_log( "pGpibPhysDev:%p pGpibPhysDev->iPriBusId:%d", pGpibPhysDev, pGpibPhysDev->iPriBusId ); */
	}

	{
	    STATUS vxworks_status = OK;

	    vxworks_status = gpibPhysDevDelete( (GPIB_PHYS_DEV*)pIF->priv );

	    if( vxworks_status == ERROR )
	    {
		_camp_setMsg( "failed deleting gpib device" );
		camp_status = CAMP_FAILURE; goto return_;
	    }
	    else
	    {
		camp_status = CAMP_SUCCESS;
	    }
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    int gpib_status = 0;
    int gpib_error = 0;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[8];
    int term_len;
    char str[LEN_STR+1];
    GPIB_PHYS_DEV* pGpibPhysDev;
    char* command;
    int count;
    int readCount;
    int endOfString;
    // char term_test;
    /*    int oldPriority; */
    size_t command_size;
    timeval_t	timeLastAccess;
#define MAX_READ 0xFFFE /* Same as IBMAXIO */
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;
	CAMP_IF_t* pIF_t;
	STATUS vxworks_status = OK;

	pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked by ins mutex

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	pGpibPhysDev = (GPIB_PHYS_DEV*)pIF->priv;
	cmd = pReq->spec.REQ_SPEC_u.read.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
	buf = pReq->spec.REQ_SPEC_u.read.buf;
	buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    /* _camp_log( "pGpibPhysDev:%p pGpibPhysDev->iPriBusId:%d", pGpibPhysDev, pGpibPhysDev->iPriBusId ); */
	}

	camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

        command_size = cmd_len + term_len + 1;
	command = (char*)camp_malloc( command_size );
	camp_strncpy_num( command, command_size, cmd, cmd_len ); // this terminates
	camp_strncpy_num( &command[cmd_len], command_size-cmd_len, term, term_len ); // this terminates

	/*
	 *  Can't get this to work.
	 *  Just disable.
	 */
        // endOfString = ( (REOS) << 8 ) | term[0]; // 0x040A or 0x040D
	endOfString = 0;
	// ibeos( endOfString ); // 20140421  TW  moved inside mutex lock

	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    // _camp_log( "eos = %04X", endOfString );

	    char* msg = strdup_stoprint_expand( command, command_size+6 );

	    _camp_log( "writing '%s' (len=%d)", msg, command_size-1 );

	    free( msg );
	}

	{
	    int globalMutexLockCount = mutex_unlock_global_all();
	    /*  
	     *  Get the current priority and slow it down so that the 
	     *  GPIB driver does not block other tasks
	     */
	    /* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */

	    /*
	     *  Warning: don't take another mutex between if_t take/release
	     *  (could cause deadlock with mainInterp or instrument mutex)
	     */
	    mutex_lock_if_t( pIF_t, 1 ); // semTake( semGPIB, WAIT_FOREVER ); 
	    {
		/* priority_down was here */

		ibeos( endOfString );  // 20140421  TW  moved inside mutex lock

		// 20140421  TW  set timeout for pIF 
		ibtmo( mvip300TimeoutSecondsToFlag( pIF->timeout ) ); // if_gpib_timeout( pIF->timeout );
		mvip300SetTimerDelay( pIF->timeout ); // timer with better resolution than ibtmo

		count = cmd_len + term_len; 

		/*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
		 *                   you send a clear every time.  Is this excessive?
		 *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
		 *                   (e.g., HP devices) use a GPIB device clear as
		 *                   more than an interface clear - it can affect the
		 *                   operational state of the device.
		 *                   Remove this, but consider as a separate Tcl
		 *                   command to be called from instrument drivers.
		 */
		/* gpib_status = dvclr( pGpibPhysDev->iPriBusId ); */
		gpib_status = dvwrt( pGpibPhysDev->iPriBusId, command, count );
		gpib_error = iberr;

		/*
		 *  Often get addressing error, works if repeat command
		 */
		if( ( gpib_status & ERR ) && ( gpib_error == EADR ) )
		{
		    /* gpib_status = dvclr( pGpibPhysDev->iPriBusId ); */
		    gpib_status = dvwrt( pGpibPhysDev->iPriBusId, command, count );
		    gpib_error = iberr;
		}

		vxworks_status = ( gpib_status & ERR ) ? ERROR : OK; // note: ERR includes TIMO
		
		gettimeval( &timeLastAccess );

		//if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
		    aggregate_statistics( pGpibPhysDev, 1 );

		/* moved priority_set down */
	    }
	    mutex_lock_if_t( pIF_t, 0 ); // semGive( semGPIB );

	    /* priority_set( oldPriority );*/  /*  Set priority back  */

	    mutex_lock_global_all( globalMutexLockCount );
	}
	
	/* if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) */
	/* { */
	/*     unsigned long tick_diff; */
	/*     tick_diff = tickGet() - pGpibPhysDev->time_last_dump[1]; */
	/*     if( ticks_to_seconds( tick_diff ) > LOG_STATISTICS_INTERVAL ) print_statistics( get_current_ident(), pGpibPhysDev, 1, 1 ); */
	/* } */

	if( vxworks_status != ERROR )
	{
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
	    {
		char* msg = strdup_stoprint_expand( command, count+6+1 );

		_camp_log( "wrote '%s' (len=%d)", msg, count );

		free( msg );
	    }
	}

	free( command );

	if( vxworks_status == ERROR )
	{
	    if( gpib_status & TIMO )
	    {
		_camp_setMsg( "gpib write timeout, gpib_status 0x%08X", gpib_status );
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "write timeout, gpib_status = 0x%08X", gpib_status ); }
	    }
	    else
	    {
		_camp_setMsg( "gpib write failed, gpib_status = 0x%08X", gpib_status );
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "write failed, gpib_status = 0x%08X", gpib_status ); }
	    }

	    camp_status = CAMP_FAILURE; goto return_;
	}
	else
	{
	    camp_status = CAMP_SUCCESS;
	}

	readCount = 0;

	camp_if_termTokenToChars( camp_getIfGpibReadTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

	/*
	 *  Can't get this to work.
	 *  Just disable.
	 */
        // endOfString = ( (REOS) << 8 ) | term[0]; // 0x040A or 0x040D
	endOfString = 0;
	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    // _camp_log( "eos = %04X", endOfString );
	}

	while( readCount < buf_len )
	{
	    // add an accessDelay between write and consecutive reads
	    {
		double delay, diff;
		timeval_t timeCurrent;
		CAMP_IF* pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked

		gettimeval( &timeCurrent );

		diff = difftimeval( &timeCurrent, &timeLastAccess ); // timeCurrent - timeLastAccess

		delay = DELAY_READ_AFTER_WRITE - diff;

		if( delay > 0.0 )
		{
		    int globalMutexLockCount = mutex_unlock_global_all();
		    camp_fsleep( delay );
		    mutex_lock_global_all( globalMutexLockCount );
		}
	    }

	    {
		int globalMutexLockCount = mutex_unlock_global_all();

		/*  
		 *  Get the current priority and slow it down so that the 
		 *  GPIB driver does not block other tasks
		 */
		/* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */

		if( _camp_debug(CAMP_DEBUG_MUTEX) ) { _camp_log( "read: gave global mutex, taking gpib mutex" ); }

		/*
		 *  Warning: don't take another mutex between if_t take/release
		 */
		mutex_lock_if_t( pIF_t, 1 ); // semTake( semGPIB, WAIT_FOREVER );
		{
		    int maxReadCount = _min( buf_len-readCount, MAX_READ );

		    if( _camp_debug(CAMP_DEBUG_MUTEX) ) { _camp_log( "read: got gpib mutex, doing read..." ); }

		    ibeos( endOfString );

		    // 20140421  TW  set timeout for pIF 
		    ibtmo( mvip300TimeoutSecondsToFlag( pIF->timeout ) ); // if_gpib_timeout( pIF->timeout );
		    mvip300SetTimerDelay( pIF->timeout ); // timer with better resolution than ibtmo

		    gpib_status = dvrd( pGpibPhysDev->iPriBusId, &buf[readCount], maxReadCount );
		    gpib_error = iberr;
		    count = ibcnt;
		    vxworks_status = ( gpib_status & ERR ) ? ERROR : OK; // note: ERR includes TIMO
		
		    gettimeval( &timeLastAccess );

		    // if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
		    aggregate_statistics( pGpibPhysDev, 0 );

		    if( _camp_debug(CAMP_DEBUG_MUTEX) ) { _camp_log( "read: done read, giving gpib mutex" ); }
		}
		mutex_lock_if_t( pIF_t, 0 ); // semGive( semGPIB );

		if( _camp_debug(CAMP_DEBUG_MUTEX) ) { _camp_log( "read: taking global mutex" ); }

		/* priority_set( oldPriority );*/  /*  Set priority back  */

		mutex_lock_global_all( globalMutexLockCount );
	    }

	    if( _camp_debug(CAMP_DEBUG_MUTEX) ) { _camp_log( "read: got global mutex" ); }
	
	    /* if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) */
	    /* { */
	    /* 	unsigned long tick_diff; */
	    /* 	tick_diff = tickGet() - pGpibPhysDev->time_last_dump[0]; */
	    /* 	if( ticks_to_seconds( tick_diff ) > LOG_STATISTICS_INTERVAL ) print_statistics( get_current_ident(), pGpibPhysDev, 0, 1 ); */
	    /* } */

	    if( vxworks_status != ERROR )
	    {
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
		{
		    char* msg = strdup_stoprint_expand( &buf[readCount], count+6+1 );

		    _camp_log( "read '%s' (len=%d)", msg, count );

		    free( msg );
		}
	    }

	    if( vxworks_status == ERROR )
	    {
		if( gpib_status & TIMO )
		{
		    _camp_setMsg( "gpib read timeout, gpib_status 0x%08X", gpib_status );
		    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "read timeout, gpib_status = 0x%08X", gpib_status ); }
		}
		else
		{
		    _camp_setMsg( "gpib read failed, gpib_status = 0x%08X", gpib_status );
		    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "read failed, gpib_status = 0x%08X", gpib_status ); }
		}

		camp_status = CAMP_FAILURE; goto return_;
	    }
	    else
	    {
		readCount += count;

		camp_status = CAMP_SUCCESS;
	    }

	    /* 
	     *  Could put check for termination here,
	     *  instead just quit after one read
	     */
	    break;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  Return number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = readCount;

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    int gpib_status = 0;
    int gpib_error = 0;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    char str[LEN_STR+1];
    GPIB_PHYS_DEV* pGpibPhysDev;
    char* command;
    int endOfString;
    int count;
    /* int oldPriority; */
    size_t command_size;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;
	CAMP_IF_t* pIF_t;
	STATUS vxworks_status = OK;

	pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	pGpibPhysDev = (GPIB_PHYS_DEV*)pIF->priv;
	cmd = pReq->spec.REQ_SPEC_u.write.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

	camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

	/*
	 *  Can't get this to work.
	 *  Just disable.
	 */
        // endOfString = ( (REOS) << 8 ) | term[0]; // 0x040A or 0x040D
	endOfString = 0;
	// ibeos( endOfString ); // 20140421  TW  moved inside mutex lock

	command_size = cmd_len + term_len + 1;
	command = (char*)camp_malloc( command_size );
	camp_strncpy_num( command, command_size, cmd, cmd_len ); // this terminates
	camp_strncpy_num( &command[cmd_len], command_size-cmd_len, term, term_len ); // this terminates

	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
	{
	    // _camp_log( "eos = %04X", endOfString );

	    char* msg = strdup_stoprint_expand( command, command_size+6 );

	    _camp_log( "writing '%s' (len=%d)", msg, command_size-1 );

	    free( msg );
	}

	{
	    int globalMutexLockCount = mutex_unlock_global_all();

	    /*  
	     *  Get the current priority and slow it down so that the 
	     *  GPIB driver does not block other tasks
	     */
	    /* oldPriority = priority_down( GPIB_PRIORITY_REDUCTION ); */

	    /*
	     *  Warning: don't take another mutex between if_t take/release
	     */
	    mutex_lock_if_t( pIF_t, 1 ); // semTake( semGPIB, WAIT_FOREVER );
	    {
		ibeos( endOfString ); // 20140421  TW  moved inside mutex lock

		// 20140421  TW  set timeout for pIF 
		ibtmo( mvip300TimeoutSecondsToFlag( pIF->timeout ) ); // if_gpib_timeout( pIF->timeout );
		mvip300SetTimerDelay( pIF->timeout ); // timer with better resolution than ibtmo

		count = cmd_len + term_len;

		/*  20-Dec-1999  TW  Found some devices (lake330) are more reliable if
		 *                   you send a clear every time.  Is this excessive?
		 *  21-Dec-1999  TW  Syd says that this is excessive.  Some devices
		 *                   (e.g., HP devices) use a GPIB device clear as
		 *                   more than an interface clear - it can affect the
		 *                   operational state of the device.
		 *                   Remove this, but consider as a separate Tcl
		 *                   command to be called from instrument drivers.
		 */
		/* gpib_status = dvclr( pGpibPhysDev->iPriBusId ); */
		gpib_status = dvwrt( pGpibPhysDev->iPriBusId, command, count );
		gpib_error = iberr;

		/*
		 *  Often get addressing error, works if repeat command
		 */
		if( ( gpib_status & ERR ) && ( gpib_error == EADR ) )
		{
		    /* gpib_status = dvclr( pGpibPhysDev->iPriBusId ); */
		    gpib_status = dvwrt( pGpibPhysDev->iPriBusId, command, count );
		    gpib_error = iberr;
		}

		vxworks_status = ( gpib_status & ERR ) ? ERROR : OK; // note: ERR includes TIMO
		
		// if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
		    aggregate_statistics( pGpibPhysDev, 1 );
	    }
	    mutex_lock_if_t( pIF_t, 0 ); // semGive( semGPIB );

	    /* priority_set( oldPriority );*/  /*  Set priority back  */

	    mutex_lock_global_all( globalMutexLockCount );
	}
	
	/* if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) */
	/* { */
	/*     unsigned long tick_diff; */
	/*     tick_diff = tickGet() - pGpibPhysDev->time_last_dump[1]; */
	/*     if( ticks_to_seconds( tick_diff ) > LOG_STATISTICS_INTERVAL ) print_statistics( get_current_ident(), pGpibPhysDev, 1, 1 ); */
	/* } */

	if( vxworks_status != ERROR )
	{
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) )
	    {
		char* msg = strdup_stoprint_expand( command, count+6+1 );

		_camp_log( "wrote '%s' (len=%d)", msg, count );

		free( msg );
	    }
	}

	free( command );

	if( vxworks_status == ERROR )
	{
	    if( gpib_status & TIMO )
	    {
		_camp_setMsg( "gpib write timeout, gpib_status 0x%08X", gpib_status );
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "write timeout, gpib_status = 0x%08X", gpib_status ); }
	    }
	    else
	    {
		_camp_setMsg( "gpib write failed, gpib_status 0x%08X", gpib_status );
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "write failed, gpib_status = 0x%08X", gpib_status ); }
	    }

	    camp_status = CAMP_FAILURE; goto return_;
	}
	else
	{
	    camp_status = CAMP_SUCCESS;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 */
int 
if_gpib_clear( int gpib_addr )
{
    int camp_status = CAMP_SUCCESS;
    int gpib_status = 0;
    int gpib_error = 0;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	STATUS vxworks_status = OK;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	{
	    int globalMutexLockCount = mutex_unlock_global_all();

	    /*
	     *  Warning: don't take another mutex between if_t take/release
	     */
	    mutex_lock_if_t( pIF_t, 1 ); // semTake( semGPIB, WAIT_FOREVER );
	    {
		gpib_status = dvclr( gpib_addr );
		gpib_error = iberr;

		/*
		 *  Often get addressing error, works if repeat command
		 */
		if( ( gpib_status & ERR ) && ( gpib_error == EADR ) )
		{
		    gpib_status = dvclr( gpib_addr );
		    gpib_error = iberr;
		}

		vxworks_status = ( gpib_status & ERR ) ? ERROR : OK; // note: ERR includes TIMO
	    }
	    mutex_lock_if_t( pIF_t, 0 ); // semGive( semGPIB );
	    mutex_lock_global_all( globalMutexLockCount );
	}

	if( vxworks_status == ERROR )
	{
	    if( gpib_status & TIMO )
	    {
		_camp_setMsg( "gpib clear timed-out" );
	    }
	    else
	    {
		_camp_setMsg( "gpib clear failed" );
	    }

	    camp_status = CAMP_FAILURE; goto return_;
	}
	else
	{
	    camp_status = CAMP_SUCCESS;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}

/*
 *  if_gpib_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 *                   For DELTAT implementation, GPIB_INIT would need
 *                   to be called again.
 *  20140403     TW  changed argument from int to float
 */
int 
if_gpib_timeout( float timeoutSeconds_f )
{
    int camp_status = CAMP_SUCCESS;
    int gpib_status = 0;
    int gpib_error = 0;
    int timeoutFlag;
    // int timeoutSeconds = (int)timeoutSeconds_f;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	STATUS vxworks_status = OK;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	timeoutFlag = mvip300TimeoutSecondsToFlag( timeoutSeconds_f );

	{
	    int globalMutexLockCount = mutex_unlock_global_all();

	    /*
	     *  Warning: don't take another mutex between if_t take/release
	     */
	    mutex_lock_if_t( pIF_t, 1 ); // semTake( semGPIB, WAIT_FOREVER );
	    {
		gpib_status = ibtmo( timeoutFlag );
		gpib_error = iberr;
		vxworks_status = ( gpib_status & ERR ) ? ERROR : OK;
	    }
	    mutex_lock_if_t( pIF_t, 0 ); // semGive( semGPIB );
	    mutex_lock_global_all( globalMutexLockCount );
	}

	if( vxworks_status == ERROR )
	{
	    _camp_setMsg( "failed setting timeout value (time:%f flag:%d)", timeoutSeconds_f, timeoutFlag );

	    camp_status = CAMP_FAILURE; goto return_;
	}
	else
	{
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { _camp_log( "set timeout value (time:%f flag:%d)", timeoutSeconds_f, timeoutFlag ); }

	    camp_status = CAMP_SUCCESS;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


#ifdef UNUSED
static int
priority_down( int down_amount )
{
    int tid, iOldPriority, iNewPriority;

    tid = taskIdSelf();

    taskPriorityGet( tid, &iOldPriority );

    iNewPriority = _min( iOldPriority + down_amount, 255 );

    taskPrioritySet( tid, iNewPriority );

    return( iOldPriority );
}
#endif // UNUSED


#ifdef UNUSED
static void
priority_set( int iOldPriority )
{
    taskPrioritySet( taskIdSelf(), iOldPriority );
}
#endif // UNUSED


/*
 *  20140421  TW  
 */
void camp_vxworks_sleep_calibrate()
{
    unsigned long tick_diff = 0;
    double tdiff;
    /* double diff, diff_target; */
    int n = 1;
    int tick_target = 10; // 10 = 0.1667sec, 100 = 1.667sec delay at startup
    char* mode;

    taskLock(); // no preempt while calibrating
    {
	// with nothing in the loop:
	//   1.04e7 counts = 100 ticks (1.667 sec) -> 0.16 usec/count
	//
	// with gettimeval in the loop:
	//   1.7e5  counts =  99 ticks (1.65  sec) -> 10 usec/count
	//
	// with tickGet in the loop:
	//   1.24e6 counts = 100 ticks (1.667 sec) -> 1.34 usec/count

#ifdef VXWORKS_SLEEP_USES_TICKGET
	unsigned long t0 = tickGet();
	mode = "tickGet";
	n = 0;
	do {
	    tick_diff = tickGet() - t0;
	    n++;
	} while( tick_diff < tick_target ); 
#else
	mode = "no-op";
	n = ( tick_target < 100 ) ? 1100000 : 11000000;
	for( ; n <= 1000000000; n *= 2 )
	{
	    int i;
	    unsigned long t0 = tickGet();
	    for( i = 0; i < n; i++ ) { } // this might get optimized-out
	    tick_diff = tickGet() - t0;

	    if( tick_diff >= tick_target ) break;
	}
#endif
    }
    taskUnlock();

    tdiff = ticks_to_seconds( tick_diff );

    camp_vxworks_sleep_counts_per_second = ( tick_diff > 0 ) ? ((double)n)/(double)tdiff : 0.0;

    if( camp_vxworks_sleep_counts_per_second == 0.0 )
    {
#ifdef VXWORKS_SLEEP_USES_TICKGET
	camp_vxworks_sleep_counts_per_second = 1.24e6/1.667; // default
#else
	camp_vxworks_sleep_counts_per_second = 1.04e7/1.667; // default
#endif
    }

    _camp_log( "calibrate sleep_counts_per_second=%g (mode=%s n=%d time=%g)", camp_vxworks_sleep_counts_per_second, mode, n, tdiff );

    /* for( diff_target = 1.0e-6; diff_target < 1.0e1; diff_target *= 10.0 ) */
    /* { */
    /* 	gettimeval( &t0 ); */
    /* 	do { */
    /* 	    gettimeval( &t1 ); */
    /* 	    diff = difftimeval( &t1, &t0 ); // t1 - t0 */
    /* 	} while( diff <= diff_target ); */

    /* 	_camp_log( "diff_target=%g diff=%g", diff_target, diff ); */
    /* } */
}


/*
 *  20140421  TW  
 */
void camp_vxworks_sleep( double delay )
{
    volatile unsigned long t1;
    volatile int i; // 20140422 volatile suggested by DA
    // int n = _max( (int)ceil( camp_vxworks_sleep_counts_per_second*delay ), 1 ); // causes craziness
    int n = _max( (int)( camp_vxworks_sleep_counts_per_second*delay + 0.5 ), 1 );

    //if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
    /* _camp_log( "sleep_counts_per_second=%g delay=%g n=%d", camp_vxworks_sleep_counts_per_second, delay, n ); */

#ifdef VXWORKS_SLEEP_USES_TICKGET
    for( i = 0; i < n; i++ ) { t1 = tickGet(); } // approx 0.8us per count
#else
    for( i = 0; i < n; i++ ) { } // approx 0.17us per count
#endif
}


/*
 *  20140421  TW  
 */
void aggregate_statistics( GPIB_PHYS_DEV* pGpibPhysDev, int which )
{
    unsigned long transfers_old                = pGpibPhysDev->transfers[which];
    unsigned long transfer_bytes_old           = pGpibPhysDev->transfer_bytes[which];
    unsigned long transfer_checks_tot_old      = pGpibPhysDev->transfer_checks_tot[which];
    unsigned long transfer_checks_max_old      = pGpibPhysDev->transfer_checks_max[which];
    unsigned long transfer_checks_byte_max_old = pGpibPhysDev->transfer_checks_byte_max[which];
    unsigned long transfer_delays_tot_old      = pGpibPhysDev->transfer_delays_tot[which];
    unsigned long transfer_delays_max_old      = pGpibPhysDev->transfer_delays_max[which];
    unsigned long transfer_delays_byte_max_old = pGpibPhysDev->transfer_delays_byte_max[which];
    double        transfer_time_avg_old        = pGpibPhysDev->transfer_time_avg[which];
    unsigned long transfer_time_max_old        = pGpibPhysDev->transfer_time_max[which];
    double        transfer_time_byte_avg_old   = pGpibPhysDev->transfer_time_byte_avg[which];
    unsigned long transfer_time_byte_max_old   = pGpibPhysDev->transfer_time_byte_max[which];

    double        transfer_time_byte_avg      = ( mvip300_transfers > 0 ) ? mvip300_transfer_time / (double)mvip300_transfers : 0.0;

    unsigned long transfers_new                = transfers_old+1;
    unsigned long transfer_bytes_new           = transfer_bytes_old + mvip300_transfers;
    unsigned long transfer_checks_tot_new      = transfer_checks_tot_old + mvip300_transfer_checks_tot;
    unsigned long transfer_checks_max_new      = _max( transfer_checks_max_old, mvip300_transfer_checks_tot );
    unsigned long transfer_checks_byte_max_new = _max( transfer_checks_byte_max_old, mvip300_transfer_checks_byte_max );
    unsigned long transfer_delays_tot_new      = transfer_delays_tot_old + mvip300_transfer_delays_tot;
    unsigned long transfer_delays_max_new      = _max( transfer_delays_max_old, mvip300_transfer_delays_tot );
    unsigned long transfer_delays_byte_max_new = _max( transfer_delays_byte_max_old, mvip300_transfer_delays_byte_max );
    double        transfer_time_avg_new        = (transfers_new>0) ? ( ((double)transfers_old)*transfer_time_avg_old + (double)mvip300_transfer_time ) / (double)transfers_new : 0.0;
    unsigned long transfer_time_max_new        = _max( transfer_time_max_old, mvip300_transfer_time );
    double        transfer_time_byte_avg_new   = (transfer_bytes_new>0) ? ( ((double)transfer_bytes_old)*transfer_time_byte_avg_old + ((double)mvip300_transfers)*(double)transfer_time_byte_avg ) / (double)transfer_bytes_new : 0.0;
    unsigned long transfer_time_byte_max_new   = _max( transfer_time_byte_max_old, mvip300_transfer_time_byte_max );

    pGpibPhysDev->transfers[which]                = transfers_new;
    pGpibPhysDev->transfer_bytes[which]           = transfer_bytes_new;
    pGpibPhysDev->transfer_checks_tot[which]      = transfer_checks_tot_new;
    pGpibPhysDev->transfer_checks_max[which]      = transfer_checks_max_new;
    pGpibPhysDev->transfer_checks_byte_max[which] = transfer_checks_byte_max_new;
    pGpibPhysDev->transfer_delays_tot[which]      = transfer_delays_tot_new;
    pGpibPhysDev->transfer_delays_max[which]      = transfer_delays_max_new;
    pGpibPhysDev->transfer_delays_byte_max[which] = transfer_delays_byte_max_new;
    pGpibPhysDev->transfer_time_avg[which]        = transfer_time_avg_new;
    pGpibPhysDev->transfer_time_max[which]        = transfer_time_max_new;
    pGpibPhysDev->transfer_time_byte_avg[which]   = transfer_time_byte_avg_new;
    pGpibPhysDev->transfer_time_byte_max[which]   = transfer_time_byte_max_new;
}


/*
 *  20140421  TW  
 */
void print_statistics( const char* msg, GPIB_PHYS_DEV* pGpibPhysDev, int which, int where )
{
    char prefix[16];
    const char* type = ( which == 0 ) ? "read" : "write";

    camp_snprintf( prefix, sizeof( prefix ), "  %5s", type );

    if( where == 0 )
    {
	printf( "gpib mvip300 statistics for instrument '%s':\n", msg );
	printf( "%s: transfers=%ld\n",                prefix, pGpibPhysDev->transfers[which] );
	printf( "%s: transfer_bytes=%ld\n",           prefix, pGpibPhysDev->transfer_bytes[which] );
	printf( "%s: transfer_bytes_avg=%.4g\n",      prefix, (pGpibPhysDev->transfers[which]>0) ? (double)pGpibPhysDev->transfer_bytes[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	printf( "%s: transfer_checks_tot=%ld\n",      prefix, pGpibPhysDev->transfer_checks_tot[which] );
	printf( "%s: transfer_checks_avg=%.4g\n",     prefix, (pGpibPhysDev->transfers[which]>0) ? (double)pGpibPhysDev->transfer_checks_tot[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	printf( "%s: transfer_checks_max=%ld\n",      prefix, pGpibPhysDev->transfer_checks_max[which] );
	printf( "%s: transfer_checks_byte_max=%ld\n", prefix, pGpibPhysDev->transfer_checks_byte_max[which] );
	printf( "%s: transfer_delays_tot=%ld\n",      prefix, pGpibPhysDev->transfer_delays_tot[which] );
	printf( "%s: transfer_delays_avg=%.4g\n",     prefix, (pGpibPhysDev->transfers[which]>0) ? pGpibPhysDev->transfer_delays_tot[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	printf( "%s: transfer_delays_max=%ld\n",      prefix, pGpibPhysDev->transfer_delays_max[which] );
	printf( "%s: transfer_delays_byte_max=%ld\n", prefix, pGpibPhysDev->transfer_delays_byte_max[which] );
	printf( "%s: transfer_time_avg=%.4g\n",       prefix, pGpibPhysDev->transfer_time_avg[which] );
	printf( "%s: transfer_time_max=%ld\n",        prefix, pGpibPhysDev->transfer_time_max[which] );
	printf( "%s: transfer_time_byte_avg=%.4g\n",  prefix, pGpibPhysDev->transfer_time_byte_avg[which] );
	printf( "%s: transfer_time_byte_max=%ld\n",   prefix, pGpibPhysDev->transfer_time_byte_max[which] );
    }
    else
    {
	camp_log( "gpib mvip300 statistics for instrument '%s':", msg );
	camp_log( "%s: transfers=%ld",                prefix, pGpibPhysDev->transfers[which] );
	camp_log( "%s: transfer_bytes=%ld",           prefix, pGpibPhysDev->transfer_bytes[which] );
	camp_log( "%s: transfer_bytes_avg=%.4g",      prefix, (pGpibPhysDev->transfers[which]>0) ? (double)pGpibPhysDev->transfer_bytes[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	camp_log( "%s: transfer_checks_tot=%ld",      prefix, pGpibPhysDev->transfer_checks_tot[which] );
	camp_log( "%s: transfer_checks_avg=%.4g",     prefix, (pGpibPhysDev->transfers[which]>0) ? (double)pGpibPhysDev->transfer_checks_tot[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	camp_log( "%s: transfer_checks_max=%ld",      prefix, pGpibPhysDev->transfer_checks_max[which] );
	camp_log( "%s: transfer_checks_byte_max=%ld", prefix, pGpibPhysDev->transfer_checks_byte_max[which] );
	camp_log( "%s: transfer_delays_tot=%ld",      prefix, pGpibPhysDev->transfer_delays_tot[which] );
	camp_log( "%s: transfer_delays_avg=%.4g",     prefix, (pGpibPhysDev->transfers[which]>0) ? pGpibPhysDev->transfer_delays_tot[which]/(double)pGpibPhysDev->transfers[which] : 0.0 );
	camp_log( "%s: transfer_delays_max=%ld",      prefix, pGpibPhysDev->transfer_delays_max[which] );
	camp_log( "%s: transfer_delays_byte_max=%ld", prefix, pGpibPhysDev->transfer_delays_byte_max[which] );
	camp_log( "%s: transfer_time_avg=%.4g",       prefix, pGpibPhysDev->transfer_time_avg[which] );
	camp_log( "%s: transfer_time_max=%ld",        prefix, pGpibPhysDev->transfer_time_max[which] );
	camp_log( "%s: transfer_time_byte_avg=%.4g",  prefix, pGpibPhysDev->transfer_time_byte_avg[which] );
	camp_log( "%s: transfer_time_byte_max=%ld",   prefix, pGpibPhysDev->transfer_time_byte_max[which] );

	pGpibPhysDev->time_last_dump[which] = tickGet();
    }
}


/*
 *  20140422  TW  
 *
 *  console command to print stats
 */
void mvip300Stats( int where )
{
    int i;

    for( i = 0; i < 31; ++i )
    {
	char msg[16];
	GPIB_PHYS_DEV* pGpibPhysDev = gpibDevices[i];

	if( pGpibPhysDev == NULL ) continue;

	camp_snprintf( msg, sizeof( msg ), "addr=%d", i );

	print_statistics( msg, pGpibPhysDev, 0, where );
	print_statistics( msg, pGpibPhysDev, 1, where );
    }
}
