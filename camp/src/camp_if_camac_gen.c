/*
 *  Name:       camp_if_camac_gen.c
 *
 *  Purpose:    Provides a CAMAC interface type.
 *              CAMAC interface routines implementing a generic CAMAC library.
 *
 *              NOTE: this is not a template, but it depends on the CAMAC
 *              commands in camp_tcl.c being implemented (which may only
 *              be the case for VxWorks).  This was a convenient way to 
 *              adapt CAMAC instruments to the way most instruments work
 *              by reading/writing strings, get Tcl to do the dispatching of 
 *              CAMAC commands.
 *
 *              Note that CAMAC implementation in CAMP is by way of
 *              ASCII command strings which are interpreted in a Tcl
 *              interpreter.  The routines if_camac_write and if_camac_read
 *              supplied here simply pass valid Camp-Tcl command strings to
 *              the interpreter.  Consequently, there doesn't need to be
 *              anything specific to a particular library in this file.
 *              The intent is that the command strings perform CAMAC access
 *              using commands provided by a particular library (cdreg, cfsa,
 *              etc.; see camp_tcl.c),
 *
 *              A CAMP CAMAC interface definition must provide the following
 *              routines:
 *                int if_camac_init ( void );
 *                int if_camac_online ( CAMP_IF *pIF );
 *                int if_camac_offline ( CAMP_IF *pIF );
 *                int if_camac_read( REQ* pReq );
 *                int if_camac_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines are called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP CAMAC interfaces one must be
 *              careful in considering the removal of the global lock during
 *              interpretation of the Tcl command.  The Tcl interpreter
 *              could be the main interpreter or an intrument interpreter
 *              and one must be sure about the safeness of the underlying
 *              CAMAC library.  For these reasons, CAMAC commands are called
 *              with the lock on.  This has not been a problem because
 *              there are normally few CAMAC commands in CAMP drivers, and
 *              they execute very quickly in comparison with other CAMP
 *              device type (RS232, GPIB).
 *
 *  Revision history:
 *
 *    20140217  TW  rename set_global_mutex_noChange to mutex_lock_mainInterp
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
if_camac_init( void )
{
    int camp_status = CAMP_SUCCESS;

    {
	// pedantic check
	CAMP_IF_t* pIF_t = camp_ifGetpIF_t( "camac" );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "none" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }

return_:
    return( camp_status );
}


int
if_camac_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;

    {
	// pedantic check
	CAMP_IF_t* pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }

return_:
    return( camp_status );
}


int
if_camac_offline( CAMP_IF* pIF )
{
    return( CAMP_SUCCESS );
}


int
if_camac_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd; 
    _mutex_lock_begin();

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    {
	int tcl_status = TCL_OK;
	int useMainInterp = FALSE;
	Tcl_Interp* interp = get_thread_interp();
	if( interp == camp_tclInterp() ) useMainInterp = TRUE;

	//  20140225  TW  don't use interp outside of lock
	if( useMainInterp ) _mutex_lock_mainInterp_on();
	{
	    tcl_status = Tcl_Eval( interp, cmd );

	    if( tcl_status == TCL_ERROR )
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );		
		_camp_appendMsg( "failed CAMAC read" );

		free( tcl_message );
	    }
	}
	if( useMainInterp ) _mutex_lock_mainInterp_off();

	if( tcl_status == TCL_ERROR )
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( camp_status );
}


int
if_camac_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd; 
    _mutex_lock_begin();

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    {
	int tcl_status = TCL_OK;
	int useMainInterp = FALSE;
	Tcl_Interp* interp = get_thread_interp();
	if( interp == camp_tclInterp() ) useMainInterp = TRUE;

	//  20140225  TW  don't use interp outside of lock
	if( useMainInterp ) _mutex_lock_mainInterp_on();
	{
	    tcl_status = Tcl_Eval( interp, cmd );

	    if( tcl_status == TCL_ERROR )
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );		
		_camp_appendMsg( "failed CAMAC write" );

		free( tcl_message );
	    }
	}
	if( useMainInterp ) _mutex_lock_mainInterp_off();

	if( tcl_status == TCL_ERROR )
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( camp_status );
}
