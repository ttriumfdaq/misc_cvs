/*
 *  Name:       camp_write.c
 *
 *  Purpose:    General utilities for CAMP file writing
 *
 *  Called by:  camp_*_write.c
 * 
 *  Preconditions:
 *              openOutput must be called before any file I/O.  This will
 *              set the "fout" global variable of which there is only one,
 *              meaning that there can only be one open file at a time.
 *
 *              add_tab and del_tab change the tabbing context that is
 *              maintained in the global variable "tabs".  These are used
 *              to somewhat prettify the output files.
 *
 *  Postconditions:
 *              closeOutput must be called when a file write procedure
 *              is finished.
 *
 *  Revision history:
 *   v1.2  16-May-1994  [T. Whidden] Taken from the old camp_parse.c
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include "camp_srv.h"

static FILE *fout = NULL;                        /* file pointer of current input */
#define LEN_TABS 19
static char tabs[LEN_TABS+1];


int
openOutput( const char* filename_in )
{
    char filename[LEN_FILENAME+1];

    camp_translate_generic_filename( filename_in, filename, sizeof( filename ) ); // 20140325

    fout = fopen( filename, "w" );
    if( fout == NULL )
    {
	_camp_setMsg( "Unable to open file '%s' for output", filename );
	return( CAMP_INVAL_FILE );
    }

    return( CAMP_SUCCESS );
}


int
closeOutput( void )
{
    if( fclose( fout ) == 0 )
    {
        fout = NULL;
        return( CAMP_SUCCESS );
    }
    else
    {
        return( CAMP_FAILURE );
    }
}


void
add_tab( void )
{
    camp_strncat( tabs, LEN_TABS+1, "\t" ); // strcat( tabs, "\t" ); // terminates, size includes terminator
}


void
del_tab( void )
{
    tabs[ strlen( tabs ) - 1 ] = '\0';
}


void
print( const char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    vfprintf( fout, fmt, args );

    va_end( args );
}


void
print_tab( const char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );

    fputs( tabs, fout );
    vfprintf( fout, fmt, args );

    va_end( args );
}


/*
 * initialize the world
 */
void 
reinitialize( const char* name, PFB parse_proc, const char* line )
{
    tabs[0] = '\0';
}


int
camp_writeDumpFile( const char* filename, unsigned int fnlen,
                    const char* dump, unsigned int dump_len )
{
    int n;
    int camp_status;

    camp_status = openOutput( filename );

    if( _failure( camp_status ) ) 
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed openOutput" ); }
	return( camp_status );
    }

    n = fwrite( dump, 1, dump_len, fout );

    closeOutput();

    if( n == dump_len )
    {
	return( CAMP_SUCCESS );
    }
    else
    {
	return( CAMP_FAIL_DUMP );
    }
}

/*
 * numStrTrimz: trim trailing zeros from a string representation of a number
 */
void
numStrTrimz( char* str )
{
    int i,id,ie,len;

    len = strlen(str);
    id = ie = 0;

    for( i=0; i<len; i++ )
    {
	if( str[i]=='.' ) id = i+1;
	if( str[i]=='E' || str[i]=='e' || str[i]=='D' || str[i]=='d' ) ie = i;
	if( ie == 0 && id > 0 && i > id && str[i] != '0' ) id = i;
    }

    if( id > 0 ) 
    {
	if( ie == 0 )
	{/* float */
	    str[id+1] = '\0';
	}
	else 
	{/* exp */
	    if( id+1 < ie )
	    {
		for( id++; ie<=len; ie++,id++)
		{
		    str[id] = str[ie];
		}
	    }
	}
    }
}

