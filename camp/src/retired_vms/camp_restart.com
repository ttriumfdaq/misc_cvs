$ max_checks = 25
$ @camp:camp_login
$ gosub check
$ if( .not. up ) then goto start
$ camp_cmd "sysRundown"
$ i = 0
$wait_loop:
$ i = i + 1
$ if i .gt. max_checks then exit 0
$ wait 00:00:01.00
$ gosub check
$ if( up ) then goto wait_loop
$ wait 00:00:05.00
$start:
$ @camp:camp_srv
$ exit 1
$check:
$ up = 0
$ set noon
$ camp_cmd ""
$ status = $status
$ set on
$ if( status ) then up = 1
$ return
$ !
$ !  camp_restart.com  -  restart CAMP server
$ !
$ !  Revision history:
$ !   v1.0   12-Jul-1994  [TW] Initial version
$ !   v1.0a  06-Sep-1994  [TW] changed camp_cmd format
$ !
