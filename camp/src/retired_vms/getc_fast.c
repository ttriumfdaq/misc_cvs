From:	COM%"info-vax-request@KL.SRI.COM" 19-APR-1991 09:09:03.09
To:	CORRIE KOST <KOST@TRIUMFRG.BITNET>
CC:	
Subj:	VAX C RTL bashing: An alternative for GETC

Received: From UVVM(MAILER) by TRIUMFCL with Jnet id 3916
          for KOST@TRIUMFCL; Fri, 19 Apr 91 09:08 PDT
Received: by UVVM (Mailer R2.07) id 8877; Fri, 19 Apr 91 09:07:46 PDT
Date:         Thu, 18 Apr 91 10:33:46 GMT
Reply-To:     INFO-VAX@SRI.COM
Sender:       INFO-VAX Discussion <INFO-VAX@UBVM.BITNET>
From:         info-vax-request@KL.SRI.COM
Subject:      VAX C RTL bashing: An alternative for GETC
Comments: To: info-vax@kl.sri.com
To:           CORRIE KOST <KOST@TRIUMFRG.BITNET>
 
The VAXCRTL is certainly one of the biggest abortions of all time.
 
Here is an easy fix to programs which are SLOW because of the poor
performance of GETC. Just include this file *after* you include <stdio.h>
and you can speed up simple I/O by up to 600% or more.
 
Filename: stdio_fix.h
 
#undef getc
#define getc(p) \
 (--(*p)->_cnt>=0 ? \
  ((int) * ((unsigned char *) (*p)->_ptr++)) : \
  ((*p)->_cnt = 0, fgetc(p)))
 
 
-------------------------------------------------------------------
Now, here is a benchmark file to prove the performance claims:
 
Filename: getc.c
 
/* Measure the performance of GETC vs XGETC.
   18-APR-91 GJC@MITECH.COM
 
This shows how piss-poor SLOW the DIGITAL's VAXCRTL is on
the most basic a simple of functionality: "getc"
 
On a moderately sized file "SYS$SYSTEM:SDA.EXE" it is over 600% slower
than what is should be! On "DECW$INCLUDE:XPROTO.H" it is over 400% slower.
Might such a thing as the poor software quaility of the VAXCRTL
have an effect on the SPECMARK and other benchmark ratings of the VAX?
You bet it does!
 
   %getc <filename>
 
 
The following measurements run on a VS-3100 with a SCSI disk drive.
 
$ getc sys$system:sda.exe
Opening sys$system:sda.exe and using getc
7.86 seconds user time, 0 seconds system time
293376 bytes read
Opening sys$system:sda.exe and using xgetc
1.23 seconds user time, 0 seconds system time
293376 bytes read
getc-time / xgetc-time = 639.024%
 
$ getc decw$include:xproto.h
Opening decw$include:xproto.h and using getc
1.63 seconds user time, 0 seconds system time
55383 bytes read
Opening decw$include:xproto.h and using xgetc
0.33 seconds user time, 0 seconds system time
55808 bytes read
getc-time / xgetc-time = 493.939%
 
*/
 
#include <stdio.h>
#include <time.h>
 
/* Pre VMS 5.4 this XGETC worked on all files getc worked on
   (getc is still broken on fortran format files),
   except: HA, STREAM_LF files! Where it will see extra bytes at the
   end of the file.
   However, now it seems to work on all files.
 */
 
#define xgetc(p) \
 (--(*p)->_cnt>=0 ? \
  ((int) * ((unsigned char *) (*p)->_ptr++)) : \
  ((*p)->_cnt = 0, fgetc(p)))
 
double stat_time(n)
{static double utime,stime;
 double u,s,t;
 struct tbuffer buff;
 times(&buff);
 u = ((double)buff.proc_user_time) / CLK_TCK;
 s = ((double)buff.proc_system_time) / CLK_TCK;
 if (n == 0)
   {utime = u;
    stime = s;
    return(0.0);}
 else
   {printf("%g seconds user time, %g seconds system time\n",
	   u - utime,s - stime);
    t = u - utime + s - stime;
    utime = u;
    stime = s;
    return(t);}}
 
main(argc,argv)
     int argc;
     char **argv;
{double t1,t2;
 int count;
 FILE *f;
 char *filename;
 if (argc > 1)
   filename = argv[1];
 else
   {printf("getc <filename>\n");
    exit(1);}
 printf("Opening %s and using getc\n",filename);
 if (!(f = fopen(filename,"r")))
   {perror(filename);
    exit(1);}
 count = 0;
 stat_time(0);
 while(getc(f) != EOF) ++count;
 t1 = stat_time(1);
 printf("%d bytes read\n",count);
 fclose(f);
 printf("Opening %s and using xgetc\n",filename);
  if (!(f = fopen(filename,"r")))
   {perror(filename);
    exit(1);}
 count = 0;
 stat_time(0);
 while(xgetc(f) != EOF) ++count;
 t2 = stat_time(1);
 printf("%d bytes read\n",count);
 fclose(f);
 printf("getc-time / xgetc-time = %g%%\n",t1*100/t2);}
