$ SaveVerify = f$verify(0)
$ @camp:[000000]camp_login      ! define symbol camp_cmd
$ log_file := camp:[log]start_camp_srv.log
$ log_file2 := camp:[log]camp_srv.log
$ if( f$mode() .nes. "OTHER" ) .or. -               ! detached is OTHER
    ( f$getjpi("","prcnam") .nes. "START_CAMP" )  - ! but so is system startup
    then goto StartDetached                         ! so check proc name
$ set message /nof/nos/noi/not
$ delete/symbol/global/nolog camp_status
$ delete/symbol/local/nolog  camp_status
$ set message /f/s/i/t
$ !
$ ! Stop any old server
$ !
$StopIt:
$ gosub Check
$ if ( .not. up ) then goto RunIt
$ set noon
$ camp_cmd "sysShutdown"
$ status = $status
$ set on
$ wait ::10
$ if( status ) then goto RunIt
$ stop/id='serverPid'
$ !
$ ! Run the server
$ !
$RunIt:
$ srv_exe = "camp:[vax-vms]camp_srv.exe"
$ if ( f$sea( srv_exe ) .eqs. "" )
$ then
$   write sys$output "CAMP Server executable ''srv_exe' not found.  CAMP not started."
$   ExitStatus = 1
$   goto Exit
$ endif
$! msg_stat = f$environment( "MESSAGE" )
$ set noon
$! set message /nof/nos/noi/not
$ if f$search( "''log_file'" ) .nes. "" then purge/keep=3 'log_file'
$ if f$search( "''log_file2'" ) .nes. "" then purge/keep=3 'log_file2'
$! set message /f/s/i/t
$ set on
$ set process/name="CAMP_SERVER"
$ set noon
$ run 'srv_exe'
$ ExitStatus = $status
$ set on
$ if( ( camp_status .eqs. "RESTART" ) .or. -
      ( camp_status .eqs. "FAILURE" ) ) then goto RunIt
$ goto Exit
$StartDetached:
$! gosub Check
$! if( up )
$! then
$!   write sys$output "CAMP Server already running."
$!   ExitStatus = 1
$!   goto Exit
$! endif
$ run sys$system:loginout -
    /detach -
    /uic=[1,4] -
    /input='F$ENVIRONMENT("PROCEDURE")' -
    /output='log_file' -
    /priv=(nosame,tmpmbx,netmbx) -
    /process_name="START_CAMP"
$ ExitStatus = 1
$Exit:
$ SaveVerify = f$verify(SaveVerify)
$ exit ExitStatus
$Check:
$PidLoop:
$ pid = f$pid( context )
$ if ( pid .eqs. "" ) then goto CheckNotUp
$ name = f$getjpi( pid, "PRCNAM" )
$ if ( name .nes. "CAMP_SERVER" ) then goto PidLoop
$ image = f$getjpi( pid, "IMAGNAME" )
$ if ( f$parse( image,,,"NAME" ) .nes. "CAMP_SRV" ) then goto PidLoop
$CheckIsUp:
$ up = 1
$ serverPid = pid
$ return
$CheckNotUp:
$ up = 0
$ return
$ !
$ !  camp_srv.com  -  start CAMP server if nonexistant
$ !
$ !  Revision history:
$ !   30-Nov-1994  TW  Combined 3 startup files into this one
$ !   10-Oct-1995  TW  directory camp_log: -> camp_root:[log]
$ !   25-Oct-1995  TW  directory camp: -> camp_root:[000000]
$ !   31-Oct-1995  TW  camp_root: -> camp:
$ !   13-Nov-1996  TW  Run as UIC [1,4], stop old server
$ !
