$ !
$ home = f$environment ("default")
$ pid  = f$getjpi (0, "pid")
$ say := write sys$output
$ wt  := write tmp
$ bel[0,8] = 7
$ renamed  = 0
$ on warning then goto err_exit
$ !
$ !  Now add the ACLs to all files in the directory.
$ !
$ set default dsk1:[camp]
$ set noon
$ !
$ !  First is the camp: dir
$ !
$ set file dsk1:[000000]camp.dir /owner=[1,4]/prot=w:re 
$ set file dsk1:[camp]*.* /owner=[1,4]/prot=w:re
$ set file dsk1:[camp]log.dir,dat.dir /prot=w:re
$ !
$ !  Do camp_lib:
$ !
$ if f$search( "dsk1:[camp.lib]*.*" ) .nes. "" then -
    set file dsk1:[camp.lib]*.* /owner=[1,4]/prot=w:re
$ !
$ !  Do camp_log:
$ !
$ if f$search( "dsk1:[camp.log]*.*" ) .nes. "" then -
    set file dsk1:[camp.log]*.* /owner=[1,4]/prot=w:re
$ !
$ !  Do camp_dat:
$ !
$ if f$search( "dsk1:[camp.dat]*.*" ) .nes. "" then -
    set file dsk1:[camp.dat]*.* /owner=[1,4]/prot=w:re
$ !
$ set on
$ !
$ set default 'home'
$ exit
$err_exit:
$ say "Error detected in set_acls_camp.com"
$ exit
$ !
$ !  camp_set_acls.com  -  CAMP ACL setup
$ !
$ !  This procedure sets up a CAMP directory tree ACL structure.
$ !  It should only be necessary to execute this procedure once following
$ !  the installation of CAMP.
$ !
$ !  Revision history:
$ !   v1.0   25-Apr-1994  TW  Initial version, based on
$ !                           SET_ACLS.COM (1A16  3-May-1993 D.Maden)
$ !          28-Apr-1994  TW  Check for existance of files
$ !          23-Jun-1994  TW  Move camp dir, get rid of all acls
$ !          18-Apr-1995  TW  Don't use world-writeable directories
$ !
