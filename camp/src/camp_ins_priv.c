/*
 *  Name:       camp_ins_priv.c
 *
 *  Purpose:    Routines to call the instrument's instrument type procedures
 *              (initProc, deleteProc, onlineProc, offlineProc).
 *
 *              An instrument type is one of the available instrument types
 *              in the CAMP server.
 *
 *              NOTE that these routines call a C routine specific to the
 *              instrument (i.e., initProc, etc.) rather than simply
 *              interpreting the Tcl script associated with that proc
 *              _because_ CAMP was written so that there could be a suite
 *              of compiled instruments built into the server (non-Tcl
 *              instruments).  In implementation, however, Tcl instrument
 *              drivers were found to be much more convenient and sufficiently
 *              fast that compiled drivers were not implemented.
 *
 *              If you look closely at the SYS_ADDINSTYPE procedure in
 *              camp_tcl_sys (camp_tcl.c) you'll see that all instrument
 *              drivers are Tcl drivers.
 *
 *              Because all instrument drivers are Tcl drivers, this layer
 *              of routines is somewhat redundant, but nonetheless convenient.
 *
 *              BUT, compiled drivers could be implemented, in which case
 *              the sysAddInsType procedure in camp_tcl.c (see SYS_ADDINSTYPE)
 *              would need to be expanded to include consideration of
 *              the compiled drivers, including assignment of the
 *              associated ins_*_* routines (e.g., ins_tcl_init, etc.).
 *              There is also a test of the driver type in
 *              campsrv_insadd that would need to be expanded.
 *
 *  Called by:  camp_srv_proc.c
 * 
 *  Inputs:     Pointer to instrument's CAMP variable structure.
 *
 *  Outputs:    CAMP status
 *
 *  Revision history:
 *    20140217     TW  tidying CAMP_DEBUG
 *    16-Dec-1999  TW  CAMP_DEBUG conditional compilation
 *
 */

#include <stdio.h>
#include "camp_srv.h"


/*
 *  camp_insInit -- call an instrument's "initProc"
 */
int
camp_insInit( CAMP_VAR* pVar )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    int (*insType_initProc)( struct CAMP_VAR* ) = NULL;
    // _mutex_lock_begin();

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{        
            _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE;
	    goto error;
	}

	insType_initProc = pInsType->procs.initProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar->core.ident );

    if( insType_initProc != NULL )
    {
        camp_status = (*insType_initProc)( pVar );
        if( _failure( camp_status ) )
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed initProc '%s'", pVar->core.path ); }
	    goto error;
        }
    }

    // _mutex_lock_end();
    return( CAMP_SUCCESS );

error:
    // _mutex_lock_end();
    return camp_status;
}


/*
 *  camp_insDelete -- call an instruments "deleteProc"
 */
int
camp_insDelete( CAMP_VAR* pVar )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    int (*insType_deleteProc)( struct CAMP_VAR* ) = NULL;
    // _mutex_lock_begin();

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{        
	    _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE;
	    goto error;
	}

	insType_deleteProc = pInsType->procs.deleteProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar->core.ident );

    if( insType_deleteProc != NULL )
    {
        camp_status = (*insType_deleteProc)( pVar );
        if( _failure( camp_status ) )
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed deleteProc '%s'", pVar->core.path ); }
            goto error;
        }
    }
    else
    {
        /*
         *  Default action is to try to call the "offlineProc"
         */
        camp_status = camp_insOffline( pVar );
        if( _failure( camp_status ) )
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insOffline '%s'", pVar->core.path ); }
        }
    }

    /* 
     *  Make sure it is really offline
     *  Do this to try and ensure that the interface
     *  is freed (i.e., port deallocated, etc.).
     */
    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF = pIns->pIF;
	if( ( pIF != NULL ) && ( pIF->status & CAMP_IF_ONLINE ) )
	{
	    camp_status = camp_ifOffline( pIF );
	    if( _failure( camp_status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) {  _camp_appendMsg( "failed camp_ifOffline '%s'", pVar->core.path ); }
	    }
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
    return( CAMP_SUCCESS );

error:
    // _mutex_lock_end();
    return camp_status;
}


/*
 *  camp_insOnline -- call an instruments "onlineProc"
 */
int
camp_insOnline( CAMP_VAR* pVar )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    int (*insType_onlineProc)( struct CAMP_VAR* ) = NULL;
    // _mutex_lock_begin();

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
    CAMP_IF* pIF = pIns->pIF;
    if( pIF == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF, pVar->core.path );
        camp_status = CAMP_INVAL_IF;
	goto error;
    }
*/

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{        
	    _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE;
	    goto error;
	}

	insType_onlineProc = pInsType->procs.onlineProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar->core.ident );

    if( insType_onlineProc != NULL )
    {
        camp_status = (*insType_onlineProc)( pVar );
        if( _failure( camp_status ) )
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed onlineProc '%s'", pVar->core.path ); }
            goto error;
        }
    }

    // _mutex_lock_end();
    return( CAMP_SUCCESS );

error:
    // _mutex_lock_end();
    return camp_status;
}


/*
 *  camp_insOffline -- call an instruments "offlineProc"
 */
int 
camp_insOffline( CAMP_VAR* pVar )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    int (*insType_offlineProc)( struct CAMP_VAR* ) = NULL;
    // _mutex_lock_begin();

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
	CAMP_IF* pIF = pIns->pIF;
        if( pIF == NULL ) 
        {
            _camp_setMsgStat( CAMP_INVAL_IF, pVar->core.path );
            camp_status = CAMP_INVAL_IF );
    	    goto error;
        }
*/

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{        
	    _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE;
	    goto error;
	}

	insType_offlineProc = pInsType->procs.offlineProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar->core.ident );

    if( insType_offlineProc != NULL )
    {
        camp_status = (*insType_offlineProc)( pVar );
        if( _failure( camp_status ) && ( camp_status != CAMP_INVAL_IF ) )
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed offlineProc '%s'", pVar->core.ident ); }
            goto error;
        }
    }

    // _mutex_lock_end();
    return( CAMP_SUCCESS );

error:
    // _mutex_lock_end();
    return camp_status;
}


