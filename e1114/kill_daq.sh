#!/bin/sh

case `hostname` in
midtis08*)
    ;;
*)
    echo "The kill_daq script should be executed on midtis06"
    exit 1
    ;;
esac

./kill_client.perl analyzer analyzer
./kill_client.perl rootana  analyzer
./kill_client.perl fecamac  fecamac
./kill_client.perl logger   mlogger
./kill_client.perl mhttpd   mhttpd

#end file
