/* Osaka_epics,c

Frontend code to scan the epics devices (NaCell) for OSAKA
Based on bnmr version bnmr_epics.c Rev 1.25  2007/10/04

   $Log$

*/
#ifdef GONE
#include <stdio.h>
#include <math.h>
#include "cadef.h"
#include "midas.h"
#include "experim.h"
#include "connect.h"
#include "bnmr_epics.h"
#include "EpicsStep.h"
#include "osaka_params.h"
/* Function prototypes */

extern TRIGGER_SETTINGS ts;
extern INT debug;  /* debugs */
extern INT d5;
extern INT d7;
extern INT d11;
extern char *RhelOn;
extern char *RhelSwitch;
extern char *WhelOn;
extern char *WhelOff;
extern char *NaRead_name;
extern char *NaWrite_name;
#endif



static float   Diff;     /* value has changed if (abs(difference between read & write increments) < Diff)*/
static float   Offset;   /* current offset value  */
static float   Epics_stop;






/************************************************************************************/
INT EpicsInit(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* Initialize Epics Scan device - calls NaCell_init */
  INT status;

#ifdef EPICS_ACCESS
  
 if(d5)printf("EpicsInit starting\n");
  if(p_epics_params->NaCell_flag) 
      status = NaCell_init(flip, p_epics_params );
  else
    {
      printf("Epics_init: Invalid Epics Scan device\n" );
      return ( FE_ERR_HW );
    }

  if(d5)printf("EpicsInit returning with status = %d  (last_offset =%.4f) \n",
	       status, p_epics_params->Epics_last_offset);
  return (status);
#else
  return SUCCESS:
#endif
}



/*********************************************************************************/
INT HelicityWatchdog(void)
/************************************************************************************/
{
  INT status;
  float read_val; /* local variable */
  
  /* note: this routine can be used to connect the required channels */
  
  
#ifdef EPICS_ACCESS  
  /* Maintain EPICS Hel Read/Write channels live */
  /* check that channels have not disconnected 
     Channels are:
             RhelOn i.e. "ILE2:POLSW2:STATON"   chid=Rchid_helOn
             WhelOn i.e. "ILE2:POLSW2:DRVON"    chid=Wchid_helOn 
             WhelOff i.e.  "ILE2:POLSW2:DRVOFF" chid=Wchid_helOff
  */
  
  /* 
     check the channels 
  */
  printf("HelicityWatchdog:starting\n");
  
  status = ChannelWatchdog( RhelOn, &Rchid_helOn );
  if (status != SUCCESS )return status;
  status=caRead(Rchid_helOn,&read_val);
  if(status==-1)
    {
      printf("HelicityWatchdog: Bad status after caRead for %s\n", RhelOn);
      return(FE_ERR_HW);
    }
  else
    if(debug)printf("HelicityWatchdog: Read value for %s as %8.3f\n", RhelOn,read_val);

  status = ChannelWatchdog( WhelOn, &Wchid_helOn);
  if (status != SUCCESS )return status;

  status = ChannelWatchdog( WhelOff, &Wchid_helOff);
  if (status != SUCCESS )return status;


#else
  printf("HelicityWatchdog: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif
  return SUCCESS;
}

/********************************************************************/

#ifdef GONE
/*********************************************************************************/
void HelicityDisconnect(void)
/*********************************************************************************/
{
#ifdef EPICS_ACCESS
  /* disconnects all the helicity channels */
 if(Rchid_helOn != -1) 
   clear_channel(Rchid_helOn);
 Rchid_helOn=-1;
 if(Wchid_helOn != -1) 
   clear_channel(Wchid_helOn);
 Rchid_helOn=-1;
 if(Wchid_helOff != -1) 
   clear_channel(Wchid_helOff);
 Rchid_helOn=-1;

#else
  printf("HelicityDisconnect: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif  
 return;
}
#endif

/*********************************************************************************/
INT Helicity_init(void)
     /*********************************************************************************/
{
  INT status,i;
  float value;
  /* Get ID for  HEL ON, HEL OFF HEL SWITCH  (also check HEL SWITCH) 
     Set the Midas Watchdog to a long time before calling !   
     
     Fills global params Rchid_helOn Rchid_helOff Wchid_helOff Wchid_helOn
  */
  
#ifdef EPICS_ACCESS
  // Called from frontend_init
  
  Rchid_helOn=Rchid_helOff=Rchid_helSwitch=Wchid_helOn=Wchid_helOff=-1;
  
  status = caGetSingleId(RhelSwitch, &Rchid_helSwitch);
  if(status==-1)
    {
      printf("Helicity_init: Bad status after caGetID for status of HEL Switch \n");
      caExit(); /* clear any channels that are open */
      return DB_NO_ACCESS;
    }
  
  
  
  
  if(d7)printf("Helicity_init: caGetId returns chan_id for status HEL SWITCH (%s) as %d \n",
	       RhelSwitch,Rchid_helSwitch);
  
  if(d7)printf("Calling read_Epics_chan for helSwitch\n");
  status = read_Epics_chan(Rchid_helSwitch,  &value);
  i=(INT)value;
  printf("Helicity_init: helicity switch %s value is %d (switch control: 1 EPICS 0: HARDWARE)\n",RhelSwitch,i);
  if(i != 1)
    {
      cm_msg(MINFO,"Helicity_init",
	     "ERROR - EPICS does not have control of helicity switch; cannot flip helicity by channel access");
      caExit(); /* clear any channels that are open */
      return FE_ERR_HW;
    }

  status = HelicityWatchdog();
  if(status != SUCCESS)return status;

  printf("Helicity_init: successfully connected to Helicity channels\n");
#else
  printf("Helicity_init: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return SUCCESS;
}



/************************************************************************************/
INT read_Epics_chan(INT chan_id,  float * pvalue)
/************************************************************************************/
{
  /* read an Epics channel directly using channel access
     
  (this routine is used to read the helicity; set the midas watchdog before calling!)
  */
  
  INT status;

#ifdef EPICS_ACCESS
  float fval;
  
  if(chan_id == -1)
  {
    printf("read_Epics_chan: chan_id set to -1; channel has disconnected\n");
    return(FE_ERR_HW);
  }
  if ( !caCheck(chan_id) )
    {
      printf("read_Epics_chan: channel  (chid %d) has disconnected\n",chan_id  );
      return(FE_ERR_HW);
    }

  if(d7)printf("read_Epics_chan: calling caRead with chan_id=%d\n",chan_id);
  status=caRead(chan_id,&fval);
  if(status==-1)
 {
    printf("read_Epics_chan: Bad status after caRead for chan_id=%d \n", chan_id);
    return(FE_ERR_HW);
  }
  else
    if(d7)printf("read_Epics_chan: Read value = %8.3f\n", fval);

  *pvalue=fval;
  /// if(d7)printf("read_Epics_chan for %s returning value = %f \n",ca_name(chan_id),*pvalue);

#else
  printf("read_Epics_chan: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return(DB_SUCCESS);
}
/************************************************************************************/
INT write_Epics_chan(INT chan_id,  float value)
/************************************************************************************/
{
  /* write an Epics channel directly using channel access
     
  (this routine is used to write the helicity; set the midas watchdog before calling!)
  */
  
  INT status;
  float fval;

#ifdef EPICS_ACCESS

  fval=value;

  if(chan_id == -1)
  {
    printf("write_Epics_chan: chan_id set to -1; channel has disconnected\n");
    return(FE_ERR_HW);
  }
  if ( !caCheck(chan_id) )
    {
      printf("write_Epics_chan: channel  (chid %d) has disconnected\n",
	     chan_id  );
      return(FE_ERR_HW);
    }
  
  if(d7)
    printf("write_Epics_chan: calling caWrite with fval=%f chan_id=%d\n",fval,chan_id);
  status=caWrite(chan_id,&fval);
  if(status==-1)
 {
     printf("write_Epics_chan: Bad status after caWrite for chan_id=%d\n", chan_id);
    return(FE_ERR_HW);
  }
  else
    if(d7)
      printf("write_Epics_chan: wrote value for chanid=%d as %8.3f\n", chan_id,fval);

#else
  printf("write_Epics_chan: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif 
  return(DB_SUCCESS);
}

/************************************************************************************/
INT EpicsRead(float* pvalue, EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{

  /* Returns SUCCESS  and value read in pvalue. 
     or -1 for failure

     Calls caRead to read appropriate epics device 

     caRead returns -1 for failure and 0 for success
  
         prototype is   int caRead(int chan_id, float *pval);
*/
  INT status;

#ifdef EPICS_ACCESS
  status =  caRead(p_epics_params->XxRchid , pvalue);
  if(status != -1)
    status = SUCCESS;
#else
  printf("EpicsRead: Epics Access is not defined -> DUMMY access to Epics in this code\n");
  status = SUCCESS;
#endif
  return(status);
}

void EpicsDisconnect(EPICS_PARAMS *p_epics_params)
{
  /* disconnect from the channels */
  if(d5)printf("EpicsDisconnect: starting\n");
  if(epics_constants.Rchan_id != -1)
    clear_channel(epics_constants.Rchan_id);
  if(epics_constants.Wchan_id != -1)
    clear_channel(epics_constants.Wchan_id);
  if(d5)printf("Cleared channels %d and %d\n",epics_constants.Rchan_id, epics_constants.Wchan_id);
  epics_constants.Rchan_id=epics_constants.Wchan_id=-1;
  p_epics_params->XxRchid=p_epics_params->XxWchid=-1;	  
  /* set to -1 to indicate these channels are disconnected */
  return;
}


INT EpicsReconnect(EPICS_PARAMS *p_epics_params)
{
  INT status;
  INT max_err=3;
  INT icount;
  BOOL connected;


  if(d5)printf("\nEpicsReconnect: ***  reconnecting to Epics device .....\n");
 
  icount=0;

  connected=FALSE;

  while ( ! connected && icount < max_err )
    {
      /* Clear old channels if open */
      if(p_epics_params->XxRchid != -1)
	clear_channel(p_epics_params->XxRchid);
      if(p_epics_params->XxWchid != -1)
	clear_channel(p_epics_params->XxWchid);
	  
	  
      /* GetIDs for Epics scan ; opens only channels for enabled device  */
      status = EpicsGetIds( p_epics_params );
      if(d5)printf("After EpicsGetIds, status = %d XxRchid=%d XxWchid=%d\n",
	     status, p_epics_params->XxRchid, p_epics_params->XxRchid);
      if( status == SUCCESS) 
	{
	  epics_constants.Rchan_id = p_epics_params->XxRchid ;
	  epics_constants.Wchan_id = p_epics_params->XxWchid ;
	  connected=TRUE;
	}
      else if (status == -2)
	return(-1); /* no valid device specified */


      if(connected) 
	break;
      icount++;
      printf("EpicsReconnect: could not connect to Epics channels.....waiting 15s and retrying (%d)\n",icount);
      jwait(15); /* wait 15s before retrying */
    }
  
  if(connected) 
    return SUCCESS;
  
  cm_msg(MERROR,"EpicsReconnect","Could not reconnect to Epics. Epics system may not be available ");
  return (-1);
}

/************************************************************************************/
INT EpicsWatchdog(EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  INT status;
  float read_val; /* local variable */
  BOOL flag;
  /* note: EpicsReconnect should be called before calling this routine, otherwise
     epics_constants.Rchan_id will not be filled */
  
#ifdef EPICS_ACCESS  
  /* Maintain EPICS live */
  /* check that channel(s) have not disconnected */
  
  /* just make sure we can access ts */
  if (d7)printf("Epics device selected for scan = \"%s\"\n",ts.epics_scan_device);


  flag=FALSE;
  /* 
     check the channels 
  */
 if(d7) printf("EpicsWatchdog: XxRchid=%d,XxWchid=%d\n",
	       p_epics_params->XxRchid,p_epics_params->XxWchid);
  if ( !caCheck(epics_constants.Rchan_id)) 
    {
      printf("EpicsWatchdog: channel  (chid %d) has disconnected... attempting to reconnect \n",
	     epics_constants.Rchan_id);
      status = EpicsReconnect(p_epics_params);
       if(status != SUCCESS)
	 {
	   cm_msg(MERROR, "EpicsWatchdog", "error reconnecting to Epics (%d)",status);
	  printf("EpicsWatchdog: ERROR: bad status after attempting to reconnect to Epics (chid %d)\n",
		 epics_constants.Rchan_id);
	  return(status);
	 }
    }
  if(d7)printf("Watchdog: calling caRead\n"); 
  status=caRead(epics_constants.Rchan_id , &read_val);
  if(status)
    {
      cm_msg(MERROR, "EpicsWatchdog", "error  reading epics voltage (status=%d)",status);
      printf("EpicsWatchdog: ERROR: bad status after calling caRead (%d), chid=%d\n",
	     status,epics_constants.Rchan_id);	  
      return(status);
    }
  else
    if(d7)printf("Watchdog read :%f [%d]\n", read_val, status);

      
  return (SUCCESS);
  
#else
  printf("EpicsWatchdog: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif
  return(SUCCESS);
  
}

/*---------------------------------------------------------------------------*/
INT EpicsGetIds( EPICS_PARAMS *p_epics_params )
/*---------------------------------------------------------------------------*/
{
  INT status;
  p_epics_params->XxRchid=p_epics_params->XxWchid= -1; /* initialize to a bad value */

#ifdef EPICS_ACCESS

  /* Get Epics IDs */
  if(p_epics_params->NaCell_flag)
    {
      if(d5)printf("EpicsGetIds: getting epics channel ID for access to NaCell ....\n");
      status=caGetId (NaRead_name, 
		      NaWrite_name, 
		      &p_epics_params->XxRchid, 
		      &p_epics_params->XxWchid);
    }

  else
    {
      cm_msg(MERROR,"EpicsGetIds","No valid epics device has been specified");
      return -2;
    }

  if(status==-1)
    {
      cm_msg(MERROR,"EpicsGetIds","Cannot get epics channel ID; EPICS system may  not be available");
      return(status);
    }
  else
    {
      if(d5)printf("EpicsGetIds: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
      if (caCheck(p_epics_params->XxRchid) && caCheck(p_epics_params->XxWchid))
	{ if(d5)printf("EpicsGetIds: Info: Epics channels are  connected \n"); }
      else
	printf("EpicsGetIds: Info: One or both of the Epics channels are not connected \n");
    }
#else
  printf("EpicsGetIds: EPICS_ACCESS is not defined -> DUMMY access only in this code\n");
#endif 
  
  return SUCCESS;
}



/************************************************************************************/
INT EpicsIncr( EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  /* called by cycle_start to increment Epics value 

     returns 
     SUCCESS 
     -1 failure returned  from setting/reading Epics device; may no longer have a connection 
     -2 invalid input parameters; may no longer have a connection
     
     -3  read increment and write increment do not agree
     -4  offset between read and set values is outside allowed  range

 */
  
  INT status;
  float check_offset;
  float typical_offset;
  float stability;

  epics_constants.debug=d6;  /* update debugging for EpicsStep */
  if(d5)
    {
      printf("\nEpicsIncr starting\n");
      
      /* Epics constants should have been set up by EpicsInit */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
  
#ifdef EPICS_ACCESS 

  /*
    call EpicsSet_step to set the next setpoint
  */
  
  if(d5)printf("EpicsIncr: calling EpicsSet_step to set %s value to %.4f%s\n",
	       ts.epics_scan_device,p_epics_params->Epics_val, p_epics_params->Epics_units);


  /* derive offset limit from last offset value i.e. widen offset by the stability */
  if (p_epics_params->Epics_last_offset < 0)
    stability =  p_epics_params->Epics_stability * -1; /* stability is positive */
  else
    stability =  p_epics_params->Epics_stability;

  
  check_offset = p_epics_params->Epics_last_offset + stability; 

  if(p_epics_params->NaCell_flag) 
    typical_offset = NaCell_get_typical_offset (p_epics_params->Epics_val); /* variable offset */
  else
    typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */


  status = EpicsSet_step(  
			 p_epics_params->Epics_val,
			 check_offset, 
			 typical_offset,
			 p_epics_params->Epics_inc, 
			 Diff, 
			 &p_epics_params->Epics_read, 
			 &Offset,
			 epics_constants);
  
  if(d5)
    {
      printf("EpicsIncr: EpicsSet_step returns with status =%d\n",status);
      printf("      and Read value=%.4f, offset = %.4f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      if(d5)
	printf("EpicsIncr: Success - Accepting set point %.4f%s (read %.4f%s)\n",
	       p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read, p_epics_params->Epics_units);
        else /* do not remove these spaces or line is overwritten and cannot be read */
      	{	
	  if(d11)printf("\r                                             Set %.4f%s (Read  %.4f%s)",
	   p_epics_params->Epics_val, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units );
      	}
      p_epics_params->Epics_last_offset = Offset;  /* remember this good value */
      return SUCCESS;
      break;
    case (-1):
      /* Cannot get a connection to Epics or error from caread/write; need to stop the run   */  
      return (-1);
    case(-2):
      return (-1); /* error from caread/write or  may not longer have a connection */
      break;
    case (-3):
      printf("EpicsIncr: Info: accepting set point  %.4f%s (read %.4f%s) since read/write values within allowed offset (%.4f%s)\n",
	     p_epics_params->Epics_val, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units,
	     check_offset, p_epics_params->Epics_units );
      p_epics_params->Epics_last_offset = Offset;  /* remember this value */
      return SUCCESS;
      break;
    case(-4):
      printf("EpicsIncr: %s not responding at set point %.4f%s (read %.4f%s) actual offset=%.4f, allowed offset is %.4f%s \n", 
	     ts.epics_scan_device, p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read,
	     p_epics_params->Epics_units, Offset, check_offset, p_epics_params->Epics_units );
      /* if callbacks are working, we should not need to repeat ..... 
	    -  if callbacks are NOT working, repeating just queues up set values
	        may repeat once after a long delay */
      cm_msg(MERROR,"EpicsIncr","Cannot change %s value at set value = %.4f%s (read %.4f%s)",
	     ts.epics_scan_device, p_epics_params->Epics_val, p_epics_params->Epics_units,
	     p_epics_params->Epics_read, p_epics_params->Epics_units);
      return status; 
      break;
    case(-5):
      printf("EpicsIncr: %s is still at old setpoint (read %.4f%s); new setpoint= %.4f%s (actual offset=%.4f, allowed offset=%.4f) \n",
	     ts.epics_scan_device, p_epics_params->Epics_read, p_epics_params->Epics_units,
	     p_epics_params->Epics_val, p_epics_params->Epics_units, Offset, check_offset);
      return (-5); /* value has not changed */
      break;
    default:
      printf("EpicsIncr: Unknown return code %d from EpicsSet_step\n",status);
      return status;
      break;
    } /* end of switch for EpicsSet_step */
  
#else
  printf("EpicsIncr: EPICS_ACCESS not defined; would set  %s value to %.4f%s \n",
	 ts.epics_scan_device,p_epics_params->Epics_val, p_epics_params->Epics_units);
#endif
  return (SUCCESS);
  
  
}  /* end of EpicsIncr */



/************************************************************************************/
INT EpicsCheck( EPICS_PARAMS *p_epics_params)
/************************************************************************************/
{
  /* called by cycle_start to check Epics device  is at the setpoint
     if not, calls EpicsSet 
  */
  
  INT status;
  float ftemp,rtemp,stemp;
  float typical_offset;
  
  epics_constants.debug=d6;  /* update debugging for EpicsStep */

  if(d5)
    {
      printf("\nEpicsCheck starting\n");
      
      /* Epics constants should have been set up by Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
  
#ifdef EPICS_ACCESS
  
  if(d5)printf("EpicsCheck: calling EpicsRead\n");
  /* check if Epics is  set to the correct value */
  status = EpicsRead( &ftemp, p_epics_params);
  if(status!=SUCCESS)
    {	    
      printf("EpicsCheck: Bad status returned from EpicsRead for (%d)\n",status);
      return FE_ERR_HW;
    }
  
  
  printf("EpicsCheck: set value %.4f%s, just read back = %.4f%s Epics_last_offset=%f\n",
	 p_epics_params->Epics_val, p_epics_params->Epics_units,ftemp, p_epics_params->Epics_units,p_epics_params->Epics_last_offset);
  if ( fabs (p_epics_params->Epics_val - ftemp) <= fabs(p_epics_params->Epics_last_offset))
    {
      p_epics_params->Epics_read = ftemp;      
      printf("EpicsCheck: Success: set value (%.4f%s) and read value (%.4f%s) are within last offset of %.4f\n",
	     p_epics_params->Epics_val, p_epics_params->Epics_units, p_epics_params->Epics_read ,
	     p_epics_params->Epics_units, p_epics_params->Epics_last_offset);
      return(SUCCESS);
    }
    
  /* Unless last offset was zero (may not have set even the first value)
     let's widen the offset by adding the stability */
  if (p_epics_params->Epics_last_offset != 0)
    {
      stemp = fabs(p_epics_params->Epics_last_offset) + fabs (p_epics_params-> Epics_stability); 
      if(stemp >= fabs(p_epics_params->Epics_inc))
	{   /* make sure it's less than the increment */
	  rtemp = ( fabs(p_epics_params->Epics_inc) - fabs (p_epics_params-> Epics_stability));
	  if (rtemp >  fabs(p_epics_params->Epics_last_offset) )
	    stemp = rtemp;
	}
    }
  else
    stemp =  fabs(p_epics_params->Epics_last_offset);

   if(p_epics_params->NaCell_flag) 
    typical_offset = NaCell_get_typical_offset (p_epics_params->Epics_val); /* variable offset */
  else
    typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */
   /* typical_offset and stemp are the absolute value */
   if(stemp> typical_offset)stemp=typical_offset;

  printf("EpicsCheck: Read back Epics value as %.4f%s; attempting to set to %.4f%s using acceptable offset=%.4f (device typical offset=%.4f) \n",
	 ftemp, p_epics_params->Epics_units, p_epics_params->Epics_val, p_epics_params->Epics_units, stemp, typical_offset);
  
 
  /* EpicsSet2 sets and reads back Epics value  */
  status=EpicsSet2(
		   p_epics_params->Epics_val,
		   stemp,/* last offset value to check against */
		   typical_offset, /* typical offset at this setpoint */
		   &p_epics_params->Epics_read,
		   &Offset,
		   epics_constants);
  if(d5)
    {
      printf("EpicsCheck: EpicsSet2 returns with status =%d\n",status);
      printf("      and Read value=%.4f%s, offset = %.4f\n",p_epics_params->Epics_read, p_epics_params->Epics_units,Offset );
    }
  switch(status)
    {
    case(0):
      printf("EpicsCheck: Success after EpicsSet2; now at setpoint = %.4f%s, read back %.4f%s, offset %.4f \n",
	     p_epics_params->Epics_val , p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units, Offset);
      p_epics_params->Epics_bad_flag=0;  /* initialize global counter */
      p_epics_params->Epics_last_offset=Offset;
      return (SUCCESS);
      break;
    case(-1):
      printf("EpicsCheck: cannot access Epics devices\n");
      return (-1);
    case(-2):
      printf("EpicsCheck: EpicsSet2 detects invalid parameters\n");
      return (-2);
    case(-4):
      printf("EpicsCheck: %s value not responding at setpoint %.4f%s (read %.4f%s) last acceptable offset=%.4f device typical offset=%.4f\n",
	     ts.epics_scan_device, p_epics_params->Epics_val, p_epics_params->Epics_units,
	     p_epics_params->Epics_read, p_epics_params->Epics_units,
	     p_epics_params->Epics_last_offset, typical_offset);
      
      printf("EpicsCheck: Failure from EpicsSet2 (repeat) at set point %.4f%s (%d)\n",p_epics_params->Epics_val, 
	     p_epics_params->Epics_units, status);
      /*      printf("EpicsCheck: Check if %s is ON\n",ts.epics_scan_device, p_epics_params->Epics_val, status);      
	      cm_msg(MERROR,"EpicsCheck","Cannot change %s value at set value = %.4f (read %.4f)",
	      ts.epics_scan_device, p_epics_params->Epics_val, p_epics_params->Epics_read);
	      cm_msg(MERROR,"EpicsCheck","Check status of %s then restart",ts.epics_scan_device);
	      cm_msg(MTALK,"EpicsCheck","Can't set %s device value",ts.epics_scan_device); */

      p_epics_params->Epics_bad_flag++; /* used to stop trying to set the Epics after Epics_bad_flag = 5  */

      return FE_ERR_HW; /* return error */
      break;
    default:
      printf("EpicsCheck: Invalid return status from EpicsSet2 (%d)\n",status);
      return (FE_ERR_HW);
      break;
    }	/* end of switch for EpicsSet2 */
  
  
  /*   S U C C E S S  */
  /* Epics value has been set successfully */
  
#else
  printf("EpicsCheck: DUMMY access to Epics Device  %s\n",ts.epics_scan_device);
#endif
  return (SUCCESS);
}


/************************************************************************************/
INT  EpicsNewScan(EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /* called by cycle_start at each new scan 

     - sets value to p_epics_params->Epics_val (unless at Begin_Of_Run, as device has 
     already been set to starting point)

     at begin of a scan, usually set Epics device to 1 increment different from scan start.
  */

  INT status;
  float Value_tmp;
  float typical_offset;

  epics_constants.debug=d6;  /* update debugging for EpicsStep */

  if(d5)
    {
      printf("\nEpicsNewScan starting\n");

      /* epics_constants was filled in Laser_init or NaCell_init */
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.4f  min_set_value=%.4f max_set_value=%.4f stability=%.4f, debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }
#ifdef EPICS_ACCESS	  
  
  /* set Epics value to this value (1 incr. away from start value) now for two reasons:
     1. So that the read_inc will be within range for the check later on
     2. To give the Epics device a chance to reach the start value if there is a large sweep */
  
  Value_tmp = p_epics_params->Epics_val;
  /* make sure we are within range - if not get within range by adding/subtracting 2 increments */
  if(Value_tmp > epics_constants.max_set_value ) Value_tmp = p_epics_params->Epics_val - 2* fabs(p_epics_params->Epics_inc) ;
  if(Value_tmp < epics_constants.min_set_value ) Value_tmp = p_epics_params->Epics_val + 2* fabs(p_epics_params->Epics_inc)  ;
  
    if(p_epics_params->NaCell_flag) 
      typical_offset = NaCell_get_typical_offset(Value_tmp); /* variable offset */
    else
      typical_offset = 0.7 *   epics_constants.max_offset;  /* constant offset */

    /* EpicsSet sets & reads back  & checks  Epics value & returns the offset */
    if(d5)printf("\n EpicsNewScan: NEW SCAN -  calling EpicsSet with pre-start value of %.4f%s and typical offset of %.4f\n",
		 Value_tmp, p_epics_params->Epics_units,typical_offset);
    status=EpicsSet( Value_tmp,
		     typical_offset,
		     &p_epics_params->Epics_read,
		     &Offset,
		     epics_constants);
    if(d5)
    {
      printf("EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.4f%s, offset = %.4f\n",p_epics_params->Epics_read, p_epics_params->Epics_units,Offset);
    }
      switch(status)
	{
	case(0):
	  if(d5)printf("Success after EpicsSet at setpoint = %.4f%s, read back %.4f%s (new scan)\n",
		       Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	  p_epics_params->Epics_last_offset=Offset; /* remember this acceptable offset */
	  break;
	case(-1):
	  printf("EpicsNewScan: EpicsSet indicates may have lost connection to Epics Device %s\n",p_epics_params->Epics_units);
	  return (-1);
	case(-2):
	  printf("EpicsNewScan: EpicsSet indicates invalid input parameters supplied for Epics Device at %.4f%s\n",Value_tmp, p_epics_params->Epics_units);

	  return (-2); /* bad params */
	case(-4):
	  printf("EpicsNewScan: %s value not responding at setpoint %.4f%s (read back %.4f%s)\n",
		 ts.epics_scan_device, Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	default:
	  printf("EpicsNewScan: Failure from EpicsSet (new scan) at setpoint %.4f%s (%d)\n",Value_tmp, p_epics_params->Epics_units,status);
	  cm_msg(MERROR, "EpicsNewScan", "cannot set %s value to %.4f%s (read %.4f%s)",
		 ts.epics_scan_device,Value_tmp, p_epics_params->Epics_units,p_epics_params->Epics_read, p_epics_params->Epics_units);
	  cm_msg(MTALK,"EpicsNewScan","can't set %s value",ts.epics_scan_device);
	  return DB_NO_ACCESS;
	}  
      p_epics_params->Epics_last_value = p_epics_params->Epics_read;  /* new scan:  set this for Epics_stability check later */
#else
      printf("EpicsNewScan: DUMMY access to %s\n",ts.epics_scan_device);
#endif
  return(SUCCESS); /* end of new scan */
}



/*****************************************************************************************/
INT  NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params )
/************************************************************************************/
{
  /*   Initialize NaCell
       Called from begin of run 
  */ 
  
  //  float read_inc;
  float NaVolt_tmp;
  
  INT status;
  float Na_typical_offset; /* not a constant for NaCell */

  /* Check we have channel IDs for NaCell access */
#ifdef EPICS_ACCESS
  if (p_epics_params->XxRchid != -1)
    if (!caCheck(p_epics_params->XxRchid)) p_epics_params->XxRchid=-1;
  
  if (p_epics_params->XxWchid != -1)
    if (!caCheck(p_epics_params->XxWchid)) p_epics_params->XxWchid=-1;
  
  
  if (p_epics_params->XxRchid == -1 || p_epics_params->XxWchid == -1)  /* no access */
    {   /* try again for access to NaCell */      
      if(d5)printf("NaCell_init: attempting to get epics channel ID for access to NaCell ....\n");
      p_epics_params->XxRchid=p_epics_params->XxWchid= -1; /* initialize both channels to a bad value */
      
      status = EpicsReconnect(p_epics_params);
      if(status!=SUCCESS)
	{
	  printf("NaCell_init: Cannot get epics channel ID; access to NaCell is not possible\n");
	  cm_msg(MERROR,"NaCell_init","Cannot get epics channel ID; access to NaCell is not possible");
	  return (DB_NO_ACCESS);
	}
      if(d5)printf("NaCell_init: caGetId returns channel ID for read: %d and write: %d\n",
	     p_epics_params->XxRchid,p_epics_params->XxWchid);
    }
#else
  printf("NaCell_init: DUMMY access to NaCell in this code\n");
#endif
  
  sprintf(p_epics_params->Epics_units,"%s",NaUnits);
  p_epics_params->Epics_start = ts.scan_start__volts_;
  Epics_stop = ts.scan_stop__volts_;
  p_epics_params->Epics_inc = ts.scan_step__volts_;
  if( p_epics_params->Epics_inc == 0 )
    {
      cm_msg(MERROR,"NaCell_init","Invalid increment value: %f volt(s)",
	     p_epics_params->Epics_inc);
      return FE_ERR_HW;
    }
  
  if(    p_epics_params->Epics_start < Na_min  || Epics_stop < Na_min || p_epics_params->Epics_start > Na_max || Epics_stop > Na_max)
    {
      cm_msg(MERROR,"NaCell_init","NaVolt start (%.2f) or stop (%.2f) parameter(s) out of range. Allowed range is between %.2f and %.2f volts",
	     p_epics_params->Epics_start, Epics_stop,  Na_min, Na_max);
      return FE_ERR_HW;
    }
  
  if( ((p_epics_params->Epics_inc > 0) && (Epics_stop < p_epics_params->Epics_start)) ||
      ((p_epics_params->Epics_inc < 0) && (p_epics_params->Epics_start < Epics_stop)) )
    p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_ninc =(DWORD) ((Epics_stop - p_epics_params->Epics_start)/p_epics_params->Epics_inc + 1.); 
  if( p_epics_params->Epics_ninc < 1)
    {
      cm_msg(MERROR,"NaCell_init","Invalid number of voltage steps: %d",
	     p_epics_params->Epics_ninc);
      return FE_ERR_HW;
    }
  
  printf("NaCell_init: Selected %d increments starting at %.2f volt(s) by steps of %.2f volt(s)\n",
	 p_epics_params->Epics_ninc,p_epics_params->Epics_start,p_epics_params->Epics_inc);



  //printf("NaCell_init: flip = %d\n",flip);
  if(flip) p_epics_params->Epics_inc *= -1;
  p_epics_params->Epics_val = p_epics_params->Epics_start;
  
  

  if (fabs (p_epics_params->Epics_inc) < Nainc_min )  /* use a minimum increment of currently 0.3V */
    {
      cm_msg(MERROR,"NaCell_init","increment (%.2f) must be at least %.2f",
	     p_epics_params->Epics_inc,Nainc_min);
      return FE_ERR_HW;
    }
  else if  (fabs (p_epics_params->Epics_inc) < Nainc_limit )
    {
      Diff = Nadiff_min;
      p_epics_params->Epics_stability = Nadiff_min *2 ;
    } 
  else
    {
      Diff = Nadiff_max; /* use default constant  value for comparison */
      p_epics_params->Epics_stability = Nadiff_min + 0.15 ;
    }
  if(d5)printf("NaCell_init: Calculated Diff = %.2fV & Epics_stability = %.2f V\n",Diff,p_epics_params->Epics_stability);
  
  
  
  /* 
     check whether we can access the NaCell 
  */
  NaVolt_tmp = p_epics_params->Epics_start - p_epics_params->Epics_inc; /* set a value one incr. away from Epics_start */
  /* make sure this is within range  */
  if(NaVolt_tmp > Na_max || NaVolt_tmp < Na_min ) 
    NaVolt_tmp =  p_epics_params->Epics_start + p_epics_params->Epics_inc ; 
  
  printf("NaCell_init: Initial setpoint (%.2f volts) will be 1 increment away from start value\n",
	 NaVolt_tmp);
  
  /* load up some constants needed by EpicsSet and EpicsSet_Step; these are now independent of device (NaCell or NaCell) */
  epics_constants.Rchan_id = p_epics_params->XxRchid ;
  epics_constants.Wchan_id = p_epics_params->XxWchid ;
  epics_constants.max_offset = Na_max_offset;  /* use an initial large value */
  epics_constants.min_set_value = Na_min;
  epics_constants.max_set_value=  Na_max;
  epics_constants.stability = Na_typical_stability;
  epics_constants.debug=d6;  /* debug EpicsStep */
  Na_typical_offset = NaCell_get_typical_offset(NaVolt_tmp) ;


  if(d5)
    {
      printf(" Epics constants : Rchan_id = %d, Wchan_id = %d \n", epics_constants.Rchan_id, epics_constants.Wchan_id );
      printf("  max_offset=%.2f  min_set_value=%.2f max_set_value=%.2f stability=%.2f debug=%d\n",
	     epics_constants.max_offset,
	     epics_constants.min_set_value, epics_constants.max_set_value,  epics_constants.stability,
	     epics_constants.debug);
    }

#ifdef EPICS_ACCESS
  /* EpicsSet sets and reads back value to check access */
  status=EpicsSet(	NaVolt_tmp,
			Na_typical_offset,
		&p_epics_params->Epics_read,
		&Offset,
		epics_constants);
  if(d5)
    {
      printf("NaCell_init: EpicsSet returns with status =%d\n",status);
      printf("      and Read value=%.2f, offset = %.2f\n",p_epics_params->Epics_read,Offset);
    }
  switch(status)
    {
    case(0):
      printf("NaCell_init: Success after EpicsSet at setpoint = %.2f, read back %.2f\n",
	     NaVolt_tmp, p_epics_params->Epics_read);
      p_epics_params->Epics_last_offset=Offset; /* remember last offset */
      break;
    case(-2):
      printf("NaCell_init: EpicsSet detects invalid parameters\n");
    case(-4):
      printf("NaCell_init: NaCell voltage not responding (could be switched off)\n");
      printf("              at setpoint = %.2f, read back %.2f\n",NaVolt_tmp,p_epics_params->Epics_read);
    default: 
      printf("NaCell_init: Failure from EpicsSet (%d)\n",status);
     cm_msg(MERROR, "NaCell_init", "cannot set NaCell value. Check Osaka expt is granted Epics write access");
      cm_msg(MERROR, "NaCell_init", " and that NaCell is switched on (see EPICS Main Control Page) ");
      return DB_NO_ACCESS;
    }
#else
  printf("NaCell_init: Info - DUMMY - would have set NaCell to setpoint = %.2f \n", NaVolt_tmp);
#endif
  return(SUCCESS);

} /* end of NaCell_init */





float NaCell_get_typical_offset (  float set_point )
{
  float typical_offset;
  /* The NaCell readback offset varies according
     to the voltage. 
     All offsets are in Volts.
  */
  /* Aug 2007 offset seem to have changed!
  if(set_point < 50)       typical_offset=0.3;
  else if(set_point < 75)  typical_offset=0.45;
  else if(set_point < 100) typical_offset=0.4;
  else if(set_point < 175) typical_offset=0.45;
  else if(set_point < 250) typical_offset=0.5;
  else if(set_point < 325) typical_offset=0.55;
  else if(set_point < 400) typical_offset=0.6;
  else if(set_point < 500) typical_offset=0.65;
  else if(set_point < 600) typical_offset=0.7;
  else if(set_point < 700) typical_offset=0.75;
  else                     typical_offset=0.8;
  
  Offset = read - set                    
e.g wrote 20 read 19.2264  -> offset ~ -0.78  */
  if(set_point < 50)       typical_offset=-0.85;
  else if(set_point < 75)  typical_offset=-0.85;
  else if(set_point < 100) typical_offset=-0.8;
  else if(set_point < 175) typical_offset=-0.75;
  else if(set_point < 250) typical_offset=-0.7;
  else if(set_point < 325) typical_offset=-0.6;
  else if(set_point < 400) typical_offset=-0.5;
  else if(set_point < 500) typical_offset=-0.45;
  else if(set_point < 600) typical_offset=-0.45;
  else if(set_point < 700) typical_offset=-0.45;
  else                     typical_offset=-0.4;
  
  
  if(d5)
    printf("get_typical_offset: typical offset at set_point=%.2f is %.2f\n",
	   set_point,typical_offset);
  return(typical_offset);
}


/*********************************************************************************/
INT ChannelReconnect(char *Rname)
/*********************************************************************************/
{
  INT status;

#ifdef EPICS_ACCESS
  INT max_err=1;
  INT icount;
  BOOL connected;
  INT chid;
  
  printf("ChannelReconnect: reconnecting to Epics device \"%s\" .....\n",Rname);
  
  icount=0;
  
  connected=FALSE;
  
  while ( ! connected && icount < max_err )
    {
      /* Clear old channel if open */
      chid = -1;
      status =caGetSingleId(Rname, &chid );
      if(status==0)
	connected=TRUE;
      
      if(connected) 
	break;
      icount++;
      printf("ChannelReconnect: could not connect to Epics channel \"%s\".....waiting 15s and retrying (%d)\n",
	     Rname,icount);
      if(icount < max_err)
	jwait(15); /* wait 15s before retrying */
    }
  if(connected)
    {
      if(d5)printf("ChannelReconnect: successfully reconnected to Epics channel \"%s\" \n",Rname);
      return chid;
    }
  
  cm_msg(MERROR,"ChannelReconnect",
	 "Could not reconnect to Epics channel\"%s\". Epics system may not be available ",Rname);
  return (-1);

#else
  printf("ChannelReconnect: Epics Access is not defined -> DUMMY access to Epics in this code\n");
  return SUCCESS;
#endif 
}

/*********************************************************************************/
INT ChannelWatchdog(char *Name, INT *Chid)
/************************************************************************************/
{
  //  float read_val; /* local variable */
  BOOL flag;
  INT chid;
  INT status;


  /* NOTE:  ChannelReconnect should be called for initialization before calling this routine 
        If reconnects, returns new value of Chid 
  */
  
#ifdef EPICS_ACCESS  
  
  /* check that channel has not disconnected */
  
  chid = *Chid; 
  flag=FALSE;
  /* 
     check the channel 
  */
   if(d5)printf("ChannelWatchdog: checking chid %d\n",chid);

   if (chid == -1)
     {
       status =0;
       printf("ChannelWatchdog: channel is not connected. Attempting to connect \n");
     }
   else
     {
       status = caCheck(chid);
       if (!status)
	 printf("ChannelWatchdog: channel  (chid %d) has disconnected... attempting to reconnect \n",
		chid  );
     }

   if(!status)
     {
       chid = *Chid = -1; /* init to -1 */
       chid = ChannelReconnect(Name);
       if(chid == -1)
	 {
	   cm_msg(MERROR, "ChannelWatchdog", "error reconnecting %s to Epics",Name);
	   return(-1);
	 }
       else
	 {
	   if(d5)printf("ChannelWatchdog: channel id is now %d\n",chid);
	   *Chid=chid; /* return the new value */
	 }
     }
#else
   printf("ChannelWatchdog: Epics Access is not defined -> DUMMY access to Epics in this code\n");
#endif      
   return SUCCESS;
}


/*********************************************************************************/
void ChannelDisconnect(INT *Chid)
/*********************************************************************************/
{
  if(*Chid != -1)  
    clear_channel(*Chid);
  *Chid=-1;
  return;
}

