/* bnmr_epics.h

   e1114 version based on BNMR's Revision 1.10  2005/07/19

   $Log$

 */

/* Define structure EPICS_PARAMS

     contains globals needed by Main program  AND  
     epics routines in bnmr_epics.c  

*/
typedef struct {
  BOOL    NaCell_flag; /* True if we scanning NaCell voltage (i.e. ppg_mode=1n) */
  BOOL    Laser_flag;
  BOOL    Field_flag;
  /* EPICS channel IDs */
  int XxRchid; /* ID for read;  set to -1 if no access */
  int XxWchid; /* ID for write;  set to -1 if no access */
  /* other globals */
  float   Epics_stability; /* value used for stability check (histo process) & NaSet */
  float   Epics_val;      /* Na cell voltage to request */
  float   Epics_inc;
  DWORD   Epics_ninc;     /* number of increments */
  float   Epics_read;     /* value read back */
  INT     Epics_bad_flag; /* counter increments if cannot set NaCell voltage */
  float   Epics_last_value;  /* value as read and accepted by histo_process */
  float   Epics_start;
  float   Epics_last_offset; /* last good offset value */
  char    Epics_units[6]; /* set point units */
} EPICS_PARAMS;

/* direct epics channel access to read helicity  */
int Rchid_helOn, Rchid_helOff; /* channel access helicity read */
int Rchid_helSwitch; /* these are used in helicity_init */
int Wchid_helOn, Wchid_helOff;






/* function prototypes */

/* Functions for Epics devices used for scanning
   i.e. NaCell, Laser, Field
*/
void EpicsDisconnect(EPICS_PARAMS *p_epics_params);
INT EpicsReconnect(EPICS_PARAMS *p_epics_params);
INT EpicsWatchdog  (EPICS_PARAMS *pEpics_params);
INT EpicsIncr      (EPICS_PARAMS *pEpics_params);
INT EpicsGetIds   (EPICS_PARAMS *pEpics_params);
INT  EpicsInit     (BOOL flip,  EPICS_PARAMS *pEpics_params);
INT  EpicsNewScan (EPICS_PARAMS *pEpics_params);
INT EpicsRead      (float* value, EPICS_PARAMS *pEpics_params);
INT EpicsCheck( EPICS_PARAMS *p_epics_params);























