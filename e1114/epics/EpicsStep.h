/* EpicsStep.h

  Header file for EpicsStep.c
E1114 version based on bnmr's version 1.2
 $Log$


*/

typedef struct {
  int Rchan_id;      /* read channel ID */ 
  int Wchan_id;      /* write channel ID */ 
  float max_offset;    /* maximum allowable difference between values written and read back */
  float typical_offset; /* typical offset at this setpoint */
  float min_set_value; /* minimum allowable value for set point */
  float max_set_value; /* maximum allowable value for set point */
  float stability;    /* values read back should be stable to within this value     Used by EpicsSet only */
  int   debug;        /* debug */
} EPICS_CONSTANTS;

/* prototypes */
int EpicsSet (  float val, float device_typical_offset, float *pRvalue,  float *pOffset, 
		EPICS_CONSTANTS epics_constants) ; 
int EpicsSet_step ( float val,float offset, float device_typical_offset, float step, float diff,
		    float *pRvalue, float *pOffset, EPICS_CONSTANTS epics_constants);
int EpicsSet2 (  float val, float last_offset, float device_typical_offset, float *pRvalue,  
		 float *pOffset, EPICS_CONSTANTS epics_constants) ; 

/* global */
//int d4; /* debug ca routines and event handlers etc. */ 
//int d6; /* debug  in LaserSet and LaserSet_step */





