/* connect.h

Prototypes for routines in connect.c 
E1114 version based on bnmr version 1.3

CVS log information:
$Log$

*/

/* caRead, caWrite, caGetId, caExit */
int caGetId(char *pRname, char *pWname, int *pchan_Rid, int *pchan_Wid);
int caRead(int chan_id, float *pval);
int caWrite(int chan_id, float *pval);
void caExit(void);
int caCheck(int chan_id);
int caGetSingleId(char* pSname, int *pSchan_ID);
int caInit( void );
void clear_channel(int chanid);
