/* osaka_epics.h  

Function prototypes for functions in Osaka_epics.c

$Log$

 */
INT  NaCell_init(BOOL flip, EPICS_PARAMS *p_epics_params) ;
INT NaGetIds(EPICS_PARAMS *p_epics_params);
float  NaCell_get_typical_offset (  float set_point );

INT read_Epics_chan(INT chan_id,  float * pvalue);
INT write_Epics_chan(INT chan_id,  float value);
INT ChannelReconnect(char *Rname);
void ChannelDisconnect(INT *Rchid);
INT ChannelWatchdog(char *Rname, INT *Rchid); /* watchdog on single channel */

INT HelicityWatchdog(void);
void HelicityDisconnect(void);
INT Helicity_init(void);












