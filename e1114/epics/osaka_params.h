/* osaka_params.h

$Log$


EPICS Parameters:
*/

/* Read Helicity directly
---------------------------------------------------------------------------
  Read/Write  Helicity 
----------------------------------------------------------------------------
 Epics names of Helicity variables */ 

char RhelOn[]="ILE2:POLSW2:STATON";
char RhelOff[]="ILE2:POLSW2:STATOFF"; // we only need read one of these in fact
char RhelSwitch[]="ILE2:POLSW2:STATLOC";/* 0=DAQ has control 1=Epics has control 
					   Must be in position 1 for channel access
					                       0 for hardware switch (BNMR)
					*/
char WhelOn[]="ILE2:POLSW2:DRVON";   
char WhelOff[]="ILE2:POLSW2:DRVOFF";
/* Epics names of NaCell variables */ 
char NaRead_name[]="ILE2:BIAS15:RDVOL";
char NaWrite_name[]="ILE2:BIAS15:VOL";
 

/*
------------------------------------------------------------------------------

   Sodium Cell :     CONSTANTS
  
------------------------------------------------------------------------------
*/

/* parameters - used at begin run to calculate Diff and stability
   Absolute values!!  */
const float   Nadiff_min=0.05; /* Diff set to  Nadiff_min if the increment fabs (Epics_inc) < Nainc_limit */
const float   Nadiff_max=0.2;  /*      else   set to  Nadiff_max */
const float   Nainc_limit=1.5;/* below this value, Diff = Nadiff_min */
const float   Nainc_min=0.5;   /* minimum increment user is allowed to specify (was 0.3 before offset increased) */
/* note also that stability is calculated using above values */
const float   Na_max=999; /* maximum value that user can set on NaCell (not reliable higher) */
const float   Na_min=0.5; /* minimum value that user can set on NaCell (not reliable lower)  */
const float   Na_max_offset = -1.0; /* maximum offset (read-set) for NaCell (more accurate offset value will be calculated */
const float   Na_typical_stability = 0.2;
char NaUnits[]="V"; /* set points are in volts */
/*      end of NaCell constants        */


/* globals within this file 

    Note: offset = difference between read and write values
          diff   = difference between read and write increments, i.e.
          increment = difference between value now and value at last set point
*/

/* structure defined in EpicsStep.h */
EPICS_CONSTANTS epics_constants = { -1, -1, 0, 0, 0, 0 };
