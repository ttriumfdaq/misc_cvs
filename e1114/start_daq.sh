#!/bin/sh

case `hostname` in
midtis08*)
    ;;
*)
    echo "The start_daq script should be executed on midtis06"
    exit 1
    ;;
esac

./kill_daq.sh

odbedit -c clean
odbedit -c "rm /Analyzer/Trigger/Statistics"
odbedit -c "rm /Analyzer/Scaler/Statistics"

mhttpd -p 8081 -D
sleep 2
xterm -sb -rightbar -j -T fecamac  -e /usr/local/bin/dio ./fecamac &
#xterm -sb -rightbar -j -T Analyzer -e ./sipmanalyzer.exe &
xterm -sb -rightbar -j -T Analyzer -e ./analyzer -w -p9090 &
mlogger -D

echo Please point your web browser to http://localhost:8081
echo Or run: firefox http://localhost:8081 &
echo To look at live histograms, run: roody -Hlocalhost

#end file
