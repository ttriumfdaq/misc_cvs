/* fecamac.h
   prototypes for fecamac.cxx

 $Log$

*/
INT chk_pause(void);
INT chk_resume(void);
INT stop_run(void);
  INT set_init_hel_state(BOOL flip, INT hel_state);
  INT helicity_read(void);
  INT  helicity_write(INT gbl_set_hel);
  BOOL flip_helicity(void);
  INT cycle_start(void);
INT fe_epics_write(void);
//float pol_cal(DWORD ul,DWORD ur,DWORD dl,DWORD dr);
//float dpol_cal(DWORD ul,DWORD ur,DWORD dl,DWORD dr,float pol);



  INT epics_reconnect(void);
  INT read_epics_val(float *pval );
  INT set_epics_val(void);
  INT set_epics_incr(void);
  void jwait(int s);

