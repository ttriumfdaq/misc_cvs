/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Fri Nov 16 16:22:50 2007

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[80];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"Comment = STRING : [80] test",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      active;
  BOOL      write_data;
  char      comment[80];
  BOOL      flip_helicity;
  DWORD     dwell_time__sec_;
  DWORD     helicity_sleep__ms_;
  BOOL      scan_enabled;
  float     scan_stop__volts_;
  float     scan_start__volts_;
  float     scan_step__volts_;
  INT       debug;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"Record data = LINK : [35] /Logger/Channels/0/Settings/Active",\
"Write Data = LINK : [19] /Logger/Write data",\
"Comment = LINK : [35] /Experiment/Run Parameters/Comment",\
"Flip helicity = LINK : [42] /Equipment/Trigger/Settings/Flip Helicity",\
"Dwell time (sec) = LINK : [45] /Equipment/Trigger/Settings/Dwell time (sec)",\
"Helicity sleep (sec) = LINK : [48] /Equipment/Trigger/Settings/Helicity sleep (ms)",\
"Bias scan enabled = LINK : [41] /Equipment/Trigger/Settings/Scan enabled",\
"Bias stop voltage = LINK : [46] /Equipment/Trigger/Settings/Scan stop (volts)",\
"Bias start voltage = LINK : [47] /Equipment/Trigger/Settings/Scan start (volts)",\
"Bias step voltage = LINK : [46] /Equipment/Trigger/Settings/Scan step (volts)",\
"debug = LINK : [34] /Equipment/Trigger/Settings/debug",\
"",\
NULL }

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 2",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 10",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] midtis08.triumf.ca",\
"Frontend name = STRING : [32] fecamac",\
"Frontend file name = STRING : [256] fecamac.cxx",\
"",\
NULL }

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  BOOL      flip_helicity;
  DWORD     dwell_time__sec_;
  DWORD     helicity_sleep__ms_;
  BOOL      scan_enabled;
  float     scan_start__volts_;
  float     scan_stop__volts_;
  float     scan_step__volts_;
  BOOL      camac_pause_enabled;
  char      epics_scan_device[32];
  INT       debug;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Flip Helicity = BOOL : n",\
"Dwell time (sec) = DWORD : 10",\
"Helicity sleep (ms) = DWORD : 500",\
"Scan enabled = BOOL : y",\
"Scan start (volts) = FLOAT : 13",\
"Scan stop (volts) = FLOAT : 15",\
"Scan step (volts) = FLOAT : 1",\
"Camac pause enabled = BOOL : y",\
"Epics scan device = STRING : [32] NaCell",\
"debug = INT : 0",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 1",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 377",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 10",\
"Frontend host = STRING : [32] midtis08.triumf.ca",\
"Frontend name = STRING : [32] fecamac",\
"Frontend file name = STRING : [256] fecamac.cxx",\
"",\
NULL }

#endif

#ifndef EXCL_EPICS

#define EPICS_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} EPICS_COMMON;

#define EPICS_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 12",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 2000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] midtis08.triumf.ca",\
"Frontend name = STRING : [32] Epics",\
"Frontend file name = STRING : [256] frontend.c",\
"",\
NULL }

#define EPICS_SETTINGS_DEFINED

typedef struct {
  struct {
    INT       epics;
  } channels;
  struct {
    struct {
      char      channel_name[20][32];
    } epics;
  } devices;
  char      names[20][32];
  float     update_threshold_measured[20];
} EPICS_SETTINGS;

#define EPICS_SETTINGS_STR(_name) char *_name[] = {\
"[Channels]",\
"Epics = INT : 24",\
"",\
"[Devices/Epics]",\
"Channel name = STRING[20] :",\
"[32] OSAKA:VAR1",\
"[32] OSAKA:VAR2",\
"[32] OSAKA:VAR3",\
"[32] OSAKA:VAR4",\
"[32] OSAKA:VAR5",\
"[32] OSAKA:VAR6",\
"[32] OSAKA:VAR7",\
"[32] OSAKA:VAR8",\
"[32] OSAKA:VAR9",\
"[32] OSAKA:VAR10",\
"[32] OSAKA:VAR11",\
"[32] OSAKA:VAR12",\
"[32] OSAKA:VAR13",\
"[32] OSAKA:VAR14",\
"[32] OSAKA:VAR15",\
"[32] OSAKA:VAR16",\
"[32] OSAKA:VAR17",\
"[32] OSAKA:VAR18",\
"[32] OSAKA:VAR19",\
"[32] OSAKA:VAR20",\
"",\
"[.]",\
"Names = STRING[20] :",\
"[32] Temporal Beta Rate",\
"[32] Temporal L/R ratio",\
"[32] Temporal U/D ratio",\
"[32] Beam Current[nA]",\
"[32] Total L/R ratio",\
"[32] Total beta Asymmetry",\
"[32] LaserPower",\
"[32] NaCell read[V]",\
"[32] Na Cell set[V]",\
"[32] Default%CH 9",\
"[32] Default%CH 10",\
"[32] Default%CH 11",\
"[32] Default%CH 12",\
"[32] Default%CH 13",\
"[32] Default%CH 14",\
"[32] Default%CH 15",\
"[32] Default%CH 16",\
"[32] Default%CH 17",\
"[32] Default%CH 18",\
"[32] Watchdog",\
"Update Threshold Measured = FLOAT[20] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"",\
NULL }

#endif

