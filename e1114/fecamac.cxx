/********************************************************************\

  Name:         osakadfe.c 
  Created by:   Pierre-Andre

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example implement ...
                event" and a "scaler event" which are filled with
                random data. The trigger event is filled with
                two banks (ADC0 and TDC0), the scaler event with 
                one bank (SCLR).

$Log$
Revision 1.7  2007/11/20 21:15:09  suz
Add scaler sums written to epics equipment demand 19Nov07

Revision 1.6  2007/11/20 21:10:24  suz
Yoshikazu's changes 19Nov07

Revision 1.5  2007/11/20 21:15:32  suz
Yoshikazu's changes 17Nov07

Revision 1.4  2007/11/20 20:58:00  suz
epics channel access is added; NaCell scan, helicity flip 16Nov07

Revision 1.3  2007/11/20 20:36:59  suz
Yoshikazu's changes prior to adding epics ca 16Nov07

Revision 1.2  2007/11/20 20:26:03  suz
Yoshikazu's changes including add hel,nacell banks 07Nov07

Revision 1.1  2007/11/20 20:13:17  suz
original for e1114 24Jul07


  Revision history
  ------------------------------------------------------------------
  date         by    modification
  ---------    ---   ------------------------------------------------
  (25-JAN-95    SR    created)
  22-Jan-98    PAA/UG  Ohio Chamber tests in Detector facility
  11-Feb-98    UG     clear TDC with A7F2 (like ADC earlier)

\********************************************************************/

#include <stdlib.h>
#include "midas.h"
#include "msystem.h"
#include "mcstd.h"
#include "experim.h"
#include "fecamac.h" // prototypes

#ifdef EPICS_ACCESS
// For direct epics access via the frontend to NaCell and Helicity */
#include "connect.h"      /* for scanning NaCell or Laser */
#include "bnmr_epics.h"
#include "EpicsStep.h"
#include "osaka_epics.h" // prototypes
#include "osaka_params.h" // epics channel names

static EPICS_PARAMS epics_params;

extern  INT run_state;
INT     gbl_read_hel, gbl_set_hel, gbl_hel_flipped;
INT    helicity_current;
float  heli_moni;
float  na_bias_current, na_set_bias_cur, na_set_bias_old;
DWORD     gbl_inc_cntr;
INT     epicstime;
BOOL      epics_flag;
DWORD  my_dwell_time=0;
#define HEL_DOWN                0         /* helicity down */
#define HEL_UP                  1         /* helicity up */
INT d6=0,d5=0,d7=0,d11=0; // various debugs
#endif  // EPICS_ACCESS

INT debug=0; // debug
BOOL flip=FALSE;
/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

FILE *fmoni, *fbias, *fbias2;

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fecamac";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 3000;

/* buffer size to hold events */
INT event_buffer_size = 10*3000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5*300*300;

  //INT   gbl_run_number;
  //INT   status, crate=0;
  //DWORD lam=0;

  INT   gbl_run_number;
  INT   gbl_CYCLE_N=0;
  INT   run_status=0;
  INT   helicity_demand=0;
  INT   status, crate=0;
  DWORD lam=0;
#ifdef EPICS_ACCESS     
/* Epics scan variable
   Most are in header file */
  INT gbl_volt_n;
  float end_value;
#endif // EPICS_ACCESS

  INT tdemand = 1000;

// Midas EPICS watchdog   
DWORD   gbl_watchdog_timeout;      /* midas watchdog timeout */
BOOL    gbl_watchdog_flag=TRUE;         /* midas watchdog flag */
BOOL    gbl_dachshund_flag=FALSE;  /* true when a long watchdog timeout is set */



/*-- Function declarations -----------------------------------------*/
#ifdef USE_GPIB
    INT read_gpib_event(char *pevent, INT off);
#endif
    INT frontend_init();
    INT frontend_exit();
    INT begin_of_run(INT run_number, char *error);
    INT end_of_run(INT run_number, char *error);
    INT pause_run(INT run_number, char *error);
    INT resume_run(INT run_number, char *error);
    INT frontend_loop();
    
    INT poll_trigger_event(INT count, PTYPE test);
    INT interrupt_configure(INT cmd, PTYPE adr);
    INT read_trigger_event(char *pevent, INT off);
    INT read_scaler_event(char *pevent, INT off);
    INT trigger_create_rec(void);

#ifdef EPICS_ACCESS
  INT get_epics_IDs(void);
  INT set_long_watchdog(DWORD my_watchdog_timeout);
  INT restore_watchdog(void);
#endif

#define CRATE        0

/* Station addresses */
/* IO_N bit pattern:
   0xF000  : Run Gate
   0x0F00  : Clear pulse BOR
   0x00FF  : EOB pulse
*/

#define RUN_STOP   (0x0000)
#define RUN_GATE   (0x0F00)
#define CL_BOR     (0x00F0)  
#define PL_EOB     (0x0001) 
#define RUN_BEGIN  (0x0040) 
#define RUN_RESUME (0x0040) 
#define RUN_END    (0x0080) 
#define RUN_PAUSE  (0x0080)

/* Slot positions */
#define LAM_N       2
#define IO_N        3
#define COI1_N      4
#define AGE1_N      8
#define ADC1_N      13
#define TDC1_N      15
#define NTDC_N      23
#define SCL1_N      20
//#define DISPLAY_N    1

/* Number of ch */
#define A_GE         4  // 1x4

/* Number of slots */
#define N_IO         1    // 
#define N_LAM        1    // 
#define N_COI        2    // 2x16
#define N_AGE        3    // 3x4
#define N_ADC        2    // 2x12
#define N_TDC        4    // 4x8
#define N_NTDC       1    // 1x1
#define N_SCL        2    // 1x16

// SCLR 1 
//  0 : Ge-R (raw : CFD out)
//  1 : Ge-L (raw : CFD out)
//  2 : Ge-1 (raw : CFD out)
//  3 : Ge-2 (raw : CFD out)
//  4 : Ge-3 (raw : CFD out)
//  5 : Ge-4 (raw : CFD out)
//  6 : Ge-5 (raw : CFD out)
//  7 : Ge-6 (raw : CFD out)
//  8 : Ge-7 (raw : CFD out)
//  9 : gamma or
// 10 : gamma-gamma
// 11 : beta-gamma
// 12 : trigger
// 13 : event
// 14 : 
// 15 : 

// SCLR 2 
//  0 : Ge-R (beta anti-coin.)
//  1 : Ge-L (beta anti-coin.)
//  2 : Ge-1 (beta anti-coin.)
//  3 : Ge-2 (beta anti-coin.)
//  4 : Ge-3 (beta anti-coin.)
//  5 : Ge-4 (beta anti-coin.)
//  6 : Ge-5 (beta anti-coin.)
//  7 : Ge-6 (beta anti-coin.)
//  8 : Ge-7 (beta anti-coin.)
//  9 : 
// 10 : 
// 11 : 
// 12 : 
// 13 : 
// 14 : 
// 15 : 

// Coin. Reg.
//  0 : beta-gamma
//  1 : beta-R
//  2 : beta-L
//  3 : gamma/N
//  4 : gamma-gamma
//  5 :
//  6 :
//  7 :


  // Scaler calculations
#define A_SCL 16 // check this value
  DWORD  scaler[N_SCL*A_SCL]; // A_SCL   N_SCL=2
  DWORD  dscaler[N_SCL*A_SCL]; // A_SCL
  DWORD  ul, ur, dl, dr;
  DWORD  ul2 ,ur2 ,dl2, dr2;
  float  al_cps, al_pps, beta_rate, beta_rate_raw;
  float  al_cps_tmp, al_pps_tmp;
  DWORD  counter, counter2;
  float  asym, asym_err;
  float  asym2, asym_err2;
  float  asym_gate6000kev, asym_err_gate6000kev;
  float  asym_gate1500kev, asym_err_gate1500kev;
  float  lr_ratio, ud_ratio, lr_gated6000kev_ratio;
  float test;
  float beta_r_gateGe_6000kev, beta_l_gateGe_6000kev;

  float beta_r_gateGe_1500kev, beta_l_gateGe_1500kev;
  float beta_u_gateGe_1500kev, beta_d_gateGe_1500kev;

  float beta_r_p_gateGe_6000kev, beta_l_p_gateGe_6000kev;
  float beta_r_n_gateGe_6000kev, beta_l_n_gateGe_6000kev;

  float beta_r_p_gateGe_1500kev, beta_l_p_gateGe_1500kev;
  float beta_r_n_gateGe_1500kev, beta_l_n_gateGe_1500kev;

  float beta_r_p_gateGe_1500kev_2, beta_l_p_gateGe_1500kev_2;
  float beta_r_n_gateGe_1500kev_2, beta_l_n_gateGe_1500kev_2;

//#define NTD_TIMEUNIT 0x0022 // 2x10^(2-1) us/ch sample
#define NTD_TIMEUNIT 0x0042 // 4x10^(2-1) us/ch for 28Na
  //#define NTD_TIMEUNIT 0x0052 // 5x10^(2-1) us/ch for 29Na

HNDLE  hDB, hTSKey, hEPD, hEPM;
TRIGGER_SETTINGS  ts;


/*-- Equipment list ------------------------------------------------*/

//#define USE_INT 1

EQUIPMENT equipment[] = {

  { "Trigger",            /* equipment name */
    1, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
#ifdef USE_INT
    EQ_INTERRUPT,         /* equipment type */
#else
    EQ_POLLED,            /* equipment type */
#endif
    LAM_SOURCE(CRATE, LAM_STATION(LAM_N)),  /* event source */
    "MIDAS",               /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |          /* read only when running */
    RO_ODB,               /* and update ODB */ 
    10,                   /* poll for 200ms */
    0,
    0,                    /* stop run after this event limit */
    0,                    /* don't log history */
    "", "", "",
    read_trigger_event,   /* readout routine */
    NULL,                 /* init string */
    NULL,                 /* init string */
  },

  { "Scaler",             /* equipment name */
    2, 1,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |
    RO_TRANSITIONS |      /* read when running and on transitions */
    RO_ODB,               /* and update ODB */ 
    1000,                 /* read every 1 sec */
    0,
    0,                    /* stop run after this event limit */
    10,                   /* log history */
    "", "", "",
    read_scaler_event,    /* readout routine */
    NULL,                 /* polling routine */
    NULL,                 /* init string */
  },

  { "" }
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

//asymmetry parameter
float pol_cal(DWORD a,DWORD b,DWORD c,DWORD d){
  float R;
  if((a>0)&&(b>0)&&(c>0)&&(d>0)){
    R=(float)a/(float)b/(float)c*(float)d;
    return (float)(sqrt(R)-1.f)/(sqrt(R)+1.0f);
  }else{
    return 100.;
  }
}

float pol_cal_err(DWORD a,DWORD b,DWORD c,DWORD d){
  float R, err;
  //  printf("a, b, c, d; %d  %d  %d  %d\n",a, b, c, d);
  if((a>0)&&(b>0)&&(c>0)&&(d>0)){
    R=(float)a/(float)b/(float)c*(float)d;
    err=2.*R/(sqrt(R)+1.)/(sqrt(R)+1.)*sqrt(1/(float)a+1/(float)b+1/(float)c+1/(float)d);
    //    printf("R , err %f %f\n",R,err);
    //    return (float)2*R/pow(sqrt(R)+1,2)*sqrt(1/a+1/b+1/c+1/d);
    return err;
  }else{
    return 100.;
  }
}


/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  INT status;

  run_status=0;
  status = cm_get_experiment_database(&hDB, NULL);
  if(status != DB_SUCCESS)return status;
 

  printf("frontend_init: starting\n");

  /* put here CAMAC hardware initialization */
  register_cnaf_callback(1);
  status=cam_init();
  if(status == 0)
    {
      cm_msg(MERROR,"frontend_init","error from cam_init (%d)",status);
      return FE_ERR_HW;
    }

  // Create record for trigger settings
  status = trigger_create_rec();
  if(status != SUCCESS) return status;


  /* Get EPICS odb keys for fe_epics */
  status = db_find_key(hDB, 0, "/equipment/epics/variables/demand", &hEPD);
  if (status != DB_SUCCESS) hEPD = 0;
  status = db_find_key(hDB, 0, "/equipment/epics/variables/measured", &hEPM);
  if (status != DB_SUCCESS) hEPM = 0;
  

#ifdef EPICS_ACCESS
/*
  Direct Epics Access For NaCell/helicity
  initialize epics scan globals in Osaka_epics.h
*/
  epics_params.NaCell_flag=TRUE; // scan is always NaCell
  epics_params.Epics_bad_flag = 0;
  epics_params.Laser_flag = epics_params.Field_flag= FALSE; // Laser/Field not used
  epics_params.XxRchid = epics_params.XxWchid  =-1;
  caInit(); /* Initialize Epics;  calls  ca_task_initialize(); */


    /* Open the EPICS connections here so we don't keep reconnecting each run */

    epicstime=ss_time();

    /*  open direct channel access to Epics helicity (for reading/writing helicity state)
     */
   
    // gbl_hel_live = FALSE;
    printf("frontend_init: opening direct channel access for helicity read\n");
    set_long_watchdog(300000);

    status = Helicity_init(); /* Opens direct access to Epics hel read and writes 
			      and checks Hel switch is set for DAQ control
				    */
    restore_watchdog();
    if (status != SUCCESS) 
      {
	cm_msg(MERROR,"frontend_init","failure from Helicity_init(%d)",status);
	return FE_ERR_HW;
      }
   

    /* GetIDs for Epics scan */
    status = epics_reconnect();
    if (status != SUCCESS)
      {
	printf("frontend_init: cannot initialize Epics Scan Device (NaCell)\n");
	cm_msg(MINFO,"frontend_init","cannot initialize Epics Scan Device (NaCell)");
	/* Epics device not accessible  .... may not want to use it */
      }
    
#endif // EPICS_ACCESS

  printf("frontend_init: done\n\n");
    return CM_SUCCESS;
}


/* ------------------------------------------------------------------*/
INT trigger_create_rec(void)
  /* ------------------------------------------------------------------*/
{
  /* Create record for trigger/settings; find various handles */
  INT size, status;
  char str[128];
  /*
#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Heli_enabled = BOOL : n",\
"Heli_period = DWORD : 30",\
"Helicity sleep (ms) = DWORD : 1",\
"Scan_enabled = BOOL : n",\
"Vol_start = FLOAT : 0",\
"Vol_stop = FLOAT : 0",\
"Vol_step = FLOAT : 0",\
"pause_enabled = BOOL : y",\
"flip_with_scaler = BOOL : n",\
"Watch_period = INT : 10",\
"scaler_clear = BOOL : n",\
"Epics scan device = STRING : [32] NaCell",\
"",\
NULL }
  */

  TRIGGER_SETTINGS_STR (trigger_settings_str); /*  (from experim.h) */
  
  
  sprintf(str,"/Equipment/Trigger/Settings"); 
  status = db_find_key(hDB, 0, str, &hTSKey);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("trigger_create_rec: Failed to find the key %s ",str);
      
      status = db_create_record(hDB, 0, str, strcomb(trigger_settings_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("trigger_create_rec: Failure creating Trigger record\n");
	  cm_msg(MINFO,"trigger_create_rec","Could not create record for %s  (%d)\n", str,status);
	  return(status);
	}
      status = db_find_key(hDB, 0, str, &hTSKey);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"trigger_create_rec", "Failed to get the key %s ",str);
	  return(status);
	}
    }    
  else  /* key has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hTSKey, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "trigger_create_rec", "error during get_record_size (%d) for %s",status,str);
	  return status;
	}
      printf("trigger_create_rec:  Size of Trigger saved structure: %d, size of Trigger record: %d\n", 
	     sizeof (trigger_settings_str) ,size);
      if (sizeof(trigger_settings_str) != size) 
	{
	  cm_msg(MINFO,"trigger_create_rec","mismatch between size of structure (%d) & record size (%d)", 
		 sizeof(trigger_settings_str) ,size);
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(trigger_settings_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"trigger_create_rec","Could not create trigger/settings record (%d)\n",status);
	      return status;
	    }
	  else
	    if (debug)printf("trigger_create_rec: Success from create record for %s\n",str);
	}
    }
  size = sizeof(ts);
  status = db_get_record(hDB, hTSKey, &ts, &size, 0);
  if (status != DB_SUCCESS) 
    {
      cm_msg(MERROR, "trigger_create_rec", "cannot retrieve Trigger/Settings");
      return DB_NO_ACCESS;
    }
  printf(" debug = %d\n",ts.debug);
  debug = ts.debug;
  return(SUCCESS);
}

int xpoll(int crate, int slot)
{
  static DWORD lam;
  static WORD cr1, cr2, xlam, lld1, lld2, lld3, lld4;
  static WORD adc1, adc2, adc3, adc4;

  int x = 0, q = 0;

  //slot = 22;

  cam16i(crate, slot, 0, 0, &cr1);
  cam16i(crate, slot, 1, 0, &cr2);
#if 1
  cam16i_q(crate, slot, 0, 8, &xlam, &x, &q);

  cam16i(crate, slot, 0, 1, &lld1);
  cam16i(crate, slot, 1, 1, &lld2);
  cam16i(crate, slot, 2, 1, &lld3);
  cam16i(crate, slot, 3, 1, &lld4);

  cam16i(crate, slot, 0, 2, &adc1);
  cam16i(crate, slot, 1, 2, &adc2);
  cam16i(crate, slot, 2, 2, &adc3);
  cam16i(crate, slot, 3, 2, &adc4);

  cam_lam_read(crate, &lam);
#endif
  if (q)
  printf("slot %d, lam: 0x%x, cr1: 0x%x, cr2: 0x%x, lam: 0x%x (x=%d, q=%d), LLD: %d, %d, %d, %d, ADC: %d %d %d %d\n", slot, lam, cr1, cr2, xlam, x, q, lld1&0xFF, lld2&0xFF, lld3&0xFF, lld4&0xFF, adc1, adc2, adc3, adc4);

#if 1
  if (q)
    {
      printf("Reset!\n");
      //while (1) { sleep(10); }
      cam16i_q(crate, slot, 0, 10, &xlam, &x, &q);
      camc(crate, slot, 0, 9);
    }
#endif

  //sleep(1);

  return 0;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  cam_exit();
/* shut down direct access to NaCell/Laser via Epics */
    caExit(); /* closes any open channels for Epics */
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int status,size,n,i;
  WORD wtmp;
  char c[256];

  //  if(epics_flag){
    sprintf(c,"./na_bias/Na_scan_%d.dat",run_number);
    if(NULL==(fbias=fopen(c,"w"))){
      printf("no output file exist \n");
      exit(1);
    }
    printf("OPEN : %s \n", c);
    sprintf(c,"./na_bias/Na_scan_6000keV_%d.dat",run_number);
    if(NULL==(fbias2=fopen(c,"w"))){
      printf("no output file exist \n");
      exit(1);
    }
    printf("OPEN : %s \n", c);
    //  }

  sprintf(c,"./pol_moni/pmoni%d.dat",run_number);
  if(NULL==(fmoni=fopen(c,"w"))){
    printf("no output file exist \n");
    exit(1);
  }
  printf("OPEN : %s \n", c);

  ul=0;
  ur=0;
  dl=0;
  dr=0;

  ul2=0;
  ur2=0;
  dl2=0;
  dr2=0;

  beta_r_p_gateGe_6000kev=0.;
  beta_l_p_gateGe_6000kev=0.;
  beta_r_n_gateGe_6000kev=0.;
  beta_l_n_gateGe_6000kev=0.;

  beta_r_p_gateGe_1500kev=0.;
  beta_l_p_gateGe_1500kev=0.;
  beta_r_n_gateGe_1500kev=0.;
  beta_l_n_gateGe_1500kev=0.;

  beta_r_p_gateGe_1500kev_2=0.;
  beta_l_p_gateGe_1500kev_2=0.;
  beta_r_n_gateGe_1500kev_2=0.;
  beta_l_n_gateGe_1500kev_2=0.;

  al_cps=0;
  al_pps=0;  
  al_cps_tmp=0;
  al_pps_tmp=0;  
  beta_rate=0;  
  beta_rate_raw=0;  

  asym=0;
  asym_err=0;
  asym2=0;
  asym_err2=0;
  asym_gate6000kev=0; asym_err_gate6000kev=0;
  asym_gate1500kev=0; asym_err_gate1500kev=0;

  lr_ratio=0;
  ud_ratio=0;

  lr_gated6000kev_ratio=0;

  beta_r_gateGe_6000kev=0;
  beta_l_gateGe_6000kev=0;
  beta_r_gateGe_1500kev=0;
  beta_l_gateGe_1500kev=0;
  beta_u_gateGe_1500kev=0;
  beta_d_gateGe_1500kev=0;
  
  helicity_current=5;
  na_bias_current=500.;
  na_set_bias_cur=-500.;
  na_set_bias_old=-500.;
  gbl_CYCLE_N =0;
  flip = epics_flag = FALSE;


  if(debug)printf("begin_of_run: starting\n");
  /* Get current Trigger settings */

  size = sizeof(ts);
  status = db_get_record(hDB, hTSKey, &ts, &size, 0);
  if (status != DB_SUCCESS) 
    {
      cm_msg(MERROR, "begin_of_run", "cannot retrieve Trigger/Settings");
      return DB_NO_ACCESS;
    }
  if(debug)
    {  
      printf("ts.scan_enabled=%d\n",ts.scan_enabled);
      printf("ts.flip_helicity=%d\n",ts.flip_helicity);
    }
  debug = ts.debug;

  printf("debug=%d\n",debug);

#ifdef EPICS_ACCESS
  if (ts.flip_helicity)  // Flipping Helicity
    {
      flip = TRUE;
      printf("\nbegin_of_run: EPICS Helicity will be flipped\n");
    }

/*
    Epics Scan : Scanning NaCell ?
  */
  if (ts.scan_enabled)  // NaCell scan
    {
      epics_flag = TRUE; // indicates NaCell scan
      printf("begin_run: EPICS NaCell will be scanned\n");
    }

  //Set initial helicity state  
  status = set_init_hel_state(flip,HEL_DOWN);

  if(debug)printf("BOR: set helicity Down\n");


  if(flip || epics_flag)
    {
      if(ts.dwell_time__sec_ <=0)
	{
	  cm_msg(MERROR,"begin_of_run","helicity period must be > 0");
	  return DB_INVALID_PARAM;
	}
    }
#endif // EPICS_ACCESS

  cam_inhibit_clear(CRATE);

  /* Disable LAM & Clear ADC */
  for (n=ADC1_N ; n< ADC1_N + N_ADC; n++) {
    camc(CRATE, n, 0, 24);
    camc(CRATE, n, 0, 9);
  }

#ifdef TDC1_N
  /* Disable LAM & Clear TDC */
  for (n=TDC1_N ; n< TDC1_N + N_TDC; n++) {
    camc(CRATE, n, 0, 24);
    camc(CRATE, n, 0, 9); 
  }
#endif // TDC1_N
  
#ifdef SCL1_N  
  for(n=0;n<32;n++) {
    scaler[n]=0;
    dscaler[n]=0;
 }
  counter = 0;
  counter2 = 0;
  /* clear scalers */
  for (n=SCL1_N; n < SCL1_N + N_SCL; n++) {
    camc_sa(CRATE, n, 0, 9, 16); 
  }
  /* clear sum scalers */
  for( i=0; i<A_SCL*N_SCL; i++){
    scaler[i]=0;
    dscaler[i]=0;
  }
#endif // SCL1_N
  
#define AD413A
#ifdef AD413A
        for (n=AGE1_N/2 ; n< AGE1_N/2 + N_AGE; n++) {

//
//    B9  256*1  zero suppression 1:disable
//    B10 512*1  ECL  1:disable(CAMAC enable)
//    B11 1024*0
//    B12 2048*0
//    B13 4096*0 coincidence select 0: coincidence (1: singles)
//    B14 8192*1 CAMAC access 1:enable
//    B15 16384*0 LAM 0:disable
//    B16 32768*1 Overflow 1:not suppressyon (0: suppression)

//   B1  enable gate1 1:disable
//   B2  enable gate2 1:disable
//   B3  enable gate3 1:disable
//   B4  enable gate4 1:disable
//   B5  enable gate1 0:enable

#define B1  (1<<0)
#define B2  (1<<1)
#define B3  (1<<2)
#define B4  (1<<3)
#define B5  (1<<4)
#define B6  (1<<5)
#define B7  (1<<6)
#define B8  (1<<7)
#define B9  (1<<8)
#define B10 (1<<9)
#define B11 (1<<10)
#define B12 (1<<11)
#define B13 (1<<12)
#define B14 (1<<13)
#define B15 (1<<14)
#define B16 (1<<15)

                camc(CRATE, 2*n, 0, 9); 

                camo(CRATE, 2*n, 0, 16,256*1+512*1+4096*1+8192*1+32768*1); 

                //camo(CRATE, 2*n, 1, 16,1*0+2*0+4*0+8*0+16*1); 
		//		camo(CRATE, 2*n, 1, 16,0); 

		//cam16o(CRATE, 2*n, 0, 16, B9|B10|B13|B14|B15); // FIXME: KO
		cam16o(CRATE, 2*n, 1, 16, B1|B2|B3|B4); // FIXME: KO

                cami(CRATE, 2*n, 0, 0,&wtmp);
                printf("Control Reg1: %x\n",wtmp);
                wtmp=0;
                cami(CRATE, 2*n, 1, 0,&wtmp);

		//while (1)
		//xpoll(CRATE, 2*n);
        }

                printf("Control Reg2: %x\n",wtmp);
                wtmp=0;
                camo(CRATE, 8, 0, 17, 25); 
                camo(CRATE, 8, 1, 17, 25); 
		//                camo(CRATE, 8, 2, 17, 22); 
		camo(CRATE, 8, 2, 17, 25); 
                camo(CRATE, 8, 3, 17, 25); 
		//                camc(CRATE, n, 0, 24);
                cami(CRATE, 8, 0, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 8, 1, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 8, 2, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 8, 3, 1,&wtmp);
                printf("threshold: %x\n",wtmp);

                printf("Control Reg2: %x\n",wtmp);
                wtmp=0;
                camo(CRATE, 10, 0, 17, 25); 
		//                camo(CRATE, 10, 1, 17, 25); 
                camo(CRATE, 10, 1, 17, 30); 
		camo(CRATE, 10, 2, 17, 21); 
                camo(CRATE, 10, 3, 17, 15); 
		//                camc(CRATE, n, 0, 24);
                cami(CRATE, 10, 0, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 10, 1, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 10, 2, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 10, 3, 1,&wtmp);
                printf("threshold: %x\n",wtmp);

                printf("Control Reg2: %x\n",wtmp);
                wtmp=0;
                camo(CRATE, 12, 0, 17, 15); 
                camo(CRATE, 12, 1, 17, 21); 
                camo(CRATE, 12, 2, 17, 25); 
		camo(CRATE, 12, 3, 17, 25); 
		//                camc(CRATE, n, 0, 24);
                cami(CRATE, 12, 0, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 12, 1, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 12, 2, 1,&wtmp);
                printf("threshold: %x\n",wtmp);
                cami(CRATE, 12, 3, 1,&wtmp);
                printf("threshold: %x\n",wtmp);

#endif   // AD413A

   /* NTDC set */
   camo(CRATE,NTDC_N,0,16,NTD_TIMEUNIT);
   cam16i(CRATE,NTDC_N,1,0,&wtmp);
   printf("setunit:%x readunit:%x\n", NTD_TIMEUNIT, wtmp);

  /* Enable THE LAM station LAM_N */
  camc(CRATE, LAM_N, 0, 26);

  /* Clear LAM */
  camc(CRATE, LAM_N, 0,9);

  /* Enable CC LAM */
  cam_lam_enable(CRATE, LAM_N);

#ifdef EPICS_ACCESS
 // NaCell scan enabled?
 if(epics_flag)
   {
     epics_params.NaCell_flag = TRUE;
              
     /*
       Test access, checks parameters,
       and attempts to set NaCell close to first value
       - call with flip = false always (one scan only for Osaka).
     */
     status = EpicsInit( FALSE, &epics_params ); /* Initialize direct access to Epics device */
     if (status !=SUCCESS) // if NaCell not working, comment this
       return(status); //  if NaCell not working, comment this
     gbl_inc_cntr = epics_params.Epics_ninc; /* set to max to satisfy cycle_start routine */
  
     /* set to start value */
     epics_params.Epics_val =epics_params.Epics_start;

     na_bias_current=epics_params.Epics_start;
     na_set_bias_cur=epics_params.Epics_val;
     na_set_bias_old=epics_params.Epics_val;
     printf("na_bias STRAT, cur, old : %f\n", na_bias_current,na_set_bias_cur,na_set_bias_old);

     epics_params.Epics_last_value =  epics_params.Epics_read;
     ///printf("BOR Setting NaCell to %.2f\n", epics_params.Epics_val);
     status = EpicsIncr( &epics_params );/// if NaCell not working, comment this
     if(status != SUCCESS)/// if NaCell not working, comment this
       return (status);/// if NaCell not working, comment this
     
   }
 else
   if(debug)printf("begin_of_run: NaCell scan not enabled \n");
 

 if(helicity_read() != SUCCESS)
   {
     cm_msg(MERROR,"begin_of_run","error from helicity_read");
     return status;
   }

 if(debug)printf("BOR: required helicity  0x%x, read 0x%x\n",
	HEL_DOWN,gbl_read_hel);
 if(gbl_read_hel != HEL_DOWN)
   {
     if(debug)printf("begin_of_run: waiting for helicity to change\n");
     ss_sleep(3000); // waiting 3s
     helicity_read();
     if(gbl_read_hel != HEL_DOWN)
       {
	 printf("begin_of_run: WARNING helicity may not be in initial state\n");
       }
   }
#endif // EPICS_ACCESS

 run_status = 1;
 if(flip || epics_flag)
   {
     printf("begin_of_run: calling cycle_start\n");
     cycle_start(); // start the first cycle
     camo(CRATE, IO_N, 0, 16, 0x0002);
     camo(CRATE, IO_N, 0, 16, RUN_STOP);
     camo(CRATE, IO_N, 0, 16, RUN_BEGIN);
     camo(CRATE, IO_N, 0, 16, RUN_GATE);
     printf("flip or scan mode ON\n");
   }
 else
   { // free-running; no helicity flipping, no scan
     
     /* Clear Scaler, Open Run Gate, EOB */
     /* camo(CRATE, IO_N, 0, 16, 0x0FFF);*/
     //        camo(CRATE, IO_N, 0, 16, (CL_BOR | PL_EOB));
     
     camo(CRATE, IO_N, 0, 16, 0x0002);
     camo(CRATE, IO_N, 0, 16, RUN_STOP);
     camo(CRATE, IO_N, 0, 16, RUN_BEGIN);
     camo(CRATE, IO_N, 0, 16, RUN_GATE);
     printf("flip and scan OFF\n");
   }
 printf("Begin_of_run: done\n");
 return CM_SUCCESS;
}


/*--------- set initial helicity state -----------------------------*/
INT set_init_hel_state(BOOL flip, INT hel_state)
{
  BOOL tmp;
  
  if(helicity_read() != SUCCESS)
    return FE_ERR_HW;  // returns SUCCESS fills gbl_read_hel
  if(gbl_read_hel != hel_state)
    {
      flip_helicity(); // waits until hel is flipped
    }
  if(debug)printf("set_init_hel_state: gbl_set_hel = %d\n",
	 gbl_set_hel);
  return SUCCESS;
}

/*---------- stop the run -----------------------------*/
INT stop_run(void)
{
  INT status;
  char str[80];
  printf("stop_run: attempting to stop the run\n");
  status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0);
  if(status != SUCCESS)printf("stop_run: %s (%d)\n",str,status);
  return status;
}

/*---------- start the cycle ---------------------------*/
INT cycle_start(void)
{
#ifdef EPICS_ACCESS
  // called if epic_flag or flip are true; start another cycle

  BOOL epics_same_value=FALSE;
  float rvalue;
  INT old_hel;
  BOOL first_cycle;

  first_cycle=FALSE;
  gbl_CYCLE_N++;  /* Count cycle from 1 on */
  if(debug)
    printf("cycle_start: starting with gbl_CYCLE_N=%d\n",gbl_CYCLE_N);

  /* Note - first time through, gbl_cycle_N is now 1 */  
 
  chk_pause(); // pause the hardware while we flip the helicity etc.
  old_hel = gbl_set_hel; // state of previous cycle (HEL_DOWN at BOR)

  if(epics_flag) // we are doing an EPICS scan; need to set or increment EPICS value
    {

      /* ---------------------------------------------------------
	 Epics scan (NaCell) 
	 ---------------------------------------------------------*/
        if(debug)printf("gbl_inc_cntr=%d  epics_params.Epics_ninc=%d\n",
	     gbl_inc_cntr,  epics_params.Epics_ninc);
      if(gbl_inc_cntr >=  epics_params.Epics_ninc)  
	{     
	  if(gbl_CYCLE_N !=1)
	    {
	      if(flip && old_hel==HEL_DOWN)goto cont; // need to continue
 
	      cm_msg(MINFO,"cycle_start","NaCell scan is finished; stopping run");
	      /*  E N D   O F  S C A N */
	      status=stop_run();
	      return status; // do not restart
	    }
	  else
	    {
	      if(debug)printf("cycle_start: first cycle; NaCell will not be incremented\n");
	      // First Cycles is starting
	      gbl_inc_cntr = 1; // set to one for next time
	      // we don't want to change NaCell voltage or flip helicity
	      first_cycle=TRUE;
	    }

	} /* E N D  of First Cycle for Epics */
    } // end of NaCell scan

 cont:
  if(!first_cycle)
    {
      if(flip)
	{
	  /* flip the helicity after each cycle */
	  
	  flip_helicity(); /* flip the helicity */
	}
      
      if(epics_flag)
	{
	  if(flip && old_hel == HEL_DOWN) // incr NaCell on HEL UP only
	    {
	      if(debug)
		printf("NOT incrementing NaCell; flip is TRUE and old_hel=HEL_DOWN\n");
	    }
	  else
	    {
	      if(debug)printf("incrementing NaCell:  epics_params.Epics_inc=%.2f gbl_inc_cntr=%d\n",
		       epics_params.Epics_inc, gbl_inc_cntr);	 
	      epics_params.Epics_val = epics_params.Epics_start + 
		( gbl_inc_cntr  * epics_params.Epics_inc);
	      
	      gbl_inc_cntr ++; 

	      na_set_bias_cur = epics_params.Epics_val;
	    
	      if(debug)printf("set value is %.2f\n", epics_params.Epics_val);
	      status = set_epics_incr(); // if NaCell not working, comment this
	      if(status != SUCCESS)   
		{
		  cm_msg(MERROR,"cycle_start", "cannot increment NaCell");
		  return status; /* should stop the run */
		}
	    }
	} // end of epics_flag
    }// end of first_cycle
  
  rvalue=-1;
  if(read_epics_val(&rvalue) != SUCCESS)
    printf("cycle_start: error reading epics val\n");

  printf("****************************\n");


  na_bias_current=rvalue;

  printf("NA_BIAS_CURRENT : %f\n", na_bias_current);
 
  helicity_read();

  helicity_current =gbl_read_hel;
  heli_moni=(float)gbl_read_hel;
  printf("CURRENT HELICITY : %d \n",helicity_current);

  if(debug)printf ("\n");  
  if(epics_flag)
    printf("+++++ cycle_start: Cycle %d hel %d NaSet %f NaRead %f flip=%d +++++\n", 
	   gbl_CYCLE_N,  gbl_set_hel, 
	   epics_params.Epics_val,rvalue,flip);
  else
    printf("+++++ cycle_start: Cycle%d hel %d flip=%d ++++++\n", 
	   gbl_CYCLE_N,  gbl_set_hel,flip);

  chk_resume(); // Enable CAMAC again after flip and/or Epics increment
#endif // EPICS_ACCESS
  
  return CM_SUCCESS;
}


/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  /* Close Run Gate */
  camo(CRATE, IO_N, 0, 16, RUN_END);
  camo(CRATE, IO_N, 0, 16, RUN_STOP);
  /* Disable CC LAM */
  cam_lam_disable(CRATE, LAM_N);
  cam_inhibit_set(CRATE);  
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  camo(CRATE, IO_N, 0, 16, RUN_PAUSE);
  camo(CRATE, IO_N, 0, 16, RUN_STOP);
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  camo(CRATE, IO_N, 0, 16, RUN_RESUME);
  camo(CRATE, IO_N, 0, 16, RUN_GATE);
  return CM_SUCCESS;
}

/*-- Pause the hardware  ----------------------------------------------------*/

INT chk_pause()
{
  /* Pause the CAMAC acquistion by hardware 
       (while the helicity is flipped) 
  */   
  if(run_status)
    {
      if(ts.camac_pause_enabled)
	{
	  camo(CRATE, IO_N, 0, 16, RUN_PAUSE);
	  camo(CRATE, IO_N, 0, 16, RUN_STOP);
	}
    }  
  return CM_SUCCESS;
}

/*-- Resume the hardware  ----------------------------------------------------*/

INT chk_resume()
{
  /* Resume the CAMAC acquistion by hardware 
       (after the helicity is flipped) 
  */
  if(run_status)
    {
      if(epics_flag || flip)
	my_dwell_time=ss_time(); // start timing
      if(ts.camac_pause_enabled)
	{
	  camo(CRATE, IO_N, 0, 16, RUN_RESUME);
	  camo(CRATE, IO_N, 0, 16, RUN_GATE);
	}
    }
  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
#if 0
  /* Execute s worm run acrossthe 24bit of a given CAMAC station
     (Dataway display).
     requires CRATE and SLOT to run.
  */
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  static DWORD _ddd=0x1;
  static DWORD _up=1;

  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  if (_up==1) {
    if ((_ddd==0x2) || (_ddd==0x6) || (_ddd==0xe) || (_ddd==0x1e) )
      _ddd |= 0x1;
    
    cam24o(CRATE,DISPLAY_N,0,16,_ddd);
    _ddd <<= 1;
    if (_ddd > 0x1F000000)
      _up=0;
  }
  else
  {
    _ddd >>= 1;
    cam24o(CRATE,DISPLAY_N,0,16,_ddd);
    if (_ddd==0) {
      _up=1;
      _ddd =0x1;
    }
  }
#endif
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int   i;
  DWORD lam;
  
  for (i=0 ; i<count ; i++)
  {
    cam_lam_read(CRATE, &lam);
    //printf("source: 0x%x, lam: 0x%x\n", source, lam);
    
    if (lam & (1<<(LAM_N-1)))
      if (!test)
        return TRUE;
  }
  return FALSE;
}

/*-- Interrupt configuration for trigger event ---------------------*/

INT interrupt_configure(INT cmd, PTYPE adr)
{
#ifdef OS_VXWORKS
  switch(cmd)
    {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_INSTALL:
      break;
    case CMD_INTERRUPT_DEINSTALL:
      break;
    }
#endif
  return CM_SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{
  WORD *pbkdat;
  INT   n,a;
  INT pattern[2];
  WORD itmp;
  float end_value;
  bk_init(pevent);


//#if defined(EPICS_ACCESS)
  // only send HELC bank if helicity flipping is set
//  if(flip)
//    {
//      if(helicity_read() == SUCCESS)
//	itmp = (WORD)gbl_read_hel;
//      else
//	itmp=5;  // error

//      itmp = helicity_current;
//      if(helicity_current != gbl_set_hel)
//      if(gbl_read_hel != gbl_set_hel)
//	printf("read_trigger_event: helicity mismatch\n");

      /*---- event by event helicity ----*/
      bk_create(pevent, "HELC", TID_WORD, &pbkdat);
      *pbkdat=helicity_current;
      pbkdat++;
//      *pbkdat=(WORD)gbl_set_hel;
//      pbkdat++;
      bk_close(pevent, pbkdat);
//    }
//  else {
//    /*---- event by event helicity ----*/
//    bk_create(pevent, "HELC", TID_WORD, &pbkdat);
//    *pbkdat=5;
//    pbkdat++;
//    bk_close(pevent, pbkdat);
//  }

// only send EVOL bank if NaCell scan is enabled
//  if(epics_flag) // NaCell scan
//    {
//      status = read_epics_val( &end_value);
//      if(status != SUCCESS)
//	end_value = 900; // error
      /*---- event by event EVOL ----*/
      bk_create(pevent, "EVOL", TID_WORD, &pbkdat);
/*      *pbkdat= (WORD)(end_value * 1000); //readback  this is a float 
      pbkdat++;
      *pbkdat= (WORD)( epics_params.Epics_val * 1000); //set value (float)
      pbkdat++; */
//      *pbkdat= (WORD)( epics_params.Epics_val); //set value (float)
      *pbkdat= (WORD)(na_bias_current); //set value (float)
      pbkdat++;
      bk_close(pevent, pbkdat);
//    }
//  else {
//      /*---- event by event EVOL ----*/
//      bk_create(pevent, "EVOL", TID_WORD, &pbkdat);
//      *pbkdat= 500;
//      pbkdat++;
//      bk_close(pevent, pbkdat);
//  }
//
//#endif // EPICS_ACCESS

#if 1
  /*---- Read & Clear GE ADCs ----*/
  bk_create(pevent, "ADGE", TID_WORD, &pbkdat);
  WORD *pp;
  pp = pbkdat;
  for (n=AGE1_N/2; n < AGE1_N/2 + N_AGE ; n++) {
    int slot = 2*n;
    int i, x, q;
    WORD xlam;
    cam16i_sa(CRATE, 2*n, 0, 2, &pbkdat, 4);
    camc(CRATE, slot, 0, 9);
  }
  //  cam16i_sa(CRATE, AGE1_N, 0, 2, &pbkdat, 4);
  //  cam16i_sa(CRATE, AGE1_N+2, 0, 2, &pbkdat, 4);
  //  cam16i_sa(CRATE, AGE1_N+4, 0, 2, &pbkdat, 3);
  //  camc(CRATE, AGE1_N, 0, 9);
  //  camc(CRATE, AGE1_N+2, 0, 9);  
  //  camc(CRATE, AGE1_N+4, 0, 9);

  float al_e=0, eff=0.00205;
  if(pp[5]>0){
    al_e=0.776087*(pp[5])-1.5028;
    //if((1170 < al_e)&&(1176 > al_e)){
    //    printf("pp[5],al_e %d %f\n", pp[5], al_e);
    // if((1776 < al_e)&&(1782 > al_e)){               // 28AL
      if((1270.5 < al_e)&&(1276.5 > al_e)){       // 29AL 
      al_cps_tmp+=1;
    }
    al_pps_tmp=al_cps_tmp/eff;
  }

  float age_r_ene=0, age_l_ene=0;
  float age_u_ene=0, age_d_ene=0;
  if(pp[0]>0) age_r_ene=(float)pp[0]*1.729005+9.646886;
  if(pp[2]>0) age_l_ene=(float)pp[2]*1.896668+2.421867;
  if(pp[5]>0) age_u_ene=0.776087*(float)pp[5]-1.5028;
  if(pp[9]>0) age_d_ene=0.872052*(float)pp[9]+5.288038;

  bk_close(pevent, pbkdat);

#endif

#ifdef TDC1_N
  /*---- Read TDCs ----*/
  bk_create(pevent, "TDCS", TID_WORD, &pbkdat);
  //  for (n=TDC1_N; n < TDC1_N + N_TDC ; n++) {
  //    cam16i_sa(CRATE, n, 0, 0, &pbkdat, 8);
  //  }
  cam16i_sa(CRATE, TDC1_N, 0, 0, &pbkdat, 8);
  cam16i_sa(CRATE, TDC1_N+1, 0, 0, &pbkdat, 8);
  cam16i_sa(CRATE, TDC1_N+2, 0, 0, &pbkdat, 8);
  cam16i_sa(CRATE, TDC1_N+3, 0, 0, &pbkdat, 4);
  bk_close(pevent, pbkdat);
#endif // TDC1_N

#ifdef ADC1_N
  /*---- Read ADCs ----*/
  bk_create(pevent, "ADCS", TID_WORD, &pbkdat);
  pp = pbkdat;
  //  for (n=ADC1_N; n < ADC1_N + N_ADC; n++) {
  //    cam16i_sa(CRATE, n, 0, 0, &pbkdat, 12);
  //  }
  cam16i_sa(CRATE, ADC1_N, 0, 0, &pbkdat, 12);
  cam16i_sa(CRATE, ADC1_N+1, 0, 0, &pbkdat, 6);

  WORD qdc_r_a, qdc_r_b, qdc_l_a, qdc_l_b;
  WORD qdc_u_a, qdc_u_b, qdc_d_a, qdc_d_b;
  qdc_r_a=pp[0];
  qdc_r_b=pp[1];
  qdc_l_a=pp[2];
  qdc_l_b=pp[3];
  qdc_u_a=pp[6];
  qdc_u_b=pp[7];
  qdc_d_a=pp[14];
  qdc_d_b=pp[15];

  if((qdc_r_a>100)&&(qdc_r_b>100)&&(age_r_ene>6000)){
    beta_r_gateGe_6000kev +=1;
    if(helicity_current==1) beta_r_p_gateGe_6000kev+=1;
    if(helicity_current==0) beta_r_n_gateGe_6000kev+=1;
  }
  if((qdc_l_a>100)&&(qdc_l_b>100)&&(age_l_ene>6000)){
    beta_l_gateGe_6000kev +=1;
    if(helicity_current==1) beta_l_p_gateGe_6000kev+=1;
    if(helicity_current==0) beta_l_n_gateGe_6000kev+=1;
  }

  if((qdc_r_a>100)&&(qdc_r_b>100)&&(age_r_ene>1500)){
    beta_r_gateGe_1500kev +=1;
    if(helicity_current==1) beta_r_p_gateGe_1500kev+=1;
    if(helicity_current==0) beta_r_n_gateGe_1500kev+=1;
    if(helicity_current==1) beta_r_p_gateGe_1500kev_2+=1;
    if(helicity_current==0) beta_r_n_gateGe_1500kev_2+=1;
  }
  if((qdc_l_a>100)&&(qdc_l_b>100)&&(age_l_ene>1500)){
    beta_l_gateGe_1500kev +=1;
    if(helicity_current==1) beta_l_p_gateGe_1500kev+=1;
    if(helicity_current==0) beta_l_n_gateGe_1500kev+=1;
    if(helicity_current==1) beta_l_p_gateGe_1500kev_2+=1;
    if(helicity_current==0) beta_l_n_gateGe_1500kev_2+=1;
  }

  if((qdc_u_a>100)&&(qdc_u_b>100)&&(age_u_ene>1500)){
    beta_u_gateGe_1500kev +=1;
  }
  if((qdc_d_a>100)&&(qdc_d_b>100)&&(age_d_ene>1500)){
    beta_d_gateGe_1500kev +=1;
  }

  bk_close(pevent, pbkdat);
#endif //ADC1_N

  /*---- Read Coincidence Register ----*/
  bk_create(pevent, "COIS", TID_WORD, &pbkdat);
  cam16i(CRATE, 4, 0, 0, pbkdat);
  pbkdat++;
  cam16i(CRATE, 5, 0, 0, pbkdat);
  pbkdat++;
  bk_close(pevent, pbkdat);

  /*---- Read NTDC ----*/
  bk_create(pevent, "NTDC", TID_WORD, &pbkdat);
  cam16i(CRATE, NTDC_N, 0, 0, pbkdat);
  pbkdat++;
  bk_close(pevent, pbkdat);

  /* Clear C212 & Clear CC LAM*/
  camc (CRATE, LAM_N, 0 , 9);
  cam_lam_clear(CRATE, LAM_N);

  /* Reset Latch (EOB) */
  camo(CRATE, IO_N, 0, 16, PL_EOB);
  camo(CRATE, IO_N, 0, 16, RUN_STOP);

  /* END OF EVENT */
  return bk_size(pevent);
}

/*-- Scaler event --------------------------------------------------*/

INT read_scaler_event(char *pevent, INT off)
{
  DWORD *pbkdat,*pbktmp, n,i;

  
#ifdef SCL1_N
  
  bk_init(pevent);
  /*---- USER Stuff ----*/
  /* Inhibit the scalers for proper sync read */
  //camo(CRATE,IO_N,0,16,(RUN_GATE|INH_SCL));
  
  bk_create(pevent, "SCLR", TID_DWORD, &pbkdat);
  pbktmp=pbkdat;
  for (n=SCL1_N; n < SCL1_N + N_SCL ; n++) {
    cam24i_sa(CRATE, n, 0, 0, &pbkdat, 16);
  }
  
  // printf("al scaler %d \n", al_scaler);
  // time=clock/10000.;
  // al_scaler_rate = al_scaler/time;

  /* Clear all scaler by hardware */
  //camo(CRATE,IO_N,0,16,(RUN_GATE|INH_SCL|CLR_SCL));

  /* Remove Inhibit on scalers */
  //camo(CRATE,IO_N,0,16,RUN_GATE);

 
  /* calculate some values to send to the control room via EPICS 
      i.e. (fe_epics, epics slow control program)
  */

  //  printf("scaler event %d\n",counter);

  for(i=0;i<32;i++)
    {
      dscaler[i]=pbktmp[i]-scaler[i];
      if(dscaler[i]<0) dscaler[i]+=16777216;
      scaler[i]=pbktmp[i];
      //      printf("scaler event %d, %d\n",i, scaler[i]);
    }

  //    printf("scaler 1\n");

  test=dscaler[0];

  //LR ratio
  //  if(dscaler[17]!=0){
  //    lr_ratio=(double)dscaler[16]/(double)dscaler[17];
  // }
  if(beta_l_gateGe_1500kev!=0){
    lr_ratio=beta_r_gateGe_1500kev/beta_l_gateGe_1500kev;
  }
  if(beta_l_gateGe_6000kev!=0){
    lr_gated6000kev_ratio=beta_r_gateGe_6000kev/beta_l_gateGe_6000kev;
    beta_r_gateGe_6000kev=0; beta_l_gateGe_6000kev=0;
  }
  // UD ratio   
  //  if(dscaler[23]!=0){
  //    ud_ratio=(double)dscaler[19]/(double)dscaler[23];
  //  }
  if(beta_d_gateGe_1500kev!=0){
    ud_ratio=beta_u_gateGe_1500kev/beta_d_gateGe_1500kev;
  }

  if(dscaler[25]!=0){
    //    beta_rate=(float)(dscaler[16]+dscaler[17]+dscaler[19]+dscaler[23])/((float)dscaler[25]/10000.f);
    beta_rate=(float)(beta_r_gateGe_1500kev+beta_l_gateGe_1500kev+beta_u_gateGe_1500kev+beta_d_gateGe_1500kev)/((float)dscaler[25]/10000.f);
    beta_r_gateGe_1500kev=0; beta_l_gateGe_1500kev=0; beta_u_gateGe_1500kev=0; beta_d_gateGe_1500kev=0;
  }

  if(dscaler[25]!=0){
    beta_rate_raw=(float)(dscaler[16]+dscaler[17]+dscaler[19]+dscaler[23])/((float)dscaler[25]/10000.f);
  }

  if(dscaler[25]!=0){
    //    printf("al_cps_tmp, al_pps_tmp %f %f\n",al_cps_tmp, al_pps_tmp);
    al_cps = al_cps_tmp/((float)dscaler[25]/10000.f);
    al_pps = al_pps_tmp/((float)dscaler[25]/10000.f);
  }

  //   printf("scaler 2\n");

  if(helicity_current==1){  // helON is true
    ur+=dscaler[16];
    ul+=dscaler[17];
    ur2+=dscaler[16];
    ul2+=dscaler[17];
  }else if(helicity_current==0) { // helOFF is true
    dr+=dscaler[16];
    dl+=dscaler[17];
    dr2+=dscaler[16];
    dl2+=dscaler[17];
  }
  //  printf("ur,ul,ur2,ul2, %d  %d  %d  %d\n",ur,ul,ur2,ul2);
  //  printf("dr,dl,dr2,dl2, %d  %d  %d  %d\n",dr,dl,dr2,dl2);

  //  printf("scaler 3\n");

  if(counter==600){
  //    if(counter==30){
    //    asym=pol_cal(ur,ul,dr,dl);
    //    asym_err=pol_cal_err(ur,ul,dr,dl);
    asym=pol_cal(beta_r_p_gateGe_1500kev_2,beta_l_p_gateGe_1500kev_2,beta_r_n_gateGe_1500kev_2,beta_l_p_gateGe_1500kev_2);
    asym_err=pol_cal_err(beta_r_p_gateGe_1500kev_2,beta_l_p_gateGe_1500kev_2,beta_r_n_gateGe_1500kev_2,beta_l_p_gateGe_1500kev_2);
    printf("counter %d, asymmetry : %f +- %f \n",counter2,asym,asym_err);
    //    asym=5.3;asym_err=2.2;
    fprintf(fmoni,"%d %f %f\n",counter2,asym,asym_err);
    if((EOF==(fflush(fmoni)))){
      printf("failed fflush  \n");
      exit(1);
    }
    counter2+=1;
    counter = 0;
    ul=0; 
    ur=0;
    dl=0;
    dr=0;

    beta_r_p_gateGe_1500kev_2=0.;
    beta_l_p_gateGe_1500kev_2=0.;
    beta_r_n_gateGe_1500kev_2=0.;
    beta_l_n_gateGe_1500kev_2=0.;

    }
  counter += 1;

  //    printf("scaler 4\n");

  if(epics_flag){
    //    printf("epics_flag = TRUE \n");
    //    printf("na_bias current, old %f, %f\n",na_set_bias_cur,na_set_bias_old);
    if(na_set_bias_cur!=na_set_bias_old){
      //      asym2=pol_cal(ur2,ul2,dr2,dl2);
      //      asym_err2=pol_cal_err(ur2,ul2,dr2,dl2);
      asym_gate1500kev=pol_cal(beta_r_p_gateGe_1500kev,beta_l_p_gateGe_1500kev,beta_r_n_gateGe_1500kev,beta_l_p_gateGe_1500kev);
      asym_err_gate1500kev=pol_cal_err(beta_r_p_gateGe_1500kev,beta_l_p_gateGe_1500kev,beta_r_n_gateGe_1500kev,beta_l_p_gateGe_1500kev);

      asym_gate6000kev=pol_cal(beta_r_p_gateGe_6000kev,beta_l_p_gateGe_6000kev,beta_r_n_gateGe_6000kev,beta_l_p_gateGe_6000kev);
      asym_err_gate6000kev=pol_cal_err(beta_r_p_gateGe_6000kev,beta_l_p_gateGe_6000kev,beta_r_n_gateGe_6000kev,beta_l_p_gateGe_6000kev);
      printf("Na bias scan mode\n");
      printf("Gated 1500keV, Na bias %f [V], asymmetry : %f +- %f \n",na_set_bias_old,asym_gate1500kev,asym_err_gate1500kev);
      fprintf(fbias,"%f %f %f\n",na_set_bias_old,asym_gate1500kev,asym_err_gate1500kev);
      if((EOF==(fflush(fbias)))){
          printf("failed fflush  \n");
       exit(1);
      }
      printf("Gated 6000keV, Na bias %f [V], asymmetry : %f +- %f \n",na_set_bias_old,asym_gate6000kev,asym_err_gate6000kev);
      fprintf(fbias2,"%f %f %f\n",na_set_bias_old,asym_gate6000kev,asym_err_gate6000kev);
      if((EOF==(fflush(fbias2)))){
          printf("failed fflush  \n");
       exit(1);
      }
      ul2=0; 
      ur2=0;
      dl2=0;
      dr2=0;

      beta_r_p_gateGe_6000kev=0.;
      beta_l_p_gateGe_6000kev=0.;
      beta_r_n_gateGe_6000kev=0.;
      beta_l_n_gateGe_6000kev=0.;
      
      beta_r_p_gateGe_1500kev=0.;
      beta_l_p_gateGe_1500kev=0.;
      beta_r_n_gateGe_1500kev=0.;
      beta_l_n_gateGe_1500kev=0.;

      na_set_bias_old=na_set_bias_cur;
    }
  }

  //    printf("scaler 5\n");

  bk_close(pevent, pbkdat);

  //  printf("scaler 6\n");  

  fe_epics_write(); // write these values for the control room

  //    printf("scaler 7\n");  

#endif // SCL_1N   

#ifdef EPICS_ACCESS
  if(flip || epics_flag)
    {
      if(debug)printf("read_scaler_event: checking if cycle is done\n");
      if(my_dwell_time > 0)
	{
	  if(debug)printf("read_scaler_event: ss_time-my_dwell_time = %d ts.dwell_time__sec_=%d\n", 
			  (ss_time() - my_dwell_time), ts.dwell_time__sec_);
	  
	  if (  (ss_time() - my_dwell_time) >= ts.dwell_time__sec_)
	    {
	      if(debug)printf("read_scaler_event: end of cycle detected, calling cycle_start\n");
	      /* end of cycle */
	      
	      my_dwell_time=0; // clear this
	      cycle_start(); // new cycle
	    }
	}
      else
	printf("read_scaler_event: Cannot restart cycle (my_dwell_time=0)\n");
    }
  
#endif  // EPICS_ACCESS
  
#ifdef SCL1_N // SCL_1N
  if(debug)printf("read_scaler_event: returning size=%d\n", bk_size(pevent));
  return  bk_size(pevent);
#else
  return 0;
#endif
}



//---------------------------------------------------------------------
INT fe_epics_write(void)
{
  /* Write a demand values to the epics equipment demand -> the control room  
     hEPD is a key to "/equipment/epics/variables/demand"

  */

  INT index,status;
  char str[]="/equipment/epics/variables/demand";

  /* write these values to EPICS equipment demand (handle hEPD) */

  //  printf("fe_epics_write: starting\n");
  if(hEPD)
    {
      index=0;
      //      printf(" fe_epics_write: writing beta_rate=%f to index %d\n",beta_rate,index);
      status = db_set_data_index(hDB, hEPD, &beta_rate, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;
      
      index++;

      status = db_set_data_index(hDB, hEPD, &lr_ratio, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;

      status = db_set_data_index(hDB, hEPD, &ud_ratio, sizeof(float),  index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;

       status =  db_set_data_index(hDB, hEPD, &al_cps, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;
      al_cps_tmp=0.;

      index++;
      status =  db_set_data_index(hDB, hEPD, &al_pps, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;
      al_pps_tmp=0.;

      index++;
      status =   db_set_data_index(hDB, hEPD, &asym, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &asym_err, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &test, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &heli_moni, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &na_bias_current, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &lr_gated6000kev_ratio, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

      index++;
      status =   db_set_data_index(hDB, hEPD, &beta_rate_raw, sizeof(float), index, TID_FLOAT);
      if(status != DB_SUCCESS)goto err;

    }
  else
    cm_msg(MINFO,"fe_epics_write","cannot write demand values to %s. No handle",str);

  return SUCCESS;
  
 err:
  cm_msg(MINFO,"write_epics","error writing scaler value  to \"%s\" at index=%d (%d)",
	 index,str,status);
  return status;
}



/*------------------------------------------------------------------------*/
INT  helicity_write(INT gbl_set_hel)
/*------------------------------------------------------------------------*/
{
  /* Write the Epics ILE2:POLSW2:DRVON or DRVOFF using channel access routines 

  NOTE: both gbl_read_hel (value read from EPICS) and 
             gbl_set_hel (Value set) are globals


	  status = SUCCESS  successfully wrote HEL
	           FE_ERR_HW could not read HEL from epics

		   if gbl_set_hel=1  write to helOn
		                  0           helOff
*/
  INT status;
  const float value=1.0;

#ifdef EPICS_ACCESS
  /* write helicity via direct channel access */

  set_long_watchdog(50000);

  if(gbl_set_hel==1) // set helicity ON 
    status = ChannelWatchdog(WhelOn, &Wchid_helOn);
  else
    status = ChannelWatchdog(WhelOff, &Wchid_helOff);

  if (status != SUCCESS )
    {
      printf("helicity_write: cannot reconnect channel; cannot write helicity\n"); 
      return status;
    }

  // value is always 1 - write 1 to helOn or helOff

  if(gbl_set_hel==1)
    { // we want helicity to be set to ON
      if(debug)printf("writing %f to Wchid_helOn=%d(%s); gbl_set_hel=%d read_hel=%d \n",
	     value,Wchid_helOn,WhelOn,gbl_set_hel,gbl_read_hel);
    status = write_Epics_chan( Wchid_helOn, value);
    }
  else
    { // we want helicity to be set to OFF
      if(debug)printf("writing %f to Wchid_helOff=%d(%s); gbl_set_hel=%d read_hel=%d\n",
	     value,Wchid_helOff,WhelOff,gbl_set_hel,gbl_read_hel);
      status =  write_Epics_chan ( Wchid_helOff, value);
    }

  if(debug)printf("helicity_write: write_Epics_chan returns with status=%d\n",status); // 0=success
  
  restore_watchdog();
  if(status != SUCCESS)
    return(status); /* cannot read helicity */

#else
  /* no EPICS access */
  printf("helicity_write: Helicity state not available from direct Epics Access\n");
  gbl_read_hel=gbl_set_hel; // testing 
#endif  /* no EPICS access */

  return SUCCESS; 
}


/*------------------------------------------------------------------------*/
INT helicity_read(void)
/*------------------------------------------------------------------------*/
{
  /* Read the Epics ILE2:POLSW2:STATON using channel access routines 

  NOTE: both gbl_read_hel (value read from EPICS) and 
             gbl_set_hel (Value set) are globals

	  fills gbl_read_hel and gbl_set_hel, returns STATUS
	  status = SUCCESS  successfully read HEL, values agree
	           FE_ERR_HW could not read HEL from epics
		  
*/
  float value;
  INT i,status;
  

#ifdef EPICS_ACCESS
  /* read helicity via direct channel access */

  //  Hel_last_time = ss_time(); /* we are about to read helicity... reset watchdog to keep Epics live  */
  value  = -1;
  i=0;
  if(debug)printf("helicity_read: starting with chid=%d name=%s \n",Rchid_helOn,RhelOn);
  set_long_watchdog(50000);
  while(i < 2)
    {
      if(debug)printf("calling read_Epics_chan with chid=%d,(%s)\n",Rchid_helOn,RhelOn); 
      status = read_Epics_chan(Rchid_helOn,  &value);
      if(debug)
	  printf("read_Epics_chan returns with status=%d and value=%f\n",status,value);
      if(status == SUCCESS) break;

      if(debug)printf("calling ChannelReconnect for helicity \n");
      status = ChannelReconnect(RhelOn); // reconnect
      if(status != -1 )
	Rchid_helOn = status; // new chid
      else
	{
	  restore_watchdog();
	  return(FE_ERR_HW); /* cannot read helicity */
	}
      i++;
    }
  restore_watchdog();
  if(status != SUCCESS)
    { 
      cm_msg(MERROR,"helicity_read","error reading helicity from Epics");
      return FE_ERR_HW;
    }

  if(debug)
    printf("After read_Epics_chan helicity value = %f\n",value);
  gbl_read_hel = (INT)value;

#else
  /* no EPICS access */
  printf("helicity_read: Helicity state not available from direct Epics Access\n");
  gbl_read_hel=gbl_set_hel; // testing 
#endif  /* no EPICS access */

  return SUCCESS; 
}


/*--------------------------------------------------------------*/
BOOL flip_helicity(void)
/*--------------------------------------------------------------*/
{
  INT old_hel;
  INT i,repeat;
  float ftime;
  /* flip the requested helicity */
  if(helicity_read() != SUCCESS)
    return FALSE;  // returns FALSE
  if(debug)printf("flip_helicity: read_helicity as %d\n",gbl_read_hel);
  old_hel = gbl_read_hel; // state of helOn
  if (old_hel==1)
    gbl_set_hel=0; // set hel off
  else
    gbl_set_hel=1; // set hel on

#ifdef EPICS_ACCESS
  helicity_write(gbl_set_hel); // state required
#endif

  i=repeat=0;
  while(gbl_set_hel != gbl_read_hel)
    {
      if(debug)printf("flip_helicity: sleeping %d ms\n",ts.helicity_sleep__ms_);
      ss_sleep(ts.helicity_sleep__ms_); // sleep for helicity sleep time (ss_sleep takes ms)

  // No good reading helicity immediately; needs time to respond

      if(helicity_read() != SUCCESS)
	return FE_ERR_HW;  // returns SUCCESS fills gbl_read_hel
      i++;
      if(i> 5)
	{
	  if(repeat)return FALSE; 
	  repeat=TRUE;
	  printf("flip_helicity: hel state not changed after %d sec; sending another helicity_write\n",
		 i*ts.helicity_sleep__ms_);
	  helicity_write(gbl_set_hel); // state required
	  i=0; // wait again
	}
    }
  if(repeat)i+=5;
  ftime=i*ts.helicity_sleep__ms_/1000;
  if(debug)printf("flip_helicity: successfully flipped helicity from %d to %d (after %.1f ms)\n",
	 old_hel,gbl_read_hel,ftime);
  //  helicity_current =gbl_read_hel;
  //  printf("1. CURRENT HELICITY : %d \n",helicity_current);
  return TRUE; /* returns true (hel_flipped)  */
  
}

/*-----------------------------------------------------*/
INT restore_watchdog(void)
/*-----------------------------------------------------*/
{
  /* restore the midas watchdog to the original value 
   */
  DWORD my_timeout;

  if(!gbl_dachshund_flag)
    {  /* check flag */
      printf("restore_watchdog: set_long_watchdog has not been called previously\n");
      return SUCCESS;
    }
  /* temp check... */
  cm_get_watchdog_params(&gbl_watchdog_flag, &my_timeout);
    if(debug) 
    printf("restore_watchdog: watchdog value is presently %d; restoring it to %d; gbl_watchdog_flag=%d\n",
	 my_timeout, gbl_watchdog_timeout, gbl_watchdog_flag);
  cm_set_watchdog_params(gbl_watchdog_flag, gbl_watchdog_timeout);  /* e.g. 5 min for reconnect = 5*60*1000ms */
  gbl_dachshund_flag = FALSE;
  return SUCCESS;
}

/*-----------------------------------------------------*/
INT set_long_watchdog(DWORD my_watchdog_timeout)
/*-----------------------------------------------------*/
{
 /* Set a long midas watchdog timer 
    Parameter  my_watchdog_timeout in ms 
  */
  DWORD k9;




  if(gbl_dachshund_flag)
    {  /* check flag */
      cm_get_watchdog_params(&gbl_watchdog_flag, &k9);
      printf("set_long_watchdog: long watchdog is set already, currently watchdog is set to %d\n",k9);
      return SUCCESS;
    }

  cm_set_watchdog_params(gbl_watchdog_flag, my_watchdog_timeout );
   if(debug ) 
    printf("set_long_watchdog:  gbl_watchdog_timeout=%d; set timeout to %d; \n",
	   gbl_watchdog_timeout, my_watchdog_timeout);
  
  gbl_dachshund_flag = TRUE;
  return SUCCESS;
}

 
#ifdef EPICS_ACCESS
/*----------------------------------------------------------------------------*/
INT epics_reconnect(void)
/*----------------------------------------------------------------------------*/
{
  
  /*  set long watchdog before calling EpicsReconnect */
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */
 INT status;
  

  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */

 
  //printf("calling EpicsReconnect\n");
  status=EpicsReconnect(&epics_params); /* reconnect the scan device */
  //printf("after Reconnect, status = %d\n",status);
  
  restore_watchdog(); /* restore watchdog timeout */
  //cm_set_watchdog_params(watchdog_flag, watchdog_timeout); /* restore watchdog timeout */
  
  if(status != SUCCESS)
   {
        cm_msg(MERROR,"epics_reconnect", "cannot connect to Epics device %s",
	      ts.epics_scan_device);
     
     return(FE_ERR_HW);
   }
  else    
    return SUCCESS;
}


/*-----------------------------------------------------------------------*/
INT set_epics_val(void)
/*-----------------------------------------------------------------------*/
{
  INT status;
  //  BOOL watchdog_flag;
  // DWORD watchdog_timeout;
  INT ncounts;

  /* Set the Epics Scan device to the value  epics_params.Epics_val
     Usually called at the beginning of a Scan  */

  epics_params.Epics_bad_flag = 0;
     
  // EpicsNewScan involves  waiting so set watchdog for 3 min 
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);   5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */

  ncounts=0;
  while (ncounts < 10)
    {
      /* Note: EpicsNewScan sets Epics device, reads it back and checks correct value is set.

	   - it is called EpicsNewScan because it is called at the beginning of a scan to set a value (unless
           helicity is flipped -> scan direction reversed).
           At the beginning of a scan Epics device  will be set to 1 increment different from start value
           to give device time to settle (there may be big change in value). Then EpicsIncr will be called to set
           device to correct value.

      */

      status = EpicsNewScan( &epics_params ); /* set Epics value to  epics_params.Epics_val */
					      
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout);  restore watchdog timeout */
	  return status;
	}
      if(status == -1)
	{
	  /* hardware error */ 
	  cm_msg(MERROR,"set_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      printf("\n  *** set_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     ts.epics_scan_device);
	      restore_watchdog(); /* restore watchdog timeout */
	      return(FE_ERR_HW);
	    }
	  
	}
      else if (status == -2)
	{
	  cm_msg(MERROR,"set_epics_val","Epics step parameters are outside max or min value allowed");
	  restore_watchdog(); /* restore watchdog timeout */
	  return (FE_ERR_HW);
	}
      else if (status == DB_NO_ACCESS)
	{
	  cm_msg(MERROR,"set_epics_val","no access to scan device. Check device is available and switched on");
	  restore_watchdog(); /* restore watchdog timeout */
	  return(FE_ERR_HW);
	}
      
      printf("set_epics_val:  waiting 10s then retrying.... \n");
      jwait(10); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout);  restore watchdog timeout */
  
  printf("set_epics_val:  Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);
  return (FE_ERR_HW);
}


/*-------------------------------------------------------------------------*/
INT read_epics_val(float *pval )
/*-------------------------------------------------------------------------*/
{
  INT status;
  /*  BOOL watchdog_flag;
      DWORD watchdog_timeout; */
  INT ncounts;

  /* Read the Epics Scan device into the value  epics_params.Epics_read */

  epics_params.Epics_bad_flag = 0;

  /* EpicsRead may involve waiting if we have to reconnect so set watchdog for 5 min */
  /*  cm_get_watchdog_params(&watchdog_flag, &watchdog_timeout);
  cm_set_watchdog_params(watchdog_flag, 300000);   5 min for reconnect 5*60*1000 */
  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  ncounts=0;

  while (ncounts < 10)
    {
      status = EpicsRead( pval, &epics_params ); /* read value from Epics */
      if(status == SUCCESS) 
	{
	  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout);  restore watchdog timeout */
	  return status;
	}
      
  
      if(status == -1)
	{
	  /* no connection or timeout */ 
	  cm_msg(MERROR,"read_epics_val","trying to reconnect to Epics \n");
	  status=EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      printf("\n  *** read_epics_val: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     ts.epics_scan_device);
	      restore_watchdog(); /* restore watchdog timeout */
	      return(FE_ERR_HW);
	    }
	  
	}
      
      printf("read_epics_val:  waiting 10s then retrying.... \n");
      jwait(10); /* wait 10s */
      ncounts++;
    }

  restore_watchdog(); /* restore watchdog timeout */
/*cm_set_watchdog_params(watchdog_flag, watchdog_timeout);  restore watchdog timeout */
  
  printf("read_epics_val: Epics Hardware is not responding after %d retries ... make sure device is switched on \n",ncounts);
  return (FE_ERR_HW);
}


/*-----------------------------------------------------------------------*/
INT set_epics_incr(void)
/*-----------------------------------------------------------------------*/
{
  /* set an epics value
     retry on failure
     stop the run after n retries
  */
  INT status;
  INT num_retries=2;
  /*  BOOL watchdog_flag;
  DWORD watchdog_timeout;
  */
  epics_params.Epics_bad_flag = 0;
  /* retrying can take some time so set watchdog for 5 min */

  set_long_watchdog( 300000);  /* 5 min for reconnect 5*60*1000 */
  status = EpicsIncr( &epics_params );
  if(status == SUCCESS)
    {
      restore_watchdog(); /* restore watchdog timeout */

      epics_params.Epics_bad_flag = 0;
      return (status);
    }
  else
    {
      if(status == -1 )
	{
	  /* serious error from reading/writing Epics device 
	     try to reconnect  */
	  status = EpicsReconnect(&epics_params);
	  if(status != SUCCESS)
	    {
	      printf("\n  *** set_epics_incr: Error  setting %s. Stop & restart run after checking Epics device *** \n",
		     ts.epics_scan_device);
	      restore_watchdog(); /* restore watchdog timeout */
	      return(FE_ERR_HW);
	    }
	}
      epics_params.Epics_bad_flag++;
    }
  printf("set_epics_incr: Waiting 0.5s ..... then rechecking Epics voltage \n");
  ss_sleep(500);
  cm_yield(100);
  while(epics_params.Epics_bad_flag < num_retries) 
    {
      status = EpicsCheck( &epics_params);  /* Resends the set point */
      if(status==SUCCESS)
	{
	  if(d5)printf("set_epics_incr: success from  EpicsCheck after %d failures\n",epics_params.Epics_bad_flag);
	  epics_params.Epics_bad_flag = 0;
	  restore_watchdog(); /* restore watchdog timeout */
	  return SUCCESS;
	}	
      else if (status == -1)
	{
	  cm_msg(MERROR, "set_epics_incr","Error accessing Epics device");
	  return FE_ERR_HW;
	}
      else if (status == -2)
	{ /* this should be picked up in earlier checks */
	  cm_msg(MERROR, "set_epics_incr","Invalid set value outside min/max range ");
	     restore_watchdog(); /* restore watchdog timeout */
	  return FE_ERR_HW;
	}

      epics_params.Epics_bad_flag++; /*   increment epics_params.Epics_bad_flag */
      printf("set_epics_incr: waiting 3s...... then retrying (attempt %d)\n",
	     epics_params.Epics_bad_flag);
      jwait(3);
    } /* while ends */
  
  /* cannot set Epics voltage */
     restore_watchdog(); /* restore watchdog timeout */
  printf("\n  *** set_epics_incr: Given up trying to set %s. Stop & restart run after checking Epics device *** \n",
	 ts.epics_scan_device);

  return(FE_ERR_HW);
}




void jwait(int sec)
{
  ss_sleep (sec*1000); // ms
  return;
}

#include "Osaka_epics.c"

#endif // EPICS_ACCESS



// end file
