/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for Frequency Synthesizer Control Module
 *
 * Author: Suzannah Daviel, TRIUMF
 *---------------------------------------------------------------------------*
 * CVS log information:
 *$Log$
 *Revision 1.2  2000/11/28 18:14:25  renee
 *Add MEMORY_LAST
 *
 *---------------------------------------------------------------------------*/
#ifndef _FSC_INCLUDE_
#define _FSC_INCLUDE_

#include "stdio.h"
#include "string.h"

#if defined( _MSC_VER )
#define INLINE __inline
#elif defined(__GNUC__)
#define INLINE __inline__
#else 
#define INLINE
#endif

#define EXTERNAL extern 

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED

typedef unsigned short int WORD;
typedef unsigned char      BYTE;
typedef int                 INT;

#ifdef __alpha
typedef unsigned int       DWORD;
#else
typedef unsigned long int  DWORD;
#endif

#endif /* MIDAS_TYPE_DEFINED */

#ifdef PPCxxx
#define A16            0xfbff0000
#define A24	       0xfa000000              /* A24 VME access for ppc */
#else
#define A24	       0xf0000000              /* MVME162 A24 VME access */
#endif

#define FSC_BASE 0x800000 /* set with Jumpers A17-23 on FSC board */

/* FSC register offset defines  

          register        offset        bits     access     
            name                        used     type        */
#define MEMORY_BASE          0x00000   /* 16     word    RW  */
#define MEMORY_LAST          0x0FFFC   /* 16     word    RW  */
#define UP_DOWN_INT          0x10000   /* 14     word    RW  */
#define UP_DOWN_PAGE         0x10002   /*  5     byte    RW  */
#define INT_FREQ_SET_HI      0x10004   /* 16     word    RW  */ 
#define INT_FREQ_SET_LO      0x10006   /* 16     word    RW  */ 
#define MEM_ADDR_SEL_SRC     0x10008   /*  1     byte    RW  */
#define FREQ_SETP_SEL_SRC    0x10009   /*  1     byte    RW  */ 
#define READ_EXT_MEM_ADDR    0x1000A   /* 14     word    RO  */
#define READ_FREQ_SETP_HI    0x1000C   /* 16     word    RO  */
#define READ_FREQ_SETP_LO    0x1000E   /* 16     word    RO  */

#define FREQ_BUF_LTCH_DLY    0x10010   /*  8     byte    RW  */
#define FREQ_SYN_LTCH_DLY    0x10011   /*  8     byte    RW  */
#define FREQ_BUF_LTCH_CNTL   0x10013   /*  1     byte    RW  */ 
#define FREQ_SYN_LTCH_CNTL   0x10012   /*  1     byte    RW  */ 
#define FREQ_SYN_LTCH_MASK   0x10014   /*  5     byte    RW  */ 
#define VME_STROBE_CTRL      0x10015   /*  4     byte    RW  */ 
#define FREQ_SYN_RMTE_ENABLE 0x10016   /*  1     byte    RW  */ 
#define FREQ_SYN_STATUS      0x10017   /*  2     byte    RO  */ 
#define VME_RESET_STATUS     0x10018   /*  1     byte    RW  */ 
#define SEND_VME_RESET       0x10019   /*  8     byte    WO  */
#define SEND_INT_STROBE      0x1001A   /*  8     byte    WO  */

/* bit masks for above registers */
#define UP_DOWN_INT_MSK         0x3FFF   /* 14 bits */
#define UP_DOWN_PAGE_MSK        0x001F   /* 5  bits */
#define INT_FREQ_SET_HI_MSK     0xFFFF   /* 16 bits */
#define INT_FREQ_SET_LO_MSK     0xFFFF   /* 16 bits  */ 
#define TOGL_MEM_ADDR_SEL_MSK   0x0001   /*  1 bit  */
#define TOGL_FREQ_SETP_SEL_MSK  0x0001   /*  1 bit  */ 
#define READ_EXT_MEM_ADDR_MSK   0x3FFF   /* 14 bits  */
#define READ_FREQ_SETP_HI_MSK   0xFFFF   /* 16 bits  */
#define READ_FREQ_SETP_LO_MSK   0xFFFF   /* 16 bits  */
#define FREQ_BUF_LTCH_DLY_MSK   0x00FF   /*  8  bits  */
#define FREQ_SYN_LTCH_DLY_MSK   0x00FF   /*  8  bits  */
#define TOGL_FREQ_BUF_LTCH_MSK  0x0001   /*  1  bits  */ 
#define TOGL_FREQ_SYN_LTCH_MSK  0x0001   /*  1  bits  */ 
#define FREQ_SYN_LTCH_MASK_MSK  0x001F   /*  5  bits  */ 
#define VME_STROBE_CTRL_MSK     0x000F   /*  4  bits  */ 
#define TOGL_FREQ_SYN_RMTE_MSK  0x0001   /*  1  bits  */ 
#define FREQ_SYN_STATUS_MSK     0x0003   /*  2  bits  */ 
#define VME_RESET_STATUS_MSK    0x0001   /*  1  bits  */ 
#define MAX_FREQ_RANGE          15999999
/* trFSC.c prototypes:    */
INLINE INT  fscLoad(const DWORD base_adr, char * file, DWORD *lastBCDfreq);
INLINE INT  fscLoad_one(const DWORD base_adr, DWORD infreq, DWORD memadd);
INLINE INT  fscRegRead(const DWORD base_adr, DWORD reg_offset);
INLINE INT  fscRegWrite(const DWORD base_adr, DWORD reg_offset, const WORD value);
INLINE void fscFrqWrite(const DWORD base_adr, DWORD freq);
INLINE void fscMemWrite(const DWORD base_adr, DWORD *srce, const DWORD length);
INLINE void fscMemRead(const DWORD base_adr, DWORD *pdest, DWORD length);
INLINE INT  fscRegWrite8(const DWORD base_adr, const DWORD reg_offset, const BYTE value);
INLINE INT  fscRegRead8(const DWORD base_adr, DWORD reg_offset);
INT fscMemReset(const DWORD base_adr);
void fscInit2A(const DWORD base_adr);
void fscInit1A(const DWORD base_adr);
INT  fscFrq2BCD(DWORD fi, DWORD *fo);
#endif



