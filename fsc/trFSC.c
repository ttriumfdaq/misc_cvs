/*------------------------------------------------------------------*
 * trFSC.c 
 * Procedures for handling the Frequency Synthesizer Control board
 * This is a VME board specified by Syd Kreitzman - TRIUMF
 * designed and produced by Triumf electronics group - Hubert Hui
 *
 * CVS log information:
 *$Log$
 *Revision 1.7  2003/01/16 21:46:22  suz
 *Renee? changed debug ddd to fdd
 *
 *Revision 1.6  2001/10/23 21:33:51  suz
 *add delay & input->fscinput
 *
 *Revision 1.5  2001/08/02 19:01:22  suz
 *changes to allow last freq to be checked
 *
 *Revision 1.3  2000/12/01 21:21:32  renee
 *add mode 0A functions
 *
 *Revision 1.2  2000/11/28 18:16:32  renee
 *When loading the memory from a file, copy the freq in the 1st memory location to the last memory location
 *
 *------------------------------------------------------------------*/
#include "trFSC.h"

int fdd = 0;
/*------------------------------------------------------------------*/
void fsc(void)
{
   printf("Frequency Synthesizer control\n");
   printf("inline: fscRegWrite8(fsc_base, reg, value)\n");
   printf("inline: reg = fscRegRead8(fsc_base, reg)\n");
   printf("inline: fscRegWrite(fsc_base, reg, value)\n");
   printf("inline: reg = fscRegRead(fsc_base, reg)\n");
   printf("inline: fscLoad(fsc_base, file, *lastBCDfreq)\n");
   printf("inline: fscLoad_one(fsc_base, infreq, memadd)\n");
   printf("inline: fscSetInt(fsc_base, infreq)\n");
   printf("inline: fscFrqLoad(fsc_base, freq)\n");
   printf("inline: fscMemWrite(fsc_base, *srce, len[WORD])\n");
   printf("inline: fscMemRead(fsc_base, *dest, len[WORD])\n");
   printf("inline: fscFrq2BCD(infreq, *outfreq)\n");
   printf("inline: fscInit0A(fsc_base)\n");
   printf("inline: fscInit1A(fsc_base)\n");
   printf("inline: fscInit2A(fsc_base)\n");
   printf("inline: fscMemReset(fsc_base)\n");
}

/*------------------------------------------------------------------*/
/** fscInit2A
    Initialize the FSC for the experiment 2A
    @memo Init FSC for 2A.
    @param base FSC VME base address
    @return void
*/
void fscInit2A(const DWORD base_adr)
{
  fscRegWrite (base_adr, UP_DOWN_INT, 0xffff);

  fscRegWrite8(base_adr, MEM_ADDR_SEL_SRC,  0x0);
  /* the 14bit up/dpwn counter selects the memory address */
  fscRegWrite8(base_adr, FREQ_SETP_SEL_SRC, 0x1);
  /* the output frequency comes from the memory */
  fscRegWrite8(base_adr, FREQ_BUF_LTCH_CNTL, 0x0);
  /* above line puts the FREQ BUFFER in latch mode */
  fscRegWrite8(base_adr, FREQ_SYN_LTCH_CNTL, 0x1);
  fscRegWrite8(base_adr, FREQ_SYN_LTCH_MASK, 0xff);
  /* above 2 lines enables x-28 drv control (for coherent freq strobing), 
     pass thru on individual decades of the freq synth 
  */
  fscRegWrite8(base_adr, VME_STROBE_CTRL,    0x9);
  /* bit 0 VME count up
     bit 1 U/D contrl via VME 
     bit 2 U/D strobe Enabled 
     bit 3 External Strobe Enabled
     bit 4 External Preset Enabled
  */
}
/*------------------------------------------------------------------*/
/** fscInit1A
    Initialize the FSC for the experiment 1A
    @memo Init FSC for 1A.
    @param base FSC VME base address
    @return void
*/
void fscInit1A(const DWORD base_adr)
{
  /*  fscRegWrite (base_adr, SEND_VME_RESET, 0x0);*/ /* reset fsc to power up state */
  /* If you want to SEND_VME_RESET, you have to put some wait time otherwise the 
     following commands arrive too fast and do not get executed */
  fscRegWrite8(base_adr, MEM_ADDR_SEL_SRC,  0x1);
  fscRegWrite8(base_adr, FREQ_SETP_SEL_SRC, 0x1);
  fscRegWrite (base_adr, UP_DOWN_PAGE, 0x0);
  /* Change requested by Syd 01/08/05 ; was 0, now is 1*/
  fscRegWrite8(base_adr, FREQ_BUF_LTCH_CNTL, 0x1);
  /* latch the freq output buffer with strobe pulse */
  /* Change requested by Syd 01/08/05 ; was 1, now is 3*/
  fscRegWrite8(base_adr, FREQ_SYN_LTCH_CNTL, 0x3);
  /* enable x-28 drv control to freq synth, 
     pass through on rest if FREQ_SYN_LTCH_MASKs are high 
  */
  fscRegWrite8(base_adr, FREQ_SYN_LTCH_MASK, 0xff);
  fscRegWrite8(base_adr, VME_STROBE_CTRL,    0x1D);
  /* bit 0 VME count dowm
     bit 1 U/D contrl via VME 
     bit 2 U/D strobe Disabled 
     bit 3 External Strobe Enabled
     bit 4 External Preset Disabled
  */
}
/*------------------------------------------------------------------*/
/** fscInit0A
    Initialize the FSC for the experiment 0A
    @memo Init FSC for 0A.
    @param base FSC VME base address
    @return void
*/
void fscInit0A(const DWORD base_adr)
{
  /*  fscRegWrite (base_adr, SEND_VME_RESET, 0x0);*/ 
  /* reset fsc to power up state */
  /* If you want to SEND_VME_RESET, you have to put 
     some wait time otherwise the 
     following commands arrive too fast and do not get executed */
  fscRegWrite8(base_adr, FREQ_SETP_SEL_SRC, 0x0);
  fscRegWrite (base_adr, UP_DOWN_PAGE, 0x0);
  fscRegWrite8(base_adr, FREQ_BUF_LTCH_CNTL, 0x1);
  fscRegWrite8(base_adr, FREQ_SYN_LTCH_CNTL, 0x3);
  /**
    Next line turns on the F/S strobing for all decades that
    have their F/S Latch Nask set high (which is all of them by def.)
  */  
  /*  fscRegWrite8(base_adr, FREQ_SYN_LTCH_CNTL, 0x0); enable FSC strobing */

  fscRegWrite8(base_adr, FREQ_SYN_LTCH_MASK, 0x1f);
}

/*------------------------------------------------------------------*/
/** fscMemReset
    Reset memory pointer for the frequency table
    @memo Reset memory pointer.
    @param base FSC VME base address
    @return 1 SUCCESS -1 FAIL
*/
INT fscMemReset(const DWORD base_adr)
{
  INT data;
  data = fscRegWrite (base_adr, UP_DOWN_INT, 0xffff);
  /* check value read back is as expected */
  if (data != 0x3fff) /* bits 14,15 read back 0 */
    return -1 ;
  return 1 ;
}

/*------------------------------------------------------------------*/
/** fscFrq2BCD
    Convert frequency to HEX and BCD code.
    @memo convert freq.
    @param fi decimal frequency in Hz
    @param fo Coded frequency
    @return 1=SUCCESS, -1=Error
*/
INT fscFrq2BCD(DWORD fi, DWORD *fo)
{
  int top;
  char s[16], sh[8], sl[16];
  int len;

  *fo = 0;
  sprintf(s,"%d",fi);
  if ((top = (strlen(s) - 7)) > 0)
  {
    strncpy(sh, s, top);
    sh[top] = 0;
    if ((*fo = strtoul(sh, NULL, 10)) > 0xf)
    {
      printf("Number too big (%ld) ... fscFrqWrite() aborted\n",fi);
      return -1;
    }
    *fo <<= 28;
  }
  else
    top = 0;

  len=strlen(s)-top;
  /* if(fdd) printf("s=%s, top=%d, strlen(s)=%d, len=%d\n",s,top,strlen(s),len); */

  strncpy(sl, &s[top], len);
  /* the following line is wrong - it leaves an extra spurious digit
     sl[strlen(s)] = 0;  */
  sl[len]=0; /* replace by this line */
  *fo |= strtoul(sl, NULL, 16);

  if (fdd) printf("len: %d freq:%s f:0x%08x\n",strlen(s), s, *fo);

  return 1;
}

/*------------------------------------------------------------------*/
/** fscLoad
    Load frequency list from file.
    @memo Load frequency table.
    @param base\_adr FSC VME base address
    @param file frequency file
    @param returns last loaded frequency in BCD
    @return 1=SUCCESS, -1=File not found
*/
INLINE INT fscLoad(const DWORD base_adr, char * file , DWORD *lastBCDfreq)
{
  volatile WORD * spec_Adr;
  FILE  *fscinput;
  DWORD  infreq, outfreq;
  INT    status;
  DWORD  first_outfreq;

  fscinput = fopen(file,"r");
  ss_sleep(50);
  if(fscinput == NULL){
    printf("FSC table file %s could not be opened.\n", file); 
    return -1;    
  }

  /* The first frequency should be loaded in the last memory 
     address as well as in the 1st memory address */
  status = fscanf(fscinput,"%d", &infreq);
  status = fscFrq2BCD(infreq, &outfreq);
  if (status < 0)
    return status;
  if(fdd)  printf("First freq= %d (0x%x); BCD  (0x%x)\n",infreq,infreq,outfreq);
  first_outfreq = outfreq; /* remember this value */
  /* first freq in last mem address */
  spec_Adr = (WORD *)(A24 + base_adr + MEMORY_LAST);
  *spec_Adr++ = *((WORD *) &outfreq);
  *spec_Adr++ = *((WORD *) &outfreq + 1);

  /* first freq in first mem address */
  spec_Adr = (WORD *)(A24 + base_adr + MEMORY_BASE);
  *spec_Adr++ = *((WORD *) &outfreq);
  *spec_Adr++ = *((WORD *) &outfreq + 1);


  /* other freq */
  while (fscanf(fscinput,"%d", &infreq) != EOF)
    {
      status = fscFrq2BCD(infreq, &outfreq);
      if (status < 0)
	return status;
      *spec_Adr++ = *((WORD *) &outfreq);
      *spec_Adr++ = *((WORD *) &outfreq + 1);
    }
  if(fdd) printf("Last freq %d (0x%x); BCD (0x%x)\n",infreq,infreq,outfreq);
  printf("Frequency range (BCD): %x to %x \n",first_outfreq,outfreq);

  *lastBCDfreq = outfreq;  /* return this parameter */
  fclose(fscinput);
  return 1;
}
/*------------------------------------------------------------------*/
/** fscLoad_one
    Load one frequency in a given memory location.
    @memo Load one frequency in a given memory location .
    @param base\_adr FSC VME base address 
    @param frequency value
    @param relative address in FSC memory
    @return 1=SUCCESS -1=Invalid frequency
*/
INLINE INT fscLoad_one(const DWORD base_adr, DWORD infreq, DWORD memadd)
{
  volatile WORD * spec_Adr;
  DWORD  outfreq;
  INT    status;
  
 
  spec_Adr = (WORD *)(A24 + base_adr + MEMORY_BASE + memadd );

  status = fscFrq2BCD(infreq, &outfreq);
  if (status < 0)
    return status;
  *spec_Adr++ = *((WORD *) &outfreq);
  *spec_Adr++ = *((WORD *) &outfreq + 1);

  
  return 1;
}

/*------------------------------------------------------------------*/
/** fscSetInt
    Set internal frequency register.
    @memo Set internal frequency register.
    @param base\_adr FSC VME base address 
    @param frequency value
    @return 1=SUCCESS -1=Invalid frequency
*/
INLINE INT fscSetInt(const DWORD base_adr, DWORD infreq, DWORD memadd)
{
  volatile WORD * spec_Adr;
  DWORD  outfreq;
  INT    status;
  
 
  status = fscFrq2BCD(infreq, &outfreq);
  if (status < 0)
    return status;
  spec_Adr = (WORD *)(A24 + base_adr + INT_FREQ_SET_HI  );
  *spec_Adr = *((WORD *) &outfreq);
  spec_Adr = (WORD *)(A24 + base_adr + INT_FREQ_SET_LO  );
  *spec_Adr++ = *((WORD *) &outfreq + 1);

  
  return 1;
}

/*------------------------------------------------------------------*/
/** fscRegWrite
    Write 16bit to register.
    @memo 16bit write.
    @param base\_adr FSC VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
INLINE INT fscRegWrite(const DWORD base_adr, const DWORD reg_offset,
                           const WORD value)
{
  volatile WORD * spec_Adr;

  spec_Adr = (WORD *)(A24 + base_adr + reg_offset);
  *spec_Adr = value;
  return fscRegRead(base_adr, reg_offset);
}

/*------------------------------------------------------------------*/
/** fscRegWrite8
    Write 8bit to register.
    @memo 8bit write.
    @param base\_adr FSC VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
INLINE INT fscRegWrite8(const DWORD base_adr, const DWORD reg_offset,
                           const BYTE value)
{
  volatile BYTE * spec_Adr;

  spec_Adr = (BYTE *)(A24 + base_adr + reg_offset);
  *spec_Adr = value;
  return fscRegRead8(base_adr, reg_offset);
}

/*------------------------------------------------------------------*/
/** fscRegRead
    Read 16bit to register.
    @memo 16bit read.
    @param base\_adr FSC VME base address
    @param reg\_offset register offset
    @return read back value
*/
INLINE INT fscRegRead(const DWORD base_adr, DWORD reg_offset)
{
  volatile WORD * spec_Adr;

  spec_Adr = (WORD *)(A24 + base_adr + reg_offset);
  return *spec_Adr;  
}

/*------------------------------------------------------------------*/
/** fscRegRead8
    Read 8bit to register.
    @memo 8bit read.
    @param base\_adr FSC VME base address
    @param reg\_offset register offset
    @return read back value
*/
INLINE INT fscRegRead8(const DWORD base_adr, DWORD reg_offset)
{
  volatile BYTE * spec_Adr;

  spec_Adr = (BYTE *)(A24 + base_adr + reg_offset);
  return (*spec_Adr & 0xff);  
}

/*------------------------------------------------------------------*/
/** fscFrqWrite
    Load frequency register.
    @memo Load frequency register.
    @param base\_adr FSC VME base address
    @param freq frequency value in Hz
    @return void
*/
INLINE void fscFrqWrite(const DWORD base_adr, const DWORD freq)
{
  volatile WORD *spec_Adr;
  int top;
  char s[16], sh[8], sl[16];
  DWORD f = 0;

  sprintf(s,"%d",freq);
  if ((top = (strlen(s) - 7)) > 0)
  {
    strncpy(sh, s, top);
    sh[top] = 0;
    if ((f = strtoul(sh, NULL, 10)) > 0xf)
    {
      printf("Number too big (%ld) ... fscFrqWrite() aborted\n",freq);
      return ;
    }
    f <<= 28;
  }
  else
    top = 0;
  strncpy(sl, &s[top], strlen(s)-top);
  sl[strlen(s)] = 0;
  f |= strtoul(sl, NULL, 16);
  if (fdd) printf("len: %d freq:%s f:0x%010x\n",strlen(s), s, f);
  spec_Adr = (WORD *)(A24 + base_adr + INT_FREQ_SET_HI);
  *spec_Adr = *((WORD *) f);
  spec_Adr = (WORD *)(A24 + base_adr + INT_FREQ_SET_LO);
  *spec_Adr = *((WORD *) f + 1);
  return;
}

/*------------------------------------------------------------------*/
/** fscMemWrite
    Load FSC memory.
    @memo Load FSC memory.
    @param base\_adr FSC VME base address
    @param psrce source pointer
    @param length segment length in 2bytes (WORD)
    @return void
*/
INLINE void fscMemWrite(const DWORD base_adr, DWORD *psrce, const DWORD length)
{
  volatile WORD *spec_Adr, *p;
  int i;
  
  p = (WORD *) psrce;
  spec_Adr = (WORD *)(A24 + base_adr + MEMORY_BASE);
  for(i=0 ; i < length ; i++)
    *spec_Adr++ = *p++;  
  return;
}

/*------------------------------------------------------------------*/
/** fscMemRead
    Read FSC memory.
    @memo read FSC memory.
    @param base\_adr FSC VME base address
    @param pdest source pointer
    @param length segment length in 2bytes (WORD)
    @return void
*/
INLINE void fscMemRead(const DWORD base_adr, DWORD *pdest, DWORD length)
{
  volatile WORD *spec_Adr, *p;
  int i;

  p = (WORD *) pdest;
  if (length > MAX_FREQ_RANGE) length = MAX_FREQ_RANGE;
  spec_Adr = (WORD *)(A24 + base_adr + MEMORY_BASE);
  for(i=0 ; i < length ; i++)
    *p++ = *spec_Adr++;  
  return;
}










