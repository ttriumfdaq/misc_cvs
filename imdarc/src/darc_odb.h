/* darc_odb.h

   Structure to store the odb data needed for bnmr_darc

   IMPORTANT - the sizes of these parameters MUST MATCH the sizes
   in the odb PARTICULARLY where hotlinks are used (db_create_record)
   otherwise get mis-match. 

  $Log: darc_odb.h,v $
  Revision 1.1  2003/09/29 19:47:54  suz
  add IMUSR support

  Revision 1.13  2002/12/04 20:56:24  suz
  BNMR needs temperature_variable & field_variable. Remove ifdef MUSR

  Revision 1.12  2002/10/02 22:17:20  asnd
  Implement dynamic run headers for temperature and field

  Revision 1.11  2002/05/10 17:55:05  suz
  increase no. scalers from 60 to 80

  Revision 1.10  2002/04/17 21:18:45  suz
  add 2 parameters needed by musr_config

  Revision 1.9  2002/04/17 20:21:54  suz
  max scalers changed for MUSR

  Revision 1.8  2001/09/28 19:30:32  suz
  increase length of run_title

  Revision 1.7  2001/09/14 18:58:07  suz
  support MUSR with ifdef MUSR

  Revision 1.6  2001/03/29 19:49:10  suz
  Increase max. scalers from 45 to 60 for second scaler module

  Revision 1.5  2000/11/03 21:10:29  midas
  change his_total and total_save to type float from long

  Revision 1.4  2000/10/27 19:27:31  midas
  MAX_HIS increased from 4 to 16.

  Revision 1.3  2000/10/11 04:10:50  midas
  added cvs log

*/
#ifdef IMUSR
/* maximum number of IMUSR histograms */
#define MAX_IHIS 19  /* IMUSR has max. 19 fixed histograms
		        PD,clk,totalrate, 4 front , 4 back
			[ 4 aux1 4 aux2 are optional ]  */
#endif

/* Maximum number of TD-MUSR histograms */
#define MAX_HIS 16     /* change from 4 to 16 to generalize for different xpts */ 


#ifdef MUSR
#define MAX_NSCALS 16  /* maximum no. of scalers  for MUSR */
// the following needed by musr_config:
#define MAX_CHAR 10    // max charaters of histo titles
#define MAX_CNTR MAX_HIS/2     // max number of counters supported
#else  
#define MAX_NSCALS 80  /* maximum no. of scalers for BNMR  
     this value must be >=  N_SCALER_TOT in frontend code */
#endif
typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */
  //
  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[15];
  char orientation[15];
  char field[15];
  char temperature[15];
  char start_time_str[32];
  long start_time_binary;
  char stop_time_str[32];
  long stop_time_binary;
  //
  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  long ncycles;           /* not used */


    
  /* histogram information */
  long his_n;             /* number of histograms defined */
  long his_nbin;          /* number of histogram bins */
  long nchan;             /* number of scaler channels enabled */
  
  long his_bzero[MAX_HIS];     /* time zero */     
  long his_good_begin[MAX_HIS];/* first good bin */
  long his_good_end[MAX_HIS];  /* last good bin */
  long his_first_bkg[MAX_HIS]; /* first background bin */
  long his_last_bkg[MAX_HIS];  /* last background bin */
  
  char his_tits[MAX_HIS][11]; /* histogram titles */
  
  long  purge_after;          /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];


  // scalers
  long nscal; /* number of scalers in use */
  long scaler_save[MAX_NSCALS];/* contains scaler data */
  char scaler_titles[MAX_NSCALS][32];
  
  // darc writes these into odb:    
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */
  float his_total[MAX_HIS];    /* outputs his_total */
  float total_save;            /*  output  total_saved */

#ifdef IMUSR
    // IMUSR specific
    char comment1[32];
    char comment2[32];
    char comment3[32];
    char subtitle[32];
    char IMUSR_his_titles[MAX_IHIS][11]; /* histogram titles */
    float IMUSR_his_total[MAX_IHIS];
#endif
}D_ODB;







