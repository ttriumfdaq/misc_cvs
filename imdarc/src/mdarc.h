/*  mdarc.h
    common (global) variables for mdarc & bnmr_darc.c
    added imdarc support to mdarc's Rev 1.10
    
    $Log: mdarc.h,v $
    Revision 1.2  2003/09/29 21:39:15  suz
    nHistBanks replaced by nHistograms for IMUSR (less confusing)

    Revision 1.1  2003/09/29 19:48:41  suz


*/

caddr_t pHistData;

INT     nHistBins; // number of histogram bins

#ifdef IMUSR
INT nHistograms; // number of histograms (IMUSR)
#else
INT     nHistBanks; // number of histograms (TD-MUSR)
#endif

BOOL     firstxTime;

HNDLE   hDB, hKey;
BOOL    bGotEvent;
BOOL    bGotCamp;
char    feclient[32], eqp_name[32];
#ifdef IMUSR
char i_eqp_name[32];
#endif
INT     size;
char expt_name[HOST_NAME_LENGTH];

/* make the run number and run type global so
   if user changes them mid-run it is ignored. */
INT    run_number;
INT    old_run_number;
char   run_type[5];  /* test or real */
BOOL   toggle;

/* debugs */
BOOL debug ;
BOOL debug_mud;
BOOL debug_dump;
BOOL debug_check ;
INT  dump_begin;
INT  dump_length;
//BOOL timer ;    /* timing */
char perl_path[80]; /* perlscript path */

#ifdef MUSR
DWORD scaler_counts[16];
HNDLE hMusrSet;
INT db,dl;

INT nHist;
INT gbl_data_point;
INT gbl_camp_data_point;
INT numCamp;// number of camp variables enabled in /equipment/camp/settings
/* Array of camp variables from CVAR event */
double *pCampData;

/* Array of scaler totals, allocated by realloc in M2MI_expand, not freed: */
IMUSR_CAMP_VAR *campVars ;

#endif

#ifdef IMUSR
static FILE *Imusr_log_fileHandle = NULL ;
#endif

