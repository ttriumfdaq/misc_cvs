/********************************************************************\
  Name:         imdarc.c
  Created by:   
  Contents:    data logger for IMUSR (Musr DAQ) run under MIDAS on linux


  $Log: imdarc.c,v $
  Revision 1.6  2003/10/15 19:04:02  suz
  add support for imdarc stopping the run (i.e. client flags)

  Revision 1.5  2003/10/15 03:44:53  asnd
  Round data when casting to DWORD

  Revision 1.4  2003/10/07 21:46:43  suz
  save perlscript info file from being overwritten on error

  Revision 1.3  2003/10/03 10:54:58  asnd
  Get ImuSR Camp variables logged in mud file (finally)

  Revision 1.2  2003/09/29 21:50:52  suz
  bank length now depends on trigger; zero data is suppressed

  Revision 1.1  2003/09/29 19:50:02  suz
  original

\*****************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

//#include <errno.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#include "ybos.h"


//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
//#include "c_utils.h"
//#include "triumf_fmt.h"
#include "trii_fmt.h"

#include "mdarc.h" /* common header for mdarc & imusr_darc */
#include "darc_files.h" /* for msg_print only */
#include "experim.h" 
#include "run_numbers.h"
#include "client_flags.h"  // prototypes
// note caddr_t is defined as char*


/* prototypes */
INT    tr_start(INT rn, char *error);
INT    tr_stop(INT rn, char *error);

INT    setup_hotlink();
INT    setup_hot_toggle();
void   update_Time(INT , INT , void *);
void  hot_toggle (INT, INT, void * );
INT print_file(char* filename);
INT check_midas_logger(INT rn); // type 1 only
INT auto_run_number_check(INT rn);

static INT allocate_storage (void);
static void  allocate_camp_storage( void );

// IMUSR probably can't trigger a save since we have to wait for the
// event and save each time anyway - 
// but may want to manually triggers CVAR events from fe_camp
void  process_event_cvar(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader,
			 void *pevent);
void  process_event_darc(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader,
			 void *pevent);
void  process_event_camp(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader,
			 void *pevent);

INT get_client_name(char *pname);
BOOL trigger_histo_event(INT *state_running);
char ClientName[256];

// needed for MUSR save now command
// we may possibly need this for IMUSR
INT    setup_hot_save();
void   hot_save (INT, INT, void * );
HNDLE  hSave;

BOOL   saving_now; // flag needed for MUSR hotlink
// this for MDARC stopping the run
HNDLE hCF;
#ifdef MUSR
MUSR_TD_ACQ_CLIENT_FLAGS fcf;
#else
FIFO_ACQ_CLIENT_FLAGS fcf;
#endif
char client_str[132];
// end of MDARC stop run section

// globals
//          (globals shared with imusr_darc.c in mdarc.h)

float version = 1.0; /* imdarc version number - set manually to closest CVS version */ 

DWORD  time_save, last_time, start_time,stop_time;
INT    hBufEvent;
INT    request_id;
INT   status;
BOOL  mdarc_need_update;
DWORD mdarc_last_call;
INT debug_proc; // global for debug of process_event

HNDLE hMDarc; // handles for mdarc
char beamline[6];
INT run_state;
BOOL write_data; // odb  mdarc flag enable_mdarc_logging

MUSR_TD_ACQ_MDARC fmdarc;
MUSR_I_ACQ_SETTINGS imusr;
CAMP_SETTINGS camp;
HNDLE hIS, hC;




char perl_script[80]; /* get_run_number.pl  Directory now read from odb in setup_hotlink */
char outfile[]="/var/log/midas/get_run_number.txt";
char errfile[]="/var/log/midas/get_run_number.lasterr";

/* initialize values (declared in mdarc.h) */
BOOL   bGotEvent = FALSE;
double *pCampData = NULL;
IMUSR_CAMP_VAR *campVars = NULL;

//temp
caddr_t pMaxPointer;
FILE *fileH; // temp

BOOL tr_flag;
#include "client_flags.c"  // code for client_flags handling

/*----- prestart  ----------------------------------------------*/
INT tr_prestart(INT rn, char *error)
{

  int j;
  char str[128];

  /* Move the run number checking from tr_start to tr_prestart so we can be sure of stopping the
     run from starting if there is a failure 
      - still can't stop run reliably so implement client flags
*/

  printf("tr_prestart: starting\n");

  /* Use of cm_exist:

 Purpose: Check if a MIDAS client exists in current experiment

  Input:
    char   name             Client name
    BOOL   bUnique          If true, look for the exact client name.
                            If false, look for namexxx where xxx is
                            a any number

  Output:
    none

  Function value:
    CM_SUCCESS              Client found
    CM_NO_CLIENT            No client with that name


     e.g.   status=cm_exist("mdarc",TRUE);
     Compare status with CM_NO_CLIENT or CM_SUCCESS

     or call with FALSE and "febnmr"  
     e.g.   status = cm_exist("febnmr", FALSE);
     in this case it will return CM_SUCCESS for febnmr, febnmr2, febnmr1

     To check febnmr2 specifically have to specify:
            status = cm_exist("febnmr2", TRUE);

 */
  status=cm_exist("imdarc",FALSE); 
  //printf("status after cm_exist for imdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "tr_prestart","Another copy of imdarc may be already running");
      return(CM_NO_CLIENT); /* return an error */
  }
  if (rn == 0)
    printf ("tr_prestart:  this is a dummy call for initialization\n");

  // client flags
  if(rn != 0)
    {   // do on a genuine run start only
      start_time =  ss_time(); // set a timer when run was started
      tr_flag = FALSE; // and a flag for check_client_flags
      //  DO NOT CLEAR CLIENT FLAGS HERE ( this is what cleared musr_config's flag after it was set)
      //  they are cleared after a stop ready for next time.

      // get the latest value of this
      size = sizeof(fcf.enable_client_check);
      sprintf(str,"%s/enable client check",client_str);
      status = db_get_value(hDB, 0, str, &fcf.enable_client_check, &size, TID_BOOL, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_prestart","could not get value for \"%s\"  (%d)",str,status);
	  return(status);
	}
      printf("Disable Client flag check = %d\n",fcf.enable_client_check);

    }
  else
    {
      start_time = 0; // dummy value
      if(run_state == STATE_STOPPED)
	{
	  printf("tr_prestart: dummy run start, run is STOPPED, clearing client flags\n");
	  status = clear_client_flags(); // clear the client flags of all clients that use a tr_start (+ mdarc's)
	  if(status != DB_SUCCESS)
	    return status;
	}
    }  


  /* update the whole mdarc record first to reflect any changes */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_prestart","Failed to retrieve mdarc record  (%d)",status);
    write_message1(status,"tr_prestart");
    return(status);
  }

    /* Get the beamline */

  
  /* the beamline for MUSR should have been copied into mdarc/histograms/musr/beamline
     by musr_config  */


  strcpy(beamline,fmdarc.histograms.musr.beamline);
  for (j=0; j<strlen(beamline); j++)
    beamline[j] = toupper (beamline[j]); /* convert to upper case */
  if(debug) printf(" beamline:  %s\n",beamline);
  

  
  if (strlen(beamline) <= 0 )
  {
    printf("No valid beamline is supplied\n");
    return (DB_INVALID_PARAM);
  }

  /* look for automatic run number check */
  if (fmdarc.disable_run_number_check)
    {
      status = check_run_number(rn);
      if (status != DB_SUCCESS)
	return (status); // check_run_number wrote an error message
    } 
  else    /* automatic run number checking is enabled */
    {
      status = auto_run_number_check(rn);
      if (status != DB_SUCCESS)
	return (status); // auto_run_number_check failed
      
    } // end of automatic run number checking
  
} // end of prestart
  
/*----- start  ----------------------------------------------*/
INT tr_start(INT rn, char *error)
{
  char str[128];
  char string[256];
  HNDLE hRM,  hKeyTmp;
  char  cmd[132];
  int j;
  int stat;


  /* called every new run and as a dummy when imdarc is started */
  if(debug)printf("tr_start starting, setting firstxTime true\n");

  firstxTime = TRUE;  /* set global flag so imusr_darc opens camp connection on begin run  */


  /* update the imusr record to reflect any changes */
  size = sizeof(imusr);
  status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_start","Failed to retrieve imusr record  (%d)",status);
    write_message1(status,"tr_start");
    return(status);
  }

  /* update the camp record  to reflect any changes */
  size = sizeof(camp);
  status = db_get_record (hDB, hC, &camp, &size, 0);/* get the whole record for camp */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_start","Failed to retrieve camp record  (%d)",status);
    write_message1(status,"tr_start");
    return(status);
  }

  
  stop_time=0;  // initialize

  if(rn == 0)
    cm_msg(MINFO,"tr_start","Making a DUMMY call to imusr_darc for initialization");
  else
    cm_msg(MINFO,"tr_start","In the process of starting run %d",
           run_number);

  write_data = fmdarc.enable_mdarc_logging;
  if(write_data)
    {
      if(debug)printf ("tr_start: logging is enabled\n");
      
      /* set up hot link on toggle on a genuine run start (or if run is going) 
	 hot link on toggle is closed by tr_stop
      */
      if (rn != 0  ||   run_state == STATE_RUNNING )
	{
	  if(debug)printf("tr_start: Calling setup_hot_toggle\n");
	  status = setup_hot_toggle();
	  if(status!=DB_SUCCESS)
	    printf("tr_start: Failure from setup_hot_toggle. Can still save data, but toggle won't work\n");
	  
#ifndef IMUSR
	  
	  // IMUSR may not need this - leave code in in case.
	  
	  /* set up hot link on "save now" on a genuine run start (or if run is going) 
	     hot link on is closed by tr_stop
	  */
	  
	  if(debug)printf("tr_start: Calling setup_hot_save\n");
	  status = setup_hot_save();
	  if(status!=DB_SUCCESS)
	    printf("tr_start: Failure from setup_hot_save. Can still save data, but hot key won't work\n");
	  
#else
	  /* IMUSR : allocate the space for saving the data points */
	  /* Shouldn't this be inside #ifdef IMUSR ??? - yes */

	
	  status = allocate_storage(); // returns the number of bytes required for histos
	  if (pHistData == NULL)
	    {
	      cm_msg(MERROR,"tr_start","Error allocating total space for histograms (%d bytes)"
		     ,status);
	      return DB_INVALID_PARAM;
	    }

	  /* IMUSR : open or re-open the data log file */
	  
	  if (Imusr_log_fileHandle) 
	    fclose (Imusr_log_fileHandle);
	  Imusr_log_fileHandle = NULL;
	  if (*imusr.imdarc.log_file)
	    {
	      Imusr_log_fileHandle = fopen (imusr.imdarc.log_file, "w");
	      if ( Imusr_log_fileHandle == NULL ) 
		{
		  cm_msg(MERROR,"tr_start","Error opening log file %s for writing.",
			 imusr.imdarc.log_file);
		}
	      else
		printf("tr_start: successfully opened log file %s for writing \n",imusr.imdarc.log_file);
	    }
	  fileH=Imusr_log_fileHandle; // temp
#endif
	  
	}
    } /* if write_data */
  else
      printf ("tr_start: logging is currently disabled\n");
    
#ifndef IMUSR  //IMUSR may not need this
  status = get_client_name(ClientName);  // get the ACTUAL frontend client name (for trigger event)
  if(debug)printf("tr_start: client name is %s\n",ClientName);
#endif  


  /* set up hot link on febnmr client flag on a genuine run start (or if run is going) 
     hot link on client flag febnmr is closed by tr_stop
  */
  if (rn != 0  ||   run_state == STATE_RUNNING )
    {
      if(debug)printf("tr_start: Calling setup_hot_clients\n");
      stat =  setup_hot_clients();
      if (stat != DB_SUCCESS)
	{
	  return(status); // failure from hot links
	}
      
      
      /* set mdarc's client flag to SUCCESS now */
      printf("tr_start: setting mdarc client flag TRUE\n");
      fcf.mdarc=TRUE;
      size = sizeof(fcf.mdarc);
      status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_start", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}   
    }
  printf("tr_start: returning success\n");
  return DB_SUCCESS;
}

/*----- stop  ----------------------------------------------*/
INT tr_stop(INT rn, char *error)
{
  
  char str[128], filename[128];/* standard length 128 */
  INT next, nfile;
  BOOL flag;
  HNDLE hktmp;

#ifndef IMUSR
  /* remove hot link on "save now" */
  if(hSave) 
    {
      if(debug)
	printf("tr_stop: closing record for \"save now\", hSave=%d\n",hSave);
      db_close_record(hDB, hSave);
    }
#endif
  /* remove hot link on client flags while the run is off */
  
  status = close_client_hotlinks();
  if(status != DB_SUCCESS)
    printf("tr_stop: error closing client hotlinks\n");
  
  /* clear client success flags for next time */
  status = clear_client_flags();
  if(status != DB_SUCCESS)
    printf("tr_stop: error clearing client success flags\n");


  if(!write_data) return SUCCESS;

  if(debug)printf("tr_stop: In the process of stopping the run %d\n", rn);

 
  if (hBufEvent)
    {
      INT n_bytes, n_try=0;
      do
	{
	  bm_get_buffer_level(hBufEvent, &n_bytes);
	  cm_yield(100);
	} while (n_try++ < 5);
    }
  /* An event is sent by frontend at BOR - no need to trigger (MUSR) */
  if(bGotEvent)
    {
      // no need to set saving_now flag as hotlink is closed
      cm_msg(MINFO,"tr_stop","Calling imusr_darc to save the final data for this run");
      status = imusr_darc(pHistData);
    }
  else
    cm_msg(MINFO,"tr_stop","No final data available for this run (no event received from frontend)");
  
  
  /* 
     if the toggle flag is set, we are in the middle of the toggle procedure,
     and haven't saved a file with the new run number 
     - this will happen if no valid data is available  */
  
  /* check whether toggle flag is still set  */
  if(toggle)
    {
      cm_msg(MINFO,"tr_stop","Warning - toggle is set. There may not be a final saved file\n");
      
      toggle = FALSE;  /* set global toggle false */
      /* turn toggle bit off in odb  */
      size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "tr_stop", "cannot clear toggle flag ");
    
    if(debug) printf ("tr_stop:  Toggle is cleared \n");
    
    
    /* DO NOT restore the hot link on toggle  */
    
    /*    if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	  {
	  size = sizeof(fmdarc.toggle);
	  status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
	  if (status != DB_SUCCESS)
	  {
	  cm_msg(MERROR,"tr_stop","Failed to open record (hotlink) for toggle (%d)", status );
	  write_message1(status,"imusr_darc");
          return(status);
	  }      
	  }
    */
    }     // if toggle
  else   
    {
      /* remove hot link while the run is off */
      if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
	{
	  if(debug)printf("tr_stop: Closing toggle hotlink\n");
	  db_close_record(hDB, hktmp);
	}
    }


  /* kill script also sets this flag */
  if( ! fmdarc.end_of_run_purge_rename)
    {
      if(debug) printf("tr_stop: End of run purge/rename procedure flag (%d)  is FALSE\n"
		       ,fmdarc.end_of_run_purge_rename);
      cm_msg(MINFO,"tr_stop","Saved file WILL NOT be PURGED and RENAMED at end of run (flag is FALSE)");
      cm_msg(MINFO,"tr_stop","Note - kill button sets this flag while run is being killed");
      /*
	cm_msg(MINFO,"tr_stop","You may run 'cleanup' to execute end-of-run procedure for this run");
	cm_msg(MINFO,"tr_stop","once you have deleted any bad saved files");
      */
    }
  else
    {
      if(debug)
	{
	  printf(" End of run purge/rename procedure flag (%d)  is TRUE\n"
		 ,fmdarc.end_of_run_purge_rename);
	  printf("tr_stop: saved_data_directory: %s\n",fmdarc.saved_data_directory);
	  printf("tr_stop: archived_data_directory: %s\n",fmdarc.archived_data_directory);
	  printf("calling darc_rename with run number %d   \n",rn);
	}
      
      /* purge/rename final run file  & archive */    
      status = darc_rename_file(rn, fmdarc.saved_data_directory
				, fmdarc.archived_data_directory, filename);
      if (status == SUCCESS)
	{      
	  /* update filename in odb 
	     &  make sure we write correct length string to odb */
	  sprintf(fmdarc.last_saved_filename, "%s", filename);
	  sprintf(str,"last_saved_filename");
	  if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			   fmdarc.last_saved_filename, str);
	  size=sizeof(fmdarc.last_saved_filename);
	  status = db_set_value(hDB, hMDarc, str, fmdarc.last_saved_filename, size, 
				1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/settings/%s", eqp_name,str);
	      write_message1(status,"tr_stop");
	    }
	}
    }

  if (debug) 
    printf("tr_stop: releasing camp connection and saving odb\n");
  DARC_release_camp(); /* close the camp connection on end of run */

#ifdef IMUSR
  /* Close the plain-text data log file, if any */
  if (Imusr_log_fileHandle) 
    fclose (Imusr_log_fileHandle);
  Imusr_log_fileHandle = NULL;
#endif

  odb_save(rn, fmdarc.saved_data_directory);  // save odb
  stop_time = ss_time(); // set a timer when run was stopped (if saving data only)
  //                          this should get automation run number checker to run after the run is ended

  return SUCCESS;
}

/*----- process_event IDAT ----------------------------------------------*/
void  process_event(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[YB_STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  double * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    num_HistBanks;
  INT    bank_length; /* local variable */
  char   type[10];
  char   str[128];
  DWORD  itemp;


  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event: Starting with Ser: %ld, ID: %d, size: %ld and bGotEvent %d\n",
	   pheader->serial_number, pheader->event_id, pheader->data_size,bGotEvent);
  
  first = TRUE; 
  status = yb_any_event_swap(FORMAT_MIDAS, pheader);
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event: #banks:%i Bank list:-%s-\n", nbk, banklist);
  
  /*
    process histogram bank
  */
  
  /* For imusr we are looking for the following banks:
     ONE IDAT  scaler data bank  - sent by frontend, then save data
     DARC header BOR EOR
     CAMP info  IMUSR_CAMP_VAR camp_info; BOR and EOR
     CVAR values   periodic, read when running, update ODB - manually trigger ?
     
  */
  
  
  
  num_HistBanks=0; /* zero number of histogram banks in this event (local)
		      no. of variables in the bank */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
  {
    /* bank name */
    strncpy(bk, &banklist[jj], 4);
    bk[4] = '\0';
    if (  (strncmp(bk,"IDAT",4) == 0)  )
    {
      if(debug_proc)printf ("Found bankname IDAT , bk=%s\n",bk);
      num_HistBanks++;
      
      /* Find the length of the bank  */
      
      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
      
      
      if(debug_proc)
	printf("bank type = %d, length = %d \n",bktyp,bklen);
      if (bktyp != TID_DOUBLE )
      {
	cm_msg(MERROR,"process_event","Error - Data type %d illegal for Histogram banks\n",bktyp);
	return;
      }
      bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
      if (bank_length == 0)
      {
	cm_msg(MERROR,"process_event","Error - Bank %s length = 0; Bank not found", bk);
	return;
      }
      else
	if(debug_proc)printf("Number of words in bank is %d\n",bank_length);
    }
    else
      if(debug_proc)printf ("bankname is NOT IDAT , bk=%s\n",bk); 
  } // end of for loop
  


  if(debug_proc)printf("process_event: event contains %d front-end data banks\n",num_HistBanks);  
  if(num_HistBanks != 1)
  {
    /* only 1 bank allowed for IMUSR */
    cm_msg(MERROR,"process_event","Error - More than 1 (i.e. %d) IDAT Banks found in the event", num_HistBanks);
    return;
  }
  if(debug_proc)
    {
      for (i=0; i<bank_length; i++)
	printf("index %d  contents = %f -> %d \n",i, pdata[i],  (DWORD)pdata[i] );
    }
  /* read number of words expected in bank from odb */
  size = sizeof(imusr.imdarc.num_datawords_in_bank);
  status = db_get_value(hDB, hIS, "imdarc/num datawords in bank", &imusr.imdarc.num_datawords_in_bank, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"process_event","could not get value for \"../imdarc/num datawords in bank\" (%d)",status);
    return;
  }
  /* For IMUSR, the number of words in the IDAT bank determines the number of histograms saved */
  
  nHistograms = imusr.imdarc.num_datawords_in_bank -1 ; // first value (data point counter) is not histogrammed
  
  if(debug_proc)printf("process_event: No. data words expected (from odb): %d; no. data in bank:%d\n",
		       imusr.imdarc.num_datawords_in_bank,bank_length);
  
  
  if( bank_length != imusr.imdarc.num_datawords_in_bank )  /* compare with expected value supplied by odb  */
  {
    cm_msg(MERROR, "process_event", "Frontend has sent %d words in bank IDAT. Expected %d",
	   bank_length, imusr.imdarc.num_datawords_in_bank);
    return;
  }
  
    
  if(!write_data)
  {
    /* bail out at this point if we are not saving the data */
    if(debug || debug_proc) printf("process_event: not saving data since logging is disabled\n"); 
    return;
  }
  /* check we have some storage available */
  if (pHistData == NULL)
  {
    cm_msg(MERROR,"process_event", "Space for histogram storage has not been allocated" );
    return;
  }
  
  
  /* store the data into the array pointed at by pHistData */
  gbl_data_point = (INT) (pdata[0]);
  if(debug_proc)printf("Data point : %d \n",gbl_data_point);
// data point gives the index into the arrays and the number of bins in the histogram
  nHistBins =  gbl_data_point +1;
  
  if (nHistBins >  imusr.imdarc.maximum_datapoints)
  {
    cm_msg(MERROR, "process_event", "Front end has sent %d data points this run, which exceeds maximum allowed (%d)",
	   nHistBins, imusr.imdarc.maximum_datapoints);
    cm_msg(MERROR, "process_event", "***  No more datapoints can be stored this run. Stop run and restart! *** ");
    return;
  }
  // gbl_data_point gives the number of bins in each histogram to be saved
  
  /* write to plain-text data log file */
  // TEMP:
  if(Imusr_log_fileHandle != fileH)printf("*** error fileH has changed\n");
  if ( Imusr_log_fileHandle )
    fprintf (Imusr_log_fileHandle,"%4d",gbl_data_point);


  pdata++; // skip over no. of points - not histogrammed.
  

  for(i=0; i<nHistograms; i++)  // for the number of histograms
  {
    offset =  i * imusr.imdarc.maximum_datapoints ; /* offset in words into
						       pHistData */
    itemp = (DWORD) ((*pdata)+0.49) ;

    ( (long*)(pHistData + (offset*sizeof(long))))[gbl_data_point] = itemp;
    if(debug_proc)
      printf("Index %d  offset into pHistData=%d : Copied *pdata = %f ->  0x%x (%d)  to pHistData[%d]=0x%x\n",
	   i,offset , *pdata, itemp,itemp, gbl_data_point+offset,
	   ((long*)pHistData)[gbl_data_point +offset]);
    pdata++;


    if(Imusr_log_fileHandle != fileH)printf("*** error fileH has changed (2)\n");

    /* write to plain-text data log file */
    if ( Imusr_log_fileHandle )
      fprintf (Imusr_log_fileHandle," %8d",itemp);

  } // done all histograms

  if ( Imusr_log_fileHandle ) 
    { 
      fprintf (Imusr_log_fileHandle,"\n");
      fflush( Imusr_log_fileHandle );
    }

  if(debug_dump)
    {
      /* check the input params each time so our array dump can increase
	 as more data points are added */
      db=dump_begin;
      dl=dump_length;
      /* check parameters dump_begin, dump_length are valid */
      if(db < 0 || db > gbl_data_point) db = 0; // begin of dump
      if(dl < 1 || (dl + db) > gbl_data_point)
	dl = gbl_data_point + 1  - db;
      if((db+dl) <= gbl_data_point) db=gbl_data_point-dl+1;
      printf("debug_dump -  Dump_length is  %d, dump_begin=%d\n",
	     dl,db);
      
      printf("%s bank: data point %d, offset in individual copied array will be: %d\n",
	     bk,gbl_data_point ,db);
      
      printf("Dumping contents of arrays : \n");
      for (j=0; j<nHistograms; j++)
	{
	  offset =  j * imusr.imdarc.maximum_datapoints; /* offset in words to start of histogram in pHistData */
	  printf("Histogram %d starting at Offset = %d\n",j,offset);
	    for (i=db; i< (dl + db) ;i++)
	      {
		printf("Data point %d:  pHistData[%d]=0x%x (%d)\n",
		       i, (i+offset),  ((long*)pHistData)[i+offset], ((long*)pHistData)[i+offset]) ;
	      }
	}
      printf("gbl_data_point = %d\n",gbl_data_point);
    } // end of debug_dump
  if(debug_proc)
    printf("process_event:  setting bGotEvent TRUE\n");
  
  bGotEvent = TRUE;
  
  return;
}

/*---- HOT_LINKS ---------------------------------------------------*/
INT setup_hotlink()
{
  /* does all initialization including setting up hotlinks */
  char str[128];
  HNDLE hktmp;
  char  *s;
  INT i,j;
  

  //  MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);


  if(debug) printf("setup_hotlink starting\n");

  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
    {
      cm_msg(MERROR, "setup_hotlink","Another mdarc client has been detected");
      return(CM_NO_CLIENT); /* return an error */
    }



  status=mdarc_create_record();
  if(status != DB_SUCCESS)
    return status;
  
  /*
    get the path for the perl script run_number_check.pl
  */
  sprintf(perl_script,"%s",fmdarc.perlscript_path);
  trimBlanks(perl_script,perl_script);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
 
  //printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);
  if ( i == (j-1) )
  {
    //if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
    perl_script[i]='\0';
  }
  strcpy(perl_path,perl_script); /* save in global; needed later (also by imusr_darc for MUSR) */
  strcat(perl_script,"/get_next_run_number.pl");
  if(debug)printf("setup_hotlink: using perl_script = %s\n",perl_script);

/* create the record for IMUSR */
  status = imusr_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to imusr_create_rec (%d)\n",status);
    return(status);
  }

/* create the record for camp */
  status = camp_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to camp_create_rec (%d)\n",status);
    return(status);
  }
  
  /*  S E T   U P   H O T   L I N K S */

  /* set hot link on num_versions_before_purge  */
  if (db_find_key(hDB, hMDarc, "num_versions_before_purge", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.num_versions_before_purge);
    status = db_open_record(hDB, hktmp, &fmdarc.num_versions_before_purge, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for num_versions_before_purge (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
    else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for num_versions_before_purge   (%d)", status );

  
  /* set hot link on end_of_run_purge_rename  */
  if (db_find_key(hDB, hMDarc, "end_of_run_purge_rename", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.end_of_run_purge_rename);
    status = db_open_record(hDB, hktmp, &fmdarc.end_of_run_purge_rename, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for end_of_run_purge_rename (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for end_of_run_purge_rename (%d)", status );
  
    
  /* set hot link on save_interval  */
  if (db_find_key(hDB, hMDarc, "save_interval(sec)", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.save_interval_sec_);
    status = db_open_record(hDB, hktmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for save_interval (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for save_interval (%d)", status );

  /* saving previous save_Time for limit check later in update_Time */
  time_save = fmdarc.save_interval_sec_;
  if((INT)time_save < 1 || (INT)time_save > 24*3600 )                 /* time_save > 1sec, < 1 day */
  {
    fmdarc.save_interval_sec_ = time_save = 60;
    size=sizeof(fmdarc.save_interval_sec_);
    status = db_set_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
  }

  saving_now = FALSE; // initialize - only used for MUSR

#ifndef IMUSR
  if (db_find_key(hDB, hMDarc, "histograms/musr/save now", &hSave) != DB_SUCCESS)
    {
      hSave=0;
      cm_msg(MERROR,"setup_hotlink","Failed to find key for \"save now\" (%d)", status );
      cm_msg(MINFO,"setup_hotlink","Hot link to save data is not available");
    }
  else
    if(debug)printf("setup_hotlink: got key for hSave=%d\n",hSave);
  /* hot link is actually opened at begin of run */
#endif


  if(debug)
  {
    printf("setup_hotlink: Updated parameters from odb\n");
    printf("    time_save = %d \n",time_save);
    printf("    arch_dir = %s\n", fmdarc.archived_data_directory);
    printf("    save_dir = %s\n", fmdarc.saved_data_directory);
    if(fmdarc.end_of_run_purge_rename)
      printf("  eor flag is set : %d\n", fmdarc.end_of_run_purge_rename);
    else
      printf("  eor flag is NOT set : %d\n", fmdarc.end_of_run_purge_rename);
  }

  status = create_ClFlgs_record(); // get the record for the client flags
  if (status != DB_SUCCESS)
    printf("setup_hotlink: error return from create_ClFlgs_record (%d)\n",status);


  /* set up hot link on musr_config flag */
  if (db_find_key(hDB, hCF, "musr_config", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fcf.musr_config);
    status = db_open_record(hDB, hktmp, &fcf.musr_config, size, MODE_READ, hot_musr_config , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for client flag musr_config (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for client flag musr_config (%d)", status );
      return(status);
    }
  return(status);
}
/*------------------------------------------------------------------*/
INT setup_hot_toggle()
{
  /* called by tr_start only on genuine run start, so toggle cannot be set before 
     a valid run number is established */
  char str[128];
  HNDLE hktmp;
  
  
  if(debug) printf("setup_hot_toggle starting\n");

  
/* set up hot link on toggle flag */
  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
  {
    /* make sure toggle bit is switched off to start with */
    toggle = FALSE ;  // global
    size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "setup_hot_toggle", "cannot initialize toggle flag ");
    
    status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hot_toggle","Failed to open record (hotlink) for toggle (%d)", status );
      write_message1(status,"setup_hot_toggle");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hot_toggle","Failed to find key for toggle (%d)", status );
  
  return(status);
}

#ifndef IMUSR
/*------------------------------------------------------------------*/

INT setup_hot_save()

/* ----------------------------------------------------------------------------------------*/
{
  /* called after by tr_start on genuine run start
     to  set up hot link on "save now" flag */

  char str[128];
  BOOL off = FALSE;
  
  
  if(debug)printf("setup_hot_save starting with hSave=%d\n",hSave);
  
  /* global key hSave has been found in setup_hotlink */

  if (hSave != 0)
    {

      /* make sure global flag "save now"  is set off to start with */
      status = db_set_value(hDB, hMDarc, "histograms/musr/save now", &off, sizeof(BOOL), 1, TID_BOOL);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "setup_hot_save", "cannot initialize \"save now\" flag ");
    
      status = db_open_record(hDB, hSave, &fmdarc.histograms.musr.save_now, size, MODE_READ, hot_save , NULL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hot_save","Failed to open record (hotlink) for \"save now\" (%d)", status );
	  write_message1(status,"setup_hot_save");
	  return(status);
	}
    }
  else
    cm_msg(MERROR,"setup_hot_save","No key for \"save now\" (%d)", status );
  
  return(status);
}

/* ----------------------------------------------------------------------------------------*/

void hot_save (HNDLE hDB, HNDLE hSave ,void *info)

/* ----------------------------------------------------------------------------------------*/

{
/*  Save now is done by setting the flag ..mdarc/histograms/musr/save now
      later could add a user button for this   
 */ 
  
  // uses BOOL save_now - a global 
  char str[128];
  char cmd[256];
  BOOL off=FALSE;
  INT i,state_running;
  
  printf ("hot_save: starting .....\"save now\" has been touched\n");

  /* "save now"  has been touched - user wants to save the histograms now */
  if(!write_data)
    {
      cm_msg(MINFO,"hot_save","Hot save CANNOT SAVE DATA since data logging is disabled in odb");
      return;
    }


  if (saving_now)
    {
      cm_msg(MINFO,"hot_save","Imusr_darc is already in the process of saving the data");
      return;
    }
  else
    {
      saving_now = TRUE;   // set a flag to say data is being saved
      last_time = ss_time(); // reset the time
      printf("hot_save: Calling trigger_histo_event as saving_now is %d\n",saving_now);
      if( trigger_histo_event(&state_running) ) // an event has been triggered (write_data MUST also be true)
	{
	  INT msg;
	  printf("hot_save: event has been triggered\n");
	  msg = cm_yield(1000); /* call yield */
	  ss_sleep(2000); // wait 2 seconds
	  
	  /* has process_event run ? */	  
	  i=0;
	  while(!bGotEvent)
	    {
	      i++;
	      printf("bGotEvent is false; waiting for process_event to run (%d).",i);
	      msg = cm_yield(1000); /* call yield */
	      ss_sleep(500); // sleep
	      if(i>10)
		{
		  printf("... given up waiting\n");
		  break;
		}
	    }
	  
	  if(!bGotEvent)
	    {
	    printf("bGotEvent is still false; no data available\n");
	    cm_msg(MINFO,"hot_save","No data is available to be saved");
	    }	  
	  else
	    {
	      printf("\n");
	      cm_msg(MINFO,"hot_save","Hot save is calling imusr_darc to save the data");
	      if(debug)printf("hot_save: Calling imusr_darc to save the data\n");
	  
	      /* save the event */ 
	      status = imusr_darc(pHistData);
	    }
	} // if trigger_histo_event
      else
	printf("hot_save: could not trigger an event\n");
      saving_now = FALSE;
    }
  return;
}  
  
#endif   //  Save now procedure is defined for MUSR only

/*------------------------------------------------------------------*/
void update_Time(HNDLE hDB, HNDLE htmp, void *info)
{

  /* remove hot link */
  if(debug)printf("update_Time starting...present time_save=%d\n",time_save);
  
  db_close_record(hDB, htmp);
  
  /* get the new value */

  size = sizeof(fmdarc.save_interval_sec_);
  status = db_get_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, &size, TID_DWORD, FALSE);

  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"update_Time","could not get new value of save interval ");
      write_message1(status,"update_Time");
      return ;
    }
  if(debug)printf("update_Time: Time_save is to be updated to %d\n",fmdarc.save_interval_sec_);

  if((INT)fmdarc.save_interval_sec_ < 1
     || (INT)fmdarc.save_interval_sec_ > 24*3600 ) /* time_save > 1sec, < 1 day */
    {
      cm_msg(MINFO,"update_Time","Invalid time save interval (%d). Using previous value (%d sec)",
	     fmdarc.save_interval_sec_,time_save);
      fmdarc.save_interval_sec_ = time_save;
      
      /* update the odb with this value */
      if(debug)printf("update_Time: Restoring /Equipment/%s/mdarc/save_interval(sec) to %d\n"
		      , eqp_name, fmdarc.save_interval_sec_);
      size=sizeof(fmdarc.save_interval_sec_);
      status = db_set_value(hDB, hMDarc, "save_interval(sec)"
			    , &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"update_Time","cannot write to odb key /Equipment/%s/mdarc/save_interval(sec) %d"
		 , eqp_name, status );
	  write_message1(status,"update_Time");
	}   
    }  // end of bad value

  time_save = fmdarc.save_interval_sec_;
  if (debug)printf("update_Time: Time_save is now updated to %d\n",fmdarc.save_interval_sec_);


  /* restore Hot link */
  size = sizeof(fmdarc.save_interval_sec_);
  status = db_open_record(hDB, htmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
  if (status != DB_SUCCESS)
  {
      cm_msg(MERROR,"update_Time","Failed to open record (hotlink) for mdarc (%d)", status );
      write_message1(status,"updata_Time");
      return;
  }

   return;
}

/*------------------------------------------------------------------*/

void hot_toggle (HNDLE hDB, HNDLE hktmp ,void *info)
{
/* toggle is done by user button TOGGLE - calls a perlscript toggle.pl which
   sets odb toggle flag on certain conditions - including that automatic run
   numbering in enabled */ 
  
  // uses BOOL toggle - a global in mdarc.h 
  char str[128];
  char cmd[256];
  char old_type[10];
  
  
  /* toggle bit has been touched - user wants to toggle between real and test
     runs  */
  printf ("hot_toggle: starting ..... toggle has been touched.  \n");
  if(debug)
    printf ("hot_toggle: global toggle flag = %d, run number = %d,  closing record\n",toggle,run_number);
  /* remove hot link while we toggle the run */
  db_close_record(hDB, hktmp);
  
  if (toggle) /* shouldn't happen if hot link is closed */
    {
      printf("hot_toggle: unexpected error - toggle global flag is already set\n");
      return;
    }
  
  
  toggle = TRUE;   /* global flag */
  
  old_run_number=run_number; /* save run number */
  strcpy(old_type,run_type);
  
  if (debug)
    printf("hot_toggle: old run number = %d, and  run type = %s\n",
           old_run_number, old_type);
  
  /* determine the run type */
  if(debug) printf ("hot_toggle: run_type = %s \n",run_type);
  if  (strncmp(run_type,"real",4) == 0)
    {
      if(debug) printf ("hot_toggle: detected present run type as real \n");
      strcpy(run_type,"test"); /* run is real so we now want a test run */
      //if (debug) printf ("now should be test;  run type=%s \n",run_type);
    }
  else if  (strncmp(run_type,"test",4) == 0)
    {
      if (debug) printf ("hot_toggle: detected present run type as test \n");
      strcpy(run_type,"real"); /* run is test so we now want a real run */
      //if (debug) printf ("now should be real;  run type=%s \n",run_type);
    }
  else
    {
      cm_msg(MERROR,"hot_toggle","Unknown run type detected (%s). Cannot toggle run \n",run_type);
      return; 
    }
  if(debug) printf ("Writing new run_type = %s to odb\n",run_type);
  sprintf(str, "/Equipment/%s/mdarc/run type",eqp_name);
  size = sizeof(run_type);
  if(size != sizeof(fmdarc.run_type))
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
	     str);
      return;
    }

  status = db_set_value(hDB, 0, str, run_type, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
	     str,status);
      write_message1(status,"hot_toggle");
      return ;
    }    
  
  /*
    Get a new run number with perl script
  */
  if(debug) printf ("hot_toggle: getting a run number for a %s run...\n",run_type);
  unlink(outfile); /* delete any old copy of perl output file in case of failure */

  /* add parameter perl_path */
  sprintf(cmd,"%s %s %s %d %s 0, %s",perl_script,perl_path,expt_name,run_number,eqp_name,beamline);
  if(debug) printf("Hot_toggle: sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  if (status)
    /* cannot get a new run number */
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR," hot_toggle",
	      "A status of %d has been returned from perl script get_next_run_number.pl",status);
      status=print_file(outfile);   
      if(status)
	cm_msg (MERROR,"hot_toggle","There may be compilation errors in the perl script");
      else
	{ // make a copy of the error file as the outfile is overwritten each time
	  sprintf(cmd,"cp %s %s",outfile,errfile);
	  status = system(cmd);
	  if(status)cm_msg(MINFO,"tr_start","failure from system command: %s",cmd);
	}
      if(debug) printf(" hot_toggle: Resetting run type to previous value (%s)",old_type);
      
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      if(size != sizeof(fmdarc.run_type))
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
		 str);
	  return;
	}

      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}
      toggle = FALSE;   /* clear global flag */
      cm_msg (MERROR," hot_toggle","Cannot toggle run. Continuing with run %d",run_number);
      return;
    }
  
  //  get the new run number - this is now a global value in mdarc.h
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"hot_toggle","key not found  /Runinfo/Run number");
      write_message1(status,"hot_toggle");
      return ;
    }
  if (run_number == old_run_number)
    {
      status=cm_msg(MERROR,"hot_toggle","??? Strange error: Run number not toggled after perlscript ");
      /* rewrite old run type */
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}    
      return;
      
    }
  else
    status=cm_msg(MINFO,"hot_toggle",
                  "* * * *  hot_toggle:  Run number has been toggled from %d to %d ( %s to %s) * * * * ",
                  old_run_number,run_number,old_type, run_type);
  
  return;
}

INT print_file(char* filename)
{
  FILE *FIN;
  char str[256];

  printf ("\n------------------------------------------------------------------------\n");
  printf ("   print_file: Contents of information file %s:\n",filename);
  printf ("--------------------------------------------------------------------------\n");
  FIN = fopen (filename,"r");
  if (FIN == NULL)
  {
      printf ("print_file : Error opening file %s\n",filename);
      return(1);     
  }
  else
  {
    while(fgets(str,256,FIN) != NULL)
      printf("%s",str);
  }
  fclose (FIN);
  printf ("--------------------------------------------------------------------------\n");
  printf ("   print_file: End of information file %s: \n",filename);
  printf ("--------------------------------------------------------------------------\n\n");
  return(0);
}


//---------------------------------------------------------------------------------------
INT get_run_state(INT *pval)
//----------------------------------------------------------------------------------
{
  INT my_run_state;

  *pval=0;

  /* get current run state */
  size = sizeof(my_run_state);
  /* get current run state */
  status = db_get_value(hDB, 0, "/runinfo/State", &my_run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      printf("get_run_state:cannot access /runinfo/State (%d)... retrying after 500ms\n",status); 
      ss_sleep(500); /* wait for a moment */
      status = db_get_value(hDB, 0, "/runinfo/State", &my_run_state, &size, TID_INT, FALSE);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_run_state","cannot access /runinfo/State (%d)",status);
	  return status;
	}
    }
  *pval = my_run_state;
  return status;
}



/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT     size;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg, i;
  BOOL   daemon=FALSE;
  int j=0;  
  char   dbg[25];
  int    bug;

#ifndef IMUSR
  BOOL event_triggered=FALSE;
#endif
  

  printf("\n IMUSR mdarc (version  %.2f) : Data Logger saves MIDAS data in MUD format \n\n",version); 
 
  /* set defaults for globals */
  debug = debug_proc = debug_mud = debug_check = debug_dump = FALSE;
  saving_now = FALSE;
  dump_begin = 0;
  dump_length = 5;
  run_number = -1; /* initialize to a silly value for go_toggle */ 
  toggle = FALSE;  /* initial value */
  
  /* end defaults */
  
  /* for Midas 1.9.3 */
    cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
  
  /* for earlier Midas */
    /* cm_get_environment (host_name, expt_name); */

  /* initialize global variables */

  sprintf(eqp_name,"MUSR_TD_acq");
  sprintf(i_eqp_name,"MUSR_I_acq");
  sprintf(feclient,"fe_musr"); // TEMP ... fe_musr for VXWORKS client


  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    /* Run as a daemon */    
    if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    
    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
        goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i],"-q",2)==0)
        strcpy(eqp_name, argv[++i]);
      else if (strncmp(argv[i],"-f",2)==0)
        strcpy(feclient, argv[++i]);
/*      else if (strncmp(argv[i],"-s",2)==0)   put time as odb param
        time_save = atoi(argv[++i]);
*/

      else if (strncmp(argv[i],"-d",2)==0)
      {
        strcpy(dbg, argv[++i]);  // dbg = debug level 
        debug=TRUE;
      }
      else if (strncmp(argv[i],"-b",2)==0)  /* in mdarc.h (these are debug parameters) */
        dump_begin =  atoi(argv[++i]);
      else if (strncmp(argv[i],"-l",2)==0)
        dump_length =  atoi(argv[++i]);
    }
    else
    {
   usage:
      printf("usage: imdarc  [-h Hostname] [-e Experiment]\n");
//      printf("              [-s time interval (sec)] to save data
//                                (def:60s)\n");
      printf("              [-f FE client name] (default:%s)\n",feclient);
      printf("              [-q FE equipment name] (default:%s)\n",eqp_name);
      printf("              [-D ] (become a daemon)\n");
      printf("              [-d debug level] (0=debug 1=mud/camp 2=save files 3=dump 4=process event 5=all\n");
      printf("              [-b begin] [-l length] (histo dump params for debug level=3. Default b=0 l=5. \n");
      printf(" e.g.  imdarc -h dasdevpc -e bnmr  \n");
      return 0;
    }
  }
 
  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
    }


/* convert expt_name to lower case */
  for (j=0; j<strlen(expt_name); j++) expt_name[j]=tolower (expt_name[j]); 
/* make sure we have the right version of imdarc for this experiment */

  if (  strncmp(expt_name,"imusr",5)  )
  {
    printf("This version of imdarc for IMUSR does  NOT support experiment %s\n",expt_name);
    goto error;
  }

    
  if (debug)   
  {
    bug = atoi(dbg);
    switch (bug)
    {
    case 0:      /* level 0  Just debug */
      break;   
    case 1:      /* level 1  Just debug_mud  */
      debug_mud = TRUE;
      debug = FALSE;
      break;   
    case 2:      /* level 2 Just debug_check */
      debug_check = TRUE;
      debug = FALSE;
      break;
    case 3:
      debug_dump = TRUE;
      break;
    case 4:
      debug_proc = TRUE;
      break;
    case 5:
      debug_dump=TRUE;
      debug_mud=TRUE;
      debug_check=TRUE;
      debug_proc=TRUE;
      break;
    default: 
    }
  }
  if(debug_dump) printf("main: data dump will begin at bin = %d, length = %d\n",dump_begin,dump_length);

  /* connect to experiment */
  printf("calling cm_connect_experiment with host_name \"%s\", expt_name \"%s\"\n",host_name,expt_name);
  status = cm_connect_experiment(host_name, expt_name, "Imdarc", 0);
  if (status != CM_SUCCESS)
    goto error;

  /* turn off watchdog if in debug mode */
  if (debug)
    cm_set_watchdog_params(TRUE, 0);
  
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);
  
  /* open the "system" buffer, 1M size */
  bm_open_buffer("SYSTEM", EVENT_BUFFER_SIZE, &hBufEvent);

  /* set the buffer cache size */
  bm_set_cache_size(hBufEvent, 100000, 0);
  
  /* place a request for a specific event id */
  /* IMUSR needs to process at BOR/EOR :
     DARC   ID 14
     CAMP   ID 15
   and while running : 
     IDAT   ID 7
     CVAR   ID 13
  */
  bm_request_event(hBufEvent, 7, TRIGGER_ALL, GET_ALL, &request_id,
		   process_event);
  bm_request_event(hBufEvent, 13, TRIGGER_ALL, GET_ALL, &request_id,
		   process_event_cvar);  
//  bm_request_event(hBufEvent, 14, TRIGGER_ALL, GET_ALL, &request_id, process_event_darc);  
  bm_request_event(hBufEvent, 15, TRIGGER_ALL, GET_ALL, &request_id,
		   process_event_camp);


  /* Register to the Transition */
 
  if (cm_register_transition(TR_POSTSTART, tr_start) != CM_SUCCESS ||
      cm_register_transition(TR_POSTSTOP, tr_stop) != CM_SUCCESS ||
      cm_register_transition(TR_PRESTART, tr_prestart) != CM_SUCCESS)
  {
       
    printf("Failed to register\n");
    goto error;
  }

  /* connect to ODB */
  status = cm_get_experiment_database(&hDB, &hKey);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR,"imdarc","failure getting experiment database (%d)\n",status);
      return 0;
    }      


  printf("calling get_run_state \n");
  /* get the run state to see if run is going */
  status = get_run_state(&run_state);
  if(status != DB_SUCCESS) 
    {
      cm_msg(MERROR,"imdarc","cannot determine run state (%d)\n",status);
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
#ifdef IMUSR
  if (run_state != STATE_STOPPED)
    {
      cm_msg(MERROR,"imdarc","cannot save imusr data when a run is already in progress\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
#endif


  /* Create and setup <eqp_name>/settings */
  if(debug)printf ("calling setup_hotlink\n");  
  status = setup_hotlink(); /* initialization including set up keys hMDarc,hSave and open hot links */
  if(status!=DB_SUCCESS)
  {
    cm_disconnect_experiment(); /* abort the program. Cannot save the data */
    return 0;
  }

  /* Init sequence through tr_prestart once */
  {
    char str[256];
    if(tr_prestart(0, str) !=DB_SUCCESS)
    {
      printf("Failure from tr_prestart\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
     
    if(tr_start(0, str) !=DB_SUCCESS)
    {
      printf("Failure from tr_start\n");
      cm_disconnect_experiment(); /* abort the program. Cannot save the data */
      return 0;
    }
  }

  if(debug)
  {
    printf("\n");
    printf("imdarc parameters : \n");

    printf("Camp hostname: %s\n",fmdarc.camp.camp_hostname);

    printf("Time of last save: %s\n",fmdarc.time_of_last_save);
    printf("Last saved filename: %s\n",fmdarc.last_saved_filename);
    printf("Saved data directory: %s\n",fmdarc.saved_data_directory);
    printf("# versions before purge: %d\n",fmdarc.num_versions_before_purge);
    printf("End of run purge/rename flag: %d\n",fmdarc.end_of_run_purge_rename);
    printf("Archived data directory: %s\n",fmdarc.archived_data_directory);
    printf("Save interval (sec): %d\n",fmdarc.save_interval_sec_);
  }

  
  /* initialize ss_getchar() */
  ss_getchar(0);
  
  last_time = 0;
#ifndef IMUSR
  event_triggered=FALSE; /* no event has been triggered */
#endif 
 do
    {
      /* call yield once every second */
      msg = cm_yield(1000);
      
	 if(start_time > 0) // run has recently started 
	   {
	     if(ss_time() - start_time > 20) // allow 20s for everything to start
	       {
		 /* if !fcf.enable_client_check, then check_client_flag will issue a warning message
		    but will not stop the run */
		 status = check_client_flags(&tr_flag); // may stop the run
		 if(status == DB_SUCCESS)
		   {
		     printf("check_client_flag returns status=%d and tr_flag=%d\n",status,tr_flag);
		     if(tr_flag) // run is in transition; leave the flag set for next time
		       printf("run is still in transition... waiting to recheck client flags\n");
		     else
		       start_time = 0; // reset so we don't come here again
		   }	    
	       }
	   } // start_time > 0


     if( fmdarc.enable_poststop_rn_check)
       {
	 if(stop_time > 0) // run has recently stopped
	   if(ss_time() - stop_time > 30) // allow 30s for everything to have stopped
	     {
	       /* run the run number checker ready for next time */
	       if (!fmdarc.disable_run_number_check)
		 {
		   printf("\n\n mdarc: calling auto_run_number_check 30s after a stop\n");
		   status = auto_run_number_check(0);
		   if (status != DB_SUCCESS)
		     cm_msg(MERROR,"mdarc","Warning: post-stop run number check failed"); // auto_run_number_check failed
		   else
		     printf("mdarc main: success from post-stop run number check\n"); // auto_run_number_check worked
		   stop_time = 0; // reset so we don't come here again
		 }
	     }
       } // poststop rn check
	 
      /* time to save data */
      if (ss_time() - last_time > time_save )
	{
	  last_time = ss_time();
#ifndef IMUSR
	  if(saving_now)
	    printf("Imdarc: flag is set to say data is ALREADY being saved (by hotlink \"save now\"\n");
	  else
	    {
	      if(write_data)  // don't request an event if imdarc is not writing it 
		{
		  saving_now = TRUE; // set a flag to tell hot_save that a save is in progress
		  if(debug)printf("imdarc: Calling trigger_histo_event; \n");
		  event_triggered = trigger_histo_event(&run_state);
		  if(debug)
		    printf("trigger_histo_event returns %d with run_state=%d\n",event_triggered,run_state);
		  if(event_triggered)  // an event has been triggered (write_data MUST also be true)
		    {
		      printf("event has been triggered\n");
		      event_triggered = FALSE; // reset flag
		      
		      /* has process_event run yet? (test on bGotEvent) */
		      i=0;
		      while(!bGotEvent)
			{
			  printf("imdarc: bGotEvent is false; waiting for process_event to run (%d)\n",i);
			  msg = cm_yield(1000); /* call yield */
			  ss_sleep(500); // sleep
			  i++;
			  if(i>5)
			    {
			      printf("...given up waiting for bGotEvent\n");
			      break; // event is not coming for some reason
			    }
			}
		      
		      if(!bGotEvent) 
			{
			  printf("imdarc:bGotEvent is still false after %d loops; cannot save data\n",i);
			  cm_msg(MINFO,"imdarc","No data is available at this time to save");
			}
		      else
			{
			  printf("Imdarc: Calling imusr_darc to save the data\n");
			  status = imusr_darc(pHistData);
			}
		    } // event_triggered
		  else
		    {  // event was not triggered
		      if (run_state == STATE_RUNNING)  // only send message if we are running
			cm_msg(MINFO,"Imdarc",
			       "Event has not been triggered from frontend  ... no data can be saved \n");
		    }
		} // end of if (write_data)
	      else // write_data is false
		if(debug)printf(" Imdarc: data logging is disabled in odb ... not calling imusr_darc \n");

	      saving_now = FALSE;
	    } // end of saving_now
	
#else     // IMUSR
	  if(write_data)
	    {
	      if(debug)printf("     Imdarc: Calling imusr_darc to save the data\n");
	      status = imusr_darc(pHistData);
	    }
	  else
	    if(debug)printf(" Imdarc: data logging is disabled in odb ... not calling imusr_darc \n");
#endif
	  fflush(stdout); /* output may be sent to a file */	  
	} // end of if(ss_time() ...  )

  
      /* check keyboard */
      ch = 0;
      if (ss_kbhit())
	{
	  ch = ss_getchar(0);
	  if (ch == -1)
	    ch = getchar();
	  if ((char) ch == '!')
	    break;
	}
    }
  while (msg != RPC_SHUTDOWN && msg != SS_ABORT);
  
error:
  /* reset terminal */
  ss_getchar(TRUE);
  
  DARC_release_camp();
  cm_disconnect_experiment();
}



#ifndef IMUSR
/*-----------------------------------------------------------------------------------------------*/
INT get_client_name(char *pname)
/*-----------------------------------------------------------------------------------------------*/
{
  char name[256],str[256];
  INT i,len;
  HNDLE  hKeyTmp,hSubkey;
  INT size;
  
  /* get the ACTUAL client name for the frontend */
  sprintf(name,"");
  len=strlen(feclient);
  if (cm_exist(feclient, FALSE) != CM_SUCCESS)
  {
    cm_msg(MERROR,"get_client_name","Frontend \"%s\" is not running! (%d)", feclient,status);
  }
  else
  {
    if(debug)printf("get_client_name: Client %s is running\n",feclient);
    status = db_find_key(hDB, 0, "System/Clients", &hKeyTmp);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "get_client_name", "cannot find System/Clients entry in database");
    else
    {
      /* search database for clients with transition mask set */
      for (i=0,status=0 ; ; i++)
      {
	status = db_enum_key(hDB, hKeyTmp, i, &hSubkey);
	if (status == DB_NO_MORE_SUBKEYS)
	  break;
	
	if (status == DB_SUCCESS)
	{
	  size = sizeof(name);
	  db_get_value(hDB, hSubkey, "Name", name, &size, TID_STRING, TRUE);
	  if(strncmp(feclient,name,len)==0)
	  {
	    if(debug)printf("get_client_name: found client name %s\n",name);
	    break;
	  }        
	}
      }
    }
  }
  
  strcpy(pname,name);
  if(debug)printf("get_client_name: returning client name as %s\n",pname);
 
  return;
}

/*-----------------------------------------------------------------------------------------------*/
BOOL trigger_histo_event(INT * pRunstate)
/*-----------------------------------------------------------------------------------------------*/
{
  HNDLE hconn;
  INT run_state;
BOOL event_triggered;
  
  event_triggered = FALSE;
  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"trigger_histo_event","key not found /Runinfo/State (%d)",status);
      return(FALSE); //Error
    }
  *pRunstate = run_state;
  if (run_state == STATE_RUNNING)
    {
      // Use the clientname found by get_client_name called from tr_start
      if(cm_exist(ClientName,TRUE))
	{
	  if(debug)printf("trigger_histo_event: trying to connect to the client %s\n",ClientName);
	  status = cm_connect_client (ClientName, &hconn);
	  if(status != RPC_SUCCESS)
	    cm_msg(MERROR,"trigger_histo_event","Cannot connect to frontend \"%s\" (%d)",ClientName,status);
	  else	    {  // successfully connected to frontend client
	    rpc_client_call(hconn, RPC_MANUAL_TRIG, 2); // trigger an event
	    if (status != CM_SUCCESS)
	      cm_msg(MERROR,"trigger_histo_event","Error triggering event from frontend (%d)",status);
	    else
	      {  // successfully triggered event
		if(debug)printf("trigger_histo_event: success from rpc_client_call to trigger event\n");
		event_triggered=TRUE;
		status =cm_disconnect_client(hconn, FALSE);
		if (status != CM_SUCCESS)
		  cm_msg(MERROR,"trigger_histo_event","Error disconnecting client after event trigger(%d)",status);
	      }
	  }
	} // end of cm_exist
      else
	cm_msg(MERROR,"trigger_histo_event","Frontend client %s not running",ClientName);
    } // end of if running
  return(event_triggered);
}
#endif


//--------------------------------------------------------------------------------
INT mdarc_create_record(void)
//---------------------------------------------------------------------------
{
  
  char str[128];
  INT struct_size;
 MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);

  /* For IMUSR, run state was determined just prior to calling this routine */
#ifndef IMUSR
  status = run_state = get_run_state();
  if(status != DB_SUCCESS)
    return status;
#endif

  /* find the key for mdarc */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("setup_hotlink: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hotlink","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hMDarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_hotlink", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}

      struct_size =   sizeof(MUSR_TD_ACQ_MDARC);

      
      printf("setup_hotlink:Info - size of mdarc saved structure: %d, size of mdarc record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"setup_hotlink","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"setup_hotlink","Could not create mdarc record (%d)\n",status);
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }
  
  /* try again to get the key hMDarc  */
  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_hotlink", "key %s not found (%d)", str,status);
    write_message1(status,"setup_hotlink");
    return (status);
  }

  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"setup_hotlink");
    return(status);
  }
}

//-----------------------------------------------------------------------------------

INT imusr_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for IMUSR area - called by setup_hotlink */
  
  
  MUSR_I_ACQ_SETTINGS_STR (musr_i_acq_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("imusr_create_rec is starting...\n");
//  I - MUSR
    /* find the key for I-musr area */
  sprintf(str,"/Equipment/%s/Settings",i_eqp_name); 
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
    {
      hIS=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for I-musr settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"imusr_create_rec","Failure creating I_musr settings area (%d)",status);
          write_message1(status,"imusr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hIS has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hIS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "imusr_create_rec", "error during get_record_size (%d) for imusr/settings record",status);
          write_message1(status,"imusr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_I_ACQ_SETTINGS);
      
      if(debug)
	printf("imusr_create_rec:Info - size of IMUSR Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("imusr_create_rec:Info - creating IMUSR record due to size mismatch\n");
        cm_msg(MINFO,"imusr_create_rec","creating record (I_musr Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"imusr_create_rec","Could not create imusr settings record (%d)",status);
          write_message1(status,"imusr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"imusr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hIS  */
  
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "imusr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"imusr_create_rec");
    return (status);
  }

  size = sizeof(imusr);
  status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"imusr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"imusr_create_rec");
    return(status);
  }

  // print something out from imusr record area
  if(debug)
    printf("imusr_create_rec: current sweep device  is %s\n",imusr.input.sweep_device);
  
  return(SUCCESS);
}

//-----------------------------------------------------------------------------------

INT camp_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for CAMP equipment area - called by setup_hotlink */
  
  
  CAMP_SETTINGS_STR (camp_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("camp_create_rec is starting...\n");
  /* find a key for camp equipment area */
  sprintf(str,"/Equipment/Camp/Settings"); 
  status = db_find_key(hDB, 0, str, &hC);
  if (status != DB_SUCCESS)
    {
      hC=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for Camp settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(camp_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"camp_create_rec","Failure creating camp settings area (%d)",status);
          write_message1(status,"camp_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hC has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hC, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp/settings record",status);
          write_message1(status,"camp_create_rec");
	  return status;
	}
      struct_size =   sizeof(CAMP_SETTINGS);
      
      if(debug)
	printf("camp_create_rec:Info - size of CAMP Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("camp_create_rec:Info - creating CAMP record due to size mismatch\n");
        cm_msg(MINFO,"camp_create_rec","creating record (Camp Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(camp_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"camp_create_rec","Could not create camp settings record (%d)",status);
          write_message1(status,"camp_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"camp_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hC  */
  
  status = db_find_key(hDB, 0, str, &hC);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "camp_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"camp_create_rec");
    return (status);
  }

  size = sizeof(camp);
  status = db_get_record (hDB, hC, &camp, &size, 0);/* get the whole record for camp */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"camp_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"camp_create_rec");
    return(status);
  }

  // print something out from camp record area
  if(debug)
    printf("camp_create_rec: number of camp variables is %d\n",camp.n_var_logged);
  
  return(SUCCESS);
}

//----------------------------------------------------------------------------------------
void  allocate_camp_storage( void )
//---------------------------------------------------------------------------------
{
  /* called from allocate_storage, which is called from tr_start */ 
// pCampData is a global
  DWORD nbytes;

  
  if (pCampData)    // CVAR data
    {
      free (pCampData); 
      pCampData = NULL;
    }

  /* allocate full storage space once only at BOR
     imusr.imdarc.maximum_datapoints is the maximum data points allowed
  */


  if(debug_proc)printf("allocate_camp_storage: maximum datapoints =  %d\n",  imusr.imdarc.maximum_datapoints);


  printf("allocate_camp_storage: number of Camp variables=%d\n",camp.n_var_logged);
  
  /* number of IMUSR CAMP histograms = number of camp variables  camp.n_var_logged (c.f. nHistograms)  */
  
  if(camp.n_var_logged <1)
    {
      printf("allocate_camp_storage: no camp variables to log\n");
      return;
    }

  nbytes =  imusr.imdarc.maximum_datapoints * sizeof(double) * camp.n_var_logged;    
  if(debug_proc)
    printf("Histogram allocation max datapoints * # bytes * camp.n_var_logged i.e. %d * %d *%d = %d bytes \n",
	  imusr.imdarc.maximum_datapoints,sizeof(double), camp.n_var_logged ,  nbytes);
  pCampData = (double *) malloc(nbytes ); /* allocate histogram space */
  if (pCampData == NULL)
    cm_msg(MERROR,"allocate_camp_storage","Error allocating total space for camp data %d"
	     ,nbytes);
  
  memset (pCampData,0,nbytes);
  printf("cleared pCampData\n");
  return ;
}
/*-----------------------------------------------------------------------------------*/
INT  allocate_storage( void)
/*-------------------------------------------------------------------------------------------*/
{
  /* allocate storage for IMUSR */

// pHistData is a global
  DWORD nbytes;
  
  if (pHistData) free(pHistData); pHistData = NULL;
 
  /* allocate full storage space once only at BOR
     Note for IMUSR: data is stored as DWORD in pHistData ; 
     DARC_write currently only deals with DWORD

     imusr.imdarc.maximum_datapoints is the maximum data points allowed
  */

  size = sizeof(imusr.imdarc.maximum_datapoints);
  status = db_get_value(hDB, hIS, "imdarc/maximum datapoints", &imusr.imdarc.maximum_datapoints, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"allocate_storage","could not get value for \"../imdarc/maximum datapoints\" (%d)",status);
    return;
  }
  if(debug_proc)printf("allocate_storage: Read maximum datapoints from odb as %d\n", imusr.imdarc.maximum_datapoints);

  /* Calculate the number of IMUSR histograms: this is called nHistograms */
  
  nHistograms = imusr.imdarc.num_datawords_in_bank  -1 ; /* data point counter is not
						     histogrammed ; this gives the NUMBER OF HISTOGRAMS */
  //printf("size of long is %d size of DWORD is %d\n",sizeof(long),sizeof(DWORD) );
  nbytes = imusr.imdarc.maximum_datapoints * sizeof(long) * nHistograms;    
  if(debug_proc)
    printf("Histogram allocation max datapoints * # bytes * nHistos i.e. %d * %d *%d = %d bytes \n",
	 imusr.imdarc.maximum_datapoints,sizeof(long), nHistograms ,  nbytes);
  pHistData = (caddr_t) malloc(nbytes ); /* allocate histogram space */
  if (pHistData == NULL)      cm_msg(MERROR,"allocate_storage","Error allocating total space for histograms %d"
	     ,nbytes);
  pMaxPointer = pHistData + nbytes;
  memset (pHistData,0,nbytes);
  printf("cleared pHistData\n");
  if(debug_proc)
      printf("allocate_storage: maximum pointer for phistData is %p\n",pMaxPointer);

  allocate_camp_storage();
  return (nbytes);

}

void  process_event_cvar(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[YB_STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  double * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    numBanks;
  INT    bank_length; /* local variable */
  
  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event_cvar: Starting with Ser: %ld, ID: %d, size: %ld \n",
	   pheader->serial_number, pheader->event_id, pheader->data_size);
  
  /* The serial number should be the same as the IDAT event - can use it to point to correct place in the array */
  
  
  first = TRUE; 
  status = yb_any_event_swap(FORMAT_MIDAS, pheader);
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) 
    printf("process_event_cvar: #banks:%i Bank list:-%s-\n", nbk, banklist);
  numBanks=0;
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
    {
      /* bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"CVAR",4) == 0)  )
	{
	  if(debug_proc)
	    printf ("Found bankname CVAR , bk=%s\n",bk);
	  numBanks++;
	  
	  /* Find the length of the bank  */
	  
	  status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
	  
	  
	  if(debug_proc)
	    printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
	  if (bktyp != TID_DOUBLE )
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Data type %d illegal for Camp  banks\n",bktyp);
	      return;
	    }
	  bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
	  if (bank_length == 0)
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Bank %s length = 0; Bank not found", bk);
	      return;
	    }
	  else if (bank_length != camp.n_var_logged)
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Bank %s length = %d; No. of camp variables logged = %d", bk,bank_length,camp.n_var_logged);
	      return;
	    }
	  else
	    {
	      if(debug_proc)
		printf("Number of words in CVAR bank is %d\n",bank_length);
	    }
	}
      else
	{
	  if(debug_proc)
	    printf ("bankname is NOT CVAR , bk=%s\n",bk);
	} 
    } // end of for loop
  
  if(numBanks > 1)
    {
      cm_msg(MERROR,"process_event_cvar","Error - More than 1 (i.e. %d)   %s Banks found ", numBanks,bk);
      return;
    }
  if(debug_proc)
    {
      for (i=0; i<bank_length; i++)
	printf("index %d  contents = %f -> %u \n",i, pdata[i],  (unsigned int)pdata[i] );
    }
  
  if(!write_data)
    {
      /* bail out at this point if we are not saving the data */
      if(debug || debug_proc) 
	printf("process_event_cvar: not saving data since logging is disabled\n"); 
      return;
    }
  
  /* check we have some storage available */
  if (pCampData == NULL)
    {
      cm_msg(MERROR,"process_event_cvar", "Space for camp data storage has not been allocated" );
      return;
    }
  else
    {
    if (debug_proc)
      printf("pCampData=%p\n",pCampData);
    }

  //   pheader->serial_number starts at 1 for first event
  gbl_camp_data_point =   pheader->serial_number -1 ; // cf gbl_data_pointer which starts at 0
  
  if ( pheader->serial_number  >   imusr.imdarc.maximum_datapoints)
    {
      cm_msg(MERROR, "process_event_cvar", "Front end has sent %d data points this run, which exceeds maximum allowed (%d)",
	     pheader->serial_number  ,  imusr.imdarc.maximum_datapoints);
      cm_msg(MERROR, "process_event_cvar", "***  No more datapoints can be stored this run. Stop run and restart! *** ");
      return;
    }
  
  for( i=0; i<camp.n_var_logged; i++ )
    {
      offset =  i *  imusr.imdarc.maximum_datapoints ; /* offset in words into
							  pCampData */
      if(debug_proc)
	printf("offset=%d;  gbl_camp_data_point=%d\n",offset,gbl_camp_data_point);

//    pCampData[offset + gbl_camp_data_point] =  *((int*)&f); /* float not converted */
      pCampData[offset + gbl_camp_data_point] =  pdata[i];
      if(debug_proc)
	printf("Camp variable %d :  offset %d  gbl_camp_data_point=%d : Copied pdata[%d] = %f   to pCampData[%d]= %f\n",
	       i,offset ,gbl_camp_data_point,i,pdata[i], ( gbl_camp_data_point+offset), pCampData[offset + gbl_camp_data_point]);
      
    }
  
   
  if(debug_proc)
    printf("process_event_cvar:  setting bGotCamp TRUE\n");
  bGotCamp = TRUE;
  
  return;
}

void  process_event_darc(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
}
void  process_event_camp(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
   char  bk[5], banklist[YB_STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  DWORD * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    numBanks;
  INT    bank_length; /* local variable */
  float f;
  INT    my_numCamp;
  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event_camp: Starting with Ser: %ld, ID: %d, size: %ld \n",
	   pheader->serial_number, pheader->event_id, pheader->data_size);
  
  
  first = TRUE; 
  status = yb_any_event_swap(FORMAT_MIDAS, pheader);
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event_camp: #banks:%i Bank list:-%s-\n", nbk, banklist);
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
  {
    /* bank name */
    strncpy(bk, &banklist[jj], 4);
    bk[4] = '\0';
    if (  (strncmp(bk,"CAMP",4) == 0)  )
    {
      if(debug_proc)printf ("Found bankname CAMP , bk=%s\n",bk);
      numBanks++;
      
      /* Find the length of the bank  */
      
      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
      
      
      if(debug_proc)
	printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
      if (bktyp != TID_CHAR )
      {
	cm_msg(MERROR,"process_event_camp","Error - Data type %d illegal for Camp header bank\n",bktyp);
	return;
      } 
      bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
      if (bank_length == 0)
      {
	cm_msg(MERROR,"process_event_camp","Error - Bank %s length = 0; Bank not found", bk);
	return;
      }

      if(!write_data)
	{
	  /* bail out at this point if we are not saving the data */
	  if(debug || debug_proc) printf("process_event_camp: not saving data since logging is disabled\n"); 
	  return;
	}

      if (bank_length > 0)
	{
	  if( campVars == NULL )	
	    {
	      my_numCamp = bank_length/sizeof(IMUSR_CAMP_VAR);
	      if (my_numCamp != camp.n_var_logged)
		{ 
		cm_msg(MERROR,"process_event_camp",
		       "Num camp variables (%d) from bank %s different from odb (%d)", my_numCamp,bk,camp.n_var_logged);
		return;
		}
	      printf("process_event_camp: reallocating campVars\n");
	      
	  campVars = realloc( (void*) campVars, (size_t) bank_length );
	  if( !campVars )
	    {
	      cm_msg(MERROR,"process_event_camp","Error - could not re-allocate memory for campVars"); 
	      return;
	    }
	  /*  OK, remember number globally */
	  numCamp = my_numCamp;
	}
	else
	  {
	    if( bank_length !=camp.n_var_logged *sizeof(IMUSR_CAMP_VAR) )
	      {
		cm_msg(MERROR,"process_event_camp","Error in %s banklength (%d) ",bk,bank_length); 
		free (campVars);
		campVars = NULL;
		numCamp = 0;
		return;
	      }
	  }
      }
      /*
       * Copy camp variable strucure.  Note that structure contains fixed length 
       * strings, not just pointers, so memcpy does copy everything.
       */
      memcpy( campVars, (char*) pdata, bank_length );
      printf("Copied CAMP header to campVars (%p)\n",campVars);

    }
    else
    {
      if(debug_proc)printf ("bankname is NOT CAMP , bk=%s\n",bk); 
    }
  } // end of for loop
    
}
// check_run_number should be common for bnmr,bnqr,musr,imusr so use ifdefs
/*---------------------------------------------------------------------------*/
INT check_run_number(INT rn)
/*  ---------------------------------------------------------------------------*/
{
 
  /* Checks run number and saved directories when automatic run number check is turned off. 
     Perlscripts NOT called in this mode
  */

  INT status;
  INT i,j,len,len1;
  char str[128];
  
  cm_msg(MINFO,"check_run_number","* * WARNING:  Automatic run numbering is disabled. This mode    * *");
  cm_msg(MINFO,"check_run_number","* *           is NOT RECOMMENDED except for emergencies         * *");

#ifndef MUSR   // BNMR/BNQR only
  if(debug) printf("check_run_number: Midas logger writes saved files into directory : %s\n",MData_dir);
  len = strlen(MData_dir); // filled in check_midas_logger
  len1=strlen(fmdarc.saved_data_directory);
  if (len1 <= 0  )
    {
      status=cm_msg(MERROR,"check_run_number","Empty string detected for mdarc odb key \"saved_data_directory\" ; No saved directory supplied");
      return (DB_OUT_OF_RANGE);
    }
  if (len <= 0  )
    {
      status=cm_msg(MERROR,"check_run_number","Empty string detected for odb key \"%s\" ; No saved directory supplied",str);
      return (DB_OUT_OF_RANGE);
    }
  if(debug) 
    {
      printf("mdarc saved directory : %s, length %d\n",fmdarc.saved_data_directory,len1);
      printf("mlogger saved directory : %s, length %d\n",MData_dir,len);
    }

  /* Check this directory is identical with saved directory in mdarc area
     perlscript does this (only called when auto run numbering is enabled) */

  if(strncmp(fmdarc.saved_data_directory,MData_dir,len1) != 0 )    
    {
      cm_msg(MINFO,"check_midas_logger","Midas saved data directory (\"%s\") is not same as in mdarc area (\"%s\")",
	     MData_dir,fmdarc.saved_data_directory);
    }
  else
    if(debug) printf("Midas data directory IS identical to saved_data_directory in mdarc area\n");
  
#endif  // BNMR only 

  //  get the actual run number;  (run_number is a global in mdarc.h)
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_run_number","key not found  /Runinfo/Run number");
      write_message1(status,"check_run_number");
      return (status);
    }
  printf (" Run number = %d\n",run_number);
      
  /* check run number against beamline when automatic run number checking is
     disabled  ONLY  for a genuine run start */
  //printf("rn=%d, run_state=%d\n",rn,run_state);
      
  if ( rn !=0  ||   run_state == STATE_RUNNING )    /* rn not 0 or  run is going already  */
    {      /* check run number against beamline */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      /* check for test run first */
      printf("Automatic run numbering is DISABLED: checking run number against beamline\n");
      if ( run_number  >= MIN_TEST && run_number <= MAX_TEST  ) 
	{
	  /* test run for any beamline */
	  if  (strncmp(run_type,"test",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range 30000-30499");
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number. ");
	      return (DB_INVALID_PARAM);
	    }
	  cm_msg(MINFO,"check_run_number","Starting a TEST data run (%d)",run_number);
	}
      
      else   /* real run */
	{


#ifndef MUSR // BNMR/BNQR only

	  if(run_number >= MIN_BNMR && run_number <= MAX_BNMR )   //BNMR
	    {
	      if (strncmp(beamline,"BNMR",4) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNMR for this run number",run_number, beamline );
		  cm_msg(MINFO,"check_run_number", "Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if (run_number >= MIN_BNQR && run_number <= MAX_BNQR)   //BNQR
	    {
	      if (strncmp(beamline,"BNQR",4) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNQR for this run number",run_number, beamline );
		  cm_msg(MINFO,"check_run_number", "Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }
	
#else // MUSR only   
	  if(run_number  >= MIN_M20 && run_number <= MAX_M20 )        // M20
	    {
	      if (strncmp(beamline,"M20",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20 for this run number",run_number,beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number ");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if(  run_number >= MIN_M15 &&  run_number <= MAX_M15 )      // M15
	    {
	      if (strncmp(beamline,"M15",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M15 for this run number",run_number,beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if(  run_number < MIN_M9B)   // M13
	    {
	      cm_msg(MERROR,"check_run_number",
		     "Run number (%d) matches obsolete beamline (M13)\n",run_number);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  else if ( run_number  >= MIN_M9B &&  run_number <= MAX_M9B)
	    {
	      if (strncmp(beamline,"M9B",3) != 0)  // M9B
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M9B for this run number",run_number, beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if ( run_number  >= MIN_DEV &&  run_number <= MAX_DEV  )    // DEV
	    {
	      if (strncmp(beamline,"DEV",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = DEV for this run number",run_number, beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }
#endif
    
	  else
	    {
	      cm_msg(MERROR,"check_run_number","Run number (%d) does not match a valid beamline\n",run_number); 
	      cm_msg(MINFO,"check_run_number","Test runs must be in range %d to %d",MIN_TEST,MAX_TEST);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  printf("detected run %d for beamline %s \n",run_number,beamline);
	  /*  real run */
	  if  (strncmp(run_type,"real",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range 30000-30499");
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  else
	    cm_msg(MINFO,"check_run_number","Starting a REAL data run (%d), (beamline %s) ",run_number,beamline);  
	}  // end of else real run
      
      
    } /* end of run number check on genuine start or run already going when mdarc starts */
  
      
  return (status);
}




// this code should be common with BNMR - presently perlscript has extra param for bnmr
/* ----------------------------------------------------------*/ 
INT auto_run_number_check(INT rn)
/* ----------------------------------------------------------*/ 
{
  char  cmd[132];
  INT j;

     /*
	perl script to find next valid run number and write it to /Runinfo/run number
	It (should) handle all eventualities  including mdarc restarted midway through a
	run.  It uses run_type to determine what type of run (test/real) the user
	wants, and assigns a run number accordingly.
      */
      unlink(outfile); /* delete any old copy of perl output file in case of failure */
#ifdef MUSR
      sprintf(cmd,"%s %s %s %d %s 0, %s",perl_script, perl_path, expt_name,rn,eqp_name,beamline); 
#else
      sprintf(cmd,"%s %s %s %d %s 0, %s %s",perl_script, perl_path, expt_name,rn,eqp_name,beamline,ppg_mode); 
#endif
      if(debug) printf("auto_run_number_check: Sending system command  cmd: %s\n",cmd);
      status =  system(cmd);
      
      
      if (status)
	{
	  printf (" ========================================================================================\n");
	  cm_msg (MERROR,"auto_run_number_check","Perl script get_next_run_number.pl returns failure status (%d)",status);
	  if(print_file(outfile))   
	    cm_msg (MERROR,"auto_run_number_check","There may be compilation errors in the perl script"); // no file to print
	  return (DB_INVALID_PARAM); // bad status from perl script
	}
      else
	if(debug)printf("auto_run_number_check : Success from perl script to get next run number\n");
      
      
      // Remember: 
      //     db_get_value formerly creates the key if it doesn't exist (i.e. returns
      //           no error)
      // Midas 9.0 does not create key if last param is FALSE
      
      //  get the run number which may have been changed by get_next_run_number
      //  this is now a global value in mdarc.h
      size = sizeof(run_number);
      status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"auto_run_number_check","key not found  /Runinfo/Run number");
	  write_message1(status,"auto_run_number_check");
	  return (status);
	}
      
      /* and the run type (a global)  */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
     
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      return(status);
}
